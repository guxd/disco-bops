# 源码自动生成模板 pandora-boot-archetype-docker

### 概述

* 模板: pandora-boot-archetype-docker
* 模板答疑人: [子观](https://work.alibaba-inc.com/nwpipe/u/64988)
* 模板使用时间: 2019-04-11 17:36:40

### Docker
* Image: reg.docker.alibaba-inc.com/bootstrap/image
* Tag: 0.1
* SHA256: e4b70f4f7d0b60aa3e5666eba441a376b31ec6e0bd550a4efc5af8f057c6d7d8

### 用户输入参数
* repoUrl: "git@gitlab.alibaba-inc.com:cbuscm/disco-bops.git" 
* appName: "disco-bops" 
* javaVersion: "1.8" 
* groupId: "com.alibaba.cbuscm" 
* artifactId: "discobops" 
* style: "springmvc" 
* operator: "119083" 

### 上下文参数
* appName: disco-bops
* operator: 119083
* gitUrl: git@gitlab.alibaba-inc.com:cbuscm/disco-bops.git
* branch: master


### 命令行
	sudo docker run --rm -v `pwd`:/workspace -e repoUrl="git@gitlab.alibaba-inc.com:cbuscm/disco-bops.git" -e appName="disco-bops" -e javaVersion="1.8" -e groupId="com.alibaba.cbuscm" -e artifactId="discobops" -e style="springmvc" -e operator="119083"  reg.docker.alibaba-inc.com/bootstrap/image:0.1

