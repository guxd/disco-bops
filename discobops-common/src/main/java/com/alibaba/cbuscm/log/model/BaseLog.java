package com.alibaba.cbuscm.log.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/10/8
 */
@Data
public class BaseLog implements Serializable,Cloneable {

    private String empId;

}
