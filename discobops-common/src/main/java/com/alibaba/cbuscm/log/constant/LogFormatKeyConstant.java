package com.alibaba.cbuscm.log.constant;

/**
 *
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午6:14
 */
public class LogFormatKeyConstant {

    public static final String BIZKEY = "bizKey";
    public static final String LOGIN_ID = "loginId";
    public static final String FLOWID = "flowId";
    public static final String EMP_ID = "empId";
    public static final String NICK_NAME = "nickNameCn";
}
