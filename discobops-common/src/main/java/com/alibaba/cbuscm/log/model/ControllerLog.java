package com.alibaba.cbuscm.log.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 服务日志信息
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/9/13 上午11:33
 */
@Getter
@Setter
public class ControllerLog extends BaseControllerLog {

    public ControllerLog(){
        setSuccess(true);
    }
    /**
     * 结果
     */
    private String result;

}
