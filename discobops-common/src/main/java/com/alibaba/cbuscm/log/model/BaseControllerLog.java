package com.alibaba.cbuscm.log.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/9/13 上午11:34
 */
@Getter
@Setter
public class BaseControllerLog extends BaseLog {

    /**
     * 是否成功
     */
    private Boolean success;
    /**
     * 日志类型
     */
    private String logType;
    /**
     * 业务key
     */
    private String bizKey;
    /**
     * 消耗时间
     */
    private long cost;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 鹰眼id
     */
    private String traceId;
    /**
     * 流程id
     */
    private String flowId;
    /**
     * 是否压测
     */
    private Boolean yace;
    /**
     * 参数
     */
    private String args;
    /**
     * 类名
     */
    private String className;
    /**
     * 方法名
     */
    private String methodName;


}
