package com.alibaba.cbuscm.log.constant;

/**
 * 日志类型常量类
 *
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:05
 */
public interface LogConstant {

    enum LogType{
        //
        SAL("sal","接口访问层"),
        SERVICE("service","服务层"),
        REST("rest","REST服务层"),
        INNER("inner","内部日志"),
        MSG("msg","消息层"),
        JOB("job","job入口"),
        ;

        private String code;
        private String desc;

        LogType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }

    enum ErrorType{
        //
        BIZ("biz","业务异常"),
        SYSTEM("sys","系统异常"),
        INNER("inner","系统异常");

        private String code;
        private String desc;

        ErrorType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum ErrorPriority {
        /**
         *
         */
        P0("0", "P0"),
        P1("1", "P1"),
        P2("2", "P2"),
        P3("3", "P3"),
        P4("4", "P4"),
        P5("5", "P5"),
        DEFAULT_P("6", "P6");

        private String code;
        private String desc;

        private ErrorPriority(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getDesc() {
            return this.desc;
        }

        public String getCode() {
            return this.code;
        }

    }

    enum DomainEnum{
        /**
         * CHANNEL_DISTRIBUTION 渠道分销
         * CHANNEL_PRICE 渠道价格
         * CHANNEL_INVENTORY 渠道库存
         */
        COMMON,CHANNEL_DISTRIBUTION,CHANNEL_PRICE,CHANNEL_INVENTORY,
    }
}
