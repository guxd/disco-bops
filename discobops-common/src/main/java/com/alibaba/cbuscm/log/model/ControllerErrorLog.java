package com.alibaba.cbuscm.log.model;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO：类说明
 *
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/9/13 上午11:41
 */
@Getter
@Setter
public class ControllerErrorLog extends BaseControllerLog {
    public ControllerErrorLog(){
        setSuccess(false);
    }

    /**
     * 错误类型
     */
    private String errorType;

    /**
     * 错误码
     */
    private String errorCode;
    /**
     * 错误信息
     */
    private String errorMsg;
    /**
     * 异常堆栈
     */
    private String exceptionStack;

}
