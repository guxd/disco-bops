package com.alibaba.cbuscm.constants;


import com.alibaba.cbuscm.log.constant.LogConstant;

/**
 * 为了方便排查问题，一个错误码尽可能只在一个地方出现
 * 错误代码code格式为：[biz][bizDomain][priority][value]
 * 内部value定义号段：
 * bizModule = 0， 公共模块。【默认值】
 * bizModule = 10， 渠道价格
 * bizModule = 20， 渠道商品
 * bizModule = 30， 渠道库存
 * bizModule = 40， 渠道店铺
 * bizModule = 50， 渠道营销
 *
 *
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:15
 */
public enum BopsErrorCodeEnum {

    /**
     * 公共错误码
     */
    SYS_ERROR(1,"系统错误"),
    PARAM_NULL(2,"参数为空"),
    PARAM_ILLEGAL(3,"参数错误"),
    INTERFACE_SPH_BLOCK(4,"限流异常"),
    CHANNEL_MARKET_NOT_EXIST(5,"渠道市场不存在"),
    CHANNEL_SUB_MARKET_NOT_EXIST(6,"渠道子市场不存在"),
    ID_GENERATE_ERROR(7,"数据库表的ID生成失败"),
    INIT_METAQ_PRODUCER_EXCEPTION(8,"初始化metaq消费者异常"),
    OFC_FULFIL_COMPRESS_FULFIL_CONTEXT_EXCEPTION(9,"解压缩异常"),






    /************************************* common **********************************************************/
    LIST_ITEM_BY_IDS_FAILED(1,"通过ids批量查询IC异常"),
    LIST_ITEM_BY_IDS_RESULT_NULL(2,"通过ids批量查询IC异常"),
    LIST_ITEM_BY_IDS_EXCEPTION(3,"通过ids批量查询IC异常"),
    CONVERT_MEMBERID_2_USERID_EXCEPTION(4,"memberId转换成userId异常"),
    QUERY_PTS_DEPOSIT_USER_EXCEPTION(5,"查询查证金用户异常"),
    QUERY_MEMBER_MODEL_BY_USER_ID_EXCEPTION(6,"通过userId查询member模型异常"),
    QUERY_MEMBER_MODEL_BY_USER_IDS_EXCEPTION(7,"通过userIds查询members模型异常"),

    QUERY_ALL_SKU_BY_OFFER_ID_EXCEPTION(5000,"通过offerId查询所有sku结果异常"),
    LIST_ALL_SKU_BY_OFFER_IDS_EXCEPTION(5001,"通过offerIds查询所有sku结果异常"),
    LIST_SUPPLIER_WAREHOUSE_RELATION_RESULT_NULL(5002,"通过memberIds查询供应商仓库关系结果为空"),
    LIST_SUPPLIER_WAREHOUSE_RELATION_FAILED(5003,"通过memberIds查询供应商仓库关系结果失败"),
    LIST_SUPPLIER_WAREHOUSE_RELATION_EXCEPTION(5004,"通过memberIds查询供应商仓库关系结果异常"),
    QUERY_SUPPLIER_WAREHOUSE_RELATION_RESULT_NULL(5005,"通过memberId查询供应商仓库关系结果为空"),
    QUERY_SUPPLIER_WAREHOUSE_RELATION_FAILED(5006,"通过memberId查询供应商仓库关系结果失败"),
    QUERY_SUPPLIER_WAREHOUSE_RELATION_EXCEPTION(5007,"通过memberId查询供应商仓库关系结果异常"),
    QUERY_OFFER_BY_OFFER_ID_EXCEPTION(5008,"通过offerId查询offer信息结果异常"),
    LIST_OFFER_BY_OFFER_ID_EXCEPTION(5009,"通过offerIds查询offer信息结果异常"),
    QUERY_URC_RESOUCRCE_BY_U_CODE_RESULT_NULL(5010,"通过uCode查询urc资源结果为空"),
    QUERY_URC_RESOUCRCE_BY_U_CODE_FAILED(5011,"通过uCode查询urc资源结果失败"),
    QUERY_URC_RESOUCRCE_BY_U_CODE_EXCEPTION(5012,"通过uCode查询urc资源结果异常"),
    QUERY_URC_RESOUCRCE_BY_U_CODE_EXCEPTION_WITH_NULL_ADDRESS(5015,"通过uCode查询urc资源结果异常,地址和联系人为空"),
    CURRENCY_CONVERT_RATE_IS_NULL(5013,"币种转换比例为空"),
    QUERY_OFFER_PACKAGE_BY_OFFER_ID_EXCEPTION(5014,"通过offerId查询图片包结果异常"),
    SEND_METAQ_RESULT_NULL(5015,"发送metaq消息结果为空"),
    SEND_METAQ_RESULT_FAILED(5016,"发送metaq消息结果失败"),
    SEND_METAQ_RESULT_EXCEPTION(5017,"发送metaq消息结果异常"),
    CHANNEL_BIZ_INFO_CONFIG_NOT_FOUND(5018,"渠道业务信息配置没有找到"),


    /************************************* 渠道铺货 **********************************************************/
    QUERY_PRODUCT_SKU_RESULT_NULL(10001,"查询productSku结果为空"),
    QUERY_PRODUCT_SKU_FAILED(10001,"查询productSku结果失败"),
    QUERY_PRODUCT_SKU_EXCEPTION(10002,"查询productSku结果异常"),
    QUERY_PRODUCT_SKU_SUPPLY_REL_EXCEPTION(10002,"查询productSku供应关系异常"),
    PRODUCT_NOT_EXIST(10003,"产品不存在"),
    PRODUCT_SKU_NOT_EXIST_OR_STATUS_INVALID(10004,"产品SKU不存在或者状态不合法"),
    PRODUCT_SKU_VALID_SUPPLY_RELATION_NOT_EXIST(10005,"产品SKU无有效品供关系存在"),
    PRODUCT_VALID_SKU_IS_EMPTY(10006,"产品对应的有效的SKU列表为空"),
    PRODUCT_SKU_FORBIDDEN_RETAIL_SWITCH_TO_PROXY(10007,"产品SKU禁止从经销转换到代销"),
    UNKNOWN_BUSINESS_MODE_4_SUPPLY_RELATION(10008,"无法识别供应关系上的经营模式"),
    CHANNEL_SHOP_NOT_EXIST(10009,"渠道店铺不存在"),
    CHANNEL_SHOP_BUSINESS_MODES_IS_EMPTY(10010,"渠道店铺经营模式为空"),
    CHANNEL_SHOP_BUSINESS_MODES_NOT_SUPPORT(10011,"渠道店铺经营模式配置不支持"),
    MULTI_IN_PROCESS_SKU_CHANNEL_REL_EXIST(10012,"同一个sku多条处理中的记录"),
    MAPPING_PRODUCT_STATUS_4_GSP_FAILED(10013,"映射gsp产品状态失败"),
    GET_OFFER_PACKAGE_ORIGIN_FAILED(10014,"获取offer数据包失败"),
    DISTRIBUTION_STD_CHANNEL_PRODUCT_MODEL_NULL(10015,"标准渠道铺货模型为空"),
    DISTRIBUTION_GSP_SHOP_LIST_EMPTY(10015,"铺货到gsp的时候店铺列表为空"),
    MAPPING_BUSINESS_MODE_4_GSP_FAILED(10013,"映射gsp经营模式失败"),
    CN_SKU_ID_NOT_EXIST_4ACTIVE_PROXY_SWITCH_RETAIL(10014,"代销转经销时候后端商品id不存在"),
    CN_SKU_ID_NOT_EXIST_4INIT_RETAIL_ADD_PROXY(10015,"经销增加代销时候后端商品id不存在"),
    LIST_SUPPLY_REL_BY_OFFER_ID_RESULT_NULL(10016,"通过offerId查询供应关系列表结果为空"),
    LIST_SUPPLY_REL_BY_OFFER_ID_RESULT_FAILED(10017,"通过offerId查询供应关系列表结果失败"),
    LIST_SUPPLY_REL_BY_OFFER_ID_RESULT_EXCEPTION(10018,"通过offerId查询供应关系列表结果异常"),
    LIST_SUPPLY_REL_BY_OFFER_SKU_ID_RESULT_NULL(10016,"通过offerSkuId查询供应关系列表结果为空"),
    LIST_SUPPLY_REL_BY_OFFER_SKU_ID_RESULT_FAILED(10017,"通过offerSkuId查询供应关系列表结果失败"),
    LIST_SUPPLY_REL_BY_OFFER_SKU_ID_RESULT_EXCEPTION(10018,"通过offerSkuId查询供应关系列表结果异常"),
    PRODUCT_STATUS_ERROR_NO_PUBLISH(10016,"产品状态非publish状态"),
    PRODUCT_SKU_SUPPLY_REL_NO_MEMBERID(10017,"产品SKU供应关系无memberId"),
    PRODUCT_OWNER_ID_IS_NULL(10018,"产品上的ownerId为空"),
    QUERY_PRODUCT_SKU_STATUS_PARAM_NULL(10019,"产品sku状态同步参数异常"),
    QUERY_PRODUCT_SKU_BY_CNSKU_NOT_EXIST(10020,"通过后端货品查询productSku不存在"),
    PRODUCT_SKU_MAIN_SUPPLY_REL_IS_NULL(10021,"产品sku对应的主供应关系为空"),
    PRODUCT_VALID_SUPPLY_RELATION_NOT_EXIST(10022,"产品无有效品供关系存在"),
    CHANNEL_REL_NOT_FOUND_UPDATE_PROCESS(10013,"update场景下没有找到对应的渠道关系"),
    MODIFY_CHANNEL_REL_STATUS_UPDATE_ROW_FAILED(10014,"更新渠道关系状态时更新列异常"),
    DISTRIBUTION_SYNC_PRODUCT_GSP_SKU_LIST_EMPTY(10015,"产品初始化铺货到GSP,sku列表为空"),
    DISTRIBUTION_SYNC_PRODUCT_GSP_SKU_SUCCESS_LIST_EMPTY(10016,"产品初始化铺货到GSP,sku成功列表为空"),
    ASYNC_DEAL_INV_CHANNEL_REL_NOT_FOUND(10017,"异步处理库存逻辑时候渠道分销关系没有找到"),
    ASYNC_DEAL_INV_INIT_PARAM_LIST_EMPTY(10018,"异步处理库存原始参数为空"),
    BUILD_DISTRIBUTE_RESULT_DB_EXIST_REL_LIST_EMPTY(10019,"构建铺货见过的时候db已经存在的铺货关系列表为空"),
    BUILD_DISTRIBUTE_RESULT_DB_EXIST_VALID_REL_LIST_EMPTY(10020,"构建铺货见过的时候db已经存在的有效铺货关系列表为空"),



    DISTRIBUTION_SYNC_PRODUCT_GSP_SERVCIE_EXCEPTION(1001,"产品初始化铺货到GSP异常"),
    DISTRIBUTION_SYNC_PRODUCT_GSP_FAIL(1002,"产品初始化铺货到GSP失败"),
    DISTRIBUTION_SYNC_PRODUCT_GSP_SKU_SIZE_EXCEPTION(1003,"产品初始化铺货到GSP,sku同步的个数异常"),
    DISTRIBUTION_SYNC_PRODUCT_GSP_CATEGORY_EXCEPTION(1004,"产品初始化铺货到GSP,获取类目映射关系异常"),
    DISTRIBUTION_REL_INSERT_DB_FAIL(1005,"铺货记录插入失败"),
    DISTRIBUTION_REL_INSERT_DB_EXCEPTION(1006,"铺货记录插入失败异常"),
    DISTRIBUTION_SYNC_REL_PRICE_GSP_FAIL(1007, "同步GSP的供应关系及价格失败"),
    DISTRIBUTION_SYNC_REL_PRICE_GSP_EXCEPTION(1008, "同步GSP的供应关系及价格异常"),
    DIVISION_ADDRESS_QUERY_SERVICE_EXCEPTION(1009,"查询地址详细信息服务异常"),
    DIVISION_ADDRESS_QUERY_ERROR(1010,"查询地址详细信息为空"),
    DISTRIBUTION_ADD_PRODUCT_SKU_GSP_SERVCIE_EXCEPTION(1011,"增加sku铺货GSP异常"),
    DISTRIBUTION_ADD_PRODUCT_SKU_GSP_SERVCIE_FAIL(1012,"增加sku铺货GSP失败"),
    DISTRIBUTION_SYNC_PRODUCT_STATUS_GSP_SERVCIE_FAIL(1013,"同步GSP产品上下架状态失败"),
    DISTRIBUTION_SYNC_PRODUCT_STATUS_GSP_SERVCIE_EXCEPTION(1014,"同步GSP产品上下架状态异常"),

    /************************************* 渠道库存 **********************************************************/
    MODIFY_AIC_CHANNEL_RULE_RESULT_NULL(20001,"修改AIC渠道库存规则结果为空"),
    MODIFY_AIC_CHANNEL_RULE_FAILED(20002,"修改AIC渠道库存规则结果失败"),
    MODIFY_AIC_CHANNEL_RULE_EXCEPTION(20003,"修改AIC渠道库存规则结果异常"),
    ADJUST_INVENTORY_4_MERCHANT_STORE_RESULT_NULL(20004,"调整商家仓库存结果为空"),
    ADJUST_INVENTORY_4_MERCHANT_STORE_FAILED(20005,"调整商家仓库存结果失败"),
    ADJUST_INVENTORY_4_MERCHANT_STORE_EXCEPTION(20006,"调整商家仓库存结果异常"),
    REDUCE_INVENTORY_RESULT_NULL(20004,"扣减库存（无需占用）结果为空"),
    REDUCE_INVENTORY_FAILED(20005,"扣减库存（无需占用）结果失败"),
    REDUCE_INVENTORY_EXCEPTION(20006,"扣减库存（无需占用）结果异常"),
    SUPPLIER_WAREHOUSE_RELATION_NOT_EXIST(20006,"通过memberIds查询供应商仓库关系不存在"),
    OFFER_NOT_EXIST(20007,"offer不存在"),
    MER_STORE_INT_ADJUST_PARAM_LIST_EMPTY(20008,"库存调整参数列表为空"),
    OFFER_SKU_NOT_EXIST(20009,"offerSku不存在"),
    BATCH_INIT_WRS_FDC_REL_RESULT_NULL(20010,"批量初始化仓前关系结果为空"),
    BATCH_INIT_WRS_FDC_REL_FAILED(20011,"批量初始化仓前关系结果失败"),
    BATCH_INIT_WRS_FDC_REL_EXCEPTION(20012,"批量初始化仓前关系结果异常"),
    OIC_INVENTORY_CHANGE_MSG_LIST_EMPTY(20013,"oic库存变更消息内容列表为空"),
    OIC_INVENTORY_NOT_EXIST(20013,"交易子订单信息列表为空"),
    OIC_INVENTORY_ADJUST_MER_STORE_RESULT_EMPTY(20014,"调整商家仓库存结果为空"),
    ADJUST_CHANNEL_INVENTORY_RESULT_NULL(20015,"调整渠道库存结果为空"),
    ADJUST_CHANNEL_INVENTORY_FAILED(20016,"调整渠道库存结果失败"),
    ADJUST_CHANNEL_INVENTORY_EXCEPTION(20017,"调整渠道库存结果异常"),
    QUERY_CHANNEL_INVENTORY_RESULT_NULL(20018,"查询渠道库存结果为空"),
    QUERY_CHANNEL_INVENTORY_FAILED(20018,"查询渠道库存结果失败"),
    QUERY_CHANNEL_INVENTORY_EXCEPTION(20019,"查询渠道库存结果异常"),
    OFFER_INVENTORY_CHANGE_ITEM_LIST_EMPTY(20020,"offer编辑库存变更明细列表为空"),
    P0_OIC_CHANNEL_RULE_NOT_EXIST(20021,"渠道规则不存在", LogConstant.ErrorPriority.P0),
    P0_OIC_CHANNEL_RULE_RETURN_MULTI(20022,"渠道规则存在多条", LogConstant.ErrorPriority.P0),
    QUERY_AIC_CHANNEL_RULE_RESULT_NULL(20023,"查询AIC渠道库存规则结果为空"),
    QUERY_AIC_CHANNEL_RULE_FAILED(20024,"查询AIC渠道库存规则结果失败"),
    QUERY_AIC_CHANNEL_RULE_EXCEPTION(20025,"查询AIC渠道库存规则结果异常"),
    INIT_WRS_FDC_REL_RESULT_NULL(20026,"初始化仓前关系结果为空"),
    INIT_WRS_FDC_REL_FAILED(20027,"初始化仓前关系结果失败"),
    INIT_WRS_FDC_REL_EXCEPTION(20028,"初始化仓前关系结果异常"),
    QUERY_WRS_FDC_REL_RESULT_NULL(20029,"查询仓前关系结果为空"),
    QUERY_WRS_FDC_REL_FAILED(20030,"查询仓前关系结果失败"),
    QUERY_WRS_FDC_REL_EXCEPTION(20031,"查询仓前关系结果异常"),
    WRS_FDC_REL_EXIST_WITH_OTHER_RDC(20032,"区前关系已经绑定其他的rdc了"),
    WRS_FDC_REL_EXIST_MULTI(20033,"区前关系存在多条"),
    UPDATE_WRS_FDC_REL_RESULT_NULL(20034,"更新区前关系结果为空"),
    UPDATE_WRS_FDC_REL_FAILED(20035,"更新区前关系结果为空"),
    UPDATE_WRS_FDC_REL_EXCEPTION(20036,"更新区前关系结果异常"),
    ONLY_SUPPORT_DELETE_WRS_FDC_REL(20037,"只支持删除区前关系"),



    /************************************* 渠道价格 **********************************************************/
    NO_PRICE_RECORD_WHIT_GIVEN_PRODUCT_SKU_ID(30001,"指定渠道商品Sku没有价格记录"),
    NO_PRICE_RECORD_FEE_DETAILS_WHIT_GIVEN_RECORD_ID(30002,"指定渠道商品Sku价格记录对应费用明细记录为空"),
    FAILED_TO_UPDATE_DB_WITH_PRICE_RECORDS(30003,"插入渠道商品价格记录和费用明细更新DB失败"),
    POST_FEE_NOT_EXIST(30004,"运费因子不存在"),

    CALCULATE_DELIVERY_COST_RESULT_NULL(30015,"计算运费结果为空"),
    CALCULATE_DELIVERY_COST_FAILED(30016,"计算运费结果失败"),
    CALCULATE_DELIVERY_COST_EXCEPTION(30017,"计算运费结果异常"),

    CURRENCY_NOT_SUPPORT_ERROR(30018,"目前只支持美元到人民币汇率"),
    UNSUPPORT_SALESPLATFORM_ERROR(30019,"未知的销售平台"),
    NOT_FIND_CURRENCY_SUPPLY_PRICE(30020,"未找到当前平台的供货价"),
    NO_PRICING_RULE_FOUND(30021,"没有找到定价规则"),
    EXCHANGE_RATE_DATE_NOT_NULL(30022,"汇率日期不能为空"),
    EXCHANGE_RATE_NOT_FOUND(30023,"没有找到需要的汇率"),

    /************************************* 渠道店铺 **********************************************************/
    MODIFY_CHANNEL_SHOP_OWNER_UPDATE_DB_FAILED(40001,"修改渠道店铺运营人员更新DB失败"),
    CONFIRM_CHANNEL_SHOP_CONFIG_UPDATE_DB_FAILED(40002,"确认渠道店铺配置更新DB失败"),
    CREATE_CHANNEL_SHOP_CONFIG_UPDATE_DB_FAILED(40003,"新建渠道店铺配置更新DB失败"),
    CHANNEL_SHOP_READ_FAILED(40004,"渠道店铺列表查询失败"),

    /************************************* 渠道营销 **********************************************************/

    QUERY_ENROLLED_MARKETING_PRODUCT_MEMBER_ID_NULL(50, 1,"查询已经提报的活动商品供应商id为空"),
    QUERY_ENROLLED_MARKETING_PRODUCT_SELLER_ID_NULL(50, 2,"查询已经提报的活动商品店铺id为空"),
    SUPPLIER_SETTLED_DAYS_PARAM_LIST_INVALID(50, 3,"供应商入驻市场资质参数列表不合法"),
    LIST_OFFER_ENROLLED_RESULT_NULL(50, 4,"查询所有提报过的offer列表结果为空"),
    LIST_OFFER_ENROLLED_FAILED(50, 5,"查询所有提报过的offer列表失败"),
    LIST_OFFER_ENROLLED_EXCEPTION(50, 5,"查询所有提报过的offer列表异常"),
    QUERY_OFFER_SKU_ENROLLED_DETAIL_RESULT_NULL(50, 6,"查询所有提报过的offerSku列表详情结果为空"),
    QUERY_OFFER_SKU_ENROLLED_DETAIL_FAILED(50, 7,"查询所有提报过的offerSku列表详情失败"),
    QUERY_OFFER_SKU_ENROLLED_DETAIL_EXCEPTION(50, 8,"查询所有提报过的offerSku列表详情异常"),
    MARKETING_PLAN_NOT_FOUND_BY_ID(50, 9,"通过id没有找到对应的营销活动"),
    UPDATE_MARKETING_PLAN_STATUS_ROW_FAILED(50, 10,"更新营销计划状态更新行数失败"),
    FINISH_MARKETING_PLAN_WITH_INVALID_PRODUCT_STATUS(50, 11,"营销计划结束的时候，活动商品状态不合法"),
    MARKETING_PLAN_STATE_MACHINE_HANDLER_NOT_FOUND(50, 12,"营销计划结束状态机handler没有找到"),
    MARKETING_PLAN_STATE_MACHINE_STATUS_INVALID(50, 13,"营销计划结束状态机状态不合法"),
    MARKETING_PLAN_STATE_MACHINE_PLAN_NULL(50, 14,"营销计划结束状态机计划为空"),
    MARKETING_PLAN_STATE_MACHINE_CONTEXT_NULL(50, 14,"营销计划结束状态机上下文为空"),
    SAVE_MARKETING_COMMISSION_RATE_FAILED(50, 15,"保存营销佣金比例失败"),
    SAVE_MARKETING_COMMISSION_RATE_EXCEPTION(50, 15,"保存营销佣金比例失败"),
    QUERY_MARKETING_COMMISSION_RATE_FAILED(50, 16,"查询营销佣金比例失败"),
    QUERY_MARKETING_COMMISSION_RATE_EXCEPTION(50, 17,"查询营销佣金比例失败"),
    QUERY_FIRST_ENABLE_RATE_FAILED(50, 18,"查询营销佣金比例失败"),
    MARKETING_PRODUCT_NOT_FOUND_BY_ID(50, 19,"营销商品不存在"),
    QC_CODE_NOT_FOUND_IN_TEMPLATE(50, 20,"资质code在模板里面没有找到"),
    QUERY_WHITE_SUPPLIER_NULL(50, 21,"查询商户白名单为空"),
    WHITE_ITEM_LIST_ADD(50,22,"商品白名单添加错误"),
    WHITE_SUPPLIER_LIST_ADD(50,23,"供应商白名单添加错误"),
    SAVE_MARKETING_PRODUCT(50,24,"保存活动商品失败"),
    QUERY_MARKETING_PRODUCT_ID_NULL(50,25,"查询活动商品为空"),
    UPDATE_MARKETING_PRODUCT_FAILED(50,26,"更新活动商品失败"),
    CREATE_MATERIAL_FAILED(50,27,"创建素材失败"),
    UPDATE_MATERIAL_FAILED(50,28,"更新素材失败"),
    CREATE_WHITE_ITEM_FAILED(50, 29,"创建品的白名单失败"),
    UPDATE_WHITE_ITEM_FAILED(50,30,"更新品的白名单失败"),
    CREATE_WHITE_SUPPLIER_FAILED(50, 31,"创建供应商的白名单失败"),
    UPDATE_WHITE_SUPPLIER_FAILED(50,32,"更新供应商的白名单失败"),
    PRODUCT_MATERIAL_PARAM_REQUIRED_EXCEPTION(50,33,"活动商品上报的时候需要输入素材必填项"),
    ONE_ITEMID_ONE_MARKETING_PLAN_EXCEPTION(50,34,"同一个品，不能多次报名同一个活动"),
    ONE_ITEMID_MANY_MARKETING_PLAN_CHARGE_TYPE_EXCEPTION(50,35,"同一个品，不能同时报多个收费方式一样的活动"),
    UN_VALIABLE_MARKETING_PRODUCT_STATUS(50,36,"无效状态"),
    AUDIT_OPTIMISTIC_LOCK_FAIL(50,37,"活动商品审核失败"),
    PRODUCT_ENROLL_EXCEPTION(50,38,"活动提保失败"),
    MARKETING_PLAN_IS_DELETED(50,39,"活动已经被删除"),
    WHITE_ITEM_LIST_UPDATE(50,40,"修改商品白名单"),
    WHITE_ITEM_LIST_DELETE(50,41,"删除商品白名单"),
    SAVE_MATERIAL_EXCEPTION(50,42,"素材保存失败"),
    SAVE_BIZ_OPERATELOG_EXCEPTION(50,43,"保存业务日志失败"),
    PRODUCT_STATUS_CLASH(50,44,"活动商品状态冲突"),
    PRODUCT_MATERIAL_EXCEPTION(50,45,"报的商家活动有素材要求，请补充"),
    SAVE_MATERIAL_BIND_EXCEPTION(50,46,"素材数量无法匹配"),
    QC_TEMPLATE_INIT_EXCEPTRION(50,47,"资质信息加载失败"),
    CONFIG_PAYMENT_EXCEPTION(50,48,"配置金额失败"),
    PAYMENT_CONFIRM_BY_OWNER_EXCEPTION(50,49,"审核人员确认收款失败"),
    AUDIT_REJECT_EXCEPTION(50,51,"审核拒绝失败"),
    CONFIG_SALES_EXCEPTION(50,52,"配置销售信息失败"),
    QUERY_STATUS_STATISCIS_EXCEPTION(50,53,"状态统计失败"),
    QUERY_MEMBER_BY_LOGIN_ID_EXCEPTION(50,54,"根据login查询会员信息失败"),
    QUERY_PRODUCT_PAGE_EXCEPTION(50,55,"分页查询活动商品失败"),
    QUERY_PAYMENT_INFO_EXCEPTION(51,56,"查询付款信息失败"),
    QUERY_MARKETING_PLAN_PAGE_EXCEPTION(50,57,"分页查询活动失败"),
    FILTRER_LOGIN_PRIVILEGE_EXCEPTION(50,58,"没有权限"),
    ;

    /**
     * biz = 1， 业务系统
     */
    private final int biz = 1;
    /**
     * bizDomain = 1， 渠道域
     */
    private final int bizDomain = 1;

    /**
     * bizModule = 0， 公共模块
     * bizModule = 10， 渠道价格
     * bizModule = 20， 渠道商品
     * bizModule = 30， 渠道库存
     * bizModule = 40， 渠道店铺
     * bizModule = 50， 渠道营销
     */
    private int bizModule = 0;

    private LogConstant.ErrorPriority priority;

    private Integer value;

    private String desc;

    BopsErrorCodeEnum(Integer bizModule, Integer value, String desc, LogConstant.ErrorPriority priority) {
        this.bizModule = bizModule;
        this.value = value;
        this.desc = desc;
        this.priority = priority;
    }

    BopsErrorCodeEnum(Integer value, String desc, LogConstant.ErrorPriority priority) {
        this.value = value;
        this.desc = desc;
        this.priority = priority;
    }

    BopsErrorCodeEnum(Integer bizModule, Integer value, String desc) {
        this.bizModule = bizModule;
        this.value = value;
        this.desc = desc;
        this.priority = LogConstant.ErrorPriority.P4;
    }

    BopsErrorCodeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
        this.priority = LogConstant.ErrorPriority.P4;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public LogConstant.ErrorPriority getPriority() {
        return priority;
    }

    public void setPriority(LogConstant.ErrorPriority priority) {
        this.priority = priority;
    }

    public String getErrorMsg() {
        return name() + "#" + desc;
    }

    public String getErrorCode() {
        StringBuilder errorCode = new StringBuilder();
        errorCode.append(String.format("%02d", Integer.valueOf(biz)));
        errorCode.append(String.format("%02d", Integer.valueOf(bizDomain)));
        errorCode.append(String.format("%04d", Integer.valueOf(value)));
        return errorCode.toString();
    }
}
