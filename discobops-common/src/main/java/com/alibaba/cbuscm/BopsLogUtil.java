package com.alibaba.cbuscm;

import com.alibaba.cbuscm.constants.BopsErrorCodeEnum;
import com.alibaba.cbuscm.exceptions.BopsBizRuntimeException;
import com.alibaba.cbuscm.exceptions.BopsRuntimeException;
import com.alibaba.cbuscm.log.constant.LogConstant;
import com.alibaba.cbuscm.log.constant.LogFormatKeyConstant;
import com.alibaba.cbuscm.log.model.ControllerErrorLog;
import com.alibaba.cbuscm.log.model.ControllerLog;
import com.alibaba.cbuscm.log.model.ServiceErrorLog;
import com.alibaba.cbuscm.log.model.ServiceLog;
import com.alibaba.fastjson.JSON;
import com.taobao.eagleeye.EagleEye;
import org.aspectj.lang.Signature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

;

/**
 * 日志工具
 *
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:54
 */
public class BopsLogUtil {

    private final static Logger controllerErrorLogger=LoggerFactory.getLogger("controller-error");
    private final static Logger controllerLogger=LoggerFactory.getLogger("controller");
    private final static Logger serviceErrorLogger= LoggerFactory.getLogger("service-error");
    private final static Logger serviceLogger= LoggerFactory.getLogger("service");

    public static void logServiceSuccess(Signature signature, Object input, Object output, Long costTime, LogConstant.LogType logType){
        try {
            ServiceLog serviceLog = buildServiceLog(signature.getDeclaringType().getSimpleName(), signature.getName(),
                input, output, costTime, logType);
            serviceLogger.error(JSON.toJSONString(serviceLog));
        }catch (Throwable t){
            serviceErrorLogger.error("logServiceSuccess method exception", t);
        }
    }

    public static void logServiceSuccess(String className, String methodName, Object input,
                                         Object output, Long costTime, LogConstant.LogType logType){
        try {
            ServiceLog serviceLog = buildServiceLog(className, methodName, input, output, costTime, logType);
            serviceLogger.error(JSON.toJSONString(serviceLog));
        }catch (Throwable t){
            controllerErrorLogger.error("logServiceSuccess method exception", t);
        }
    }

    public static void logControllerSuccess(Signature signature, Object input, Object output, Long costTime, LogConstant.LogType logType){
        try {

            ControllerLog controllerLog = buildControllerLog(signature.getDeclaringType().getSimpleName(), signature.getName(),
                    input, output, costTime, logType);
            controllerLogger.error(JSON.toJSONString(controllerLog));
        }catch (Throwable t){
            controllerErrorLogger.error("logControllerSuccess method exception", t);
        }
    }


    public static void logControllerSuccess(String className, String methodName, Object input,
                                         Object output, Long costTime, LogConstant.LogType logType){
        try {
            ControllerLog serviceLog = buildControllerLog(className, methodName, input, output, costTime, logType);
            controllerLogger.error(JSON.toJSONString(serviceLog));
        }catch (Throwable t){
            controllerErrorLogger.error("logControllerSuccess method exception", t);
        }
    }


    public static void logControllerFailed(Signature signature, Object input, Long costTime, LogConstant.LogType logType, Throwable throwable){
        try{
            logControllerFailed(signature.getDeclaringType().getSimpleName(), signature.getName(),
                    input, costTime, logType, throwable);
        }catch (Throwable t){
            controllerErrorLogger.error("logControllerFailed method exception", t);
        }
    }



    public static void logControllerFailed(String className, String methodName, Object input, Long costTime, LogConstant.LogType logType, Throwable throwable){
        try{
            ControllerErrorLog controllerErrorLog = buildControllerErrorLog(className, methodName,
                    input, costTime, logType, throwable);
            controllerErrorLogger.error(JSON.toJSONString(controllerErrorLog));
        }catch (Throwable t){
            controllerErrorLogger.error("logControllerFailed method exception", t);
        }
    }

    public static void logServiceFailed(Signature signature, Object input, Long costTime, LogConstant.LogType logType, Throwable throwable){
        try{
            logServiceFailed(signature.getDeclaringType().getSimpleName(), signature.getName(),
                input, costTime, logType, throwable);
        }catch (Throwable t){
            serviceErrorLogger.error("logServiceFailed method exception", t);
        }
    }



    public static void logServiceFailed(String className, String methodName, Object input, Long costTime, LogConstant.LogType logType, Throwable throwable){
        try{

            ServiceErrorLog serviceErrorLog = buildServiceErrorLog(className, methodName,
                    input, costTime, logType, throwable);
            serviceErrorLogger.error(JSON.toJSONString(serviceErrorLog));
        }catch (Throwable t){
            serviceErrorLogger.error("logControllerFailed method exception", t);
        }
    }


    private static ControllerErrorLog buildControllerErrorLog(String className, String methodName, Object input,
                                                              Long costTime, LogConstant.LogType logType, Throwable t){
        ControllerErrorLog controllerErrorLog = new ControllerErrorLog();
        LogConstant.ErrorType errorType = LogConstant.ErrorType.SYSTEM;
        if(t instanceof BopsBizRuntimeException){
            errorType = LogConstant.ErrorType.BIZ;
        }
        BopsErrorCodeEnum errorCodeEnum = BopsErrorCodeEnum.SYS_ERROR;
        if(t instanceof BopsRuntimeException){
            errorCodeEnum = ((BopsRuntimeException)t).getErrorCodeConstant();
        }
        controllerErrorLog.setErrorType(errorType.getCode());
        controllerErrorLog.setErrorCode(errorCodeEnum.getErrorCode());
        controllerErrorLog.setErrorMsg(errorCodeEnum.getErrorMsg());
        controllerErrorLog.setExceptionStack(obtainThrowableStackStr(t));
        controllerErrorLog.setLogType(logType.getCode());
        controllerErrorLog.setBizKey(MDC.get(LogFormatKeyConstant.BIZKEY));
        controllerErrorLog.setCost(costTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        controllerErrorLog.setStartTime(sdf.format(new Date()));
        controllerErrorLog.setTraceId(EagleEye.getTraceId());
        controllerErrorLog.setFlowId(MDC.get(LogFormatKeyConstant.FLOWID));
        controllerErrorLog.setYace(EagleEyeUtil.isYace());
        try{
            controllerErrorLog.setEmpId(MDC.get(LogFormatKeyConstant.EMP_ID));
        }catch(Exception e){

        }
        try{
            controllerErrorLog.setArgs(JSON.toJSONString(input));
        }catch(Exception e){
            controllerErrorLog.setArgs("参数无法解析");
        }
        controllerErrorLog.setClassName(className);
        controllerErrorLog.setMethodName(methodName);
        return controllerErrorLog;
    }

    private static ServiceErrorLog buildServiceErrorLog(String className, String methodName, Object input,
                                                        Long costTime, LogConstant.LogType logType, Throwable t){
        ServiceErrorLog serviceErrorLog = new ServiceErrorLog();
        LogConstant.ErrorType errorType = LogConstant.ErrorType.SYSTEM;
        if(t instanceof BopsBizRuntimeException){
            errorType = LogConstant.ErrorType.BIZ;
        }
        BopsErrorCodeEnum errorCodeEnum = BopsErrorCodeEnum.SYS_ERROR;
        if(t instanceof BopsRuntimeException){
            errorCodeEnum = ((BopsRuntimeException)t).getErrorCodeConstant();
        }
        serviceErrorLog.setErrorType(errorType.getCode());
        serviceErrorLog.setErrorCode(errorCodeEnum.getErrorCode());
        serviceErrorLog.setErrorMsg(errorCodeEnum.getErrorMsg());
        serviceErrorLog.setExceptionStack(obtainThrowableStackStr(t));
        serviceErrorLog.setLogType(logType.getCode());
        serviceErrorLog.setBizKey(MDC.get(LogFormatKeyConstant.BIZKEY));
        serviceErrorLog.setCost(costTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        serviceErrorLog.setStartTime(sdf.format(new Date()));
        serviceErrorLog.setTraceId(EagleEye.getTraceId());
        serviceErrorLog.setFlowId(MDC.get(LogFormatKeyConstant.FLOWID));
        serviceErrorLog.setYace(EagleEyeUtil.isYace());
        serviceErrorLog.setArgs(JSON.toJSONString(input));
        serviceErrorLog.setClassName(className);
        serviceErrorLog.setMethodName(methodName);
        try{
            serviceErrorLog.setEmpId(MDC.get(LogFormatKeyConstant.EMP_ID));
        }catch(Exception e){

        }
        return serviceErrorLog;
    }


    private static ControllerLog buildControllerLog(String className, String methodName, Object input, Object output, Long costTime, LogConstant.LogType logType){
        ControllerLog controllerLog = new ControllerLog();
        controllerLog.setTraceId(EagleEye.getTraceId());
        controllerLog.setYace(EagleEyeUtil.isYace());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        controllerLog.setStartTime(sdf.format(new Date()));
        controllerLog.setLogType(logType.name());
        controllerLog.setBizKey(MDC.get(LogFormatKeyConstant.BIZKEY));
        controllerLog.setFlowId(MDC.get(LogFormatKeyConstant.FLOWID));
        controllerLog.setCost(costTime);
        try{
            controllerLog.setArgs(JSON.toJSONString(input));
        }catch(Exception e){
            controllerLog.setArgs("参数无法解析");
        }
        controllerLog.setClassName(className);
        controllerLog.setMethodName(methodName);
        controllerLog.setResult(JSON.toJSONString(output));
        try{
            controllerLog.setEmpId(MDC.get(LogFormatKeyConstant.EMP_ID));

        }catch (Exception e){

        }

        return controllerLog;
    }

    private static ServiceLog buildServiceLog(String className, String methodName, Object input, Object output, Long costTime, LogConstant.LogType logType){
        ServiceLog serviceLog = new ServiceLog();
        serviceLog.setTraceId(EagleEye.getTraceId());
        serviceLog.setYace(EagleEyeUtil.isYace());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        serviceLog.setStartTime(sdf.format(new Date()));
        serviceLog.setLogType(logType.name());
        serviceLog.setBizKey(MDC.get(LogFormatKeyConstant.BIZKEY));
        serviceLog.setFlowId(MDC.get(LogFormatKeyConstant.FLOWID));
        serviceLog.setCost(costTime);
        serviceLog.setArgs(JSON.toJSONString(input));
        serviceLog.setClassName(className);
        serviceLog.setMethodName(methodName);
        serviceLog.setResult(JSON.toJSONString(output));
        try{
            serviceLog.setEmpId(MDC.get(LogFormatKeyConstant.EMP_ID));

        }catch (Exception e){

        }

        return serviceLog;
    }

    private static String obtainThrowableStackStr(Throwable throwable) {
        if(throwable == null){
            return "";
        }
        Throwable actualCause = throwable;
        if((throwable instanceof BopsRuntimeException) && throwable.getCause() != null){
            actualCause = throwable.getCause();
        }
        StackTraceElement[] allStackTraceElement = actualCause.getStackTrace();
        List<String> allStackInfo = new ArrayList<>();
        allStackInfo.add(actualCause.getMessage());
        int stackDeep = 20;
        if(allStackTraceElement != null){
            stackDeep = Math.min(stackDeep, allStackTraceElement.length);
            for(int i = 0; i < stackDeep; ++i){
                StackTraceElement stackTraceElement = allStackTraceElement[i];
                allStackInfo.add(stackTraceElement.getClassName()+"."+stackTraceElement.getMethodName()+":"+ stackTraceElement.getLineNumber());
            }
        }
        return JSON.toJSONString(allStackInfo);
    }


    public static void initFlowId() {
        MDC.put(LogFormatKeyConstant.FLOWID, UUID.randomUUID().toString().replaceAll("-", ""));
    }


    public static void setEmpId(String empId) {
        MDC.put(LogFormatKeyConstant.EMP_ID, empId);
    }

    public static String getEmpId(){
        return MDC.get(LogFormatKeyConstant.EMP_ID);
    }

    public static void setNickNamecn(String nickNameCn) {
        MDC.put(LogFormatKeyConstant.NICK_NAME, nickNameCn);
    }

    public static String getNickNamecn(){
        return MDC.get(LogFormatKeyConstant.NICK_NAME);
    }


}
