package com.alibaba.cbuscm;

import com.alibaba.cbuscm.log.constant.LogConstant;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:05
 */
@Aspect
@Component
public class BopsLogInterceptor {

    private final static String ARG_ERROR = "参数解析异常";


    @Around("com.alibaba.cbuscm.BopsJoinPointConfig.controllerExecution()")
    public Object aroundcontroller(ProceedingJoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
/*        Object[] arguments = new Object[args.length-1];
        //过滤带有servlet的参数
        try {
            for (int i = 0; i < args.length-1; i++) {
                arguments[i] = args[i+1];
            }
        }catch(Throwable t){
            BopsLogUtil.logControllerFailed(joinPoint.getSignature(), ARG_ERROR, 0L, LogConstant.LogType.REST, t);
        }*/
        long start = System.currentTimeMillis();
        try {
            BopsLogUtil.initFlowId();
            Object result = joinPoint.proceed();
            long time = System.currentTimeMillis() - start;
            BopsLogUtil.logControllerSuccess(joinPoint.getSignature(), args, result, time, LogConstant.LogType.REST);
            return result;
        } catch (Throwable t) {
            long time = System.currentTimeMillis() - start;
            BopsLogUtil.logControllerFailed(joinPoint.getSignature(), args, time, LogConstant.LogType.REST, t);
            return RestResult.failResult(t);
        } finally {
            MDC.clear();
        }
    }

    @Around("com.alibaba.cbuscm.BopsJoinPointConfig.serviceExecution()")
    public Object aroundService(ProceedingJoinPoint joinPoint) throws Throwable{
        long start = System.currentTimeMillis();
        try {
            Object result = joinPoint.proceed();
            long time = System.currentTimeMillis() - start;
            //TODO 总开关+黑名单开关
            BopsLogUtil.logServiceSuccess(joinPoint.getSignature(), joinPoint.getArgs(), result, time, LogConstant.LogType.SERVICE);
            return result;
        } catch (Throwable t) {
            long time = System.currentTimeMillis() - start;
            BopsLogUtil.logServiceFailed(joinPoint.getSignature(), joinPoint.getArgs(), time, LogConstant.LogType.SERVICE, t);
            throw t;
        }
    }

}
