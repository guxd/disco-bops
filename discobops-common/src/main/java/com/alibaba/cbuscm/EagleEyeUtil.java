package com.alibaba.cbuscm;

import com.taobao.eagleeye.EagleEye;

/**
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午8:17
 */
public class EagleEyeUtil {
    
    /**
     * 是否是全链路压测
     *
     * @return true - 全链路；false - 不是全链路
     */
    public static boolean isYace() {
        try {
            return "1".equals(EagleEye.getUserData("t"));
        } catch (Throwable t) {
            //ignore
            t.printStackTrace();
            return false;
        }
    }
}
