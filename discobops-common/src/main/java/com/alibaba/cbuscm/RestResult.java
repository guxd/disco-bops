package com.alibaba.cbuscm;


import com.alibaba.cbuscm.constants.BopsErrorCodeEnum;
import com.alibaba.cbuscm.exceptions.BopsRuntimeException;
import lombok.Data;
/**
 *rest接口统一返回结果
 *结果处理类
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/23
 */

@Data
public class RestResult<T> {

    private Boolean success;

    private String errorCode;

    private String errorMsg;

    private T data;


    /**
     * 生成成功结果
     * @param data
     * @return
     */
    public static <T> RestResult<T> success( T data) {
        RestResult<T> result = new RestResult<>();
        result.setSuccess(true);
        result.setData(data);
        return result;
    }


    public static <T> RestResult<T> failResult(Throwable t) {
        RestResult<T> DcBaseResult = new RestResult<>();
        DcBaseResult.setSuccess(false);
        if (t instanceof BopsRuntimeException) {
            BopsErrorCodeEnum errorCodeEnum = ((BopsRuntimeException)t).getErrorCodeConstant();
            DcBaseResult.setErrorCode(errorCodeEnum.getErrorCode());
            DcBaseResult.setErrorMsg(errorCodeEnum.getDesc());
        } else{
            DcBaseResult.setErrorCode(BopsErrorCodeEnum.SYS_ERROR.getErrorCode());
            DcBaseResult.setErrorMsg(t.getMessage());
        }
        return DcBaseResult;
    }


    /**
     * 生成失败结果
     * @param exception
     * @return
     */
    public static <T> RestResult<T> fail(BopsRuntimeException exception) {
        RestResult<T> result = new RestResult<>();
        result.setSuccess(false);
        BopsErrorCodeEnum BopsErrorEnum = exception.getErrorCodeConstant();
        result.setErrorCode(BopsErrorEnum.getErrorCode());
        result.setErrorMsg(exception.getMessage());
        return result;
    }



    /**
     * 生成失败结果
     * @param errorCode
     * @param errorMsg
     * @return
     */
    public static <T> RestResult<T> fail(String errorCode,String errorMsg) {
        RestResult<T> result = new RestResult<>();
        result.setSuccess(false);
        result.setErrorCode(errorCode);
        result.setErrorMsg(errorMsg);
        return result;
    }

}