package com.alibaba.cbuscm.exceptions;

import com.alibaba.cbuscm.constants.BopsErrorCodeEnum;

import java.util.Arrays;

/**
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:14
 */
public class BopsRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 2901411991591466919L;

    /**
     * 标准错误码
     */
    protected BopsErrorCodeEnum errorCodeConstant;

    protected Object[] parameters;

    public BopsRuntimeException(BopsErrorCodeEnum errorCodeConstant) {
        super(errorCodeConstant.getDesc());
        this.errorCodeConstant=errorCodeConstant;
    }

    public BopsRuntimeException(BopsErrorCodeEnum errorCodeConstant, Throwable throwable) {
        super(throwable);
        this.errorCodeConstant=errorCodeConstant;
    }

    public BopsRuntimeException(BopsErrorCodeEnum errorCodeConstant, Throwable throwable, Object... parameters) {
        super(Arrays.deepToString(parameters), throwable);
        this.errorCodeConstant=errorCodeConstant;
        this.parameters = parameters;
    }

    public BopsRuntimeException(BopsErrorCodeEnum errorCodeConstant, Object... parameters) {
        super(errorCodeConstant.getDesc()+"#"+ Arrays.deepToString(parameters));
        this.errorCodeConstant = errorCodeConstant;
        this.parameters = parameters;
    }

    public BopsErrorCodeEnum getErrorCodeConstant() {
        return errorCodeConstant;
    }

    public void setErrorCodeConstant(BopsErrorCodeEnum errorCodeConstant) {
        this.errorCodeConstant = errorCodeConstant;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
