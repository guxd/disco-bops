package com.alibaba.cbuscm.exceptions;


import com.alibaba.cbuscm.constants.BopsErrorCodeEnum;

/**
 * 业务异常，不需要重试
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:14
 */
public class BopsBizRuntimeException extends BopsRuntimeException {
    private static final long serialVersionUID = 7648119053545826250L;


    public BopsBizRuntimeException(BopsErrorCodeEnum errorConstant) {
        super(errorConstant);
    }

    public BopsBizRuntimeException(BopsErrorCodeEnum errorConstant, Object... args) {
        super(errorConstant, args);
    }


    public BopsBizRuntimeException(BopsErrorCodeEnum errorCodeConstant, Throwable throwable) {
        super(errorCodeConstant, throwable);
    }

    public BopsBizRuntimeException(BopsErrorCodeEnum errorCodeConstant, Throwable throwable, Object... parameters) {
        super(errorCodeConstant, throwable, parameters);
    }
}
