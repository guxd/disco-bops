package com.alibaba.cbuscm.exceptions;

import com.alibaba.cbuscm.constants.BopsErrorCodeEnum;

/**
 * 系统异常，需要重试
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:14
 */
public class BopsSystemRuntimeException extends BopsRuntimeException {

    private static final long serialVersionUID = -5924581598532044087L;

    public BopsSystemRuntimeException(BopsErrorCodeEnum codeConstant) {
        super(codeConstant);
    }

    public BopsSystemRuntimeException(BopsErrorCodeEnum errorCodeConstant, Object... parameters) {
        super(errorCodeConstant, parameters);
    }

    public BopsSystemRuntimeException(BopsErrorCodeEnum errorCodeConstant, Throwable throwable) {
        super(errorCodeConstant, throwable);
    }

    public BopsSystemRuntimeException(BopsErrorCodeEnum errorCodeConstant, Throwable throwable, Object... parameters) {
        super(errorCodeConstant, throwable, parameters);
    }
}
