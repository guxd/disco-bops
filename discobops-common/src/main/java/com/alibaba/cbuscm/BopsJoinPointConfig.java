package com.alibaba.cbuscm;

import org.aspectj.lang.annotation.Pointcut;

/**
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/8/7 下午5:05
 */
public class BopsJoinPointConfig {


    /**
     * 服务切面
     */
    @Pointcut("execution(* com.alibaba.cbuscm.controller.marketing..*.*(..)) ")
    public void controllerExecution(){}


    /**
     * 服务切面
     */
    @Pointcut("execution(* com.alibaba.cbuscm.service.marketing..*.*(..)) ")
    public void serviceExecution(){}

}
