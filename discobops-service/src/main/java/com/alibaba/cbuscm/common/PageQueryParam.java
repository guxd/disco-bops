package com.alibaba.cbuscm.common;

import java.io.Serializable;

public class PageQueryParam implements Serializable {

    private static final long serialVersionUID = 3945398336603477493L;

    private static final int DEFAULT_PAGE_NO = 1;


    private static final int DEFAULT_PAGE_SIZE = 20;


    private static final int MAX_PAGE_SIZE = 1000;


    protected int pageNo = DEFAULT_PAGE_NO;

    protected int pageSize = DEFAULT_PAGE_SIZE;


    protected boolean enableReturnCount = true;


    protected String orderBy;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        if (pageNo < 1) {
            throw new IllegalArgumentException("pageNo must be less than 1");
        }
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if (pageSize > MAX_PAGE_SIZE || pageSize < 1) {
            throw new IllegalArgumentException("pageSize must be between 1 and " + MAX_PAGE_SIZE);
        }
        this.pageSize = pageSize;
    }

    public boolean isEnableReturnCount() {
        return enableReturnCount;
    }

    public void setEnableReturnCount(boolean enableReturnCount) {
        this.enableReturnCount = enableReturnCount;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

}
