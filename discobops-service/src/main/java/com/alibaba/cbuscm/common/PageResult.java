package com.alibaba.cbuscm.common;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;
/**
 * Created by duanyang.zdy on 2019/4/17.
 *
 */
@Data
@ToString
public class PageResult<T> implements Serializable {

    private boolean success;

    private T data;

    private String errorCode;

    private String errorMessage;

    private Integer pageNo;

    private Integer pageSize;

    private Integer total;


    public static <T> PageResult<T> done(T data) {
        PageResult<T> result = new PageResult<T>();
        result.setSuccess(true);
        result.setData(data);
        return result;
    }

    public static <T> PageResult<T> fail(String errorCode) {
        PageResult<T> result = new PageResult<>();
        result.setSuccess(false);
        result.setErrorCode(errorCode);
        return result;
    }

    public static <T> PageResult<T> fail(String errorCode, String errorMessage) {
        PageResult<T> result = new PageResult<>();
        result.setSuccess(false);
        result.setErrorCode(errorCode);
        result.setErrorMessage(errorMessage);
        return result;
    }

    public static <T> PageResult<T> succ(T data, Integer pageNo, Integer pageSize, Integer total) {
        PageResult<T> result = new PageResult<>();
        result.setSuccess(true);
        result.setData(data);
        result.setPageNo(pageNo);
        result.setPageSize(pageSize);
        result.setTotal(total);
        return result;
    }

}
