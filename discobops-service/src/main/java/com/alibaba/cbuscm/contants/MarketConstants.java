package com.alibaba.cbuscm.contants;

/**
 * Created by duanyang.zdy on 2019/9/3.
 */
public class MarketConstants {

    public static final Integer MARKET_ITEM_SQL_ID = 1007599;

    public static final Integer MARKET_ITEM_COUNT_SQL_ID = 1007606;

    public static final Integer MARKET_ITEM_PAGE_COUNT_SQL_ID = 1007686;

    public static final Integer MARKET_PRICE_SQL_ID = 1007607;

    public static final Integer Market_TRENDS_SQL_ID = 1007638;

    public static final Integer Market_TRENDS_COUNT_SQL_ID = 1007639;


    public static final Integer MarketViewQuery_SQL_ID = 1007513;

    public static final Integer MarketViewQuery_Count_SQL_ID = 1007602;

    public static final Integer MarketDetailuery_SQL_ID = 1007508;

    public static final Integer MarketViewQuery_Global_SQL_ID = 1007670;
    public static final Integer MarketViewQuery_Count_Global_SQL_ID = 1007671;


    public static final String MARKET_COMMU_TABLE_NAME = "global_market_oppotunity_commu_t";

    public static final String MARKET_TASK_TYPE = "0";

    public static final String SITE_TASK_TYPE = "1";

    public static final String AppName = "disco-bops";

    public static final String Pass = "S7YqTp7I";

    public static final String MARKET_MY_TASK_URL = "https://disco-bops.alibaba-inc.com/?_path_=channel/index/selectionTaskList#activeKey=my";


    public static final String MARKET_TASK_SEP = "/";
}
