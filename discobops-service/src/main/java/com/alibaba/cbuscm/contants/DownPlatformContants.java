package com.alibaba.cbuscm.contants;

/**
 * Created by duanyang.zdy on 2019/4/22.
 */
public class DownPlatformContants {

    public static final String DOWN_SITE_AE= "ae";

    public static final Integer PAGE_START = 0;

    public static final Integer PAGE_SIZE = 1000;

    public static final String SITE="site";

    public static final String MATCH_ITMEM_IDS = "match_3_item_ids";

    public static final String PRE_DETAIL_URL = "https://detail.1688.com/offer/";

    public static final String AFT_DETAIL_URL = ".html";

    public static final String JOIN = "$";
}
