package com.alibaba.cbuscm.contants;

import com.alibaba.china.offer.api.domain.general.Offer;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/10/11
 */
public class ProductImgContants {

    private final static String  IMAGE_SERVER_HTTPS = "https://cbu01.alicdn.com";

    private final static String  IMAGE_SERVER = "http://img.china.alibaba.com/";


    /**
     *  封装1688商品图片地址
     * @param offer
     * @return
     */
    public static String convertOfferImg(Offer offer) {
        if (null != offer.getProduct() && null != offer.getProduct().getImageList()
                && offer.getProduct().getImageList().size() > 0
                && null != offer.getProduct().getImageList().get(0)) {
            return ProductImgContants.IMAGE_SERVER + offer.getProduct().getImageList().get(0).getSize120x120ImageURI();
        } else {
            return "https://cbu01.alicdn.com/cms/upload/2015/961/852/2258169_228805014.png";
        }
    }
}
