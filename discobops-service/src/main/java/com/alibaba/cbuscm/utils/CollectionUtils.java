package com.alibaba.cbuscm.utils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * @author zhuoxian
 */
public class CollectionUtils {

    public static <T> Stream<T> streaming(Collection<T> source) {
        return source != null ? source.stream() : Stream.empty();
    }

    public static <T> List<T> filter(List<T> source, Predicate<T> predicate) {
        return streaming(source).filter(predicate).collect(Collectors.toList());
    }

    public static <S, T> List<T> map(List<S> list, Function<S, T> mapper) {
        return streaming(list).map(mapper).collect(toList());
    }

    public static <R, S, T> List<T> zip(List<R> a, List<S> b, BiFunction<R, S, T> mapper, Predicate<T> filter) {
        return IntStream.range(0, Math.min(a.size(), b.size()))
            .mapToObj(i -> mapper.apply(a.get(i), b.get(i)))
            .filter(filter)
            .collect(toList());
    }

    public static <S, K, V> Map<K, V> mapping(Collection<S> source, Function<S, K> keyProvider, Function<S, V> valueProvider) {
        return streaming(source).collect(toMap(keyProvider, valueProvider));
    }

    public static <K, V, T> Map<K, T> mapEach(Map<K, V> source, Function<V, T> valueMapping) {
        return source.entrySet().stream().collect(toMap(
            Entry::getKey,
            e -> valueMapping.apply(e.getValue())
        ));
    }

    public static <T> boolean any(Collection<T> source, Predicate<T> predicate) {
        return streaming(source).anyMatch(predicate);
    }

    public static <T> Set<T> toSet(Collection<T> source) {
        return streaming(source).collect(Collectors.toSet());
    }

    public static <T> boolean isEmpty(Collection<T> source) {
        return source == null || source.isEmpty();
    }

}
