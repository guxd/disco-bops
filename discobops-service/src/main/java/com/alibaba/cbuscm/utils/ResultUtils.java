package com.alibaba.cbuscm.utils;

import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;

import java.util.function.Function;

public class ResultUtils {

    public static <S, T> PageOf<T> map(PageOf<S> pageResult, Function<S, T> mapper) {
        if (pageResult == null) {
            return null;
        }
        return PageOf.isValid(pageResult)
            ? PageOf.general(CollectionUtils.map(pageResult.getData(), mapper),
                pageResult.getTotal(), pageResult.getPageNo(), pageResult.getPageSize())
            : PageOf.error(pageResult.getErrorCode(), pageResult.getErrorMessage());
    }
}
