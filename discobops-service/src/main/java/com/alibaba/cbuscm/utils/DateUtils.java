package com.alibaba.cbuscm.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DateUtils {

    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final DateTimeFormatter DATE_TIME_DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


    /**
     * Parse date time.
     * @param dateStr *
     * @return *
     */
    public static Date parseDateTime(String dateStr) {
        try {
            if (StringUtils.isBlank(dateStr)) {
                return null;
            }
            DateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
            return df.parse(dateStr);
        } catch (Throwable err) {
            return null;
        }
    }

    public static String formateDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return DATE_TIME_DTF.format(localDateTime);
    }

    public static Date getCurrentYearLastDay() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        return calendar.getTime();
    }
}
