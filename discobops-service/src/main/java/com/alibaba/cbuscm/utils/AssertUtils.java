package com.alibaba.cbuscm.utils;

/**
 * @author zhuoxian
 */
public class AssertUtils {

    public static void asserts(boolean value, String errorMessage) throws AssertionError {
        if (!value) {
            throw new AssertionError(errorMessage);
        }
    }
}
