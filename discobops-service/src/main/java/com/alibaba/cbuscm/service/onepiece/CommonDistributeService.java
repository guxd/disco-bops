package com.alibaba.cbuscm.service.onepiece;

import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum;
import com.alibaba.cbu.disco.channel.api.model.ChannelBaseResult;
import com.alibaba.cbu.disco.channel.api.model.ChannelShopDTO;
import com.alibaba.cbu.disco.channel.api.service.DiscoChannelShopService;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.OperationResultOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.product.api.DcDistributeManageService;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeEntryParam;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeOfferParam;
import com.alibaba.cbuscm.service.onepiece.model.PlatformShopModel;
import com.alibaba.cbuscm.service.onepiece.model.ShopChannelModel;
import com.alibaba.cbuscm.vo.onepiece.selection.SelectionItemStatusVO;
import com.alibaba.cbuscm.vo.onepiece.selection.StatusListVO;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.testng.collections.Lists;

import java.util.*;
import java.util.stream.Collectors;

import static com.alibaba.cbuscm.utils.CollectionUtils.streaming;
import static java.lang.Boolean.TRUE;

/**
 * onePiece选品中心铺货列表
 *
 * @author yvyuve.dyy
 * @date 2019/08/13
 */

@Component
@Slf4j
public class CommonDistributeService {
    private final static String BIZ_TYPE = "CROSSBORDER";
    @Autowired
    private DcDistributeManageService dcDistributeManageService;

    @Autowired
    private DiscoChannelShopService discoChannelShopService;

    /**
     * 获取渠道信息
     *
     * @param user
     * @return
     */
    public List<PlatformShopModel> getChannelShopList(BucSSOUser user) {
        ChannelBaseResult<List<ChannelShopDTO>> listChannelResult = discoChannelShopService.listDistributeChannelShops(
            user.getEmpId());
        log.info("渠道返回的数据：{}", JSON.toJSONString(listChannelResult));
        if (!listChannelResult.isSuccess() || listChannelResult.getData() == null
            || listChannelResult.getData().size() == 0) {
            return Lists.newArrayList();
        }
        return buildChannelShopList(listChannelResult.getData());
    }

    /**
     * 执行铺货接口
     * @param statusListVO
     * @param distChannels
     * @param user
     * @return
     */
    public ResultOf<? extends Object> distributionToChannel(StatusListVO statusListVO, List<String> distChannels,
                                                            BucSSOUser user) {
        if (CollectionUtils.isEmpty(statusListVO.getList())) {
            return ResultOf.error("empty entry", "entries cannot be null");
        }
        Long taskId = statusListVO.getList().get(0).getTaskId();
        try {
            List<DcDistributeEntryParam> distributeEntryList = new ArrayList<>(20);
            for (SelectionItemStatusVO entry : statusListVO.getList()) {
                if (entry == null) {
                    continue;
                }
                String itemId = null;
                String site = null;
                if (StringUtils.isNotBlank(entry.getDownItemId())) {
                    String[] arr = entry.getDownItemId().split("\\$");
                    site = arr[1];
                    itemId = arr[0];
                }
                distributeEntryList.add(new DcDistributeEntryParam(entry.getOfferId(), site, itemId));
            }
            if (CollectionUtils.isEmpty(distributeEntryList)) {
                return ResultOf.error("empty entry", "entries cannot be null");
            }

            // 进行多渠道铺货
            DcDistributeOfferParam distributeOfferParam = DcDistributeOfferParam.builder()
                .channels(distChannels)
                .bizType(BIZ_TYPE)
                .entries(distributeEntryList)
                .selectionTaskId(taskId)
                .operator(OperatorModel.builder()
                    .id(user.getEmpId())
                    .name(user.getLoginName())
                    .build())
                .build();


            log.info("开始铺货的数据：{}", JSON.toJSONString(distributeEntryList));
            // 时间戳
            long startTime = System.currentTimeMillis();
            ListOf<OperationResultOf<Long>> listOf = dcDistributeManageService.batchDistribute(distributeOfferParam);
            long endTime = System.currentTimeMillis();
            long time = endTime - startTime;
            log.info("执行时长为time:{}",time);
            log.info("返回的结果为：{}", JSON.toJSONString(listOf));
            log.info("验证结果为：{}",ListOf.isValid(listOf));

            if(listOf == null){
                return ResultOf.error("false", "批量铺货接口返回异常！");
            }

            if (listOf != null && !listOf.isSuccess()) {
                return ResultOf.error("false", listOf.getErrorMessage());
            }
            if (ListOf.isValid(listOf)) {
                // 获取所有返回的铺货状态数据
                List<OperationResultOf> operationResults = (List)listOf.getData();
                int count = distributeEntryList.size();
                int failCount = operationResults.size();
                StringBuilder resultMsg = new StringBuilder(32);
                if (operationResults != null && operationResults.size() > 0) {
                    resultMsg.append("总执行铺货数:" + count + "个。" +  "其中：失败个数为：" + failCount + "个。");
                    resultMsg.append("\n");
                    for (OperationResultOf vo : operationResults) {
                        resultMsg.append("失败offerId为:" + vo.getKey() + "原因为:" + vo.getMessage() + "。");
                    }
                    return ResultOf.error("false", resultMsg.toString());
                }
            }
            return ResultOf.general(true);
        } catch (Throwable e) {
            log.error("ERR_DISTRIBUTE:", e);
            return ResultOf.error("sys error", e.getMessage());
        }
    }

    /**
     * 组装渠道列表信息相关接口
     *
     * @param channelShopDTOS
     * @return
     */
    private List<PlatformShopModel> buildChannelShopList(List<ChannelShopDTO> channelShopDTOS) {
        log.info("channelShopDTOS:{}", JSON.toJSONString(channelShopDTOS));
        List<PlatformShopModel> platformShopModelList = Lists.newArrayList();
        // 先按照market进行分组
        Map<String, List<ChannelShopDTO>> groupBy = channelShopDTOS.stream()
            .collect(Collectors.groupingBy(ChannelShopDTO::getMarket));
        PlatformShopModel platformShopModel;
        for (Map.Entry<String, List<ChannelShopDTO>> entry : groupBy.entrySet()) {
            platformShopModel = new PlatformShopModel();
            List<ChannelShopDTO> channelShops = entry.getValue();
            List<ShopChannelModel> shopChannelModelList;
            ShopChannelModel shopChannelModel;
            String platformName = "";
            Long platformId = null;
            if (channelShops != null && channelShops.size() > 0) {
                shopChannelModelList = Lists.newArrayList();
                for (ChannelShopDTO channelShopDTO : channelShops) {
                    if (channelShopDTO.getShopId() == -1) {
                        platformName = channelShopDTO.getMarket();
                        platformName = ChannelMarketEnum.valueOf(platformName) != null ? ChannelMarketEnum.valueOf(
                            platformName).getDesc()
                            : "";
                        platformId = channelShopDTO.getId();
                        continue;
                    }
                    shopChannelModel = ShopChannelModel.builder().build();
                    shopChannelModel.setShopId(channelShopDTO.getId());
                    shopChannelModel.setShopName(channelShopDTO.getSellerName());
                    shopChannelModelList.add(shopChannelModel);
                }
                platformShopModel.setShopChannelModelList(shopChannelModelList);
            }
            platformShopModel.setPlatformId(platformId);
            platformShopModel.setPlatformName(platformName);
            platformShopModelList.add(platformShopModel);
        }
        return platformShopModelList;
    }

}
