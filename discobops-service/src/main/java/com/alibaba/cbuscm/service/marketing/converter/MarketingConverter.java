package com.alibaba.cbuscm.service.marketing.converter;

import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcMultiUserModel;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @Auther: gxd
 * @Date: 2019/11/13 19:30
 * @Description:
 */
public class MarketingConverter {
    public static List<String> convertSellerList(List<DcMultiUserModel> list) {
        List<String> shopNames = Lists.newArrayList();
        if (CollectionUtils.isEmpty(list)) {
            return shopNames;
        }
        for (DcMultiUserModel dcMultiUserModel : list) {
            shopNames.add(dcMultiUserModel.getShopName());
        }
        return shopNames;
    }

    public static Map<Long,String> convertSellerIdAndNameList(List<DcMultiUserModel> list) {
        Map<Long,String> shop = Maps.newHashMap();
        if (CollectionUtils.isEmpty(list)) {
            return shop;
        }
        for (DcMultiUserModel dcMultiUserModel : list) {
            shop.put(Long.valueOf(dcMultiUserModel.getChannelSellerId()),dcMultiUserModel.getShopName());
        }
        return shop;
    }


}
