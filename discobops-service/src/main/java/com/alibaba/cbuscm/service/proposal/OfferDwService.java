package com.alibaba.cbuscm.service.proposal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import com.alibaba.china.idatacenter.service.IDataCenterQueryService;
import com.alibaba.china.idatacenter.service.model.IDataCenterQueryParam;
import com.alibaba.china.idc.common.model.Result;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 类IDataCenterAdapter
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Service
@Slf4j
public class OfferDwService {

    @Resource
    private IDataCenterQueryService iDataCenterQueryService;

    private final static long COMBINE_ID = 607L;

    /**
     * 应用名字
     */
    public final static String APP_NAME = "disco-bops";

    private final static Set<String> RETURN_FIELDS = new HashSet<>();

    static {
        RETURN_FIELDS.add("offerId");
        RETURN_FIELDS.add("payLowestPrice1M001");
        RETURN_FIELDS.add("payItmUnitPrice6m002");
    }

    public List<DwData> queryDwData(List<Long> offerIds) {

        String paramString = StringUtils.collectionToDelimitedString(offerIds, ",");
        IDataCenterQueryParam param = new IDataCenterQueryParam();
        param.setCombineId(COMBINE_ID);
        param.setAppName(APP_NAME);
        param.setTimeout(10000);
        param.setReturnFields(RETURN_FIELDS);
        param.addParameter("offerId", paramString);
        param.setPageSize(offerIds.size());
        param.setPageNO(1);

        Result<List<Map<String, Object>>> result = iDataCenterQueryService.query(param);

        if (!(result != null && result.isSuccess() && result.getDefaultModel() != null)) {
            log.error("iDataCenterQueryService failed! msg=" + result.getErrorMsgs());
            if (result.getDefaultModel() != null) {
                log.info("WTF: {}", result.getDefaultModel());
            }
            return new ArrayList<>();
        }
        List<DwData> dwDataList = new ArrayList<>();
        for (Map<String, Object> dwData : result.getDefaultModel()) {
            DwData data = new DwData();
            data.setOfferId(Optional.ofNullable(dwData.get("offerId"))
                .map(Object::toString)
                .map(Long::parseLong)
                .orElse(null));
            data.setMinPrice30(Optional.ofNullable(dwData.get("payLowestPrice1M001"))
                .map(Object::toString)
                .map(Double::parseDouble)
                .orElse(null));
            data.setMinPrice180(Optional.ofNullable(dwData.get("payItmUnitPrice6m002"))
                .map(Object::toString)
                .map(Double::parseDouble)
                .orElse(null));
            dwDataList.add(data);
        }
        return dwDataList;
    }

    @Getter
    @Setter
    public static class DwData {

        private Long offerId;
        private Double minPrice30;
        private Double minPrice180;
    }
}
