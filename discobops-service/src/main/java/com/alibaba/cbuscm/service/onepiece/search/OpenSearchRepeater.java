package com.alibaba.cbuscm.service.onepiece.search;


import com.alibaba.cbuscm.service.onepiece.search.structure.OpenSearchQueryParam;
import com.alibaba.cbuscm.service.onepiece.search.util.Util;
import com.alibaba.cbuscm.service.onepiece.search.util.adapter.MapAdapter;
import com.alibaba.fastjson.JSON;
import com.aliyun.opensearch.OpenSearchClient;
import com.aliyun.opensearch.SearcherClient;
import com.aliyun.opensearch.sdk.dependencies.com.google.common.collect.Lists;
//import com.aliyun.opensearch.sdk.dependencies.org.json.JSONObject;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.opensearch.sdk.generated.OpenSearch;
import com.aliyun.opensearch.sdk.generated.search.*;
import com.aliyun.opensearch.sdk.generated.search.general.SearchResult;
import com.aliyun.opensearch.search.SearchParamsBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * creation hezhangbing 2019/09/27
 */
@Slf4j
public class OpenSearchRepeater {
    private static String appName;
    private static String accesskey;
    private static String secret;
    private static String host;

    private OpenSearchClient serviceClient;
    private SearcherClient searcherClient;
    private OpenSearchQueryParam queryParam;
//    private DocumentClient documentClient;

    private Map<String, String> queryFields; //查询条件，key-字段，value-值
    private Map<String, String> sortFields; //排序，key-字段，value-排序类型
    private List<String> filters;
    private List<String> returnFields; //查询返回字段集合

    private JSONObject jsonResult;

    public OpenSearchRepeater(String appName, String accesskey, String secret, String host) {
        this.appName = appName;
        this.accesskey = accesskey;
        this.secret = secret;
        this.host = host;
    }

    public OpenSearchRepeater() {
    }

    private void ini() throws Exception {
        if (StringUtils.isEmpty(appName) || StringUtils.isEmpty(accesskey) || StringUtils.isEmpty(secret) || StringUtils.isEmpty(host)) {
            log.error("It can't be empty:[appName,accesskey,secret,host]");
            throw new Exception("It can't be empty:[appName,accesskey,secret,host]");
        }
        //创建并构造OpenSearch对象
        OpenSearch openSearch = new OpenSearch(accesskey, secret, host);
        //创建OpenSearchClient对象，并以OpenSearch对象作为构造参数
        serviceClient = new OpenSearchClient(openSearch);
        //创建SearcherClient对象，并以OpenSearchClient对象作为构造参数
        searcherClient = new SearcherClient(serviceClient);

        //定义DocumentClient对象添加json格式doc数据批量提交
//        documentClient = new DocumentClient(serviceClient);
    }

    public void ini(OpenSearchQueryParam queryParam) throws Exception {
        ini();
        queryFields = Util.unpack(queryParam.getQuery()); //初始化查询条件
        sortFields = Util.unpack(queryParam.getSort()); //初始化排序
        returnFields = Util.fitField(queryParam.getFieldClass());
        filters = new MapAdapter(Util.unpack(queryParam.getFilter()));
        if (queryParam.getStart() == null || queryParam.getStart() < 0 || queryParam.getHits() == null || queryParam.getHits() <= 0) {
            log.error("must:start >= 0 ,hits > 0");
            throw new Exception("must:start >= 0 ,hits > 0");
        }
        this.queryParam = queryParam;
        log.info("initialization success");
    }

    /**
     * 简单查询
     *
     * @throws Exception
     */
    public JSONObject simplenessQuery() throws Exception {
        // 配置查询字段和分页
        Config config = new Config(Lists.newArrayList(appName));
        config.setStart(queryParam.getStart());
        config.setHits(queryParam.getHits());
        config.setSearchFormat(SearchFormat.JSON);
        config.setFetchFields(returnFields);
        SearchParams searchParams = new SearchParams(config);
        // 设置查询条件
        StringBuffer queryStr = new StringBuffer();
        Set<String> queryFieldKeys = queryFields.keySet();
        int i = 0;
        for (String key :
                queryFieldKeys) {
            if (i + 1 == queryFieldKeys.size())
                queryStr.append(key).append(":'").append(queryFields.get(key)).append("'");
            else

                queryStr.append(key).append(":'").append(queryFields.get(key)).append("' AND ");
            i++;
        }
        log.info("query parameters:{}", queryStr.toString());
        searchParams.setQuery(queryStr.toString());
        // 设置排序
        if (sortFields != null) {
            Sort sort = new Sort();
            Set<String> sortFieldKeys = sortFields.keySet();
            for (String key :
                    sortFieldKeys) {
                sort.addToSortFields(new SortField(key, Util.orderAdapter(sortFields.get(key))));
            }
            searchParams.setSort(sort);
        }
        SearchParamsBuilder paramsBuilder = SearchParamsBuilder.create(searchParams);
        // 设置查询过滤条件
        if (filters != null) {
            for (String filter :
                    filters) {
                paramsBuilder.addFilter(filter, "AND");
            }
        }
        log.info("searchParams:" + JSON.toJSONString(searchParams));
        log.info("query assembly is complete and query execution begins.");
        return execute(paramsBuilder);
    }

    private JSONObject execute(SearchParamsBuilder paramsBuilder) throws Exception {
        // 执行返回查询结果。用户需按code和message，进行异常情况判断。code对应的错误信息查看——错误码文档。
        SearchResult searchResult = searcherClient.execute(paramsBuilder);
        String result = searchResult.getResult();
        jsonResult = JSONObject.parseObject(result);
        // 输出查询结果
//            System.out.println(jsonResult.toString());
        log.debug(jsonResult.toString());
        log.info("query to complete.");
        return jsonResult;
    }
}
