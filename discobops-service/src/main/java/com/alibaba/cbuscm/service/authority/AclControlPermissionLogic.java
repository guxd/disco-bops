package com.alibaba.cbuscm.service.authority;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.alibaba.buc.acl.api.input.check.CheckPermissionsParam;
import com.alibaba.buc.acl.api.output.check.CheckPermissionsResult;
import com.alibaba.buc.acl.api.output.check.CheckPermissionsResult.CheckPermissionResultInner;
import com.alibaba.buc.acl.api.service.AccessControlService;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditEmpType;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbuscm.utils.CollectionUtils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jianhao.yzy
 * @Description:
 * @date 2019/7/10上午10:40
 */
@Service
@Slf4j
public class AclControlPermissionLogic {

    @Autowired
    private AccessControlService accessControlService;

    private static final String BS_CBU_OPERATOR = "obms-bigshop_cbu_operator";
    private static final String BS_TAO_OPERATOR = "obms-bigshop_tao_operator";
    private static final String BS_SUPER_OPERATOR = "obms-bigshop_super_operator";



    /**
     * 注意，这里给入的是BUC-userId
     * @param userId
     * @return
     */
    public DcAuditEmpType hasTaoShopPermission(Integer userId) {

        if(userId == null) {
            return null;
        }

        String logPrefix = "AclControlPermissionLogic#hasObmsPermission#";
        String paramInfo = "#uid:" + userId + "#";


        try{
            List<String> permissions = new ArrayList<>();
            permissions.add(BS_CBU_OPERATOR);
            permissions.add(BS_SUPER_OPERATOR);
            permissions.add(BS_TAO_OPERATOR);


            CheckPermissionsParam param = new CheckPermissionsParam();
            param.setAccessKey("obms-Kl482pr6C6&$U2(px0(IFRnQr");
            param.setUserId(userId);
            param.setPermissionNames(permissions);
            param.setOperatorUserId(userId);

            //{\"checkPermissionResults\":[{\"accessible\":true,\"permissionName\":\"obms-bigshop_super_operator\"},
            // {\"accessible\":true,\"permissionName\":\"obms-bigshop_tao_operator\"}],
            // \"code\":0,\"msg\":\"OK\",\"success\":true}
            CheckPermissionsResult result =  accessControlService.checkPermissions(param);

            if(result == null) {
                log.error(logPrefix + "Result_Null" + paramInfo);
                return null;
            }
            else if(!result.isSuccess()) {
                log.error(logPrefix + "Result_Fail_" + result.getCode() + "_" + result.getMsg() + paramInfo);
                return null;
            }

            //返回成功，进行解析
            DcEmpModel dcEmpModel = new DcEmpModel();
            List<CheckPermissionResultInner> authList = result.getCheckPermissionResults();

            if(CollectionUtils.isEmpty(authList)) {
                return null;
            }

            boolean isSuper = false;
            boolean isTao = false;
            boolean isCbu = false;

            for(CheckPermissionResultInner cur : authList) {
                if(cur == null) {
                    continue;
                }

                if(StringUtils.equals(cur.getPermissionName(), BS_CBU_OPERATOR) && cur.isAccessible()) {
                    isCbu = true;
                }
                else if(StringUtils.equals(cur.getPermissionName(), BS_TAO_OPERATOR) && cur.isAccessible()) {
                    isTao = true;
                }
                else if(StringUtils.equals(cur.getPermissionName(), BS_SUPER_OPERATOR) && cur.isAccessible()) {
                    isSuper = true;
                }
            }

            if(isSuper) {
                return DcAuditEmpType.SUPER;
            }
            else if(isCbu) {
                return DcAuditEmpType.CBU;
            }
            else if(isTao) {
                return DcAuditEmpType.TAO;
            }

        }catch (Exception e) {
            log.error(logPrefix + "Exception" + paramInfo, e);
        }

        return null;
    }



}
