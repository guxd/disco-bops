package com.alibaba.cbuscm.service;

import com.alibaba.cbuscm.common.PageResult;
import com.alibaba.cbuscm.service.onepiece.search.OpenSearchRepeater;
import com.alibaba.cbuscm.service.onepiece.search.structure.OpenSearchQueryParam;
//import com.aliyun.opensearch.sdk.dependencies.org.json.JSONObject;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AEOpenSearchService {

    private final static String appName = "cbu_overseas_oppo_search";
    private final static String accesskey = "LTAIjXTriel2EgAP";
    private final static String secret = "gu8a5fnatoHiiInGL0lf05GjXQwfxJ";
    private final static String host = "http://opensearch-cn-corp.aliyuncs.com";

    public PageResult query(OpenSearchQueryParam queryParam) throws Exception {
        OpenSearchRepeater openSearchRepeater = new OpenSearchRepeater(appName, accesskey, secret, host);
        openSearchRepeater.ini(queryParam);
        JSONObject json = openSearchRepeater.simplenessQuery();
        String status = json.getString("status");
        PageResult pageResult = new PageResult();
        pageResult.setSuccess("OK".equals(status) ? true : false);
        if (pageResult.isSuccess()) {
            pageResult.setPageSize(json.getJSONObject("result").getInteger("num"));
            pageResult.setPageNo(queryParam.getStart() + 1);
            pageResult.setTotal(json.getJSONObject("result").getInteger("total"));
            pageResult.setData(json.getJSONObject("result").getJSONArray("items"));
        } else {
            String code = Long.toString(json.getJSONArray("errors").getJSONObject(0).getLong("code"));
            String message = json.getJSONArray("errors").getJSONObject(0).getString("message");
            pageResult.setErrorMessage(code);
            pageResult.setErrorCode(message);
        }
        log.info(pageResult.toString());
        return pageResult;
    }
}

