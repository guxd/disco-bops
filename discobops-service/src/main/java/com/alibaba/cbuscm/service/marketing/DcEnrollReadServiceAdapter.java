package com.alibaba.cbuscm.service.marketing;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcEnrollReadService;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcItemEnrollDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:zhaoyu.zy@alibaba-inc.com">思渝</a>
 * @since 2019/9/27 上午12:01
 */
@Component
@Slf4j
public class DcEnrollReadServiceAdapter {

    private static String TAOBAO_PREFIX = "img.alicdn.com/bao/uploaded/";


    @Autowired
    private DcEnrollReadService dcEnrollReadService;

    /**
     * 查询offer和item在内贸的规格匹配情况
     * 本接口不关心淘宝商品商品的上架状态
     * 不查询SKU详情信息
     * @param itemId
     * @return
     */
    public DcItemEnrollDTO queryOfferEnrollDTO(Long itemId){
        try {
            ResultOf<DcItemEnrollDTO> result = dcEnrollReadService.queryOfferEnrollDTO(itemId);
            if(result != null){
                DcItemEnrollDTO itemEnroll = result.getData();
                if(itemEnroll != null && StringUtils.isNotBlank(itemEnroll.getItemPicUrl())){
                    itemEnroll.setItemPicUrl(TAOBAO_PREFIX + itemEnroll.getItemPicUrl());
                }else{
                    itemEnroll.setItemPicUrl(itemEnroll.getItemPicUrl());
                }
                return result.getData();
            }else {
                return null;
            }
        }catch (Exception e) {
            log.error("DcEnrollReadServiceAdapter.queryOfferEnrollDTO error!", e);
            return null;
        }
    }
}
