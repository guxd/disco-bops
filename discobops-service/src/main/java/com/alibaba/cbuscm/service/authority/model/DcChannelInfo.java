package com.alibaba.cbuscm.service.authority.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 类DcChannelInfo
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class DcChannelInfo {

    private String bizType;

    private String channel;

    private String priceCurrency;
}
