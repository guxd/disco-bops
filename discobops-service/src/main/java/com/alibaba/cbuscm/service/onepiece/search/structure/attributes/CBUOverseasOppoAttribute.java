package com.alibaba.cbuscm.service.onepiece.search.structure.attributes;

import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Filter;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Sort;
import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CBUOverseasOppoAttribute implements Sort, Filter {
    private String market_id;
    private String site_code;
    private String cate_id;
    private String type;
    private String x;
    private String y;
    private String market_gmv_30d;
    private String market_sales_30d;
    private String rate_for_same_pic;
    private String rate_for_same_style;
    private String rate_for_similar_style;
    private String x_median;
    private String y_median;
    private String is_global;
    private String product_num;
    private String market_growth_for_gmv;
    private String market_growth_for_sales;
    private String market_share_for_gmv;
    private String market_share_for_sales;
}
