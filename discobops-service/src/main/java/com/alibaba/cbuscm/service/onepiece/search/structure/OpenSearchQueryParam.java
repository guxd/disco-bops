package com.alibaba.cbuscm.service.onepiece.search.structure;


import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Field;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Filter;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Query;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Sort;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class OpenSearchQueryParam {

    private Integer start; //页码
    private Integer hits; //每页数量
    private Filter filter;
    private Query query;
    private Sort sort;
    private Field field;
    private Class fieldClass;

}
