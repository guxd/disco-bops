package com.alibaba.cbuscm.service.marketing.converter;

import com.alibaba.cbu.panama.dc.client.constants.marketingproduct.MarketingProductStatusEnum;
import com.alibaba.cbu.panama.dc.client.model.marketingproduct.MarketingProductDTO;
import com.alibaba.cbuscm.vo.marketing.product.MarketingProductListRespVO;
import org.testng.collections.Lists;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Auther: gxd
 * @Date: 2019/10/9 11:33
 * @Description:
 */
public class MarketingProductConvertDTOVO {
    public static List<MarketingProductListRespVO> getMarketingProductListRespVO(List<MarketingProductDTO> list) {
        return Optional.ofNullable(list).orElse(Lists.newArrayList()).stream().map(MarketingProductConvertDTOVO::getMarketingProductListRespVO).collect(Collectors.toList());
    }

    public static MarketingProductListRespVO getMarketingProductListRespVO(MarketingProductDTO param){
        MarketingProductListRespVO respVO = new MarketingProductListRespVO();
        respVO.setMarketingPlanId(param.getMarketingPlanId());
        respVO.setItemId(param.getItemId());
        respVO.setOfferId(param.getOfferId());
        respVO.setStatusMessage(MarketingProductStatusEnum.valueOf(param.getStatus()).getDesc());
        respVO.setStatus(param.getStatus());
        respVO.setMarketingProductId(param.getId());
        return respVO;
    }

}
