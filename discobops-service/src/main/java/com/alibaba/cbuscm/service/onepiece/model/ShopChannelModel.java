package com.alibaba.cbuscm.service.onepiece.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 店铺渠道模型
 * @author yvyuve.dyy
 * @date  2019/08/13
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShopChannelModel {
    private Long shopId;
    private String shopName;
}
