package com.alibaba.cbuscm.service.marketing.plan;

import com.alibaba.cbu.panama.dc.client.model.DcBaseResult;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanInitDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MaterialDTO;
import com.alibaba.cbu.panama.dc.client.service.marketingplan.MarketingPlanWriteService;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.RestResult;
import com.alibaba.cbuscm.log.constant.LogConstant;
import com.alibaba.cbuscm.vo.marketing.plan.req.MaterialReqVO;
import com.alibaba.cbuscm.vo.marketing.plan.req.QualificationReqVO;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * @Auther: gxd
 * @Date: 2019/10/29 14:03
 * @Description:
 */
@Slf4j
@Component
public class MarketingPlanWriteAdapter {
    @Autowired
    private MarketingPlanWriteService marketingPlanWriteService;

    /**
     * 创建活动（基础信息）
     * @param marketingPlanInitDTO
     * @return Long:活动id
     */
    public RestResult saveBaseinfo(MarketingPlanInitDTO marketingPlanInitDTO) {
        DcBaseResult<Long> dtoResult = marketingPlanWriteService.createMarketingPlan(marketingPlanInitDTO);
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","saveBaseinfo.marketingPlanWriteService.createMarketingPlan",
                    JSON.toJSON(marketingPlanInitDTO),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());
    }

    /**
     * 增加资质配置
     * @param reqVO marketingPlanId qcType qcInitDTO
     * @return
     */
    public RestResult<Boolean> addQualification(QualificationReqVO reqVO) {

        if (Objects.isNull(reqVO)){
            return RestResult.fail("001","QualificationReqVO为空");
        }
        if (Objects.isNull(reqVO.getMarketingPlanId())||Objects.isNull(reqVO.getQcInitDTO())||StringUtils.isEmpty(reqVO.getQcType())){
            return RestResult.fail("001","marketingPlanId或qcInitDTO或qcType为空");
        }

        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.addQualification(reqVO.getMarketingPlanId(),reqVO.getQcType(),reqVO.getQcInitDTO());
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","addQualification.marketingPlanWriteService.addQualification",
                    String.format("marketingPlanId=%s,qcType=%s,qcInitDTO=%s",reqVO.getMarketingPlanId(),reqVO.getQcType(),JSON.toJSON(reqVO.getQcInitDTO())),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());
    }

    /**
     * 移除资质配置
     * @return
     */
    public RestResult removeQualification(QualificationReqVO reqVO) {
        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.removeQualification(reqVO.getMarketingPlanId(), reqVO.getQcType(), reqVO.getQcCode());
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","removeQualification.marketingPlanWriteService.removeQualification",
                    String.format("marketingPlanId=%s,qcType=%s,qcCode=%s",reqVO.getMarketingPlanId(), reqVO.getQcType(), reqVO.getQcCode()),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());
    }

    /**
     * 添加素材配置
     */
    public RestResult addMaterial(MaterialReqVO reqVO) {
        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.addMaterial(reqVO.getMarketingPlanId(), reqVO.getMaterial());
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","addMaterial.marketingPlanWriteService.addMaterial",
                    String.format("marketingPlanId=%s,material=%s",reqVO.getMarketingPlanId(),JSON.toJSON(reqVO.getMaterial())),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());
    }

    /**
     * 移除素材配置
     * @param marketingPlanId
     * @param materialCode
     * @return
     */
    public RestResult removeMaterial(Long marketingPlanId, String materialCode) {
        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.removeMaterial(marketingPlanId, materialCode);
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","removeMaterial.marketingPlanWriteService.removeMaterial",
                    String.format("marketingPlanId=%s,materialCode=%s",marketingPlanId,materialCode),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());
    }

    /**
     * 增加商品白名单
     * @param marketingPlanId
     * @param itemIdList
     * @param operator
     * @return
     */
    public RestResult addWhiteItemIdList(Long marketingPlanId, List<Long> itemIdList, String operator) {
        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.addWhiteItemIdList(marketingPlanId, itemIdList,operator);
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","addWhiteItemIdList.marketingPlanWriteService.addWhiteItemIdList",
                    String.format("marketingPlanId=%s,itemIdList=%s,operator=%s",marketingPlanId,itemIdList,operator),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());

    }

    /**
     * 移除商品白名单
     * @param marketingPlanId
     * @param itemIdList
     * @param operator
     * @return
     */
    public RestResult removeWhiteItemIdList(Long marketingPlanId, List<Long> itemIdList, String operator) {
        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.removeWhiteItemIdList(marketingPlanId, itemIdList,operator);
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","removeWhiteItemIdList.marketingPlanWriteService.removeWhiteItemIdList",
                    String.format("marketingPlanId=%s,itemIdList=%s,operator=%s",marketingPlanId,itemIdList,operator),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());
    }

    /**
     * 发布活动
     * @param marketingPlanId
     * @param operator
     * @return
     */
    public RestResult publishMarketingPlan(Long marketingPlanId, String operator) {
        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.publishMarketingPlan(marketingPlanId, operator);
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","publishMarketingPlan.marketingPlanWriteService.publishMarketingPlan",
                    String.format("marketingPlanId=%s,operator=%s",marketingPlanId,operator),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
            return RestResult.fail(dtoResult.getErrorCode(),dtoResult.getErrorDesc());
        }
        return RestResult.success(dtoResult.getData());
    }
}
