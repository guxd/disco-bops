package com.alibaba.cbuscm.service.proposal;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.common.api.CategoryReadService;
import com.alibaba.cbu.disco.shared.core.common.constant.StdCategoryChannelEnum;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditTypeEnum;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductReadService;
import com.alibaba.cbu.disco.shared.core.product.constant.DcProductSkuStatusEnum;
import com.alibaba.cbu.disco.shared.core.product.constant.DcProductSupplyModeEnum;
import com.alibaba.cbu.disco.shared.core.product.model.DcLogisticAttributes;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductModel;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductSupplyRelation;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.proposal.contant.OfferProposalTagEnum;
import com.alibaba.cbu.disco.shared.core.proposal.model.DcProposalAuditLogModel;
import com.alibaba.cbu.disco.shared.core.proposal.model.DcProposalModel;
import com.alibaba.cbu.disco.shared.core.proposal.model.SkuInfoModel;
import com.alibaba.cbuscm.service.proposal.OfferDwService.DwData;
import com.alibaba.cbuscm.utils.BeanUtilsExt;
import com.alibaba.cbuscm.utils.MoneyUtils;
import com.alibaba.cbuscm.vo.disco.OfferProposalAuditLogVO;
import com.alibaba.cbuscm.vo.disco.OfferProposalDetailVO;
import com.alibaba.cbuscm.vo.disco.OfferProposalVO;
import com.alibaba.cbuscm.vo.disco.SkuInfoVO;
import com.alibaba.china.shared.discosupplier.enums.planning.design.PlanningDesignTypeEnum;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningDesignModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningProposalModel;
import com.alibaba.china.shared.discosupplier.model.planning.opp.PlanningOppItemModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 类OfferProposalExtendService
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Service
@Slf4j
public class OfferProposalExtendService {

    private static Map<String, String> RESULT_DESC_MAP;
    private static Map<String, String> PROPOSAL_STATUS_MAP;

    static {
        RESULT_DESC_MAP = new HashMap<>(4);
        RESULT_DESC_MAP.put("pass", "提报通过");
        RESULT_DESC_MAP.put("deny", "提报不通过");
        RESULT_DESC_MAP.put("reject", "提报退回");
        RESULT_DESC_MAP.put("delete", "已删除");

        PROPOSAL_STATUS_MAP = new HashMap<>(4);
        PROPOSAL_STATUS_MAP.put("init", "待审核");
        PROPOSAL_STATUS_MAP.put("deny", "提报不通过");
        PROPOSAL_STATUS_MAP.put("bind", "提报通过");
        PROPOSAL_STATUS_MAP.put("delete", "已删除");
    }

    @Autowired
    private OfferDwService offerDwService;

    @Autowired
    private CategoryReadService discoCategoryReadService;

    @Autowired
    private DcProductReadService productReadService;

    public OfferProposalDetailVO fillOfferProposalDetail(DcProposalModel proposal,
                                                         List<DcProposalAuditLogModel> auditLogs) {
        if (proposal == null) {
            return null;
        }
        Long offerId = proposal.getOfferId();
        if (offerId == null) {
            return null;
        }
        Set<Long> productExistSet = queryProposalProductSkuId(proposal.getProductId());
        DwData dwData = queryDwData(offerId);
        OfferProposalDetailVO vo = new OfferProposalDetailVO();
        BeanUtils.copyProperties(proposal, vo);
        vo.setTitle(proposal.getOfferTitle());
        vo.setImage(proposal.getOfferImage());
        vo.setMinPayPrice30(Optional.ofNullable(dwData).map(DwData::getMinPrice30).map(Double::longValue)
            .map(d -> d / 100.0d).orElse(null));
        vo.setMinPayPrice180(Optional.ofNullable(dwData).map(DwData::getMinPrice180).map(Double::longValue)
            .map(d -> d / 100.0d).orElse(null));
        vo.setAuditLogs(convertAuditLog(auditLogs));
        vo.setSkuInfo(convertSku(proposal.getSkuInfoList(), productExistSet));
        if (proposal.getCateIdLv3() != null) {
            vo.setCatePath(Arrays.asList(discoCategoryReadService.getCategoryNamePath(
                StdCategoryChannelEnum.CHANNEL_1688, proposal.getCateIdLv3(), ",").split(",")));
        } else if (proposal.getCateIdLv2() != null) {
            vo.setCatePath(Arrays.asList(discoCategoryReadService.getCategoryNamePath(
                StdCategoryChannelEnum.CHANNEL_1688, proposal.getCateIdLv2(), ",").split(",")));
        }
        vo.setStatusDesc(PROPOSAL_STATUS_MAP.getOrDefault(proposal.getStatus(), proposal.getStatus()));
        if(CollectionUtils.isNotEmpty(proposal.getTags())) {
            //long tagL = 0L;
            //for(String tag : proposal.getTags()) {
            //    OfferProposalTagEnum tagEnum = OfferProposalTagEnum.valueOf(tag);
            //    if(tagEnum!=null){
            //        tagL = OfferTagUtil.addTag(tagL,tagEnum);
            //    }
            //}
            OfferProposalTagEnum tagEnum = OfferProposalTagEnum.valueOf(proposal.getTags().get(0));
            if(tagEnum!=null) {
                vo.setOfferSource(String.valueOf(tagEnum.getIndex()));
            }
        }
        return vo;
    }

    public static List<SkuInfoVO> convertSku(List<SkuInfoModel> skuList, Set<Long> productExistSet) {
        if (CollectionUtils.isEmpty(skuList)) {
            return Collections.emptyList();
        }
        List<SkuInfoVO> resultList = new ArrayList<>(skuList.size());
        skuList.forEach(sku -> {
            SkuInfoVO proposalSku = new SkuInfoVO();
            BeanUtils.copyProperties(sku, proposalSku);
            proposalSku.setSkuPrice(MoneyUtils.fenToYuan(sku.getSkuPrice()));
            proposalSku.setPurchasePrice(MoneyUtils.fenToYuan(sku.getPurchasePrice()));
            proposalSku.setBarCode(sku.getBarCode());
            proposalSku.setHeight(Optional.ofNullable(sku.getLogisticAttributes())
                .map(DcLogisticAttributes::getHeight)
                .orElse(0));
            proposalSku.setWidth(Optional.ofNullable(sku.getLogisticAttributes())
                .map(DcLogisticAttributes::getWidth)
                .orElse(0));
            proposalSku.setLength(Optional.ofNullable(sku.getLogisticAttributes())
                .map(DcLogisticAttributes::getLength)
                .orElse(0));
            proposalSku.setWeight(Optional.ofNullable(sku.getLogisticAttributes())
                .map(DcLogisticAttributes::getWeight)
                .orElse(0));

            proposalSku.setSupplyModeDesc("经销");
            proposalSku.setSkuStatus(productExistSet.contains(sku.getSkuId() == null ? 0 : sku.getSkuId()));
            resultList.add(proposalSku);
        });
        return resultList;
    }

    private static List<OfferProposalAuditLogVO> convertAuditLog(List<DcProposalAuditLogModel> auditLogs) {
        if (CollectionUtils.isEmpty(auditLogs)) {
            return Collections.emptyList();
        }
        return auditLogs.stream()
            .filter(Objects::nonNull)
            .map(OfferProposalExtendService::convertAuditLog)
            .collect(Collectors.toList());
    }

    public static OfferProposalAuditLogVO convertAuditLog(DcProposalAuditLogModel auditLog) {
        // 原因
        String msg = auditLog.getMsg();
        String msgType = null;
        // 审核不通过原因类型
        if (DcAuditTypeEnum.safeValueOf(auditLog.getMsgType()) != null) {
            msgType = DcAuditTypeEnum.safeValueOf(auditLog.getMsgType()).getDesc();
        }
        if (StringUtils.isNotBlank(msg) && StringUtils.isNotBlank(msgType)) {
            msg = msgType + "(" + msg + ")";
        } else if (StringUtils.isBlank(msg) && StringUtils.isNotBlank(msgType)) {
            msg = msgType;
        }
        return OfferProposalAuditLogVO.builder()
            .gmtCreate(auditLog.getGmtCreate())
            .msg(msg)
            .operatorName(
                Optional.of(auditLog).map(DcProposalAuditLogModel::getOperator).map(OperatorModel::getName)
                    .orElse(null))
            .result(auditLog.getResult())
            .resultDesc(RESULT_DESC_MAP.get(auditLog.getResult()))
            .build();
    }

    public OfferProposalVO convertOfferProposalVO(DcProposalModel offerProposalModel) {
        OfferProposalVO offerProposalVO = BeanUtilsExt.convertToType(offerProposalModel, OfferProposalVO.class);
        offerProposalVO.setProductId(Optional.ofNullable(offerProposalModel.getProductId()).map(Object::toString).orElse(null));
        offerProposalVO.setOfferPrice(MoneyUtils.fenToYuan(offerProposalModel.getOfferPrice()));
        offerProposalVO.setSkuInfo(convertSku(offerProposalModel.getSkuInfoList(), new HashSet<>()));
        offerProposalVO.setStatusDesc(PROPOSAL_STATUS_MAP.get(offerProposalVO.getStatus()));
        offerProposalVO.setOfferStatusDesc(offerProposalModel.getOfferStatus() != null &&
            "PUBLISHED".equals(offerProposalModel.getOfferStatus().toUpperCase())
            ? "上架" : "下架");
        if(CollectionUtils.isNotEmpty(offerProposalModel.getTags())) {
            //long tagL = 0L;
            //for(String tag : offerProposalModel.getTags()) {
            //    OfferProposalTagEnum tagEnum = OfferProposalTagEnum.valueOf(tag);
            //    if(tagEnum!=null){
            //        tagL = OfferTagUtil.addTag(tagL,tagEnum);
            //    }
            //}
            OfferProposalTagEnum tagEnum = OfferProposalTagEnum.valueOf(offerProposalModel.getTags().get(0));
            if(tagEnum!=null) {
                offerProposalVO.setOfferSource(String.valueOf(tagEnum.getIndex()));
            }
        }
        return offerProposalVO;
    }

    private DwData queryDwData(Long offerId) {
        List<DwData> dwData = offerDwService.queryDwData(Collections.singletonList(offerId));
        if (dwData != null && !dwData.isEmpty()) {
            return dwData.get(0);
        }
        return null;
    }

    private Set<Long> queryProposalProductSkuId(Long productId) {
        Set<Long> productExistSet = new HashSet<>();
        if (productId == null) {
            return productExistSet;
        }
        ResultOf<DcProductModel> result = productReadService.getProductWithDetail(productId);
        if(ResultOf.isValid(result)) {
            if (result.getData().getProductSkuList().size() == 1
                && result.getData().getProductSkuList().get(0).getSupplyRelations() != null
                && !result.getData().getProductSkuList().get(0).getSupplyRelations().isEmpty()
                && result.getData().getProductSkuList().get(0).getSupplyRelations().get(0).getOfferSkuId() == null) {
                productExistSet.add(0L);
            } else {
                result.getData().getProductSkuList().stream()
                    .filter(ps -> DcProductSkuStatusEnum.PUBLISH.name().equals(ps.getStatus()))
                    .forEach(productSku -> productSku.getSupplyRelations().stream()
                        .filter(sr -> DcProductSupplyModeEnum.RETAIL.name().equals(sr.getSupplyMode()))
                        .map(DcProductSupplyRelation::getOfferSkuId)
                        .findFirst().ifPresent(productExistSet::add)
                    );
            }
        }
        return productExistSet;
    }

    public void setProposalPlanning(PlanningOppItemModel planningOppItemModel,
        PlanningDesignModel planningDesignModel,OfferProposalDetailVO vo){
        if(planningOppItemModel!=null && planningDesignModel!=null){
            PlanningProposalModel proposalModel = new PlanningProposalModel();
            proposalModel.setSpecs(planningDesignModel.buildPlanningSpecs());
            proposalModel.setTitle(planningDesignModel.getTitle());
            proposalModel.setPlanningId(planningDesignModel.getId());
            proposalModel.setKeyAttrs(planningOppItemModel.getAttrInfo());
            proposalModel.setDesc(planningDesignModel.getDesc());
            proposalModel.setCategoryValue(planningDesignModel.getCategoryValue());
            PlanningDesignTypeEnum type = planningDesignModel.getType();
            if(type!=null) {
                proposalModel.setType(type.getMessage());
            }
            vo.setPlanning(proposalModel);
        }
    }
}
