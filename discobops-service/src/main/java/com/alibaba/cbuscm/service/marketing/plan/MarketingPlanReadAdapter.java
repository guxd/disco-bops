package com.alibaba.cbuscm.service.marketing.plan;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcChannelInfoService;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcMultiUserModel;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanWhiteItemDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.ProtocolDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanDTO;

import java.util.*;

import com.alibaba.cbu.panama.dc.client.model.marketingplan.InvManageDTO;

import com.alibaba.cbu.panama.dc.client.model.DcBaseResult;
import com.alibaba.cbu.panama.dc.client.model.PageResult;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.*;
import com.alibaba.cbu.panama.dc.client.model.param.marketingplan.MarketingPlanPageParam;
import com.alibaba.cbu.panama.dc.client.service.marketingplan.MarketingPlanReadService;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.log.constant.LogConstant;
import com.alibaba.cbuscm.service.marketing.converter.MarketingConverter;
import com.alibaba.cbuscm.service.marketing.converter.MarketingProductConvert;
import com.alibaba.cbuscm.vo.marketing.param.*;
import com.alibaba.cbuscm.vo.marketing.plan.MarketingPlanRespVO;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Slf4j
@Component
public class MarketingPlanReadAdapter {

    @Autowired
    private MarketingPlanReadService marketingPlanReadService;

    @Autowired
    private DcChannelInfoService dcChannelInfoService;

    public PageResult<MarketingPlanDTO> pageMarketingPlanByParam(MarketingPlanPageParam param){
        try {
            DcBaseResult<PageResult<MarketingPlanDTO>> result = marketingPlanReadService.pageMarketingPlanByParam(param);
            if (!result.isSuccess()) {
                log.error(String.format("分页查询活动失败,param=%s",JSON.toJSONString(param)));
            }
            return result.getData();
        } catch (Exception cre) {
            log.error(String.format("分页查询活动失败,param=%s,error=%s",JSON.toJSONString(param),cre.getMessage()));
            throw cre;
        }
    }


    public List<MarketingPlanDTO> listMarketingPlanByParam(MarketingPlanPageParam param){

        PageResult<MarketingPlanDTO> result = pageMarketingPlanByParam(param);
        return result.getData();
    }

    public MarketingPlanRespVO queryMarketingPlanModelById(Long marketingPlanId,Long userId) {
        MarketingPlanRespVO respVO = new MarketingPlanRespVO();

        DcBaseResult<MarketingPlanModel> dtoResult = marketingPlanReadService.queryMarketingPlanModelById(marketingPlanId);
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanReadAdapter","queryMarketingPlanModelById",
                    String.format("marketingPlanId =%s",marketingPlanId),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
        }

        if (Objects.isNull(dtoResult.getData())){
            return respVO;
        }
        MarketingPlanModel marketingPlanModel = dtoResult.getData();

        //基础信息
        buildMarketingPlan(respVO, marketingPlanModel,userId);

        //协议
        buildProtoco(respVO, marketingPlanModel);

        //素材
        buildMaterial(respVO, marketingPlanModel);

        //资质
        buildQualification(respVO, marketingPlanModel);

        //白名单信息
        buildWhiteItem(respVO, marketingPlanModel);
        return respVO;
    }

    public Map<Long, String> querySellerList() {
        //userId 如果用户不传，则展示所有店，若传入，则基于条件进行测试店的判断。查询阶段默认不传userId;
        Long userId=null;
        ListOf<DcMultiUserModel> listOf = null;
        try {
            listOf = dcChannelInfoService.listMultiUserModel(userId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        if (null != listOf && CollectionUtils.isNotEmpty(listOf.getData())) {
            return MarketingConverter.convertSellerIdAndNameList(listOf.getData());
        }
        return null;
    }


    private void buildMarketingPlan(MarketingPlanRespVO respVO, MarketingPlanModel marketingPlanModel,Long userId) {
        MarketingPlanDTO marketingPlan = null;
        if (Objects.nonNull(marketingPlanModel.getMarketingPlan())){

            marketingPlan = marketingPlanModel.getMarketingPlan();
            BaseVO baseVO = new BaseVO();
            BeanUtils.copyProperties(marketingPlan,baseVO);
            respVO.setBaseInfo(baseVO);

            ListOf<DcMultiUserModel> listOf = null;
            try {
                listOf = dcChannelInfoService.listMultiUserModel(userId);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            if (null != listOf && CollectionUtils.isNotEmpty(listOf.getData())) {
                respVO.getBaseInfo().setSellerList(MarketingConverter.convertSellerList(listOf.getData()));
            }
        }
    }

    private void buildWhiteItem(MarketingPlanRespVO respVO, MarketingPlanModel marketingPlanModel) {
        MarketingPlanWhiteItemDTO whiteItem = null;
        if (Objects.nonNull(marketingPlanModel.getWhiteItem())){
            whiteItem = marketingPlanModel.getWhiteItem();
            MarketingPlanWhiteItemVO whiteItemVO = new MarketingPlanWhiteItemVO();
            BeanUtils.copyProperties(whiteItem,whiteItemVO);
            respVO.setMarketingPlanWhiteItem(whiteItemVO);
        }
    }

    private void buildQualification(MarketingPlanRespVO respVO, MarketingPlanModel marketingPlanModel) {
        List<QualificationDTO> qcDTOList = null;
        if (CollectionUtils.isNotEmpty(marketingPlanModel.getQcList())){
            qcDTOList = marketingPlanModel.getQcList();
            List<QualificationVO> qualificationVOList = Optional.ofNullable(qcDTOList).orElse(Lists.newArrayList()).stream().map(qualificationDTO -> {
                QualificationVO qualificationVO = new QualificationVO();
                BeanUtils.copyProperties(qualificationVO,qualificationDTO);
                return qualificationVO;
            }).collect(Collectors.toList());
            respVO.setQualificationList(qualificationVOList);
        }
    }

    private void buildMaterial(MarketingPlanRespVO respVO, MarketingPlanModel marketingPlanModel) {
        List<MaterialDTO> materialDTOList = null;
        if (CollectionUtils.isNotEmpty(marketingPlanModel.getMaterialList())){
            materialDTOList = marketingPlanModel.getMaterialList();
            List<MaterialVO> materialVOList = Optional.ofNullable(materialDTOList).orElse(Lists.newArrayList()).stream().map(materialDTO -> {
                MaterialVO materialVO = new MaterialVO();
                BeanUtils.copyProperties(materialVO,materialDTO);
                return materialVO;
            }).collect(Collectors.toList());
            respVO.setMaterialList(materialVOList);
        }
    }

    private void buildProtoco(MarketingPlanRespVO respVO, MarketingPlanModel marketingPlanModel) {
        ProtocolDTO protocolDTO = null;
        if (Objects.nonNull(marketingPlanModel.getProtocol())){
            protocolDTO = marketingPlanModel.getProtocol();
            ProtocolVO protocolVO = new ProtocolVO();
            BeanUtils.copyProperties(protocolDTO,protocolVO);
            respVO.setProtocol(protocolVO);
        }
    }
}
