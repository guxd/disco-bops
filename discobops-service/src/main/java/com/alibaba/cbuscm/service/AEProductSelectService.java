package com.alibaba.cbuscm.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.alibaba.alimonitor.jmonitor.utils.StringUtils;
import com.alibaba.cbuscm.common.PageResult;
import com.alibaba.cbuscm.config.RateDiamondConfiguration;
import com.alibaba.cbuscm.contants.DownPlatformContants;
import com.alibaba.cbuscm.enums.ErrorCodeEnum;
import com.alibaba.cbuscm.enums.SortFieldEnum;
import com.alibaba.cbuscm.vo.international.ae.AeProductSelectVO;
import com.alibaba.cbuscm.vo.international.ae.CbuOfferVO;
import com.alibaba.cbuscm.vo.international.ae.DownItemVO;
import com.alibaba.cbuscm.vo.international.ae.LogisticsVO;
import com.alibaba.cbuscm.params.AEProductQueryParam;
import com.alibaba.cbuscm.utils.DateUtils;
import com.alibaba.cbuscm.utils.MoneyUtils;
import com.alibaba.china.global.business.library.common.PageOf;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.enums.CurrencySymbolEnum;
import com.alibaba.china.global.business.library.enums.SelectionTaskExecStateEnum;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionTaskReadService;
import com.alibaba.china.global.business.library.interfaces.supply.selection.AeShopSelectService;
import com.alibaba.china.global.business.library.models.search.CbuProductModel;
import com.alibaba.china.global.business.library.models.search.DownstreamProductModel;
import com.alibaba.china.global.business.library.models.search.ha3.QueryRuleModel;
import com.alibaba.china.global.business.library.models.selection.SelectionTaskModel;
import com.alibaba.china.global.business.library.models.supply.selection.SelectionResultModel;
import com.alibaba.china.global.business.library.params.selection.SelectionTaskQueryParam;
import com.alibaba.china.global.business.library.utils.rate.CurrencyRateUtil;
import com.alibaba.common.lang.StringUtil;
import com.alibaba.fastjson.JSON;

import com.beust.jcommander.internal.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by duanyang.zdy on 2019/4/18.
 */
@Service
@Slf4j
public class AEProductSelectService {

    @Resource
    private AeShopSelectService aeShopSelectService;

    @Resource
    private SelectionTaskReadService selectionTaskReadService;

    @Resource
    private RateDiamondConfiguration rateiamondConfiguration;

    public PageResult<List<AeProductSelectVO>> search(AEProductQueryParam param){


        if(StringUtils.isNotBlank(param.getOrderFiled()) && SortFieldEnum.getTypeEnumByValue(param.getOrderFiled())==null){

            log.error("call AEProductSelectService error!task id is null");

            return PageResult.fail(ErrorCodeEnum.RESUELT_PARAM_ERROR.getValue(),ErrorCodeEnum.RESUELT_PARAM_ERROR.getValue());
        }

        try {

            PageResult<List<AeProductSelectVO>> pageResult = new PageResult<List<AeProductSelectVO>>();

            List<AeProductSelectVO> models = new ArrayList<>();

            //根据taskId取对应的标签,为去下游数据做准备

            ResultOf<SelectionTaskModel> selectionTaskModel =  selectionTaskReadService.findOne(SelectionTaskQueryParam.builder()
                                                                                                    .taskId(param.getTaskId())
                                                                                                    .build());

            log.error("selectionTaskModel="+ JSON.toJSONString(selectionTaskModel));

            if(!checkTaskStatus(selectionTaskModel).isSuccess()){return checkTaskStatus(selectionTaskModel);}


            //圈选下游商品(1个下游对应多个1688:查H3)
            QueryRuleModel queryRuleModel = new QueryRuleModel();

            queryRuleModel.addFilterEqualTo("tag_ids",String.valueOf(selectionTaskModel.getData().getSearchTagId()));
            queryRuleModel.addPrimarySortField(StringUtils.isEmpty(param.getOrderFiled())?SortFieldEnum.SALE_COUNT_30D_AVG.getValue():param.getOrderFiled(),true);
            queryRuleModel.setPageSize(param.getPageSize());
            queryRuleModel.setPageStart(param.getPageNo());

            log.error("queryRuleModel="+JSON.toJSONString(queryRuleModel));

            PageOf<DownstreamProductModel> downstreamGoodsModels =  aeShopSelectService.getDownstreamGoodsInfo(queryRuleModel);


            if(!PageOf.isValid(downstreamGoodsModels)){
                return PageResult.done(null);
            }

            log.error("down:downstreamGoodsModels.size="+ downstreamGoodsModels.getData().size());


            //过滤符合成本计算后的下游商品(1个下游对应1个1688)
            ResultOf<List<SelectionResultModel>> selectionResultModels = aeShopSelectService.getSelectionResult(param.getTaskId(),convertDownItemId(downstreamGoodsModels.getData()));


            //聚合页面数据
            if(!ResultOf.isValid(selectionResultModels)){

                return PageResult.done(null);
            }

            log.error("selectionResultModels="+JSON.toJSONString(selectionResultModels)+",size="+selectionResultModels.getData().size());


            //聚合1688数据(查H3)
            Map<Long,CbuOfferVO> cbuMap = queryB2bInfo(convertB2bOfferId(selectionResultModels.getData()));

            //聚合AE数据(查H3)
            Map<Long,DownItemVO> downMap = query(convertB2bOfferId(selectionResultModels.getData()), DownPlatformContants.DOWN_SITE_AE);


            log.error("ae:map="+JSON.toJSONString(downMap));

            downstreamGoodsModels.getData().forEach(model->{

                SelectionResultModel selectionResultModel = filterRelation(selectionResultModels.getData(),model.getSiteItemKey());

                LogisticsVO logisticsModel = LogisticsVO.builder()
                    .logisticsName(selectionResultModel.getRouteName())
                    .build();

                //todo:币种的转换
                DownItemVO downItemModel = DownItemVO.builder()
                    .siteItemId(selectionResultModel.getDownStreamItemId())
                    .pic(StringUtil.isEmpty(model.getTfsId())?model.getImage():model.getTfsId())
                    .price(model.getPrice()!=null?convertRate(model):"0")
                    .siteId(model.getSite())
                    .title(model.getTitle())
                    .itemUrl(model.getDetailUrl())
                    .cmtCount(model.getCmtCnt())
                    .likeCount(model.getLikeCnt())
                    .saleCount30dAvg(model.getSaleCnt30dAvg())
                    .build();

                CbuOfferVO cbuOfferModel =cbuMap.get(selectionResultModel.getExpectItemId());

                AeProductSelectVO aeModel = AeProductSelectVO.builder()
                    .aeItemCount(downMap.get(selectionResultModel.getExpectItemId()).getSameOfferCount())
                    .aePrice(new BigDecimal(MoneyUtils.fenToYuan(downMap.get(selectionResultModel.getExpectItemId()).getPrice())))
                    .estimatedPrice(selectionResultModel.getOverallCost()==null?new BigDecimal("0"):new BigDecimal(selectionResultModel.getOverallCost()))
                    .estimatedProfit(selectionResultModel.getProfitCost() == null?"0":selectionResultModel.getProfitCost().toString())
                    .gmtModified(DateUtils.formateDateTime(selectionTaskModel.getData().getGmtModified()))
                    .item(cbuOfferModel==null?CbuOfferVO.builder().build():cbuOfferModel)
                    .itemCost(selectionResultModel.getGoodsCost() == null?new BigDecimal("0"):new BigDecimal(selectionResultModel.getGoodsCost()))
                    .logistics(logisticsModel)
                    .relationId(selectionTaskModel.getData().getSearchTagId())
                    .siteItem(downItemModel)
                    .taskId(selectionTaskModel.getData().getTaskId().intValue())
                    .build();

                models.add(aeModel);
            });



            pageResult.setPageSize(downstreamGoodsModels.getPageSize());
            pageResult.setPageNo(downstreamGoodsModels.getPageNo());
            pageResult.setData(models);
            pageResult.setSuccess(true);
            pageResult.setTotal(downstreamGoodsModels.getTotal());

            return pageResult;


        }catch (Exception e){

            log.error("call search error!",e);

            return PageResult.fail(ErrorCodeEnum.SYSTEM_ERROR.getValue(),ErrorCodeEnum.SYSTEM_ERROR.getValue());
        }


    }

    private Boolean isTaskInit(String status){

        return SelectionTaskExecStateEnum.INIT.name().equals(status);

    }
    private Boolean isTaskStage(String status){

        return SelectionTaskExecStateEnum.STAGE.name().equals(status);

    }

    private Boolean isTaskRunning(String status){

        return SelectionTaskExecStateEnum.RUNNING.name().equals(status);

    }

    private Boolean isTaskPause(String status){

        return SelectionTaskExecStateEnum.RUNNING.name().equals(status);

    }

    /**
     * 重新进行排序
     */

    /**
     * 检查任务状态
     * @param selectionTaskModel
     * @return
     */

    private PageResult checkTaskStatus(ResultOf<SelectionTaskModel> selectionTaskModel){

        if(!ResultOf.isValid(selectionTaskModel)){

            return PageResult.fail(ErrorCodeEnum.TASK_NOT_CONFIG.getValue(),ErrorCodeEnum.TASK_NOT_CONFIG.getValue());

        }

        if(ResultOf.isValid(selectionTaskModel) && isTaskStage(selectionTaskModel.getData().getExecState())&& selectionTaskModel.getData().getPublishState().equals(true)){

            return PageResult.fail(ErrorCodeEnum.TASK_WAIT_QUEUE.getValue(),ErrorCodeEnum.TASK_WAIT_QUEUE.getValue());

        }

        if(ResultOf.isValid(selectionTaskModel) && isTaskInit(selectionTaskModel.getData().getExecState())){

            return PageResult.fail(ErrorCodeEnum.TASK_NOT_CONFIG.getValue(),ErrorCodeEnum.TASK_NOT_CONFIG.getValue());

        }

//        if(ResultOf.isValid(selectionTaskModel) && isTaskRunning(selectionTaskModel.getData().getExecState())){
//
//            return PageResult.fail(ErrorCodeEnum.TASK_RUNNING.getValue(),ErrorCodeEnum.TASK_RUNNING.getValue());
//
//        }

//        if(ResultOf.isValid(selectionTaskModel) && isTaskStage(selectionTaskModel.getData().getExecState()) && selectionTaskModel.getData().getPublishState().equals(false)){
//
//            return PageResult.fail(ErrorCodeEnum.TASK_PAUSE.getValue(),ErrorCodeEnum.TASK_PAUSE.getValue());
//
//        }

        return PageResult.done(null);

    }
    /**
     * 汇率转换
     * @param model
     * @return
     */
    private String convertRate(DownstreamProductModel model){

        return String.valueOf(CurrencyRateUtil.dollarsConverter(rateiamondConfiguration.getCurrencySymbolRateMap().get(CurrencySymbolEnum.valueOf(model.getPriceCurrency())),new BigDecimal(MoneyUtils.fenToYuan(model.getPrice().toString()))));

    }
    /**
     * 获取下游站点数据
     * @param offerIds
     * @param site
     * @return
     */
    private Map<Long,DownItemVO> query(List<Long> offerIds, String site){


        try {

            Map<Long,DownItemVO> map = Maps.newHashMap();

            QueryRuleModel queryRuleModel = new QueryRuleModel();


            queryRuleModel.setPageStart(DownPlatformContants.PAGE_START);
            queryRuleModel.setPageSize(DownPlatformContants.PAGE_SIZE);
            queryRuleModel.addQueryEqualTo(DownPlatformContants.SITE,site);
            queryRuleModel.addQueryEqualTo(DownPlatformContants.MATCH_ITMEM_IDS,offerIds.stream().map(i->i.toString()).collect(Collectors.joining(",")));


            log.error("ae:queryRuleModel="+JSON.toJSONString(queryRuleModel));

            PageOf<DownstreamProductModel> result = aeShopSelectService.getDownstreamGoodsInfo(queryRuleModel);

            if(result == null || result.getData() == null || !result.isSuccess()){return null;}


            offerIds.stream().distinct().forEach(offerId->{


                List<DownstreamProductModel>  models = result.getData().stream()
                    .filter(var->var.getPrice()!=null && !var.getPrice().equals(0L) && StringUtils.isNotBlank(var.getSameItems()) && var.getSameItems().contains(offerId.toString()))
                    .sorted((h1,h2)->h1.getPrice().compareTo(h2.getPrice())).collect(Collectors.toList());

                DownItemVO model = DownItemVO.builder()
                    .price(models.size()<1?"0":models.get(0).getPrice().toString())
                    .sameOfferCount(result.getData().stream().filter(data->data.getPrice()!=null &&  !data.getPrice().equals(0L) &&StringUtils.isNotBlank(data.getSameItems())&&data.getSameItems().contains(offerId.toString())).count())
                    .build();


                map.put(offerId,model);

            });

            return map;

        }catch (Exception e){

            log.error("call query error",e);
            return Maps.newHashMap();
        }


    }
    /**
     * 过滤出下游数据
     */
    private DownstreamProductModel filter(List<DownstreamProductModel> models,String downItemId){

        return models.stream().filter(model->downItemId.equals(model.getSiteItemKey())).findFirst().get();

    }

    /**
     * 过滤出下游数据
     */
    private SelectionResultModel filterRelation(List<SelectionResultModel> models,String downItemId){

        return models.stream().filter(model->StringUtils.isNotBlank(model.getDownStreamItemId())&&downItemId.equals(model.getDownStreamItemId())).findFirst().get();

    }

    /**
     * 获取全部下游itemId
     * @param models
     * @return
     */
    private List<String> convertDownItemId(List<DownstreamProductModel> models){

        List<String> res = new ArrayList<>();

        models.stream().forEach(model->{

            res.add(model.getSiteItemKey());
        });

        return res;

    }

    /**
     * 获取1688 offerid
     * @param models
     * @return
     */
    private List<Long> convertB2bOfferId(List<SelectionResultModel> models){

        List<Long> res = new ArrayList<>();

        models.stream().forEach(model->{

            res.add(model.getExpectItemId());
        });

        return res;

    }

    /**
     * 查询1688数据
     * @param offerIds
     * @return
     */
    private Map<Long,CbuOfferVO> queryB2bInfo(List<Long> offerIds){

        try {

            Map<Long,CbuOfferVO> map = Maps.newHashMap();

            QueryRuleModel queryRuleModel = new QueryRuleModel();

            queryRuleModel.setPageSize(offerIds.size());

            queryRuleModel.setPageStart(DownPlatformContants.PAGE_START);

            queryRuleModel.addQueryEqualTo("item_id", offerIds.stream().map(Object::toString).collect(Collectors.joining(",")));

            PageOf<CbuProductModel> cbuProductModels = aeShopSelectService.getCbuGoodsInfo(queryRuleModel);

            if(!PageOf.isValid(cbuProductModels)){

                return map;
            }

            cbuProductModels.getData().stream().forEach(data->{

                CbuOfferVO cbuOfferModel = CbuOfferVO.builder()
                    .itemId(data.getItemId())
                    .pic(data.getMainImg())
                    .title(data.getTitle())
                    .itemUrl(DownPlatformContants.PRE_DETAIL_URL+data.getItemId()+DownPlatformContants.AFT_DETAIL_URL)
                    .build();

                map.put(data.getItemId(),cbuOfferModel);
            });


            return map;


        }catch (Exception e){


            log.error("call queryB2bInfo error!",e);

            return null;

        }

    }

    public static void main(String[] args) {
    }

}
