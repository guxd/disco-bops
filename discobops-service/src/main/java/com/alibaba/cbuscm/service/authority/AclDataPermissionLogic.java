package com.alibaba.cbuscm.service.authority;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.buc.acl.api.common.AclResult;
import com.alibaba.buc.acl.api.service.DataAccessControlService;
import com.alibaba.buc.api.datapermission.param.CheckDataPermissionParam;
import com.alibaba.buc.api.datapermission.param.DataProfileParam;
import com.alibaba.buc.api.datapermission.param.OperationProfileParam;
import com.alibaba.buc.api.datapermission.param.UserProfileParam;
import com.alibaba.buc.api.datapermission.property.EnumProperty;
import com.alibaba.buc.api.datapermission.result.CheckDataPermissionResult;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.fastjson.JSON;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 类AclDataPermissionService
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Service
@Slf4j
public class AclDataPermissionLogic {

    public static final String PERMISSION_NAME_BIZ_TYPE = "productManageBizTypeAccess";
    public static final String PERMISSION_NAME_CHANNEL = "productManageChannelAccess";
    public static final String OPERATION_NAME_DATA_ACCESS = "dataAccess";
    public static final String DATA_MODEL_BIZ_TYPE = "bizType";
    public static final String DATA_MODEL_CHANNEL = "channel";

    @Value("${acl.accress.key}")
    private String ACL_ACCESS_KEY;

    @Autowired
    private DataAccessControlService dataAccessControlService;

    public ListOf<String> checkDataPermissionAccess(Integer userId,
                                                    String permissionName,
                                                    String operationName,
                                                    String dataModelName,
                                                    List<String> enumPropertyName) {
        try {
            CheckDataPermissionParam param = buildEnumPropertyCheckDataParam(userId, permissionName,
                operationName, dataModelName, enumPropertyName);
            AclResult<Map<DataProfileParam, CheckDataPermissionResult>> result = dataAccessControlService.checkDataPermission(param);
            log.info("Check Data Permission: userId={}, permName={}, operName={}, dataMode={}, propName={}, rawResult={}",
                userId, permissionName, operationName, dataModelName, enumPropertyName, JSON.toJSONString(result));
            if (result.isSuccess() && result.getResult() != null && !result.getResult().isEmpty()) {
                List<String> access = new ArrayList<>();
                result.getResult().forEach((k,v) -> {
                    if (v.isAccessible()){
                        access.add(k.getPropertyList().get(0).getPropertyValue());
                    }
                });
                return ListOf.general(access);
            } else {
                return ListOf.error("acl-error-" + result.getCode(), result.getMsg());
            }
        } catch (Exception e) {
            log.error("AuthorityLogic.checkDataPermissionAccess", e);
            return ListOf.error("system-error", e.getMessage());
        }
    }

    private CheckDataPermissionParam buildEnumPropertyCheckDataParam(Integer userId,
                                                                     String permissionName,
                                                                     String operationName,
                                                                     String dataModelName,
                                                                     List<String> enumPropertyName) {
        CheckDataPermissionParam param = new CheckDataPermissionParam();
        param.setAccessKey(ACL_ACCESS_KEY);
        param.setDataPermissionName(permissionName);
        UserProfileParam user = new UserProfileParam();
        user.setUserId(userId);
        param.setUser(user);
        OperationProfileParam operation = new OperationProfileParam();
        operation.setOperationName(operationName);
        param.setOperation(operation);
        List<DataProfileParam> dataList = new ArrayList<>();
        for (String propertyName : enumPropertyName) {
            DataProfileParam dataProfile = new DataProfileParam();
            dataProfile.setDataModelName(dataModelName);
            List<EnumProperty> properties = new ArrayList<>();
            EnumProperty p = new EnumProperty();
            p.setPropertyName(dataModelName);
            p.setEnumValue(propertyName);
            properties.add(p);
            dataProfile.setPropertyList(properties);
            dataList.add(dataProfile);
        }
        param.setDataList(dataList);
        return param;
    }
}
