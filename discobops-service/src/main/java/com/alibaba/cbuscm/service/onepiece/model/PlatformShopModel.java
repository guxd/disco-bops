package com.alibaba.cbuscm.service.onepiece.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 平台店铺服务模型
 * @author yvyuve.dyy
 * @date  2019/08/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlatformShopModel {
    private Long platformId;
    private String platformName;
    private List<ShopChannelModel> shopChannelModelList;
}
