package com.alibaba.cbuscm.service.authority;

import com.alibaba.boot.hsf.annotation.HSFConsumer;
import com.alibaba.buc.acl.api.service.DataAccessControlService;
import com.alibaba.cbu.disco.shared.core.config.api.DcContextService;

import org.springframework.context.annotation.Configuration;

/**
 * 类ProductPermissionConfig
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */@Configuration
public class ProductPermissionConfig {

    @HSFConsumer(serviceVersion = "1.0.0")
    DcContextService dcContextService;

    @HSFConsumer(serviceVersion = "${acl.hsf.version}")
    DataAccessControlService dataAccessControlService;
}
