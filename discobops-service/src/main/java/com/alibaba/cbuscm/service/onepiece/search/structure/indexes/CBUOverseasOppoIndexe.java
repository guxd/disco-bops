package com.alibaba.cbuscm.service.onepiece.search.structure.indexes;

import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Query;
import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CBUOverseasOppoIndexe implements Query {
    private String id;
    private String market_name;
    private String site_code;
    private String cate_id;
    private String type;
    private String is_global;
    private String cate_name;
    private String market_tags;
    private String sites;
}
