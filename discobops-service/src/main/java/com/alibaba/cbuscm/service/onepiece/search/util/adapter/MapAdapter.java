package com.alibaba.cbuscm.service.onepiece.search.util.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapAdapter<T> extends ArrayList {
    public MapAdapter(Map map) {
        if (map == null)
            return;
        Set keys = map.keySet();
        for (Object key :
                keys) {
            super.add(map.get(key));
        }
    }
}
