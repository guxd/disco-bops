package com.alibaba.cbuscm.service.onepiece.search.util;

import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.OpenSearchStructure;
import com.aliyun.opensearch.sdk.generated.search.Order;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Util {

    /**
     * 将所有属性名称放入list
     */
    public static List fitField(Class<com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Field> clazz) {
        if (clazz == null)
            return null;
        List fieldName = new ArrayList();
        fieldName.clear();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field :
                fields) {
            fieldName.add(field.getName());
        }
        return fieldName;
    }

    /**
     * 拆包 将对象里的值放入map中
     */
    public static Map unpack(OpenSearchStructure openSearchStructure) throws IllegalArgumentException, IllegalAccessException {
        if (openSearchStructure == null)
            return null;
        Map map = new HashMap();
        Field[] fields = openSearchStructure.getClass().getDeclaredFields();
        for (Field field :
                fields) {
            field.setAccessible(true);
            Object fieldValue = field.get(openSearchStructure);
            if (fieldValue != null)
                map.put(field.getName(), fieldValue);
        }
        return map;
    }

    public static Order orderAdapter(String order) {
        if ("DECREASE".equals(order))
            return Order.DECREASE;
        else if ("INCREASE".equals(order))
            return Order.INCREASE;
        return Order.INCREASE;
    }
}
