package com.alibaba.cbuscm.service.onepiece.search.test;

import com.alibaba.cbuscm.service.AEOpenSearchService;
import com.alibaba.cbuscm.service.onepiece.search.OpenSearchRepeater;
import com.alibaba.cbuscm.service.onepiece.search.structure.OpenSearchQueryParam;
import com.alibaba.cbuscm.service.onepiece.search.structure.attributes.CBUOverseasOppoAttribute;
import com.alibaba.cbuscm.service.onepiece.search.structure.fields.CBUOverseasOppoField;
import com.alibaba.cbuscm.service.onepiece.search.structure.indexes.CBUOverseasOppoIndexe;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Filter;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Query;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Sort;
import com.aliyun.opensearch.sdk.dependencies.org.json.JSONObject;

public class Test {
    public static void main(String[] a) throws Exception {
        Query query = CBUOverseasOppoIndexe.builder().market_name("手表").build();
        Sort sort = CBUOverseasOppoAttribute.builder()
                .rate_for_same_style("DECREASE")
                .build();
        Filter filter = CBUOverseasOppoAttribute.builder()
                .rate_for_same_style("rate_for_same_style<20").build();
        OpenSearchQueryParam queryParam = OpenSearchQueryParam.builder()
                .query(query)
                .sort(sort)
                .filter(filter)
                .start(12)
                .hits(100)
                .fieldClass(CBUOverseasOppoField.class)
                .build();
//        OpenSearchRepeater openSearchRepeater = new OpenSearchRepeater("cbu_overseas_oppo_search", "LTAIjXTriel2EgAP", "gu8a5fnatoHiiInGL0lf05GjXQwfxJ", "http://opensearch-cn-corp.aliyuncs.com");
//        openSearchRepeater.ini(queryParam);
//        JSONObject json = openSearchRepeater.simplenessQuery();
        AEOpenSearchService aeOpenSearchService = new AEOpenSearchService();
        System.out.println(aeOpenSearchService.query(queryParam));
    }
}
