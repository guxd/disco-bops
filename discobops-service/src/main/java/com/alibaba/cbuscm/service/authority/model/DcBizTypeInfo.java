package com.alibaba.cbuscm.service.authority.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 类DcBisTypeInfo
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class DcBizTypeInfo {

    private String bizType;

    private List<String> channel;

    private String priceCurrency;
}
