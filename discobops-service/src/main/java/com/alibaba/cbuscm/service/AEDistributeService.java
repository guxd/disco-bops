package com.alibaba.cbuscm.service;

import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.OperationResultOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.config.api.DcContextService;
import com.alibaba.cbu.disco.shared.core.config.model.DcContext;
import com.alibaba.cbu.disco.shared.core.product.api.DcDistributeManageService;
import com.alibaba.cbu.disco.shared.core.product.api.DcOfferImportService;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeEntryParam;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeOfferParam;
import com.alibaba.cbuscm.service.authority.ProductDataAccessService;
import com.alibaba.cbuscm.service.authority.model.DcChannelInfo;
import com.alibaba.cbuscm.vo.onepiece.selection.SelectionItemStatusVO;
import com.alibaba.cbuscm.vo.onepiece.selection.StatusListVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.alibaba.cbuscm.utils.CollectionUtils.streaming;
import static com.alibaba.cbuscm.utils.CollectionUtils.toSet;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.ObjectUtils.isEmpty;

@Component
@Slf4j
public class AEDistributeService {

    @Autowired
    private DcOfferImportService dcOfferImportService;

    @Autowired
    private ProductDataAccessService productDataAccessService;

    @Autowired
    private DcDistributeManageService dcDistributeManageService;

    @Autowired
    private DcContextService dcContextService;

    public ResultOf<? extends Object> distributionToAe(StatusListVO statusListVO, BucSSOUser user) {
        try {
            Long taskId = statusListVO.getList().get(0).getTaskId();
            if (CollectionUtils.isEmpty(statusListVO.getList())) {
                return ResultOf.error("empty entry", "entries cannot be null");
            }
            List<DcDistributeEntryParam> distributeEntryList = new ArrayList<>(20);
            for (SelectionItemStatusVO entry : statusListVO.getList()) {
                if (entry == null) {
                    continue;

                }
                String site = null;
                String itemId = null;
                if(!StringUtils.isBlank(entry.getDownItemId())){
                    String [] arr = entry.getDownItemId().split("\\$");
                    site = arr[1];
                    itemId = arr[0];
                }
                distributeEntryList.add(new DcDistributeEntryParam(entry.getOfferId(), site, itemId));
            }
            if (CollectionUtils.isEmpty(distributeEntryList)) {
                return ResultOf.error("empty entry", "entries cannot be null");
            }
            // 获取用户的授权渠道+配置的铺货渠道
            List<String> distChannels = getDistributeChannels(user);
            log.info("user authorized distribution channels: {}", distChannels);
            if (isEmpty(distChannels)) {
                return ResultOf.error("ERR_DISTRIBUTE", "current user has no available channel");
            }

            // 默认取第一个渠道的 bizType
            DcContext.Channel channel = dcContextService.getChannel(distChannels.get(0));

            // 进行多渠道铺货
            DcDistributeOfferParam distributeOfferParam = DcDistributeOfferParam.builder()
                    .channels(distChannels)
                    .bizType(channel.getBizTypeName())
                    .entries(distributeEntryList)
                    .selectionTaskId(taskId)
                    .operator(OperatorModel.builder()
                            .id(user.getEmpId())
                            .name(user.getLoginName())
                            .build())
                    .build();
            ListOf<OperationResultOf<Long>> listOf = dcDistributeManageService.batchDistribute(distributeOfferParam);

            if (ListOf.isValid(listOf)) {
                return ResultOf.general(listOf.getData());
            } else {
                List<OperationResultOf<Long>> result = listOf.getData();
                if (CollectionUtils.isEmpty(result) || result.size() == 1) {
                    return ResultOf.error(listOf.getErrorCode(), listOf.getErrorMessage());
                }
                long successCount = streaming(result).map(OperationResultOf::getResult).filter(TRUE::equals).count();
                long errorCount = result.size() - successCount;
                return ResultOf.error(listOf.getErrorCode(),
                        String.format("%d个开始铺货，%d个不能铺货", successCount, errorCount));
            }
        } catch (Throwable e) {
            log.error("ERR_DISTRIBUTE:", e);
            return ResultOf.error("sys error", e.getMessage());
        }
    }

    private List<String> getDistributeChannels(BucSSOUser user) {

        // 管理员配置的铺货渠道
        ListOf<String> opChannels = dcDistributeManageService.getDistributeChannels(user.getEmpId());
        Set<String> opChannelSet = ListOf.isNonEmpty(opChannels)
                ? toSet(opChannels.getData())
                : emptySet();

        log.info("operator channels: {}", opChannels);

        // 管理员已授权的渠道
        List<DcChannelInfo> authorizedChannelList = getAuthorizedChannelList(user);

        log.info("authorized channels: {}", authorizedChannelList);
        return streaming(authorizedChannelList)
                .map(DcChannelInfo::getChannel)
                .filter(opChannelSet::contains)
                .collect(toList());
    }

    private List<DcChannelInfo> getAuthorizedChannelList(BucSSOUser user) {
        try {
            ListOf<DcChannelInfo> authorizedChannelList = productDataAccessService.queryUserAuthorizedChannel(user.getId());
            return ListOf.isValid(authorizedChannelList) ? authorizedChannelList.getData() : Collections.emptyList();
        } catch (Exception e) {
            log.error("AeShopDistributionController.getAuthorizedChannelList exception!", e);
            return Collections.emptyList();
        }
    }

    /**
     * 前端传入的数据格式不是真的下游站点id，格式：itemId$site
     *
     * @param originItemId
     * @return
     */
    private String pureDownstreamItemId(String originItemId) {
        if (StringUtils.isBlank(originItemId)) {
            return null;
        }
        String[] data = originItemId.split("\\$");
        return data[0];
    }

}
