package com.alibaba.cbuscm.service.marketing.converter;

import com.alibaba.cbu.panama.dc.client.constants.marketingproduct.MarketingProductStatusEnum;
import com.alibaba.cbu.panama.dc.client.model.param.marketingproduct.MarketingProductPageParam;
import com.alibaba.cbuscm.vo.marketing.param.MarketProductListReq;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @Auther: gxd
 * @Date: 2019/10/8 22:25
 * @Description:
 */
public class MarketingProductConvert {



    public static MarketingProductPageParam getMarketingProductPageParam(MarketProductListReq req) {
        MarketingProductPageParam marketingProductPageParam = new MarketingProductPageParam();
        if (Objects.nonNull(req.getMarketingPlanId())) {
            marketingProductPageParam.setMarketPlanIdList(Lists.newArrayList(req.getMarketingPlanId()));
        }
        if (StringUtils.isNotBlank(req.getStatus())) {
            marketingProductPageParam.setStatusList(Lists.newArrayList(MarketingProductStatusEnum.valueOf(req.getStatus()).name()));
        }
        marketingProductPageParam.setOfferId(req.getOfferId());
        marketingProductPageParam.setItemId(req.getItemId());
        marketingProductPageParam.setPageNo(req.getPageNo());
        marketingProductPageParam.setPageSize(req.getPageSize());
        return marketingProductPageParam;


    }

}
