package com.alibaba.cbuscm.service.marketing.product;

import com.alibaba.cbu.panama.dc.client.model.DcBaseResult;
import com.alibaba.cbu.panama.dc.client.model.marketingproduct.MarketingProductSalesDTO;
import com.alibaba.cbu.panama.dc.client.model.param.marketingproduct.ModifyPaymentInfoParam;
import com.alibaba.cbu.panama.dc.client.model.param.marketingproduct.RejectMarketingProductParam;
import com.alibaba.cbu.panama.dc.client.service.marketingproduct.MarketingProductWriteService;
import com.alibaba.cbuscm.constants.BopsErrorCodeEnum;
import com.alibaba.cbuscm.exceptions.BopsBizRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Slf4j
@Component
public class MarketingProductWriteAdapter {

    @Autowired
    private MarketingProductWriteService marketingProductWriteService;

    public Boolean configPayment(ModifyPaymentInfoParam param) {
        try{
            DcBaseResult<Boolean> result = marketingProductWriteService.modifyPaymentInfo(param);
            if(!result.isSuccess()) {
                throw new BopsBizRuntimeException(BopsErrorCodeEnum.CONFIG_PAYMENT_EXCEPTION,result.getErrorDesc());

            }
            return result.getData();
        }catch (Exception cre){
            throw new BopsBizRuntimeException(BopsErrorCodeEnum.CONFIG_PAYMENT_EXCEPTION);
        }
    }

    public Boolean paymentConfirmByOwner(Long marketingProductId, String operator) {
        try{
            DcBaseResult<Boolean> result = marketingProductWriteService.paymentConfirmByOwner(marketingProductId,operator);
            if(!result.isSuccess()) {
                throw new BopsBizRuntimeException(BopsErrorCodeEnum.PAYMENT_CONFIRM_BY_OWNER_EXCEPTION,result.getErrorDesc());

            }
            return result.getData();
        }catch (Exception cre){
            throw new BopsBizRuntimeException(BopsErrorCodeEnum.PAYMENT_CONFIRM_BY_OWNER_EXCEPTION);
        }

    }

    public Boolean firstAudit(Long marketingProductId, String operator, String remark) {
        try{
            DcBaseResult<Boolean> result = marketingProductWriteService.firstAudit(marketingProductId,operator,remark);
            if(!result.isSuccess()) {
                throw new BopsBizRuntimeException(BopsErrorCodeEnum.AUDIT_OPTIMISTIC_LOCK_FAIL,result.getErrorDesc());
            }
            return result.getData();
        }catch (Exception cre){
            throw new BopsBizRuntimeException(BopsErrorCodeEnum.AUDIT_OPTIMISTIC_LOCK_FAIL);
        }
    }

    public Boolean secondAudit(Long marketingProductId, String operator, String remark) {
        try{
            DcBaseResult<Boolean> result = marketingProductWriteService.secordAudit(marketingProductId,operator,remark);
            if(!result.isSuccess()) {
                throw new BopsBizRuntimeException(BopsErrorCodeEnum.AUDIT_OPTIMISTIC_LOCK_FAIL,result.getErrorDesc());
            }
            return result.getData();
        }catch (Exception cre){
            throw new BopsBizRuntimeException(BopsErrorCodeEnum.AUDIT_OPTIMISTIC_LOCK_FAIL);
        }
    }

    public Boolean reject(RejectMarketingProductParam param) {
        try {
            DcBaseResult<Boolean> result = marketingProductWriteService.reject(param);
            if (!result.isSuccess()) {
                throw new BopsBizRuntimeException(BopsErrorCodeEnum.AUDIT_OPTIMISTIC_LOCK_FAIL,result.getErrorDesc());
            }
            return result.getData();
        } catch (Exception cre) {
            throw new BopsBizRuntimeException(BopsErrorCodeEnum.AUDIT_OPTIMISTIC_LOCK_FAIL);
        }
    }


    public Boolean configSale(Long marketingProductId, Integer guaranteedSales,Integer expectedSales) {
        MarketingProductSalesDTO marketingProductSalesDTO = new MarketingProductSalesDTO();
        marketingProductSalesDTO.setGuaranteedSales(guaranteedSales);
        marketingProductSalesDTO.setExpectedSales(expectedSales);
        try {
            DcBaseResult<Boolean> result = marketingProductWriteService.saveProductSales(marketingProductId,marketingProductSalesDTO);
            if (!result.isSuccess()) {
                throw new BopsBizRuntimeException(BopsErrorCodeEnum.CONFIG_SALES_EXCEPTION,result.getErrorDesc());
            }
            return result.getData();
        } catch (Exception cre) {
            throw new BopsBizRuntimeException(BopsErrorCodeEnum.CONFIG_SALES_EXCEPTION);

        }
    }

}
