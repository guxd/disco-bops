package com.alibaba.cbuscm.service.product;

import com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum;
import com.alibaba.cbu.disco.channel.api.constants.PricingStrategyEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.core.product.model.DcChannelProductModel;
import com.alibaba.cbu.disco.shared.core.product.model.DcChannelProductSkuModel;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductModel;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductSkuModel;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductSupplyRelation;
import com.alibaba.cbu.disco.shared.core.product.model.SalesRelationModel;
import com.alibaba.cbu.disco.shared.core.supplier.model.DcSupplierModel;
import com.alibaba.cbuscm.utils.MoneyUtils;
import com.alibaba.cbuscm.vo.international.product.ChannelProductSkuVO;
import com.alibaba.cbuscm.vo.international.product.ChannelProductVO;
import com.alibaba.cbuscm.vo.international.product.ProductSkuVO;
import com.alibaba.cbuscm.vo.international.product.ProductVO;
import com.alibaba.cbuscm.vo.international.product.SalesRelationVO;
import com.alibaba.cbuscm.vo.international.product.SupplierVO;
import com.alibaba.cbuscm.vo.international.product.SupplyRelationVO;
import com.taobao.mtop.commons.utils.CollectionUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 类ProductConvertService
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
public class ProductConverter {

    private static final Map<String, String> statusConverter = new HashMap<>();
    private static final Map<String, String> storageTypeConverter = new HashMap<>();
    private static final Map<String, String> marketingModeConverter = new HashMap<>();
    private static final Map<String, String> distributeStatusConverter = new HashMap<>();
    private static final Map<String, String> priceCurrencyConverter = new HashMap<>();
    private static final Map<String, String> supplyModelConverter = new HashMap<>();

    static {
        statusConverter.put("PUBLISH", "在售");
        statusConverter.put("UNPUBLISH", "停售");
        statusConverter.put("DELETE", "删除");

        storageTypeConverter.put("VIRTUAL", "代销");
        storageTypeConverter.put("REAL", "寄售");

        marketingModeConverter.put("MARKUP", "加价");
        marketingModeConverter.put("SHARE", "分佣");

        distributeStatusConverter.put("INIT", "未铺货");
        distributeStatusConverter.put("SUCCESS", "已铺货");
        distributeStatusConverter.put("FAIL", "铺货失败");

        priceCurrencyConverter.put("USD", "$");
        priceCurrencyConverter.put("CNY", "¥");

        supplyModelConverter.put("PROXY", "代销");
        supplyModelConverter.put("RETAIL", "经销");
    }

    public static <T, V> PageOf<T> convert(PageOf<V> source, Function<List<V>, List<T>> converter) {
        PageOf<T> target = new PageOf<>();
        target.setData(converter.apply(source.getData()));
        target.setSuccess(source.isSuccess());
        target.setErrorCode(source.getErrorCode());
        target.setErrorMessage(source.getErrorMessage());
        target.setPageNo(source.getPageNo());
        target.setPageSize(source.getPageSize());
        target.setTotal(source.getTotal());
        return target;
    }

    public static List<ProductVO> convertProduct(List<DcProductModel> products) {
        if (products == null || products.isEmpty()) {
            return null;
        }
        return products.stream().map(ProductConverter::convertProduct).collect(Collectors.toList());
    }

    public static ProductVO convertProduct(DcProductModel product) {
        ProductVO vo = new ProductVO();
        BeanUtils.copyProperties(product, vo);
        vo.setId(product.getId().toString());
        vo.setStatus(statusConverter.getOrDefault(product.getStatus(), product.getStatus()));
        vo.setProductSkuList(convertSku(product.getProductSkuList()));
        vo.setExtraAttrs(product.getExtra());
        return vo;
    }

    public static List<ProductSkuVO> convertSku(List<DcProductSkuModel> products) {
        if (products == null || products.isEmpty()) {
            return null;
        }
        return products.stream().map(ProductConverter::convertSku).collect(Collectors.toList());
    }

    public static ProductSkuVO convertSku(DcProductSkuModel product) {
        ProductSkuVO vo = new ProductSkuVO();
        BeanUtils.copyProperties(product, vo);
        vo.setId(product.getId().toString());
        vo.setProductId(product.getProductId().toString());
        vo.setStatus(statusConverter.getOrDefault(product.getStatus(), product.getStatus()));
        vo.setStatusDesc(statusConverter.getOrDefault(product.getStatus(), product.getStatus()));
        vo.setSupplyRelationList(convertSupplyRelationList(product.getSupplyRelations()));
        vo.setSalesRelationList(convertSalesRelationList(product.getSalesRelationModels()));
        vo.setCnSkuId(product.getCnSkuId() == null ? null : product.getCnSkuId().toString());
        vo.setBarCode(convertBarCode(product.getGpin(), product.getBarCode()));
        vo.setGspSkuId(product.getGspSkuId() == null ? null : product.getGspSkuId().toString());
        vo.setLogisticAttributes(product.getLogisticAttributes());
        vo.setGspId(product.getGspId() == null ? null : product.getGspId().toString());
        if (product.getSupplyRelations() != null) {
            StringBuilder stringBuilder = new StringBuilder();
            Set<String> supplyModels = product.getSupplyRelations().stream().map(supply -> supply.getSupplyMode()).collect(Collectors.toSet());
            for (String supplyModel : supplyModels) {
                if (supplyModelConverter.get(supplyModel) != null) {
                    if (!(stringBuilder.toString().length() <= 0)) {
                        stringBuilder.append("/");
                    }
                    stringBuilder.append(supplyModelConverter.get(supplyModel));
                }
            }
            vo.setSupplyMode(stringBuilder.toString());
        }

        return vo;
    }

    private static String convertBarCode(String gpin, List<String> barCodeStrList) {

        if (StringUtils.isBlank(gpin) && CollectionUtils.isEmpty(barCodeStrList)) {
            return null;
        }
        StringBuilder barCodeString = new StringBuilder();
        if (StringUtils.isNotBlank(gpin)) {
            barCodeString.append(gpin);
        }
        if (CollectionUtil.isNotEmpty(barCodeStrList)) {
            for (String s : barCodeStrList) {
                if (barCodeString.length() == 0) {
                    barCodeString.append(s);
                } else {
                    barCodeString.append("#").append(s);
                }
            }
        }
        return barCodeString.toString();
    }

    private static List<SalesRelationVO> convertSalesRelationList(List<SalesRelationModel> salesRelationModels) {
        if (salesRelationModels == null || salesRelationModels.isEmpty()) {
            return null;
        }
        return salesRelationModels.stream().map(ProductConverter::convertSalesRelation).collect(Collectors.toList());
    }

    private static SalesRelationVO convertSalesRelation(SalesRelationModel salesRelationModel) {
        if (salesRelationModel == null) {
            return null;
        }
        SalesRelationVO salesRelationVO = new SalesRelationVO();
        BeanUtils.copyProperties(salesRelationModel, salesRelationVO);
        // 渠道业务
        Map<String, String> bizTypeMap = ChannelMarketEnum.map();
        salesRelationVO.setBizType(bizTypeMap.get(salesRelationModel.getBizType()));
        // 业务模式
        Map<String, String> marketIngMap = PricingStrategyEnum.map();
        salesRelationVO.setMarketingMode(marketIngMap.get(salesRelationModel.getMarketingMode()));
        return salesRelationVO;
    }


    private static List<SupplyRelationVO> convertSupplyRelationList(List<DcProductSupplyRelation> supplyRelations) {
        if (supplyRelations == null || supplyRelations.isEmpty()) {
            return null;
        }
        return supplyRelations.stream().map(ProductConverter::convert).collect(Collectors.toList());
    }

    public static SupplyRelationVO convert(DcProductSupplyRelation relation) {
        if (relation == null) {
            return null;
        }
        SupplyRelationVO vo = new SupplyRelationVO();
        BeanUtils.copyProperties(relation, vo);
        vo.setId(relation.getId().toString());
        vo.setProductId(relation.getProductId().toString());
        vo.setProductSkuId(relation.getProductSkuId().toString());
        vo.setStatus(statusConverter.getOrDefault(relation.getStatus(), relation.getStatus()));
        vo.setStatusDesc(statusConverter.getOrDefault(relation.getStatus(), relation.getStatus()));
        vo.setSupplier(convert(relation.getSupplier()));
        vo.setLogisticsCost(relation.getLogisticsCost() == null ? null : MoneyUtils.fenToYuan(Long.valueOf(relation.getLogisticsCost())));
        vo.setPurchasePrice(relation.getPurchasePrice() == null ? null : MoneyUtils.fenToYuan(Long.valueOf(relation.getPurchasePrice())));
        vo.setRelationId(relation.getId());
        return vo;
    }

    private static SupplierVO convert(DcSupplierModel supplier) {
        if (supplier == null) {
            return null;
        }
        SupplierVO vo = new SupplierVO();
        BeanUtils.copyProperties(supplier, vo);
        return vo;
    }

    public static List<ChannelProductVO> convertChannel(List<DcChannelProductModel> products) {
        if (products == null || products.isEmpty()) {
            return null;
        }
        return products.stream().map(ProductConverter::convertChannel).collect(Collectors.toList());
    }

    public static ChannelProductVO convertChannel(DcChannelProductModel product) {
        ChannelProductVO vo = new ChannelProductVO();
        BeanUtils.copyProperties(product, vo);
        vo.setProductId(product.getProductId().toString());
        vo.setTitle(product.getTitle());
        vo.setStatus(statusConverter.getOrDefault(product.getStatus(), product.getStatus()));
        vo.setDistributeStatus(distributeStatusConverter.getOrDefault(product.getDistributeStatus(),
            product.getDistributeStatus()));
        vo.setChannelSkuList(convertChannelSku(product.getChannelSkuList()));
        return vo;
    }

    public static List<ChannelProductSkuVO> convertChannelSku(List<DcChannelProductSkuModel> products) {
        if (products == null || products.isEmpty()) {
            return null;
        }
        return products.stream().map(ProductConverter::convertChannelSku).collect(Collectors.toList());
    }

    public static ChannelProductSkuVO convertChannelSku(DcChannelProductSkuModel product) {
        ChannelProductSkuVO vo = new ChannelProductSkuVO();
        BeanUtils.copyProperties(product, vo);
        vo.setProductId(product.getProductId().toString());
        vo.setProductSkuId(product.getProductSkuId().toString());
        vo.setStatus(statusConverter.getOrDefault(product.getStatus(), product.getStatus()));
        /*vo.setStorageType(storageTypeConverter.getOrDefault(product.getStorageType(), product.getStorageType()));
        vo.setMarketingMode(marketingModeConverter.getOrDefault(product.getMarketingMode(),
            product.getMarketingMode()));
        vo.setPriceCurrency(convertPriceCurrency(product.getPriceCurrency()));*/
        return vo;
    }

    public static String convertPriceCurrency(String priceCurrency) {
        return priceCurrencyConverter.getOrDefault(priceCurrency, priceCurrency);
    }

    public static ProductSkuVO convertProductSku(DcProductModel productModel) {
        if (productModel == null) {
            return null;
        }
        DcProductSkuModel skuModel = productModel.getProductSkuList().get(0);
        if (skuModel == null) {
            return null;
        }
        ProductSkuVO productSkuVO = convertSku(skuModel);
        productSkuVO.setProductTitle(productModel.getTitle());
        return productSkuVO;
    }
}
