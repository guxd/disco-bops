package com.alibaba.cbuscm.service.onepiece.search.structure.fields;

import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Field;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class CBUOverseasOppoField implements Field {
    private String market_id;
    private String market_name;
    private String site_code;
    private String cate_id;
    private String cate_name;
    private String type;
    private String x;
    private String y;
    private String market_gmv_30d;
    private String market_sales_30d;
    private String rate_for_same_pic;
    private String rate_for_same_style;
    private String rate_for_similar_style;
    private String x_median;
    private String y_median;
    private String market_tags;
    private String is_global;
    private String sites;
    private String product_num;
    private String images;
    private String market_growth_for_gmv;
    private String market_growth_for_sales;
    private String market_share_for_gmv;
    private String market_share_for_sales;
}
