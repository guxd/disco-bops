package com.alibaba.cbuscm.service.channel;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductSupplyRelation;
import com.alibaba.cbu.disco.shared.core.product.model.DcLogisticAttributes;
import com.alibaba.cbuscm.vo.channel.req.ModifiedDetailVO;
import com.google.common.collect.Maps;
import com.google.common.collect.Lists;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductReadService;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductModel;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductSkuModel;
import com.alibaba.cbu.panama.dc.client.model.DcBaseResult;
import com.alibaba.cbu.panama.dc.client.model.PageResult;
import com.alibaba.cbu.panama.dc.client.model.channelprice.ChannelPriceDTO;
import com.alibaba.cbu.panama.dc.client.model.channelprice.ChannelPriceRecordOuterDTO;
import com.alibaba.cbu.panama.dc.client.model.channelprice.ModifiedDetailDTO;
import com.alibaba.cbu.panama.dc.client.model.channelprice.WarehouseSubCostDTO;
import com.alibaba.cbu.panama.dc.client.model.param.channelprice.ChannelPriceLogParam;
import com.alibaba.cbu.panama.dc.client.model.param.channelprice.ChannelPricePageParam;


import com.alibaba.cbu.panama.dc.client.service.channelprice.ChannelPriceReadService;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.log.constant.LogConstant;
import com.alibaba.cbuscm.vo.channel.FeeTypeEnum;
import com.alibaba.cbuscm.vo.channel.WarehouseRespVO;
import com.alibaba.cbuscm.vo.channel.resp.ChannelPriceListRespVO;
import com.alibaba.cbuscm.vo.channel.resp.ChannelProductRespVO;
import com.alibaba.cbuscm.vo.channel.resp.ChannelProductSKUResqVO;
import com.alibaba.cbuscm.vo.marketing.product.OperateLogRespVO;
import com.alibaba.cbuscm.vo.marketing.product.PageResultResp;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Auther: gxd
 * @Date: 2019/10/16 10:35
 * @Description:
 */
@Slf4j
@Component
public class ChannelPriceReadAdapter {


    @Autowired
    private ChannelPriceReadService channelPriceReadService;

    @Autowired
    private DcProductReadService dcProductReadService;

    @Value("${uri.image.server}")
    private String imageServer;

    /**
     * 渠道价格列表
     *
     * @param param
     * @return
     */
    public PageResultResp<ChannelPriceListRespVO> pageChannelPriceByParam(ChannelPricePageParam param) {
        PageResultResp<ChannelPriceListRespVO> voResult = new PageResultResp<>();

        //1 查询商品sku信息
        DcBaseResult<PageResult<ChannelPriceDTO>> dtoResult = channelPriceReadService.pageMarketingProductByParam(param);
        if (!dtoResult.isSuccess()) {
            BopsLogUtil.logServiceFailed("ChannelPriceReadAdapter", "pageMarketingProductByParam", JSON.toJSONString(param),
                    0L, LogConstant.LogType.SAL, new Exception(dtoResult.getErrorDesc()));
            return voResult;
        }
        if (Objects.isNull(dtoResult.getData())){
            return voResult;
        }


        //2封装VO
        List<ChannelPriceDTO> dtoList = dtoResult.getData().getData();
        List<ChannelPriceListRespVO> voList = Optional.ofNullable(dtoList).orElse(Lists.newArrayList()).stream().map(priceDTO -> {
            ChannelPriceListRespVO respVO = new ChannelPriceListRespVO();
            //商品信息
            ResultOf<DcProductModel> dtoProductResult = dcProductReadService.getProductWithDetail(priceDTO.getProductId());
            if (!dtoProductResult.isSuccess()) {
                BopsLogUtil.logServiceFailed("ChannelPriceReadAdapter", "getProductWithDetail", JSON.toJSONString(param),
                        0L, LogConstant.LogType.SAL, new Exception(dtoProductResult.getErrorMessage()));

            }
            buildPagePriceProduct(priceDTO, respVO, dtoProductResult);

            // sku信息
            ResultOf<DcProductSkuModel> dtoProductSkuResult = dcProductReadService.findProductSkuByCnSkuId(priceDTO.getCnSkuId());
            if (!dtoProductSkuResult.isSuccess()) {
                BopsLogUtil.logServiceFailed("ChannelPriceReadAdapter", "findProductSkuByCnSkuId", JSON.toJSONString(param),
                        0L, LogConstant.LogType.SAL, new Exception(dtoProductResult.getErrorMessage()));

            }
            buildPagePriceProductSku(respVO, dtoProductSkuResult);
            //其他价格信息
            buildPagePriceOthers(priceDTO, respVO);
            return respVO;
        }).collect(Collectors.toList());

        //3返回
        voResult.setData(voList);
        voResult.setTotal(dtoResult.getData().getTotal());
        return voResult;
    }

    private PageResultResp<ChannelPriceListRespVO> pageChannelPriceByParamTest(ChannelPricePageParam param) {
       PageResultResp<ChannelPriceListRespVO> voResult = new PageResultResp<>();

       //1 查询商品sku信息
       WarehouseSubCostDTO warehouseSubCostDTO = new WarehouseSubCostDTO();
       warehouseSubCostDTO.setType("DOMESTIC_WAREHOUSE_OPERATING_COST");
       warehouseSubCostDTO.setCost(1.0D);


       ChannelPriceDTO channelPriceDTO = new ChannelPriceDTO();
       channelPriceDTO.setId(0L);
       channelPriceDTO.setProductId(1L);
       channelPriceDTO.setProductSkuId(1L);
       channelPriceDTO.setCnSkuId(0L);
       channelPriceDTO.setMarket("");
       channelPriceDTO.setGoodsCost(0.0D);
       channelPriceDTO.setWarehouseCost(Lists.newArrayList(warehouseSubCostDTO));
       channelPriceDTO.setExchangeRate(0.0D);
       channelPriceDTO.setGrossProfitMargin(0.0D);
       channelPriceDTO.setSupplyPrice(0.0D);
       channelPriceDTO.setGmtCreate(new Date());
       channelPriceDTO.setGmtModified(new Date());

       PageResult pageResult = new PageResult();
       pageResult.setData(Lists.newArrayList(channelPriceDTO));
       pageResult.setTotal(0);

       DcBaseResult<PageResult<ChannelPriceDTO>> dtoResult = new DcBaseResult();
       dtoResult.setData(pageResult);
       dtoResult.setErrorCode("");
       dtoResult.setErrorDesc("");
       dtoResult.setSuccess(false);
       dtoResult.setCanRetry(false);




       //2封装VO
       List<ChannelPriceDTO> dtoList = dtoResult.getData().getData();
       List<ChannelPriceListRespVO> voList = Optional.ofNullable(dtoList).orElse(Lists.newArrayList()).stream().map(x -> {
           ChannelPriceListRespVO respVO = new ChannelPriceListRespVO();
           //商品信息
           DcProductModel dcProductModel = new DcProductModel();
           dcProductModel.setId(0L);
           dcProductModel.setGmtCreate(new Date());
           dcProductModel.setGmtModified(new Date());
           dcProductModel.setStatus("");
           dcProductModel.setTitle("");
           dcProductModel.setImage("");
           dcProductModel.setDetail("");
           dcProductModel.setKeyAttributes(Lists.newArrayList());
           dcProductModel.setFeatureAttributes(Lists.newArrayList());
           dcProductModel.setProductSkuList(Lists.newArrayList());
           dcProductModel.setExtra(Maps.newHashMap());
           dcProductModel.setBizType("");
           dcProductModel.setTags(0L);
           dcProductModel.setImagePkg(Lists.newArrayList());
           dcProductModel.setGspId(0L);
           dcProductModel.setDistributeStatus("");
           dcProductModel.setTradeAttributes(Lists.newArrayList());
           dcProductModel.setCateIdLv1(0L);
           dcProductModel.setCateIdLv2(0L);
           dcProductModel.setCateIdLv3(0L);
           dcProductModel.setOwnerId(0L);

           ResultOf<DcProductModel> dtoProductResult = new ResultOf<>();
           dtoProductResult.setSuccess(false);
           dtoProductResult.setData(dcProductModel);
           dtoProductResult.setErrorCode("1");
           dtoProductResult.setErrorMessage("");
           dtoProductResult.setStatus("");
           buildPagePriceProduct(x, respVO, dtoProductResult);

           // sku信息
           DcProductSkuModel dcProductSkuModel = new DcProductSkuModel();
           dcProductSkuModel.setId(0L);
           dcProductSkuModel.setGmtCreate(new Date());
           dcProductSkuModel.setGmtModified(new Date());
           dcProductSkuModel.setStatus("");
           dcProductSkuModel.setProductId(0L);
           dcProductSkuModel.setKeyAttributes(Lists.newArrayList());
           dcProductSkuModel.setDesc("");
           dcProductSkuModel.setImage("");
           dcProductSkuModel.setLogisticAttributes(new DcLogisticAttributes());
           dcProductSkuModel.setPrice(0);
           dcProductSkuModel.setPriceCurrency("");
           dcProductSkuModel.setRetailPrice(0);
           dcProductSkuModel.setBizType("");
           dcProductSkuModel.setStock(0);
           dcProductSkuModel.setGmtSync(new Date());
           dcProductSkuModel.setGpin("");
           dcProductSkuModel.setBarCode(Lists.newArrayList());
           dcProductSkuModel.setExtraJson(Maps.newHashMap());
           dcProductSkuModel.setCnSkuId(0L);
           dcProductSkuModel.setGspSkuId(0L);
           dcProductSkuModel.setGspId(0L);
           dcProductSkuModel.setMainSupplyRelation(new DcProductSupplyRelation());
           dcProductSkuModel.setSupplyRelations(Lists.newArrayList());
           dcProductSkuModel.setSalesRelationModels(Lists.newArrayList());

           ResultOf<DcProductSkuModel> dtoProductSkuResult = new ResultOf<>();
           dtoProductSkuResult.setSuccess(false);
           dtoProductSkuResult.setData(dcProductSkuModel);
           dtoProductSkuResult.setErrorCode("");
           dtoProductSkuResult.setErrorMessage("");
           dtoProductSkuResult.setStatus("");

           buildPagePriceProductSku(respVO, dtoProductSkuResult);
           //其他价格信息
           buildPagePriceOthers(x, respVO);
           return respVO;
       }).collect(Collectors.toList());

       //3返回
       voResult.setData(voList);
       voResult.setTotal(dtoResult.getData().getTotal());
       return voResult;
   }

    private void buildPagePriceProduct(ChannelPriceDTO channelPriceDTO, ChannelPriceListRespVO respVO, ResultOf<DcProductModel> dtoProductResult) {
        DcProductModel dtoProduct = dtoProductResult.getData();

        if (dtoProduct == null || respVO == null) {
            return;
        }

        ChannelProductRespVO product = respVO.getProduct();
        if (product == null) {
            product = new ChannelProductRespVO();
        }

        product.setProductTitle(dtoProduct.getTitle());
        if (StringUtils.isNotBlank(dtoProduct.getImage())){
            product.setProductImg(imageServer+dtoProduct.getImage());
        }
        product.setProductId(channelPriceDTO.getProductId());

        respVO.setProduct(product);
    }

    private void buildPagePriceOthers(ChannelPriceDTO priceDTO, ChannelPriceListRespVO respVO) {
        if (priceDTO == null || respVO == null) {
            return;
        }
        respVO.setProductCost(priceDTO.getGoodsCost());

        //封装其他费用
        respVO.setExchangeRate(priceDTO.getExchangeRate());
        respVO.setGrossProfitRate(priceDTO.getGrossProfitMargin());
        respVO.setSupplyPrice(priceDTO.getSupplyPrice());
        respVO.setMarket(priceDTO.getMarket());
        respVO.setOfferId(priceDTO.getOfferId());


        //封装仓库费用明细
        List<WarehouseSubCostDTO> costList = priceDTO.getWarehouseCost();
        if (CollectionUtils.isEmpty(costList)) {
            return;
        }

        List<WarehouseRespVO> warehouseRespList = costList.stream().map(warehouseSubCostDTO -> {
            WarehouseRespVO warehouseRespVO = new WarehouseRespVO();
            warehouseRespVO.setCode(warehouseSubCostDTO.getType());

            if (StringUtils.isNotBlank(FeeTypeEnum.valueOf(warehouseSubCostDTO.getType()).getDesc())){
                warehouseRespVO.setDesc(FeeTypeEnum.valueOf(warehouseSubCostDTO.getType()).getDesc());
            }

            warehouseRespVO.setValue(warehouseSubCostDTO.getCost());
            return warehouseRespVO;
        }).collect(Collectors.toList());
        respVO.setWarehouseCost(warehouseRespList);
        respVO.setRelId(priceDTO.getId());
    }

    public static void main(String[] args) throws ParseException {
        ChannelPriceReadAdapter adapter = new ChannelPriceReadAdapter();

        ChannelPriceDTO priceDTO = new ChannelPriceDTO();
        priceDTO.setId(107462139L);
        priceDTO.setProductId(650000273624680936L);
        priceDTO.setProductSkuId(4252049037582L);
        priceDTO.setMarket("");
        priceDTO.setExchangeRate(7.0657);
        priceDTO.setSupplyPrice(1.26);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        priceDTO.setGmtCreate(df.parse("2019-10-23 18:56:03,000"));
        priceDTO.setGmtCreate(df.parse("2019-10-23 18:56:03,000"));

        ChannelPriceListRespVO respVO = new ChannelPriceListRespVO();

        adapter.buildPagePriceOthers(priceDTO, respVO);

        System.out.println(JSON.toJSONString(respVO));
    }

    private void buildPagePriceProductSku(ChannelPriceListRespVO respVO, ResultOf<DcProductSkuModel> dtoProductSkuResult) {
        if (respVO == null || dtoProductSkuResult == null) {
            return;
        }
        DcProductSkuModel dtoProductSku = dtoProductSkuResult.getData();
        if (dtoProductSku == null) {
            return;
        }

        ChannelProductSKUResqVO productSku = Optional.ofNullable(respVO.getProductSku()).orElse(new ChannelProductSKUResqVO());

        if (StringUtils.isNotBlank(dtoProductSku.getDesc())){
            productSku.setDesc(dtoProductSku.getDesc().replace(",","/"));
        }
        if (Objects.nonNull(dtoProductSku.getId())){
            productSku.setSkuId(dtoProductSku.getId());
        }
        if (Objects.nonNull(dtoProductSku.getCnSkuId())){
            productSku.setSkuCnId(dtoProductSku.getCnSkuId());
        }
        respVO.setProductSku(productSku);
    }

    public PageResultResp<OperateLogRespVO> queryProductLogList(ChannelPriceLogParam param) {
        PageResultResp<OperateLogRespVO> voResult = new PageResultResp<>();

        //1调用远程接口
        DcBaseResult<PageResult<ChannelPriceRecordOuterDTO>> dtoResult  = channelPriceReadService.pagePriceChangeLog(param);;
        if (!dtoResult.isSuccess()) {
            BopsLogUtil.logServiceFailed("ChannelPriceReadAdapter", "pagePriceChangeLog", JSON.toJSONString(param),
                    0L, LogConstant.LogType.SAL, new Exception(dtoResult.getErrorDesc()));
            return voResult;
        }
        if (Objects.isNull(dtoResult.getData())){
            return voResult;
        }

        //2封装VO
        List<ChannelPriceRecordOuterDTO> listDto = dtoResult.getData().getData();
        List<OperateLogRespVO> operateLogList = Optional.ofNullable(listDto).orElse(Lists.newArrayList()).stream().map(operateLog -> {
            OperateLogRespVO operateLogRespVO = new OperateLogRespVO();
            if (StringUtils.isNotBlank(operateLog.getLastModifier())){
                operateLogRespVO.setOperator(operateLog.getLastModifier());//变更人
            }
            if (Objects.nonNull(operateLog.getGmtModified())){
                operateLogRespVO.setCreateTime(operateLog.getGmtModified());//变更时间
            }
            if (StringUtils.isNotBlank(operateLog.getModifiedType())){
                operateLogRespVO.setOperateType(operateLog.getModifiedType());//变更类型
            }
            if (StringUtils.isNotBlank(operateLog.getTrend())){
                operateLogRespVO.setTrend(operateLog.getTrend());//变更趋势：上升，持平，下降
            }
            //变更影响
            List<ModifiedDetailDTO> modifiedDetailDTOList = operateLog.getModifiedDetail();
            List<ModifiedDetailVO> modifiedDetailVOList = Optional.ofNullable(modifiedDetailDTOList).orElse(Lists.newArrayList()).stream().map(modifiedDetailDTO ->{
                ModifiedDetailVO modifiedDetailVO = new ModifiedDetailVO();
                if (StringUtils.isNotBlank(modifiedDetailDTO.getModifiedType())){
                    switch (modifiedDetailDTO.getModifiedType()){
                        case "SUPPLY_PRICE":
                            if (StringUtils.isNotBlank(modifiedDetailDTO.getFrom())&&StringUtils.isNotBlank(modifiedDetailDTO.getTo())){
                                modifiedDetailVO.setFrom("$"+modifiedDetailDTO.getFrom());
                                modifiedDetailVO.setTo("$"+modifiedDetailDTO.getTo());
                            }
                            break;
                        case "GROSS_PRICE":
                            if (StringUtils.isNotBlank(modifiedDetailDTO.getFrom())&&StringUtils.isNotBlank(modifiedDetailDTO.getTo())){
                                modifiedDetailVO.setFrom(modifiedDetailDTO.getFrom()+"%");
                                modifiedDetailVO.setTo(modifiedDetailDTO.getTo()+"%");
                            }
                            break;
                        case "WAREHOUSE_PRICE":
                            if (StringUtils.isNotBlank(modifiedDetailDTO.getFrom())&&StringUtils.isNotBlank(modifiedDetailDTO.getTo())){
                                modifiedDetailVO.setFrom("¥"+modifiedDetailDTO.getFrom());
                                modifiedDetailVO.setTo("¥"+modifiedDetailDTO.getTo());
                            }
                            break;
                        default:
                            if (StringUtils.isNotBlank(modifiedDetailDTO.getFrom())&&StringUtils.isNotBlank(modifiedDetailDTO.getTo())){
                                modifiedDetailVO.setFrom(modifiedDetailDTO.getFrom());
                                modifiedDetailVO.setTo(modifiedDetailDTO.getTo());
                            }
                            break;
                    }
                }
                modifiedDetailVO.setType(modifiedDetailDTO.getModifiedType());
                return modifiedDetailVO;
            }).collect(Collectors.toList());
            operateLogRespVO.setOperateModifies(modifiedDetailVOList);
            return operateLogRespVO;
        }).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(operateLogList)){
            voResult.setData(operateLogList);
        }
        if (Objects.nonNull(dtoResult.getData().getTotal())){
            voResult.setTotal(dtoResult.getData().getTotal());
        }
        return voResult;
    }
    public PageResultResp<OperateLogRespVO> queryProductLogListTest(ChannelPriceLogParam param) {
        PageResultResp<OperateLogRespVO> voResult = new PageResultResp<>();

        //调用接口
        ModifiedDetailDTO modifiedDetailDTO = new ModifiedDetailDTO();
        modifiedDetailDTO.setModifiedType("仓库成本");
        modifiedDetailDTO.setFrom("1");
        modifiedDetailDTO.setTo("2");



        ChannelPriceRecordOuterDTO outerd = new ChannelPriceRecordOuterDTO();
        outerd.setId(0L);
        outerd.setLastModifier("dd");
        outerd.setGmtModified(new Date());
        outerd.setModifiedType("修改毛利率");
        // outerd.setModifiedDetail(modifiedDetailDTO);
        outerd.setTrend("持平");

        PageResult<ChannelPriceRecordOuterDTO> pageResult = new PageResult<>();
        pageResult.setData(Lists.newArrayList(outerd));
        pageResult.setTotal(1);

        DcBaseResult<PageResult<ChannelPriceRecordOuterDTO>> dtoBaseResult = new DcBaseResult<>();
        dtoBaseResult.setData(pageResult);
        dtoBaseResult.setErrorCode("");
        dtoBaseResult.setErrorDesc("");
        dtoBaseResult.setSuccess(true);
        dtoBaseResult.setCanRetry(true);




        //封装VO
        List<ChannelPriceRecordOuterDTO> listDto = dtoBaseResult.getData().getData();
        List<OperateLogRespVO> operateLogList = Optional.ofNullable(listDto).orElse(Lists.newArrayList()).stream().map(x -> {
            OperateLogRespVO operateLogRespVO = new OperateLogRespVO();
            operateLogRespVO.setOperator(x.getLastModifier());//操作人
            operateLogRespVO.setCreateTime(x.getGmtModified());//变更时间
            operateLogRespVO.setOperateType(x.getModifiedType());//操作类型
            operateLogRespVO.setTrend(x.getTrend());
            //变更影响
            // ModifiedDetailDTO modifiedDetail = x.getModifiedDetail();
            // if (Objects.nonNull(modifiedDetail)) {
            //     ModifiedDetailVO modifyVO = new ModifiedDetailVO();
            //     modifyVO.setFrom(modifiedDetail.getFrom());
            //     modifyVO.setTo(modifiedDetail.getTo());
            //     modifyVO.setType(modifiedDetail.getModifiedType());
            //     operateLogRespVO.setOperateModifies(Lists.newArrayList(modifyVO));
            // }
            return operateLogRespVO;
        }).collect(Collectors.toList());

        voResult.setData(operateLogList);
        voResult.setTotal(dtoBaseResult.getData().getTotal());
        return voResult;
    }


    // public static void main(String[] args) {
    //     ChannelPriceReadAdapter adapter = new ChannelPriceReadAdapter();
    //     ChannelPriceLogParam param = new ChannelPriceLogParam();
    //
    //     PageResultResp<OperateLogRespVO> resp = adapter.queryProductLogListTest(param);
    //     System.out.println(resp);
    // }
}
