package com.alibaba.cbuscm.service;

import java.util.Arrays;

import javax.annotation.Resource;

import com.alibaba.cbuscm.common.PageResult;
import com.alibaba.cbuscm.contants.DownPlatformContants;
import com.alibaba.cbuscm.params.AEProductDelParam;
import com.alibaba.china.global.business.library.enums.SelectionBlacklistTargetTypeEnum;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionBlacklistWriteService;
import com.alibaba.china.global.business.library.interfaces.tag.ItemTagService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by duanyang.zdy on 2019/4/22.
 */
@Service
@Slf4j
public class AEProductDeleteService {

    @Resource
    private ItemTagService itemTagService;

    @Resource
    private SelectionBlacklistWriteService selectionBlacklistWriteService;

    public PageResult<Boolean> delete(AEProductDelParam param){

        try {
            //移除标签
            itemTagService.removeTag(param.getDownItemId(), Arrays.asList(param.getSearchTagId()));

            //记录黑名单
            selectionBlacklistWriteService.addTargetKey(param.getTaskId(), SelectionBlacklistTargetTypeEnum.ITEM_RELATION.getTargetType(),param.getDownItemId()+ DownPlatformContants.JOIN+param.getOfferId(),false);

            return PageResult.done(true);

        }catch (Exception e){

            log.error("call delete error",e);

            return PageResult.fail("call delete error");
        }




    }
}
