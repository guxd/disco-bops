package com.alibaba.cbuscm.service.authority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.core.config.api.DcContextService;
import com.alibaba.cbu.disco.shared.core.config.model.DcContext;
import com.alibaba.cbu.disco.shared.core.product.constant.DcProductBizTypeEnum;
import com.alibaba.cbuscm.service.authority.model.DcBizTypeInfo;
import com.alibaba.cbuscm.service.authority.model.DcChannelInfo;
import com.alibaba.cbuscm.service.product.ProductConverter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 类ProductDataAccessService
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Service
@Slf4j
public class ProductDataAccessService {

    @Autowired
    private AclDataPermissionLogic aclDataPermissionLogic;

    @Autowired
    private DcContextService dcContextService;

    public ListOf<DcChannelInfo> queryUserAuthorizedChannel(Integer userId) {
        try {
            List<DcContext.Channel> channels = dcContextService.getAllChannel();
            List<DcContext.BizType> bizTypes = dcContextService.getAllBizType();
            List<String> channelList = channels.stream().map(DcContext.Channel::getName).collect(Collectors.toList());
            ListOf<String> accessResult = aclDataPermissionLogic.checkDataPermissionAccess(userId,
                AclDataPermissionLogic.PERMISSION_NAME_CHANNEL, AclDataPermissionLogic.OPERATION_NAME_DATA_ACCESS,
                AclDataPermissionLogic.DATA_MODEL_CHANNEL, channelList);
            return convertChannel(accessResult, channels, bizTypes);
        } catch (Exception e) {
            log.error("DcAuthorityCheckService.queryUserAuthorizedChannel", e);
            return ListOf.error("system-error", e.getMessage());
        }
    }

    public ListOf<DcBizTypeInfo> queryUserAuthorizedBizType(Integer userId) {
        try {
            List<String> bizTypeList = Arrays.stream(DcProductBizTypeEnum.values())
                .map(DcProductBizTypeEnum::name)
                .collect(Collectors.toList());
            ListOf<String> accessResult = aclDataPermissionLogic.checkDataPermissionAccess(userId,
                AclDataPermissionLogic.PERMISSION_NAME_BIZ_TYPE, AclDataPermissionLogic.OPERATION_NAME_DATA_ACCESS,
                AclDataPermissionLogic.DATA_MODEL_BIZ_TYPE, bizTypeList);
            return convertBizType(accessResult);
        } catch (Exception e) {
            log.error("DcAuthorityCheckService.queryUserAuthorizedBizType", e);
            return ListOf.error("system-error", e.getMessage());
        }
    }

    public List<String> queryUserAuthorizedOriginBizType(Integer userId) {
        try {
            List<String> bizTypeList = Arrays.stream(DcProductBizTypeEnum.values())
                .map(DcProductBizTypeEnum::name)
                .collect(Collectors.toList());
            ListOf<String> accessResult = aclDataPermissionLogic.checkDataPermissionAccess(userId,
                AclDataPermissionLogic.PERMISSION_NAME_BIZ_TYPE, AclDataPermissionLogic.OPERATION_NAME_DATA_ACCESS,
                AclDataPermissionLogic.DATA_MODEL_BIZ_TYPE, bizTypeList);
            return filterBizType(accessResult, bizTypeList);
        } catch (Exception e) {
            log.error("DcAuthorityCheckService.queryUserAuthorizedBizType", e);
            return new ArrayList<>();
        }
    }

    private List<String> filterBizType(ListOf<String> accessResult, List<String> bizTypes) {
        if (!ListOf.isNonEmpty(accessResult)) {
            return new ArrayList<>();
        }
        List<String> bizTypeList = new ArrayList<>();
        bizTypes.forEach(bizType -> {
            for (String biz : accessResult.getData()) {
                if (bizType.equals(biz)) {
                    bizTypeList.add(bizType);
                }
            }
        });
        return bizTypeList;
    }

    private ListOf<DcBizTypeInfo> convertBizType(ListOf<String> accessResult) {
        if (!ListOf.isValid(accessResult)) {
            return ListOf.error(accessResult.getErrorCode(), accessResult.getErrorMessage());
        }
        List<DcBizTypeInfo> bizTypeList = new ArrayList<>();
        if (accessResult.getData().isEmpty()) {
            return ListOf.general(bizTypeList);
        }
        for (String bizType : accessResult.getData()) {
            DcBizTypeInfo info = new DcBizTypeInfo();
            info.setBizType(bizType);
            bizTypeList.add(info);
        }
        return ListOf.general(bizTypeList);
    }

    private ListOf<DcChannelInfo> convertChannel(ListOf<String> accessResult, List<DcContext.Channel> channels,
                                                 List<DcContext.BizType> bizTypes) {
        if (!ListOf.isNonEmpty(accessResult)) {
            return ListOf.error(accessResult.getErrorCode(), accessResult.getErrorMessage());
        }
        List<DcChannelInfo> channelList = new ArrayList<>();
        for (String channel : accessResult.getData()) {
            DcChannelInfo info = new DcChannelInfo();
            info.setChannel(channel);
            info.setBizType(channels.stream().filter(b -> channel.equals(b.getName())).findFirst()
                .map(DcContext.Channel::getBizTypeName).orElse(null));
            info.setPriceCurrency(bizTypes.stream().filter(b -> info.getBizType().equals(b.getName())).findFirst()
                .map(DcContext.BizType::getCurrency).map(ProductConverter::convertPriceCurrency)
                .orElse(null));
            channelList.add(info);
        }
        return ListOf.general(channelList);
    }
}
