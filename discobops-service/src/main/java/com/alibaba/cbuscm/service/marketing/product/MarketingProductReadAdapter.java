package com.alibaba.cbuscm.service.marketing.product;

import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcItemEnrollDTO;
import com.alibaba.cbu.panama.dc.client.constants.OperateTypeEnum;
import com.alibaba.cbu.panama.dc.client.constants.marketingproduct.MarketingProductStatusEnum;
import com.alibaba.cbu.panama.dc.client.model.DcBaseResult;
import com.alibaba.cbu.panama.dc.client.model.PageResult;
import com.alibaba.cbu.panama.dc.client.model.common.AttrModel;
import com.alibaba.cbu.panama.dc.client.model.common.BizConstantDefDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanModel;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MaterialDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.OperateCodeDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingproduct.*;
import com.alibaba.cbu.panama.dc.client.model.param.marketingplan.MarketingPlanPageParam;
import com.alibaba.cbu.panama.dc.client.model.param.marketingproduct.MarketingProductPageParam;
import com.alibaba.cbu.panama.dc.client.service.marketingplan.MarketingPlanReadService;
import com.alibaba.cbu.panama.dc.client.service.marketingproduct.MarketingProductReadService;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.enums.SuccessFailEnum;
import com.alibaba.cbuscm.log.constant.LogConstant;
import com.alibaba.cbuscm.service.marketing.DcEnrollReadServiceAdapter;
import com.alibaba.cbuscm.service.marketing.converter.MarketingProductConvertDTOVO;
import com.alibaba.cbuscm.service.marketing.plan.MarketingPlanReadAdapter;
import com.alibaba.cbuscm.utils.MoneyUtils;
import com.alibaba.cbuscm.vo.marketing.param.MarketProductListReq;
import com.alibaba.cbuscm.vo.marketing.param.MarketingSkuVO;
import com.alibaba.cbuscm.vo.marketing.param.MaterialVO;
import com.alibaba.cbuscm.vo.marketing.plan.MarketingPlanEnrolledRespVO;
import com.alibaba.cbuscm.vo.marketing.product.*;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.member.service.models.MemberModel;
import com.alibaba.china.offer.api.domain.extensions.Sku;
import com.alibaba.china.offer.api.domain.extensions.SkuModel;
import com.alibaba.china.offer.api.domain.extensions.TradeAttributes;
import com.alibaba.china.offer.api.query.model.OfferModel;
import com.alibaba.china.offer.api.query.service.OfferQueryService;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
/**
 * @Auther: gxd
 * @Date: 2019/10/8 15:25
 * @Description:
 */
@Slf4j
@Component
public class MarketingProductReadAdapter {

    private final static Logger logger = LoggerFactory.getLogger(MarketingProductReadAdapter.class);

    @Autowired
    private MarketingPlanReadAdapter marketingProductReadAdapter;

    @Autowired
    private MarketingProductReadService marketingProductReadService;

    @Autowired
    private MemberReadService memberReadService;

    @Autowired
    private MarketingPlanReadService marketingPlanReadService;

    @Autowired
    private OfferQueryService<OfferModel> offerQueryService;

    @Autowired
    private DcEnrollReadServiceAdapter dcEnrollReadServiceAdapter;

    public List<ProductStatusNumRespVO> statistics(Long marketingPlanId) {

        List<ProductStatusNumRespVO> list = new ArrayList<>();
        MarketingProductStatusEnum[] values = MarketingProductStatusEnum.values();

        for (MarketingProductStatusEnum value : values) {
            ProductStatusNumRespVO vo = new ProductStatusNumRespVO();
            vo.setProductStatus(value.name());
            vo.setProductStatusMessage(value.getDesc());
            list.add(vo);
        }
        return list;
//        try {
//            DcBaseResult<List<ProductStatusStatisticsDTO>> result = marketingProductReadService.statistics(marketingPlanId);
//            if (!result.isSuccess()) {
//                log.error(String.format("状态统计失败,marketingPlanId=%s,errCode:%s",marketingPlanId,result.getErrorDesc()));
//            }
//
//
//
//            return getProductStatusNumRespVOS(result);
//        } catch (Exception cre) {
//            log.error(String.format("状态统计失败,marketingPlanId=%s,error:%s",marketingPlanId,cre.getMessage()));
//            throw cre;
//        }
    }


    public List<BizConstantDefDTO> listAllRejectReason(){
        try{
            DcBaseResult<List<BizConstantDefDTO>> result = marketingProductReadService.listAllRejectReason();
            if(!result.isSuccess()){
                BopsLogUtil.logServiceFailed("marketingProductReadService","listAllRejectReason",
                        "",0L,LogConstant.LogType.SAL,new Exception(result.getErrorDesc()));
            }
            return result.getData();
        }catch(Exception cre){
            BopsLogUtil.logServiceFailed("marketingProductReadService","listAllRejectReason",
                    "",0L,LogConstant.LogType.SAL,cre);
            return null;
        }
    }

//    private List<ProductStatusNumRespVO> getProductStatusNumRespVOS(DcBaseResult<List<ProductStatusStatisticsDTO>> result) {
////        List<ProductStatusNumRespVO> list = new ArrayList<>();
////        MarketingProductStatusEnum[] values = MarketingProductStatusEnum.values();
////
////        for (MarketingProductStatusEnum value : values) {
////            ProductStatusNumRespVO vo = new ProductStatusNumRespVO();
////            vo.setProductStatus(value.name());
////            vo.setProductStatusMessage(value.getDesc());
////            list.add(vo);
////        }
//
////        List<ProductStatusNumRespVO> list = Optional.ofNullable(result.getData()).orElse(new ArrayList<>()).stream()
////                .map(x -> {
////                    ProductStatusNumRespVO vo = new ProductStatusNumRespVO();
////                    vo.setProductStatus(MarketingProductStatusEnum.valueOf(x.getStatus()).name());
////                    vo.setNum(x.getNum());
////                    vo.setProductStatusMessage(MarketingProductStatusEnum.valueOf(x.getStatus()).getDesc());
////                    return vo;
////                }).collect(Collectors.toList());
////
////        List<String> remainList = list.stream().map(ProductStatusNumRespVO::getProductStatus).collect(Collectors.toList());
////        List<String> allEnumDesc = Arrays.stream(MarketingProductStatusEnum.values()).map(MarketingProductStatusEnum::getDesc).collect(Collectors.toList());
////
////        for (String s : allEnumDesc) {
////            if (!remainList.contains(s)) {
////                list.add(new ProductStatusNumRespVO(s, 0,null));
////            }
////        }
////        return list;
//    }


    public PageResult<MarketingProductListRespVO> marketingProductList(MarketingProductPageParam param) {

        try {
            DcBaseResult<PageResult<MarketingProductDTO>> result = marketingProductReadService.pageMarketingProductByParam(param);
            if (!result.isSuccess()) {
                BopsLogUtil.logServiceFailed("marketingProductReadService","pageMarketingProductByParam",JSON.toJSONString(param),
                        0L, LogConstant.LogType.SAL,new Exception(result.getErrorDesc()));
            }
            PageResult<MarketingProductListRespVO> modelPageResult = new PageResult<>();

            PageResult<MarketingProductDTO> dtoPageResult = result.getData();
            if (Objects.nonNull(dtoPageResult) && CollectionUtils.isNotEmpty(dtoPageResult.getData())) {

                List<MarketingProductListRespVO> list = Lists.newArrayList();
                for (MarketingProductDTO marketingProductDTO : dtoPageResult.getData()) {
                    MarketingProductListRespVO vo = MarketingProductConvertDTOVO.getMarketingProductListRespVO(marketingProductDTO);
                    vo.setLoginId(queryLoginIdMemberByUserId(marketingProductDTO.getSupplierId()));
                    vo.setNickName(marketingProductDTO.getPlanReviewer());
                    list.add(vo);
                }

                BopsLogUtil.logServiceSuccess("marketingProductReadService","pageMarketingProductByParam",JSON.toJSONString(param),"",
                        0L, LogConstant.LogType.SERVICE);

                List<Long> marketingPlanIds = list.stream().map(MarketingProductListRespVO::getMarketingPlanId).collect(Collectors.toList());
                MarketingPlanPageParam pageParam = new MarketingPlanPageParam();
                pageParam.setPageNo(0);
                pageParam.setPageSize(dtoPageResult.getData().size());
                pageParam.setIdList(marketingPlanIds);

                List<MarketingPlanDTO> marketingPlanDTOList = marketingProductReadAdapter.listMarketingPlanByParam(pageParam);
                Map<Long, MarketingPlanDTO> marketingPlanDTOMap = marketingPlanDTOList.stream().collect(Collectors.toMap(MarketingPlanDTO::getId, Function.identity()));

                for (MarketingProductListRespVO vo : list) {
                    Long marketingPlanId = vo.getMarketingPlanId();
                    MarketingPlanDTO marketingPlanDTO = marketingPlanDTOMap.get(marketingPlanId);
                    vo.setMarketingPlanName(marketingPlanDTO.getMarketingPlanName());
//                    vo.setEnrollStartTime(marketingPlanDTO.getEnrollStartTime());
//                    vo.setEnrollEndTime(marketingPlanDTO.getEnrollEndTime());
                    vo.setGmtCreate(marketingPlanDTO.getGmtCreate());
                    vo.setEffectiveStartTime(marketingPlanDTO.getEffectiveStartTime());
                    vo.setEffectiveEndTime(marketingPlanDTO.getEffectiveEndTime());
                }
                modelPageResult.setData(list);
            }
            modelPageResult.setTotal(result.getData().getTotal());
            return modelPageResult;
        } catch(Exception cre){
            BopsLogUtil.logServiceFailed(this.getClass().getSimpleName(),"pageMarketingProductByParam",JSON.toJSONString(param),
                    0L, LogConstant.LogType.SAL,cre);
            throw cre;
        }
    }



    public SupplierRespVO payInfo(Long marketingProductId) {
        try {

            DcBaseResult<PaymentInfoDTO> result = marketingProductReadService.loadPaymentInfo(marketingProductId);

            if (!result.isSuccess()) {
                log.error(String.format("获取缴费信息失败,marketingProductId=%s,error:%s",marketingProductId,result.getErrorDesc()));
            }
            SupplierRespVO supplierRespVO = new SupplierRespVO();
            supplierRespVO.setSellerName(result.getData().getSellerNick());
            supplierRespVO.setMarketingPlanName(result.getData().getMarketingPlanName());
            supplierRespVO.setChargeParam(MoneyUtils.fenToYuan((long)result.getData().getFee()));
            return supplierRespVO;

        } catch (Exception cre) {
            log.error(String.format("获取缴费信息失败,marketingProductId=%s,error:%s",marketingProductId,cre.getMessage()));

            throw cre;
        }
    }

    /**
     * 查询活动商品详情
     * @param param
     * @return
     */
    public MarketingProductDetailRespVO enrollOfferDetail(MarketProductListReq param) {
        try {
            MarketingProductDetailRespVO vo = new MarketingProductDetailRespVO();

            //查询商品详情
            DcBaseResult<MarketingProductDTO> productResult = marketingProductReadService.queryMarketingProductById(param.getMarketingProductId());
            if (!productResult.isSuccess()) {
                BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","通过活动商品id查询详情：queryMarketingProductById",JSON.toJSONString(param.getMarketingProductId()),
                        0L, LogConstant.LogType.SAL,new Exception(productResult.getErrorDesc()));
            }
            MarketingProductDTO product = productResult.getData();
            if (Objects.isNull(product)){
                return null;
            }
            List<MarketingSkuDTO> marketingSkuList = product.getMarketingSkuList();
            List<MaterialDTO> materialList = product.getMaterialList();
            Long marketingPlanId = product.getMarketingPlanId();
            Long offerId = product.getOfferId();
            Long itemId = product.getItemId();

            //商家信息
            buildMemberModel(vo, product);
            //活动model
            MarketingPlanModel plan = buildMarketingPlanInfo(vo, product, marketingPlanId);
            //内贸的规格匹配情况
            buildOfferItemInfo(param, vo, offerId, itemId);


            //商品分页查询接口
            //该商品报名的其他活动
            MarketingProductPageParam pageParam = new MarketingProductPageParam();
            pageParam.setItemId(param.getItemId());
            DcBaseResult<PageResult<MarketingProductDTO>> pageResult = marketingProductReadService.pageMarketingProductByParam(pageParam);
            if (!pageResult.isSuccess()) {
                BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","pageMarketingProductByParam",JSON.toJSONString(pageParam),
                        0L, LogConstant.LogType.SAL,new Exception(pageResult.getErrorDesc()));
            }
            List<MarketingProductDTO> productList = pageResult.getData().getData();

            //封装其他活动VO
            List<MarketingPlanEnrolledRespVO> enrollVO = Optional.ofNullable(productList).orElse(Lists.newArrayList())
                    .stream().filter(x->!marketingPlanId.equals(x.getMarketingPlanId())).map(x -> {
                MarketingPlanEnrolledRespVO enrolledRespVO = new MarketingPlanEnrolledRespVO();
                enrolledRespVO.setMarketingPlanId(x.getMarketingPlanId());
                DcBaseResult<MarketingPlanModel> marketingPlanModelDcBaseResult = marketingPlanReadService.queryMarketingPlanModelById(x.getMarketingPlanId());
                MarketingPlanDTO marketingPlanDTO = marketingPlanModelDcBaseResult.getData().getMarketingPlan();
                enrolledRespVO.setMarketingPlanName(
                        marketingPlanDTO.getMarketingPlanName()
                );
                enrolledRespVO.setOperateCreateTime(x.getGmtCreate());
                enrolledRespVO.setParam(marketingPlanDTO.getChargeParam());
                enrolledRespVO.setStatus(x.getStatus());
                enrolledRespVO.setStatusMessage(MarketingProductStatusEnum.valueOf(x.getStatus()).getDesc());
                enrolledRespVO.setChargeType(marketingPlanDTO.getChargeType());
                return enrolledRespVO;
            }).collect(Collectors.toList());
            vo.setMarketingPlanEnrolledList(enrollVO);

            //审核记录
            List<String> operateList = new ArrayList<>();
            buildAuditEnumList(operateList);
            buildOperateLogVO(param, vo, operateList);
            // sku信息初始化以及回填
            buildSkuModel(vo, product, marketingSkuList, marketingPlanId, offerId, itemId);
            // 素材进行初始化以及回填
            convertMaterialVOList(vo, plan.getMaterialList(), materialList);
            return vo;
        } catch (Exception e) {
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","enrollOfferDetail",JSON.toJSONString(param),
                    0L, LogConstant.LogType.SAL,e);
            log.error("查询商品商品失败");
            return null;
        }
    }

    private void buildOperateLogVO(MarketProductListReq param, MarketingProductDetailRespVO vo, List<String> operateList) {
        DcBaseResult<List<OperateLogDTO>> operateLog = marketingProductReadService.getOperateLog(param.getMarketingProductId(), operateList);
        if (!operateLog.isSuccess()) {
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","getOperateLog",String.format("getMarketingProductId:%s",param.getMarketingProductId()),
                    0L, LogConstant.LogType.SAL,new Exception(operateLog.getErrorDesc()));
        }
        List<OperateLogRespVO> operateRespVO = Optional.ofNullable(operateLog.getData()).orElse(Lists.newArrayList()).stream().map(x -> {
            OperateLogRespVO operateVO = new OperateLogRespVO();
            //判断流程阶段
            operateVO.setStatus(x.getOperateType().split("_")[0].equals("FIRST") ? "FIRST_AUDIT":"SECORD_AUDIT");
            operateVO.setStatusMessage(OperateTypeEnum.valueOf(x.getOperateType()).getDesc().substring(0,2));
            //操作类型
            operateVO.setOperateType(SuccessFailEnum.map.get(x.getOperateType()).getDesc());
            //操作人
            operateVO.setOperator(x.getOperator());
            operateVO.setCreateTime(x.getGmtCreate());
            operateVO.setRemark(x.getRemark());
            return operateVO;
        }).collect(Collectors.toList());
        vo.setOperateLogRespVO(operateRespVO);
    }

    private void buildOfferItemInfo(MarketProductListReq param, MarketingProductDetailRespVO vo, Long offerId, Long itemId) {
        DcItemEnrollDTO dcOfferItemEnroll = dcEnrollReadServiceAdapter.queryOfferEnrollDTO(param.getItemId());
        if (dcOfferItemEnroll == null){
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","queryOfferEnrollDTO",String.format("itemId=%s",param.getItemId()),
                    0L, LogConstant.LogType.SAL,new Exception("参数为空"));
        }
        try {
            if(dcOfferItemEnroll != null) {
                vo.setItemTitle(dcOfferItemEnroll.getItemTitle());
                vo.setItemImg(dcOfferItemEnroll.getItemPicUrl());
            }
            vo.setItemId(itemId);
            vo.setOfferId(offerId);
        } catch (Exception e) {
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","buildOfferItemInfo",JSON.toJSONString(""),
                    0L, LogConstant.LogType.SAL,e);
        }
    }

    private MarketingPlanModel buildMarketingPlanInfo(MarketingProductDetailRespVO vo, MarketingProductDTO product, Long marketingPlanId) {
        DcBaseResult<MarketingPlanModel> dcBaseResult = marketingPlanReadService.queryMarketingPlanModelByIdWiihoutItem(marketingPlanId);
        if (!dcBaseResult.isSuccess()) {
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","按照活动id加载活动model:queryMarketingPlanModelById",String.format("marketingPlanId:%s",marketingPlanId),
                    0L, LogConstant.LogType.SAL,new Exception(dcBaseResult.getErrorDesc()));
        }
        MarketingPlanModel plan = dcBaseResult.getData();
        //封装活动相关信息
        try {
            vo.setMarketingPlanName(plan.getMarketingPlan().getMarketingPlanName());
            vo.setEnrollEndTime(plan.getMarketingPlan().getEnrollEndTime());
            vo.setEnrollStartTime(plan.getMarketingPlan().getEnrollStartTime());
            vo.setEffectiveEndTime(plan.getMarketingPlan().getEffectiveEndTime());
            vo.setEffectiveStartTime(plan.getMarketingPlan().getEffectiveStartTime());
            vo.setStatus(product.getStatus());
            vo.setStatusMessage(MarketingProductStatusEnum.valueOf(product.getStatus()).getDesc());//活动状态信息
            vo.setChargeType(plan.getMarketingPlan().getChargeType());//收费类型
            vo.setParam(plan.getMarketingPlan().getChargeParam());
            vo.setMarketingPlanId(product.getMarketingPlanId());
            vo.setGmtCreate(product.getGmtCreate());
            if(Objects.nonNull(product.getMarketingProductSalesDTO())){
                vo.setGuaranteedSales(product.getMarketingProductSalesDTO().getGuaranteedSales());//保底销量
                vo.setExpectedSales(product.getMarketingProductSalesDTO().getExpectedSales());//预期销量
            }
        } catch (IllegalArgumentException e) {
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","buildMarketingPlanInfo",JSON.toJSONString(product),
                    0L, LogConstant.LogType.SAL,e);
        }
        return plan;
    }

    private void buildSkuModel(MarketingProductDetailRespVO vo, MarketingProductDTO product, List<MarketingSkuDTO> marketingSkuList, Long marketingPlanId, Long offerId, Long itemId) {
        if (null != marketingPlanId && marketingPlanId > 0) {
            DcBaseResult<EligibleProductModel> dcBaseResultSku = null;
            try {
                dcBaseResultSku  = marketingProductReadService.listAllEligibleProductSkuBySupplier(marketingPlanId,
                        product.getSupplierId(), offerId, itemId);
            } catch (Exception e) {
                BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","listAllEligibleProductSkuBySupplier",String.format("marketingPlanId=%s,supplierId=%s,offerId=%s,itemId=%s",marketingPlanId,product.getSupplierId(), offerId, itemId),
                        0L, LogConstant.LogType.SAL,e);
            }
            if (null != dcBaseResultSku && null != dcBaseResultSku.getData()) {
                EligibleProductModel eligibleProductModel = dcBaseResultSku.getData();
                if(eligibleProductModel != null) {
                    convertMarketSkuVOList(vo, eligibleProductModel, marketingSkuList,
                            this.getOfferSkuIdNameMap(offerId));
                    if(eligibleProductModel.getEligibleProduct() != null){
                        vo.setItemTitle(eligibleProductModel.getEligibleProduct().getItemTitle());
                        vo.setItemImg(eligibleProductModel.getEligibleProduct().getItemPicUrl());
                    }
                }
            }
        }
    }

    private void buildMemberModel(MarketingProductDetailRespVO vo, MarketingProductDTO product) {
        try {
            MemberModel memberModel = memberReadService.findMemberByUserId(product.getSupplierId());
            if (Objects.nonNull(memberModel)){
                SupplierRespVO supplierRespVO = new SupplierRespVO();
                supplierRespVO.setLoginId(memberModel.getLoginId());
                supplierRespVO.setMemberId(memberModel.getMemberId());
                supplierRespVO.setCompanyName(memberModel.getCompanyName());
                vo.setSupplierRespVO(supplierRespVO);
                vo.setLoginId(memberModel.getLoginId());
                vo.setMemberId(memberModel.getMemberId());
            }
        } catch (Exception e) {
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","findMemberByUserId",String.format("supplierId:%s",product.getSupplierId()),
                    0L, LogConstant.LogType.SAL,e);
        }
    }

    private void buildAuditEnumList(List<String> operateList) {
        operateList.add(OperateTypeEnum.FIRST_AUDIT_SUCESS.name());
        operateList.add(OperateTypeEnum.FIRST_AUDIT_FAIL.name());
        operateList.add(OperateTypeEnum.SECORD_AUDIT_SUCESS.name());
        operateList.add(OperateTypeEnum.SECORD_AUDIT_FAIL.name());
    }

    private void buildOfferItemInfo(MarketingProductDetailRespVO vo, Long offerId, Long itemId, DcItemEnrollDTO dcOfferItemEnroll) {

    }

    private void buildMarketingPlanInfo(MarketingProductDetailRespVO vo, MarketingProductDTO product, MarketingPlanModel plan) {

    }


    public Long queryUserIdByLoginId(String loginId){
        try{
            MemberModel result = memberReadService.findMemberByLoginId(loginId);
            return result.getUserId();
        }catch (Exception cre){
            log.error(String.format("通过loginId获取UserId失败,loginId=%s,error:%s",loginId,cre.getMessage()));
            throw cre;
        }
    }


    public String queryLoginIdMemberByUserId(Long userId){

        try{
            MemberModel result = memberReadService.findMemberByUserId(userId);
            return result.getLoginId();
        }catch (Exception cre){
            log.error(String.format("通过userId获取loginId失败,userId=%s,error:%s",userId,cre.getMessage()));
            throw cre;
        }
    }

    public String queryMemberIdByUserId(Long userId){

        try{
            MemberModel result = memberReadService.findMemberByUserId(userId);
            return result.getMemberId();
        }catch (Exception cre){
            log.error(String.format("通过userId获取memberId失败,userId=%s,error:%s",userId,cre.getMessage()));
            throw cre;
        }
    }


    public PaymentRespVO getPayment(Long marketingProductId){

        PaymentRespVO respVO = new PaymentRespVO();
        try{
            DcBaseResult<MarketingProductDTO> result = marketingProductReadService.queryMarketingProductById(marketingProductId);
            if(!result.isSuccess()){
                log.error(String.format("通过marketingProductId查询活动商品失败,marketingProductId=%s,error:%s",marketingProductId,result.getErrorDesc()));
            }
            MarketingProductDTO marketingProductDTO = result.getData();

            DcBaseResult<MarketingPlanModel> marketingPlanModelDcBaseResult = marketingPlanReadService.queryMarketingPlanModelById(marketingProductDTO.getMarketingPlanId());
            if(!marketingPlanModelDcBaseResult.isSuccess()){
                log.error(String.format("通过marketingPlanId查询活动商品失败,marketingPlanId=%s,error:%s",marketingProductId,result.getErrorDesc()));
            }
            MarketingPlanDTO marketingPlanDTO = marketingPlanModelDcBaseResult.getData().getMarketingPlan();
            respVO.setMarketingPlanName(marketingPlanDTO.getMarketingPlanName());
            respVO.setChargeFee(marketingPlanDTO.getChargeParam());
            respVO.setFee(Objects.nonNull(marketingProductDTO.getFee())?MoneyUtils.fenToYuan((long)marketingProductDTO.getFee()):"0");
            Long userId = marketingProductDTO.getSupplierId();

            MemberModel memberModel = memberReadService.findMemberByUserId(userId);

            respVO.setLoginId(memberModel.getLoginId());
            respVO.setSupplierShopName(memberModel.getCompanyName());
            return respVO;
        }catch (Exception cre){
            log.error(String.format("通过marketingProductId获取缴费配置信息失败,marketingProductId=%s,error:%s",marketingProductId,cre.getMessage()));
            throw cre;
        }

    }


    /**
     * 按照状态列举所有可能的操作码
     * @return
     */
    public Map<String, List<OperateCodeDTO>> listAvailableOperate(String pageCode,List<String> statusList){
        try{
            DcBaseResult<Map<String, List<OperateCodeDTO>>> result = marketingProductReadService.listAvailableOperate(pageCode,statusList);

            if(!result.isSuccess()){
                log.error(String.format("获取操作码为空,pageCode=%s,statusList=%s,error:%s",pageCode,JSON.toJSONString(statusList),result.getErrorDesc()));
            }
            return result.getData();
        }catch (Exception cre){
            log.error(String.format("获取操作码为空,pageCode=%s,statusList=%s,error:%s",pageCode,JSON.toJSONString(statusList),cre.getMessage()));
            throw cre;
        }
    }



    /**
     * 封装活动详情资质VO
     *
     * @param vo
     * @param productSkuList 提报信息中的sku列表
     */
    private void convertMarketSkuVOList(MarketingProductDetailRespVO vo, EligibleProductModel eligibleProductModel,
                                        List<MarketingSkuDTO> productSkuList, Map<Long, String> offerSkuMap) {
        List<MarketingSkuVO> marketingSkuList = Lists.newArrayList();
        if (eligibleProductModel == null) {
            return;
        }

        vo.setCurrentRate(eligibleProductModel.getCurrentRate());
        vo.setMarketingProcessingRate(eligibleProductModel.getMarketingProcessingRate());
        vo.setAfterMarketingRate(eligibleProductModel.getAfterMarketingRate());
        List<EligibleProductSkuDTO> list = eligibleProductModel.getEligibleProductSkuList();
        for (EligibleProductSkuDTO eligibleProductSkuDTO : list) {
            MarketingSkuVO marketingSkuVO = new MarketingSkuVO();
            if (CollectionUtils.isNotEmpty(productSkuList)) {
                // 回填已经提报信息
                for (MarketingSkuDTO marketingSkuDTO : productSkuList) {
                    if (null != eligibleProductSkuDTO.getItemSkuId() && eligibleProductSkuDTO.getItemSkuId().equals(marketingSkuDTO.getItemSkuId())
                            && null != eligibleProductSkuDTO.getOfferSkuId() && eligibleProductSkuDTO.getOfferSkuId().equals(marketingSkuDTO.getOfferSkuId())) {
                        marketingSkuVO.setMainSku(marketingSkuDTO.getMainSku());
                        marketingSkuVO.setMarketingStock(marketingSkuDTO.getMarketingStock());
                    }
                }
            }
            marketingSkuVO.setOfferSkuId(eligibleProductSkuDTO.getOfferSkuId());
            marketingSkuVO.setItemSkuId(eligibleProductSkuDTO.getItemSkuId());
            marketingSkuVO.setInventory(eligibleProductSkuDTO.getItemSkuInventory());
            marketingSkuVO.setRetailPrice(eligibleProductSkuDTO.getTaoRetailPrice());
            marketingSkuVO.setSupplyPrice(eligibleProductSkuDTO.getDcSupplyPrice());
            marketingSkuVO.setMinInvForecast(eligibleProductSkuDTO.getMinInvForecast());
            marketingSkuVO.setMaxInvForecast(eligibleProductSkuDTO.getMaxInvForecast());
            if(MapUtils.isNotEmpty(offerSkuMap)){
                for (Map.Entry<Long, String> entry : offerSkuMap.entrySet()) {
                    if (entry.getKey().equals(eligibleProductSkuDTO.getOfferSkuId())) {
                        marketingSkuVO.setOfferSkuName(offerSkuMap.get(entry.getKey()));
                    }
                }
            }
            if(CollectionUtils.isNotEmpty(eligibleProductSkuDTO.getItemAttrList())){
                // sku属性拼接成skuName
                marketingSkuVO.setItemSkuName(eligibleProductSkuDTO.getItemAttrList().stream().map(AttrModel::getValueText).collect(Collectors.joining("")));
            }
            marketingSkuList.add(marketingSkuVO);
        }
        vo.setMarketingProductSKUVOList(marketingSkuList);
    }

    private Map<Long, String> getOfferSkuIdNameMap(Long offerId) {
        Map<Long, String> skuIdNameMap = new HashMap<Long, String>();
        OfferModel offerModel;
        try {
            offerModel = (OfferModel) offerQueryService.findAllWithoutDetail(offerId, null);
            skuIdNameMap = this.getOfferSkuIdNameMap(offerModel);
        } catch (Exception e) {
            logger.warn("getSkuIdNameMap exception:", e);
        }
        return skuIdNameMap;
    }

    private Map<Long, String> getOfferSkuIdNameMap(OfferModel offerModel) {
        if (offerModel == null) {
            return null;
        }
        Map<Long, String> skuIdNameMap = new HashMap<Long, String>();
        SkuModel skuModel = offerModel.getSkuModel();
        Map<String, Sku> skuMap = skuModel.getSkus();
        for (Map.Entry<String, Sku> entry : skuMap.entrySet()) {
            TradeAttributes sku = entry.getValue().getTradeAttributes();
            Long skuId = sku.getSkuId();
            Map<Long, String> skuDetailMap = sku.getAttributes();
            String skuInformation = null;
            for (Map.Entry<Long, String> entry2 : skuDetailMap.entrySet()) {
                if (skuInformation == null) {
                    skuInformation = entry2.getValue();
                } else {
                    skuInformation = skuInformation + "," + entry2.getValue();
                }
            }
            skuIdNameMap.put(skuId, skuInformation);
        }
        return skuIdNameMap;
    }

    /**
     * 封装素材信息
     *
     * @param vo
     * @param list                活动详情中的素材模板列表
     * @param productMaterialList 提报信息中的素材列表
     */
    private void convertMaterialVOList(MarketingProductDetailRespVO vo, List<MaterialDTO> list,
                                       List<MaterialDTO> productMaterialList) {
        try {
            List<MaterialVO> materials = Lists.newArrayList();
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            for (MaterialDTO materialDTO : list) {
                MaterialVO materialVO = new MaterialVO();
                materialVO.setName(materialDTO.getName());
                materialVO.setCode(materialDTO.getCode());
                materialVO.setDesc(materialDTO.getDesc());
                if (CollectionUtils.isNotEmpty(productMaterialList)) {
                    // 回填已经提报信息
                    for (MaterialDTO productMaterialDTO : productMaterialList) {
                        if (StringUtils.equals(productMaterialDTO.getCode(), materialDTO.getCode())) {
                            materialVO.setValue(productMaterialDTO.getParam());
                        }
                    }
                }
                materials.add(materialVO);
            }
            vo.setMaterialVO(materials);
        } catch (Exception e) {
            BopsLogUtil.logServiceFailed("MarketingProductReadAdapter","convertMaterialVOList",JSON.toJSONString(list),
                    0L, LogConstant.LogType.SAL,e);
        }
    }

}