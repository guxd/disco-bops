package com.alibaba.cbuscm.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 保证金展示状态枚举
 */
public enum DepositDisplayFieldEnum {

    /**
     * 保证金不足
     */
    INSUFFICIENT("insufficient"),
    /**
     * 保证金7天未缴纳
     */
    SEVEN_DAYS_NON_SURRENDER("7DaysNonSurrender");


    private static Map<String, DepositDisplayFieldEnum> enumHolder = new HashMap<String, DepositDisplayFieldEnum>();

    private String value;

    DepositDisplayFieldEnum(String value) {
        this.value = value;
    }

    static {
        for (DepositDisplayFieldEnum sortFieldEnum : DepositDisplayFieldEnum.values()) {
            enumHolder.put(sortFieldEnum.getValue(), sortFieldEnum);
        }
    }

    public String getValue() {
        return value;
    }

    public static DepositDisplayFieldEnum getTypeEnumByValue(String value) {
        return enumHolder.get(value);
    }

    public static DepositDisplayFieldEnum parseByValue(String valueStr) {
        if (StringUtils.isBlank(valueStr)) {
            return null;
        }

        for (DepositDisplayFieldEnum value : DepositDisplayFieldEnum.values()) {
            if (valueStr.equals(value.getValue())) {
                return value;
            }
        }
        return null;
    }
}
