package com.alibaba.cbuscm.enums.international;

/**
 * 境外
 */
public enum InternationalChannelEnum {
    AE("AE中东大店","AE_SHOP");

    private String value;

    private String name;

    InternationalChannelEnum(String name,String value) {
        this.name=name;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
