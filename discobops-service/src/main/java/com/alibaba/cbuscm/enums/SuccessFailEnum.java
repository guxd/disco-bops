package com.alibaba.cbuscm.enums;

import com.alibaba.cbu.panama.dc.client.constants.OperateTypeEnum;
import lombok.Getter;
import org.testng.collections.Maps;

import java.util.Map;

/**
 * Created by duanyang.zdy on 2019/4/17.
 * 排序枚举
 */
@Getter
public enum SuccessFailEnum {

    /**
     * 一审通过
     */
    SUCCESS(10,"通过"),
    /**
     * 二审通过
     */
    FAIL(20,"拒绝"),

    ;



    private Integer value;

    private String desc;

    SuccessFailEnum(Integer value, String desc) {

        this.value = value;
        this.desc = desc;
    }


    public static Map<String,SuccessFailEnum> map = Maps.newHashMap();


    static{
        map.put(OperateTypeEnum.FIRST_AUDIT_SUCESS.name(),SUCCESS);
        map.put(OperateTypeEnum.SECORD_AUDIT_SUCESS.name(),SUCCESS);
        map.put(OperateTypeEnum.FIRST_AUDIT_FAIL.name(),FAIL);
        map.put(OperateTypeEnum.SECORD_AUDIT_FAIL.name(),FAIL);
    }
}
