package com.alibaba.cbuscm.enums;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastvalidator.constraints.utils.StringUtils;

/**
 * Created by duanyang.zdy on 2019/9/5.
 */
public enum MarketTaskStatusEnum {

    /**
     * 全部成功
     */
    ALL_SUCC("allSucc"),
    /**
     * 部分成功
     */
    PART_SUCC("partSucc"),
    /**
     * LAZAD印尼站选品任务
     */
    ALL_FAILED("allFailed");

    private static Map<String, MarketTaskStatusEnum> enumHolder = new HashMap<String, MarketTaskStatusEnum>();

    private String value;

    MarketTaskStatusEnum(String value) {
        this.value = value;
    }

    static {
        for (MarketTaskStatusEnum marketTaskStatusEnum : MarketTaskStatusEnum.values()) {
            enumHolder.put(marketTaskStatusEnum.name(), marketTaskStatusEnum);
        }
    }

    public String getValue() {
        return value;
    }

    public static MarketTaskStatusEnum getTypeEnumByValue(String value) {
        return enumHolder.get(value);
    }

    public static MarketTaskStatusEnum parseByValue(String valueStr) {
        if (StringUtils.isBlank(valueStr)) {
            return null;
        }

        for (MarketTaskStatusEnum value : MarketTaskStatusEnum.values()) {
            if (valueStr.equals(value.getValue())) {
                return value;
            }
        }
        return null;
    }
}
