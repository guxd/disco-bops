package com.alibaba.cbuscm.enums;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastvalidator.constraints.utils.StringUtils;

/**
 * Created by duanyang.zdy on 2019/9/4.
 */
public enum SelectorTaskTypeEnum {

    /**
     * AE大店自动圈选任务类型
     */
    ONE_PIECE_AE_SHOP_AUTO_SELECT_TASK("AE大店"),
    /**
     * LAZAD泰国站选品任务
     */
    ONE_PIECE_LAZADA_TH_AUTO_SELECT_TASK("LAZAD泰国站"),
    /**
     * LAZAD印尼站选品任务
     */
    ONE_PIECE_LAZADA_ID_AUTO_SELECT_TASK("LAZAD印尼站"),
    /**
     * 天猫出海选品任务
     */
    ONE_PIECE_TM_TO_SEA_SELECT_TASK("天猫出海"),
    /**
     * 补品圈选任务
     */
    ONE_PIECE_ADD_GOODS_AUTO_SELECT_TASK("补品");

    private static Map<String, SelectorTaskTypeEnum> enumHolder = new HashMap<String, SelectorTaskTypeEnum>();

    private String value;

    SelectorTaskTypeEnum(String value) {
        this.value = value;
    }

    static {
        for (SelectorTaskTypeEnum selectorTaskTypeEnum : SelectorTaskTypeEnum.values()) {
            enumHolder.put(selectorTaskTypeEnum.name(), selectorTaskTypeEnum);
        }
    }

    public String getValue() {
        return value;
    }

    public static SelectorTaskTypeEnum getTypeEnumByValue(String value) {
        return enumHolder.get(value);
    }

    public static SelectorTaskTypeEnum parseByValue(String valueStr) {
        if (StringUtils.isBlank(valueStr)) {
            return null;
        }

        for (SelectorTaskTypeEnum value : SelectorTaskTypeEnum.values()) {
            if (valueStr.equals(value.getValue())) {
                return value;
            }
        }
        return null;
    }
}
