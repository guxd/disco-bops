/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.enums;

/**
 * 类ChannelNameEnum.java的实现描述：下游渠道名称
 * 
 * @author jizhi.qy 2019年4月26日 下午2:43:17
 */
public enum ChannelNameEnum {
    TB("淘宝");
    private String value;

    ChannelNameEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
