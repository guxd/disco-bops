/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.enums;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * 类IndicationSortFieldEnum.java的实现描述：指标筛选条件的排序方式枚举
 * 
 * @author jizhi.qy 2019年4月18日 上午11:41:16
 */
public enum IndicationSortFieldEnum {
    /**
     * 按照价格升序
     */
    PRICE_ASC("price_asc", "按照价格升序"),

    /**
     * 按照价格降序
     */
    PRICE_DESC("price_desc", "按照价格降序"),

    /**
     * 1688 7天销量 降序
     */
    SELL_AMOUNT_7DAYS_1688_DESC("sell_amount_7days_1688_desc", "7天销量降序"),

    /**
     * 1688 30天销量 降序
     */
    SELL_AMOUNT_30DAYS_1688_DESC("sell_amount_30days_1688_desc", "30天销量降序"),

    /**
     * 1688 7天买家数 降序
     */
    BUYER_AMOUNT_7DAYS_1688_DESC("buyer_amount_7days_1688_desc", "7天买家数降序"),

    /**
     * 1688 30天买家数 降序
     */
    BUYER_AMOUNT_30DAYS_1688_DESC("buyer_amount_30days_1688_desc", "30天买家数降序");

    /**
     * 淘宝同款 7天销量 降序
     */
    // SELL_AMOUNT_7DAYS_TAOBAO_DESC("sell_amount_7days_taobao_desc", "淘宝同款7天销量降序"),

    /**
     * 淘宝 30天销量 降序
     */
    // SELL_AMOUNT_30DAYS_TAOBAO_DESC("sell_amount_30days_taobao_desc", "淘宝30天销量降序");

    private String value;
    private String text;

    IndicationSortFieldEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static Map<String, String> fullMap = new HashMap<String, String>();

    static {
        for (IndicationSortFieldEnum sortFieldEnum : IndicationSortFieldEnum.values()) {
            fullMap.put(sortFieldEnum.getValue(), sortFieldEnum.getText());
        }
    }

    public static IndicationSortFieldEnum parseByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        for (IndicationSortFieldEnum value1 : IndicationSortFieldEnum.values()) {
            if (value.equals(value1.getValue())) {
                return value1;
            }
        }
        return null;
    }

}
