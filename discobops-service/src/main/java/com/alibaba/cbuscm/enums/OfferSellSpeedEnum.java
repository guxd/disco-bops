/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.enums;

/**
 * 类OfferSellSpeedEnum.java的实现描述：offer销售增速
 * 
 * @author jizhi.qy 2019年4月26日 下午2:44:46
 */
public enum OfferSellSpeedEnum {
    HIGH("高"),
    MID("中"),
    LOW("低");
    private String value;

    OfferSellSpeedEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
