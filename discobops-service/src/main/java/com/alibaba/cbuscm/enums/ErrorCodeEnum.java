package com.alibaba.cbuscm.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by duanyang.zdy on 2019/4/18.
 */
public enum
ErrorCodeEnum {

    /**
     * 请求参数错误
     */
    RESUELT_PARAM_ERROR("request_param_error"),
    /**
     * 请求参数错误
     */
    SYSTEM_ERROR("system_error"),
    /**
     * 没有该任务
     */
    TASK_NOT_CONFIG("task_not_config"),
    /**
     * 任务排队中
     */
    TASK_WAIT_QUEUE("task_wait_queue"),
    /**
     * 任务正在运行
     */
    TASK_PAUSE("task_pause"),
    /**
     * 任务正在运行
     */
    TASK_RUNNING("task_running");


    private static Map<String, ErrorCodeEnum> enumHolder = new HashMap<String, ErrorCodeEnum>();

    private String value;

    ErrorCodeEnum(String value) {
        this.value = value;
    }

    static {
        for (ErrorCodeEnum errorCodeEnum : ErrorCodeEnum.values()) {
            enumHolder.put(errorCodeEnum.getValue(), errorCodeEnum);
        }
    }

    public String getValue() {
        return value;
    }

    public static ErrorCodeEnum getTypeEnumByValue(String value) {
        return enumHolder.get(value);
    }
}
