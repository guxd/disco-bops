package com.alibaba.cbuscm.enums;

import lombok.Getter;

/**
 * Created by duanyang.zdy on 2019/4/17.
 * 排序枚举
 */
@Getter
public enum RemarkEnum {

    /**
     * 一审通过
     */
    FIRST_AUDIT(10,"一审通过"),
    /**
     * 二审通过
     */
    SECOND_AUDIT(20,"二审通过"),

    ;



    private Integer value;

    private String desc;

    RemarkEnum(Integer value, String desc) {

        this.value = value;
        this.desc = desc;
    }

}
