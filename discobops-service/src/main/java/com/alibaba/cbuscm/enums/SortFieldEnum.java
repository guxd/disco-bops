package com.alibaba.cbuscm.enums;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by duanyang.zdy on 2019/4/17.
 * 排序枚举
 */
public enum SortFieldEnum {

    /**
     * 30天日均销量
     */
    SALE_COUNT_30D_AVG("sale_count_30d_avg"),
    /**
     * 点赞数量
     */
    LIKE_COUNT("like_count"),
    /**
     * 评论数
     */
    CMT_COUNT("cmt_count");


    private static Map<String, SortFieldEnum> enumHolder = new HashMap<String, SortFieldEnum>();

    private String value;

    SortFieldEnum(String value) {
        this.value = value;
    }

    static {
        for (SortFieldEnum sortFieldEnum : SortFieldEnum.values()) {
            enumHolder.put(sortFieldEnum.getValue(), sortFieldEnum);
        }
    }

    public String getValue() {
        return value;
    }

    public static SortFieldEnum getTypeEnumByValue(String value) {
        return enumHolder.get(value);
    }

    public static SortFieldEnum parseByValue(String valueStr) {
        if (StringUtils.isBlank(valueStr)) {
            return null;
        }

        for (SortFieldEnum value : SortFieldEnum.values()) {
            if (valueStr.equals(value.getValue())) {
                return value;
            }
        }
        return null;
    }
}
