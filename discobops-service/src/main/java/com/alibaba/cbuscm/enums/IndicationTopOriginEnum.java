/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.enums;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * 类IndicationTopOriginEnum.java的实现描述：指标查看原产地货源类型
 * 
 * @author jizhi.qy 2019年4月18日 上午11:57:52
 */
public enum IndicationTopOriginEnum {
    /**
     * 仅看源产地货源
     */
    TOP_ORIGIN_1("top_origin_1", "仅看源产地货源"),

    /**
     * 仅看源产地货源top3
     */
    TOP_ORIGIN_3("top_origin_3", "仅看源产地货源top3"),

    /**
     * 仅看源产地货源top5
     */
    TOP_ORIGIN_5("top_origin_5", "仅看源产地货源top5");

    private String value;
    private String text;

    IndicationTopOriginEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static Map<String, String> fullMap = new HashMap<String, String>();

    static {
        for (IndicationTopOriginEnum topOriginEnum : IndicationTopOriginEnum.values()) {
            fullMap.put(topOriginEnum.getValue(), topOriginEnum.getText());
        }
    }

    public static IndicationTopOriginEnum parseByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        for (IndicationTopOriginEnum value1 : IndicationTopOriginEnum.values()) {
            if (value.equals(value1.getValue())) {
                return value1;
            }
        }
        return null;
    }

}
