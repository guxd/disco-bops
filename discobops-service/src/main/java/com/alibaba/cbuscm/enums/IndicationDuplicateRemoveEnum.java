/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.enums;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * 类DuplicateRemoveTypeEnum.java的实现描述：指标去重类型
 * 
 * @author jizhi.qy 2019年4月18日 上午11:53:57
 */
public enum IndicationDuplicateRemoveEnum {
    /**
     * 一商1品
     */
    SELLER_1_ITEM_1("seller_1_item_1", "一商1品"),
    /**
     * 一商2品
     */
    SELLER_1_ITEM_2("seller_1_item_2", "一商2品"),
    /**
     * 一商3品
     */
    SELLER_1_ITEM_3("seller_1_item_3", "一商3品"),
    /**
     * 一商5品
     */
    SELLER_1_ITEM_5("seller_1_item_5", "一商5品");
    
    private String value;
    private String text;

    IndicationDuplicateRemoveEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }
    
    public String getText() {
        return text;
    }

    public static Map<String, String> fullMap =
        new HashMap<String, String>();

    static {
        for (IndicationDuplicateRemoveEnum duplicateRemoveEnum : IndicationDuplicateRemoveEnum.values()) {
            fullMap.put(duplicateRemoveEnum.getValue(), duplicateRemoveEnum.getText());
        }
    }

    public static IndicationDuplicateRemoveEnum parseByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        for (IndicationDuplicateRemoveEnum value1 : IndicationDuplicateRemoveEnum.values()) {
            if (value.equals(value1.getValue())) {
                return value1;
            }
        }
        return null;
    }

}
