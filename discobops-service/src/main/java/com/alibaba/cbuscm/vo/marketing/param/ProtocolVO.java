package com.alibaba.cbuscm.vo.marketing.param;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @Auther: gxd
 * @Date: 2019/10/28 22:26
 * @Description:
 */
@Data
public class ProtocolVO {
    /**
     * 协议名称
     */
    @NotBlank(message = "协议名称")
    private String name;

    /**
     * 协议链接
     */
    @NotBlank(message = "协议链接不能为空")
    private String url;
}
