package com.alibaba.cbuscm.vo.domestic.tb.planning;

import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.ToString;

/**
 * @author yaogaolin 2019-08-19 16:09
 * 添加商家 或者 同款匹配 商机vo
 */
@Data
@ToString
public class PlanningSourceItemVO {
    /**
     * 供应商memberId
     */
    private String memberId;
    /**
     * 公司名
     */
    private String companyName;

    /**
     * 登录名
     */
    private String loginId;

    /**
     * 地址
     */
    private String address;

    /**
     * 商品基础信息
     */
    private PlanningOppItemVO item;

}
