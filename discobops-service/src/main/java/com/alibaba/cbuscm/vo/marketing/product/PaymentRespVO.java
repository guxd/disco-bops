package com.alibaba.cbuscm.vo.marketing.product;

import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/10/10
 */

@Data
public class PaymentRespVO extends BaseVO {

    private String loginId;

//    private String supplierName;


    private String supplierShopName;

    private String marketingPlanName;

    private Double chargeFee;

    private String fee;
}
