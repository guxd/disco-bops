/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb;

import java.util.List;

/**
 * 选品任务组展示VO
 * 
 * @author luxuetao 2019年4月18日 下午3:11:50
 */
public class SelectionTaskGroupViewVO {

    // 聚合id
    private Long id;
    // 类目id
    private Long categoryId;
    // 类目名称
    private String name;
    // 图片地址
    private String imgUrl;
    // 所属任务列表
    private List<SelectionTaskViewVO> tasks;
    // 是否为“综合活动”，不是则选取类目
    private Boolean hasComposite;

    /**
     * 虚拟类目id
     */
    private Long virtualCategoryId;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getHasComposite() {
        return hasComposite;
    }

    public void setHasComposite(Boolean hasComposite) {
        this.hasComposite = hasComposite;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public List<SelectionTaskViewVO> getTasks() {
        return tasks;
    }

    public void setTasks(List<SelectionTaskViewVO> tasks) {
        this.tasks = tasks;
    }

    public Long getVirtualCategoryId() {
        return virtualCategoryId;
    }

    public void setVirtualCategoryId(Long virtualCategoryId) {
        this.virtualCategoryId = virtualCategoryId;
    }
}
