package com.alibaba.cbuscm.vo.international;

import java.io.Serializable;

public class PurchaseOrderVO implements Serializable {

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 销售订单编号
     */
    private String saleOrderId;

    /**
     * 状态名称
     */
    private String statusName;

    /**
     * 采购单状态
     */
    private String status;

    /**
     * 记录生成时间
     */
    private long createTime;


    /**
     * 总体采购价
     */
    private String totalPay;


    /**
     * 预计数量
     */
    private int quantity;


    /**
     * 收货数量
     */
    private int receiveQuantity;

    /**
     * 分表键（现在表示采购单买家）
     */
    private String userId;

    /**
     *
     */
    private String loginId;

    /**
     * CBU交易ID
     */
    private String cbuTradeId;


    private String channelId;


    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(String totalPay) {
        this.totalPay = totalPay;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getReceiveQuantity() {
        return receiveQuantity;
    }

    public void setReceiveQuantity(int receiveQuantity) {
        this.receiveQuantity = receiveQuantity;
    }

    /**

     * 采购单号
     */
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(String saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getCbuTradeId() {
        return cbuTradeId;
    }

    public void setCbuTradeId(String cbuTradeId) {
        this.cbuTradeId = cbuTradeId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
