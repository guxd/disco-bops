/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import lombok.Data;
import lombok.ToString;

/**
 * 类PayParamVO.java的实现描述：TODO 类实现描述 
 * @author jizhi.qy 2019年5月28日 下午9:45:27
 */
@Data
@ToString
public class PayParamVO {
    private String loginId;
    private String orderId;
    private Long chargeBackMoney;
    private String receiveShop;
    private String reason;
    private String fileLink;
}
