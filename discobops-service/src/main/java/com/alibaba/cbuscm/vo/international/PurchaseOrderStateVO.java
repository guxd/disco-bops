package com.alibaba.cbuscm.vo.international;

/**
 * @author xuhb
 * @title: PurchaseOrderStateVO
 * @projectName discobops
 * @description: 销售单状态
 * @date 2019-05-0821:58
 */
public class PurchaseOrderStateVO {

    /**
     * 状态显示名称
     */
    private String name;

    /**
     * 状态id
     */
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
