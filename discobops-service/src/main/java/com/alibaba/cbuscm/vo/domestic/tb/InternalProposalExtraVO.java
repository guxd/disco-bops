package com.alibaba.cbuscm.vo.domestic.tb;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 类InternalExtraModel
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Data
@ToString
@NoArgsConstructor
public class InternalProposalExtraVO {

    /**
     * 工艺
     */
    private String technics;

    /**
     * 材质
     */
    private String material;

    /**
     * 包装成本
     */
    private Double packingCost;

    /**
     * 物流成本
     */
    private Double logisticsCost;

    /**
     * 制造费用
     */
    private Double manufactureCost;

    /**
     * 材料成本
     */
    private Double materialCost;

    /**
     * 期间费用
     */
    private Double periodCost;

    /**
     * 品牌名称
     */
    private String brandName;

    /**
     * 是否新增品牌
     */
    private boolean newBrand = false;

    /**
     * 品牌注册号
     */
    private String brandRegNo;

    /**
     * 品牌所有人
     */
    private String brandOwner;

    /**
     * 商标注册证
     */
    private String trcUrl;

    /**
     * 商标使用授权书
     */
    private String tlUrl;

    /**
     * 3C 认证证书
     */
    private String auth3cUrl;

    /**
     * 包装及规格
     */
    private String packingSpec;

    /**
     * 保养要求
     */
    private String maintainRequire;

    /**
     * 使用注意点
     */
    private String attentions;

    /**
     * 其它服务功能
     */
    private String otherServices;

    /**
     * 是否保修
     */
    private Boolean guarantee;

    /**
     * 是否易碎品
     */
    private Boolean fragile;

    /**
     * 物流方式
     */
    private String logisticsType;

    /**
     * 发货快递
     */
    private String logisticsAgent;

    /**
     * 发货地址
     */
    private String sendAddress;

    /**
     * 退货地址
     */
    private String returnAddress;
}
