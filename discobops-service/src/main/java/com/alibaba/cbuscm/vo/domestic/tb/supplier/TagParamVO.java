package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class TagParamVO {

    private String memberId;

    private List<String> tags;

    private String scene;
}
