package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import lombok.Data;
import lombok.ToString;

/**
 * Created by qinwei on 2019/9/9.
 *
 * @author qinwei
 * @date 2019/09/09
 */
@Data
@ToString
public class AliqygSupplierManageVO extends TaoSupplierManageVO {

    /**
     * 主营类目
     */
    private String mainCategory;

}
