package com.alibaba.cbuscm.vo.marketing.param;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Data
public class MarketingPlanPageReqVO extends BasePageVO{


    /**
     * 活动状态
     */
    private String status;
    /**
     * pageCode
     */
    @NotBlank
    private String pageCode;

    /**
     * 活动id
     */
    private Long marketingPlanId;

    /**
     * 活动名称
     */
    private String marketingPlanName;

    /**
     * 收费类型
     * @see com.alibaba.cbu.panama.dc.client.constants.marketingplan.ChargeTypeEnum
     */
    private String chargeType;

    /**
     * 排序方式--起始时间
     */
    private String enrollTime;

    /**
     * 排序方式--结束时间
     */
    private String effectiveTime;
}


