package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MarketViewResultModel {

    private Integer count;
    private List<MarketViewModel> data;
    private Integer pageIndex;
    private Integer pageNum;
    private String stat_date;
    private Boolean success;
    private BigDecimal xMedian;
    private BigDecimal yMedian;
    private BigDecimal yMax;

    @Data
    @ToString
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MarketViewModel {
        private String marketId;
        private String marketName;
        //类目层级 不需要该字段
//    private String cateLevel;
        private String cateId;
        private String cateName;
        private String isGlobal;
        private String marketTags;
        private String productNum;
        private String siteCode;
        private String stat_date;
        private Double cbuSameOfferForPic;
        private Double cbuSameOfferForStyle;
        private Double cbusimilarOffer;
        //没有使用到？
        private String gmtCreate;
        private String gmtModified;
        //id 没有
        private Double marketGmv30d;
        private Double marketGrowthForGmv;
        private Double marketGrowthForSales;
        //我们这边为Double 前端为String
        private Double marketSales30d;
        private Double marketShareForGmv;
        private Double marketShareForSales;
        //        private String pic;
        private String pic;
        private String images;
        //数据库中没有此字段
        private Integer siteNum;
        private BigDecimal xMedian;
        private BigDecimal yMedian;
        //三个匹配率
        private Double rateForSamePic;
        private Double rateForSameStyle;
        private Double rateForSimilarStyle;

        private BigDecimal x;
        private BigDecimal y;
        private String type;
        private String sites;
        private Integer total;

    }


}
