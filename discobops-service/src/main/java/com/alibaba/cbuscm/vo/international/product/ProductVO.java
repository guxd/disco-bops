package com.alibaba.cbuscm.vo.international.product;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 产品视图模型
 * @author zhuoxian
 */
@Data
@ToString
public class ProductVO implements Serializable {

    /**
     * 产品ID
     */
    private String id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 产品状态
     */
    private String status;

    /**
     * 产品标题
     */
    private String title;

    /**
     * 产品图片
     */
    private String image;

    /**
     * 产品详情
     */
    private String detail;

    /**
     * 业务类型
     */
    private String bizType;

    /**
     * SKU
     */
    private List<ProductSkuVO> productSkuList;

    /**
     * 扩展属性
     */
    private Map<String, Object> extraAttrs;

    /**
     * 铺货状态
     */
    private String distributeStatus;

}