package com.alibaba.cbuscm.vo.international.product;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 供应关系视图模型
 * @author zhuoxian
 */
@Data
@ToString
public class SupplyRelationVO implements Serializable {

    /**
     * 供应关系ID
     */
    private String id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 产品ID
     */
    private String productId;

    /**
     * 产品SKU ID
     */
    private String productSkuId;

    /**
     * OFFER ID
     */
    private Long offerId;

    /**
     * OFFER SKU ID
     */
    private Long offerSkuId;

    /**
     * OFFER SKU SPEC ID
     */
    private String offerSpecId;

    /**
     * 供应商ID
     */
    private String supplierId;

    /**
     * 状态
     */
    private String status;

    /**
     * 状态描述
     */
    private String statusDesc;

    /**
     * 规格名称
     */
    private String desc;

    /**
     * 供应关系优先级，代销模式下最大优先级为首选
     */
    private Integer priority;

    /**
     * 经营模式 经销/代销
     */
    private String supplyMode;

    /**
     * 控件模式 Y/N
     */
    private String priceMode;

    /**
     * 采购价
     */
    private String purchasePrice;

    /**
     * 物流成本
     */
    private String logisticsCost;

    /**
     * 其他成本
     */
    private Long extraCost;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 供应商信息
     */
    private SupplierVO supplier;

    /**
     * 供应商id
     */
    private Long relationId;

    /**
     * ASCP 供应商ID
     */
    private Long cnMemberId;
}
