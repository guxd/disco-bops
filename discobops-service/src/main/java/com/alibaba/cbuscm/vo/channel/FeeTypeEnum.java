package com.alibaba.cbuscm.vo.channel;

public enum FeeTypeEnum {

    DOMESTIC_WAREHOUSE_OPERATING_COST(40, "仓内操作费"),
    DOMESTIC_PACKING_CHARGE(50, "包装费"),
    DOMESTIC_WAREHOUSE_RENT_COST(70, "仓租费");

    private Integer value;

    private String desc;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    FeeTypeEnum(int value, String desc){
        setDesc(desc);
        setValue(value);
    }

    /**
     * 校验外部name是否匹配枚举的name
     */
    public static boolean contains(String outerName) {
        FeeTypeEnum[] enums = values();
        for (FeeTypeEnum s : enums) {
            if (s.name().equals(outerName)) {
                return true;
            }
        }
        return false;
    }

}
