package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/4.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskVO implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;


    private String taskId;

    private String taskName;

    private String taskType;
}
