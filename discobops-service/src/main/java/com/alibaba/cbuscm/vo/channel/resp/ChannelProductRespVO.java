package com.alibaba.cbuscm.vo.channel.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: gxd
 * @Date: 2019/10/15 21:05
 * @Description:渠道商品
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelProductRespVO {

    /**
     * 商品id
     */
    private Long productId;

    /**
     * 商品标题
     */
    private String productTitle;

    /**
     * 商品图片
     */
    private String productImg;

    /**
     * 商品编码
     */
    private String productCode;


}
