package com.alibaba.cbuscm.vo.marketing.param;


import com.alibaba.cbuscm.vo.marketing.product.OwnerRespVO;
import com.alibaba.cbuscm.vo.marketing.product.SellerRespVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 13:32
 * @Description:活动基本信息
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BasicMarketingPlanInfoParam implements Serializable {
    private static final long serialVersionUID = -4057622235045233937L;

    /**
     * 所属业务
     */
    private String market;

    /**
     * 参与活动的门店
     */
    private List<SellerRespVO> sellerList;

    /**
     * 管理员
     */
    private List<OwnerRespVO> ownerList;

    /**
     * 报名类型
     */
    private String enrollType;

    /**
     * 活动描述
     */
    private String marketingPlanDesc;

    /**
     * 收费类型
     */
    private String chargeType;

    /**
     * 人员类型+人员id 鉴权
     */
    private String chargeParam;

    /**
     * 报名时间开始
     */
    private Date enrollStartTime;

    /**
     * 报名时间结束
     */
    private Date enrollEndTime;

    /**
     * 上线时间开始
     */
    private Date effectiveStartTime;

    /**
     * 上线时间结束
     */
    private Date effectiveEndTime;

    /**
     * 审核方式
     */
    private String auditType;

    /**
     * 协议url
     */
    private String protocolUrl;

    /**
     * 协议名称
     */
    private String protocolName;


}
