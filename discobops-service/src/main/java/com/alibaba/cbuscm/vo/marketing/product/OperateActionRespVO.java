package com.alibaba.cbuscm.vo.marketing.product;

import lombok.*;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class OperateActionRespVO {

    /**
     * 操作code
     */
    private String operateCode;

    /**
     * 操作描述
     */
    private String operateDesc;

    /**
     * 是否是链接，0-非链接，1-链接
     */
    private Integer isLink;
}
