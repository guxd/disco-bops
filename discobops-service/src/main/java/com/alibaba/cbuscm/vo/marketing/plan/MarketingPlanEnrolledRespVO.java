package com.alibaba.cbuscm.vo.marketing.plan;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;


import java.math.BigDecimal;
import java.util.Date;

/**
 * @Auther: gxd
 * @Date: 2019/10/10 02:08
 * @Description:
 */
@Data
public class MarketingPlanEnrolledRespVO {


    /**
     * 活动名称
     */
    private String marketingPlanName;

    /**
     * 活动id
     */
    private Long marketingPlanId;

    /**
     * 报名活动商品状态
     */
    private String status;

    /**
     * 报名时间
     */
    private Date operateCreateTime;

    /**
     * 活动收费方式:佣金比例
     */
    private Double param;

    /**
     * 活动收费方式:佣金比例
     */
    private String chargeType;

    /**
     * 状态信息
     */
    private String statusMessage;



}
