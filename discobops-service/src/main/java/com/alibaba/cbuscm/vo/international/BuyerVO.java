package com.alibaba.cbuscm.vo.international;

/**
 * @author xuhb
 * @title: BuyerVO
 * @projectName discobops
 * @description: 大店的供应方信息
 * @date 2019-05-1413:57
 */
public class BuyerVO {

    /**
     * 供应方公司名称
     */
    private String companyName;

    /**
     * 供应方公司编号
     */
    private String companyCode;

    /**
     * 渠道名称
     */
    private String channelName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
