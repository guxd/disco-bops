package com.alibaba.cbuscm.vo.marketing.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 20:07
 * @Description:审核记录
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OperateLogVO implements Serializable {
    private static final long serialVersionUID = -2203673613844172574L;

    /**
     * 操作类型
     */
    private String operateType;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 备注
     */
    private String remark;

    /**
     * 时间
     */
    private Date createTime;
}
