package com.alibaba.cbuscm.vo.marketing.plan.req;

import com.alibaba.cbu.panama.dc.client.model.marketingplan.QualificationInitDTO;
import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/10/31 11:27
 * @Description:
 */
@Data
public class QualificationReqVO extends BaseVO {
    private Long marketingPlanId;
    //资质类型：QC_SUPPLIER：供应商资质；QC_PRODUCT：商品资质
    private String qcType;
    private String qcCode;
    private QualificationInitDTO qcInitDTO;
}
