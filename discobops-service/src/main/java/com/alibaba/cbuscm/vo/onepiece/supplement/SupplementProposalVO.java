package com.alibaba.cbuscm.vo.onepiece.supplement;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 类SupplementProposalVO
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SupplementProposalVO implements Serializable {

    private Long offerId;
    private String pic;
    private String price;
    private String status;
    private Boolean locked;
}
