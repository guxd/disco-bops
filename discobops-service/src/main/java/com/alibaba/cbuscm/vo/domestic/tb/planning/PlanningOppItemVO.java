package com.alibaba.cbuscm.vo.domestic.tb.planning;

import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.ToString;

/**
 * 企划寻源商机 商品vo
 *
 * @author yaogaolin 2019-08-19 16:09
 */
@Data
@ToString
public class PlanningOppItemVO {

    /**
     * 商品id
     */
    private Long itemId;
    /**
     * 商品地址
     */
    private String link;
    /**
     * 商品图片列表
     */
    private List<String> picUrls;
    /**
     * 商品标题
     */
    private String title;
    /**
     * 1688售价
     */
    private Double price;

    /**
     * 月销量统计
     */
    private Integer sales;

    /**
     * 是否镇店之宝
     */
    private Boolean bestOffer;

    /**
     * 是否金冠商品
     */
    private Boolean goldOffer;

    /**
     * 上线时间
     */
    private Date uptime;

    /**
     * 寻源类型
     */
    private String type;
    /**
     * 寻源当前状态
     */
    private String status;

    /**
     * 寻源当前状态名称
     */
    private String statusMessage;

    /**
     * 商品关联的提报ID
     */
    private Long offerProposalId;
}
