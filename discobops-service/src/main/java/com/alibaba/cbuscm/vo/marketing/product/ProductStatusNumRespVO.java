package com.alibaba.cbuscm.vo.marketing.product;

import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 15:34
 * @Description:不同状态商品的数量
 */
@Data
public class ProductStatusNumRespVO {
    /**
     * 商品状态
     */
    private String productStatus;

    /**
     * 不同状态下商品的数量
     */
    private Integer num;

    /**
     * 商品状态描述
     */
    private String productStatusMessage;

    public ProductStatusNumRespVO(){}

    public ProductStatusNumRespVO(String productStatus, Integer num, String productStatusMessage) {
        this.productStatus = productStatus;
        this.num = num;
        this.productStatusMessage = productStatusMessage;
    }
}
