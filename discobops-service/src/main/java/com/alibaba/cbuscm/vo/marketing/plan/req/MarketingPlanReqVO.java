package com.alibaba.cbuscm.vo.marketing.plan.req;

import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/11/6 15:38
 * @Description:
 */
@Data
public class MarketingPlanReqVO extends BaseVO {
    private Long marketingPlanId;
    private String operator;
}
