package com.alibaba.cbuscm.vo.international.product;

import java.util.Date;

import com.alibaba.cbu.disco.shared.core.product.model.DcLogisticAttributes;

import lombok.Getter;
import lombok.Setter;

/**
 * 类ChannelProductSkuVO
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Getter
@Setter
public class ChannelProductSkuVO {

    private String productSkuId;
    private String channel;
    private Long id;
    private Date gmtCreate;
    private Date gmtModified;
    private String status;
    private String productId;
    private String storageType;
    private String desc;
    private String image;
    private DcLogisticAttributes logisticAttributes;
    private Integer logisticsCost;
    private Integer price;
    private String priceCurrency;
    private Integer purchasePrice;
    private Integer retailPrice;
    private Integer premiumRate;
    private String marketingMode;
    private String bizType;
    private Integer stock;
    private Date gmtSync;
}
