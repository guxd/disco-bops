package com.alibaba.cbuscm.vo.domestic.tb.planning;

import java.util.Set;

import com.alibaba.china.shared.discosupplier.model.common.UserModel;

import lombok.Data;
import lombok.ToString;

/**
 * 企划寻源 vo
 *
 * @author yaogaolin 2019/4/28
 */

@Data
@ToString
public class PlanningSourceVO {
    /**
     * 寻源id
     */
    private Long id;
    /**
     * 企划id
     */
    private Long planningId;

    /**
     * 企划标题
     */
    private String planningTitle;

    /**
     * 企划类型
     */
    private String planningType;

    /**
     * 企划波段
     */
    private String planningWaveBrand;

    /**
     * 处理人
     */
    private Set<UserModel> handlers;

    /**
     * 状态
     */
    private String status;

    /**
     * 类目id
     */
    private String categoryId;
    /**
     * 类目值
     */
    private String categoryValue;

    /**
     * 招募商家数
     */
    private Integer recruitMemberCount;

    /**
     * 待审核商品数(供货中心待审核)
     */
    private Integer approveCount;

    /**
     * 供货中心审核通过
     */
    private Integer importPassCount;

    /**
     * 商品通待审核
     */
    private Integer channelImportCount;

    /**
     * 商品通一审通过等待二审
     */
    private Integer channelOnePassCount;

    /**
     * 机身通过商品数
     */
    private Integer offerCount;

    /**
     * 是否有权限修改
     */
    private Boolean editable;

    /**
     * Database Column Remarks:
     * 提报下限
     *
     * @mbg.generated
     */
    private Integer importLower;

    /**
     * Database Column Remarks:
     * 提报上限
     *
     * @mbg.generated
     */
    private Integer importUp;
}
