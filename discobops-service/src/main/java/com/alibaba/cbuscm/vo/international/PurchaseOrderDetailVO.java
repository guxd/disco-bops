package com.alibaba.cbuscm.vo.international;

import java.util.List;

import com.alibaba.cbu.disco.shared.core.order.model.data.FsPurchaseExtra;

/**
 * @author xuhb
 * @title: SaleOrderDetailVo
 * @projectName discobops
 * @description: 销售订单详情
 * @date 2019-05-1413:51
 */
public class PurchaseOrderDetailVO {

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 采购单id
     */
    private String purchaseOrderId;

    /**
     * 订单主ID
     * 必填
     */
    private String orderId;

    /**
     * 下单时间
     */
    private Long createTime;

    /**
     * 状态名称
     */
    private String statusName;

    /**
     * 采购方信息
     */
    private BuyerVO buyer;

    /**
     * 供应商信息
     */
    private SellerVO seller;

    /**
     * 订单的付款相关状态
     */
    private String payStatus;

    /**
     * 订单的付款相关文案
     */
    private String payDesc;

    /**
     * 订单的退款相关状态
     */
    private String refundStatus;

    /**
     * 订单的退款相关文案
     */
    private String refundDesc;

    /**
     * 渠道主订单业务状态
     * 不超过32个字符
     */
    private String bizStatus;

    /**
     * 渠道主订单业务文案
     */
    private String bizDesc;

    /**
     * 境内收获地址
     */
    private ReceivingAddressModel domesticAddress;

    /**
     * 境外收获地址
     */
    private ReceivingAddressModel overseasAddress;

    /**
     * 子销售单情况
     */
    private List<PurchaseSubOrderLogisticsVO> logisticsList;

    /**
     * errorCode
     */
    private String errorCode;

    /**
     * errorMsg
     */
    private String errorMsg;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public BuyerVO getBuyer() {
        return buyer;
    }

    public void setBuyer(BuyerVO buyer) {
        this.buyer = buyer;
    }

    public ReceivingAddressModel getDomesticAddress() {
        return domesticAddress;
    }

    public void setDomesticAddress(ReceivingAddressModel domesticAddress) {
        this.domesticAddress = domesticAddress;
    }

    public ReceivingAddressModel getOverseasAddress() {
        return overseasAddress;
    }

    public void setOverseasAddress(ReceivingAddressModel overseasAddress) {
        this.overseasAddress = overseasAddress;
    }

    public List<PurchaseSubOrderLogisticsVO> getLogisticsList() {
        return logisticsList;
    }

    public void setLogisticsList(List<PurchaseSubOrderLogisticsVO> logisticsList) {
        this.logisticsList = logisticsList;
    }

    public SellerVO getSeller() {
        return seller;
    }

    public void setSeller(SellerVO seller) {
        this.seller = seller;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPayDesc() {
        return payDesc;
    }

    public void setPayDesc(String payDesc) {
        this.payDesc = payDesc;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRefundDesc() {
        return refundDesc;
    }

    public void setRefundDesc(String refundDesc) {
        this.refundDesc = refundDesc;
    }

    public String getBizStatus() {
        return bizStatus;
    }

    public void setBizStatus(String bizStatus) {
        this.bizStatus = bizStatus;
    }

    public String getBizDesc() {
        return bizDesc;
    }

    public void setBizDesc(String bizDesc) {
        this.bizDesc = bizDesc;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
