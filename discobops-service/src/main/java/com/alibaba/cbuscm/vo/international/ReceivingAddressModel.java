package com.alibaba.cbuscm.vo.international;

/**
 * @author xuhb
 * @title: ReceivingAddressModel
 * @projectName discobops
 * @description: 收获地址
 * @date 2019-05-1414:08
 */
public class ReceivingAddressModel {

    /**
     * 收获地址
     */
    private String address;

    /**
     * 联系人姓名
     */
    private String name;

    /**
     * 联系人座机
     */
    private String telephone;

    /**
     * 联系人手机
     */
    private String mobile;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
