/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb;

import com.alibaba.cbuscm.vo.UserVO;

import java.util.List;

/**
 * 选品任务VO
 * 
 * @author luxuetao 2019年4月18日 下午3:11:23
 */
public class SelectionTaskViewVO {

    // 任务id
    private Long taskId;
    // 聚合id
    private Long aggregationId;
    // 管理员名，多管理员逗号分隔
    private String members;
    // 创建时间
    private Long createTime;
    // 过期时间
    private Long fireTime;
    // 最新更新时间
    private Long updateTime;
    // 是否为“综合活动”，不是则选取类目
    private Boolean hasComposite;
    // 叶子节点id
    private Long categoryId;
    // 一级业务，目前只有“天天特卖”，everydaySale
    private String primaryBiz;
    // 二级业务(取淘宝1级类目)
    private String secondaryBiz;
    // 名称
    private String name;
    // 简介
    private String description;
    // 管理员
    private List<UserVO> admins;

    /**
     * 虚拟类目id
     */
    private Long virtualCategoryId;
    /**
     * 虚拟类目名称
     */
    private String virtualCategoryName;

    public Long getAggregationId() {
        return aggregationId;
    }

    public void setAggregationId(Long aggregationId) {
        this.aggregationId = aggregationId;
    }

    public Boolean getHasComposite() {
        return hasComposite;
    }

    public void setHasComposite(Boolean hasComposite) {
        this.hasComposite = hasComposite;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getPrimaryBiz() {
        return primaryBiz;
    }

    public void setPrimaryBiz(String primaryBiz) {
        this.primaryBiz = primaryBiz;
    }

    public String getSecondaryBiz() {
        return secondaryBiz;
    }

    public void setSecondaryBiz(String secondaryBiz) {
        this.secondaryBiz = secondaryBiz;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserVO> getAdmins() {
        return admins;
    }

    public void setAdmins(List<UserVO> admins) {
        this.admins = admins;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getFireTime() {
        return fireTime;
    }

    public void setFireTime(Long fireTime) {
        this.fireTime = fireTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getVirtualCategoryId() {
        return virtualCategoryId;
    }

    public void setVirtualCategoryId(Long virtualCategoryId) {
        this.virtualCategoryId = virtualCategoryId;
    }

    public String getVirtualCategoryName() {
        return virtualCategoryName;
    }

    public void setVirtualCategoryName(String virtualCategoryName) {
        this.virtualCategoryName = virtualCategoryName;
    }
}
