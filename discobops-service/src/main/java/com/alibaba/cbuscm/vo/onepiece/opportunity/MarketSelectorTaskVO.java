package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;
import java.util.List;

import com.alibaba.cbuscm.vo.common.PageVo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/4.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketSelectorTaskVO extends PageVo implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;


    private List<TaskVO> data;

    private String adminId;

    private String loginId;

}
