package com.alibaba.cbuscm.vo.onepiece.selection;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SelectionItemExportVO extends BaseRowModel {

    @ExcelProperty(value = "竞品商品所属类目",index = 0)
    private String dsCategory;

    @ExcelProperty(value = "竞品商品价格",index = 1)
    private String dsPrice;


    @ExcelProperty(value = "竞品最近30天销量",index = 2)
    private String dsSale30;


    @ExcelProperty(value = "竞品商品链接",index = 3)
    private String dsItemUrl;


    @ExcelProperty(value = "1688链接",index = 4)
    private String url1688;


    @ExcelProperty(value = "1688 30天销售额",index = 5)
    private String sale301688;


}
