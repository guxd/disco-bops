package com.alibaba.cbuscm.vo.marketing.param;

import com.alibaba.cbu.panama.dc.client.model.marketingproduct.OperateLogDTO;
import lombok.Data;

import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/10/28 22:18
 * @Description:
 */
@Data
public class MarketingPlanWhiteItemVO {
    /**
     * 白名单列表
     */
    private List<Long> whiteList;

    /**
     * 白名单操作记录列表
     */
    List<OperateLogDTO> operateLogList;
}
