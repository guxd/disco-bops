package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/5.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketFailedTaskVO implements Serializable {

    /**
     * 失败任务
     */
    private Long  failedTaskId;

    /**
     * 失败原因
     */
    private String  errorMsg;

}
