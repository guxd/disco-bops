package com.alibaba.cbuscm.vo.marketing.param;

import lombok.*;


/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class MarketingSkuVO {
    /**
     * 是否是主推的sku 0-否 1-是
     */
    private Integer mainSku;

    /**
     * 活动的库存
     */
    private Integer marketingStock;

    /**
     * 供应商的skuid
     */
    private Long offerSkuId;

    /**
     * 淘系的skuid
     */
    private Long itemSkuId;

    /**
     * 淘系的skuname
     */
    private String itemSkuName;

    /**
     * 供应商的skuname
     */
    private String offerSkuName;

    /**
     * 淘系零售价，单位元
     */
    private Double retailPrice;

    /**
     * 佣金比例
     */
    private String commissionRate;

    /**
     * 供货价，单位元
     */
    private Double supplyPrice;

    /**
     * 划线价，单位元
     */
    private Double lineationPrice;

    /**
     * 渠道库存
     */
    private Long inventory;

    /**
     * 最小建议销量值
     */
    private Long minInvForecast;

    /**
     * 最大建议销量值
     */
    private Long maxInvForecast;
}
