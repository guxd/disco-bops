package com.alibaba.cbuscm.vo.marketing.plan;

import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import com.alibaba.cbuscm.vo.marketing.param.MaterialVO;
import com.alibaba.cbuscm.vo.marketing.param.QualificationVO;
import com.alibaba.cbuscm.vo.marketing.product.MarketingProductWhiteVO;
import com.alibaba.cbuscm.vo.marketing.product.ProductStatusNumRespVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 活动详情
 * @author luxuetao
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MarketingPlanDetailRespVO extends BaseVO {

    /**
     * 活动详情
     */
    private MarketingPlanRespVO marketingPlan;

    /**
     * 参与该活动的商品总数
     */
    private Integer total;

    /**
     *
     */
    private List<ProductStatusNumRespVO> statusNumList;


    /**
     * 白名单列表
     */
    private List<MarketingProductWhiteVO> marketingProductWhiteList;


    /**
     * 资质
     */
    private List<QualificationVO> qcList;

    /**
     * 素材要求
     */
    private List<MaterialVO> materials;
}
