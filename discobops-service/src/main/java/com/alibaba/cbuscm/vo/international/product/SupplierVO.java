package com.alibaba.cbuscm.vo.international.product;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 供应商视图模型
 * @author zhuoxian
 */
@Data
@ToString
public class SupplierVO implements Serializable {

    /**
     * 供应商ID
     */
    private String loginId;

    /**
     * 公司名称
     */
    private String company;
}
