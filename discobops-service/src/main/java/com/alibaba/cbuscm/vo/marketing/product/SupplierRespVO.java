package com.alibaba.cbuscm.vo.marketing.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 16:28
 * @Description:供应商缴费信息
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SupplierRespVO implements Serializable {
    private static final long serialVersionUID = 1944837732500462788L;

    private Long supplierId;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 店铺id
     */
    private Long sellerId;

    /**
     * 活动名称
     */
    private String marketingPlanName;

    /**
     * 商家memberId
     */
    private String memberId;

    /**
     * 收费类型
     */
    private String chargeType;

    /**
     * 收费金额
     */
    private String chargeParam;

    /**
     * 店铺名称
     */
    private String sellerName;

    /**
     * 供应商loginid
     */
    private String loginId;

    /**
     * 公司名称
     */
    private String companyName;
}
