package com.alibaba.cbuscm.vo.channel.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Auther: gxd
 * @Date: 2019/10/15 21:17
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelLogRespVO {

    private Long operateLogId;

    /**
     * 变更人
     */
    private String operator;

    /**
     * 变更时间
     */
    private Date createTime;

    /**
     * 变更类型
     */
    private String operateType;

    /**
     * 变更影响
     */
    private String operateEffect;
}
