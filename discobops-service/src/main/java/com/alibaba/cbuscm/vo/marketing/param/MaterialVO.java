package com.alibaba.cbuscm.vo.marketing.param;

import lombok.*;


/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class MaterialVO {
    /**
     * 素材名称
     */
    private String name;
    /**
     * 素材描述
     */
    private String desc;
    /**
     * 素材code
     */
    private String code;
    /**
     * 素材值
     */
    private String value;

    /**
     * 是否必须：0-非必须；1-必须
     */
    private Integer isRequire;

    /**
     * 素材类型
     */
    private String type;



}
