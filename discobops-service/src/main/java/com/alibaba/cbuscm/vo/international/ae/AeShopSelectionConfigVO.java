package com.alibaba.cbuscm.vo.international.ae;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zhuoxian
 */
@Getter
@Setter
@ToString
public class AeShopSelectionConfigVO {

    private Long taskId;
    private List<String> sites;
    private Greps greps;
    private BigDecimal priceMaxRatio;
    private String country;
    private List<Long> logistics;
    private Costs costs;

    @Getter
    @Setter
    @ToString
    public static class Greps {
        private Range salesQuantity;
        private Range rate;
        private Range like;
    }

    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Range {
        private BigDecimal min;
        private BigDecimal max;
    }

    @Getter
    @Setter
    @ToString
    public static class Costs {
        private CostDefine itemCost;
        private CostDefine logisticsCost;
        private CostDefine profitCost;
        private CostDefine otherCost;
    }

    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CostDefine {
        private Boolean checked;
        private BigDecimal ratio;
        private String basis;
    }
}
