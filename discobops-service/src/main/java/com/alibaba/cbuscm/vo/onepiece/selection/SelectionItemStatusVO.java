package com.alibaba.cbuscm.vo.onepiece.selection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SelectionItemStatusVO implements Serializable {

    private static final long serialVersionUID = 8172015170784227879L;

    private String downItemId;

    private Long offerId;

    private Long taskId;

    private String status;

}
