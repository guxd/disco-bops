package com.alibaba.cbuscm.vo.international.ae;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Created by duanyang.zdy on 2019/4/17.
 * AE商品圈选模型
 */
@Data
@Builder
@ToString
public class AeProductSelectVO implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * ae同款数量
     */
    private Long aeItemCount;

    /**
     * ae价格
     */
    private BigDecimal aePrice;

    /**
     * 预计售价
     */
    private BigDecimal estimatedPrice;

    /**
     * 产品利润
     */
    private String estimatedProfit;

    /**
     * 更新时间
     */
    private String  gmtModified;

   /**
     * 产品成本
     */
    private BigDecimal itemCost;

    /**
     * 关联ID
     */
    private Long relationId;

    /**
     * 任务ID
     */
    private Integer taskId;

    /**
     * 1688展示字段
     */
    private CbuOfferVO item;

    /**
     * 下游竞品展示字段
     */
    private DownItemVO siteItem;

    /**
     * 渠道信息
     */
    private LogisticsVO logistics;


}
