package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import lombok.Data;
import lombok.ToString;

/**
 * 日志VO
 */

@Data
@ToString
public class TaoSupplierLogVO {
    /**
     * 操作渠道场景 @see com.alibaba.china.shared.discosupplier.enums.SupplierSceneEnum
     */
    private String scene;

    /**
     * 操作模块 @see com.alibaba.china.shared.discosupplier.enums.operation.OperationModuleEnum
     * 后台应该跟 菜单唯一id结合起来
     */
    private String module;

    /**
     * 操作动作 如审核通过 审核拒绝 @see com.alibaba.china.shared.discosupplier.model.operation.OperationActionEnum
     */
    private String action;

    /**
     * 操作对象唯一id
     */
    private String itemId;
    /**
     * 操作人
     */
    private String operator;

    /**
     * 操作人级别 @see com.alibaba.china.shared.discosupplier.model.operation.OperationDegreeEnum
     */
    private String degree;

    /**
     * 操作数据内容-备注
     */
    private String content;

    /**
     * 操作时间
     */
    private Long gmtCreate;

}
