package com.alibaba.cbuscm.vo.disco;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.JSONArray;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * DcAuditInfoRecordVO
 *
 * @author aniu.nxh
 * @date 2019/7/23 12:51
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class DcAuditInfoRecordVO implements Serializable {
    /**
     * 操作时间
     */
    private Date gmtCreate;

    /**
     * 商品报名主键ID
     */
    private Long enrollId;

    /**
     * 商家ID
     */
    private Long userId;

    /**
     * 审核类型
     */
    private String auditType;

    /**
     * 审核文案，该字段DB没有，用于展示
     */
    private String auditTypeText;

    /**
     * 操作人ID
     */
    private String operator;

    /**
     * 操作人姓名
     */
    private String operatorName;

    /**
     * 原因
     */
    private String reason;

    /**
     * 快照
     */
    private JSONArray snapshot;
}
