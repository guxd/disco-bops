package com.alibaba.cbuscm.vo.common;


import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

/**
 * @author xuhb
 * @title: LogisticsSubVO
 * @projectName discobops
 * @description: TODO
 * @date 2019-05-2315:14
 */
public class LogisticsSubVO implements Comparable<LogisticsSubVO>{

    private String date;

    private List<LogisticsTraceStep> timeLine;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<LogisticsTraceStep> getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(List<LogisticsTraceStep> timeLine) {
        this.timeLine = timeLine;
    }

    @Override
    public int compareTo(LogisticsSubVO o) {
        return this.date.compareTo(o.date);
    }

}
