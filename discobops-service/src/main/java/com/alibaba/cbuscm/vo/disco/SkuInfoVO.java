package com.alibaba.cbuscm.vo.disco;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 类SkuInfoVO
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SkuInfoVO {

    private Long skuId;
    private String specId;
    private String desc;
    private String skuPrice;
    private String purchasePrice;
    private Long stock;
    private Long beginQuality;
    private List<String> barCode;
    private Integer weight;
    private Integer width;
    private Integer height;
    private Integer length;
    private String supplyModeDesc;

    /**
     * 是否是新增的，进行标记
     * 是：true
     * 否：false
     */
    private Boolean skuStatus;
}
