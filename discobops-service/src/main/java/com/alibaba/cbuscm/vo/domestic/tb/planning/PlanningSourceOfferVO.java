package com.alibaba.cbuscm.vo.domestic.tb.planning;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author xuhb
 * @title: PlanningSourceOfferVO
 * @projectName disco
 * @description: TODO
 * @date 2019-09-2022:41
 */
@Getter
@Setter
public class PlanningSourceOfferVO {

    /**
     * offer id
     */
    private Long offerId;

    /**
     * 标题
     */
    private String title;

    /**
     * 镇店之宝
     */
    private boolean bestOffer;

    /**
     * 皇冠
     */
    private boolean goldOffer;

    /**
     * 链接
     */
    private String link;

    /**
     * 图片链接
     */
    private List<String> picUrls;

    /**
     * 月销量
     */
    private Integer sales;

    /**
     * 公司
     */
    private String company;

    /**
     * 关联的企划
     */
    private String planningTitle;

    /**
     * 如果存在关联企划返回
     */
    private Long planningId;
}
