package com.alibaba.cbuscm.vo.marketing.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 16:14
 * @Description:活动商品
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketingProductVO implements Serializable {

    private static final long serialVersionUID = 5184410008347405832L;

    /**
     * 活动名称
     */
    private String marketingPlanName;

    /**
     * 活动id
     */
    private Long marketingPlanId;

    /**
     * 商家id
     */
    private Long loginId;

    /**
     * 淘宝商品标题
     */
    private String itemTitle;

    /**
     * 淘宝商品id
     */
    private Long itemId;

    /**
     * 淘宝商品图片
     */
    private String itemImg;

    /**
     * 1688商品信息
     */
    private String offerTitle;

    /**
     * 1688商品id
     */
    private Long offerId;

    /**
     * 1688商品图片
     */
    private String offerImg;

    /**
     * 报名时间开始
     */
    private Date enrollStartTime;

    /**
     * 报名时间结束
     */
    private Date enrollEndTime;

    /**
     * 上线时间开始
     */
    private Date effectiveStartTime;

    /**
     * 上线时间结束
     */
    private Date effectiveEndTime;

    /**
     * 报名活动状态：
     * 待一审；待二审；待配置缴费金额；待付款；
     * 待审核缴费；审核不通过；审核通过
     */
    private String status;

    /**
     * 行动点
     */
    private List<String> actionList;

    /**
     * 报名时间
     */
    private Date operateCreateTime;

}
