package com.alibaba.cbuscm.vo.international.product;

import com.alibaba.cbu.disco.shared.core.product.model.DcLogisticAttributes;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 产品SKU视图模型
 * @author zhuoxian
 */
@Getter
@Setter
public class ProductSkuVO implements Serializable {

    /**
     * 货品ID
     */
    private String id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态
     */
    private String status;

    /**
     * 状态描述
     */
    private String statusDesc;

    /**
     * 产品ID
     */
    private String productId;

    /**
     * 规格
     */
    private String desc;

    /**
     * 图片
     */
    private String image;

    /**
     * 物流属性
     */
    private DcLogisticAttributes logisticAttributes;

    /**
     * 供货价
     */
    private Long price;

    /**
     * 供货价币种
     */
    private String priceCurrency;

    /**
     * 业务类型
     */
    private String bizType;

    /**
     * 商品条形码
     */
    private String barCode;

    /**
     * 同步时间
     */
    private Date gmtSync;

    /**
     * ASCP货品ID
     */
    private String cnSkuId;

    /**
     * GSP商家产品SKU ID
     */
    private String gspSkuId;

    private String gspId;

    /**
     * 供应关系
     */
    private List<SupplyRelationVO> supplyRelationList;

    /**
     * 销售关系
     */
    private List<SalesRelationVO> salesRelationList;

    /**
     * 产品标题
     */
    private String productTitle;

    /**
     * 经营模式 经销/代销
     */
    private String supplyMode;

}
