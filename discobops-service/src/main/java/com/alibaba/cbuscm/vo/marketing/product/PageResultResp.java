package com.alibaba.cbuscm.vo.marketing.product;

import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

import java.util.List;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/19
 */
@Data
public class PageResultResp<T> extends BaseVO {

    /**
     * 数据
     */
    private List<T> data;

    /**
     * 总量
     */
    private int total;

    private int pageNo;

    private int pageSize;
}
