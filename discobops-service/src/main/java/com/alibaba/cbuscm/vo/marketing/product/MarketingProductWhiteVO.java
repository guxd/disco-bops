package com.alibaba.cbuscm.vo.marketing.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 15:46
 * @Description:白名单商品
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketingProductWhiteVO implements Serializable {

    private static final long serialVersionUID = -7218356509514887019L;

    /**
     * 淘系商品id
     */
    private Long itemId;

    /**
     *淘系商品图片
     */
    private String itemImg;

    /**
     * 淘系商品标题
     */
    private String itemTitle;

    /**
     * 商品白名单设置人
     */
    private String operator;

    /**
     *商品白名单添加时间
     */
    private Date operateCreateTime;
}
