package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/11.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrendsSubVO  implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;

    /**
     * 区域名称
     */
    private String key;

    /**
     *
     */
    private String value;
}
