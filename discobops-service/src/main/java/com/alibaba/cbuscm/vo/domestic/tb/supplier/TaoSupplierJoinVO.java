package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 供应商入住列表 vo
 *
 * @author yaogaolin 2019/4/28
 */

@Data
@ToString
public class TaoSupplierJoinVO {
    /**
     * 申请人
     */
    private String memberId;
    /**
     * 公司名
     */
    private String companyName;

    /**
     * 对接人钉钉号
     */
    private String dockingDing;

    /**
     * 优势品类
     */
    private List<CategoryViewVO> categories;

    /**
     * 入驻时间
     */
    private Long gmtJoin;

    /**
     * 最近操作人
     */
    private String recentOperator;

    /**
     * 审核状态
     */
    private String status;

    /**
     * 旺铺链接
     */
    private String winportUrl;

    /**
     * loginId
     */
    private String loginId;

    /**
     * 业务（供货主体）
     */
    private String objective;
    /**
     * 渠道
     */
    private String scene;

    /**
     * 是否有提报商品
     */
    private Boolean hasEnrollOffer;

    /**
     * 联系人旺旺
     */
    private String dockingWang;

    /**
     * 入驻记录id
     */
    private Long joinId;

    private Long ascpSupplierId;
}
