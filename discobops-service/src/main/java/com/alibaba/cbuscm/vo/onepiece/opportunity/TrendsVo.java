package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Created by duanyang.zdy on 2019/9/10.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrendsVo implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;

    /**
     * 区域名称
     */
    private String areaName;

    /**
     * 搜索热词
     */
    private String queryWord;

    /**
     * 本周搜索指数
     */
    private Double queryIndexThisWeek;

    /**
     * 上周搜索指数
     */
    private Double queryIndexLastWeek;

    /**
     * 搜索增长率
     */
    private Double wowIncRate;

    /**
     * 排名
     */
    private Integer rank;

    /**
     * 趋势(json)
     */
    private List<TrendsSubVO> trends;


}
