/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import java.util.Map;

/**
 * 类TapSupplierPayFilterVO.java的实现描述：保证金扣款的筛选条件vo
 * 
 * @author jizhi.qy 2019年5月26日 下午1:42:12
 */
public class TapSupplierPayFilterVO {

    /**
     * 支付状态
     */
    private Map<String, String> psyStatus;

    /**
     * 选择输入的内容属性
     */
    private Map<String, String> enterPropertyKey;

    public Map<String, String> getPsyStatus() {
        return psyStatus;
    }

    public void setPsyStatus(Map<String, String> psyStatus) {
        this.psyStatus = psyStatus;
    }

    public Map<String, String> getEnterPropertyKey() {
        return enterPropertyKey;
    }

    public void setEnterPropertyKey(Map<String, String> enterPropertyKey) {
        this.enterPropertyKey = enterPropertyKey;
    }

}
