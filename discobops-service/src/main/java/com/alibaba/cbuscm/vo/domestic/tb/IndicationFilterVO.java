/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb;

import java.util.Map;

/**
 * 类IndicationFilterVO.java的实现描述：获取用户在指标筛选的条件项
 * @author jizhi.qy 2019年4月18日 上午11:36:17
 */
public class IndicationFilterVO {
    
    /**
     * top产地货源
     */
    private Map<String, String> topOrigin;

    /**
     * 排序方式
     */
    private Map<String, String> sortBy;

    /**
     * 去重方式
     */
    private Map<String, String> duplicateRemoveType;

    public Map<String, String> getTopOrigin() {
        return topOrigin;
    }

    public void setTopOrigin(Map<String, String> topOrigin) {
        this.topOrigin = topOrigin;
    }

    public Map<String, String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Map<String, String> sortBy) {
        this.sortBy = sortBy;
    }

    public Map<String, String> getDuplicateRemoveType() {
        return duplicateRemoveType;
    }

    public void setDuplicateRemoveType(Map<String, String> duplicateRemoveType) {
        this.duplicateRemoveType = duplicateRemoveType;
    }

}
