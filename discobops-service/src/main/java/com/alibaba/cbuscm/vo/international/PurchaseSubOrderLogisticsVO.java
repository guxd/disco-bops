package com.alibaba.cbuscm.vo.international;

import java.util.List;

/**
 * @author xuhb
 * @title: PurchaseSubOrderLogisticsVO
 * @projectName discobops
 * @description: TODO
 * @date 2019-05-1413:55
 */
public class PurchaseSubOrderLogisticsVO {

    /**
     * 物流id
     */
    private String id;

    /**
     * 物流公司名称
     */
    private String companyName;

    /**
     * 物流状态
     */
    private String statusName;

    private String logisticsId;

    private String supplierId;


    /**
     * 子销售单信息
     */
    private List<PurchaseSubOrderVO> orderList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public List<PurchaseSubOrderVO> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<PurchaseSubOrderVO> orderList) {
        this.orderList = orderList;
    }

    public String getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(String logisticsId) {
        this.logisticsId = logisticsId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }
}
