package com.alibaba.cbuscm.vo.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xuhb
 * @title: LogisticsDetailVO
 * @projectName discobops
 * @description: 物流详情
 * @date 2019-05-1616:32
 */
public class LogisticsDetailVO {

    private String mailNo;

    private String companyName;

    private List<LogisticsSubVO> dateline  = new ArrayList<LogisticsSubVO>();

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<LogisticsSubVO> getDateline() {
        return dateline;
    }

    public void setDateline(List<LogisticsSubVO> dateline) {
        this.dateline = dateline;
    }

    public String getMailNo() {
        return mailNo;
    }

    public void setMailNo(String mailNo) {
        this.mailNo = mailNo;
    }
}
