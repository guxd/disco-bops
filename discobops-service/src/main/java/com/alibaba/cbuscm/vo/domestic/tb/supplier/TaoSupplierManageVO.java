package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import com.alibaba.china.shared.discosupplier.model.supplier.SupplierTagModel;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 供应商管理里列表VO
 *
 * @author yaogaolin 2019/4/28
 */
@Data
@ToString
public class TaoSupplierManageVO {
    /**
     * 申请人
     */
    private String memberId;
    /**
     * 公司名
     */
    private String companyName;

    /**
     * 对接人钉钉号
     */
    private String dockingDing;

    /**
     * 优势品类
     */
    private List<CategoryViewVO> categories;

    /**
     * 等级
     */
    private String level;

    /**
     * 30日千单次数
     */
    private Integer korderNum;
    /**
     * 30日千单爆款数
     */
    private Integer korderStyleNum;

    /**
     * 下游商品数
     */
    private Integer itemNum;

    /**
     * 评分
     */
    private Integer score;

    /**
     * 标签
     */
    private List<SupplierTagModel> tags;

    /**
     * 状态
     */
    private String status;

    /**
     * 清退时间
     */
    private Long gmtClear;

    /**
     * 清退原因
     */
    private String clearRemark;

    /**
     * 备注
     */
    private String remark;

    /**
     * 旺铺链接
     */
    private String winportUrl;

    /**
     * loginId
     */
    private String loginId;

    /**
     * 保证金状态
     *  DepositDisplayFieldEnum
     */
    private String depositStatus;

    /**
     * 合作商品数
     */
    private Integer coopGoodsNum;
    /**
     * 合作动销数
     */
    private Integer coopSalesNum;

    /**
     * 业务（供货主体）
     */
    private String objective;
    /**
     * 渠道
     */
    private String scene;

    /**
     * 联系人旺旺
     */
    private String dockingWang;
    /**
     * 是否缴纳保证金
     */
    private Boolean hasDebt;
    /**
     * 是否提报商品
     */
    private Boolean hasEnrollOffer;


}
