package com.alibaba.cbuscm.vo.disco;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 商品提报审核记录
 *
 * @author pengqiang.ai
 * @date 2019-06-10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class OfferProposalAuditLogVO implements Serializable {

    /**
     * 时间
     */
    private Date gmtCreate;

    /**
     * 审核人姓名
     */
    private String operatorName;

    /**
     * 审核信息
     */
    private String msg;

    /**
     * 操作结果
     */
    private String result;

    /**
     * 操作结果中文描述
     */
    private String resultDesc;
}