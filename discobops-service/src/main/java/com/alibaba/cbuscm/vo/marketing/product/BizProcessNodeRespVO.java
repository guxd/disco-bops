package com.alibaba.cbuscm.vo.marketing.product;

import lombok.*;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class BizProcessNodeRespVO {

    /**
     * 节点优先级
     */
    private Integer priority = 0;

    /**
     * 节点名称
     */
    private String nodeCode;

    /**
     * 节点描述
     */
    private String nodeDesc;
}
