package com.alibaba.cbuscm.vo.marketing.plan.req;

import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanInitDTO;
import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/11/14 10:15
 * @Description:
 */
@Data
public class MarketingPlanBasicInfoReqVO extends BaseVO {
    private Long marketingPlanId;
    private MarketingPlanInitDTO marketingPlanInitDTO;
}
