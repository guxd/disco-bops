package com.alibaba.cbuscm.vo.channel.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: gxd
 * @Date: 2019/10/15 21:06
 * @Description:渠道商品SKU
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelProductSKUResqVO {

    private Long skuId;
    /**
     *商品描述
     */
    private String desc;
    /**
     * 商品中台id
     */
    private Long skuCnId;
}
