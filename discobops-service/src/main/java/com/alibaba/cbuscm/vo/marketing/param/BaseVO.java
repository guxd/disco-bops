package com.alibaba.cbuscm.vo.marketing.param;

import com.alibaba.cbu.panama.dc.client.constants.marketingplan.ChargeTypeEnum;
import com.alibaba.cbu.panama.dc.client.constants.marketingplan.EnrollTypeEnum;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.InvManageDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Data
public class BaseVO implements Serializable,Cloneable{
    /**
     * Database Column Remarks:
     *   自增主键
     *
     *
     * @mbg.generated
     */
    private Long id;

    /**
     * Database Column Remarks:
     *   活动名称
     *
     *
     * @mbg.generated
     */
    private String marketingPlanName;

    /**
     * Database Column Remarks:
     *   所属业务市场id
     *
     * @see com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum
     * @mbg.generated
     */
    private String market;

    /**
     * Database Column Remarks:
     *   参与活动的门店
     *
     *
     * @mbg.generated
     */
    private List<String> sellerList;


    /**
     * Database Column Remarks:
     *   管理员id集合，用逗号隔开
     *
     *
     * @mbg.generated
     */
    private List<String> ownerList;

    /**
     * Database Column Remarks:
     *   报名类型,参考枚举
     * @see EnrollTypeEnum
     *
     * @mbg.generated
     */
    private String enrollType;

    /**
     * Database Column Remarks:
     *   活动描述
     *
     *
     * @mbg.generated
     */
    private String marketingPlanDesc;

    /**
     * Database Column Remarks:
     *   报名起始终时间
     *
     *
     * @mbg.generated
     */
    private Date enrollStartTime;

    /**
     * Database Column Remarks:
     *   报名截止时间
     *
     *
     * @mbg.generated
     */
    private Date enrollEndTime;

    /**
     * Database Column Remarks:
     *   活动生效时间
     *
     *
     * @mbg.generated
     */
    private Date effectiveStartTime;

    /**
     * Database Column Remarks:
     *   活动截止时间
     *
     *
     * @mbg.generated
     */
    private Date effectiveEndTime;

    /**
     * Database Column Remarks:
     *   收费类型(查询的条件，这里冗余字段处理)
     * @see ChargeTypeEnum
     *
     * @mbg.generated
     */
    private String chargeType;

    /**
     * Database Column Remarks:
     *   收费参数
     *
     *
     * @mbg.generated
     */
    private Double chargeParam;

    /**
     * Database Column Remarks:
     *   审核方式  1-人工审核
     *
     *
     * @mbg.generated
     */
    private String auditType;

    ///** 放基类里面了
    // * Database Column Remarks:
    // *   拓展字段
    // *
    // *
    // * @mbg.generated
    // */
    //private Map<String, String> extension;

    /**
     * Database Column Remarks:
     *   创建时间
     *
     *
     * @mbg.generated
     */
    private Date gmtCreate;

    /**
     * Database Column Remarks:
     *   最后修改时间
     *
     *
     * @mbg.generated
     */
    private Date gmtModified;

    /**
     * Database Column Remarks:
     *   活动状态，参考活动枚举value
     *
     *
     * @mbg.generated
     */
    private String status;

    /**
     * Database Column Remarks:
     *   是否有效0-有效 1-逻辑删除
     *
     *
     * @mbg.generated
     */
    private Integer isDelete;


    /**
     * Database Column creator:
     *   创建人
     *
     *
     * @mbg.generated
     */
    private String creator;

    /**
     * Database Column lastModifier:
     *   最后修改人
     *
     *
     * @mbg.generated
     */
    private String lastModifier;
    /**
     * Database Column Remarks:
     *   版本号
     *
     *
     * @mbg.generated
     */
    private Integer version;


    /**
     * 库存管控dto
     */
    private InvManageDTO invManageDTO;
}
