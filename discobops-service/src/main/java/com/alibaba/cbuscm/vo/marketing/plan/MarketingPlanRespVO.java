package com.alibaba.cbuscm.vo.marketing.plan;

import com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum;
import com.alibaba.cbu.panama.dc.client.constants.marketingplan.ChargeTypeEnum;
import com.alibaba.cbu.panama.dc.client.constants.marketingplan.EnrollTypeEnum;
import com.alibaba.cbuscm.vo.marketing.param.*;
import com.alibaba.cbuscm.vo.marketing.product.OperateActionRespVO;
import com.alibaba.cbuscm.vo.marketing.product.OwnerRespVO;
import lombok.*;

import java.util.List;

/**
 * 活动列表vo
 * @author luxuetao
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MarketingPlanRespVO {

    /**
     * 活动基础信息
     */
    private BaseVO baseInfo;
    /**
     * 白名单
     */
    private MarketingPlanWhiteItemVO marketingPlanWhiteItem;

    /**
     * 资质
     */
    private List<QualificationVO> qualificationList;

    /**
     * 素材
     */
    private List<MaterialVO> materialList;

    /**
     * 协议
     */
    private ProtocolVO protocol;
}
