package com.alibaba.cbuscm.vo.domestic.tb.planning;

import java.util.List;
import java.util.Set;

import com.alibaba.china.shared.discosupplier.enums.planning.opp.PlanningOppTypeEnum;

import lombok.Data;
import lombok.ToString;

/**
 * 企划寻源商机 vo
 *
 * @author yaogaolin 2019/4/28
 */

@Data
@ToString
public class PlanningOpportunityVO {

    /**
     * 寻源id
     */
    private Long sourceId;

    /**
     * 企划id
     */
    private Long planningId;

    /**
     * 企划标题
     */
    private String planningTitle;

    /**
     * 供应商memberId
     */
    private String memberId;

    /**
     * 登录名
     */
    private String loginId;
    /**
     * 公司名
     */
    private String companyName;

    /**
     * 地址
     */
    private String address;

    /**
     * 供应商userId
     */
    private long userId;

    /**
     * 排序分值属性
     */
    private int score;

    /**
     * 寻源类型 逗号分隔
     */
    //private Set<PlanningOppTypeEnum> types;
    private List<String> types;

    /**
     * 商机状态
     */
    private String status;

    /**
     * 寻品信息json
     */
    private List<PlanningOppItemVO> opps;

    /**
     * 当前登录人是否可以修改该商机
     */
    private Boolean canEdit;
}
