/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonRawValue;

/**
 * 类IndicationVO.java的实现描述：TODO 类实现描述 
 * @author jizhi.qy 2019年4月18日 下午12:07:04
 */
public class IndicationVO {

    private Long taskId;

    private Long indicationId;

    private String indicationName;

    private Integer totalCount;

    private List<Property> properties;

    /**
     * 商家维度表单
     */
    @JsonRawValue
    private String shopForm;

    /**
     * 商品维度表单
     */
    @JsonRawValue
    private String offerForm;

    /**
     * 相似商品链接
     */
    private String similarLink;

    public String getSimilarLink() {
        return similarLink;
    }

    public void setSimilarLink(String similarLink) {
        this.similarLink = similarLink;
    }

    public String getIndicationName() {
        return indicationName;
    }

    public void setIndicationName(String indicationName) {
        this.indicationName = indicationName;
    }

    public Long getIndicationId() {
        return indicationId;
    }

    public void setIndicationId(Long indicationId) {
        this.indicationId = indicationId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    public String getShopForm() {
        return shopForm;
    }

    public void setShopForm(String shopForm) {
        this.shopForm = shopForm;
    }

    public String getOfferForm() {
        return offerForm;
    }

    public void setOfferForm(String offerForm) {
        this.offerForm = offerForm;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public static class Property {
        // 属性名称
        private String name;
        // 属性值
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }
}

