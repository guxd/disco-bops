package com.alibaba.cbuscm.vo.domestic.tb.planning;

import com.alibaba.china.shared.discosupplier.model.common.UserModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningAttr;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningCompetitor;
import com.alibaba.offer.tools.excel.annatation.ExcelColumn;
import lombok.Data;
import lombok.ToString;

import java.util.*;

/**
 * @author wb-qiuth
 * 导入企划EXCEL字段
 */
@Data
@ToString
public class PlanningDesignImportVO {
    @ExcelColumn(name = "企划类型", index = 1)
    private String type;
    @ExcelColumn(name = "企划名称", index = 2)
    private String title;
    @ExcelColumn(name = "企划描述", index = 3)
    private String desc;
    @ExcelColumn(name = "提报渠道", index = 4)
    private String scene;
    @ExcelColumn(name = "引流转化预测", index = 5)
    private String drainagePred;
    @ExcelColumn(name = "企划定位", index = 6)
    private String orientation;
    @ExcelColumn(name = "企划小二", index = 7)
    private String admin;
    @ExcelColumn(name = "寻源小二", index = 8)
    private String handler;
    @ExcelColumn(name = "企划波段", index = 9)
    private String waveBrand;
    @ExcelColumn(name = "上线时间", index = 10)
    private Date upTime;
    @ExcelColumn(name = "淘系叶子类目ID", index = 11)
    private String categoryId;
    @ExcelColumn(name = "核心卖点", index = 12)
    private String sellPoint;
    @ExcelColumn(name = "规格范围", index = 13)
    private String specScope;
    @ExcelColumn(name = "预期转化率", index = 14)
    private String expConvertRate;
    @ExcelColumn(name = "日销预计", index = 15)
    private String dailySales;
    @ExcelColumn(name = "配给流量", index = 16)
    private String distriFlow;
    @ExcelColumn(name = "提报数量下限", index = 17)
    private Integer importLower;
    @ExcelColumn(name = "提报数量上限", index = 18)
    private Integer importUp;
    @ExcelColumn(name = "商家提报上限", index = 19)
    private Integer importMax;
    @ExcelColumn(name = "备注", index = 20)
    private String remark;
    @ExcelColumn(name = "竞品链接1", index = 21)
    private String url1;
    @ExcelColumn(name = "竞品链接2", index = 22)
    private String url2;
    @ExcelColumn(name = "竞品链接3", index = 23)
    private String url3;
    @ExcelColumn(name = "竞品链接4", index = 24)
    private String url4;
    @ExcelColumn(name = "竞品链接5", index = 25)
    private String url5;
    @ExcelColumn(name = "竞品链接6", index = 26)
    private String url6;
    @ExcelColumn(name = "属性值1", index = 27)
    private String keyAttrs1;
    @ExcelColumn(name = "属性值2", index = 28)
    private String keyAttrs2;
    @ExcelColumn(name = "属性值3", index = 29)
    private String keyAttrs3;
    @ExcelColumn(name = "属性值4", index = 30)
    private String keyAttrs4;
    @ExcelColumn(name = "属性值5", index = 31)
    private String keyAttrs5;
    @ExcelColumn(name = "属性值6", index = 32)
    private String keyAttrs6;
    @ExcelColumn(name = "属性值7", index = 33)
    private String keyAttrs7;
    @ExcelColumn(name = "属性值8", index = 34)
    private String keyAttrs8;
    @ExcelColumn(name = "属性值9", index = 35)
    private String keyAttrs9;
    @ExcelColumn(name = "属性值10", index = 36)
    private String keyAttrs10;
    @ExcelColumn(name = "规格及指导价1", index = 37)
    private String spec1;
    @ExcelColumn(name = "规格及指导价2", index = 38)
    private String spec2;
    @ExcelColumn(name = "规格及指导价3", index = 39)
    private String spec3;
    @ExcelColumn(name = "规格及指导价4", index = 40)
    private String spec4;
    @ExcelColumn(name = "规格及指导价5", index = 41)
    private String spec5;
    @ExcelColumn(name = "规格及指导价6", index = 42)
    private String spec6;
    @ExcelColumn(name = "规格及指导价7", index = 43)
    private String spec7;
    @ExcelColumn(name = "规格及指导价8", index = 44)
    private String spec8;
    @ExcelColumn(name = "规格及指导价9", index = 45)
    private String spec9;
    @ExcelColumn(name = "规格及指导价10", index = 46)
    private String spec10;
    @ExcelColumn(name = "关联offerIds", index = 47)
    private String offerIds;
    @ExcelColumn(name = "来源平台", index = 48)
    private String origin;
    @ExcelColumn(name = "来源id", index = 49)
    private String originId;
    private List<PlanningCompetitor> competitors;
    private Set<UserModel> handlers;
    private Set<UserModel> admins;
    private Set<PlanningAttr> keyAttrs;
    private Set<PlanningAttr> specs;
    private String status;
    private String categoryValue;
    private Set<Long> offerIdSet;
}
