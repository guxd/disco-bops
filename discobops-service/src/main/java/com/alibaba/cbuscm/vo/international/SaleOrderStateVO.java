package com.alibaba.cbuscm.vo.international;

/**
 * @author xuhb
 * @title: SaleOrderStateVO
 * @projectName discobops
 * @description: 销售订单vo
 * @date 2019-05-0822:01
 */
public class SaleOrderStateVO {


    /**
     * 状态显示名称
     */
    private String name;

    /**
     * 状态id
     */
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
