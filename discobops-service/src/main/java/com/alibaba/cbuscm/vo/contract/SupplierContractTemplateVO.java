package com.alibaba.cbuscm.vo.contract;

import java.io.Serializable;

/**
 * @description:
 * @author: lanqing.hlq
 * @date: 2019-08-14 12:55
 **/
public class SupplierContractTemplateVO implements Serializable {

    /**
     * Database Column Remarks:
     * 模板id
     */
    private Long id;

    /**
     * Database Column Remarks:
     * 性质（业务/普通）
     */
    private String property;

    /**
     * Database Column Remarks:
     * 协议名称
     *
     * @mbg.generated
     */
    private String name;

    /**
     * Database Column Remarks:
     * 协议url
     *
     * @mbg.generated
     */
    private String url;

    /**
     * Database Column Remarks:
     * 模板状态
     *
     * @mbg.generated
     */
    private String status;

    /**
     * Database Column Remarks:
     * 供货主体-对象
     *
     * @mbg.generated
     */
    private String objective;

    /**
     * Database Column Remarks:
     * 供货渠道-对象
     *
     * @mbg.generated
     */
    private String scene;


    /**
     * @return the value of supplier_contract_template.name
     * @mbg.generated
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the value for supplier_contract_template.name
     * @mbg.generated
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value of supplier_contract_template.url
     * @mbg.generated
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the value for supplier_contract_template.url
     * @mbg.generated
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the value of supplier_contract_template.status
     * @mbg.generated
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the value for supplier_contract_template.status
     * @mbg.generated
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the value of supplier_contract_template.objective
     * @mbg.generated
     */
    public String getObjective() {
        return objective;
    }

    /**
     * @param objective the value for supplier_contract_template.objective
     * @mbg.generated
     */
    public void setObjective(String objective) {
        this.objective = objective;
    }

    /**
     * @return the value of supplier_contract_template.scene
     * @mbg.generated
     */
    public String getScene() {
        return scene;
    }

    /**
     * @param scene the value for supplier_contract_template.scene
     * @mbg.generated
     */
    public void setScene(String scene) {
        this.scene = scene;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
