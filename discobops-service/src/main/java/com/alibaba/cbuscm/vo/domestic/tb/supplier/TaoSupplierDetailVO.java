package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import com.alibaba.china.shared.discosupplier.model.supplier.BrandInfoModel;
import com.alibaba.china.shared.discosupplier.model.supplier.CategoryModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierTagModel;
import com.alibaba.china.shared.discosupplier.model.supplier.Warehouse;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 供应商 详情 VO
 *
 * @author yaogaolin 2019/4/28
 */
@Data
@ToString
public class TaoSupplierDetailVO {
    /**
     * 申请人
     */
    private String memberId;

    /**
     * 状态 入驻状态+管理状态合在一起
     */
    private String status;

    /*--供应商资质信息 start---*/

    /**
     * 公司名
     */
    private String companyName;
    /**
     * 审核记录
     */
    private String auditRemark;

    /**
     * 入驻时间
     */
    private Long gmtJoin;

    /**
     * 优势品类
     */
    private List<CategoryViewVO> categories;

    /**
     * 对接人姓名
     */
    private String dockingName;

    /**
     * 对接人电话
     */
    private String dockingPhone;
    /**
     * 对接人钉钉号
     */
    private String dockingDing;

    /**
     * 对接美工电话
     */
    private String dockingArtistPhone;

    /**
     * 是否有天猫店铺
     */
    private Integer tmallExist;

    /**
     * 天猫店铺链接
     */
    private String tmallUrl;

    /**
     * 合作快递
     */
    private String express;

    /**
     * 退货联系人
     */
    private String returnsName;

    /**
     * 退货联系人电话
     */
    private String returnsPhone;

    /**
     * 退货联系人钉钉号
     */
    private String returnsDing;

    /**
     * 退货地址
     */
    private String returnsAddress;

    /**
     * 注册资金(万)
     */
    private Integer registeredCapital;

    /**
     * 工厂工人数
     */
    private Integer workerNum;

    /**
     * 厂房面积(平方米)
     */
    private Integer factoryArea;

    /**
     * 工厂日产量
     */
    private Integer factoryAmount;

    /**
     * 工厂发货数量
     */
    private Integer factoryDeliveryAmount;

    /**
     * 工厂地址
     */
    private String factoryAddress;

    /**
     * 品牌资质信息
     */
    private List<BrandInfoModel> brandInfo;

    /**
     * 是否有打单机
     */
    private Integer printExist;

    /**
     * 验厂报告链接
     */
    private String auditFactoryUrl;

    /**
     * 发货能力凭证
     */
    private List<String> deliveryCertificate;

    /**
     * 打单机合照凭证
     */
    private List<String> printCertificate;

    /**
     * 电器3c证书凭证
     */
    private List<String> electricDeviceCertificate;

    /**
     * 天猫特殊行业凭证
     */
    private List<String> tmallSpecCertificate;

    /*--供应商管理信息 start---*/

    /**
     * 供应商管理状态
     */
    //private String status;

    /**
     * 等级
     */
    private String level;

    /**
     * 30日千单次数
     */
    private Integer korderNum;
    /**
     * 30日千单爆款数
     */
    private Integer korderStyleNum;

    /**
     * 下游商品数
     */
    private Integer itemNum;

    /**
     * 评分
     */
    private Integer score;

    /**
     * 标签
     */
    private List<SupplierTagModel> tags;

    /**
     * 清退时间
     */
    private Long gmtClear;

    /**
     * 清退原因
     */
    private String clearRemark;

    /**
     * 备注
     */
    private String remark;

    /**
     * 管理软件凭证
     */
    private List<String> softwareCertificate;

    /**
     * 是否支持一键代发
     */
    private Boolean substitute;

    /**
     * 是否是保险单
     */
    private Boolean policy;

    /**
     * 保证金额度
     */
    private Double occupyAmount;

    /**
     * 营业执照
     */
    private List<String> license;

    /**
     * 是否需要发票
     */
    private Boolean supportInvoice;

    /**
     * 旺铺链接
     */
    private String winportUrl;

    /**
     * loginId
     */
    private String loginId;
    // ------------- add by SupplierInfoModel start -------
    /**
     * 公司地址所在区
     */
    private String companyRegion;
    /**
     * 公司详细地址
     */
    private String companyAddress;
    /**
     * 公司性质
     */
    private String companyType;
    /**
     * 营业执照号
     */
    private String licenseNo;
    /**
     * 企业法人姓名
     */
    private String ligalName;
    /**
     * 企业法人身份证号
     */
    private String ligalNo;
    /**
     * 企业支付宝账号
     */
    private String companyAlipay;
    /**
     * 企业支付宝账号loginId
     */
    private String companyAlipayLoginId;
    /**
     * 联系人邮箱
     */
    private String dockingMail;
    /**
     * 联系人旺旺
     */
    private String dockingWang;
    /**
     * 是否能开增值税发票
     */
    private Boolean vatInvoice;
    /**
     * 默认发货地址
     */
    private String shippingAddress;
    /**
     * 主营类目  类目id和名称，逗号隔开
     */
    private CategoryModel mainCategory;
    /**
     * 银行账号
     */
    private String accountNo;
    /**
     * 开户名
     */
    private String accountName;
    /**
     * 开户银行
     */
    private String accountBank;
    /**
     * 开户支行
     */
    private String accountBranch;
    // ------------- add by SupplierInfoModel end -------

    // ------------- add by SupplierInfoExtInModel start -------
    /**
     * 工厂日最高发货量
     */
    private Integer factoryMaxAmount;
    /**
     * 商品合格证
     */
    private List<String> productCertificate;

    //天天特卖美妆店额外补充资料
    /**
     * 国产非特殊用途的化妆品备案凭证
     */
    private List<String> commonCosmetics;
    /**
     * 防爆证明
     */
    private List<String> explosionProofCertificate;
    // ------------- add by SupplierInfoExtInModel end -------

    // ------------- add by SupplierManageModel  start -------
    /**
     * 业务（供货主体）
     */
    private String objective;
    /**
     * 渠道
     */
    private String scene;
    /**
     * ascp供应商编码
     */
    private Long ascpSupplierId;
    // ------------- add by SupplierManageModel  end -------

    // ------------- add by SupplierInfoExtOutModel start -------
    /**
    /**
     * 纳税人资格证明
     */
    private List<String> taxpayerQualificationCertificate;
    /**
     * 版权证
     */
    private List<String> copyrightCertificate;
    /**
     * 专利证书
     */
    private List<String> patentCertificate;
    /**
     * 条码证明
     */
    private String barCodeProof;
    /**
     * 正规检测部门出具的当年合格检测报告
     */
    private List<String> qualificationTestReport;
    /**
     * 产品执行标准
     */
    private String implementationCriteria;
    /**
     * 是否可提供or定制商品英文包装
     */
    private Boolean englishPackaging;
    /**
     * 是否能提供形式发票
     */
    private Boolean canProformaInvoice;
    /**
     * 优势供货渠道
     */
    private String advanceSupply;
    /**
     * 是否是工厂
     */
    private Boolean factorySign;
    // ------------- add by SupplierInfoExtOutModel end -------

    //----------------------caigou ext into start-------------------------
    private CaigouSupplierDetailExtVO caigouSupplierDetailExtVO;
    //----------------------caigou ext into end-------------------------

}
