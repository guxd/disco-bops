package com.alibaba.cbuscm.vo.marketing.product;

import com.alibaba.cbuscm.vo.marketing.param.MarketingSkuVO;
import com.alibaba.cbuscm.vo.marketing.param.MaterialVO;
import com.alibaba.cbuscm.vo.marketing.plan.MarketingPlanEnrolledRespVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 16:38
 * @Description:报名活动商品性情
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketingProductDetailRespVO implements Serializable {
    private static final long serialVersionUID = 6821122659149118361L;

    private Long marketingPlanId;

    /**
     * 当前佣金比例
     * 整数，比如佣金为：15%，给的值是15
     */
    private Double currentRate;

    /**
     * 活动期间比例
     * 整数，比如佣金为：15%，给的值是15
     */
    private Double marketingProcessingRate;

    /**
     * 活动之后佣金比例
     * 整数，比如佣金为：15%，给的值是15
     */
    private Double afterMarketingRate;

    /**
     * 商家memberId
     */
    private String memberId;

    /**
     * 公司名称
     */
//    private String companyName;

    /**
     * 淘宝商品id
     */
    private Long itemId;

    /**
     * 淘宝商品图片
     */
    private String itemImg;

    /**
     * 淘系商品标题
     */
    private String itemTitle;

    /**
     * 1688商品id
     */
    private Long offerId;

    /**
     * 1688商品信息
     */
//    private String offerTitle;

    /**
     * 1688商品图片
     */
//    private String offerImg;

    /**
     *活动名称
     */
    private String marketingPlanName;


    /**
     * 提报状态
     */
    private String status;

    /**
     * 提报状态信息
     */
    private String statusMessage;


    /**
     * 保底销量
     */
    private Integer guaranteedSales;

    /**
     * 预期销量
     */
    private Integer expectedSales;

    /**
     * 提报商品
     */
//    private MarketingProductVO marketingProductVO;


    /**
     * 活动商品sku
     */
    private List<MarketingSkuVO> marketingProductSKUVOList;

    /**
     * 供应商loginid
     */
    private String loginId;

    /**
     * 商家信息
     */
    private SupplierRespVO supplierRespVO;

    /**
     *素材要求
     */
    private List<MaterialVO> materialVO;

    /**
     * 审核记录
     */
    private List<OperateLogRespVO> operateLogRespVO;

    /**
     * 该商品报名的其他活动
     */
    private List<MarketingPlanEnrolledRespVO> marketingPlanEnrolledList;


    private List<OperateActionRespVO> actionList;


    /**
     *报名起始终时间
     */
    private Date enrollStartTime;

    /**
     *  报名截止时间
     */
    private Date enrollEndTime;

    /**
     * 活动上线时间开始
     */
    private Date effectiveStartTime;

    /**
     *活动上线时间结束
     */
    private Date effectiveEndTime;

    /**
     * 收费方式
     */
    private String chargeType;

    /**
     * 收费金额或比例
     */
    private Double param;

    /**
     * 活动创建时间
     */
    private Date gmtCreate;
}
