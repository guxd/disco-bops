package com.alibaba.cbuscm.vo.disco;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningProposalModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 类OfferProposalDetailVO
 *
 * @author ruikun.xrk
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OfferProposalDetailVO {

    private Long id;
    private Date gmtCreate;
    private String status;
    private String statusDesc;
    private Long offerId;
    private String image;
    private String title;
    private String bizType;
    private String memberId;
    private String loginId;
    private String company;
    private Double minPayPrice30;
    private Double minPayPrice180;
    private List<SkuInfoVO> skuInfo;
    private Map<String, Object> extra;
    private OperatorModel operator;
    private List<String> catePath;
    private List<OfferProposalAuditLogVO> auditLogs;
    /**
     * add by sean 2019.08.15 增加商品标来源字段
     */
    private String offerSource;
    //add by xhb 添加企划信息
    private PlanningProposalModel planning;
}
