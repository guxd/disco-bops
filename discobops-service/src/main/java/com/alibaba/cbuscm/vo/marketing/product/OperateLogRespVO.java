package com.alibaba.cbuscm.vo.marketing.product;

import com.alibaba.cbuscm.vo.channel.req.ModifiedDetailVO;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Auther: guxiaodong
 * @Date: 2019/10/10 10:08
 * @Description:
 */
@Data
public class OperateLogRespVO {
    /**
     * 流程阶段
     */
    private String status;

    /**
     * 操作流程说明
     */
    private String statusMessage;

    /**
     * 变更项目：比如商品成品调整；仓库成本调整；汇率调整
     */
    private String operateType;

    /**
     * 变更人
     */
    private String operator;

    /**
     * 备注
     */
    private String remark;

    /**
     * 变更时间
     */
    private Date createTime;

    /**
     *变更影响
     */
    private List<ModifiedDetailVO> operateModifies;

    /**
     * 上升、下降、持平；比如：商品价格从10变更到12，则为上升
     */
    private String trend;

}
