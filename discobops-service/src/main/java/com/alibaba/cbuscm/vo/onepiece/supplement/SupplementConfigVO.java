package com.alibaba.cbuscm.vo.onepiece.supplement;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 类SupplementConfigVO
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SupplementConfigVO implements Serializable {

    private Integer supplyProgress;
    private Long proposalCount;
    private Long supplyCount;
    private Long supplyLimit;
}
