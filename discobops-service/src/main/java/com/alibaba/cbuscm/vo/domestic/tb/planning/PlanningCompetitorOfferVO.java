package com.alibaba.cbuscm.vo.domestic.tb.planning;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author xuhb
 * @title: PlanningCompetitorOfferVO
 * @projectName discosupplier
 * @description: 竞品数据
 * @date 2019-08-2313:22
 */
@Data
@ToString
public class PlanningCompetitorOfferVO {

    private String title;

    private String price;

    private String from;

    private List<String> picUrls;

    private String url;

    private Long itemId;
}
