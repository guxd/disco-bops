package com.alibaba.cbuscm.vo.marketing.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 16:38
 * @Description:报名活动商品性情
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketingProductDetailVO implements Serializable {
    private static final long serialVersionUID = 6821122659149118361L;

    /**
     * 活动名称
     */
    private String marketingPlanName;

    /**
     *上线时间开始
     */
    private Date effectiveStartTime;

    /**
     * 上线时间结束
     */
    private Date effectiveEndTime;

    /**
     * 提报状态
     */
    private String status;

    /**
     * 行动点
     */
    private String actionList;

    /**
     * 保底销量
     */
    private Integer guaranteedSales;

    /**
     * 预期销量
     */
    private Integer expectedSales;

    /**
     * 淘系商品图片
     */
    private String itemImg;

    /**
     * 淘系商品标题
     */
    private String itemTitle;

    /**
     * 淘系商品id
     */
    private Long itemId;

    /**
     * 1688商品id
     */
    private Long offerId;

    /**
     * 活动商品sku
     */
    private List<MarketingProductSKUVO> marketingProductSKUVOList;



}
