package com.alibaba.cbuscm.vo.international;

import com.alibaba.cbu.disco.shared.core.order.model.data.FsPurchaseSubExtra;

/**
 * @author xuhb
 * @title: SaleSubOrderVO
 * @projectName discobops
 * @description: 销售子订单信息
 * @date 2019-05-1414:16
 */
public class PurchaseSubOrderVO {
    /**
     * 商品id
     */
    private String productId;

    /**
     * 渠道物品id
     */
    private String supplierProductId;

    /**
     * 订单子ID
     * 必填
     */
    private String orderSubId;

    /**
     * 采购单offerId
     */
//    private String purchaseOfferId;

    /**
     * offer title
     */
    private String offerTitle;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 收获数量
     */
    private int receiveAmount;

    /**
     * 采购价
     */
    private Double total;

    /**
     * 单价
     */
    private Double price;
    /**
     * sku规格，形如：颜色:红色;尺寸:S码
     */
    private String spec;

    /**
     * 订单的退款相关状态
     */
    private String refundStatus;

    /**
     * 订单的退款相关文案
     */
    private String refundDesc;

    /**
     * errorCode
     */
    private String errorCode;

    /**
     * errorMsg
     */
    private String errorMsg;

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getReceiveAmount() {
        return receiveAmount;
    }

    public void setReceiveAmount(int receiveAmount) {
        this.receiveAmount = receiveAmount;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSupplierProductId() {
        return supplierProductId;
    }

    public void setSupplierProductId(String supplierProductId) {
        this.supplierProductId = supplierProductId;
    }

    public String getOrderSubId() {
        return orderSubId;
    }

    public void setOrderSubId(String orderSubId) {
        this.orderSubId = orderSubId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRefundDesc() {
        return refundDesc;
    }

    public void setRefundDesc(String refundDesc) {
        this.refundDesc = refundDesc;
    }
}
