package com.alibaba.cbuscm.vo.onepiece.supplement;

import java.io.Serializable;
import java.util.List;

import com.alibaba.china.global.business.library.models.selection.CbuOfferModel;
import com.alibaba.china.global.business.library.models.selection.DsItemModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 类SupplementItemReviewVO
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SupplementItemAuditVO implements Serializable {

    private CbuOfferModel cbuItem;
    private DsItemModel dsItem;
    private SupplementConfigVO config;
    private List<SupplementProposalVO> proposalList;

}
