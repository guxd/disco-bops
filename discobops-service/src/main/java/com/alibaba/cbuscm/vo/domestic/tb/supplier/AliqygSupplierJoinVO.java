package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.ToString;

/**
 * Created by qinwei on 2019/9/9.
 *
 * @author qinwei
 * @date 2019/09/09
 */
@Data
@ToString
public class AliqygSupplierJoinVO extends TaoSupplierJoinVO{

    /**
     * 主营类目
     */
    private String mainCategory;

    /**
     * 分销模式
     */
    private List<String> distributionMode;



}
