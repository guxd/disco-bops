package com.alibaba.cbuscm.vo.onepiece.selection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SelectionExportVO implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;

    private Long taskId;

    private String sortField;

    private String status;


}
