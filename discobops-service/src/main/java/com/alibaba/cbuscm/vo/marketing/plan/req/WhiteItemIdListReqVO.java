package com.alibaba.cbuscm.vo.marketing.plan.req;

import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/11/6 15:30
 * @Description:
 */
@Data
public class WhiteItemIdListReqVO extends BaseVO {
    private Long marketingPlanId;
    private List<String> itemIdList;
    private String operator;

}
