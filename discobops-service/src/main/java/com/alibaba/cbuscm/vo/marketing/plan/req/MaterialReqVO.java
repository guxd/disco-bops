package com.alibaba.cbuscm.vo.marketing.plan.req;

import com.alibaba.cbu.panama.dc.client.model.marketingplan.MaterialDTO;
import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/11/6 15:21
 * @Description:
 */
@Data
public class MaterialReqVO extends BaseVO {
    private Long marketingPlanId;
    private MaterialDTO material;
    private String materialCode;
}
