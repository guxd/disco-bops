package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import java.io.Serializable;
import java.util.List;

import com.alibaba.china.shared.discosupplier.model.supplier.Warehouse;

/**
 * Created by qinwei on 2019/10/10.
 *
 * 阿里企业购供应商详情 扩展信息
 * @author qinwei
 * @date 2019/10/10
 */
public class CaigouSupplierDetailExtVO implements Serializable{
    private static final long serialVersionUID = 482850793317310439L;

    /**
     * 是否有1688店铺
     */
    private Integer shop1688Exist;

    /**
     * 1688店铺链接
     */
    private String shop1688Url;

    /**
     * 电商运营人数
     */
    private Integer operatorNums;
    /**
     * 电商平台日订单量
     */
    private Integer orderEveryDay;

    /**
     * 仓库
     */
    private List<Warehouse> warehouseList;

    /**
     * 行业资质凭证
     */
    private List<String> industryCertificate;


    /**
     * 分销模式
     */
    private List<String> distribution;
    /**
     * 交易方式
     */
    private List<String> trade;

    public Integer getShop1688Exist() {
        return shop1688Exist;
    }

    public void setShop1688Exist(Integer shop1688Exist) {
        this.shop1688Exist = shop1688Exist;
    }

    public String getShop1688Url() {
        return shop1688Url;
    }

    public void setShop1688Url(String shop1688Url) {
        this.shop1688Url = shop1688Url;
    }

    public Integer getOperatorNums() {
        return operatorNums;
    }

    public void setOperatorNums(Integer operatorNums) {
        this.operatorNums = operatorNums;
    }

    public Integer getOrderEveryDay() {
        return orderEveryDay;
    }

    public void setOrderEveryDay(Integer orderEveryDay) {
        this.orderEveryDay = orderEveryDay;
    }

    public List<Warehouse> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<Warehouse> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public List<String> getIndustryCertificate() {
        return industryCertificate;
    }

    public void setIndustryCertificate(List<String> industryCertificate) {
        this.industryCertificate = industryCertificate;
    }

    public List<String> getDistribution() {
        return distribution;
    }

    public void setDistribution(List<String> distribution) {
        this.distribution = distribution;
    }

    public List<String> getTrade() {
        return trade;
    }

    public void setTrade(List<String> trade) {
        this.trade = trade;
    }
}
