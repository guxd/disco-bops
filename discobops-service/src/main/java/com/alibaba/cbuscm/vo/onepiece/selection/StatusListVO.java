package com.alibaba.cbuscm.vo.onepiece.selection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StatusListVO implements Serializable {

    private static final long serialVersionUID = -6613283372066272625L;
    private List<SelectionItemStatusVO> list;
    private List<String> channelIds;
}
