package com.alibaba.cbuscm.vo.channel.req;

import com.alibaba.cbuscm.vo.channel.WarehouseRespVO;
import lombok.Data;

import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/10/16 20:39
 * @Description:
 */
@Data
public class ChannelModifyReq {

    /**
     *仓库成本 = 仓储费 + 打包费 +
     */
    private List<WarehouseRespVO> warehouseParam;

    /**
     * 渠道价格id
     */
    private Long channelProductPriceId;

    /**
     * 毛利率
     */
    private Integer grossProfitRate;

    /**
     * 供货价
     */
    private Double supplyPrice;
}
