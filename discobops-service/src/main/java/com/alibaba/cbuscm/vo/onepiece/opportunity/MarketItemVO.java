package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;
import java.util.List;

import com.alibaba.cbuscm.vo.common.PageVo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/3.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketItemVO extends PageVo implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;

    /**
     * 市场ID
     */
    private String marketId;

    /**
     * 市场名称
     */
    private String marketName;

    /**
     * 通讯表日期
     */
    private String stat_date;

    /**
     * 商品列表
     */
    private List<ItemVO> data;
}
