package com.alibaba.cbuscm.vo.international.ae;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Created by duanyang.zdy on 2019/4/17.
 */
@Data
@ToString
@Builder
public class CbuOfferVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long  itemId;

    private String pic;

    private String title;

    private String itemUrl;
}
