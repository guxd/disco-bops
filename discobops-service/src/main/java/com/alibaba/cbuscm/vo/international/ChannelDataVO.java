package com.alibaba.cbuscm.vo.international;


public class ChannelDataVO {
    private String channelCode;
    private String channelSubMarket;
    private String channelName;
    private String channelSubName;
    /**
     * 店铺唯一ID
     */
    private Long shopId;
    /**
     * 店铺名
     */
    private String shopName;

    private String channelSellerId;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelSubMarket() {
        return channelSubMarket;
    }

    public void setChannelSubMarket(String channelSubMarket) {
        this.channelSubMarket = channelSubMarket;
    }

    public String getChannelSellerId() {
        return channelSellerId;
    }

    public void setChannelSellerId(String channelSellerId) {
        this.channelSellerId = channelSellerId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelSubName() {
        return channelSubName;
    }

    public void setChannelSubName(String channelSubName) {
        this.channelSubName = channelSubName;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
