package com.alibaba.cbuscm.vo.international.product;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 类ChannelProductVO
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Getter
@Setter
public class ChannelProductVO {

    private Long id;
    private Date gmtCreate;
    private Date gmtModified;
    private String status;
    private String title;
    private String image;
    private String detail;
    private String bizType;
    private String productId;
    private String channel;
    private String distributeStatus;
    private String channelItemId;
    private Date syncTime;
    private List<ChannelProductSkuVO> channelSkuList;
}
