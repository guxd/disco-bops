package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/4.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PriceVO implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;

    /**
     * 最小价格
     */
    private String priceMin;

    /**
     * 最大价格
     */
    private String priceMax;

    /**
     * 商品数量
     */
    private String productNum;

    /**
     * 销售数量
     */
    private String productSales;

    /**
     *
     */
    private String rank;
}
