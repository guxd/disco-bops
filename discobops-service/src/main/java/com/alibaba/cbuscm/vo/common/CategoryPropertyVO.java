package com.alibaba.cbuscm.vo.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryPropertyVO
{

    private long fid;
    private Long parentId;
    private String unit;
    private String name;
    private String fieldType;
    private Integer order;
    private String inputType;
    private String precomment;

    private List<Long> childrenFids = new ArrayList<Long>();

    private List<CategoryPropertyValueVO> featureIdValues = new ArrayList<CategoryPropertyValueVO>();


    private Map<String,List<CategoryPropertyVO>> childrenFieldMap = new HashMap<String,List<CategoryPropertyVO>>();

    public long getFid() {
        return fid;
    }

    public void setFid(long fid) {
        this.fid = fid;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getPrecomment() {
        return precomment;
    }

    public void setPrecomment(String precomment) {
        this.precomment = precomment;
    }

    public List<Long> getChildrenFids() {
        return childrenFids;
    }

    public void setChildrenFids(List<Long> childrenFids) {
        this.childrenFids = childrenFids;
    }

    public List<CategoryPropertyValueVO> getFeatureIdValues() {
        return featureIdValues;
    }

    public void setFeatureIdValues(List<CategoryPropertyValueVO> featureIdValues) {
        this.featureIdValues = featureIdValues;
    }

    public Map<String, List<CategoryPropertyVO>> getChildrenFieldMap() {
        return childrenFieldMap;
    }

    public void setChildrenFieldMap(Map<String, List<CategoryPropertyVO>> childrenFieldMap) {
        this.childrenFieldMap = childrenFieldMap;
    }
}
