package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/3.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ItemVO implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;

    /**
     * 商品ID
     */
    private String itemId;

    /**
     * 商品名称
     */
    private String itemName;

    /**
     * 商品价格
     */
    private Double price;

    /**
     * 商品销量
     */
    private String sales30d;

    /**
     * 图片地址
     */
    private String picUrl;

    /**
     * 详情地址
     */
    private String detailUrl;

    /**
     * 站点
     */
    private String  siteCode;


}
