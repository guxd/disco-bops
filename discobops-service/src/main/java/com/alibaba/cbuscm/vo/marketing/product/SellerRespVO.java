package com.alibaba.cbuscm.vo.marketing.product;

import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 13:45
 * @Description:活动店铺
 */
@Data
public class SellerRespVO {
    private Long sellerId;
    /**
     * 活动店铺名称
     */
    private String sellerName;
}
