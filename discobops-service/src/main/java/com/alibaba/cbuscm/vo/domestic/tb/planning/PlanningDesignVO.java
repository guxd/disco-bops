package com.alibaba.cbuscm.vo.domestic.tb.planning;

import com.alibaba.china.shared.discosupplier.model.common.UserModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningAttr;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningCompetitor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@ToString
public class PlanningDesignVO {

    /**
     * 企划id
     */
    private Long id;
    /**
     * Database Column Remarks:
     * 标题
     *
     * @mbg.generated
     */
    private String title;

    /**
     * Database Column Remarks:
     * 企划类型
     *
     * @mbg.generated
     */
    private String type;

    /**
     * Database Column Remarks:
     * 管理员
     *
     * @mbg.generated
     */
    private Set<UserModel> admins;

    /**
     * Database Column Remarks:
     * 淘系类目id(类目1id,类目2id,类目3id)
     *
     * @mbg.generated
     */
    private String categoryId;

    /**
     * Database Column Remarks:
     * 淘系类目名称(类目1名称/类目2名称/类目3名称)
     *
     * @mbg.generated
     */
    private String categoryValue;

    /**
     * Database Column Remarks:
     * 企划竞品信息
     *
     * @mbg.generated
     */
    private List<PlanningCompetitor> competitors;

    /**
     * Database Column Remarks:
     * 企划关键key value
     *
     * @mbg.generated
     */
    private List<PlanningAttr> keyAttrs;

    /**
     * Database Column Remarks:
     * 状态
     *
     * @mbg.generated
     */
    private String status;

    /**
     * Database Column Remarks:
     * 渠道
     *
     * @mbg.generated
     */
    private String scene;

    /**
     * Database Column Remarks:
     * 波段
     *
     * @mbg.generated
     */
    private String waveBrand;

    /**
     * Database Column Remarks:
     * 核心卖点
     *
     * @mbg.generated
     */
    private String sellPoint;

    /**
     * Database Column Remarks:
     * 上线时间
     *
     * @mbg.generated
     */
    private Date upTime;

    /**
     * Database Column Remarks:
     * 提报下限
     *
     * @mbg.generated
     */
    private Integer importLower;

    /**
     * Database Column Remarks:
     * 提报上限
     *
     * @mbg.generated
     */
    private Integer importUp;

    /**
     * Database Column Remarks:
     * 提报最大数量
     *
     * @mbg.generated
     */
    private Integer importMax;

    /**
     * Database Column Remarks:
     * 预期转换率
     *
     * @mbg.generated
     */
    private String expConvertRate;

    /**
     * Database Column Remarks:
     * 日销售预期
     *
     * @mbg.generated
     */
    private String dailySales;

    /**
     * Database Column Remarks:
     * 配给流量
     *
     * @mbg.generated
     */
    private String distriFlow;

    /**
     * Database Column Remarks:
     * 规格范围
     *
     * @mbg.generated
     */
    private String specScope;

    /**
     * Database Column Remarks:
     * 最小规格
     *
     * @mbg.generatede
     */
    private String specMin;

    /**
     * Database Column Remarks:
     * 最小规格指导价
     *
     * @mbg.generated
     */
    private String specMinPrice;

    /**
     * Database Column Remarks:
     * 描述
     *
     * @mbg.generated
     */
    private String desc;

    /**
     * Database Column Remarks:
     * 备注
     *
     * @mbg.generated
     */
    private String remark;

    /**
     * 上架商品数
     */
    private Integer offerCount;

    /**
     * 处理人
     */
    private Set<UserModel> handlers;

    /**
     * 时间
     */
    private Date gmtCreate;

    /**
     * 寻源id
     */
    private Long sourceId;

    /**
     * 是否可编辑
     */
    private Boolean editable;

    /**
     * 是否可删除
     */
    private Boolean deletable;

    /**
     * 规格信息
     */
    private List<PlanningAttr> specs;

    /**
     * 引流转化预测
     */
    private String drainagePred;

    /**
     * 企划定位
     */
    private String orientation;
}
