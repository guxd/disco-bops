package com.alibaba.cbuscm.vo.marketing.param;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/10/8
 */
@Data
public class PaymentConfigReqVO extends BaseVO {

    /**
     * 活动商品id
     */
    @NotNull(message = "marketingProductId not null")
    Long marketingProductId;

    /**
     * 收费
     */
    @NotBlank(message = "chargeFee not null")
    String chargeFee;
}
