package com.alibaba.cbuscm.vo.international;

import java.util.List;

import com.alibaba.cbu.disco.shared.core.order.model.data.FsChannelOrderExtra;

/**
 * @author xuhb
 * @title: SaleOrderDetailVo
 * @projectName discobops
 * @description: 销售订单详情
 * @date 2019-05-1413:51
 */
public class SaleOrderDetailVO {

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 采购单id
     */
    private String purchaseOrderId;

    /**
     * 订单主ID
     * 外部如GSP/淘宝的实际订单ID
     * 必填
     */
    private String orderId;

    /**
     * 下单时间
     */
    private Long createTime;

    /**
     * 状态名称
     */
    private String statusName;
    /**
     * 状态代号
     */
    private String status;

    /**
     * 采购方信息
     */
    private BuyerVO buyer;

    /**
     * 供应商信息
     */
    private SellerVO seller;

    /**
     * 境内收获地址
     */
    private ReceivingAddressModel domesticAddress;

    /**
     * 境外收获地址
     */
    private ReceivingAddressModel overseasAddress;

    /**
     * 子销售单情况
     */
    private List<SaleSubOrderLogisticsVO> logisticsList;

    /**
     * errorCode
     */
    private String errorCode;

    /**
     * errorMsg
     */
    private String errorMsg;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public BuyerVO getBuyer() {
        return buyer;
    }

    public void setBuyer(BuyerVO buyer) {
        this.buyer = buyer;
    }

    public ReceivingAddressModel getDomesticAddress() {
        return domesticAddress;
    }

    public void setDomesticAddress(ReceivingAddressModel domesticAddress) {
        this.domesticAddress = domesticAddress;
    }

    public ReceivingAddressModel getOverseasAddress() {
        return overseasAddress;
    }

    public void setOverseasAddress(ReceivingAddressModel overseasAddress) {
        this.overseasAddress = overseasAddress;
    }

    public void setLogisticsList(List<SaleSubOrderLogisticsVO> logisticsList) {
        this.logisticsList = logisticsList;
    }

    public List<SaleSubOrderLogisticsVO> getLogisticsList() {
        return logisticsList;
    }

    public SellerVO getSeller() {
        return seller;
    }

    public void setSeller(SellerVO seller) {
        this.seller = seller;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
