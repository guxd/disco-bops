package com.alibaba.cbuscm.vo.marketing.plan;

import com.alibaba.cbuscm.vo.marketing.param.MaterialVO;
import com.alibaba.cbuscm.vo.marketing.param.QualificationVO;
import com.alibaba.cbuscm.vo.marketing.product.MarketingProductWhiteVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 15:11
 * @Description:活动详情
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarketingPlanDetailVO implements Serializable {
    private static final long serialVersionUID = 7391807920206043989L;



    /**
     * 白名单商品
     */
    private List<MarketingProductWhiteVO> whiteProductList;

    /**
     *活动简介
     */
    private MarketingPlanVO marketingPlanVO;

    /**
     * 资质要求
     */
    private List<QualificationVO> qcTmeplateList;

    /**
     * 素材
     */
    private List<MaterialVO> materialVOList;

}
