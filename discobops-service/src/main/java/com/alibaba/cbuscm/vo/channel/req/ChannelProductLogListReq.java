package com.alibaba.cbuscm.vo.channel.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: gxd
 * @Date: 2019/10/16 10:23
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelProductLogListReq {


    private Long productId;
    private Long skuId;
    /**
     * skucn中台id
     */
    private Long skucnId;
    private String pageCode;
    private Integer pageNo;
    private Integer pageSize;

}
