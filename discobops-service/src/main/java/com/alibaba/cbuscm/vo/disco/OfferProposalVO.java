package com.alibaba.cbuscm.vo.disco;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
public class OfferProposalVO implements Serializable {

    private Long id;
    private Date gmtCreate;
    private Date gmtModified;
    private String status;
    private String statusDesc;
    private Long offerId;
    private String offerStatus;
    private String offerStatusDesc;
    private String offerImage;
    private String offerTitle;
    private String offerPrice;
    private String memberId;
    private String loginId;
    private String company;
    private String productId;
    private List<SkuInfoVO> skuInfo;
    private String offerSource;
}
