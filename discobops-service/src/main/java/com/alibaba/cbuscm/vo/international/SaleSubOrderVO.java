package com.alibaba.cbuscm.vo.international;

import com.alibaba.cbu.disco.shared.core.order.model.data.FsChannelSubExtra;

/**
 * @author xuhb
 * @title: SaleSubOrderVO
 * @projectName discobops
 * @description: 销售子订单信息
 * @date 2019-05-1414:16
 */
public class SaleSubOrderVO {

    /**
     * 商品id
     */
    private String productId;

    /**
     * 渠道物品id
     */
    private String channelGoodId;
    /**
     * offerId
     */
//    private String offerId;

    /**
     * 采购单offerId
     */
    private String purchaseId;

    /**
     * 订单子ID
     * 外部如GSP/淘宝的实际订单子ID
     * 必填
     */
    private String orderSubId;

    /**
     * offer title
     */
    private String offerTitle;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 收获数量
     */
    private int receiveAmount;

    /**
     * 采购价
     */
    private Double total;


    /**
     * 单价
     */
    private Double price;

    /**
     * sku规格，格式如
     */
    private String spec;

    /**
     * errorCode
     */
    private String errorCode;

    /**
     * errorMsg
     */
    private String errorMsg;

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getReceiveAmount() {
        return receiveAmount;
    }

    public void setReceiveAmount(int receiveAmount) {
        this.receiveAmount = receiveAmount;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getChannelGoodId() {
        return channelGoodId;
    }

    public void setChannelGoodId(String channelGoodId) {
        this.channelGoodId = channelGoodId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getOrderSubId() {
        return orderSubId;
    }

    public void setOrderSubId(String orderSubId) {
        this.orderSubId = orderSubId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
