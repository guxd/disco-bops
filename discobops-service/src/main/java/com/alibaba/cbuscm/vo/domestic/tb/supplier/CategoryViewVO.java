package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class CategoryViewVO {

    private List<String> categoryValues;

}
