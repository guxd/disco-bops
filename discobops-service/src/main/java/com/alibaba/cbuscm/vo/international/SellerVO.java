package com.alibaba.cbuscm.vo.international;

/**
 * @author xuhb
 * @title: SalerVO
 * @projectName discobops
 * @description: TODO
 * @date 2019-05-1414:00
 */
public class SellerVO {
    /**
     * 采购公司名称
     */
    private String companyName;

    /**
     * 采购公司编号
     */
    private String companyCode;

    /**
     * 渠道名称
     */
    private String channelName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
