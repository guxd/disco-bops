package com.alibaba.cbuscm.vo.channel.resp;

import com.alibaba.cbu.panama.dc.client.model.channelprice.WarehouseSubCostDTO;
import com.alibaba.cbuscm.vo.channel.WarehouseRespVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/10/15 20:54
 * @Description: 渠道商品价格列表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelPriceListRespVO {

    /**
     * 渠道商品价格id
     */
    private Long channelProductPriceId;

    /**
     * 渠道商品
     */
    private ChannelProductRespVO product;

    /**
     *渠道商品SKU
     */
    private ChannelProductSKUResqVO productSku;

    /**
     * 商品成本
     */
    private Double productCost;

    /**
     * 仓库成本 == 仓储费 + 打包费
     */
    private List<WarehouseRespVO> warehouseCost;

    /**
     * 仓租费
     */
    private Double storeFee;

    /**
     * 打包费
     */
    private Double packingFee;

    /**
     * 仓内操作费
     */
    private Double operateFee;

    /**
     * 汇率
     */
    private Double exchangeRate;

    /**
     * 毛利率
     */
    private Double grossProfitRate;

    /**
     * 供货价
     */
    private Double supplyPrice;

    /**
     * 在售平台
     */
    private String market;

    /**
     * 商品关系id
     */
    private Long relId;

    private Long offerId;

}
