package com.alibaba.cbuscm.vo.marketing.plan;


import com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum;
import com.alibaba.cbu.panama.dc.client.constants.marketingplan.ChargeTypeEnum;
import com.alibaba.cbu.panama.dc.client.constants.marketingplan.EnrollTypeEnum;
import com.alibaba.cbuscm.vo.marketing.product.OperateActionRespVO;
import com.alibaba.cbuscm.vo.marketing.product.OwnerRespVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 12:29
 * @Description:活动
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarketingPlanVO implements Serializable {

    private static final long serialVersionUID = 4472737384940186458L;

    /**
     * 活动状态
     * 未发布：UNPUBLISHED
     * 活动可报名：ENROLL_PROGRESS
     * 活动已报名待开始：PUBLISHED_ENROLL_NOT_START
     * 报名结束活动未开始：ENROLL_END_MARKETING_NOT_START
     * 活动进行中：MARKETING_PROGRESS
     * 活动已结束：MARKETING_END
     */
    private String status;



    /**
     * Database Column Remarks:
     *   自增主键
     *
     */
    private Long id;

    /**
     * Database Column Remarks:
     *   活动名称
     *
     */
    private String marketingPlanName;

    /**
     * Database Column Remarks:
     *   所属业务市场id
     *
     * @see ChannelMarketEnum
     */
    private String market;

    /**
     * Database Column Remarks:
     *   活动类型,参考枚举
     * @see EnrollTypeEnum
     *
     */
    private String enrollType;

    /**
     * Database Column Remarks:
     *   报名起始终时间
     *
     */
    private Long enrollStartTime;

    /**
     * Database Column Remarks:
     *   报名截止时间
     *
     */
    private Long enrollEndTime;

    /**
     * Database Column Remarks:
     *   活动生效时间
     *
     */
    private Long effectiveStartTime;

    /**
     * Database Column Remarks:
     *   活动截止时间
     *
     */
    private Long effectiveEndTime;

    /**
     * Database Column Remarks:
     *   收费类型(查询的条件，这里冗余字段处理)
     * @see ChargeTypeEnum
     *
     */
    private String chargeType;

    /**
     * Database Column Remarks:
     *   收费参数
     *
     */
    private Integer chargeParam;


    /**
     * 状态文案
     */
    private String statusMessage;

    /**
     * 操作项目
     */
    private List<OperateActionRespVO> actionList;

    /**
     * Database Column Remarks:
     *   参与活动的门店
     *
     */
    private List<String> sellerList;

    /**
     * 协议名称
     *
     */
    private String protocolName;

    /**
     * 报名协议链接
     *
     */
    private String protocolUrl;

    /**
     * Database Column Remarks:
     *   活动描述
     *
     */
    private String marketingPlanDesc;


    /**
     * 管理员名字
     */
    private List<OwnerRespVO> ownerList;


    /**
     *审核方式
     */
    private String auditType;

}
