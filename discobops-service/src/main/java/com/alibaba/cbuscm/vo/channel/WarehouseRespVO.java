package com.alibaba.cbuscm.vo.channel;

import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/10/16 23:03
 * @Description:
 */
@Data
public class WarehouseRespVO {

    /**
     * see枚举 FeeTypeEnum中的name值
     */
        private String code;

    /**
     * see枚举 FeeTypeEnum中的desc值
     */
    private String desc;

    /**
     * 具体费用
     */
    private Double value;



}
