package com.alibaba.cbuscm.vo.onepiece.opportunity;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by duanyang.zdy on 2019/9/5.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketTaskVO implements Serializable {

    private static final long serialVersionUID = -230326779722452820L;

    /**
     * 成功任务数量
     */
    private Integer succTaskCount;

    /**
     * 失败任务数量
     */
    private Integer failedTaskCount;

    /**
     * 任务列表地址
     */
    private String myTaskUrl;

    /**
     * 任务状态
     */
    private String status;

    /**
     * 失败任务原因
     */
    private List<MarketFailedTaskVO> data;

}
