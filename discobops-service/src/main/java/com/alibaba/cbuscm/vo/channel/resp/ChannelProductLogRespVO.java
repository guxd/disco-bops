package com.alibaba.cbuscm.vo.channel.resp;

import com.alibaba.cbuscm.vo.marketing.product.OperateLogRespVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/10/15 21:26
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelProductLogRespVO {

    /**
     * 渠道商品
     */
    private ChannelProductRespVO product;

    /**
     *渠道商品SKU
     */
    private ChannelProductSKUResqVO productSku;

    /**
     * 操作日志
     */
    private List<OperateLogRespVO> operateLog;

    /**
     * 在售平台
     */
    private String market;

}
