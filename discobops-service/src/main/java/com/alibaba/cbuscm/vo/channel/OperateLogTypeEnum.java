package com.alibaba.cbuscm.vo.channel;

public enum OperateLogTypeEnum {
    GROSS_RATE("毛利率"),
    SUPPLY_PRICE("供货价"),
    WAREHOUSE_PRICE("仓库成本");

    private String type;



    OperateLogTypeEnum(String type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
