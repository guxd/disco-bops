package com.alibaba.cbuscm.vo.common;

/**
 * 渠道的视图
 */
public class ChannelVO {

    /**
     * 渠道显示名称
     */
    private String name;

    /**
     *
     */
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
