package com.alibaba.cbuscm.vo.marketing.product;

import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * 活动提报商品列表vo
 * @author luxuetao
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class MarketingProductListRespVO {

    private Long marketingPlanId;

    private String marketingPlanName;

    private String loginId;

    private Long itemId;

    private String itemTitle;

    private String itemImg;

    private Long offerId;

    private String offerTitle;

    private String offerImg;

    private String status;

    private String statusMessage;

    /**
     * Database Column Remarks:
     *   报名起始终时间
     *
     */
    private Date enrollStartTime;

    /**
     * Database Column Remarks:
     *   报名截止时间
     *
     */
    private Date enrollEndTime;

    /**
     * Database Column Remarks:
     *   活动生效时间
     *
     */
    private Date effectiveStartTime;

    /**
     * Database Column Remarks:
     *   活动截止时间
     *
     */
    private Date effectiveEndTime;

    private List<OperateActionRespVO> actionList;

    private Long marketingProductId;

    /**
     *发布时间
     */
    private Date pubTime;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 花名
     */
    private String nickName;

    /**
     * 计划审核人
     */
    private String planReviewer;
}
