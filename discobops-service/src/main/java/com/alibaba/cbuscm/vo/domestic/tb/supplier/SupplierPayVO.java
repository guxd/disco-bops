/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb.supplier;

import java.util.Date;

/**
 * 类SupplierPayVO.java的实现描述：TODO 类实现描述 
 * @author jizhi.qy 2019年5月26日 下午3:02:31
 */
public class SupplierPayVO {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 供应商id
     */
    private String supplierMemberId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 收款的userId
     */
    private Long receiveUserId;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 付款时间
     */
    private Date gmtChargeBack;

    /**
     * 付款金额
     */
    private Long chargeBackMoney;

    /**
     * 原因
     */
    private String reason;

    /**
     * 状态
     */
    private String status;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 文件列表（jsonArray）
     */
    private String fileLink;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * loginId
     */
    private String loginId;

    /**
     * 钉钉信息
     */
    private String dingdingId;

    /**
     * 钉钉名称
     */
    private String dingdingName;

    /**
     * 旺铺url
     */
    private String shopUrl;

    public String getSupplierMemberId() {
        return supplierMemberId;
    }

    public void setSupplierMemberId(String supplierMemberId) {
        this.supplierMemberId = supplierMemberId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(Long receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getGmtChargeBack() {
        return gmtChargeBack;
    }

    public void setGmtChargeBack(Date gmtChargeBack) {
        this.gmtChargeBack = gmtChargeBack;
    }

    public Long getChargeBackMoney() {
        return chargeBackMoney;
    }

    public void setChargeBackMoney(Long chargeBackMoney) {
        this.chargeBackMoney = chargeBackMoney;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getDingdingId() {
        return dingdingId;
    }

    public void setDingdingId(String dingdingId) {
        this.dingdingId = dingdingId;
    }

    public String getDingdingName() {
        return dingdingName;
    }

    public void setDingdingName(String dingdingName) {
        this.dingdingName = dingdingName;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }

}
