package com.alibaba.cbuscm.vo.disco;

import lombok.Getter;
import lombok.Setter;

/**
 * 审核不通过原因类型
 *
 * @author aniu.nxh
 * @date 2019/8/24 下午16:26
 */
@Setter
@Getter
public class DcAuditTypeVO {

    /**
     * 状态显示名称
     */
    private String name;

    /**
     * 状态id
     */
    private String value;

}
