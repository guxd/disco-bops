package com.alibaba.cbuscm.vo.marketing.product;

import com.alibaba.cbuscm.vo.marketing.param.BaseVO;
import lombok.Data;

import java.util.List;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/10/9
 */
@Data
public class StatisticsStatusRespVO extends BaseVO {

    /**
     * 不同状态商品的数量
     */
    private List<ProductStatusNumRespVO> productStatusNum;

    /**
     * 商品总数量
     */
    private Integer totalNum;
}
