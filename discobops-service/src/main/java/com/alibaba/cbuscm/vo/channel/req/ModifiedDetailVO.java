package com.alibaba.cbuscm.vo.channel.req;


import lombok.Data;

/**
 * @Auther: gxd
 * @Date: 2019/10/21 17:38
 * @Description:
 */
@Data
public class ModifiedDetailVO {

//    private OperateLogTypeEnum type;
    private String type;

    private String from;

    private String to;


}
