/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb;

import com.alibaba.cbuscm.vo.UserVO;
import lombok.Data;

import java.util.List;

/**
 * 选品任务创建VO
 * 
 * @author luxuetao 2019年4月18日 下午3:09:27
 */
@Data
public class SelectionTaskCreateVO {

    private Long taskId;
    //是否为“综合活动”，不是则选取类目
    private Boolean hasComposite;
    //叶子节点id
    private Long categoryId;

    /**
     * 四级类目id
     */
    private Long virtualCategoryId;
    /**
     * 四级类目名称
     */
    private String virtualCategoryName;


    //一级业务，目前只有“天天特卖”，everydaySale
    private String primaryBiz;
    //二级业务(取淘宝1级类目)
    private String secondaryBiz;
    //名称
    private String name;
    //简介
    private String description;
    //管理员
    private List<UserVO> admins;

}
