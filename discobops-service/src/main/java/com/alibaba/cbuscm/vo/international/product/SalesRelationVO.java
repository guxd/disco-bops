package com.alibaba.cbuscm.vo.international.product;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 销售关系视图模型
 * @author zhuoxian
 */
@Data
@ToString
public class SalesRelationVO implements Serializable {

    /**
     * 关系ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 渠道业务
     */
    private String bizType;

    /**
     * 渠道店铺
     */
    private String channel;

    /**
     * 业务模式
     */
    private String marketingMode;

    /**
     * 价格比例
     */
    private Double premiumRate;

    /**
     * 供货价
     */
    private Long price;

    /**
     * 供货价币种
     */
    private String priceCurrency;
}
