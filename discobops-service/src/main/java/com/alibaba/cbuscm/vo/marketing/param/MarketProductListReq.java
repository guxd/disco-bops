package com.alibaba.cbuscm.vo.marketing.param;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @Auther: gxd
 * @Date: 2019/10/8 10:30
 * @Description: 活动商品列表的查询参数
 */
@Data
public class MarketProductListReq extends BasePageVO{

    private Long marketingProductId;

    /**
     * 状态
     */
    private String status;


    @NotBlank
    private String pageCode;

    /**
     * 活动id
     */
    private Long marketingPlanId;

    /**
     * 活动名称
     */
    private String marketingPlanName;

    /**
     * 淘系商品id
     */
    private Long itemId;

    /**
     * 淘系商品名称
     */
    private String itemName;

    /**
     * 1688商品id
     */
    private Long offerId;

    /**
     * 商家id
     */
    private String loginId;

}
