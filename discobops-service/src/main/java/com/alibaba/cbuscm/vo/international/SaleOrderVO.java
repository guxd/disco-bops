package com.alibaba.cbuscm.vo.international;

/**
 * @author xuhb
 * @title: SaleOrderVO
 * @projectName discobops
 * @description: 销售单
 * @date 2019-05-0919:33
 */
public class SaleOrderVO {
    /**
     * 渠道名称
     */
    private String channelName;


    /**
     * 渠道ID
     */
    private String channelId;

    /**
     * 销售订单编号
     */
    private String saleOrderId;

    /**
     * 状态名称
     */
    private String statusName;

    /**
     * 状态代号
     */
    private String status;


    /**
     * 记录生成时间
     */
    private long createTime;


    /**
     * 总体采购价
     */
    private String totalPay = "";


    /**
     * 预计数量
     */
    private int quantity = 0 ;


    /**
     * 收货数量
     */
    private int receiveQuantity = 0 ;

    /**
     * 子渠道名称/区域
     */
    private String channelSubName;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 采购单号
     */
    private String id;

    /**
     * 销售单卖家id
     */
    private String channelSellerId;

    public String getChannelSellerId() {
        return channelSellerId;
    }

    public void setChannelSellerId(String channelSellerId) {
        this.channelSellerId = channelSellerId;
    }

    public String getChannelSubName() {
        return channelSubName;
    }

    public void setChannelSubName(String channelSubName) {
        this.channelSubName = channelSubName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(String saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(String totalPay) {
        this.totalPay = totalPay;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getReceiveQuantity() {
        return receiveQuantity;
    }

    public void setReceiveQuantity(int receiveQuantity) {
        this.receiveQuantity = receiveQuantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
