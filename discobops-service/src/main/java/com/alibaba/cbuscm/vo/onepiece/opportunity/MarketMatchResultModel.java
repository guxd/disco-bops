package com.alibaba.cbuscm.vo.onepiece.opportunity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MarketMatchResultModel {
    private MarketMatchIndexModel data;
    private String stat_date;
    private Boolean success;

    @Data
    @ToString
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MarketMatchIndexModel {
        private String marketId;
        private String marketName;
        private String siteName;
        private String cateId;
        private String cateName;
        private Long cbuOfferNumForSamePic;
        private Long cbuOfferNumForSameStyle;
        private Long cbuOfferNumForSimilarStyle;
        private Long downItemNumForSamePic;
        private Long downItemNumForSameStyle;
        private Long downItemNumForSimilarStyle;
        private Double rateForSamePic;
        private Double rateForSameStyle;
        private Double rateForSimilarStyle;
        //gmv sales
        private Integer indexType;
        private Double marketGmv30d;
        private Double marketGrowth;
        private Double marketSales30d;
        private Double marketShare;
        private String marketTags;
        private Double productNum;
        private String type;
        private Double x;
        private Double y;
//        private Double xMedian;
//        private Double yMedian;
        private String isGlobal;
    }


}
