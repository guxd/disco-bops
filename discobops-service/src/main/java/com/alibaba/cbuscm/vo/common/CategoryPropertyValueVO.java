package com.alibaba.cbuscm.vo.common;

public class CategoryPropertyValueVO implements Comparable<CategoryPropertyValueVO>{
    private Long   vid;
    private String value;
    private Integer sort;
    public Long getVid() {
        return vid;
    }

    public void setVid(Long vid) {
        this.vid = vid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public int compareTo(CategoryPropertyValueVO o) {
        return this.sort.compareTo(o.getSort());

    }
}
