/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb;

import com.alibaba.excel.annotation.ExcelProperty;
/**
 * 类IndicationExcelModel.java的实现描述：TODO 类实现描述 
 * @author jizhi.qy 2019年4月23日 上午11:07:48
 */
import com.alibaba.excel.metadata.BaseRowModel;

import lombok.Data;

@Data
public class IndicationExcelVO extends BaseRowModel {

    @ExcelProperty(value = "商品名称", index = 0)
    private String title;

    @ExcelProperty(value = "商品id", index = 0)
    private Long itemId;
    
    @ExcelProperty(value = "公司名称", index = 0)
    private String companyName;

    @ExcelProperty(value = "勋章等级", index = 0)
    private Long tradeLevel;

    @ExcelProperty(value = "公司loginId", index = 0)
    private String loginId;

    @ExcelProperty(value = "一级类目名称", index = 0)
    private String cateLv1Name;

    @ExcelProperty(value = "二级类目名称", index = 0)
    private String cateLv2Name;
    
    @ExcelProperty(value = "叶子类目名称", index = 0)
    private String cateName;
    
    @ExcelProperty(value = "价格", index = 0)
    private String reservePrice;
    
    @ExcelProperty(value = "产地", index = 0)
    private String beltPlace;
    
    @ExcelProperty(value = "一件代发", index = 0)
    private Boolean isYJDF;

    // @ExcelProperty(value = "淘宝同款数", index = 0)
    // private Integer sameItemCount;

    @ExcelProperty(value = "30天1688销量", index = 0)
    private Long payOrdAmt30d;
    
    @ExcelProperty(value = "30天1688买家数", index = 0)
    private Long payOrdByrCnt30d;

    @ExcelProperty(value = "能否开发票", index = 0)
    private Boolean isVatInvoice;

    @ExcelProperty(value = "店铺注册资金（万元）", index = 0)
    private Long registeredCapital;
}
