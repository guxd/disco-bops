package com.alibaba.cbuscm.vo.marketing.product;

import com.alibaba.cbuscm.vo.marketing.param.MarketingSkuVO;
import com.alibaba.cbuscm.vo.marketing.param.MaterialVO;
import lombok.*;

import java.util.List;

/**
 * 活动提报提交vo
 * @author luxuetao
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class MarketingProductEditVO {

    private Long marketingProductId;

    private List<MarketingSkuVO> marketingSkuList;

    private List<MaterialVO> materialList;
}
