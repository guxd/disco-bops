package com.alibaba.cbuscm.vo.international.ae;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Created by duanyang.zdy on 2019/4/17.
 */
@Data
@ToString
@Builder
public class DownItemVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pic;

    private String price;

    private String siteId;

    private String siteItemId;

    private String title;

    private Long  sameOfferCount;

    private String itemUrl;

    private Double saleCount30dAvg;

    private Long likeCount;

    private Long cmtCount;


}
