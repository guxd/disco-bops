package com.alibaba.cbuscm.vo.marketing.param;

import lombok.*;

import java.util.List;


/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class QualificationVO {
    /**
     * 类型
     */
    private String type;
    /**
     * 名称
     */
    private String name;
    /**
     * 介绍要求
     */
    private String desc;
    /**
     *
     */
    private String code;

    /**
     * 资质参数
     */
    private List<String> qcParam;
}
