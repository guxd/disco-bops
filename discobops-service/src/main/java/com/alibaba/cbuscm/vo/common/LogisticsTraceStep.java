package com.alibaba.cbuscm.vo.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xuhb
 * @title: LogisticsTraceStep
 * @projectName discobops
 * @description: TODO
 * @date 2019-05-1616:35
 */
public class LogisticsTraceStep {

    private static final long serialVersionUID = 936677946737442384L;
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * 跟踪时间
     */
    private Date actionTime;
    /**
     * 跟踪描述
     */
    private String remark;
    /**
     * 状态
     */
    private String status;
    /**
     * 简化状态；可空
     */
    private String simplifiedStatus;
    /**
     * 简化状态描述；可空
     */
    private String simplifiedStatusDesc;
    /**
     * 地区编码
     */
    private String areaCode;
    /**
     * 地区名
     */
    private String areaName;
    /**
     * 动作类型
     */
    private String action;
    /**
     * 网点\中转站\仓库；可空
     */
    private String facility;
    /**
     * 当前位置
     */
    private String acceptAddress;

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getAcceptAddress() {
        return acceptAddress;
    }

    public void setAcceptAddress(String acceptAddress) {
        this.acceptAddress = acceptAddress;
    }

    public String getAcceptTimeString() {
        if (actionTime == null) {
            return "";
        }
        return new SimpleDateFormat(DATE_FORMAT).format(actionTime);
    }

    public String getSimplifiedStatus() {
        return simplifiedStatus;
    }

    public void setSimplifiedStatus(String simplifiedStatus) {
        this.simplifiedStatus = simplifiedStatus;
    }

    public String getSimplifiedStatusDesc() {
        return simplifiedStatusDesc;
    }

    public void setSimplifiedStatusDesc(String simplifiedStatusDesc) {
        this.simplifiedStatusDesc = simplifiedStatusDesc;
    }
}
