package com.alibaba.cbuscm.vo.common;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class CategoryVO {
    private int categoryId;

    private int value;

    private String label;

    private int parentId;

    private String channelName;

    private boolean leaf;

    private List<CategoryVO> children;

    private boolean hasVirtual = false;

    /**
     * 路径
     */
    private String path;

}
