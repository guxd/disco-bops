package com.alibaba.cbuscm.vo.marketing.product;

import lombok.Data;

/**
 * @Auther: wb-gxd621234
 * @Date: 2019/9/30 13:14
 * @Description:
 */

@Data
public class OwnerRespVO {

    /**
     * 管理员id
     */
    private Long ownerId;

    /**
     * 管理员名称
     */
    private String ownerName;
}
