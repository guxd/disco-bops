/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.vo.domestic.tb;

import java.util.List;

/**
 * 类IndicationOfferVO.java的实现描述：
 * @author jizhi.qy 2019年4月22日 下午11:09:06
 */
public class IndicationOfferVO {

    /**
     * offerId
     */
    private Long offerId;
    /**
     * offer图片地址
     */
    private String imgUrl220;

    /**
     * 标题
     */
    private String title;

    /**
     * 价格
     */
    private String price;

    /**
     * 公司名称
     */
    private String busName;

    /**
     * 产地
     */
    private String place;

    /**
     * 是否深度验厂
     */
    private Boolean hasDepthInspection;

    /**
     * 诚信通
     */
    private Boolean hasTP;

    /**
     * 在网时间
     */
    private String onlineYear;

    /**
     * 是否一件代发
     */
    private Boolean hasOnePush;

    /**
     * 店铺链接
     */
    private String winportDomain;

    /**
     * 多渠道offer以及销售信息
     */
    private List<Channel> channel;

    /**
     * 诚信通年份
     */
    private String tpYear;

    public String getWinportDomain() {
        return winportDomain;
    }

    public void setWinportDomain(String winportDomain) {
        this.winportDomain = winportDomain;
    }

    public String getImgUrl220() {
        return imgUrl220;
    }

    public void setImgUrl220(String imgUrl220) {
        this.imgUrl220 = imgUrl220;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBusName() {
        return busName;
    }

    public void setBusName(String busName) {
        this.busName = busName;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Boolean getHasDepthInspection() {
        return hasDepthInspection;
    }

    public void setHasDepthInspection(Boolean hasDepthInspection) {
        this.hasDepthInspection = hasDepthInspection;
    }

    public Boolean getHasTP() {
        return hasTP;
    }

    public void setHasTP(Boolean hasTP) {
        this.hasTP = hasTP;
    }

    public String getOnlineYear() {
        return onlineYear;
    }

    public void setOnlineYear(String onlineYear) {
        this.onlineYear = onlineYear;
    }

    public Boolean getHasOnePush() {
        return hasOnePush;
    }

    public void setHasOnePush(Boolean hasOnePush) {
        this.hasOnePush = hasOnePush;
    }

    public List<Channel> getChannel() {
        return channel;
    }

    public void setChannel(List<Channel> channel) {
        this.channel = channel;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getTpYear() {
        return tpYear;
    }

    public void setTpYear(String tpYear) {
        this.tpYear = tpYear;
    }

    public static class Channel {
        /**
         * logo的图片地址
         */
        private String logoUrl;

        /**
         * 渠道名称
         */
        private String name;

        /**
         * 7天增速
         */
        private String speed7;

        /**
         * 7天销量
         */
        private String sales7;

        /**
         * 7天价格
         */
        private String price7;

        /**
         * 7天付款人数
         */
        private String payment7;

        /**
         * 渠道offer列表
         */
        private List<ChannelOffer> channelOfferList;

        public String getLogoUrl() {
            return logoUrl;
        }

        public void setLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSpeed7() {
            return speed7;
        }

        public void setSpeed7(String speed7) {
            this.speed7 = speed7;
        }

        public String getSales7() {
            return sales7;
        }

        public void setSales7(String sales7) {
            this.sales7 = sales7;
        }

        public String getPrice7() {
            return price7;
        }

        public void setPrice7(String price7) {
            this.price7 = price7;
        }

        public String getPayment7() {
            return payment7;
        }

        public void setPayment7(String payment7) {
            this.payment7 = payment7;
        }

        public List<ChannelOffer> getChannelOfferList() {
            return channelOfferList;
        }

        public void setChannelOfferList(List<ChannelOffer> channelOfferList) {
            this.channelOfferList = channelOfferList;
        }

    }

    public static class ChannelOffer {
        /**
         * offer图片地址
         */
        private String imgUrl;

        /**
         * 价格
         */
        private String price;

        /**
         * 销量
         */
        private String sales;

        /**
         * 链接
         */
        private String link;

        /**
         * 返回64x64的图片
         */
        private String imgUrl64x64;

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getSales() {
            return sales;
        }

        public void setSales(String sales) {
            this.sales = sales;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getImgUrl64x64() {
            return imgUrl64x64;
        }

        public void setImgUrl64x64(String imgUrl64x64) {
            this.imgUrl64x64 = imgUrl64x64;
        }

    }
}
