package com.alibaba.cbuscm.vo.channel.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: gxd
 * @Date: 2019/10/16 10:23
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelPriceListReq {

    private Long skuId;
    private String productCode;
    private String skuCode;
    private Integer pageNo;
    private Integer pageSize;

}
