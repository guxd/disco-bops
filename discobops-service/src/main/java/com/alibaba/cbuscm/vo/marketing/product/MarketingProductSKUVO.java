package com.alibaba.cbuscm.vo.marketing.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Auther: gxd
 * @Date: 2019/9/30 16:59
 * @Description:商品sku信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarketingProductSKUVO implements Serializable {
    private static final long serialVersionUID = -483177076294314239L;

    /**
     * 是否是主推的sku 0-否 1-是
     */
    private Integer mainSku;

    /**
     * 淘系商品skuid
     */
    private Long itemSkuId;

    /**
     * 1688商品skuid
     */
    private Long offerSkuId;

    /**
     * 1688商品sku名称
     */
    private String offerSkuName;

    /**
     * 淘系商品sku名称
     */
    private String itemSkuName;

    /**
     * 淘系零售价，单位元
     */
    private Double retailPrice;

    /**
     * 佣金比例
     */
    private Double commissionRate;

    /**
     * 供货价，单位元
     */
    private Double supplyPrice;

    /**
     * 划线价，单位元
     */
    private Double lineationPrice;

    /**
     * 渠道库存
     */
    private Integer inventory;

    /**
     * 活动库存
     */
    private Integer marketingStock;
}
