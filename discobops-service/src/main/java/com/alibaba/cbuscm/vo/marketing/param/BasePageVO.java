package com.alibaba.cbuscm.vo.marketing.param;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@Data
public class BasePageVO extends BaseVO {
    /**
     * 页码
     */
    @NotNull
    private Integer pageNo;

    /**
     * 页面大小
     */
    @NotNull
    private Integer pageSize;


}
