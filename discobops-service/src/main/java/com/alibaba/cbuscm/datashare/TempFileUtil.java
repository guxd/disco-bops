package com.alibaba.cbuscm.datashare;

import java.io.File;
import java.io.IOException;

public class TempFileUtil {

    private static final String JAVA_IO_TMPDIR = "/home/admin/output/";

    private static final String POIFILES = "tempfiles";

    public static String createTempXlsxFile(String name) throws IOException {
        createPOIFilesDirectory();
        String filePath = JAVA_IO_TMPDIR + POIFILES + File.separator + name + System.currentTimeMillis() + ".xlsx";
        File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        return filePath;
    }

    /**
     *
     */
    private static void createPOIFilesDirectory() {

//        String tmpDir = System.getProperty(JAVA_IO_TMPDIR);
//        if (tmpDir == null) {
//            throw new RuntimeException(
//                    "Systems temporary directory not defined - set the -D" + JAVA_IO_TMPDIR + " jvm property!");
//        }
        File directory = new File(JAVA_IO_TMPDIR, POIFILES);
        if (!directory.exists()) {
            syncCreatePOIFilesDirectory(directory);
        }

    }

    /**
     * @param directory
     */
    private static synchronized void syncCreatePOIFilesDirectory(File directory) {
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }
}
