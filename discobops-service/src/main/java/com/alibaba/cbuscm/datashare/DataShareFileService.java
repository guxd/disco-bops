package com.alibaba.cbuscm.datashare;

import com.alibaba.china.global.business.library.common.ResultOf;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * 文件上传玄机藏接口
 */
@Slf4j
@Component
public class DataShareFileService {
    /**
     * 上传地址
     */
    @Value("${data.shared.upload.url}")
    private String uploadUrl;

    @Value("${data.shared.auth.secret}")
    private String secret;

    @Value("${data.shared.auth.user}")
    private String user;

    /**
     * 将文件上传到玄机藏
     *
     * @param fileComments 文件描述
     * @param filePath     文件地址
     * @param members      哪些员工可以看到
     * @return
     */
    public ResultOf<Boolean> uploadFile(String fileComments, String filePath, List<String> members) {
        PostMethod filePost = null;
        try {
            /**
             * 使用CustomFilePart解决文件名中文乱码的问题
             */
            filePost = new PostMethod(uploadUrl) {
                @Override
                public String getRequestCharSet() {
                    return "UTF-8";
                }
            };

            Part[] parts = {buildData(fileComments, filePath, members), new CustomFilePart("file", new File(filePath))};
            /**
             * 对于MIME类型的请求，httpclient建议全用MulitPartRequestEntity进行包装
             */
            MultipartRequestEntity mre = new MultipartRequestEntity(parts, filePost.getParams());
            filePost.setRequestEntity(mre);
            HttpClient client = new HttpClient();
            client.getHttpConnectionManager().getParams().setConnectionTimeout(500000);
            int status = client.executeMethod(filePost);

            if (status != HttpStatus.SC_OK) {
                return ResultOf.error("SYS ERROR", "上传文件失败，上传时HTTP返回HttpStatus " + status);
            } else if (status == HttpStatus.SC_OK) {
                return ResultOf.general(Boolean.TRUE);
            }
        } catch (Exception e) {
            log.error("", e);
            return ResultOf.error("SYS ERROR", "上传文件失败，详细信息: " + e.getMessage());
        } finally {
            if (filePost != null) {
                filePost.releaseConnection();
            }
        }
        return ResultOf.general(Boolean.TRUE);
    }

    /**
     * 构建参数
     *
     * @param fileComments
     * @param filePath
     * @param members
     * @return
     */
    private StringPart buildData(String fileComments, String filePath, List<String> members) {
        JsonArray toMembers = new JsonArray();
        for (String member : members) {
            toMembers.add(new JsonPrimitive(member));
        }
        String charSet = "utf8";
        /**
         *  必须对文件的说明信息进行utf8格式的URL编码
         */
        try {
            fileComments = URLEncoder.encode(fileComments, charSet);
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
        }
        /**
         * 如果上传的文件属于一类模板,则需要添加模板的ID,相同的模板ID,申请了模板下载后,相同模板ID的文件不需要再次申请就已经有了下载的权限,
         *保证自己的工程中唯一且长度不大于64个字符即可
         */
        JsonObject dataObj = new JsonObject();
        /**
         * 接口调用统一为APP方式
         */
        dataObj.addProperty("AuthType", "APP");
        /**
         * 此处为自己的appName
         */
        dataObj.addProperty("User", user);
        /**
         * 此处为和appName对应的Secret
         */
        dataObj.addProperty("Secret", secret);
        dataObj.addProperty("FileComments", fileComments);
        dataObj.add("ToMembers", toMembers);
        dataObj.addProperty("TemplateId", "");
        String isTextFile = "0";
        /**
         * 是纯文本文件,0为非纯文本文件
         */
        dataObj.addProperty("IsTextFile", isTextFile);
        String fileCharset = "UTF-8";
        dataObj.addProperty("Charset", fileCharset);
        String csvSeparator = "";
        /**
         * 目前仅支持tab作为参数值，若是使用逗号分隔，则无需添加此参数
         */
        dataObj.addProperty("CsvSeparator", csvSeparator);
        return new StringPart("data", dataObj.toString());
    }
}
