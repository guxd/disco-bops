package com.alibaba.cbuscm.config;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.boot.diamond.annotation.DiamondListener;
import com.alibaba.boot.diamond.listener.DiamondDataCallback;
import com.alibaba.china.global.business.library.enums.CurrencySymbolEnum;
import com.alibaba.china.global.business.library.models.rate.CurrencyRateModel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created by duanyang.zdy on 2019/4/24.
 */
@Slf4j
@DiamondListener(dataId = "global.business.supply.currency.rate", groupId = "global-business-center", executeAfterInit=true)
@Component
@Getter
@Service
public class RateDiamondConfiguration implements DiamondDataCallback {

    /**
     * 货币对美元汇率集合
     */
    private Map<CurrencySymbolEnum, CurrencyRateModel> currencySymbolRateMap;

    @Override
    public void received(String s) {
        if(StringUtils.isEmpty(s)) {
            throw new RuntimeException("global.business.supply.currency.rate is empty, please check diamond");
        }
        List<JSONObject> jsonCurrencyRates = JSON.parseObject(s, List.class);
        this.currencySymbolRateMap = jsonCurrencyRates.stream()
            .map(jsonObject -> JSON.parseObject(jsonObject.toJSONString(), CurrencyRateModel.class))
            .collect(Collectors.toMap(CurrencyRateModel::getToSymbol, currencyRateModel -> currencyRateModel));
    }

}
