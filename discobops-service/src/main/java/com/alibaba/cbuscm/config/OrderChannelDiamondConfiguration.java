package com.alibaba.cbuscm.config;

import com.alibaba.boot.diamond.annotation.DiamondListener;
import com.alibaba.boot.diamond.listener.DiamondDataCallback;
import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;


@Slf4j
@DiamondListener(dataId = "order.channel.list", groupId = "disco-bops", executeAfterInit=true)
@Component
@Getter
@Service
public class OrderChannelDiamondConfiguration implements DiamondDataCallback {

    /**
     * 货币对美元汇率集合
     */
    private List<String> oldMarketCodes;

    @Override
    public void received(String s) {
        if(StringUtils.isEmpty(s)) {
            throw new RuntimeException("order.channel.list is empty, please check diamond");
        }
        String[] channelIs = s.split(",");
        oldMarketCodes = Arrays.asList(channelIs);
        log.info("INIT@OrderChannelDiamondConfiguration,oldMarketCodes="+JSON.toJSONString(oldMarketCodes));
    }
}
