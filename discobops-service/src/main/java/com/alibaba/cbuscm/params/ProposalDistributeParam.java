package com.alibaba.cbuscm.params;

import java.util.List;

import lombok.Data;
import lombok.ToString;

/**
 * 类ProposalDistributeParam
 *
 * @author ruikun.xrk
 */
@Data
@ToString
public class ProposalDistributeParam {

    private Long proposalId;
    private List<String> channelIds;
}
