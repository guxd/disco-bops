package com.alibaba.cbuscm.params;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Created by duanyang.zdy on 2019/4/25.
 */
@Data
@ToString
@Builder
public class RequestDelParam {

    private String siteId;

    private Long taskId;

    private String downItemId;

    private Long searchTagId;

    private Long offerId;
}
