package com.alibaba.cbuscm.params;

import com.alibaba.fastvalidator.constraints.ValidateBean;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@ValidateBean
public class ProductSkuModifyParam implements Serializable {
    private String productSkuId;
    private String barCode;
    private String image;
    private Integer weight;
    private Integer length;
    private Integer Width;
    private Integer Height;
}
