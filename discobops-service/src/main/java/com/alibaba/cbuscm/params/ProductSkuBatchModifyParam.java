package com.alibaba.cbuscm.params;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class ProductSkuBatchModifyParam {
    public List<ProductSkuModifyParam> params;
}
