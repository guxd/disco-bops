package com.alibaba.cbuscm.params;

import lombok.Data;
import lombok.ToString;

/**
 * @author langjing
 * @date 2019/9/24 4:15 下午
 */
@Data
@ToString
public class ExtInfoParam {

    private String planningId;

    public ExtInfoParam() {
    }

    public ExtInfoParam(String planningId) {
        this.planningId = planningId;
    }
}
