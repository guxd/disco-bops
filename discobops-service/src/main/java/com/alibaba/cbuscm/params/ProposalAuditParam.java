package com.alibaba.cbuscm.params;

import java.util.List;

import lombok.Data;
import lombok.ToString;

/**
 * 产品提报审核参数
 * @author zhuoxian
 */
@Data
@ToString
public class ProposalAuditParam {

    private Long proposalId;
    private String msg;
}
