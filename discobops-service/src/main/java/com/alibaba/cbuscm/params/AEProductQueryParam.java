package com.alibaba.cbuscm.params;

import com.alibaba.cbuscm.common.PageQueryParam;

import com.taobao.tair.shade.com.esotericsoftware.kryo.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Created by duanyang.zdy on 2019/4/18.
 */
@Data
@Builder
@ToString
public class AEProductQueryParam extends PageQueryParam{

    @NotNull
    private Long taskId;

    private String orderFiled;
}
