package com.alibaba.cbuscm.params;

import com.taobao.tair.shade.com.esotericsoftware.kryo.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Created by duanyang.zdy on 2019/4/23.
 */
@Data
@Builder
@ToString
public class AEProductDelParam {

    @NotNull
    private Long taskId;

    @NotNull
    private String downItemId;

    @NotNull
    private String siteId;

    @NotNull
    private Long searchTagId;

    @NotNull
    private Long offerId;

}
