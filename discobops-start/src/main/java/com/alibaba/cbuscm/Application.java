package com.alibaba.cbuscm;

import com.taobao.pandora.boot.PandoraBootstrap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

/**
 * Pandora Boot应用的入口类
 *
 * @author jinxing.yjx
 */

@ImportResource({"classpath:biz-service.xml", "classpath:biz-hsf-reference.xml"})
@ComponentScan(basePackages = {"com.alibaba.cbuscm"})
@PropertySource("classpath:application.properties")
@SpringBootApplication(scanBasePackages = {"com.alibaba.cbuscm"})
public class Application {

    public static void main(String[] args) {
        PandoraBootstrap.run(args);
        SpringApplication.run(Application.class, args);
        PandoraBootstrap.markStartupAndWait();
    }
}
