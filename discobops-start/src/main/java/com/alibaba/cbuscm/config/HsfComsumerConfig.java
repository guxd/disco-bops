package com.alibaba.cbuscm.config;

import com.alibaba.boot.hsf.annotation.HSFConsumer;
import com.alibaba.buc.api.unit.EnhancedUserQueryReadService;
import com.alibaba.cbu.disco.channel.api.service.DiscoChannelPriceService;
import com.alibaba.cbu.disco.channel.api.service.DiscoChannelShopService;
import com.alibaba.cbu.disco.shared.business.ae.api.AeChannelDataService;
import com.alibaba.cbu.disco.shared.business.tao.api.TaoOrderWriteService;
import com.alibaba.cbu.disco.shared.core.config.api.DcContextService;
import com.alibaba.cbu.disco.shared.core.config.api.FsShopConfigService;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcAuditService;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcChannelInfoService;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcProductEnrollService;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcSupplierEnrollService;
import com.alibaba.cbu.disco.shared.core.order.api.FsChannelOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsOrderInfoReadService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPerformOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPurchaseOperateService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPurchaseOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsTradeInfoService;
import com.alibaba.cbu.disco.shared.core.product.api.DcChannelProductReadService;
import com.alibaba.cbu.disco.shared.core.product.api.DcDistributeManageService;
import com.alibaba.cbu.disco.shared.core.product.api.DcOfferEnrollService;
import com.alibaba.cbu.disco.shared.core.product.api.DcOfferImportService;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductManageService;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductOpLogQueryService;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductReadService;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalReadService;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalWriteService;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementAdminReadService;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementAdminWriteService;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementScheduleService;
import com.alibaba.cbu.panama.dc.client.service.channelprice.ChannelPriceReadService;
import com.alibaba.cbu.panama.dc.client.service.marketingplan.MarketingPlanWriteService;
import com.alibaba.cbu.sellergrowth.supply.api.SupplyActionService;
import com.alibaba.china.dw.dataopen.api.LastDateAPI;
import com.alibaba.china.global.business.dispatch.interfaces.SiteManageService;
import com.alibaba.china.global.business.goods.clean.service.CategoryReadService;
import com.alibaba.china.global.business.library.interfaces.onepiece.authority.AuthorityService;
import com.alibaba.china.global.business.library.interfaces.onepiece.authority.decisiondomain.DecisionDomainService;
import com.alibaba.china.global.business.library.interfaces.onepiece.bussinessOppo.UserDataRightQueryService;
import com.alibaba.china.global.business.library.interfaces.onepiece.delivery.SelectionDeliveryService;
import com.alibaba.china.global.business.library.interfaces.onepiece.resource.ResourceService;
import com.alibaba.china.global.business.library.interfaces.onepiece.selectioncenter.CommonSelectionTaskService;
import com.alibaba.china.global.business.library.interfaces.onepiece.selectioncenter.SelectionUnitService;
import com.alibaba.china.global.business.library.interfaces.search.SearchDownstreamProductService;
import com.alibaba.china.global.business.library.interfaces.search.SearchSameTbOfferSellInfoService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionBlacklistReadService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionBlacklistWriteService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionResultReadService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionResultWriteService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionTaskReadService;
import com.alibaba.china.global.business.library.interfaces.selection.aeshop.AeShopSelectionTaskReadService;
import com.alibaba.china.global.business.library.interfaces.selection.aeshop.AeShopSelectionTaskWriteService;
import com.alibaba.china.global.business.library.interfaces.supply.area.SupplyAreaService;
import com.alibaba.china.global.business.library.interfaces.supply.route.SupplyRouteService;
import com.alibaba.china.global.business.library.interfaces.supply.selection.AeShopSelectService;
import com.alibaba.china.global.business.library.interfaces.tag.ItemTagService;
import com.alibaba.china.global.business.match.interfaces.C2BDataPicSearchService;
import com.alibaba.china.idatacenter.service.IDataCenterQueryService;
import com.alibaba.china.offer.api.query.service.OfferQueryService;
import com.alibaba.china.shared.discosupplier.service.common.SupplierAblityService;
import com.alibaba.china.shared.discosupplier.service.common.SupplierOssService;
import com.alibaba.china.shared.discosupplier.service.planning.opp.PlanningTaskService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierPayService;
import com.alibaba.china.shared.manufacture.service.quality.QualityService;
import com.alibaba.china.shared.sm.api.itemquery.ItemQueryService;
import com.alibaba.dataworks.dataservice.service.HsfDataApiService;
import com.alibaba.shared.carriage.tracking.service.LogisticsTraceService;
import com.alibaba.shared.protect.api.service.PtsUserFundWriteService;

import org.springframework.context.annotation.Configuration;

@Configuration
public class HsfComsumerConfig {
    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    ItemTagService itemTagService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SelectionBlacklistWriteService selectionBlacklistWriteService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    AeShopSelectService aeShopSelectService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SelectionTaskReadService selectionTaskReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    AeShopSelectionTaskReadService aeShopSelectionTaskReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    AeShopSelectionTaskWriteService aeShopSelectionTaskWriteService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SiteManageService siteManageService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 50000)
    C2BDataPicSearchService c2BDataPicSearchService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SupplyAreaService supplyAreaService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SupplyRouteService supplyRouteService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SelectionBlacklistReadService selectionBlacklistReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SearchSameTbOfferSellInfoService searchSameTbOfferSellInfoService;

    @HSFConsumer(serviceGroup = "${buc.hsf.group}", serviceVersion = "${buc.hsf.version}", clientTimeout = 5000)
    EnhancedUserQueryReadService enhancedUserQueryService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    DcOfferImportService dcOfferImportService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    private SupplierOssService supplierOssService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    private SupplierPayService supplierPayService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    private PtsUserFundWriteService ptsUserFundWriteService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 30000)
    DcProductManageService dcProductManageService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    FsChannelOrderService channelOrderService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    FsPurchaseOrderService purchaseOrderService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    FsPerformOrderService performOrderService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    FsShopConfigService fsShopConfigService;

    @HSFConsumer(serviceGroup = "DUBBO", serviceVersion = "1.0.1", clientTimeout = 5000)
    OfferQueryService offerQueryService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    DcProductReadService dcProductReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    DcChannelProductReadService dcChannelProductReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    LogisticsTraceService logisticsTraceService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    AeChannelDataService aeChannelDataService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    CommonSelectionTaskService commonSelectionTaskService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    AuthorityService authorityService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    SelectionResultReadService selectionResultReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    private SelectionResultWriteService selectionResultWriteService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    private SelectionUnitService selectionUnitService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    private DecisionDomainService decisionDomainService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    private CategoryReadService downStreamCategoryReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 15000)
    private ResourceService resourceService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 10000)
    DcDistributeManageService dcDistributeManageService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 5000)
    DcContextService dcContextService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private DcAuditService dcAuditService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private DcProductEnrollService dcProductEnrollService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private DcChannelInfoService dcChannelInfoService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private DcSupplierEnrollService dcSupplierEnrollService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private FsOrderInfoReadService fsOrderInfoReadService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private TaoOrderWriteService taoOrderWriteService;

    @HSFConsumer(serviceVersion = "1.0.0")
    private com.alibaba.cbu.disco.shared.core.common.api.CategoryReadService discoCategoryReadService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private DcProposalReadService proposalReadService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private DcProposalWriteService proposalWriteService;

    @HSFConsumer(serviceVersion = "1.0.0", serviceGroup = "DUBBO", clientTimeout = 30000)
    private IDataCenterQueryService iDataCenterQueryService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private DiscoChannelShopService discoChannelShopService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private DiscoChannelPriceService discoChannelPriceService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private FsTradeInfoService fsTradeInfoService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private SupplementAdminReadService supplementAdminReadService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private SupplementAdminWriteService supplementAdminWriteService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private SupplementScheduleService supplementScheduleService;

    @HSFConsumer(serviceVersion = "1.0.0")
    private SelectionDeliveryService selectionDeliveryService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private SearchDownstreamProductService searchDownstreamProductService;

    @HSFConsumer(serviceVersion = "1.0.0", serviceGroup = "DUBBO", clientTimeout = 10000)
    private LastDateAPI lastDateAPI;

    @HSFConsumer(serviceVersion = "1.0.0", serviceGroup = "HSF", clientTimeout = 10000)
    private UserDataRightQueryService userDataRightQueryService;

    @HSFConsumer(serviceVersion = "1.0.0", serviceGroup = "HSF", clientTimeout = 10000)
    private HsfDataApiService   hsfDataApiService;


    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 3000)
    private FsPurchaseOperateService fsPurchaseOperateService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 5000)
    private QualityService qualityService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 3000)
    private SupplierAblityService supplierAblityService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000)
    private DcOfferEnrollService dcOfferEnrollService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 10000,serviceGroup = "DUBBO")
    private ItemQueryService itemQueryService;

    @HSFConsumer(serviceGroup = "HSF", serviceVersion = "1.0.0", clientTimeout = 10000)
    DcProductOpLogQueryService dcProductOpLogQueryService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 3000)
    private SupplyActionService supplyActionService;

    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 3000)
    private PlanningTaskService planningTaskService;

//    @HSFConsumer(serviceVersion = "1.0.0", clientTimeout = 3000)
//    private ChannelPriceReadService channelPriceReadService;

    //日常环境
//    @HSFConsumer(serviceVersion = "1.0.0 Group", clientTimeout = 3000)
//    private MarketingPlanWriteService marketingPlanWriteService;
}
