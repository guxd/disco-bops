package com.alibaba.cbuscm.config;

import com.alibaba.cbuscm.controller.onepiece.selection.AuthInterceptor;
import com.alibaba.china.global.business.library.interfaces.onepiece.authority.AuthorityService;
import com.alibaba.china.global.business.library.interfaces.onepiece.selectioncenter.SelectionUnitService;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.MultipartConfigElement;
import java.util.ArrayList;
import java.util.List;

/**
 * web相关配置
 *
 * @author lichao.wlc
 * @date 2019/04/24
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
    private static final String ORIGINS = "*";
    @Autowired
    private AuthorityService authorityService;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            .exposedHeaders(
                "Access-Control-Allow-Headers",
                "Access-Control-Allow-Methods",
                "Access-Control-Allow-Origin",
                "Access-Control-Max-Age"
            )
            .allowedHeaders(ORIGINS)
            .allowedMethods(ORIGINS)
            .allowedOrigins(ORIGINS);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        super.configureMessageConverters(converters);
        FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = fastJsonHttpMessageConverter.getFastJsonConfig();
        List<MediaType> fastMediaTypes = new ArrayList();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastJsonConfig.setSerializerFeatures(
            SerializerFeature.DisableCircularReferenceDetect,
            SerializerFeature.WriteMapNullValue
        );
        fastJsonHttpMessageConverter.setSupportedMediaTypes(fastMediaTypes);
        fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
        converters.add(fastJsonHttpMessageConverter);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor(authorityService))
            .addPathPatterns(
                "/one-piece/selection/task",
                "/one-piece/selection/resource/result/downstream",
                "/one-piece/selection/resource/result/upstream",
                "/one-piece/selection/resource/result/profit",
                "/one-piece/selection/item/query"
            ).excludePathPatterns();
    }

    /**
      * 配置上传文件大小的配置
      * @return
      */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //  单个数据大小
        factory.setMaxFileSize("102400KB");
        /// 总上传数据大小
        factory.setMaxRequestSize("102400KB");
        return factory.createMultipartConfig();
    }

}
