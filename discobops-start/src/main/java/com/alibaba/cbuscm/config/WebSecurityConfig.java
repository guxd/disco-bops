package com.alibaba.cbuscm.config;

import com.alibaba.spring.websecurity.DefaultWebSecurityConfigurer;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * Created by qinwei on 2019/9/23.
 *
 * @author qinwei
 * @date 2019/09/23
 */
@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends DefaultWebSecurityConfigurer {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);

        //disable 默认策略。 这一句不能省。
        http.headers().frameOptions().disable();
//        http.csrf().disable();
    }
}
