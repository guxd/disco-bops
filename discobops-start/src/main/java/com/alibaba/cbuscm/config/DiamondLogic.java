package com.alibaba.cbuscm.config;


import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcMultiUserModel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.taobao.diamond.client.Diamond;
import com.taobao.diamond.manager.ManagerListener;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/**
 * @Auther: gxd
 * @Date: 2019/11/13 14:09
 * @Description: 暂时没用
 */
@Service
public class DiamondLogic {
    private static final Logger unilog = LoggerFactory.getLogger(DiamondLogic.class);
    private final static String DIA_MULTI_USER_DATA = "disco.multi.user.data";
    private final static String DIA_GROUP_ID = "disco";
    private static Map<String, DcMultiUserModel> multiUserMap = new ConcurrentHashMap<String, DcMultiUserModel>();

    static {
        // 监听配置变更
        Diamond.addListener(DIA_MULTI_USER_DATA, DIA_GROUP_ID, new ManagerListener() {
            @Override
            public Executor getExecutor() {
                return null;
            }
            @Override
            public void receiveConfigInfo(String configInfo) {
                convertConfig4MultiUser(configInfo);
            }
        });

        try {
            // 首次获取配置
            String config4MultiUser = Diamond.getConfig(DIA_MULTI_USER_DATA, DIA_GROUP_ID, 5000);
            convertConfig4MultiUser(config4MultiUser);
        } catch (IOException e) {
            unilog.error("DcChannelDiamondLogic#static_init#IOException", e);
        } catch (Exception e) {
            unilog.error("DcChannelDiamondLogic#static_init#Exception", e);
        }

    }

    /**
     * 优化：如果失败，对原来的数据不清除，降低风险
     * 无论修改成功还是失败，都打印日志
     * @param configInfo
     */
    private static void convertConfig4MultiUser(String configInfo) {

        try{
            unilog.error("DiamondLogic#convertConfig4MultiUser#Change_Begin#info:" + configInfo);

            if(StringUtils.isBlank(configInfo)) {
                unilog.error("DiamondLogic#convertConfig4MultiUser#configInfo_null#info:" + configInfo);
                return;
            }

            Map<String, DcMultiUserModel> diamondMap = JSON.parseObject(configInfo, new TypeReference<Map<String, DcMultiUserModel>>(){});

            //验证，channelSellerId不允许重复，如果重复，修改失败
            if(diamondMap == null) {
                unilog.error("DiamondLogic#convertConfig4MultiUser#map_null#info:" + configInfo);
                return;
            }

            Set<String> channelSellerIdSet = new HashSet<>();
            for(DcMultiUserModel curModel : diamondMap.values()) {
                if(curModel == null) {
                    continue;
                }

                String curChannelSellerId = curModel.getChannelSellerId();

                if(channelSellerIdSet.contains(curChannelSellerId)) {
                    unilog.error("DiamondLogic#convertConfig4MultiUser#ChannelSellerId_Dup#info:" + configInfo);
                    return;
                }

                channelSellerIdSet.add(curChannelSellerId);
            }

            //校验通过
            multiUserMap.clear();
            multiUserMap.putAll(diamondMap);

            unilog.error("DiamondLogic#convertConfig4MultiUser#Success#info:" + configInfo);

        }catch (Exception e) {
            unilog.error("DiamondLogic#convertConfig4MultiUser#exception#", e);
        }

    }

//    public List<Map<String, String>> getSellerIdList(){
//        List<Map<String, String>> list = new ArrayList<>();
//        for (DcMultiUserModel model : multiUserMap.values()) {
//            Map<String, String> map = new HashMap<>();
//            if (Objects.nonNull(model)&&Objects.nonNull(model.getChannelSellerId())&&Objects.nonNull(model.getShopName())){
//                map.put(model.getChannelSellerId(),model.getShopName());
//            }
//            list.add(map);
//        }
//        return list;
//    }
//
//    public static void main(String[] args) {
//        DiamondLogic diamondLogic = new DiamondLogic();
//        List<Map<String, String>> sellerIdList = diamondLogic.getSellerIdList();
//    }

}
