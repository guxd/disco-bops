package com.alibaba.cbuscm.filter;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.log.constant.LogConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * 活动和活动商过滤器，方便日志服务切面获取args
 * 只限制于/marketingproduct/read/*,/marketingproduct/write/*,/marketingplan/read/*,/marketingplan/write/*
 * 目的是截取servlet获取empid,然后塞入线程
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/10/11
 */
@Configuration
@WebFilter(urlPatterns = "/*", filterName = "instFilter")
@Order(1)
public class InstFilter implements Filter {

    /**
     * 需要拦截的path
     */
    private final List<String> PATH_WITH_INTERCEPT = new ArrayList<>();


    @Override
    public void init(FilterConfig filterConfig) {
        //在这里添加需要拦截的path，注意都需要小写，前后都需要斜线
        PATH_WITH_INTERCEPT.add("/marketingproduct/read");
        PATH_WITH_INTERCEPT.add("/marketingproduct/write");
        PATH_WITH_INTERCEPT.add("/marketingplan/read");
        PATH_WITH_INTERCEPT.add("/marketingplan/write");
    }

    @Override
    public void doFilter(ServletRequest requestservletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest httpServletRequest = new RequestWrapper((HttpServletRequest) requestservletRequest);
        HttpServletRequest httpServletRequest = (HttpServletRequest)requestservletRequest;
        //当前请求的path
        String localPath = httpServletRequest.getServletPath().toLowerCase();
        //判断是否是例外的请求
        if (PATH_WITH_INTERCEPT.stream().noneMatch(localPath::startsWith)) {
            filterChain.doFilter(httpServletRequest, servletResponse);
            return;
        }
        processRestRequest(httpServletRequest, servletResponse, filterChain);
    }

    @Override
    public void destroy() {

    }



    /**
     * 处理rest请求，loginId放到小二实体中
     *
     * @param request
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     */
    private void processRestRequest(HttpServletRequest request, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);

            if (Objects.isNull(user)){
                BopsLogUtil.setEmpId("1234");
                BopsLogUtil.setNickNamecn("测试");
            }else {
                BopsLogUtil.setEmpId(user.getEmpId());
                String nickName = user.getNickNameCn();
                if(StringUtils.isBlank(nickName)){
                    nickName = user.getLoginName();
                }
                BopsLogUtil.setNickNamecn(nickName);
            }
            filterChain.doFilter(request, servletResponse);
        } catch (Exception e) {
            BopsLogUtil.logControllerFailed("InstFilter","processRestRequest","",0L, LogConstant.LogType.REST,e);
            return;
        }
    }

}