package com.alibaba.cbuscm.advice;

import com.alibaba.china.global.business.library.models.onepiece.authority.OnePieceAuthException;
import com.alibaba.up.common.mybatis.result.RPCResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yaogaolin 2019-05-14 21:24
 */
@ControllerAdvice
public class GlobalExceptionResolver {
    private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionResolver.class);

    /**
     * 处理所有不可知异常
     *
     * @param e 异常
     * @return json结果
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public RPCResult handleException(Exception e) {
        // 打印异常堆栈信息
        LOG.error(e.getMessage(), e);

        return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_SYSTEM);
    }

    /**
     * 处理所有业务异常
     *
     * @param e 业务异常
     * @return json结果
     */
    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public RPCResult handleOpdRuntimeException(Throwable e) {
        // 不打印异常堆栈信息
        LOG.error(e.getMessage(), e);

        return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, "unknown exception");
    }

    /**
     * 处理one piece权限相关异常
     *
     * @param e 业务异常
     * @return json结果
     */
    @ExceptionHandler(OnePieceAuthException.class)
    @ResponseBody
    public RPCResult handleOpdRuntimeException(OnePieceAuthException e) {
        return RPCResult.error(RPCResult.CODE_FORBIDDEN, e.getErrorMsg());
    }
}
