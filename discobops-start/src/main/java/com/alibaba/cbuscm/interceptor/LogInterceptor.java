package com.alibaba.cbuscm.interceptor;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.RPCResult;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * controller 全局日志拦截器
 *
 * @author langjing
 * @date 2019-08-30 1:13 下午
 */
@Aspect
@Component
public class LogInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogInterceptor.class);

    private static final ThreadLocal<String> ORIGIN_METHOD = new ThreadLocal<>();

    /**
     * 怕有其他影响暂时只覆盖企划
     */
    @Pointcut("execution(* com.alibaba.cbuscm.controller.domestic.tb.planning..*.*(..))")
    public void pointcut() {
    }

    @ResponseBody
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        // 全路径类名#方法名
        String methodName = pjp.getSignature().getDeclaringTypeName() + "#" + pjp.getSignature().getName();
        ORIGIN_METHOD.set(methodName);
        Object[] args = pjp.getArgs();
        try {
            Object[] arguments = new Object[args.length];
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof ServletRequest || args[i] instanceof ServletResponse
                    || args[i] instanceof MultipartFile
                    || args[i] instanceof byte[] || args[i] instanceof Byte[]) {
                    continue;
                }
                arguments[i] = args[i];
            }
            LOGGER.info("入参={}", JSON.toJSONString(arguments).replaceAll("\\\\", ""));
            long start = System.currentTimeMillis();
            Object result = pjp.proceed();
            LOGGER.info("耗时={}, 回参={}", (System.currentTimeMillis() - start), JSON.toJSONString(result));
            return result;
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("未知异常", e);
            return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, e.getMessage());
        } finally {
            ORIGIN_METHOD.set(null);
        }
    }

    /**
     * 返回被代理的方法名
     * 例如：com.alibaba.cbuscm.controller.domestic.tb.planning.PlanningSourceController#list
     *
     * @return
     */
    public static String getOriginMethod() {
        return ORIGIN_METHOD.get();
    }
}