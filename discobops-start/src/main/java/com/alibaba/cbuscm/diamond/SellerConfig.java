package com.alibaba.cbuscm.diamond;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Auther: 顾晓东
 * @Date: 2019/11/13 16:05
 * @Description:
 */
@Component
public class SellerConfig {
    public static Map<Long,String> map = Maps.newHashMap();
    static {
        map.put(3937219703L,"天天特卖工厂店");
        map.put(2202613935619L,"天天特卖美妆直营");
        map.put(263671308L,"淘系测试一店");
        map.put(263664947L,"淘系测试二店");
        map.put(263674311L,"淘系测试三店");
        map.put(9223370014983671308L,"压测淘系测试一店");
        map.put(9223370014983664947L,"压测淘系测试二店");
        map.put(9223370014983674311L,"压测淘系测试三店");
        map.put(9223370018657219703L,"天天特卖工厂店压测");
    }
}
