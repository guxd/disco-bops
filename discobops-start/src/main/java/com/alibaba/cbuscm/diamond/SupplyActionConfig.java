package com.alibaba.cbuscm.diamond;

import java.io.IOException;

import javax.annotation.PostConstruct;

import com.taobao.diamond.client.Diamond;
import com.taobao.diamond.manager.ManagerListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author langjing
 * @date 2019/9/24 4:55 下午
 */
@Component
public class SupplyActionConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(SupplyActionConfig.class);

    /**
     * Diamond groupId
     */
    private static final String APP_NAME = "disco-bops";

    /**
     * Diamond dataId：测试企划列表
     */
    private static final String TEST_MEMBER_ID = "com.alibaba.china.cbuscm.supplyActionAccessCode";

    private static String accessCode = "";

    @PostConstruct
    public void init() {

        try {
            accessCode = Diamond.getConfig(TEST_MEMBER_ID, APP_NAME, 1000);
        } catch (IOException e) {
            LOGGER.error("TestPlanningConfig#init error", e);
        }

        // 启动用，并且变化需要立即推送最新值
        Diamond.addListener(TEST_MEMBER_ID, APP_NAME,
            new ManagerListenerAdapter() {
                @Override
                public void receiveConfigInfo(String configInfo) {
                    try {
                        accessCode = configInfo;
                    } catch (Exception e) {
                        LOGGER.error("TestMemberConfig#receiveConfigInfo error", e);
                    }
                }
            });
    }

    public static String getAccessCode() {
        return accessCode;
    }
}
