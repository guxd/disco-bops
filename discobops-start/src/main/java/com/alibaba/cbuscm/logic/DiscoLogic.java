package com.alibaba.cbuscm.logic;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditEmpType;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbuscm.service.authority.AclControlPermissionLogic;
import com.alibaba.cbuscm.utils.LoginUtil;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@Service
public class DiscoLogic {

    @Autowired
    private AclControlPermissionLogic aclControlPermissionLogic;

    /**
     * 获取DcEmpModel
     *
     * @param request
     * @return
     */
    public ResultOf<DcEmpModel> fetchEmpModel(HttpServletRequest request) {
        ResultOf<DcEmpModel> empResult = new ResultOf<>();
        empResult.setSuccess(false);

        DcEmpModel empModel = null;
        try {
            // 工号
            String empId = LoginUtil.getEmpId(request);
            // userId
            Integer userId = LoginUtil.getUserId(request);
            // 校验是否登陆
            if (userId == null || userId <= 0) {
                empResult.setErrorCode("refreshPage");
                empResult.setErrorMessage("请刷新页面");
                return empResult;
            }
            // 名称
            String empName = LoginUtil.getLoginName(request);
            // 操作者
            DcAuditEmpType authType = aclControlPermissionLogic.hasTaoShopPermission(userId);
            // 校验操作者
            if (authType == null || StringUtils.isBlank(authType.getValue())) {
                empResult.setErrorCode("Emp_Illegal");
                empResult.setErrorMessage("无使用权限");
                return empResult;
            }
            empModel = new DcEmpModel();
            empModel.setEmpId(empId);
            empModel.setEmpType(authType.getValue());
            empModel.setEmpLogin(true);
            empModel.setEmpName(empName);

            empResult.setSuccess(true);
            empResult.setData(empModel);

        } catch (Exception e) {
            log.error("DiscoUtil.fetchEmpModel.Exception", e);

            empResult.setSuccess(false);
            empResult.setData(null);
            empResult.setErrorCode("Login_Exception");
            empResult.setErrorMessage("登陆异常");
        }


        return empResult;
    }

}
