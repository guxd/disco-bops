package com.alibaba.cbuscm.utils;

import java.math.BigInteger;
import java.math.RoundingMode;

import com.google.common.math.BigIntegerMath;

public class PrimeNumber {

    private static final BigInteger three = BigInteger.valueOf(3);
    private static final BigInteger one = BigInteger.valueOf(1);
    private static final BigInteger zero = BigInteger.valueOf(0);

    public static boolean isPrime(BigInteger n) {
        BigInteger root = BigIntegerMath.sqrt(n, RoundingMode.DOWN);
        for (BigInteger i = BigInteger.valueOf(2); i.compareTo(root) <= 0; i = i.add(one)) {
            if (n.mod(i).compareTo(zero) == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        long beginTime = System.currentTimeMillis();
        BigInteger count = BigInteger.valueOf(5);
        BigInteger targetCount = BigInteger.valueOf(10).pow(8);
        BigInteger n = BigInteger.valueOf(11);
        BigInteger[] deltas = new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(4), BigInteger.valueOf(2),
            BigInteger.valueOf(2) };
        int deltaIndex = 0;
        while (true) {
            if (isPrime(n)) {
                count = count.add(one);
                if (count.compareTo(targetCount) == 0) {
                    break;
                }
            }
            n = n.add(deltas[deltaIndex++]);
            if (deltaIndex == deltas.length) {
                deltaIndex = 0;
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println(n);
        System.out.println(endTime - beginTime);
    }
}
