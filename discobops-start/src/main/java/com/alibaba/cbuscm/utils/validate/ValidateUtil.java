package com.alibaba.cbuscm.utils.validate;

import com.alibaba.cbuscm.utils.SpringContextUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 * 业务逻辑模版
 * @author ${jiangbo.jjb}
 * @version $Id: BusinessServiceTemplate.java, v 0.1 2019年9月18日 下午12:30
 */
public class ValidateUtil {

    private static Logger log = LoggerFactory.getLogger("DEFAULT-APPENDER");

    /**
     * 根据javax.validation标准对对象内属性进行校验
     *
     * @param obj 校验的对象
     * @param <T> 泛型
     */
    public static <T> void validate(T obj){
        ApplicationContext applicationContext = SpringContextUtil.getApplicationContext();
        LocalValidatorFactoryBean validator = applicationContext.getBean(LocalValidatorFactoryBean.class);
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(obj);
        constraintViolations.forEach(error -> {
            log.error("参数校验异常，obj = {}", obj);
            final String buffer = "[" + error.getPropertyPath().toString() + "]" +
                    error.getMessage() +
                    ", value = " +
                    error.getInvalidValue();
            try {
                log.error("参数校验异常，buffer = {}", buffer);
                throw new Exception(buffer);
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    /**
     * 根据javax.validation标准对对象内属性进行校验
     *
     * @param collection 校验的集合
     * @param <T> 泛型
     */
    public static <T> void validate(Collection<T> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            return;
        }
        collection.forEach(ValidateUtil::validate);
    }

    /**
     * 校验参数是否为null(String会校验是否blank)，有null时返回true
     *
     * @param args 入参
     * @return 有null时返回true
     */
    public static boolean checkIsNull(Object... args) {
        return Arrays.stream(args).anyMatch(arg -> {
            if (arg instanceof String) {
                return StringUtils.isBlank((String) arg);
            } else {
                return arg == null;
            }
        });
    }


}
