package com.alibaba.cbuscm.utils;

import org.apache.commons.collections.bag.SynchronizedSortedBag;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xuhb
 * @title: TBUrlHelper
 * @projectName disco
 * @description: 淘宝URL解析
 * @date 2019-08-2311:35
 */
public class TBUrlHelper {
    /**
     * 根据宝贝URL截取宝贝ID
     * http://auction1.taobao.com/auction/item_detail-0db1-d57b90f4c406fe1ee1517884dafe338b.jhtml
     * 截取后为32位的字符串：d57b90f4c406fe1ee1517884dafe338b
     * http://item.taobao.com/auction/item_detail.htm?itemID=1b70b4c3fb32cf0e24af9a649ad5360d
     * 截取后为32位的字符串：1b70b4c3fb32cf0e24af9a649ad5360d
     * 如下几种宝贝URL均合法：
     * http://item.taobao.com/auction/item_detail-0db2-a159acabbeedeb61ea92231371adae67.jhtml
     * http://auction1.taobao.com/auction/item_detail-0db1-6ce724f828e554364f6bb8cd4fdf0249.jhtml
     * http://item.taobao.com/auction/item_detail.htm?itemID=1b70b4c3fb32cf0e24af9a649ad5360d&ali_refid=a3_419095_1006:380074963:6:%B7%DB:504f81b2bd89eb72144729a403c22c10
     * http://item.taobao.com/auction/item_detail.jhtml?item_id=f19580fd3d5ec2395ff6e7d4192b9230&x_id=0db1
     * http://item.taobao.com/auction/item_detail--2c8338b253d3beaa41afb51f610e2eb5.jhtml
     * @param auctionUrl
     * @return
     *
     */
    public static String parsePddUrl(String auctionUrl){
        String ret = "";
        try{
            String regex1 = "(http|https)://mobile.yangkeduo.com/.*(\\?|&)goods_id=([0-9]\\d*).*";
            Pattern pattern1 = Pattern.compile(regex1,Pattern.CASE_INSENSITIVE);
            Matcher matcher1 = pattern1.matcher(auctionUrl);

            if(matcher1.matches()){
                ret = matcher1.group(3);
                return ret;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * 根据宝贝URL截取宝贝ID
     * http://auction1.taobao.com/auction/item_detail-0db1-d57b90f4c406fe1ee1517884dafe338b.jhtml
     * 截取后为32位的字符串：d57b90f4c406fe1ee1517884dafe338b
     * http://item.taobao.com/auction/item_detail.htm?itemID=1b70b4c3fb32cf0e24af9a649ad5360d
     * 截取后为32位的字符串：1b70b4c3fb32cf0e24af9a649ad5360d
     * 如下几种宝贝URL均合法：
     * http://item.taobao.com/auction/item_detail-0db2-a159acabbeedeb61ea92231371adae67.jhtml
     * http://auction1.taobao.com/auction/item_detail-0db1-6ce724f828e554364f6bb8cd4fdf0249.jhtml
     * http://item.taobao.com/auction/item_detail.htm?itemID=1b70b4c3fb32cf0e24af9a649ad5360d&ali_refid=a3_419095_1006:380074963:6:%B7%DB:504f81b2bd89eb72144729a403c22c10
     * http://item.taobao.com/auction/item_detail.jhtml?item_id=f19580fd3d5ec2395ff6e7d4192b9230&x_id=0db1
     * http://item.taobao.com/auction/item_detail--2c8338b253d3beaa41afb51f610e2eb5.jhtml
     * @param auctionUrl
     * @return
     *
     */
    public static String parseTbUrl(String auctionUrl){
        String ret = "";
        try{
            String regex1 = "(http|https)://item.taobao.com/.*(\\?|&)id=([0-9]\\d*).*";
            Pattern pattern1 = Pattern.compile(regex1,Pattern.CASE_INSENSITIVE);
            Matcher matcher1 = pattern1.matcher(auctionUrl);

            if(matcher1.matches()){
                ret = matcher1.group(3);
                return ret;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ret;
    }



    public static String parseMallUrl(String auctionUrl){
        String ret = "";
        try{
            String regex1 = "(http|https)://detail.tmall.com/.*(\\?|&)id=([0-9]\\d*).*";
            Pattern pattern1 = Pattern.compile(regex1,Pattern.CASE_INSENSITIVE);
            Matcher matcher1 = pattern1.matcher(auctionUrl);

            if(matcher1.matches()){
                ret = matcher1.group(3);
                return ret;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ret;
    }

    public static void main(String[] args) {
        System.out.println(TBUrlHelper.parsePddUrl("http://mobile.yangkeduo.com/goods.html?goods_id=2243328295&refer_page_name=login&refer_page_id=10169_1566534690603_5f7fYrPN4q&refer_page_sn=10169"));
    }
}
