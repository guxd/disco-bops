package com.alibaba.cbuscm.utils.validate;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 * 业务逻辑模版
 * @author ${jiangbo.jjb}
 * @version $Id: BusinessServiceTemplate.java, v 0.1 2019年9月18日 下午12:30
 */
@Slf4j
@Component
public class PropertyValidateUtils {

    @Resource
    private LocalValidatorFactoryBean localValidatorFactoryBean;


    /**
     * 根据javax.validation标准对对象内属性进行校验
     *
     * @param obj 校验的对象
     * @param <T> 泛型
     */
    public <T> void validate(T obj) {
        Set<ConstraintViolation<T>> constraintViolations = localValidatorFactoryBean.validate(obj);
        constraintViolations.forEach(error -> {
            try {
                String buffer = "[" + error.getPropertyPath().toString() + "]" + error.getMessage() +
                        ", value = " + error.getInvalidValue();
                log.error(String.format("validate 失败 error:%s",buffer));
                throw new Exception( );
            } catch (Exception e) {
                log.error(String.format("validate 失败 error:%s",e.getMessage()));
                e.printStackTrace();
            }
        });
    }

    /**
     * 根据javax.validation标准对对象内属性进行校验
     *
     * @param collection 校验的集合
     * @param <T>        泛型
     */
    public <T> void validate(Collection<T> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            return;
        }
        collection.forEach(this::validate);
    }

    /**
     * 校验参数是否为null(String会校验是否blank)，有null时返回true
     *
     * @param args 动态入参
     * @return 返回是否有null值
     */
    public static boolean checkIsNull(Object... args) {
        return Arrays.stream(args).anyMatch(arg -> {
            if (arg instanceof String) {
                return StringUtils.isBlank((String) arg);
            } else {
                return arg == null;
            }
        });
    }

}