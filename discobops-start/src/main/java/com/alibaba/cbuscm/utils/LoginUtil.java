package com.alibaba.cbuscm.utils;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Login utils.
 */
@Slf4j
public class LoginUtil {

    public static String getLoginName(HttpServletRequest request) {
        String loginName = "未登录";
        try {
            BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
            if (user == null) {
                return loginName;
            }

            if (StringUtils.isNotBlank(user.getNickNameCn())) {
                return user.getNickNameCn();
            } else if (StringUtils.isNotBlank(user.getLastName())) {
                return user.getLastName();
            } else if (StringUtils.isNotBlank(user.getLoginName())) {
                return user.getLoginName();
            }

            return loginName;
        } catch (IOException e) {
            log.error("LoginUtil.getLoginName.IOException:", e);
        } catch (ServletException e) {
            log.error("LoginUtil.getLoginName.ServletException:", e);
        }
        return loginName;
    }

    public static Integer getUserId(HttpServletRequest request) {
        Integer userId = 0;
        try {
            BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
            if (user != null) {
                userId = user.getId();
            }
        } catch (IOException e) {
            log.error("LoginUtil.getLoginName.IOException:", e);
        } catch (ServletException e) {
            log.error("LoginUtil.getLoginName.ServletException:", e);
        }
        return userId;
    }

    public static String getEmpId(HttpServletRequest request) {
        String empId = "0";
        try {
            BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
            if (user != null) {
                empId = user.getEmpId();
            }
        } catch (IOException e) {
            log.error("LoginUtil.getLoginName.IOException:", e);
        } catch (ServletException e) {
            log.error("LoginUtil.getLoginName.ServletException:", e);
        }
        return empId;
    }
}
