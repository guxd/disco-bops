package com.alibaba.cbuscm.logback;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.taobao.eagleeye.EagleEye;

/**
 * @author langjing
 * @date 2019/8/31 1:38 下午
 */
public class TraceIdConverter extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {
        try {
            return EagleEye.getTraceId();
        } catch (Exception e) {
            return "n/a";
        }
    }
}
