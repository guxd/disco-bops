package com.alibaba.cbuscm.logback;

import com.alibaba.cbuscm.interceptor.LogInterceptor;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.CallerData;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * @author langjing
 * @date 2019/8/31 11:33 上午
 */
public class OriginMethodConverter extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {
        try {
            return LogInterceptor.getOriginMethod();
        } catch (Exception e) {
            return "n/a";
        }
    }
}
