package com.alibaba.cbuscm.logback;

import ch.qos.logback.classic.PatternLayout;

/**
 * 自定义PatternLayout
 *
 * @author langjing
 * @date 2019/8/31 11:31 上午
 */
public class CustomizePatternLayout extends PatternLayout {

    static {
        // 记录鹰眼traceId
        defaultConverterMap.put("T", TraceIdConverter.class.getName());
        defaultConverterMap.put("tid", TraceIdConverter.class.getName());

        // 记录日志拦截器中被代理的方法
        defaultConverterMap.put("OM", OriginMethodConverter.class.getName());
        defaultConverterMap.put("originMethod", OriginMethodConverter.class.getName());
    }
}
