package com.alibaba.cbuscm.controller.marketing.plan;

import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.RestResult;
import com.alibaba.cbuscm.diamond.SellerConfig;
import com.alibaba.cbuscm.service.marketing.plan.MarketingPlanReadAdapter;
import com.alibaba.cbuscm.utils.validate.ValidateUtil;
import com.alibaba.cbuscm.vo.international.SellerVO;
import com.alibaba.cbuscm.vo.marketing.param.MarketingPlanPageReqVO;
import com.alibaba.cbuscm.vo.marketing.param.MaterialVO;
import com.alibaba.cbuscm.vo.marketing.param.QualificationVO;
import com.alibaba.cbuscm.vo.marketing.plan.MarketingPlanDetailRespVO;
import com.alibaba.cbuscm.vo.marketing.plan.MarketingPlanRespVO;
import com.alibaba.cbuscm.vo.marketing.product.PageResultResp;
import com.alibaba.cbuscm.vo.marketing.product.SellerRespVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */



@RestController
@RequestMapping(path = "/marketingplan/read", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MarketingPlanReadController {


    @Autowired
    private MarketingPlanReadAdapter marketingPlanReadAdapter;

    /**
     * 获取供应商活动列表
     */
    @GetMapping(value = "/list")
    public RestResult<PageResultResp<MarketingPlanRespVO>> list(@RequestBody MarketingPlanPageReqVO reqVo ) {

        ValidateUtil.validate(reqVo);

        PageResultResp<MarketingPlanRespVO> page = null;
        return RestResult.success(page);
    }



    /**
     * 获取活动基本信息
     */
    @GetMapping(value = "/query/baseInfo")
    public RestResult<MarketingPlanRespVO> queryBaseInfo(@RequestParam Long marketingPlanId ) {

        if(Objects.isNull(marketingPlanId)){
            return RestResult.fail("001","marketingPlanId为空");
        }

        Long userId = null;
        String empId = BopsLogUtil.getEmpId();
        if (StringUtils.isNotBlank(empId)){
            userId = Long.valueOf(empId);
        }

        MarketingPlanRespVO vo = marketingPlanReadAdapter.queryMarketingPlanModelById(marketingPlanId,userId);
        return RestResult.success(vo);
    }

    private void setSellerNameList(MarketingPlanRespVO vo) {
//        Map<Long, String> map = SellerConfig.map;
//        List<String> list = new ArrayList<>();
//
//        if (Objects.nonNull(vo.getBaseInfo()) && CollectionUtils.isNotEmpty(vo.getBaseInfo().getSellerList())){
//            for (Long aLong : vo.getBaseInfo().getSellerList()) {
//                list.add(map.get(aLong));
//            }
//            vo.getBaseInfo().setSellerName(list);
//        }
    }

    /**
     * 获取活动店铺列表
     * @return
     */
    @GetMapping(value = "/query/sellerList")
    public RestResult<Map<Long, String>> querySellerList() {
        return RestResult.success(marketingPlanReadAdapter.querySellerList());
    }



    /**
     * 获取资质
     */
    @GetMapping(value = "/query/qc")
    public RestResult<List<QualificationVO>> quertyQcList(@RequestParam Long marketingPlanId ) {

        if(Objects.isNull(marketingPlanId)){
            return RestResult.fail("001","marketingPlanId为空");
        }

        List<QualificationVO> list = null;
        return RestResult.success(list);
    }

    /**
     * 获取素材
     */
    @GetMapping(value = "/query/material")
    public RestResult<List<MaterialVO>> queryMaterialList(@RequestParam Long marketingPlanId ) {

        if(Objects.isNull(marketingPlanId)){
            return RestResult.fail("001","marketingPlanId为空");
        }

        List<MaterialVO>  list =  null;

        return RestResult.success(list);
    }


    /**
     * 活动详情
     */
    @GetMapping(value = "/detail")
    public RestResult<MarketingPlanDetailRespVO> detail(@RequestParam Long marketingPlanId ) {


        if(Objects.nonNull(marketingPlanId)){
            return RestResult.fail("001","marketingPlanId为空");
        }

        MarketingPlanDetailRespVO vo = null;

        return RestResult.success(vo);

    }


    @GetMapping(value = "/query/ownerList")
    public RestResult<SellerRespVO> ownerList(@RequestParam Long marketingPlanId ) {

//        return RestResult.success("");
        return null;
    }



}
