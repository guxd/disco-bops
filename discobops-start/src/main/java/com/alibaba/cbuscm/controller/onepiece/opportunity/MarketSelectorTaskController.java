package com.alibaba.cbuscm.controller.onepiece.opportunity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.alimonitor.jmonitor.utils.StringUtils;
import com.alibaba.cbuscm.contants.MarketConstants;
import com.alibaba.cbuscm.enums.MarketTaskStatusEnum;
import com.alibaba.cbuscm.enums.SelectorTaskTypeEnum;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketFailedTaskVO;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketSelectorTaskVO;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketTaskVO;
import com.alibaba.cbuscm.vo.onepiece.opportunity.TaskVO;
import com.alibaba.china.global.business.library.common.NestedPageOf;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.interfaces.onepiece.selectioncenter.CommonSelectionTaskService;
import com.alibaba.china.global.business.library.models.onepiece.selectioncenter.AddToSelectionTaskResultModel;
import com.alibaba.china.global.business.library.models.onepiece.selectioncenter.CommonSelectionTaskModel;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.AddToSelectionTaskParam;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.CommonSelectionTaskQueryParam;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.SiteMarketIdVO;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.SiteOfferIdVO;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.ResultError;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商机中心打通选品中心
 * Created by duanyang.zdy on 2019/9/4.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class MarketSelectorTaskController {

    @Resource
    private CommonSelectionTaskService commonSelectionTaskService;



    @RequestMapping("/selection/queryTask")
    public Object queryTask(@RequestParam(name="pageIndex",defaultValue = "1") Integer pageIndex,
                            @RequestParam(name="pageNum",defaultValue = "50") Integer pageNum,
                            HttpServletRequest request) {

        try {

            CommonSelectionTaskQueryParam param = CommonSelectionTaskQueryParam.builder()
                .adminId(LoginUtil.getEmpId(request))
                .allTaskFlag(false)
                .pageNo(pageIndex)
                .pageSize(pageNum)
                .build();

            ResultOf<NestedPageOf<CommonSelectionTaskModel>> result =  commonSelectionTaskService.query(param);

            if(!result.isSuccess() || result.getData() == null || result.getData().getList().isEmpty()){

                return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, result.getErrorMessage());
            }

            List<TaskVO> taskVOs = new ArrayList<>();

            result.getData().getList().forEach(var->{

                TaskVO vo = TaskVO.builder()
                    .taskId(var.getTaskId()!=null?var.getTaskId().toString():null)
                    .taskName(var.getTaskName())
                    .taskType(SelectorTaskTypeEnum.getTypeEnumByValue(var.getSubTaskType()).getValue())
                    .build();

                taskVOs.add(vo);
            });

            MarketSelectorTaskVO marketSelectorTaskVO = MarketSelectorTaskVO.builder().data(taskVOs).build();

            marketSelectorTaskVO.setAdminId(LoginUtil.getEmpId(request));
            marketSelectorTaskVO.setLoginId(JSON.toJSONString(result.getData().getList().get(0).getAdmins()));
            marketSelectorTaskVO.setTotal(result.getData().getPageTotal());
            marketSelectorTaskVO.setPageSize(result.getData().getPageSize());
            marketSelectorTaskVO.setPageNo(result.getData().getPageNo());

            return ResultOf.general(marketSelectorTaskVO);

        }catch (Exception e){

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, e.getMessage());

        }

    }

    @RequestMapping("/selection/createTask")
    public Object createTask(@RequestParam(name="marketId") String marketId,
                             @RequestParam(name="taskType") String taskType,
                             @RequestParam(name="offerIds") String offerIds,
                             @RequestParam(name="taskIds")  String taskIds,
                             HttpServletRequest request){

        //参数校验
        if(StringUtils.isEmpty(taskType) || StringUtils.isEmpty(taskIds)){

            return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, "taskType or taskIds is null");
        }


        try {
            List<SiteOfferIdVO> siteOfferIdVOs = new ArrayList<>();

            SiteMarketIdVO siteMarketIdVO = null;

            if(MarketConstants.MARKET_TASK_TYPE.equals(taskType)){

                if(StringUtils.isEmpty(marketId)){

                    return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, "marketId is null");
                }
                //解析marketid例子:marketID='123123/ae,shopee,amazon'
                siteMarketIdVO = SiteMarketIdVO.builder()
                    .marketId(marketId.split(MarketConstants.MARKET_TASK_SEP)[0])
                    .site(marketId.split(MarketConstants.MARKET_TASK_SEP)[1])
                    .build();
            }

            if(MarketConstants.SITE_TASK_TYPE.equals(taskType)){
                if(StringUtils.isEmpty(offerIds)){

                    return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, "offerIds is null");
                }
                //解析offerid例子:offerIds='222/shopeeId,444/amazon'
                List<String> list = Arrays.asList(offerIds.split(","));

                Map<String,String> map = new HashMap<>();

                list.forEach(var->{

                    if(map.get(var.split(MarketConstants.MARKET_TASK_SEP)[1]) == null){

                        map.put(var.split(MarketConstants.MARKET_TASK_SEP)[1],var.split(MarketConstants.MARKET_TASK_SEP)[0]);
                    }else{

                        map.put(var.split(MarketConstants.MARKET_TASK_SEP)[1],map.get(var.split(MarketConstants.MARKET_TASK_SEP)[1])+","+var.split(MarketConstants.MARKET_TASK_SEP)[0]);
                    }

                });

                map.forEach((k,v)->{

                    SiteOfferIdVO vo = SiteOfferIdVO.builder()
                        .offerIds(Stream.of(v.split(",")).collect(Collectors.toList()))
                        .site(k)
                        .build();

                    siteOfferIdVOs.add(vo);

                });

            }

            List<String> taskId = Stream.of(taskIds.split(",")).collect(Collectors.toList());


            AddToSelectionTaskParam param = AddToSelectionTaskParam.builder()
                .siteMarketIdVO(siteMarketIdVO)
                .siteOfferIds(siteOfferIdVOs)
                .taskId(taskId.stream().map(var->Long.valueOf(var)).collect(Collectors.toList()))
                .build();

            ResultOf<List<AddToSelectionTaskResultModel>> result = commonSelectionTaskService.addToSelectionTask(param);

            if(result == null || !result.isSuccess()){

                return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, result.getErrorMessage());
            }

            //解析创建任务结果
            List<MarketFailedTaskVO> marketFailedTaskVOs = new ArrayList<>();
            result.getData().forEach(var->{

                MarketFailedTaskVO vo = MarketFailedTaskVO.builder()
                    .failedTaskId(var.getTaskId())
                    .errorMsg(generateErrorMsg(var,taskType))
                    .build();

                marketFailedTaskVOs.add(vo);
            });

            MarketTaskVO vo = MarketTaskVO.builder()
                .failedTaskCount(result.getData().size())
                .myTaskUrl(MarketConstants.MARKET_MY_TASK_URL)
                .succTaskCount(taskIds.split(",").length-result.getData().size())
                .data(marketFailedTaskVOs)
                .status(checkStatus(taskIds.split(",").length,result.getData().size()))
                .build();

            return ResultOf.general(vo);


        }catch (Exception e){

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, e.getMessage());
        }

    }

    /**
     * 生成错误信息
     * @param model
     * @param taskType
     * @return
     */
    private String generateErrorMsg(AddToSelectionTaskResultModel model,String taskType){

        if(MarketConstants.SITE_TASK_TYPE.equals(taskType) || model.getErrorSiteOfferIds() != null){

            return model.getErrorSiteOfferIds().stream().map(var->var.getSite()).collect(Collectors.toList()).toString();
        }

        if(MarketConstants.SITE_TASK_TYPE.equals(taskType) || model.getErrorSiteMarketId() != null){

            return model.getErrorSiteMarketId().getSite();
        }

        return null;
    }

    private String checkStatus(Integer totalCount,Integer failedCount){

        Integer succCount = totalCount-failedCount;

        if(failedCount == 0 && failedCount<totalCount){

            return MarketTaskStatusEnum.ALL_SUCC.getValue();
        }

        if(succCount == 0 && succCount<failedCount){

            return MarketTaskStatusEnum.ALL_FAILED.getValue();
        }

        if(failedCount >0 && failedCount<totalCount){

            return MarketTaskStatusEnum.PART_SUCC.getValue();
        }

        return MarketTaskStatusEnum.ALL_FAILED.getValue();

    }

}
