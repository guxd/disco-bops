package com.alibaba.cbuscm.controller.domestic.tb.planning.request;

/**
 * @author langjing
 * @date 2019/8/30 2:54 下午
 */
public class DeleteOppRequest {

    private Long sourceId;

    private String memberId;

    private Long itemId;

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @Override
    public String toString() {
        return "DeleteOppRequest{" +
            "sourceId=" + sourceId +
            ", memberId='" + memberId + '\'' +
            ", itemId=" + itemId +
            '}';
    }
}
