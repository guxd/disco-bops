package com.alibaba.cbuscm.controller.onepiece.selection;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbuscm.datashare.DataShareFileService;
import com.alibaba.cbuscm.datashare.TempFileUtil;
import com.alibaba.cbuscm.service.AEDistributeService;
import com.alibaba.cbuscm.service.onepiece.CommonDistributeService;
import com.alibaba.cbuscm.service.onepiece.model.PlatformShopModel;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.onepiece.selection.SelectionExportVO;
import com.alibaba.cbuscm.vo.onepiece.selection.SelectionItemExportVO;
import com.alibaba.cbuscm.vo.onepiece.selection.SelectionItemStatusVO;
import com.alibaba.cbuscm.vo.onepiece.selection.StatusListVO;
import com.alibaba.china.global.business.library.common.PageResult;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.enums.onepiece.SelectionChannelEnum;
import com.alibaba.china.global.business.library.enums.selection.SortFieldEnum;
import com.alibaba.china.global.business.library.interfaces.onepiece.selectioncenter.SelectionUnitService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionResultReadService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionResultWriteService;
import com.alibaba.china.global.business.library.models.onepiece.selectioncenter.SelectionUnitModel;
import com.alibaba.china.global.business.library.models.selection.DsSelectionResultModel;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.SelectionUnitQueryParam;
import com.alibaba.china.global.business.library.params.selection.DsSelectionQueryParam;
import com.alibaba.china.global.business.library.params.selection.DsSelectionUpdateParam;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.alibaba.security.spring.component.csrf.CsrfTokenModel;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/one-piece/selection/item")
@Slf4j
@CrossOrigin(origins = "*")
public class SelectionItemController {

    @Autowired
    private SelectionResultReadService selectionResultReadService;

    @Autowired
    private SelectionResultWriteService selectionResultWriteService;

    @Autowired
    private SelectionUnitService selectionUnitService;

    @Autowired
    private DataShareFileService dataShareFileService;

    @Autowired
    private AEDistributeService aeDistributeService;

    @Autowired
    private CommonDistributeService commonDistributeService;

    @RequestMapping("/query")
    public RPCResult query(@RequestParam(value = "sortField", required = false) String sortField,
                           @RequestParam(value = "status", defaultValue = "AWAIT_CONFIRM") String status,
                           @RequestParam(value = "taskId") Long taskId,
                           @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
                           @RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                           HttpServletRequest request) {
        try {
            String lang = request.getHeader("lang");
            Boolean only1688 = onlySelect1688(taskId);
            DsSelectionQueryParam dsSelectionQueryParam = DsSelectionQueryParam
                    .builder()
                    .status(status)
                    .taskId(taskId)
                    .language(lang)
                    .empId(LoginUtil.getEmpId(request))
                    .build();
            dsSelectionQueryParam.setPageNo(pageNo);
            dsSelectionQueryParam.setPageSize(pageSize);
            ResultOf<PageResult<DsSelectionResultModel>> result;
            if (!only1688) {
                dsSelectionQueryParam.setSortField(
                        StringUtils.isEmpty(sortField) ? SortFieldEnum.DS_SALE30_COUNT_H2D.name() : sortField);
                result = selectionResultReadService.query(dsSelectionQueryParam);
            } else {
                dsSelectionQueryParam.setSortField(
                        StringUtils.isEmpty(sortField) ? SortFieldEnum.CBU_SALE30_COUNT_H2D.name() : sortField);
                result = selectionResultReadService.query1688Item(dsSelectionQueryParam);
            }
            return RPCResult.ok(result.getData());
        } catch (Exception e) {
            log.error(" item query error ", e);
            return RPCResult.error("item query error ", e.getMessage());
        }
    }

    @RequestMapping("/export")
    public RPCResult export(@RequestBody SelectionExportVO exportVO,
                            HttpServletRequest request) {
        String filePath = null;
        OutputStream out = null;
        try {
            filePath = TempFileUtil.createTempXlsxFile("selectProduct");
            out = new FileOutputStream(filePath);
            ExcelWriter excelWriter = EasyExcelFactory.getWriter(out);
            Boolean only1688 = onlySelect1688(exportVO.getTaskId());
            DsSelectionQueryParam dsSelectionQueryParam = DsSelectionQueryParam
                    .builder()
                    .sortField(StringUtils.isEmpty(exportVO.getSortField()) ? SortFieldEnum.DS_CMT_COUNT_H2D.name()
                            : exportVO.getSortField())
                    .status(exportVO.getStatus())
                    .taskId(exportVO.getTaskId())
                    .build();
            dsSelectionQueryParam.setPageSize(100);
            ResultOf<PageResult<DsSelectionResultModel>> result;
            if (only1688) {
                return RPCResult.error("export error", "Only Match Pair Export is Supported");
            }
            Sheet sheet = new Sheet(1, "导出结果", SelectionItemExportVO.class);
            for (int i = 1; i < 51; i++) {
                dsSelectionQueryParam.setPageNo(i);
                result = selectionResultReadService.query(dsSelectionQueryParam);
                if (!ResultOf.isValid(result) || result.getData() == null || CollectionUtils.isEmpty(
                        result.getData().getList())) {
                    break;
                }
                List<SelectionItemExportVO> exportVOS = assembleExportVO(result.getData().getList());
                excelWriter.write(exportVOS, sheet);
            }
            excelWriter.finish();
            dataShareFileService.uploadFile("选品结果", filePath, Lists.newArrayList(LoginUtil.getEmpId(request)));
        } catch (Exception e) {
            log.error(" item query error ", e);
            return RPCResult.error("item query error " + e.getMessage(), e.toString());
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    log.error("output excel error", e.getMessage(), e);
                }
            }
            File file = new File(filePath);
            if (file.exists()) {
                file.delete();
            }
        }
        return RPCResult.ok();
    }

    private List<SelectionItemExportVO> assembleExportVO(List<DsSelectionResultModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        return list.stream()
                .map(a -> SelectionItemExportVO
                        .builder()
                        .dsCategory(a.getDsItem().getCategoryNameAllPath())
                        .dsItemUrl(a.getDsItem().getItemUrl())
                        .dsPrice(a.getDsItem().getPrice())
                        .dsSale30(a.getDsItem().getSaleCnt30() != null ? String.valueOf(a.getDsItem().getSaleCnt30()) : "0")
                        .url1688(a.getCbuItem().getItemUrl())
                        .sale301688(a.getCbuItem().getSale30() != null ? String.valueOf(a.getCbuItem().getSale30()) : "0")
                        .build())
                .collect(Collectors.toList());
    }

    @RequestMapping("/status")
    public RPCResult status(@RequestBody(required = false) StatusListVO data,
                            HttpServletRequest request) {
        List<SelectionItemStatusVO> statusVOS = data.getList();
        if (CollectionUtils.isEmpty(statusVOS)) {
            return RPCResult.error("param error", "param error");
        }
        try {
            Long taskId = statusVOS.get(0).getTaskId();
            String status = statusVOS.get(0).getStatus();
            List<String> ids = statusVOS.stream().map(a -> assembleKey(a)).collect(Collectors.toList());
            Boolean only1688 = onlySelect1688(taskId);
            DsSelectionUpdateParam dsSelectionUpdateParam = DsSelectionUpdateParam
                    .builder()
                    .status(status)
                    .taskId(taskId)
                    .cbuItemAndDsItemIds(Lists.newArrayList(ids))
                    .build();
            if (!only1688) {
                selectionResultWriteService.update(dsSelectionUpdateParam);
            } else {
                selectionResultWriteService.update1688Item(dsSelectionUpdateParam);
            }
        } catch (Exception e) {
            log.error("status error:", e);
            return RPCResult.error("status error:", e.getMessage());
        }
        return RPCResult.ok();
    }

    private String assembleKey(SelectionItemStatusVO statusVO) {
        if (StringUtils.isEmpty(statusVO.getDownItemId())) {
            return statusVO.getOfferId() + "";
        } else {
            return statusVO.getDownItemId() + "&|^" + statusVO.getOfferId();
        }
    }

    @RequestMapping("/distribution")
    public RPCResult distribution(@RequestBody(required = false) StatusListVO data,
                                  HttpServletRequest request) {
        try {
            BucSSOUser bucSSOUser = SimpleUserUtil.getBucSSOUser(request);
            com.alibaba.cbu.disco.shared.common.wrapper.ResultOf<?> resultOf =
                    aeDistributeService.distributionToAe(data, bucSSOUser);
            if (resultOf.isSuccess()) {
                return RPCResult.ok();
            } else {
                return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
            }
        } catch (Exception e) {
            log.error("", e);
            return RPCResult.error("distributionToAe error", e.getMessage());
        }
    }

    /**
     * 铺货到渠道
     *
     * @param data
     * @param request
     * @return
     */
    @RequestMapping("/distributionToChannel")
    public RPCResult distributionToChannel(@RequestBody(required = false) StatusListVO data,
                                           HttpServletRequest request) {
        try {
            BucSSOUser bucSSOUser = SimpleUserUtil.getBucSSOUser(request);
            com.alibaba.cbu.disco.shared.common.wrapper.ResultOf<?> resultOf =
                    commonDistributeService.distributionToChannel(data, data.getChannelIds(), bucSSOUser);
            if (resultOf.isSuccess()) {
                return RPCResult.ok();
            } else {
                return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
            }
        } catch (Exception e) {
            log.error("", e);
            return RPCResult.error("distributionToAe error", e.getMessage());
        }
    }

    /**
     * 获取渠道店铺列表信息
     *
     * @param request
     * @return
     */
    @RequestMapping("/getChannelShopList")
    public RPCResult getChannelShopList(HttpServletRequest request) {
        try {
            BucSSOUser bucSSOUser = SimpleUserUtil.getBucSSOUser(request);
            List<PlatformShopModel> platformShopModels = commonDistributeService.getChannelShopList(bucSSOUser);
            return RPCResult.ok(platformShopModels);
        } catch (Exception e) {
            log.error("error getChannelShopList", e);
            return RPCResult.error("getChannelShopList error", e.getMessage());
        }
    }

    /**
     * 判断是否只有1688的offer圈品
     *
     * @param taskId
     * @return
     */
    private Boolean onlySelect1688(Long taskId) {
        SelectionUnitQueryParam queryParam = SelectionUnitQueryParam.builder().taskId(taskId).build();
        ResultOf<List<SelectionUnitModel>> resultOf = selectionUnitService.query(queryParam);
        if (!ResultOf.isValid(resultOf)) {
            throw new RuntimeException("选品条件错误，请设置选品条件！");
        }
        for (SelectionUnitModel selectionUnitModel : resultOf.getData()) {
            if (SelectionChannelEnum.DOWNSTREAM.name().equals(selectionUnitModel.getSelectionChannel())) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

}