package com.alibaba.cbuscm.controller.common;

import com.alibaba.cbuscm.enums.international.InternationalChannelEnum;
import com.alibaba.cbuscm.vo.common.ChannelVO;
import com.alibaba.up.common.mybatis.result.RPCResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 渠道信息服务
 */
@RestController
@Slf4j
@RequestMapping("/channel")
public class ChannelController {

    /**
     * 获取境外渠道列表
     * @return
     */
    @RequestMapping("/internationalChannes")
    public RPCResult getInternationalChanneList() {
        InternationalChannelEnum[] channels = InternationalChannelEnum.values();
        List<ChannelVO> result = new ArrayList<ChannelVO>();
        Arrays.asList(channels).stream().forEach(channel->{
            ChannelVO vo = new ChannelVO();
            vo.setName(channel.getName());
            vo.setValue(channel.getValue());
            result.add(vo);
        });
        return RPCResult.ok(result);
    }

}
