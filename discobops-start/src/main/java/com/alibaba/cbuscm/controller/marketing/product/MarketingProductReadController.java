package com.alibaba.cbuscm.controller.marketing.product;

import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcItemEnrollDTO;
import com.alibaba.cbu.panama.dc.client.constants.marketingproduct.MarketingProductStatusEnum;
import com.alibaba.cbu.panama.dc.client.model.PageResult;
import com.alibaba.cbu.panama.dc.client.model.common.BizConstantDefDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.OperateCodeDTO;
import com.alibaba.cbu.panama.dc.client.model.param.marketingplan.MarketingPlanPageParam;
import com.alibaba.cbu.panama.dc.client.model.param.marketingproduct.MarketingProductPageParam;
import com.alibaba.cbuscm.RestResult;
import com.alibaba.cbuscm.contants.ProductImgContants;
import com.alibaba.cbuscm.service.marketing.DcEnrollReadServiceAdapter;
import com.alibaba.cbuscm.service.marketing.plan.MarketingPlanReadAdapter;
import com.alibaba.cbuscm.service.marketing.product.MarketingProductReadAdapter;
import com.alibaba.cbuscm.vo.marketing.param.MarketProductListReq;
import com.alibaba.cbuscm.vo.marketing.product.*;
import com.alibaba.china.offer.api.domain.general.Offer;
import com.alibaba.china.offer.api.query.model.OfferModel;
import com.alibaba.china.offer.api.query.service.OfferQueryService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
/**
 * @Auther: gxd
 * @Date: 2019/10/8 20:50
 * @Description:
 */
@RestController
@RequestMapping(path = "/marketingproduct/read", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MarketingProductReadController {

    @Autowired
    private MarketingProductReadAdapter marketingProductReadAdapter;

    @Autowired
    private MarketingPlanReadAdapter marketingPlanReadAdapter;

    @Autowired
    private OfferQueryService<OfferModel> offerQueryService;

    @Autowired
    private DcEnrollReadServiceAdapter dcEnrollReadServiceAdapter;

    /**
     * 报名活动商品列表
     * @param
     * @return
     */
    @GetMapping(value = "/list")
    public RestResult<PageResultResp<MarketingProductListRespVO>> pageMarketingProductByParam(
                                                                                              @RequestParam(required = false) String status,
                                                                                              @RequestParam(required = true) String pageCode,
                                                                                              @RequestParam(required = false) String planReviewer,
                                                                                              @RequestParam(required = false) String marketingPlanId,
                                                                                              @RequestParam(required = false) String marketingPlanName,
                                                                                              @RequestParam(required = false) String itemId,
                                                                                              @RequestParam(required = false) String itemName,
                                                                                              @RequestParam(required = false) String offerId,
                                                                                              @RequestParam(required = false) String loginId,
                                                                                              @RequestParam(required = true) Integer pageNo,
                                                                                              @RequestParam(required = true) Integer pageSize) {

        //封装参数
        PageResultResp<MarketingProductListRespVO> pageResult = new PageResultResp<>();
        pageResult.setPageNo(pageNo);
        pageResult.setPageSize(pageSize);

        MarketingProductPageParam param = new MarketingProductPageParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        if (StringUtils.isNotBlank(status)){
            List<String> arrayLists = JSON.parseArray(status, String.class);
            param.setStatusList(arrayLists);
        }

        if (StringUtils.isNotBlank(planReviewer)) {
            param.setPlanReviewer(planReviewer);
        }

        if (buildParam(marketingPlanId, itemId, offerId, loginId, param)) {
            return emptyResult(pageResult);
        }

        if(StringUtils.isNotBlank(marketingPlanName)){

            MarketingPlanPageParam pageParam = new MarketingPlanPageParam();
            pageParam.setPageSize(pageSize);
            pageParam.setPageNo(pageNo);
            pageParam.setMarketingPlanName(marketingPlanName);
            List<MarketingPlanDTO> list = marketingPlanReadAdapter.listMarketingPlanByParam(pageParam);
            if(CollectionUtils.isNotEmpty(list)){
                param.setMarketPlanIdList(list.stream().map(MarketingPlanDTO::getId).collect(Collectors.toList()));
            }else{
                return emptyResult(pageResult);
            }
        }


        //调用接口
        PageResult<MarketingProductListRespVO> respVOPageResult = marketingProductReadAdapter.marketingProductList(param);


        pageResult.setTotal(respVOPageResult.getTotal());

        if(Objects.nonNull(respVOPageResult)  && CollectionUtils.isNotEmpty(respVOPageResult.getData())){
            List<MarketingProductListRespVO> list = respVOPageResult.getData();
            List<Long> offerIds = list.stream().map(MarketingProductListRespVO::getOfferId).collect(Collectors.toList());

            // 获取操作code列表
            List<String> statusList = list.stream()
                    .map(MarketingProductListRespVO::getStatus).distinct().collect(Collectors.toList());
            Map<String, List<OperateCodeDTO>> operateCodeDTOMap = retrieveOperateCodeDTOMap(pageCode, statusList);


            Map<Long, OfferModel> offerMap = retrieveOfferModelList(offerIds);

            for (MarketingProductListRespVO vo : list) {
                // 封装1688的offer信息
                buildOfferInfo(offerMap, vo);

                buildItemInfo(vo);

                buildOperateInfo(operateCodeDTOMap, vo);
            }

            pageResult.setData(respVOPageResult.getData());

        }
        return RestResult.success(pageResult);
    }


    @GetMapping(value = "/listAllRejectReason")
    RestResult<List<BizConstantDefDTO>> listAllRejectReason(){
        return RestResult.success(marketingProductReadAdapter.listAllRejectReason());
    }


    private boolean buildParam(String marketingPlanId,String itemId,String offerId,String loginId, MarketingProductPageParam param) {
        if (StringUtils.isNotBlank(marketingPlanId)) {
            try{
                param.setMarketPlanIdList(Lists.newArrayList(Long.parseLong(marketingPlanId)));
            }catch(Exception e){
                return true;
            }
        }

        if(StringUtils.isNotBlank(offerId)){
            try{
                param.setOfferId(Long.parseLong(offerId));
            }catch(Exception e){
                return true;
            }
        }

        if(StringUtils.isNotBlank(itemId)){
            try{
                param.setItemId(Long.parseLong(itemId));
            }catch(Exception e){
                return true;
            }
        }


        if(StringUtils.isNotBlank(loginId)){
            try{
                Long userId = marketingProductReadAdapter.queryUserIdByLoginId(loginId);
                param.setSupplierId(userId);
            }catch(Exception e){
                return true;
            }
        }
        return false;
    }

    private RestResult<PageResultResp<MarketingProductListRespVO>> emptyResult(PageResultResp<MarketingProductListRespVO> pageResult) {
        pageResult.setData(null);
        pageResult.setTotal(0);
        return RestResult.success(pageResult);
    }


    private void buildOperateInfo(Map<String, List<OperateCodeDTO>> operateCodeDTOMap, MarketingProductListRespVO vo) {
        if (MapUtils.isNotEmpty(operateCodeDTOMap) &&
                CollectionUtils.isNotEmpty(operateCodeDTOMap.get(vo.getStatus()))) {
            vo.setActionList(convertOperateCode(operateCodeDTOMap.get(vo.getStatus())));
        }
    }


    private void buildItemInfo(MarketingProductListRespVO vo) {
        DcItemEnrollDTO dcOfferItemEnroll = dcEnrollReadServiceAdapter.queryOfferEnrollDTO(vo.getItemId());

        if(dcOfferItemEnroll != null) {
            vo.setItemTitle(dcOfferItemEnroll.getItemTitle());
            vo.setItemImg(dcOfferItemEnroll.getItemPicUrl());
            vo.setPubTime(dcOfferItemEnroll.getItemGmtGreate());
        }
    }

    private void buildOfferInfo(Map<Long, OfferModel> offerMap, MarketingProductListRespVO vo) {
        if (MapUtils.isNotEmpty(offerMap)) {
            OfferModel offerModel = offerMap.get(vo.getOfferId());
            Offer offer = offerModel.getOffer();
            if (null != offer) {
                vo.setOfferTitle(offer.getSubject());
                vo.setOfferImg(ProductImgContants.convertOfferImg(offer));
            }
        }
    }


    /**
     * 状态统计
     * @return
     */
    @GetMapping(value = "/statisticsStatus")
    public RestResult<StatisticsStatusRespVO> statisticsStatus(@RequestParam(required = false) Long marketingPlanId) {

        StatisticsStatusRespVO respVO = new StatisticsStatusRespVO();

        List<ProductStatusNumRespVO> productList =  marketingProductReadAdapter.statistics(marketingPlanId);

        respVO.setProductStatusNum(productList);

//        respVO.setTotalNum((int)productList.stream().map(ProductStatusNumRespVO::getNum).count());

        return RestResult.success(respVO);
    }


    /**
     * 报名活动商品详情页
     * @return
     */
    @GetMapping("/enrollOfferDetail")
    public RestResult<MarketingProductDetailRespVO> enrollOfferDetail(
            @RequestParam(required = true) Long marketingProductId,
            @RequestParam(required = true) Long itemId,
            @RequestParam(required = true) Long offerId,
            @RequestParam(required = true) String pageCode
    ){

        MarketProductListReq req = buildMarketProductListParam(marketingProductId, itemId, offerId, pageCode);

        MarketingProductDetailRespVO respVO = marketingProductReadAdapter.enrollOfferDetail(req);
        if(Objects.nonNull(respVO)){
            // 获取操作code列表
            List<String> statusList = Lists.newArrayList(respVO.getStatus());
            Map<String, List<OperateCodeDTO>> operateCodeDTOMap = retrieveOperateCodeDTOMap(pageCode, statusList);
            buildOperateInfo(operateCodeDTOMap, respVO);
        }

        return RestResult.success(respVO);
    }

    private MarketProductListReq buildMarketProductListParam(@RequestParam(required = true) Long marketingProductId, @RequestParam(required = true) Long itemId, @RequestParam(required = true) Long offerId, @RequestParam(required = true) String pageCode) {
        MarketProductListReq req = new MarketProductListReq();
        if (Objects.nonNull(itemId)){
            req.setItemId(itemId);
        }
        if (Objects.nonNull(offerId)){
            req.setOfferId(offerId);
        }
        if (StringUtils.isNotBlank(pageCode)){
            req.setPageCode(pageCode);
        }
        if (Objects.nonNull(marketingProductId)){
            req.setMarketingProductId(marketingProductId);
        }
        return req;
    }


    private void buildOperateInfo(Map<String, List<OperateCodeDTO>> operateCodeDTOMap, MarketingProductDetailRespVO vo) {
        if (MapUtils.isNotEmpty(operateCodeDTOMap) &&
                CollectionUtils.isNotEmpty(operateCodeDTOMap.get(vo.getStatus()))) {
            vo.setActionList(convertOperateCode(operateCodeDTOMap.get(vo.getStatus())));
        }
    }

    /**
     * 获取缴费金额
     * @return
     */
    @GetMapping("/getPaymentInfo")
    public RestResult<PaymentRespVO> getPaymentInfo(
            @RequestParam(required = true) Long marketingProductId
    ){
        return RestResult.success(marketingProductReadAdapter.getPayment(marketingProductId));
    }




    private Map<Long, OfferModel> retrieveOfferModelList(List<Long> offerIds) {
        Map<Long, OfferModel> offerMap = Maps.newHashMap();
        if (CollectionUtils.isEmpty(offerIds)) {
            return offerMap;
        }

        List<OfferModel> offerModels = offerQueryService.list(offerIds.toArray(new Long[0]));
        for (OfferModel offerModel : offerModels) {
            offerMap.put(offerModel.getOffer().getId(), offerModel);
        }
        return offerMap;
    }


    private Map<String, List<OperateCodeDTO>> retrieveOperateCodeDTOMap(String pageCode, List<String> statusList) {

        return marketingProductReadAdapter.listAvailableOperate(pageCode, statusList);
    }


    public static List<OperateActionRespVO> convertOperateCode(List<OperateCodeDTO> list) {
        List<OperateActionRespVO> codes = Lists.newArrayList();
        if (CollectionUtils.isEmpty(list)) {
            return codes;
        }
        for (OperateCodeDTO operateCodeDTO : list) {
            OperateActionRespVO action = new OperateActionRespVO();
            action.setOperateCode(operateCodeDTO.getOperateCode());
            action.setOperateDesc(operateCodeDTO.getOperateDesc());
            action.setIsLink(operateCodeDTO.getIsLink());
            codes.add(action);
        }
        return codes;
    }

}
