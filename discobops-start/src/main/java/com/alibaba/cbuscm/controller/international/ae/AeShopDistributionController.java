package com.alibaba.cbuscm.controller.international.ae;

import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.OperationResultOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.config.api.DcContextService;
import com.alibaba.cbu.disco.shared.core.config.model.DcContext;
import com.alibaba.cbu.disco.shared.core.product.api.DcDistributeManageService;
import com.alibaba.cbu.disco.shared.core.product.api.DcOfferImportService;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeEntryParam;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeOfferParam;
import com.alibaba.cbuscm.service.authority.ProductDataAccessService;
import com.alibaba.cbuscm.service.authority.model.DcChannelInfo;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.fastjson.JSON;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.*;

import static com.alibaba.cbuscm.utils.CollectionUtils.*;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/ae-shop/distribution", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class AeShopDistributionController {

    private final static String DIST_BIZ_TYPE = "CROSSBORDER";

    @Autowired
    DcOfferImportService dcOfferImportService;

    @Autowired
    private ProductDataAccessService productDataAccessService;

    @Autowired
    private DcDistributeManageService dcDistributeManageService;

    @Autowired
    private DcContextService dcContextService;

    @ResponseBody
    @RequestMapping(value = "/distribution_to_ae")
    public Object distributionToAe(@RequestParam(value = "entries", required = true) String inputEntries,
                                   @RequestParam(value = "taskId", required = true) Long taskId,
                                   HttpServletRequest request) {
        try {
            List<DistributeEntry> entries = JSON.parseArray(inputEntries, DistributeEntry.class);

            List<DcDistributeEntryParam> distributeEntryList = streaming(entries)
                .filter(Objects::nonNull)
                .map(entry -> new DcDistributeEntryParam(
                    entry.getOfferId(), entry.getSite(), pureDownstreamItemId(entry.getItemId())))
                .collect(toList());

            if (isEmpty(distributeEntryList)) {
                return ResultOf.error("empty entry", "entries cannot be null or empty");
            }

            // 获取用户的授权渠道+配置的铺货渠道
            List<String> distChannels = getDistributeChannels(request);
            log.info("user authorized distribution channels: {}", distChannels);
            if (isEmpty(distChannels)) {
                return ResultOf.error("ERR_DISTRIBUTE", "current user has no available channel");
            }

            // 默认取第一个渠道的 bizType
            DcContext.Channel channel = dcContextService.getChannel(distChannels.get(0));

            // 进行多渠道铺货
            DcDistributeOfferParam distributeOfferParam = DcDistributeOfferParam.builder()
                .channels(distChannels)
                .bizType(channel.getBizTypeName())
                .entries(distributeEntryList)
                .selectionTaskId(taskId)
                .operator(OperatorModel.builder()
                    .id(LoginUtil.getEmpId(request))
                    .name(LoginUtil.getLoginName(request))
                    .build())
                .build();
            ListOf<OperationResultOf<Long>> listOf = dcDistributeManageService.batchDistribute(distributeOfferParam);

            if (ListOf.isValid(listOf)) {
                return ResultOf.general(listOf.getData());
            } else {
                List<OperationResultOf<Long>> result = listOf.getData();
                if (isEmpty(result) || result.size() == 1) {
                    return ResultOf.error(listOf.getErrorCode(), listOf.getErrorMessage());
                }
                long successCount = streaming(result).map(OperationResultOf::getResult).filter(TRUE::equals).count();
                long errorCount = result.size() - successCount;
                return ResultOf.error(listOf.getErrorCode(),
                    String.format("%d个开始铺货，%d个不能铺货", successCount, errorCount));
            }
        } catch (Exception e) {
            log.error("distributionToAe error, entries:{}, taskId：{}", inputEntries, taskId, e);
            return ResultOf.error("sys error", e.getMessage());
        }
    }

    private List<String> getDistributeChannels(HttpServletRequest request) {

            // 管理员配置的铺货渠道
            ListOf<String> opChannels = dcDistributeManageService.getDistributeChannels(LoginUtil.getEmpId(request));
            Set<String> opChannelSet = ListOf.isNonEmpty(opChannels)
                ? toSet(opChannels.getData())
                : emptySet();

            log.info("operator channels: {}", opChannels);

            // 管理员已授权的渠道
            List<DcChannelInfo> authorizedChannelList = getAuthorizedChannelList(request);

            log.info("authorized channels: {}", authorizedChannelList);
            return streaming(authorizedChannelList)
                .map(DcChannelInfo::getChannel)
                .filter(opChannelSet::contains)
                .collect(toList());
    }

    private List<DcChannelInfo> getAuthorizedChannelList(HttpServletRequest request) {
        try {
            Integer userId = LoginUtil.getUserId(request);
            ListOf<DcChannelInfo> authorizedChannelList = productDataAccessService.queryUserAuthorizedChannel(userId);
            return ListOf.isValid(authorizedChannelList) ? authorizedChannelList.getData() : Collections.emptyList();
        } catch (Exception e) {
            log.error("AeShopDistributionController.getAuthorizedChannelList exception!", e);
            return Collections.emptyList();
        }
    }

    /**
     * 前端传入的数据格式不是真的下游站点id，格式：itemId$site
     *
     * @param originItemId
     * @return
     */
    private String pureDownstreamItemId(String originItemId) {
        if (StringUtils.isBlank(originItemId)) {
            return null;
        }
        String[] data = originItemId.split("\\$");
        return data[0];
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    public static class DistributeEntry implements Serializable {

        /**
         * 1688 offer id
         */
        private Long offerId;

        /**
         * 下游站点
         */
        private String site;

        /**
         * 下游商品Id
         */
        private String itemId;
    }
}
