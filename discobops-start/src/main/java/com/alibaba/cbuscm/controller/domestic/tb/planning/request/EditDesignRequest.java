package com.alibaba.cbuscm.controller.domestic.tb.planning.request;

import com.alibaba.china.shared.discosupplier.model.common.UserModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningAttr;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningCompetitor;
import lombok.Data;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author wb-qiuth
 * 编辑企划接收参数
 */
@Data
@ToString
public class EditDesignRequest {
    /**
     * 企划id
     */
    private Long id;
    /**
     * 企划类型
     */
    private String type;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String desc;
    /**
     * 渠道
     */
    private String scene;
    /**
     * 处理人
     */
    private Set<UserModel> handlers;
    /**
     * 管理员
     */
    private Set<UserModel> admins;
    /**
     * 波段
     */
    private String waveBrand;
    /**
     * 上线时间
     */
    private Date upTime;
    /**
     * 核心卖点
     */
    private String sellPoint;
    /**
     * 淘系类目id(类目1id,类目2id,类目3id)
     */
    private String categoryId;
    /**
     * 淘系类目名称(类目1名称/类目2名称/类目3名称)
     */
    private String categoryValue;
    /**
     * 提报下限
     */
    private Integer importLower;
    /**
     * 提报上限
     */
    private Integer importUp;
    /**
     * 规格范围
     */
    private String specScope;
    /**
     * 最小规格
     */
    private String specMin;
    /**
     * 最小规格指导价
     */
    private String specMinPrice;
    /**
     * 预期转换率
     */
    private String expConvertRate;
    /**
     * 日销售预期
     */
    private String dailySales;
    /**
     * 配给流量
     */
    private String distriFlow;
    /**
     * 备注
     */
    private String remark;
    /**
     * 企划关键key value
     */
    private List<PlanningAttr> keyAttrs;
    /**
     * 企划竞品信息
     */
    private List<PlanningCompetitor> competitors;
    /**
     * 提报下限
     */
    private Integer importMax;
    /**
     * 是否发布
     */
    private Boolean release;
    /**
     *  EXCEL路径
     */
    private MultipartFile file;

    /**
     * 规格信息
     */
    private List<PlanningAttr> specs;

    /**
     * 引流转化预测
     */
    private String drainagePred;

    /**
     * 企划定位
     */
    private String orientation;
}
