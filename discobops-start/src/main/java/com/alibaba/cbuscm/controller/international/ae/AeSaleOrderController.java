package com.alibaba.cbuscm.controller.international.ae;

import com.alibaba.up.common.mybatis.result.RPCResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xuhb
 * @title: AeSaleOrderController
 * @projectName discobops
 * @description: ae 销售订单服务
 * @date 2019-05-0916:07
 */
@RestController
@Slf4j
@RequestMapping("/aesaleOrder")
public class AeSaleOrderController {

    /**
     *
     * @param saleOrderId 销售单id
     * @param purchaseOrderId 采购单id
     * @param status    状态
     * @param createTime 采购单生成时间
     * @return
     */
    @RequestMapping("/getQGAEOrderList")
    public RPCResult getQGOrderList(@RequestParam(name="saleOrderId",required = false) String saleOrderId,
                                    @RequestParam(name="purchaseOrderId",required = false) String purchaseOrderId,
                                    @RequestParam(name="status",required = false) String status,
                                    @RequestParam(name="createTime",required = false) String createTime,
                                    @RequestParam(name="pageNo",required = false) int page,
                                    @RequestParam(name="pageSize",required = false) int pageSize){
        return RPCResult.ok();

    }
}
