package com.alibaba.cbuscm.controller.onepiece.opportunity;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbuscm.contants.MarketConstants;
import com.alibaba.china.dw.dataopen.api.LastDateAPI;
import com.alibaba.china.dw.dataopen.api.QueryException;
import com.alibaba.china.dw.dataopen.api.User;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.up.common.mybatis.result.ResultError;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商机中心通讯表
 * Created by duanyang.zdy on 2019/9/2.
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MarketCommuTableController {


    @Resource
    private LastDateAPI lastDateAPI;

    private static DateTimeFormatter daDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    @RequestMapping("marketOverview/queryCommTableDate")
    public Object queryMarketBaseIndex(HttpServletRequest request) {


        String tableDate = getTableDate(MarketConstants.MARKET_COMMU_TABLE_NAME);

        if(StringUtils.isEmpty(tableDate)){

            return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, "tableDate is null");

        }

        return ResultOf.general(tableDate);

    }



    public String getTableDate(String tableName){

        String dt="";
        if(StringUtils.isNotEmpty(tableName)){

            try {
                Date date = lastDateAPI.getVirtualTableDate(getUserInfo(),tableName);

                if(date != null){
                    LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
                    dt = ldt.format(daDateTimeFormatter);
                }


            }catch (QueryException e){

                return dt;
            }

        }

        return dt;
    }


    //生成OneService User信息
    static User getUserInfo() {
        User user = new User();
        user.setAppName(MarketConstants.AppName);
        user.setName(MarketConstants.AppName);
        user.setPassword(MarketConstants.Pass);
        return user;
    }


}
