package com.alibaba.cbuscm.controller.onepiece.opportunity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbuscm.common.PageResult;
import com.alibaba.cbuscm.contants.MarketConstants;
import com.alibaba.cbuscm.controller.onepiece.opportunity.util.TairAdapter;
import com.alibaba.cbuscm.service.AEOpenSearchService;
import com.alibaba.cbuscm.service.onepiece.search.structure.OpenSearchQueryParam;
import com.alibaba.cbuscm.service.onepiece.search.structure.attributes.CBUOverseasOppoAttribute;
import com.alibaba.cbuscm.service.onepiece.search.structure.fields.CBUOverseasOppoField;
import com.alibaba.cbuscm.service.onepiece.search.structure.indexes.CBUOverseasOppoIndexe;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Filter;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Query;
import com.alibaba.cbuscm.service.onepiece.search.structure.interfaces.Sort;
import com.alibaba.cbuscm.utils.CollectionUtils;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketMatchResultModel;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketViewResultModel;
import com.alibaba.china.dw.dataopen.api.*;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.enums.ErrorCode;
import com.alibaba.china.global.business.library.interfaces.onepiece.bussinessOppo.UserDataRightQueryService;
import com.alibaba.dataworks.dataservice.model.api.protocol.ApiProtocol;
import com.alibaba.dataworks.dataservice.model.hsf.DataParam;
import com.alibaba.dataworks.dataservice.sdk.facade.DataApiClient;
import com.alibaba.dataworks.dataservice.sdk.loader.http.Request;
import com.alibaba.dataworks.dataservice.sdk.loader.http.enums.Method;
import com.alibaba.dataworks.dataservice.service.HsfDataApiService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.up.common.mybatis.result.RPCResult;

import com.ali.unit.rule.util.lang.StringUtils;
import com.taobao.tair.json.Json;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class MarketMatrixController {

    @Resource
    private SQLIDQueryAPI sqlidQueryService;

    @Resource
    private UserDataRightQueryService userDataRightQueryService;

    @Resource
    private DataApiClient dataApiClient;

    @Resource
    private TairAdapter tairAdapter;

    @Resource
    private AEOpenSearchService aeOpenSearchService;

    public static final String MarketViewQuery_Pre = "disco_market_list_";


    private static final Map<String, String> fieldMapping = new HashMap<>();

    private static final Map<String, String> queryMapping = new HashMap<>();

    static {
        fieldMapping.put("productNum", "product_num");
        fieldMapping.put("marketGmv30d", "market_gmv_30d");
        fieldMapping.put("marketSales30d", "market_sales_30d");
        fieldMapping.put("marketShareForGmv", "market_share_for_gmv");
        fieldMapping.put("marketShareForSales", "market_share_for_sales");
        fieldMapping.put("marketGrowthForGmv", "market_growth_for_gmv");
        fieldMapping.put("marketGrowthForSales", "market_growth_for_sales");
        fieldMapping.put("rateForSamePic", "rate_for_same_pic");
        fieldMapping.put("rateForSameStyle", "rate_for_same_style");
        fieldMapping.put("rateForSimilarStyle", "rate_for_similar_style");

        //全球
        queryMapping.put("GlobalDESC", "/project/21519/bussinessoppo/marketview_copy");
        queryMapping.put("GlobalASC", "/project/21519/bussinessoppo/marketview_copy_copy");
        queryMapping.put("GlobalCount", "/project/21519/bussinessoppo/marketview_copy_copy");

        //全球 无定性 ok
        queryMapping.put("GlobalNoTagDESC", "/project/21519/bussinessoppo/marketview_notag_copy_copy");
        queryMapping.put("GlobalNoTagASC", "/project/21519/bussinessoppo/marketview_notag_copy_copy_copy");
        queryMapping.put("GlobalNoTagCount", "/project/21519/bussinessoppo/marketview_copy_copy_copy_copy");

        //站点
        queryMapping.put("SiteDESC", "/project/21519/bussinessoppo/marketview_site_copy");
        queryMapping.put("SiteASC", "/project/21519/bussinessoppo/marketview_site_copy_copy");
        queryMapping.put("SiteCount", "/project/21519/bussinessoppo/marketview_site_copy_copy");

        //站点 无定性 ok
        queryMapping.put("SiteNoTagDESC", "/project/21519/bussinessoppo/marketview_site_notag_copy_copy");
        queryMapping.put("SiteNoTagASC", "/project/21519/bussinessoppo/marketview_site_notag_copy_copy_copy");
        queryMapping.put("SiteNoTagCount", "/project/21519/bussinessoppo/marketview_site_copy_copy_copy_copy_copy");

        //站点类目 ok
        queryMapping.put("CateDESC", "/project/21519/bussinessoppo/marketview_cate_copy");
        queryMapping.put("CateASC", "/project/21519/bussinessoppo/marketview_cate_copy_copy");
        queryMapping.put("CateCount", "/project/21519/bussinessoppo/marketview_cate_copy_copy_copy_copy_copy");


        //类目无定性 统计 ok
        queryMapping.put("CateNoTagDESC", "/project/21519/bussinessoppo/marketview_cate_notag_copy_copy");
        queryMapping.put("CateNoTagASC", "/project/21519/bussinessoppo/marketview_cate_notag_copy_copy_copy");
        queryMapping.put("CateCountNoTag", "/project/21519/bussinessoppo/marketview_cate_copy_copy_copy_copy_copy_copy");


        //市场名称查询
        queryMapping.put("MarketNameQuery", "/project/21519/bussinessoppo/marketview");

    }

    /**
     * 查询市场矩阵 市场列表
     *
     * @param statDate     分区时间
     * @param isGlobal     是否全球
     * @param siteCode     站点名称
     * @param cateId       类目Id
     * @param marketName   市场名称
     * @param marketTags   市场定性
     * @param orderByField 排序字段
     * @param orderByType  排序类型
     * @param pageIndex    当前页数
     * @param pageNum      当前页数量
     * @param productNum   商品数量门槛
     * @param type         维度类型 gmv sales ..
     * @param request
     * @return 矩阵列表
     */
    @RequestMapping("/marketOverview/queryMarketList2")
    public RPCResult queryMarketList(@RequestParam String statDate, @RequestParam(defaultValue = "0") Integer isGlobal,
                                     @RequestParam(defaultValue = "") String siteCode, @RequestParam(defaultValue = "") String cateId,
                                     @RequestParam(defaultValue = "gmv") String type, @RequestParam(defaultValue = "") String marketName,
                                     @RequestParam(defaultValue = "") String marketTags, @RequestParam(defaultValue = "marketGmv30d") String orderByField,
                                     @RequestParam(defaultValue = "desc") String orderByType, @RequestParam(defaultValue = "1") Integer pageIndex,
                                     @RequestParam(defaultValue = "100") Integer pageNum, @RequestParam(defaultValue = "500") Integer productNum,
                                     @RequestParam(defaultValue = "0") Integer isBoston,
                                     HttpServletRequest request) {

        //每个参数均不能为空
        if (StringUtils.isBlank(statDate)
                || pageIndex == null
                || pageIndex < 0 || pageNum == null || pageNum <= 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "请求必要参数 error");
        }


        MarketViewResultModel marketViewResultModel = new MarketViewResultModel();
        Request request1 = new Request();
        request1.setMethod(Method.GET);
        request1.setAppCode("A045FA0EADB944198703D655E993417E");
        request1.setHost("https://dataservice-api.dw.alibaba-inc.com");
        request1 = setTheMarketViewRequest(request1, isGlobal, orderByType, marketTags, siteCode, cateId, marketName);

        if (request1 == null) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "参数异常");
        }

        Integer startOffset = pageIndex - 1;
        request1.getBodys().put("stat_date", statDate);
        request1.getBodys().put("type", type);
        request1.getBodys().put("product_num", productNum);
        request1.getBodys().put("startOffset", startOffset);
        request1.getBodys().put("pageSize", pageNum);
        request1.getBodys().put("oderColumn", fieldMapping.get(orderByField));
        request1.setApiProtocol(ApiProtocol.HTTP);

        try {
            HashMap response = dataApiClient.dataLoad(request1);
            Assert.state((Integer) response.get("errCode") == 0, (String) response.get("errMsg"));
            JSONArray data = (JSONArray) response.get("data");
            List<MarketViewResultModel.MarketViewModel> markets = JSON.parseArray(data.toJSONString(), MarketViewResultModel.MarketViewModel.class);

            marketViewResultModel = getMarketViewModel(markets, pageIndex, pageNum, statDate);
            if (markets != null && markets.size() > 0) {
                marketViewResultModel.setCount(markets.get(0).getTotal());
            }

            return RPCResult.ok(marketViewResultModel);

        } catch (Exception e) {
            log.error("queryMarketList 出现异常 stat_date: " + statDate + " isGlobal:" + isGlobal + " siteCode:" + siteCode
                    + "cateId:" + cateId + " type:" + type + " marketName:" + marketName + " marketTags:" + marketTags + " orderByField:" + orderByField
                    + "orderByType:" + orderByType + " pageIndex:" + pageIndex + " pageNum:" + pageNum + " productNum:" + productNum);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "d2 查询数据出现异常");
        }

    }


    /**
     * 查询市场总数
     *
     * @param isGlobal   是否全球
     * @param marketTags 市场定性
     * @param siteCode   站点
     * @param cateId     类目
     */
    Integer getMarketCount(Integer isGlobal, String marketTags, String siteCode, String cateId, String statDate, String type, Integer productNum) {

        Request request = new Request();
        request.setMethod(Method.GET);
        request.setAppCode("A045FA0EADB944198703D655E993417E");
        request.setHost("https://dataservice-api.dw.alibaba-inc.com");

        Request request1 = setMarketCountRequest(request, isGlobal, marketTags, siteCode, cateId, statDate, type, productNum);
        if (request1 == null) {
            log.error("查询d2 市场总数时候出现问题");
            return 0;
        }

        request.setApiProtocol(ApiProtocol.HTTP);

        try {
            HashMap response = dataApiClient.dataLoad(request);
            Assert.state((Integer) response.get("errCode") == 0, (String) response.get("errMsg"));
            JSONObject data = (JSONObject) response.get("data");
            Integer total = (Integer) data.get("total");

            if (total != null) {
                return total;
            }


        } catch (Exception e) {
            log.error("查询市场总数的时候出现异常" + e.getStackTrace());
        }
        return 0;
    }


    private Request setMarketCountRequest(Request request, Integer isGlobal, String marketTags, String siteCode, String cateId, String statDate, String type, Integer productNum) {


        request.getBodys().put("stat_date", statDate);
        request.getBodys().put("type", type);
        request.getBodys().put("product_num", productNum);

        if (isGlobal == 1 && !"".equals(marketTags)) {
            //全球定性 统计
            request.setPath(queryMapping.get("GlobalCount"));
            request.getBodys().put("market_tags", marketTags);

        } else if (isGlobal == 1 && "".equals(marketTags)) {
            //全球 不定性 统计
            request.setPath(queryMapping.get("GlobalNoTagCount"));
        } else if (isGlobal == 0 && "".equals(marketTags) && !"".equals(siteCode)) {
            //站点 不定性 统计
            request.getBodys().put("site_code", siteCode);

            request.setPath(queryMapping.get("SiteNoTagCount"));
        } else if (isGlobal == 0 && !"".equals(marketTags) && !"".equals(siteCode)) {
            //站点 定性 统计

            request.setPath(queryMapping.get("SiteCount"));
            request.getBodys().put("site_code", siteCode);
            request.getBodys().put("market_tags", marketTags);
        } else if (isGlobal == 0 && "".equals(marketTags) && !"".equals(siteCode) && !"".equals(cateId)) {
            //类目 不定性 统计

            request.getBodys().put("site_code", siteCode);
            request.getBodys().put("cate_id", cateId);
            request.setPath(queryMapping.get("CateCountNoTag"));

        } else if (isGlobal == 0 && !"".equals(marketTags) && !"".equals(siteCode) && !"".equals(cateId)) {
            //类目 定性 统计

            request.getBodys().put("site_code", siteCode);
            request.getBodys().put("cate_id", cateId);
            request.getBodys().put("market_tags", marketTags);
            request.setPath(queryMapping.get("CateCount"));

        } else {
            return null;
        }

        return request;

    }


    private Request setTheMarketViewRequest(Request request, Integer isGlobal, String orderByType, String marketTags, String siteCode, String cateId, String marketName) {


        //使用市场名称查询时 直接返回
        if (marketName != null && !"".equals(marketName)) {
            request.setPath(queryMapping.get("MarketNameQuery"));
            request.getBodys().put("market_name", marketName);
            request.getBodys().put("is_global", isGlobal);
            return request;
        }

        if (isGlobal == 1 && "desc".equals(orderByType) && !"".equals(marketTags)) {
            //全球 定性 降序
            request.setPath(queryMapping.get("GlobalDESC"));
            request.getBodys().put("market_tags", marketTags);

        } else if (isGlobal == 1 && "asc".equals(orderByType) && !"".equals(marketTags)) {
            //全球 定性 升序
            request.setPath(queryMapping.get("GlobalASC"));
            request.getBodys().put("market_tags", marketTags);

        } else if (isGlobal == 1 && "desc".equals(orderByType) && "".equals(marketTags)) {
            //全球 无定性 降序
            request.setPath(queryMapping.get("GlobalNoTagDESC"));
        } else if (isGlobal == 1 && "asc".equals(orderByType) && "".equals(marketTags)) {
            //全球 无定性  升序
            request.setPath(queryMapping.get("GlobalNoTagASC"));
        } else if (isGlobal == 0 && "desc".equals(orderByType) && !"".equals(siteCode) && "".equals(cateId) && "".equals(marketTags)) {
            //非全球 站点 无定性 降序
            request.getBodys().put("site_code", siteCode);
            request.setPath(queryMapping.get("SiteNoTagDESC"));
        } else if (isGlobal == 0 && "asc".equals(orderByType) && !"".equals(siteCode) && "".equals(cateId) && "".equals(marketTags)) {
            //非全球 站点 无定性 升序
            request.getBodys().put("site_code", siteCode);
            request.setPath(queryMapping.get("SiteNoTagASC"));
        } else if (isGlobal == 0 && "desc".equals(orderByType) && !"".equals(siteCode) && "".equals(cateId) && !"".equals(marketTags)) {
            //非全球 站点  有定性 降序
            request.getBodys().put("market_tags", marketTags);
            request.getBodys().put("site_code", siteCode);
            request.setPath(queryMapping.get("SiteDESC"));
        } else if (isGlobal == 0 && "asc".equals(orderByType) && !"".equals(siteCode) && "".equals(cateId) && !"".equals(marketTags)) {
            //非全球 站点 有定性  升序
            request.getBodys().put("market_tags", marketTags);
            request.getBodys().put("site_code", siteCode);
            request.setPath(queryMapping.get("SiteASC"));
        } else if (isGlobal == 0 && "desc".equals(orderByType) && !"".equals(siteCode) && !"".equals(cateId) && "".equals(marketTags)) {
            //非全球 站点 类目 无定性 降序
            request.getBodys().put("site_code", siteCode);
            request.getBodys().put("cate_id", cateId);
            request.setPath(queryMapping.get("CateNoTagDESC"));
        } else if (isGlobal == 0 && "asc".equals(orderByType) && !"".equals(siteCode) && !"".equals(cateId) && "".equals(marketTags)) {
            //非全球 站点 类目 无定性 升序
            request.getBodys().put("site_code", siteCode);
            request.getBodys().put("cate_id", cateId);
            request.setPath(queryMapping.get("CateNoTagASC"));

        } else if (isGlobal == 0 && "desc".equals(orderByType) && !"".equals(siteCode) && !"".equals(cateId) && !"".equals(marketTags)) {
            //非全球 站点 类目 有定性 降序
            request.getBodys().put("market_tags", marketTags);
            request.getBodys().put("site_code", siteCode);
            request.getBodys().put("cate_id", cateId);
            request.setPath(queryMapping.get("CateDESC"));

        } else if (isGlobal == 0 && "asc".equals(orderByType) && !"".equals(siteCode) && !"".equals(cateId) && !"".equals(marketTags)) {
            //非全球 站点 类目 有定性 升序
            request.getBodys().put("market_tags", marketTags);
            request.getBodys().put("site_code", siteCode);
            request.getBodys().put("cate_id", cateId);
            request.setPath(queryMapping.get("CateASC"));
        } else {
            return null;
        }

        return request;

    }


    private MarketViewResultModel getMarketViewModel(List<MarketViewResultModel.MarketViewModel> markets, Integer pageIndex, Integer pageNum, String statDate) {

        MarketViewResultModel marketViewResultModel = new MarketViewResultModel();
        marketViewResultModel.setData(markets);
        marketViewResultModel.setPageIndex(pageIndex);
        marketViewResultModel.setPageNum(pageNum);
        marketViewResultModel.setSuccess(true);

        if (markets != null && markets.size() > 0) {
            marketViewResultModel.setStat_date(statDate);
            marketViewResultModel.setXMedian(markets.get(0).getXMedian());
            marketViewResultModel.setYMedian(markets.get(0).getYMedian());
            marketViewResultModel.setYMax(getYmax(markets));
        } else {
            marketViewResultModel.setStat_date(statDate);
            marketViewResultModel.setXMedian(BigDecimal.ZERO);
            marketViewResultModel.setYMedian(BigDecimal.ZERO);
            marketViewResultModel.setYMax(BigDecimal.ZERO);
        }
        return marketViewResultModel;
    }


    /**
     * 市场详情 查询
     *
     * @param statDate  分区时间
     * @param marketId  市场id
     * @param indexType 维度类型 gmv slaes 不如建议前端直接传 gmv sales 来的直观
     * @param request
     * @return 市场详情数据
     */
    @RequestMapping("/marketDetail/queryMarketBaseIndex")
    public RPCResult queryMarketBaseIndex(@RequestParam String statDate, @RequestParam String marketId, @RequestParam Integer indexType, HttpServletRequest request) {

        MarketMatchResultModel resultModel = null;
        //校验参数
        if (StringUtils.isBlank(statDate) || StringUtils.isBlank(marketId) || indexType == null) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "请求必要参数 error");
        }
        String type = "";
        if (0 == indexType) {
            type = "gmv";
        } else if (1 == indexType) {
            type = "sales";
        }
        // 建立查询条件
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("stat_date", statDate);
        conditions.put("market_id", marketId);
        conditions.put("type", type);
        //设置返回类型
        String[] returnFields = {
                "market_id", "market_name", "site_code", "cate_id", "cate_name",
                "type", "x", "y", "market_gmv_30d", "market_sales_30d",
                "product_num", "rate_for_same_pic", "rate_for_same_style", "rate_for_similar_style", "cbu_offer_num_for_same_pic"
                , "cbu_offer_num_for_same_style", "cbu_offer_num_for_similar_style",
                "down_item_num_for_same_pic", "down_item_num_for_same_style", "down_item_num_for_similar_style",
                "is_global", "type"};
        //还需要user用户信息
        User user = getUserInfo();

        try {
            Result result = sqlidQueryService.get(user, MarketConstants.MarketDetailuery_SQL_ID, conditions, returnFields);
            if (result == null) {
                return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "query result null");
            }
            if (!result.isSuccessed()) {
                return RPCResult.error(RPCResult.CODE_SERVER_ERROR, result.getMessage());
            }
            //此处需要大修改
            Map<String, Object> matchResult = result.getResult();
            MarketMatchResultModel.MarketMatchIndexModel marketViewResultModel = buildMarketDetailFromMap(matchResult);
            resultModel = new MarketMatchResultModel();
            resultModel.setData(marketViewResultModel);
            return RPCResult.ok(resultModel);
        } catch (Exception e) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "query 出现异常 " + e.getMessage());
        }
    }

    /**
     * 通过用户
     *
     * @param request
     * @return
     */
    @RequestMapping("/userRightSties")
    public RPCResult userRightSties(HttpServletRequest request) {

        List<String> theUserRightSites = null;
        String workId = "";
        try {
            workId = LoginUtil.getEmpId(request);
        } catch (Exception e) {
            log.error("LoginUtil.getEmpId 出现异常 e:" + e.getStackTrace());
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "LoginUtil.getEmpId 出现异常 或未登陆 e:" + e.getMessage());
        }

        if (StringUtils.isBlank(workId)) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, " 未登陆 ");
        }


        try {
            theUserRightSites = getTheUserRightSites(workId);
        } catch (Exception e) {
            log.error("userRightSties 出现异常 workId:" + workId + " e:" + e.getStackTrace());
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "userRightSties 出现异常 e " + e.getMessage());

        }
        if (theUserRightSites == null) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "getTheUserRightSites error");
        }
        return RPCResult.ok(theUserRightSites);
    }


    /**
     * 通过用户
     *
     * @param request
     * @return
     */
    @RequestMapping("/test")
    public RPCResult test(@RequestParam String key, HttpServletRequest request) {
        {
            MarketViewResultModel marketViewResultModel = new MarketViewResultModel();
            Object o = tairAdapter.get(key);
            if (o == null) {
                return RPCResult.ok(marketViewResultModel);
            }
            marketViewResultModel = JSONObject.parseObject(String.valueOf(o), MarketViewResultModel.class);
            return RPCResult.ok(marketViewResultModel);
        }

    }

    private List<MarketViewResultModel.MarketViewModel> buildMarketsFromMap
            (List<Map<String, Object>> rawResults) {
        List<MarketViewResultModel.MarketViewModel> markets = new ArrayList<>();
        if (rawResults == null || rawResults.isEmpty()) {
            return markets;
        }
        for (Map<String, Object> raw : rawResults) {

            try {

                MarketViewResultModel.MarketViewModel marketViewModel = new MarketViewResultModel.MarketViewModel();

                marketViewModel.setMarketId(String.valueOf(raw.get("MARKET_ID")));
                marketViewModel.setMarketName(String.valueOf(raw.get("MARKET_NAME")));

                if (raw.get("CATE_ID") == null) {
                    marketViewModel.setCateId("");
                } else {
                    marketViewModel.setCateId(String.valueOf(raw.get("CATE_ID")));
                }

                if (raw.get("CATE_NAME") == null) {
                    marketViewModel.setCateName("");
                } else {
                    marketViewModel.setCateName(String.valueOf(raw.get("CATE_NAME")));
                }
                marketViewModel.setX(BigDecimal.valueOf(Double.parseDouble(String.valueOf(raw.get("X")))));
                marketViewModel.setY(BigDecimal.valueOf(Double.parseDouble(String.valueOf(raw.get("Y")))));
                marketViewModel.setSiteCode(String.valueOf(raw.get("SITE_CODE")));
                marketViewModel.setType(String.valueOf(raw.get("TYPE")));
                marketViewModel.setMarketGmv30d(Double.parseDouble(String.valueOf(raw.get("MARKET_GMV_30D"))));
                marketViewModel.setMarketSales30d(Double.parseDouble(String.valueOf(raw.get("MARKET_SALES_30D"))));
                marketViewModel.setRateForSamePic(Double.parseDouble(String.valueOf(raw.get("RATE_FOR_SAME_PIC"))));
                marketViewModel.setRateForSameStyle(Double.parseDouble(String.valueOf(raw.get("RATE_FOR_SAME_STYLE"))));
                marketViewModel.setRateForSimilarStyle(Double.parseDouble(String.valueOf(raw.get("RATE_FOR_SIMILAR_STYLE"))));
                marketViewModel.setXMedian(BigDecimal.valueOf(Double.parseDouble(String.valueOf(raw.get("X_MEDIAN")))));
                marketViewModel.setYMedian(BigDecimal.valueOf(Double.parseDouble(String.valueOf(raw.get("Y_MEDIAN")))));
                marketViewModel.setMarketTags(String.valueOf(raw.get("MARKET_TAGS")));
                marketViewModel.setIsGlobal(String.valueOf(raw.get("IS_GLOBAL")));
                marketViewModel.setSites(String.valueOf(raw.get("SITES")));
                marketViewModel.setProductNum(String.valueOf(raw.get(("PRODUCT_NUM"))).split("\\.")[0]);
                marketViewModel.setPic(String.valueOf(raw.get("IMAGES")));
                marketViewModel.setSiteNum(String.valueOf(raw.get("SITES")).split(",").length);
                marketViewModel.setStat_date(String.valueOf(raw.get("STAT_DATE")));
                marketViewModel.setMarketGrowthForGmv(Double.parseDouble(String.valueOf(raw.get("MARKET_GROWTH_FOR_GMV"))));
                marketViewModel.setMarketGrowthForSales(Double.parseDouble(String.valueOf(raw.get("MARKET_GROWTH_FOR_SALES"))));
                marketViewModel.setMarketShareForGmv(Double.parseDouble(String.valueOf(raw.get("MARKET_SHARE_FOR_GMV"))));
                marketViewModel.setMarketShareForSales(Double.parseDouble(String.valueOf(raw.get("MARKET_SHARE_FOR_SALES"))));
                markets.add(marketViewModel);

            } catch (Exception e) {
                log.error("buildMarketsFromMap 出现异常 e", e.getStackTrace());
            }

        }

        return markets;
    }


    private MarketMatchResultModel.MarketMatchIndexModel buildMarketDetailFromMap(Map<String, Object> raw) {
        MarketMatchResultModel.MarketMatchIndexModel marketDetail = null;
        if (raw == null) {
            return marketDetail;
        }
        marketDetail = new MarketMatchResultModel.MarketMatchIndexModel();

        try {

            marketDetail.setMarketId(String.valueOf(raw.get("MARKET_ID")));
            marketDetail.setMarketName(String.valueOf(raw.get("MARKET_NAME")));
            marketDetail.setSiteName(String.valueOf(raw.get("SITE_CODE")));

            if (raw.get("CATE_ID") == null) {
                marketDetail.setCateId("");
            } else {
                marketDetail.setCateId(String.valueOf(raw.get("CATE_ID")));
            }

            if (raw.get("CATE_NAME") == null) {
                marketDetail.setCateName("");
            } else {
                marketDetail.setCateName(String.valueOf(raw.get("CATE_NAME")));
            }
            marketDetail.setType(String.valueOf(raw.get("TYPE")));
            marketDetail.setX(Double.parseDouble(String.valueOf(raw.get("X"))));
            marketDetail.setY(Double.parseDouble(String.valueOf(raw.get("Y"))));
            marketDetail.setMarketGrowth(Double.parseDouble(String.valueOf(raw.get("Y"))));
            marketDetail.setMarketShare(Double.parseDouble(String.valueOf(raw.get("X"))));
            marketDetail.setMarketGmv30d(Double.parseDouble(String.valueOf(raw.get("MARKET_GMV_30D"))));
            marketDetail.setMarketSales30d(Double.parseDouble(String.valueOf(raw.get("MARKET_SALES_30D"))));
            marketDetail.setRateForSamePic(Double.parseDouble(String.valueOf(raw.get("RATE_FOR_SAME_PIC"))));
            marketDetail.setRateForSameStyle(Double.parseDouble(String.valueOf(raw.get("RATE_FOR_SAME_STYLE"))));
            marketDetail.setRateForSimilarStyle(Double.parseDouble(String.valueOf(raw.get("RATE_FOR_SIMILAR_STYLE"))));
            marketDetail.setCbuOfferNumForSamePic(Long.parseLong(String.valueOf(raw.get("CBU_OFFER_NUM_FOR_SAME_PIC"))));
            marketDetail.setCbuOfferNumForSameStyle(Long.parseLong(String.valueOf(raw.get("CBU_OFFER_NUM_FOR_SAME_STYLE"))));
            marketDetail.setCbuOfferNumForSimilarStyle(Long.parseLong(String.valueOf(raw.get("CBU_OFFER_NUM_FOR_SIMILAR_STYLE"))));
            marketDetail.setDownItemNumForSamePic(Long.parseLong(String.valueOf(raw.get("DOWN_ITEM_NUM_FOR_SAME_PIC"))));
            marketDetail.setDownItemNumForSameStyle(Long.parseLong(String.valueOf(raw.get("DOWN_ITEM_NUM_FOR_SAME_STYLE"))));
            marketDetail.setDownItemNumForSimilarStyle(Long.parseLong(String.valueOf(raw.get("DOWN_ITEM_NUM_FOR_SIMILAR_STYLE"))));
            marketDetail.setIsGlobal(String.valueOf(raw.get("IS_GLOBAL")));
            marketDetail.setProductNum(Double.valueOf(String.valueOf(raw.get(("PRODUCT_NUM")))));
            //        marketDetail.setXMedian(Double.parseDouble(String.valueOf(raw.get("x_median"))));
//        marketDetail.setYMedian(Double.parseDouble(String.valueOf(raw.get("y_median"))));
//        marketDetail.setMarketTags(String.valueOf(raw.get("market_tags")));

        } catch (Exception e) {
            log.error("buildMarketDetailFromMap 出现异常 e:" + e.getStackTrace());
        }

        return marketDetail;
    }


    //生成OneService User信息
    static User getUserInfo() {
        User user = new User();
        user.setAppName(MarketConstants.AppName);
        user.setName(MarketConstants.AppName);
        user.setPassword(MarketConstants.Pass);
        return user;
    }


    /**
     * 通过工号获取其所有权的站点信息
     *
     * @param workId 工号
     * @return
     */
    private List<String> getTheUserRightSites(String workId) {
        ResultOf<List<String>> resultOf = null;
        try {
            resultOf = userDataRightQueryService.queryUserDataRightByUserId(workId);
        } catch (Exception e) {
            log.error("getTheUserRightSites 出现异常 workId:" + workId + " e:" + e.getStackTrace());
        }

        //当出现结果为false Or 得到的data 为 null 或者 size==0 均返回NUll
        if (!resultOf.isSuccess() || resultOf.getData() == null) {
            return null;
        }
        return resultOf.getData();
    }

    /**
     * 获取矩阵中分布于四个象限的数据
     */
    private List<MarketViewResultModel.MarketViewModel> getMatrixDatas(User user, int sqlId, Map<
            String, Object> conditions, String[] returnFileds, List<OrderBy> orderByList) throws QueryException {

        List<MarketViewResultModel.MarketViewModel> matrixMarkets = new ArrayList<>();

        conditions.put("market_tags", "金牛市场");
        List<MarketViewResultModel.MarketViewModel> markets = getMarkets(user, sqlId, conditions, orderByList, returnFileds, 0, 25);
        matrixMarkets.addAll(markets);

        conditions.put("market_tags", "瘦狗市场");
        markets = getMarkets(user, sqlId, conditions, orderByList, returnFileds, 0, 25);
        matrixMarkets.addAll(markets);

        conditions.put("market_tags", "明星市场");
        markets = getMarkets(user, sqlId, conditions, orderByList, returnFileds, 0, 25);
        matrixMarkets.addAll(markets);

        conditions.put("market_tags", "机遇市场");
        markets = getMarkets(user, sqlId, conditions, orderByList, returnFileds, 0, 25);
        matrixMarkets.addAll(markets);

        return matrixMarkets;
    }


    private Integer getMatrixDatasCount(User user, Integer sqlId, Map<String, Object> conditions, String[] returnFileds) throws
            QueryException {

        Integer totalResult = 0;

        conditions.put("market_tags", "金牛市场");
        Result totalCount = sqlidQueryService.get(user, sqlId, conditions, returnFileds);
        if (totalCount == null || !totalCount.isSuccessed()) {
            log.error("sqlidQueryService.get(user, MarketViewQuery_Count_SQL_ID  失败" + sqlId);
        } else {
            Map<String, Object> result = totalCount.getResult();
            totalResult = Integer.valueOf(String.valueOf(result.get("TOTAL")));
        }

        conditions.put("market_tags", "瘦狗市场");
        totalCount = sqlidQueryService.get(user, sqlId, conditions, returnFileds);
        if (totalCount == null || !totalCount.isSuccessed()) {
            log.error("sqlidQueryService.get(user, MarketViewQuery_Count_SQL_ID  失败" + sqlId);
        } else {
            Map<String, Object> result = totalCount.getResult();
            totalResult += Integer.valueOf(String.valueOf(result.get("TOTAL")));
        }

        conditions.put("market_tags", "明星市场");
        totalCount = sqlidQueryService.get(user, sqlId, conditions, returnFileds);
        if (totalCount == null || !totalCount.isSuccessed()) {
            log.error("sqlidQueryService.get(user, MarketViewQuery_Count_SQL_ID  失败" + sqlId);
        } else {
            Map<String, Object> result = totalCount.getResult();
            totalResult += Integer.valueOf(String.valueOf(result.get("TOTAL")));
        }


        conditions.put("market_tags", "机遇市场");
        totalCount = sqlidQueryService.get(user, sqlId, conditions, returnFileds);
        if (totalCount == null || !totalCount.isSuccessed()) {
            log.error("sqlidQueryService.get(user, MarketViewQuery_Count_SQL_ID  失败" + sqlId);
        } else {
            Map<String, Object> result = totalCount.getResult();
            totalResult += Integer.valueOf(String.valueOf(result.get("TOTAL")));
        }

        return totalResult;
    }


    private List<MarketViewResultModel.MarketViewModel> getMarkets(User user, int sqlId, Map<
            String, Object> conditions, List<OrderBy> orderByList, String[] returnFileds, Integer pageStart, Integer pageSize) throws QueryException {

        Results results = sqlidQueryService.list(user, sqlId, conditions, returnFileds, orderByList, pageStart, pageSize);
        if (results == null) {
            log.error("getMatrixDatas query result null");
            return null;
        }
        if (!results.isSuccessed()) {
            log.error("getMatrixDatas results is flase");
            return null;
        }

        List<Map<String, Object>> cateResults = results.getResult();
        List<MarketViewResultModel.MarketViewModel> markets = buildMarketsFromMap(cateResults);
        return markets;
    }


    BigDecimal getYmax(List<MarketViewResultModel.MarketViewModel> markets) {
        BigDecimal max = BigDecimal.valueOf(-100.0);

        if (!CollectionUtils.isEmpty(markets)) {
            for (MarketViewResultModel.MarketViewModel item : markets) {


                if (item.getY().compareTo(max) == 1) {
                    max = item.getY();
                }
            }
        }
        return max;
    }


    /**
     * 全球市场tair查询
     *
     * @param marketTags 市场定性
     * @param orderField 排序字段
     * @param orderType  desc asc
     */
    private MarketViewResultModel readDataFromTairBostonData(String marketTags, String orderField, String orderType) {

        String key = MarketViewQuery_Pre + marketTags + orderField + "_topN_" + orderType;
        Object o = tairAdapter.get(key);
        if (o == null) {
            return null;
        }
        MarketViewResultModel marketViewResultModel = JSONObject.parseObject(String.valueOf(o), MarketViewResultModel.class);
        return marketViewResultModel;

    }


    @RequestMapping("/marketOverview/queryMarketList")
    public RPCResult queryMarketTestList(@RequestParam String statDate, @RequestParam(defaultValue = "0") Integer isGlobal,
                                         @RequestParam(defaultValue = "") String siteCode, @RequestParam(defaultValue = "") String cateId,
                                         @RequestParam(defaultValue = "gmv") String type, @RequestParam(defaultValue = "") String marketName,
                                         @RequestParam(defaultValue = "") String marketTags, @RequestParam(defaultValue = "marketGmv30d") String orderByField,
                                         @RequestParam(defaultValue = "desc") String orderByType, @RequestParam(defaultValue = "1") Integer pageIndex,
                                         @RequestParam(defaultValue = "100") Integer pageNum, @RequestParam(defaultValue = "500") Integer productNum,
                                         @RequestParam(defaultValue = "0") Integer isBoston,
                                         HttpServletRequest request) {

        //每个参数均不能为空
        if (StringUtils.isBlank(statDate)
                || pageIndex == null
                || pageIndex < 0 || pageNum == null || pageNum <= 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "请求必要参数 error");
        }

        MarketViewResultModel marketViewResultModel = new MarketViewResultModel();

//        Query query = CBUOverseasOppoIndexe.builder().market_name("手表").build();

        CBUOverseasOppoIndexe query = new CBUOverseasOppoIndexe();


        query.setIs_global(String.valueOf(isGlobal));

        if (siteCode != null && !"".equals(siteCode)) {
            query.setSite_code(siteCode);
        }
        if (cateId != null && !"".equals(cateId)) {
            query.setCate_id(cateId);
        }
        if (type != null && !"".equals(type)) {
            query.setType(type);
        }

        if (marketName != null && !"".equals(marketName)) {
            query.setMarket_name(marketName);
        }

        if (marketTags != null && !"".equals(marketTags)) {
            query.setMarket_tags(marketTags);
        }
//
//        Sort sort = CBUOverseasOppoAttribute.builder()
//                .rate_for_same_style("DECREASE")
//                .build();


        CBUOverseasOppoAttribute sort = new CBUOverseasOppoAttribute();
        if ("desc".equals(orderByType)) {
            //  降序

            if (orderByField != null && "productNum".equals(orderByField)) {
                sort.setProduct_num("DECREASE");
            } else if (orderByField != null && "marketGmv30d".equals(orderByField)) {
                sort.setMarket_gmv_30d("DECREASE");
            } else if (orderByField != null && "marketSales30d".equals(orderByField)) {
                sort.setMarket_sales_30d("DECREASE");
            } else if (orderByField != null && "marketShareForGmv".equals(orderByField)) {
                sort.setMarket_share_for_gmv("DECREASE");
            } else if (orderByField != null && "marketShareForSales".equals(orderByField)) {
                sort.setMarket_share_for_sales("DECREASE");
            } else if (orderByField != null && "marketGrowthForGmv".equals(orderByField)) {
                sort.setMarket_growth_for_gmv("DECREASE");
            } else if (orderByField != null && "marketGrowthForSales".equals(orderByField)) {
                sort.setMarket_share_for_sales("DECREASE");
            } else if (orderByField != null && "rateForSamePic".equals(orderByField)) {
                sort.setRate_for_same_pic("DECREASE");
            } else if (orderByField != null && "rateForSameStyle".equals(orderByField)) {
                sort.setRate_for_same_style("DECREASE");
            } else if (orderByField != null && "rateForSimilarStyle".equals(orderByField)) {
                sort.setRate_for_similar_style("DECREASE");
            }

        } else {
            //升序

            if (orderByField != null && "productNum".equals(orderByField)) {
                sort.setProduct_num("INCREASE");
            } else if (orderByField != null && "marketGmv30d".equals(orderByField)) {
                sort.setMarket_gmv_30d("INCREASE");
            } else if (orderByField != null && "marketSales30d".equals(orderByField)) {
                sort.setMarket_sales_30d("INCREASE");
            } else if (orderByField != null && "marketShareForGmv".equals(orderByField)) {
                sort.setMarket_share_for_gmv("INCREASE");
            } else if (orderByField != null && "marketShareForSales".equals(orderByField)) {
                sort.setMarket_share_for_sales("INCREASE");
            } else if (orderByField != null && "marketGrowthForGmv".equals(orderByField)) {
                sort.setMarket_growth_for_gmv("INCREASE");
            } else if (orderByField != null && "marketGrowthForSales".equals(orderByField)) {
                sort.setMarket_share_for_sales("INCREASE");
            } else if (orderByField != null && "rateForSamePic".equals(orderByField)) {
                sort.setRate_for_same_pic("INCREASE");
            } else if (orderByField != null && "rateForSameStyle".equals(orderByField)) {
                sort.setRate_for_same_style("INCREASE");
            } else if (orderByField != null && "rateForSimilarStyle".equals(orderByField)) {
                sort.setRate_for_similar_style("INCREASE");
            }
        }
        Filter filter = CBUOverseasOppoAttribute.builder()
                .product_num("product_num>=" + productNum).build();

        OpenSearchQueryParam queryParam = OpenSearchQueryParam.builder()
                .query(query)
                .sort(sort)
                .filter(filter)
                .start((pageIndex - 1))
                .hits(pageNum)
                .fieldClass(CBUOverseasOppoField.class)
                .build();
        try {
//            System.out.println(aeOpenSearchService.query(queryParam));

            PageResult query1 = aeOpenSearchService.query(queryParam);

            marketViewResultModel.setCount(query1.getTotal());

            JSONArray data = (JSONArray) query1.getData();
            List<MarketViewResultModel.MarketViewModel> markets = JSON.parseArray(data.toJSONString(), MarketViewResultModel.MarketViewModel.class);

            for (MarketViewResultModel.MarketViewModel marketViewModel : markets) {
                marketViewModel.setPic(marketViewModel.getImages());
            }

            marketViewResultModel.setData(markets);

            marketViewResultModel.setPageIndex(pageIndex);
            marketViewResultModel.setPageNum(pageNum);
            marketViewResultModel.setSuccess(true);

            if (markets != null && markets.size() > 0) {
                marketViewResultModel.setXMedian(markets.get(0).getXMedian());
                marketViewResultModel.setYMedian(markets.get(0).getYMedian());
                marketViewResultModel.setYMax(getYmax(markets));
            }


            return RPCResult.ok(marketViewResultModel);

        } catch (Exception e) {
            log.error("e" + e.getMessage());
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "查询 OpenSearch 出现异常");
        }

    }


}
