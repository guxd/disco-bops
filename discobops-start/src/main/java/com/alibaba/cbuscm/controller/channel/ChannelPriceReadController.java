package com.alibaba.cbuscm.controller.channel;


import com.alibaba.cbu.panama.dc.client.model.PageResult;
import com.alibaba.cbu.panama.dc.client.model.param.channelprice.ChannelPriceLogParam;
import com.alibaba.cbu.panama.dc.client.model.param.channelprice.ChannelPricePageParam;
import com.alibaba.cbuscm.RestResult;
import com.alibaba.cbuscm.service.channel.ChannelPriceReadAdapter;
import com.alibaba.cbuscm.utils.validate.ValidateUtil;
import com.alibaba.cbuscm.vo.channel.resp.ChannelPriceListRespVO;
import com.alibaba.cbuscm.vo.marketing.product.OperateLogRespVO;
import com.alibaba.cbuscm.vo.marketing.product.PageResultResp;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import java.util.Objects;


/**
 * @Auther: guxd
 * @Date: 2019/10/15 20:49
 * @Description:渠道商品价格管理
 */
@RestController
@RequestMapping(path = "/channel/read", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ChannelPriceReadController {

    @Autowired
    private ChannelPriceReadAdapter channelPriceReadAdapter;

    @GetMapping("/priceList")
    public RestResult<PageResultResp<ChannelPriceListRespVO>> pageChannelPriceByParam(
            @RequestParam(required = false) String productId,
            @RequestParam(required = false) String productSkuId,
            @RequestParam(required = false) String cnSkuId,
            @RequestParam(required = true) Integer pageNo,
            @RequestParam(required = true) Integer pageSize) {
        try {
            ChannelPricePageParam param = buildChannelPricePageParam(productId, productSkuId, cnSkuId, pageNo, pageSize);
            PageResultResp<ChannelPriceListRespVO> respVO = channelPriceReadAdapter.pageChannelPriceByParam(param);

            if (respVO != null) {
                respVO.setPageNo(pageNo);
                respVO.setPageSize(pageSize);
            }
            return RestResult.success(respVO);
        } catch (Exception e) {
            return RestResult.fail(e.getMessage(), e.getLocalizedMessage());
        }

    }

    private ChannelPricePageParam buildChannelPricePageParam(String productId, String productSkuId, String cnSkuId, Integer pageNo, Integer pageSize) {
        ChannelPricePageParam param = new ChannelPricePageParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        if (StringUtils.isNotBlank(productId)) {
            param.setProductId(Long.parseLong(productId));
        }
        if (StringUtils.isNotBlank(productSkuId)) {
            param.setProductSkuId(Long.parseLong(productSkuId));
        }
        if (StringUtils.isNotBlank(cnSkuId)) {
            param.setCnSkuId(Long.parseLong(cnSkuId));
        }
        return param;
    }


    @GetMapping("/logList")
    public RestResult<PageResultResp<OperateLogRespVO>> queryProductLogList(
            @RequestParam(required = true) String productId,
            @RequestParam(required = true) String relId,
            @RequestParam(required = true) Integer pageNo,
            @RequestParam(required = true) Integer pageSize) {

        try {
            ChannelPriceLogParam param = new ChannelPriceLogParam();
            buildChannelPriceLogParam(productId, relId, pageNo, pageSize, param);
            PageResultResp<OperateLogRespVO> respVO = channelPriceReadAdapter.queryProductLogList(param);

            if (respVO !=null){
                respVO.setPageNo(pageNo);
                respVO.setPageSize(pageSize);
            }
            return RestResult.success(respVO);
        } catch (Exception e) {
            return RestResult.fail(e.getMessage(),e.getLocalizedMessage());
        }

    }

    private void buildChannelPriceLogParam(String productId, String relId, Integer pageNo, Integer pageSize, ChannelPriceLogParam param) {
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        if (StringUtils.isNotBlank(productId)) {
            param.setProductId(Long.parseLong(productId));
        }
        if (StringUtils.isNotBlank(relId)) {
            param.setRelId(Long.parseLong(relId));
        }
    }

    public static void main(String[] args) {
        ChannelPriceReadController channelPriceReadController = new ChannelPriceReadController();
        channelPriceReadController.queryProductLogList("1","1",0,10);
    }

}
