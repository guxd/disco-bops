/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.controller.domestic.tb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.domestic.tb.SelectionTaskCreateVO;
import com.alibaba.cbuscm.vo.domestic.tb.SelectionTaskGroupViewVO;
import com.alibaba.cbuscm.vo.domestic.tb.SelectionTaskViewVO;
import com.alibaba.cbuscm.vo.UserVO;
import com.alibaba.china.global.business.library.common.PageOf;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.enums.SelectionTaskBizCodeEnum;
import com.alibaba.china.global.business.library.enums.StdCategoryChannelEnum;
import com.alibaba.china.global.business.library.interfaces.category.CategoryReadService;
import com.alibaba.china.global.business.library.interfaces.selection.taoshop.TaoShopSelectionReadService;
import com.alibaba.china.global.business.library.interfaces.selection.taoshop.TaoShopSelectionWriteService;
import com.alibaba.china.global.business.library.models.selection.SelectionAdminModel;
import com.alibaba.china.global.business.library.models.selection.taoshop.TaoShopSelectionAggregationModel;
import com.alibaba.china.global.business.library.models.selection.taoshop.TaoShopSelectionTaskModel;
import com.alibaba.china.global.business.library.params.selection.taoshop.TaoShopSelectionAggregationQueryParam;
import com.alibaba.china.global.business.library.params.selection.taoshop.TaoShopSelectionAggregationUpdateParam;
import com.alibaba.china.global.business.library.params.selection.taoshop.TaoShopSelectionTaskPostParam;
import com.alibaba.china.global.business.match.interfaces.C2BDataPicSearchService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.task.Paginator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * 选品任务RPC服务
 * 
 * @author luxuetao 2019年4月18日 下午5:24:43
 */
@RestController
@RequestMapping("/taoSelectionTask")
public class SelectionTaskController {
    protected static final Logger LOG = LoggerFactory.getLogger(SelectionTaskController.class);
    @Autowired
    private TaoShopSelectionWriteService taoShopSelectionWriteService;
    @Autowired
    private TaoShopSelectionReadService taoShopSelectionReadService;
    @Autowired
    private CategoryReadService categoryReadService;
    @Autowired
    private C2BDataPicSearchService c2BDataPicSearchService;

    /**
     * 创建选品任务
     * 
     * @param request
     * @param data
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/createTask")
    public RPCResult createTask(HttpServletRequest request, @RequestParam(name = "data") String data)
        throws IOException, ServletException {
        if (StringUtils.isBlank(data)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }
        SelectionTaskCreateVO vo = null;
        try {
            vo = JSON.parseObject(data, SelectionTaskCreateVO.class);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[createTask.parseJsonObject] fail, data:%s, exception:%s",
                JSONObject.toJSONString(data),
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
        TaoShopSelectionTaskPostParam param = buildTaskPostParam(vo, user);
        ResultOf<TaoShopSelectionTaskModel> result = null;
        try {
            result = taoShopSelectionWriteService.createTask(param);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[createTask.createTask] fail, param:%s, exception:%s",
                JSONObject.toJSONString(param),
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, result.getErrorMessage());
        }

        return RPCResult.ok(result.getData());
    }

    /**
     * 编辑选品任务
     * 
     * @param request
     * @param data
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/updateTask")
    public RPCResult updateTask(HttpServletRequest request, @RequestParam(name = "data") String data)
        throws IOException, ServletException {
        if (StringUtils.isBlank(data)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }
        SelectionTaskCreateVO vo = null;
        try {
            vo = JSON.parseObject(data, SelectionTaskCreateVO.class);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[updateTask.parseJsonObject] fail, data:%s, exception:%s",
                JSONObject.toJSONString(data),
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
        TaoShopSelectionTaskPostParam param = buildTaskPostParam(vo, user);
        ResultOf<TaoShopSelectionTaskModel> result = null;
        try {
            result = taoShopSelectionWriteService.updateTask(param);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[updateTask.updateTask] fail, param:%s, exception:%s",
                JSONObject.toJSONString(param),
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, result.getErrorMessage());
        }

        return RPCResult.ok(result.getData());
    }

    /**
     * 构建选品任务创建参数模型
     * 
     * @param vo
     * @return
     */
    private TaoShopSelectionTaskPostParam buildTaskPostParam(SelectionTaskCreateVO vo, BucSSOUser bucSSOUser) {
        TaoShopSelectionTaskPostParam param = new TaoShopSelectionTaskPostParam();
        param.setTaskId(vo.getTaskId());
        param.setIsComprehensiveActivity(vo.getHasComposite());
        param.setTaskType(SelectionTaskBizCodeEnum.TAO_SHOP_SELECTION.getBizCode());
        if (null != vo.getCategoryId()) {
            String ids = categoryReadService
                .getCategoryIdStrPath(StdCategoryChannelEnum.CHANNEL_1688, vo.getCategoryId(), ",");
            List<String> idList = Lists.newArrayList(ids.split(","));
            long lastCategoryId = 0;
            if (CollectionUtils.isNotEmpty(idList)) {
                if (null != idList.get(0) && StringUtils.isNumeric(idList.get(0))) {
                    param.setCateLv1Id(Long.valueOf(idList.get(0)));
                    lastCategoryId = Long.valueOf(idList.get(0));
                }
                if (idList.size() > 1 && null != idList.get(1) && StringUtils.isNumeric(idList.get(1))) {
                    param.setCateLv2Id(Long.valueOf(idList.get(1)));
                    lastCategoryId = Long.valueOf(idList.get(1));
                }
                if (idList.size() > 2 && null != idList.get(2) && StringUtils.isNumeric(idList.get(2))) {
                    param.setCateLv3Id(Long.valueOf(idList.get(2)));
                    lastCategoryId = Long.valueOf(idList.get(2));
                }
                if (idList.size() > 3 && null != idList.get(3) && StringUtils.isNumeric(idList.get(3))) {
                    param.setCateLv4Id(Long.valueOf(idList.get(3)));
                }
            }
            String names = categoryReadService
                .getCategoryNamePath(StdCategoryChannelEnum.CHANNEL_1688, vo.getCategoryId(), ",");
            List<String> nameList = Lists.newArrayList(names.split(","));
            if (CollectionUtils.isNotEmpty(nameList)) {
                if (null != nameList.get(0)) {
                    param.setCateLv1Name(nameList.get(0));
                }
                if (nameList.size() > 1 && null != nameList.get(1)) {
                    param.setCateLv2Name(nameList.get(1));
                }
                if (nameList.size() > 2 && null != nameList.get(2)) {
                    param.setCateLv3Name(nameList.get(2));
                }
                if (nameList.size() > 3 && null != nameList.get(3)) {
                    param.setCateLv4Name(nameList.get(3));
                }
            }
            if(vo.getVirtualCategoryId()!=null && vo.getVirtualCategoryId()!=0){
                param.setCateLv4Id(vo.getVirtualCategoryId());
            }
            if(vo.getVirtualCategoryName()!=null && !StringUtils.isEmpty(vo.getVirtualCategoryName())){
                param.setCateLv4Name(vo.getVirtualCategoryName());
            }
        }
        param.setTaskName(vo.getName());
        param.setTaskDesc(vo.getDescription());
        // 默认将创建者or编辑者设置为管理员
        List<UserVO> userVOs = vo.getAdmins();
        UserVO uservo = new UserVO();
        if (null != bucSSOUser) {
            uservo.setEmplId(bucSSOUser.getEmpId());
            uservo.setName(bucSSOUser.getLastName());
        }
        if (CollectionUtils.isEmpty(userVOs)) {
            userVOs = Lists.newArrayList();
            userVOs.add(uservo);
        } else {
            List<String> emplIds = vo.getAdmins().stream().map(UserVO::getEmplId).collect(Collectors.toList());
            if (null != bucSSOUser && !emplIds.contains(bucSSOUser.getEmpId())) {
                userVOs.add(uservo);
            }
        }
        if (CollectionUtils.isNotEmpty(userVOs)) {
            List<SelectionAdminModel> admins = new ArrayList<SelectionAdminModel>();
            for (UserVO user : userVOs) {
                SelectionAdminModel admin = new SelectionAdminModel();
                admin.setAdminId(user.getEmplId());
                admin.setAdminName(user.getName());
                admins.add(admin);
            }
            if (CollectionUtils.isNotEmpty(admins)) {
                param.setAdmins(admins);
            }
        }
        param.setBizPrimary(vo.getPrimaryBiz());
        param.setBizSecondary(vo.getSecondaryBiz());
        return param;
    }

    /**
     * 根据选品任务Id删除选品任务
     * 
     * @param request
     * @param taskId 任务id
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/deleteTask")
    public RPCResult deleteTask(HttpServletRequest request,
        @RequestParam(name = "taskId") Long taskId)
        throws IOException, ServletException {

        if (taskId == null || taskId < 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }

        ResultOf<Boolean> success = null;
        try {
            success = taoShopSelectionWriteService.deleteTask(Long.valueOf(taskId));
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[deleteTask.deleteTask] fail, taskId:%d, exception:%s",
                taskId,
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, success.getErrorMessage());
        }
        if (!success.isSuccess()) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, success.getErrorMessage());
        }

        return RPCResult.ok();

    }

    /**
     * 根据类目删除类目下的选品任务组
     * 
     * @param request
     * @param id
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/deleteCateTask")
    public RPCResult deleteCateTask(HttpServletRequest request,
        @RequestParam(name = "id") Long id)
        throws IOException, ServletException {

        if (id == null || id < 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }

        ResultOf<Boolean> success = null;
        try {
            success = taoShopSelectionWriteService.deleteAggregation(id);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[deleteCateTask.deleteAggregation] fail, id:%d, exception:%s",
                id,
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, success.getErrorMessage());
        }
        if (!success.isSuccess()) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, success.getErrorMessage());
        }

        return RPCResult.ok();

    }

    @RequestMapping("/uploadImage")
    public RPCResult uploadImage(HttpServletRequest request,
        @RequestParam(name = "file") MultipartFile file){
        if (file.isEmpty()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }
        com.alibaba.china.global.business.common.ResultOf<String> result = null;
        try {
            byte[] bytes = file.getBytes();
            result = c2BDataPicSearchService.ossUpload(bytes);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[uploadImage.ossUpload] fail, file:%s, exception:%s",
                file.toString(),
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, result.getErrorMessage());
        }
        if (!result.isSuccess()) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, result.getErrorMessage());
        }
        return RPCResult.ok(result.getData());
    }

    @RequestMapping("/modifyTaskImage")
    public RPCResult modifyTaskImage(@RequestParam(name = "url") String url,
        @RequestParam(name = "id") Long id)
        throws IOException, ServletException {
        if (StringUtils.isEmpty(url) || id == null || id < 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }
        ResultOf<Boolean> success = null;
        try {
            TaoShopSelectionAggregationUpdateParam param = new TaoShopSelectionAggregationUpdateParam();
            param.setId(id);
            param.setCateImage(url);
            success = taoShopSelectionWriteService.updateAggregation(param);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[modifyTaskImage.updateAggregation] fail, id:%d, url:%s, exception:%s",
                id, url,
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, success.getErrorMessage());
        }
        if (!success.isSuccess()) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, success.getErrorMessage());
        }
        return RPCResult.ok();
    }

    @RequestMapping("/getTaskList")
    public RPCResult getTaskList(HttpServletRequest request,
        @RequestParam(name = "type") String type, @RequestParam(name = "categoryId", required = false) Long categoryId,
        @RequestParam(name = "page", required = false) Integer page,
        @RequestParam(name = "itemsPerPage", required = false) Integer itemsPerPage)
        throws IOException, ServletException {
        if (null == page || page <= 0) {
            page = 1;
        }
        if (null == itemsPerPage || itemsPerPage <= 0) {
            itemsPerPage = 10;
        }

        TaoShopSelectionAggregationQueryParam queryParam = new TaoShopSelectionAggregationQueryParam();
        queryParam.setPageNo(page);
        queryParam.setPageSize(itemsPerPage);
        queryParam.setCateId(categoryId);
        if (StringUtils.equals(type, "my")) {
            String adminId = LoginUtil.getEmpId(request);
            queryParam.setAdminId(adminId);
        }
        PageOf<TaoShopSelectionAggregationModel> pageOf = null;
        try {
            pageOf = taoShopSelectionReadService.query(queryParam);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[getTaskList.query] fail, queryParam:%s, exception:%s",
                JSONObject.toJSONString(queryParam),
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, pageOf.getErrorMessage());
        }
        if (!pageOf.isSuccess()) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, pageOf.getErrorMessage());
        }
        List<SelectionTaskGroupViewVO> groupVOList = Lists.newArrayList();
        List<TaoShopSelectionAggregationModel> modelList = pageOf.getData();
        buildTaskGroupVOList(groupVOList, modelList);
        Paginator paginator = new Paginator(pageOf.getTotal(), pageOf.getPageSize());
        paginator.setPage(pageOf.getPageNo());
        Map<String, Object> result = Maps.newHashMap();
        result.put("data", groupVOList);
        result.put("paginator", paginator);
        return RPCResult.okNoWrapper(result);
    }

    @RequestMapping("/getTask")
    public RPCResult getTask(HttpServletRequest request,
        @RequestParam(name = "taskId") Long taskId)
        throws IOException, ServletException {
        if (taskId == null || taskId < 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }

        ResultOf<TaoShopSelectionTaskModel> result = null;
        try {
            result = taoShopSelectionReadService.findTaskById(taskId);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[getTask.findTaskById] fail, taskId:%d, exception:%s",
                taskId,
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, result.getErrorMessage());
        }
        if (!result.isSuccess()) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, result.getErrorMessage());
        }
        SelectionTaskViewVO vo = new SelectionTaskViewVO();
        TaoShopSelectionTaskModel model = result.getData();
        buildTaskVO(vo, model);
        return RPCResult.ok(vo);

    }

    /**
     * @param groupVOList
     * @param modelList
     */
    private void buildTaskGroupVOList(List<SelectionTaskGroupViewVO> groupVOList,
        List<TaoShopSelectionAggregationModel> modelList) {
        for (TaoShopSelectionAggregationModel model : modelList) {
            SelectionTaskGroupViewVO groupVO = new SelectionTaskGroupViewVO();
            // 聚合id=1代表综合任务
            if (null != model.getId() && model.getId() == 1L) {
                groupVO.setHasComposite(true);
            } else {
                groupVO.setHasComposite(false);
            }
            groupVO.setImgUrl(model.getCateImage());
            groupVO.setId(model.getId());
            groupVO.setName(model.getCateName());
//            groupVO.setCategoryId(model.getCateId());
            if(model.getCateLv4Id()!=null && model.getCateLv4Id()!=0){
                groupVO.setVirtualCategoryId(model.getCateLv4Id());
                if(model.getCateLv3Id()!=null && model.getCateLv3Id()!=0){
                    groupVO.setCategoryId(model.getCateLv3Id());
                }else if(model.getCateLv2Id()!=null && model.getCateLv2Id()!=0){
                    groupVO.setCategoryId(model.getCateLv2Id());
                }
            }else{
                groupVO.setCategoryId(model.getCateId());
            }
            if (CollectionUtils.isNotEmpty(model.getTasks())) {
                List<SelectionTaskViewVO> taskVOList = Lists.newArrayList();
                for (TaoShopSelectionTaskModel taskModel : model.getTasks()) {
                    SelectionTaskViewVO vo = new SelectionTaskViewVO();
                    this.buildTaskVO(vo, taskModel);
                    taskVOList.add(vo);
                }
                groupVO.setTasks(taskVOList);
            }
            groupVOList.add(groupVO);
        }
    }

    /**
     * @param vo
     * @param model
     */
    private void buildTaskVO(SelectionTaskViewVO vo, TaoShopSelectionTaskModel model) {
        if (model == null) {
            return;
        }
        if (null != model.getGmtCreate()) {
            vo.setCreateTime(model.getGmtCreate().getTime());
        }
        if (null != model.getGmtModified()) {
            vo.setUpdateTime(model.getGmtModified().getTime());
        }
        vo.setDescription(model.getTaskDesc());
        // 聚合id=1代表综合任务
        if (null != model.getAggregationId() && model.getAggregationId() == 1L) {
            vo.setHasComposite(true);
        } else {
            vo.setHasComposite(false);
        }
        vo.setAggregationId(model.getAggregationId());
        vo.setPrimaryBiz(model.getBizPrimary());
        vo.setSecondaryBiz(model.getBizSecondary());
        vo.setTaskId(model.getId());
        if (CollectionUtils.isNotEmpty(model.getAdmins())) {
            List<String> members = Lists.newArrayList();
            List<UserVO> users = Lists.newArrayList();
            for (SelectionAdminModel admin : model.getAdmins()) {
                members.add(admin.getAdminName());
                UserVO userVo = new UserVO();
                userVo.setEmplId(admin.getAdminId());
                userVo.setName(admin.getAdminName());
                users.add(userVo);
            }
            if (CollectionUtils.isNotEmpty(members)) {
                vo.setMembers(StringUtils.join(members, ","));
            }
            if (CollectionUtils.isNotEmpty(users)) {
                vo.setAdmins(users);
            }
        }
        vo.setName(model.getTaskName());
        vo.setTaskId(model.getId());

        if (null != model.getAggregation() && null != model.getAggregation().getCateLv4Id() && 0 != model.getAggregation().getCateLv4Id()) {
            vo.setVirtualCategoryId(model.getAggregation().getCateLv4Id());
            vo.setVirtualCategoryName(model.getAggregation().getCateLv4Name());
            if(null != model.getAggregation().getCateLv3Id() && 0!= model.getAggregation().getCateLv3Id()){
                vo.setCategoryId(model.getAggregation().getCateLv3Id());
            }else if (null != model.getAggregation().getCateLv2Id() && 0!= model.getAggregation().getCateLv2Id()){
                vo.setCategoryId(model.getAggregation().getCateLv2Id());
            }else if(null != model.getAggregation().getCateLv1Id() && 0!= model.getAggregation().getCateLv1Id()){
                vo.setCategoryId(model.getAggregation().getCateLv1Id());
            }
        }else if (null != model.getAggregation()){
            vo.setCategoryId(model.getAggregation().getCateId());
        }
    }

}
