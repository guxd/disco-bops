package com.alibaba.cbuscm.controller.domestic.tb.planning.request;

import com.alibaba.china.shared.discosupplier.model.common.UserModel;
import lombok.Data;
import lombok.ToString;

import java.util.Set;

/**
 * @author wb-qiuth
 * 新增企划接收参数
 */
@Data
@ToString
public class AddDesignRequest {

    /**
     *  企划类型
     */
    private String type;

    /**
     * 企划标题
     */
    private String title;

    /**
     * 企划描述
     */
    private String desc;

    /**
     * 企划渠道
     */
    private String scene;

    /**
     * 企划管理员
     */
    private Set<UserModel> admins;

    /**
     * 企划主键id
     */
    private Long id;
}
