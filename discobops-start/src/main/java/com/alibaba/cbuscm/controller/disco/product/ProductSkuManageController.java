package com.alibaba.cbuscm.controller.disco.product;

import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductManageService;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductReadService;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductModel;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.product.param.DcProductSkuModifyParam;
import com.alibaba.cbu.disco.shared.core.product.param.DcProductSupplyRelationModifyParam;
import com.alibaba.cbuscm.params.ProductSkuBatchModifyParam;
import com.alibaba.cbuscm.params.ProductSkuModifyParam;
import com.alibaba.cbuscm.service.authority.ProductDataAccessService;
import com.alibaba.cbuscm.service.authority.model.DcBizTypeInfo;
import com.alibaba.cbuscm.service.product.ProductConverter;
import com.alibaba.cbuscm.utils.BeanUtilsExt;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.up.common.mybatis.result.RPCResult;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

import static com.alibaba.cbuscm.utils.CollectionUtils.map;

/**
 * 产品SKU管理的 Controller
 * @author zhuoxian
 */
@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/product-sku-manage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class ProductSkuManageController {

    @Autowired
    private DcProductReadService dcProductReadService;

    @Autowired
    private DcProductManageService dcProductManageService;

    @Autowired
    private ProductDataAccessService productDataAccessService;

    /**
     * @author jipengxu
     * @param bizType
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(path = "/detail", method = RequestMethod.GET)
    public Object getSkuDetail(@RequestParam(value = "productSkuId") String productSkuIdStr,
                               @RequestParam(value = "bizType", defaultValue = "CROSSBORDER") String bizType,
                               HttpServletRequest request) {
        try {
            Integer userId = LoginUtil.getUserId(request);
          /*  ListOf<DcBizTypeInfo> bizTypeList = productDataAccessService.queryUserAuthorizedBizType(userId);
            if (!ListOf.isNonEmpty(bizTypeList) || bizTypeList.getData().stream()
                    .noneMatch(b -> bizType.equals(b.getBizType()))) {
                return PageOf.error("system-error", "no authorized BizType:" + bizType);
            }*/
            Long productSkuId = Long.parseLong(productSkuIdStr);
            ResultOf<DcProductModel> result = dcProductReadService.getProductSkuWithDetail(productSkuId);
            if (result == null || result.getData() == null) {
                return RPCResult.error(result == null ? "system_error" : result.getErrorCode(),
                        result == null ? "未查询到数据" : result.getErrorMessage());
            }
            return RPCResult.ok(ProductConverter.convertProductSku(result.getData()));
        } catch (Exception e) {
            log.error("ProductSkuManageController.getSkuInfo error,productSkuId={},errMsg={}", productSkuIdStr, e.getMessage(), e);
            return RPCResult.error("system-error", e.toString());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/modify", method = RequestMethod.POST)
    public Object modifySku(@RequestBody ProductSkuModifyParam body, HttpServletRequest request) {
        try {
            DcProductSkuModifyParam param = new DcProductSkuModifyParam();
            BeanUtilsExt.copyProperties(body, param);
            param.setProductSkuId(Long.parseLong(body.getProductSkuId()));
            param.setOperator(new OperatorModel(LoginUtil.getEmpId(request), LoginUtil.getLoginName(request)));
            return dcProductManageService.modifyProductSku(param);
        } catch (Throwable err) {
            log.error("ProductSkuManageController.modifySku error!", err);
            return ResultOf.error("system-error", err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/batch-modify", method = RequestMethod.POST)
    public Object batchModifySku(@RequestBody ProductSkuBatchModifyParam body, HttpServletRequest request) {
        try {
            String empId = LoginUtil.getEmpId(request);
            String loginId = LoginUtil.getLoginName(request);
            List<DcProductSkuModifyParam> params = map(body.getParams(), p -> {
                DcProductSkuModifyParam r = new DcProductSkuModifyParam();
                BeanUtilsExt.copyProperties(p, r);
                r.setProductSkuId(Long.parseLong(p.getProductSkuId()));
                r.setOperator(new OperatorModel(empId, loginId));
                return r;
            });
            return dcProductManageService.modifyProductSkus(params);
        } catch (Throwable err) {
            log.error("ProductSkuManageController.batchModifySku error!", err);
            return ResultOf.error("system-error", err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/supply-relations/modify", method = RequestMethod.POST)
    public Object modifySkuSupplyRelations(
        @RequestBody SupplyRelationModifyParam body, HttpServletRequest request) {
        try {
            DcProductSupplyRelationModifyParam param = DcProductSupplyRelationModifyParam.builder()
                .productSkuId(Long.parseLong(body.getProductSkuId()))
                .updateParams(body.getUpdateParams())
                .addParams(body.getAddParams())
                .createParams(body.getCreateParams())
                .deleteParams(body.getDeleteParams())
                .operator(new OperatorModel(LoginUtil.getEmpId(request), LoginUtil.getLoginName(request)))
                .build();
            if (StringUtils.isNotBlank(body.getMainRelationId())) {
                param.setMainRelationId(Long.parseLong(body.getMainRelationId()));
            }
            return dcProductManageService.modifyProductSkuSupplyRelation(param);
        } catch (Throwable err) {
            log.error("ProductSkuManageController.modifySkuSupplyRelations error!", err);
            return ResultOf.error("system-error", err.getMessage());
        }
    }

    @Getter
    @Setter
    private static class SupplyRelationModifyParam {
        private OperatorModel operator;
        private String productSkuId;
        private String mainRelationId;
        private List<DcProductSupplyRelationModifyParam.AddParam> addParams;
        private List<DcProductSupplyRelationModifyParam.CreateParam> createParams;
        private List<DcProductSupplyRelationModifyParam.UpdateParam> updateParams;
        private List<Long> deleteParams;
    }

}
