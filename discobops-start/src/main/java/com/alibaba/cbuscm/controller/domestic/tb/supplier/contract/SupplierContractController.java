package com.alibaba.cbuscm.controller.domestic.tb.supplier.contract;

import com.alibaba.cbuscm.vo.contract.SupplierContractTemplateVO;
import com.alibaba.china.shared.discosupplier.enums.SupplierObjectiveEnum;
import com.alibaba.china.shared.discosupplier.enums.supplier.BizType;
import com.alibaba.china.shared.discosupplier.model.contract.SupplierContractModel;
import com.alibaba.china.shared.discosupplier.model.contract.SupplierContractTemplateModel;
import com.alibaba.china.shared.discosupplier.param.contract.SupplierContractTemplateParam;
import com.alibaba.china.shared.discosupplier.service.contract.SupplierContractTemplateService;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.result.Result;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description:
 * @author: lanqing.hlq
 * @date: 2019-08-10 23:21
 **/
@RestController
@RequestMapping("/contract")
public class SupplierContractController {

    @Autowired
    private SupplierContractTemplateService templateService;

    @RequestMapping(path = "/queryTemplate", method = RequestMethod.GET)
    public RPCResult queryContractTemplate(@RequestParam(name = "bizType", required = false) String bizType) {
        SupplierContractTemplateParam param = new SupplierContractTemplateParam();
        if(BizType.CAIGOU.name().equalsIgnoreCase(bizType)) {
            param.setObjectiveIn(SupplierObjectiveEnum.listObjective(bizType));
        }else{
            param.setObjectiveNotIn(SupplierObjectiveEnum.listObjective(BizType.CAIGOU.name()));
        }
        Result<List<SupplierContractTemplateModel>> modelList = templateService.queryContractTemplate(param);
        List<SupplierContractTemplateVO> voList = modelToVo(modelList.getModel());
        return RPCResult.ok(voList);
    }

    private SupplierContractTemplateVO modelToVo(SupplierContractTemplateModel model) {
        SupplierContractTemplateVO vo = new SupplierContractTemplateVO();
        BeanUtils.copyProperties(model, vo);
        vo.setId(model.getId());
        if (model.getSign().isProperty()) {
            vo.setProperty("业务");
        } else {
            vo.setProperty("普通");
        }
        return vo;
    }

    private List<SupplierContractTemplateVO> modelToVo(List<SupplierContractTemplateModel> modelList) {

        List<SupplierContractTemplateVO> voList = Lists.newArrayList();
        for (SupplierContractTemplateModel model : modelList) {
            voList.add(modelToVo(model));
        }
        return voList;
    }
}
