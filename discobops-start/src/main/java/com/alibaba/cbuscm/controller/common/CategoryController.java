package com.alibaba.cbuscm.controller.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbuscm.vo.common.CategoryPropertyVO;
import com.alibaba.cbuscm.vo.common.CategoryPropertyValueVO;
import com.alibaba.cbuscm.vo.common.CategoryVO;
import com.alibaba.china.global.business.goods.clean.model.CategoryModel;
import com.alibaba.china.global.business.library.enums.StdCategoryChannelEnum;
import com.alibaba.china.global.business.library.interfaces.category.CategoryReadService;
import com.alibaba.china.global.business.library.models.category.StdCategoryModel;
import com.alibaba.china.global.business.library.models.category.StdCategoryPropertyModel;
import com.alibaba.china.global.business.library.models.category.StdCategoryPropertyValueModel;
import com.alibaba.up.common.mybatis.result.RPCResult;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.testng.collections.Maps;

/**
 * 类目服务rpc
 */
@RestController
@Slf4j
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryReadService categoryReadService;
    @Autowired
    private com.alibaba.china.global.business.goods.clean.service.CategoryReadService downStreamCategoryReadService;
    private static final String CBU_SITE_CODE = "1688";

    /**
     * 搜索类目（1688 + 下游站点）
     *
     * @param siteCode         站点编码
     * @param cateId 父类目id（第一级类目统一传0）
     * @return 子节点集合
     */
    @RequestMapping("/find")
    public RPCResult findChildCategory(@RequestParam String siteCode, @RequestParam String cateId) {
        try {
            List<CategoryModel> result = Lists.newArrayList();
            if (CBU_SITE_CODE.equals(siteCode)) {
                List<StdCategoryModel> categoryModels;
                Long cbuCategoryId = Long.parseLong(cateId);
                if (cbuCategoryId == 0L) {
                    categoryModels = categoryReadService.getChannelFirstStdCategory(
                        StdCategoryChannelEnum.CHANNEL_1688);
                } else {
                    categoryModels = categoryReadService.getChannelChildStdCategory(StdCategoryChannelEnum.CHANNEL_1688, cbuCategoryId);
                }
                if (!CollectionUtils.isEmpty(categoryModels)) {
                    result = categoryModels.stream().map(stdCategoryModel ->
                            CategoryModel.builder()
                                .cateId(String.valueOf(stdCategoryModel.getCategoryId()))
                                .cateName(stdCategoryModel.getName())
                                .isLeaf(stdCategoryModel.isLeaf() ? 1 : 0)
                                .platform(CBU_SITE_CODE)
                                .build()
                        ).collect(Collectors.toList());
                }
            } else {
                result = downStreamCategoryReadService.findChildren(cateId, siteCode);
            }
            return RPCResult.ok(result);
        } catch (Exception e) {
            log.error("findCategory error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "findCategory error");
    }

    /**
     * 获取淘宝一级类目
     *
     * @return
     */
    @RequestMapping("/taoFirstCategory")
    public RPCResult getTBFirstCatetory() {
        List<StdCategoryModel> categoryModels = categoryReadService.getChannelFirstStdCategory(
            StdCategoryChannelEnum.CHANNEL_TB);
        List<CategoryVO> result = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(categoryModels)) {
            result = categoryModels.stream().map(category -> convertCategoryToVO(category)).collect(
                Collectors.toList());
        }
        return RPCResult.ok(result);
    }

    /**
     * 展示淘宝类目
     *
     * @param request
     * @return
     */
    @RequestMapping(path = "/showcategory", method = RequestMethod.GET)
    public RPCResult showCategory(HttpServletRequest request,
                                  @RequestParam(value = "categoryId", required = true, defaultValue = "0")
                                      long categoryId) {
        if (categoryId == 0) {
            List<StdCategoryModel> list = categoryReadService.getChannelFirstStdCategory(
                StdCategoryChannelEnum.CHANNEL_TB);
            return RPCResult.ok(list);
        } else {
            List<StdCategoryModel> list = categoryReadService.getChannelChildStdCategory(
                StdCategoryChannelEnum.CHANNEL_TB, categoryId);
            return RPCResult.ok(list);
        }
    }

    /**
     * 关键词搜索淘宝类目
     *
     * @param request
     * @return
     */
    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public RPCResult search(HttpServletRequest request,
                            @RequestParam(value = "keywords", required = true) String keywords) {

        List<StdCategoryModel> list = categoryReadService.searchChannelAllStdCategory(
            StdCategoryChannelEnum.CHANNEL_TB, keywords);
        return RPCResult.ok(list);
    }
    /**
     * 获取Tb所有类目
     *
     * @return
     */
    @RequestMapping("/allTbCategory")
    public RPCResult allTbCategory() {
        List<StdCategoryModel> categoryModels = categoryReadService.getChannelAllStdCategory(
            StdCategoryChannelEnum.CHANNEL_TB);
        List<CategoryVO> result = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(categoryModels)) {
            result = categoryModels.stream().map(category -> convertCategoryToVO(category)).collect(
                Collectors.toList());
        }
        return RPCResult.ok(result);
    }

    /**
     * 获取1688所有类目
     *
     * @return
     */
    @RequestMapping("/all1688Category")
    public RPCResult getAll1688Category() {
        List<StdCategoryModel> categoryModels = categoryReadService.getChannelAllStdCategory(
            StdCategoryChannelEnum.CHANNEL_1688);
        List<CategoryVO> result = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(categoryModels)) {
            result = categoryModels.stream().map(category -> convertCategoryToVO(category)).collect(
                Collectors.toList());
        }
        return RPCResult.ok(result);
    }

    /**
     * 搜索1688类目
     *
     * @param searchStr
     * @return
     */
    @RequestMapping("/search1688Category")
    public RPCResult search1688Category(@RequestParam String searchStr) {
        List<StdCategoryModel> categoryModels = categoryReadService.searchChannelAllStdCategory(
            StdCategoryChannelEnum.CHANNEL_1688, searchStr);
        List<CategoryVO> result = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(categoryModels)) {
            result = categoryModels.stream().map(category -> convertCategoryToVO(category)).collect(
                Collectors.toList());
        }
        return RPCResult.ok(result);
    }

    /**
     * 获取1688的第四级类目
     *
     * @param categoryId
     * @return
     */
    @RequestMapping("/getFourLevelCategory")
    public RPCResult getFourLevelCategory(@RequestParam long categoryId) {
        List<StdCategoryPropertyModel> categoryPropertyList = categoryReadService.getChannelStdCategoryProperties(
            StdCategoryChannelEnum.CHANNEL_1688, categoryId);
        List<CategoryVO> fourLevels = Lists.newArrayList();
        for (StdCategoryPropertyModel cagegoryProperty : categoryPropertyList) {
            if (cagegoryProperty.isFourLevelAttr()) {
                List<StdCategoryPropertyValueModel> featureIdValues = cagegoryProperty.getFeatureIdValues();
                if (CollectionUtils.isEmpty(featureIdValues)) {
                    break;
                }
                for (StdCategoryPropertyValueModel propertyValueModel : featureIdValues) {
                    CategoryVO categoryV = new CategoryVO();
                    categoryV.setCategoryId(propertyValueModel.getVid().intValue());
                    categoryV.setValue(propertyValueModel.getVid().intValue());
                    categoryV.setLabel(propertyValueModel.getValue());
                    categoryV.setHasVirtual(false);
                    categoryV.setLeaf(Boolean.TRUE);
                    fourLevels.add(categoryV);
                }
            }
        }
        return RPCResult.ok(fourLevels);
    }

    /**
     * 获取类目属性
     *
     * @param categoryId
     * @return
     */
    @RequestMapping("/listCategoryProperty")
    public RPCResult search1688Category(@RequestParam long categoryId) {
        List<StdCategoryPropertyModel> categoryPropertyList = categoryReadService.getChannelStdCategoryProperties(
            StdCategoryChannelEnum.CHANNEL_1688, categoryId);
        List<CategoryPropertyVO> result = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(categoryPropertyList)) {
            CategoryPropertyVO top = new CategoryPropertyVO();
            Map<String, List<CategoryPropertyVO>> childMap = Maps.newHashMap();
            List<CategoryPropertyValueVO> valueList = Lists.newArrayList();
            for (StdCategoryPropertyModel model : categoryPropertyList) {
                CategoryPropertyVO vo = convertCategoryPropertyToVO(model);
                childMap.put(String.valueOf(vo.getFid()), Lists.newArrayList(vo));
                CategoryPropertyValueVO childValue = new CategoryPropertyValueVO();
                childValue.setVid(vo.getFid());
                childValue.setValue(vo.getName());
                childValue.setSort(vo.getOrder());
                valueList.add(childValue);
            }
            Collections.sort(valueList);
            top.setFeatureIdValues(valueList);
            top.setFid(-1);
            top.setChildrenFieldMap(childMap);
            top.setInputType("CHECK_BOX");
            result.add(top);
        }
        return RPCResult.ok(result);
    }

    private CategoryVO convertCategoryToVO(StdCategoryModel model) {
        if (model == null) {
            return null;
        }
        CategoryVO vo = new CategoryVO();
        BeanUtils.copyProperties(model, vo);
        vo.setLabel(model.getName());
        //有4级节点 设置为不是叶子类目
        if (model.isHasVirtual()) {
            vo.setLeaf(false);
        }
        if (!CollectionUtils.isEmpty(model.getChildren())) {
            List<CategoryVO> childVos = model.getChildren().stream().map(m ->
                convertCategoryToVO(m)
            ).collect(Collectors.toList());
            vo.setChildren(childVos);
        }
        return vo;
    }

    private CategoryPropertyVO convertCategoryPropertyToVO(StdCategoryPropertyModel model) {
        if (model == null) {
            return null;
        }
        CategoryPropertyVO vo = new CategoryPropertyVO();
        BeanUtils.copyProperties(model, vo);
        if (model.getInputType() == -1) {
            vo.setInputType("INPUT_NUM");
        } else if (model.getInputType() == 0) {
            vo.setInputType("INPUT_TEXT");
        } else if (model.getInputType() == 1) {
            vo.setInputType("LIST_BOX");
        } else if (model.getInputType() == 2) {
            vo.setInputType("CHECK_BOX");
        } else if (model.getInputType() == 5) {
            vo.setInputType("DATE");
        }
        if (!CollectionUtils.isEmpty(model.getFeatureIdValues())) {
            List<CategoryPropertyValueVO> values = new ArrayList<CategoryPropertyValueVO>();
            for (StdCategoryPropertyValueModel valueModel : model.getFeatureIdValues()) {
                CategoryPropertyValueVO valueVo = new CategoryPropertyValueVO();
                BeanUtils.copyProperties(valueModel, valueVo);
                values.add(valueVo);
            }
            vo.setFeatureIdValues(values);
        }
        return vo;
    }

}
