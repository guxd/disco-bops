package com.alibaba.cbuscm.controller.disco;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.constant.ErrorCodeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcSupplierEnrollService;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcSupplierEnrollQueryParam;
import com.alibaba.cbu.disco.shared.core.enroll.model.view.DcSupplierEnrollVO;
import com.alibaba.cbuscm.logic.DiscoLogic;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * DcSupplierEnrollController
 *
 * @author aniu.nxh
 * @date 2019/07/12 16:39
 */
@RestController
@RequestMapping(value = "/dcSupplierEnroll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class DcSupplierEnrollController {

    @Autowired
    private DcSupplierEnrollService dcSupplierEnrollService;
    @Autowired
    private DiscoLogic discoLogic;

    @ResponseBody
    @RequestMapping(value = "/listEnrollInfo", method = RequestMethod.GET)
    public ListOf<DcSupplierEnrollVO> listEnrollInfo(HttpServletRequest request) {

        String logPrefix = "DcSupplierEnrollController#listEnrollInfo#";

        log.error(logPrefix + "record");

        ListOf<DcSupplierEnrollVO> result = new ListOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                DcSupplierEnrollQueryParam param = new DcSupplierEnrollQueryParam();
                param.setValidEnroll(true);
                // 罗列商家的报名情
                result = dcSupplierEnrollService.listEnrollInfo(param);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue(), e);
        }
        return result;
    }

}
