package com.alibaba.cbuscm.controller.supply.route;

import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.interfaces.supply.route.SupplyRouteService;
import com.alibaba.china.global.business.library.models.supply.route.LogisticsRouteDefinition;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.up.common.mybatis.result.RPCResult;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 全球供货中心物流线路前端交互接口
 * @author lichao.wlc
 * @date 2019/04/18
 */
@RestController
@RequestMapping("/supply/route")
@Slf4j
public class SupplyRouteController {
    @Autowired
    private SupplyRouteService supplyRouteService;

    @RequestMapping("/routes")
    public RPCResult getRoutes(@RequestParam String countryIdentifier) {
        //Country c1 = Country.builder().label("阿拉伯联合酋长国").rawVal("United Arab Emirates").uniqueIdentifier("ARE").build();
        //Country c2 = Country.builder().label("沙特阿拉伯").rawVal("Saudi Arabia").uniqueIdentifier("SAU").build();
        //LogisticsRouteDefinition logisticsRouteDefinition = LogisticsRouteDefinition.builder().routeId(1L).routeName("中东专线>5美金").build();
        //LogisticsRouteDefinition logisticsRouteDefinition1 = LogisticsRouteDefinition.builder().routeId(2L).routeName("中东专线<=5美金").build();
        //List<LogisticsRouteDefinition> list = Lists.newArrayList(logisticsRouteDefinition, logisticsRouteDefinition1);
        //return RPCResult.ok(list);
        try {
            ResultOf<List<LogisticsRouteDefinition>> resultOf = supplyRouteService.getLogisticsRoutesByCountry(countryIdentifier);
            if(resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
        }catch (Exception e) {
            log.error("getRoutes error",e.getMessage(),e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "getRoutes error");
    }

    @RequestMapping("/onepiece-routes")
    public RPCResult getOnePieceRoutes(@RequestParam String countryIdentifier) {
        try {
            ResultOf<List<LogisticsRouteDefinition>> resultOf = supplyRouteService.getLogisticsRoutesByCountry(countryIdentifier);
            if(resultOf.isSuccess()) {
                List<LogisticsRouteDefinition> logisticsRouteDefinitionList = resultOf.getData();
                JSONArray returnJson = new JSONArray();
                for(LogisticsRouteDefinition logisticsRouteDefinition : logisticsRouteDefinitionList) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("label", logisticsRouteDefinition.getRouteName());
                    jsonObject.put("value", logisticsRouteDefinition.getRouteId());
                    returnJson.add(jsonObject);
                }
                return RPCResult.ok(returnJson);
            }
        }catch (Exception e) {
            log.error("getRoutes error",e.getMessage(),e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "getRoutes error");
    }
}
