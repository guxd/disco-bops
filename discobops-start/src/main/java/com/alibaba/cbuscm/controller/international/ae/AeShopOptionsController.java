package com.alibaba.cbuscm.controller.international.ae;

import com.alibaba.china.global.business.dispatch.interfaces.SiteManageService;
import com.alibaba.china.global.business.dispatch.model.SiteModel;
import com.alibaba.china.global.business.common.ResultOf;
import com.alibaba.security.spring.component.csrf.CsrfTokenModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Ae shop options controller.
 *
 * @author zhuoxian
 */
@RestController
@CrossOrigin("*")
@CsrfTokenModel
@RequestMapping(path = "/api/ae-shop/options", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class AeShopOptionsController {

    @Autowired
    private SiteManageService siteManageService;

    @ResponseBody
    @RequestMapping(path = "/sites", method = RequestMethod.GET)
    public Object getSites() {
        ResultOf<List<SiteModel>> siteResult = siteManageService.listAllSiteInfo();

        if (!ResultOf.isValid(siteResult)) {
            return siteResult;
        }

        List<String> sites = siteResult.getData().stream()
            .map(SiteModel::getSiteName)
            .collect(Collectors.toList());

        return ResultOf.general(sites);
    }

}
