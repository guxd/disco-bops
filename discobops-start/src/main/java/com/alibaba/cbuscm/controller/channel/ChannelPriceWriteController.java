package com.alibaba.cbuscm.controller.channel;

import com.alibaba.cbuscm.RestResult;
import com.alibaba.cbuscm.service.channel.ChannelPriceWriteAdapter;
import com.alibaba.cbuscm.vo.channel.req.ChannelModifyReq;
import com.alibaba.cbuscm.vo.channel.WarehouseRespVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Auther: gxd
 * @Date: 2019/10/16 10:08
 * @Description:渠道价格管理
 */
@RestController
@RequestMapping(path = "/channel/write", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ChannelPriceWriteController {

    @Autowired
    private ChannelPriceWriteAdapter channelPriceWriteAdapter;

    /**
     * 修改仓储费
     * @param req
     * @return
     */
    @PostMapping("/modifyWarehousePrice")
    public RestResult<Boolean> modifyWarehousePrice(@RequestBody ChannelModifyReq req){
        if (null != req.getChannelProductPriceId()){

        }

        if (null != req.getWarehouseParam()){
            List<WarehouseRespVO> warehouseParam = req.getWarehouseParam();
        }


        return RestResult.success(null);
    }


    /**
     * 调整毛利率
     * @param req
     * @return
     */
    @PostMapping("/modifyGrossPrice")
    public RestResult<Boolean> modifyGrossPrice(@RequestBody ChannelModifyReq req){



        //校验参数
        if (null !=req.getChannelProductPriceId()){

        }

        if (null !=req.getGrossProfitRate()){

        }
        //调用接口


        return RestResult.success(null);
    }


    /**
     * 调整供货价
     * @param req
     * @return
     */
    @PostMapping("/modifySupplyPrice")
    public RestResult<Boolean> modifySupplyPrice(@RequestBody ChannelModifyReq req){
        if (null != req.getChannelProductPriceId()){

        }

        if (null !=req.getSupplyPrice()){

        }
        return RestResult.success(null);
    }

}
