package com.alibaba.cbuscm.controller.international.ae;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.alimonitor.jmonitor.utils.StringUtils;
import com.alibaba.cbuscm.common.PageResult;
import com.alibaba.cbuscm.enums.ErrorCodeEnum;
import com.alibaba.cbuscm.params.AEProductDelParam;
import com.alibaba.cbuscm.params.AEProductQueryParam;
import com.alibaba.cbuscm.params.RequestDelParam;
import com.alibaba.cbuscm.service.AEProductDeleteService;
import com.alibaba.cbuscm.service.AEProductSelectService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by duanyang.zdy on 2019/4/24.
 */
@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/ae-shop/selector", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class AeProductSelectorController {

    @Autowired
    private AEProductSelectService aeProductSelectService;

    @Autowired
    private AEProductDeleteService aEProductDeleteService;

    @ResponseBody
    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public Object getItems(
        @RequestParam(value = "sortField", required = false) String sortField,
        @RequestParam(value = "taskId", required = false) String taskId,
        @RequestParam(value = "pageSize", required = false, defaultValue = "20") String pageSize,
        @RequestParam(value = "pageNo", required = false, defaultValue = "1") String pageNo,
        HttpServletRequest request) {

        if(StringUtils.isEmpty(taskId)){

            PageResult.fail(ErrorCodeEnum.RESUELT_PARAM_ERROR.getValue(),ErrorCodeEnum.RESUELT_PARAM_ERROR.getValue());
        }
        AEProductQueryParam param = AEProductQueryParam.builder()
            .orderFiled(sortField)
            .taskId(Long.valueOf(taskId))
            .build();

        param.setPageNo(Integer.valueOf(pageNo));
        param.setPageSize(Integer.valueOf(pageSize));
        return aeProductSelectService.search(param);
    }

    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public Object removeItem(@RequestBody RequestDelParam param, HttpServletRequest request) {

        if(param.getTaskId() == null || StringUtils.isEmpty(param.getDownItemId()) || param.getSearchTagId() == null){

        return PageResult.fail(ErrorCodeEnum.RESUELT_PARAM_ERROR.getValue(),ErrorCodeEnum.RESUELT_PARAM_ERROR.getValue());
    }

    return aEProductDeleteService.delete(AEProductDelParam.builder()
        .taskId(param.getTaskId())
        .siteId(param.getSiteId())
        .searchTagId(param.getSearchTagId())
        .downItemId(param.getDownItemId())
        .offerId(param.getOfferId())
        .build());
}



}
