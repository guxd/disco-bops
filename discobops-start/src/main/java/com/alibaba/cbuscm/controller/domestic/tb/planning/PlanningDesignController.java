package com.alibaba.cbuscm.controller.domestic.tb.planning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.buc.api.EnhancedUserQueryService;
import com.alibaba.buc.api.model.enhanced.EnhancedUser;
import com.alibaba.cbu.sellergrowth.growth.bean.ActionVO;
import com.alibaba.cbu.sellergrowth.supply.api.SupplyActionService;
import com.alibaba.cbuscm.controller.domestic.tb.planning.request.AddDesignRequest;
import com.alibaba.cbuscm.controller.domestic.tb.planning.request.EditDesignRequest;
import com.alibaba.cbuscm.controller.domestic.tb.planning.request.UrlDesignRequest;
import com.alibaba.cbuscm.diamond.SupplyActionConfig;
import com.alibaba.cbuscm.params.ExtInfoParam;
import com.alibaba.cbuscm.utils.DateUtils;
import com.alibaba.cbuscm.utils.ExcelUtils;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.domestic.tb.planning.PlanningDesignImportVO;
import com.alibaba.cbuscm.vo.domestic.tb.planning.PlanningDesignVO;
import com.alibaba.china.global.business.library.enums.StdCategoryChannelEnum;
import com.alibaba.china.global.business.library.interfaces.category.CategoryReadService;
import com.alibaba.china.global.business.library.models.category.StdCategoryModel;
import com.alibaba.china.shared.discosupplier.enums.SupplierSceneEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.design.PlanningDesignDrainagePredEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.design.PlanningDesignOrientationEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.design.PlanningDesignStatusEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.design.PlanningDesignTypeEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.opp.PlanningOppItemStatusEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.opp.PlanningTaskStatusEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.opp.PlanningTaskTypeEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.source.PlanningSourceStatusEnum;
import com.alibaba.china.shared.discosupplier.model.common.UserModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningAttr;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningCompetitor;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningDesignModel;
import com.alibaba.china.shared.discosupplier.model.planning.opp.PlanningTaskModel;
import com.alibaba.china.shared.discosupplier.model.planning.source.PlanningSourceModel;
import com.alibaba.china.shared.discosupplier.param.planning.design.PlanningDesignParam;
import com.alibaba.china.shared.discosupplier.param.planning.opp.PlanningOppItemParam;
import com.alibaba.china.shared.discosupplier.service.common.CompetitorOfferService;
import com.alibaba.china.shared.discosupplier.service.planning.design.PlanningDesignService;
import com.alibaba.china.shared.discosupplier.service.planning.opp.PlanningOppItemService;
import com.alibaba.china.shared.discosupplier.service.planning.opp.PlanningTaskService;
import com.alibaba.china.shared.discosupplier.service.planning.source.PlanningSourceService;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.PageList;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.result.Result;
import com.alibaba.up.common.mybatis.task.Paginator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wb-qiuth
 * 商品企划
 */
@RestController
@RequestMapping(path = "/planning", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PlanningDesignController {

    private static final Logger LOG = LoggerFactory.getLogger(PlanningDesignController.class);

    @Autowired
    private PlanningSourceService planningSourceService;
    @Autowired
    private PlanningDesignService planningDesignService;
    @Autowired
    private EnhancedUserQueryService enhancedUserReadQueryService;
    @Autowired
    private CategoryReadService categoryReadService;
    @Autowired
    private CompetitorOfferService competitorOfferService;
    @Autowired
    private PlanningOppItemService planningOppItemService;
    @Autowired
    private SupplyActionService supplyActionService;
    @Autowired
    private PlanningTaskService planningTaskService;

    /**
     * 商机企划列表
     *
     * @param request
     * @param mine           是否 我的企划
     * @param title
     * @param page
     * @param itemsPerPage
     * @param orderBy
     * @param orderDirection
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RPCResult list(HttpServletRequest request,
        @RequestParam(name = "mine", required = false) boolean mine,
        @RequestParam(name = "title", required = false) String title,
        @RequestParam(name = "page", defaultValue = "1") Integer page,
        @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
        @RequestParam(name = "orderBy", defaultValue = "GMT_MODIFIED") String orderBy,
        @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection) {
        PlanningDesignParam param = new PlanningDesignParam();
        // 是否查看我的企划
        if (mine) {
            param.setAdmin(LoginUtil.getEmpId(request));
        } else {
            param.setStatus(PlanningDesignStatusEnum.VALID.getValue());
        }
        if (StringUtils.isNotBlank(title)) {
            param.setTitle(title);
        }
        param.setPage(page);
        param.setPageSize(itemsPerPage);
        param.setOrderBy(orderBy);
        param.setOrderDirection(orderDirection);
        param.setCountNeeded(true);
        // 企划列表
        PageList<PlanningDesignModel> pageList = planningDesignService.query(param);

        Map<String, Object> result = Maps.newHashMap();
        if (pageList != null && CollectionUtils.isNotEmpty(pageList.getDataList())) {
            List<PlanningDesignVO> voList = pageList.getDataList().stream().map(i -> {
                PlanningDesignVO vo = new PlanningDesignVO();
                BeanUtils.copyProperties(i, vo, "offerCount", "type", "status", "drainagePred", "orientation");
                // 组装企划类型、状态、渠道、引流转化预测
                buildPlanningVo(vo, i);
                //机审通过商品数
                getMachinePassNum(i.getId(), vo);
                //权限判断
                getPermission(request, i, vo);
                return vo;
            }).collect(Collectors.toList());

            result.put("data", voList);
            result.put("paginator", new Paginator(pageList.getPaginator().getItems(), itemsPerPage, page));
        } else {
            result.put("data", new ArrayList<>());
            result.put("paginator", new Paginator(0, itemsPerPage, page));
        }
        return RPCResult.okNoWrapper(result);
    }

    /**
     * 新建/更新企划
     *
     * @param request
     * @param addDesignRequest
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public RPCResult add(HttpServletRequest request, @RequestBody AddDesignRequest addDesignRequest) {
        if (null == addDesignRequest || null == addDesignRequest.getType() || null == addDesignRequest.getTitle()
            || null == SupplierSceneEnum.parse(addDesignRequest.getScene())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        // 设置企划类型、企划标题、企划描述、企划渠道
        PlanningDesignModel planningDesignModel = new PlanningDesignModel();
        planningDesignModel.setType(PlanningDesignTypeEnum.parse(addDesignRequest.getType()));
        planningDesignModel.setTitle(addDesignRequest.getTitle());
        planningDesignModel.setDesc(addDesignRequest.getDesc());
        SupplierSceneEnum supplierSceneEnum = SupplierSceneEnum.parse(addDesignRequest.getScene());
        if (supplierSceneEnum != null) {
            planningDesignModel.setScene(supplierSceneEnum.getValue());
        }
        Set<UserModel> admins = new HashSet<>();
        if (null != addDesignRequest.getAdmins()) {
            admins = addDesignRequest.getAdmins();
        }
        // 添加当前登陆人
        UserModel userModel = new UserModel();
        userModel.setEmpId(LoginUtil.getEmpId(request));
        userModel.setStageName(LoginUtil.getLoginName(request));
        admins.add(userModel);
        if (admins.size() > 10) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "管理员不可超过10人");
        }
        // 企划管理员
        planningDesignModel.setAdmins(admins);
        Long id = addDesignRequest.getId();
        // 当前企划主键id不为空 且 非提报状态，则编辑头部信息, 否则新增
        if (null != id) {
            PlanningDesignModel designModel = planningDesignService.find(id);
            if (null == designModel) {
                return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "未找到对应的企划");
            }
            // 已提报 不允许再编辑
            PlanningOppItemParam oppItemParam = new PlanningOppItemParam();
            oppItemParam.setPlanningId(designModel.getId());
            oppItemParam.setStatusList(PlanningOppItemStatusEnum.careStatusEnums().stream()
                .filter(i -> !PlanningOppItemStatusEnum.START.equals(i)).map(PlanningOppItemStatusEnum::getValue)
                .collect(Collectors.toList()));
            if (planningOppItemService.count(oppItemParam) > 0) {
                return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "已提报 不允许再编辑");
            }
            // 企划主键id
            planningDesignModel.setId(id);
            // 更新企划 TODO 可能存在数据不一致
            Boolean flag = planningDesignService.update(planningDesignModel);
            // 判断企划标题是否有变更
            if (!addDesignRequest.getTitle().equals(designModel.getTitle())) {
                // 根据企划主键id获取相对应的寻源
                PlanningSourceModel planningSourceModel = planningSourceService.findByPlanningId(designModel.getId());
                // 同步下游寻源标题
                if (null != planningSourceModel) {
                    PlanningSourceModel updatePlanningSourceModel = new PlanningSourceModel();
                    updatePlanningSourceModel.setId(planningSourceModel.getId());
                    updatePlanningSourceModel.setPlanningTitle(addDesignRequest.getTitle());
                    planningSourceService.update(updatePlanningSourceModel);
                }
            }
            // 是否更新企划成功
            if (flag) {
                return RPCResult.ok(true);
            } else {
                return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_SYSTEM);
            }
        } else {
            planningDesignModel.setStatus(PlanningDesignStatusEnum.DRAFT.getValue());
            // 创建新的企划 当前企划状态为草稿状态
            Long result = planningDesignService.create(planningDesignModel);
            if (result != null && result > 0) {
                return RPCResult.ok(result);
            } else {
                return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_SYSTEM);
            }
        }
    }

    /**
     * 查询企划详情
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public RPCResult detail(HttpServletRequest request, @RequestParam(name = "id") Long id) {
        if (id == null) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        // 根据企划主键id查询相应的企划信息
        PlanningDesignModel design = planningDesignService.find(id);
        if (null == design) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        PlanningDesignVO vo = new PlanningDesignVO();
        BeanUtils.copyProperties(design, vo, "type", "status", "scene", "drainagePred", "orientation");
        // 返回叶子类目
        if (null != vo.getCategoryId()) {
            String[] categoryIds = vo.getCategoryId().split(",");
            String categoryId = categoryIds[categoryIds.length - 1];
            vo.setCategoryId(categoryId);
        }
        // 组装企划类型、状态、渠道、引流转化预测
        buildPlanningVo(vo, design);
        // 根据企划主键id查询到对应的寻源信息
        PlanningSourceModel planningSourceModel = planningSourceService.findByPlanningId(id);
        if (null != planningSourceModel) {
            vo.setSourceId(planningSourceModel.getId());
        }
        //机审通过商品数
        getMachinePassNum(id, vo);
        //权限判断
        getPermission(request, design, vo);
        return RPCResult.ok(vo);
    }

    /**
     * 编辑企划
     *
     * @param request
     * @param editDesignRequest
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public RPCResult edit(HttpServletRequest request, @RequestBody EditDesignRequest editDesignRequest) {
        if (null == editDesignRequest || null == editDesignRequest.getId()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        // 发布校验
        if (Boolean.TRUE.equals(editDesignRequest.getRelease())) {
            if (null == editDesignRequest.getWaveBrand() || null == editDesignRequest.getUpTime() ||
                null == editDesignRequest.getCategoryValue() || null == editDesignRequest.getSellPoint() ||
                CollectionUtils.isEmpty(editDesignRequest.getHandlers()) ||
                null == SupplierSceneEnum.parse(editDesignRequest.getScene()) || null == editDesignRequest.getId() ||
                null == editDesignRequest.getImportMax()) {
                return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
            }
            if (editDesignRequest.getHandlers().size() > 10) {
                return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, "寻源小二不可超过10人");
            }
        }
        // 根据企划主键id查询到相应的企划信息
        PlanningDesignModel design = planningDesignService.find(editDesignRequest.getId());
        // 判断是否已提报
        PlanningOppItemParam oppItemParam = new PlanningOppItemParam();
        oppItemParam.setPlanningId(editDesignRequest.getId());
        oppItemParam.setStatusList(PlanningOppItemStatusEnum.careStatusEnums().stream().filter(i -> {
            return !PlanningOppItemStatusEnum.START.equals(i);
        }).map(PlanningOppItemStatusEnum::getValue).collect(Collectors.toList()));
        if (null == design || CollectionUtils.isEmpty(design.getAdmins()) || planningOppItemService.count(oppItemParam)
            > 0
            || design.getAdmins().stream().noneMatch(i -> {
            return i.getEmpId().equals(LoginUtil.getEmpId(request));
        })) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "已提报状态不可编辑");
        }
        PlanningDesignModel planningDesignModel = new PlanningDesignModel();
        planningDesignModel.setId(editDesignRequest.getId());
        if (null != editDesignRequest.getType()) {
            planningDesignModel.setType(PlanningDesignTypeEnum.parse(editDesignRequest.getType()));
        }
        planningDesignModel.setTitle(editDesignRequest.getTitle());
        planningDesignModel.setDesc(editDesignRequest.getDesc());
        SupplierSceneEnum supplierSceneEnum = SupplierSceneEnum.parse(editDesignRequest.getScene());
        if (supplierSceneEnum != null) {
            planningDesignModel.setScene(supplierSceneEnum.getValue());
        }
        if (CollectionUtils.isNotEmpty(editDesignRequest.getHandlers())) {
            // 企划处理人
            planningDesignModel.setHandlers(Sets.newHashSet(editDesignRequest.getHandlers()));
        }

        Set<UserModel> admins = editDesignRequest.getAdmins();
        if (null == admins) {
            admins = Sets.newHashSet();
        }
        // 添加当前登陆人
        UserModel userModel = new UserModel();
        userModel.setEmpId(LoginUtil.getEmpId(request));
        userModel.setStageName(LoginUtil.getLoginName(request));
        admins.add(userModel);
        planningDesignModel.setAdmins(admins);

        if (planningDesignModel.getAdmins().size() > 10) {
            return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, "管理员不可超过10人");
        }
        planningDesignModel.setWaveBrand(editDesignRequest.getWaveBrand());
        planningDesignModel.setUpTime(editDesignRequest.getUpTime());
        planningDesignModel.setSellPoint(editDesignRequest.getSellPoint());
        if (null != editDesignRequest.getCategoryId()) {
            PlanningCategoryInfo categoryInfo = getPlanningCategoryInfo(
                Long.parseLong(editDesignRequest.getCategoryId()));
            planningDesignModel.setCategoryId(categoryInfo.getCategoryId());
            planningDesignModel.setCategoryValue(categoryInfo.getCategoryValue());
        }
        planningDesignModel.setImportLower(editDesignRequest.getImportLower());
        planningDesignModel.setImportUp(editDesignRequest.getImportUp());
        planningDesignModel.setSpecScope(editDesignRequest.getSpecScope());
        planningDesignModel.setExpConvertRate(editDesignRequest.getExpConvertRate());
        planningDesignModel.setDailySales(editDesignRequest.getDailySales());
        planningDesignModel.setDistriFlow(editDesignRequest.getDistriFlow());
        planningDesignModel.setRemark(editDesignRequest.getRemark());
        if (CollectionUtils.isNotEmpty(editDesignRequest.getKeyAttrs())) {
            planningDesignModel.setKeyAttrs(editDesignRequest.getKeyAttrs());
        }
        if (CollectionUtils.isNotEmpty(editDesignRequest.getSpecs())) {
            planningDesignModel.setSpecs(editDesignRequest.getSpecs());
        }
        if (CollectionUtils.isNotEmpty(editDesignRequest.getCompetitors())) {
            planningDesignModel.setCompetitors(editDesignRequest.getCompetitors());
        }
        planningDesignModel.setImportMax(editDesignRequest.getImportMax());
        buildDrainagePred(editDesignRequest.getDrainagePred(), planningDesignModel);
        if (PlanningDesignOrientationEnum.checkValid(editDesignRequest.getOrientation()) != null) {
            planningDesignModel.setOrientation(editDesignRequest.getOrientation());
        }
        // 是否发布或更新成功
        Boolean flag = false;
        // 是否 发布企划任务
        // 是 则除了修改企划表，还需往寻源表添加一条数据，否 只需要修改企划表
        if (Boolean.TRUE.equals(editDesignRequest.getRelease())) {
            planningDesignModel.setStatus(PlanningDesignStatusEnum.VALID.getValue());
            // 更新企划信息 当前企划状态为有效
            flag = planningDesignService.update(planningDesignModel);
            if (flag) {
                PlanningSourceModel planningSourceModel = new PlanningSourceModel();
                planningSourceModel.setPlanningId(editDesignRequest.getId());
                planningSourceModel.setPlanningTitle(editDesignRequest.getTitle());
                Set<UserModel> handlers = new HashSet<>(editDesignRequest.getHandlers());
                planningSourceModel.setHandlers(handlers);
                planningSourceModel.setStatus(PlanningSourceStatusEnum.VALID.getValue());
                // 发布企划时 判断是否存在对应的 寻源信息，若存在 做更新操作，不存在 做新增操作
                PlanningSourceModel dbSource = planningSourceService.findByPlanningId(editDesignRequest.getId());
                if (null != dbSource) {
                    planningSourceModel.setId(dbSource.getId());
                    // 更新寻源信息
                    flag = planningSourceService.update(planningSourceModel);
                } else {
                    // 创建一条寻源
                    Long result = planningSourceService.create(planningSourceModel);
                    flag = result > 0;
                }

                // 企划发布后同步给智运营策略中心、新增类目匹配任务
                syncSellerGrowth(planningDesignModel.getId(), LoginUtil.getEmpId(request));
            } else {
                return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_SYSTEM);
            }
        } else {
            // 更新企划信息
            flag = planningDesignService.update(planningDesignModel);
        }
        if (flag) {
            return RPCResult.ok(true);
        } else {
            return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_SYSTEM);
        }
    }

    private void syncSellerGrowth(Long planningId, String empId) {
        LOG.info("syncSellerGrowth planningId=" + planningId);
        try {
            ActionVO actionVO = new ActionVO();
            actionVO.setActionPrototypeId(63L);
            actionVO.setEndDate(DateUtils.getCurrentYearLastDay());
            actionVO.setExtInfo(JSON.toJSONString(new ExtInfoParam(String.valueOf(planningId))));
            LOG.info("createOrUpdateActionPlan, param={}, empId={}", JSON.toJSONString(actionVO), empId);
            com.alibaba.cbu.sellergrowth.growth.bean.Result<Long> result =
                supplyActionService.createOrUpdateActionPlan(actionVO, null, empId);
            if (result == null || !result.isSuccess()) {
                LOG.error("supplyActionService.createOrUpdateActionPlan fail, result={}",
                    JSON.toJSONString(result));
            }
            LOG.info("createOrUpdateActionPlan success, result={}", JSON.toJSONString(result));
        } catch (Exception e) {
            LOG.error("supplyActionService.createOrUpdateActionPlan error", e);
        }
        try {
            PlanningTaskModel planningTaskModel = new PlanningTaskModel();
            planningTaskModel.setPlanningId(planningId);
            planningTaskModel.setStatus(PlanningTaskStatusEnum.START.getValue());
            planningTaskModel.setType(PlanningTaskTypeEnum.INFORMAL_OPP.getValue());
            if (!planningTaskService.createIfAbsent(planningTaskModel)) {
                LOG.error("planningTaskService.create fail");
            }
        } catch (Exception e) {
            LOG.error("planningTaskService.create error", e);
        }
    }

    /**
     * 删除企划
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public RPCResult delete(HttpServletRequest request,
        @RequestParam(name = "id", required = false) Long id) {
        if (id == null) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        // 根据主键id获取到相应的企划信息
        PlanningDesignModel design = planningDesignService.find(id);
        // 校验管理人
        if (null == design || design.getAdmins() == null || design.getAdmins().stream().noneMatch(
            i -> i.getEmpId().equals(LoginUtil.getEmpId(request)))) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        // 已提报状态不可删除
        PlanningOppItemParam oppItemParam = new PlanningOppItemParam();
        oppItemParam.setPlanningId(id);
        oppItemParam.setStatusList(PlanningOppItemStatusEnum.careStatusEnums().stream().filter(i -> {
            return !PlanningOppItemStatusEnum.START.equals(i);
        }).map(PlanningOppItemStatusEnum::getValue).collect(Collectors.toList()));
        if (planningOppItemService.count(oppItemParam) > 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "已提报状态不可删除");
        }
        // 根据企划主键id进行删除
        Boolean result = planningDesignService.delete(id);
        if (Boolean.TRUE.equals(result)) {
            // 已经发布 && 删除成功后同步智运营策略中心
            if (PlanningDesignStatusEnum.VALID.getValue().equals(design.getStatus())) {
                deleteSupplyActionWithRetry(id);
            }
            return RPCResult.ok(true);
        } else {
            return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_SYSTEM);
        }
    }

    private void deleteSupplyActionWithRetry(Long id) {
        try {
            com.alibaba.cbu.sellergrowth.growth.bean.Result<Boolean> booleanResult =
                supplyActionService.deleteSupplyAction(63L, String.valueOf(id), SupplyActionConfig.getAccessCode());
            if (booleanResult == null || !booleanResult.isSuccess()) {
                LOG.error("supplyActionService.deleteSupplyAction fail, id={}", id);
            }
        } catch (Exception e) {
            try {
                LOG.error("supplyActionService.deleteSupplyAction error, id={}", id, e);
                com.alibaba.cbu.sellergrowth.growth.bean.Result<Boolean> booleanResult =
                    supplyActionService.deleteSupplyAction(63L, String.valueOf(id), SupplyActionConfig.getAccessCode());
                if (booleanResult == null || !booleanResult.isSuccess()) {
                    LOG.error("retry supplyActionService.deleteSupplyAction fail, id={}", id);
                }
            } catch (Exception ex) {
                LOG.error("retry supplyActionService.deleteSupplyAction error, id={}", id, ex);
            }
        }
    }

    /**
     * 获取企划类型
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getTypes", method = RequestMethod.GET)
    public RPCResult getTypes(HttpServletRequest request) {
        List<Map<String, String>> typeList = new ArrayList<>();
        PlanningDesignTypeEnum[] designTypeEnums = PlanningDesignTypeEnum.values();
        for (PlanningDesignTypeEnum designTypeEnum : designTypeEnums) {
            if (designTypeEnum == null) {
                continue;
            }
            Map<String, String> designTypeMap = Maps.newHashMap();
            designTypeMap.put("value", designTypeEnum.getValue());
            designTypeMap.put("label", designTypeEnum.getMessage());
            typeList.add(designTypeMap);
        }
        return RPCResult.ok(typeList);
    }

    /**
     * 获取渠道类型
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getScenes", method = RequestMethod.GET)
    public RPCResult getScenes(HttpServletRequest request) {
        List<Map<String, String>> sceneList = new ArrayList<>();
        SupplierSceneEnum[] sceneEnums = SupplierSceneEnum.values();
        for (SupplierSceneEnum sceneEnum : sceneEnums) {
            // 过滤不可见
            if (sceneEnum == null || !sceneEnum.isPlanningDisplay()) {
                continue;
            }
            Map<String, String> sceneMap = Maps.newHashMap();
            sceneMap.put("value", sceneEnum.getValue());
            sceneMap.put("label", sceneEnum.getMessage());
            sceneList.add(sceneMap);
        }
        return RPCResult.ok(sceneList);
    }

    /**
     * 拷贝企划
     *
     * @param request
     * @param planningId
     * @return
     */
    @RequestMapping(value = "/copyPlanning", method = RequestMethod.GET)
    public RPCResult copyPlanning(HttpServletRequest request, @RequestParam(name = "planningId") Long planningId) {
        Assert.notNull(planningId, "企划id不能为空");
        // 根据企划id获取对应的企划信息
        PlanningDesignModel planningDesignModel = planningDesignService.find(planningId);
        if(planningDesignModel == null){
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "无效的企划");
        }
        planningDesignModel.setId(null);
        planningDesignModel.setStatus(PlanningDesignStatusEnum.DRAFT.getValue());
        planningDesignModel.setTitle(planningDesignModel.getTitle() + "_复制企划" + planningId);

        Set<UserModel> admins = new HashSet<>();
        if (planningDesignModel.getAdmins() != null) {
            admins = planningDesignModel.getAdmins();
        }
        // 添加当前登陆人
        UserModel userModel = new UserModel();
        userModel.setEmpId(LoginUtil.getEmpId(request));
        userModel.setStageName(LoginUtil.getLoginName(request));
        admins.add(userModel);
        // 企划管理员
        planningDesignModel.setAdmins(admins);
        // 插入一条企划数据
        Long count = planningDesignService.create(planningDesignModel);
        if (count <= 0) {
            return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, "拷贝企划失败");
        }
        return RPCResult.ok();
    }

    /**
     * 获取引流转化预测 types
     *
     * @return
     */
    @RequestMapping("/getDrainagePredTypes")
    public RPCResult getDrainagePredTypes() {
        PlanningDesignDrainagePredEnum[] drainagePredEnums = PlanningDesignDrainagePredEnum.values();
        List<Map<String, String>> drainagePredList = new ArrayList<>();
        Arrays.asList(drainagePredEnums).forEach(drainagePredEnum -> {
            Map<String, String> map = new HashMap<>();
            map.put("value", drainagePredEnum.getValue());
            map.put("desc", drainagePredEnum.getDesc());
            drainagePredList.add(map);
        });
        return RPCResult.ok(drainagePredList);
    }

    /**
     * 获取企划定位 types
     *
     * @return
     */
    @RequestMapping("/getOrientationTypes")
    public RPCResult getOrientationTypes() {
        PlanningDesignOrientationEnum[] orientationEnums = PlanningDesignOrientationEnum.values();
        List<Map<String, String>> orientationList = new ArrayList<>();
        Arrays.asList(orientationEnums).forEach(orientationEnum -> {
            Map<String, String> map = new HashMap<>();
            map.put("value", orientationEnum.getValue());
            map.put("desc", orientationEnum.getDesc());
            orientationList.add(map);
        });
        return RPCResult.ok(orientationList);
    }

    /**
     * 导入企划EXCEL
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/uploadFiles", method = RequestMethod.POST)
    public RPCResult importExcel(HttpServletRequest request, @RequestParam(name = "file") MultipartFile file) {
        List<PlanningDesignImportVO> voList = ExcelUtils.readExcel(PlanningDesignImportVO.class, file);
        if (voList.size() > 200) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "限制导入200条以内");
        }
        if (CollectionUtils.isEmpty(voList)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "Excel没有数据");
        }
        Map<String, Object> result = Maps.newHashMap();
        Set<Integer> errorLines = new HashSet<>();
        // 获取所有的企划EXCEL
        List<PlanningDesignImportVO> importList = handleExcelData(voList, errorLines);
        // errorLines不为null说明校验失败，流程结束
        if (CollectionUtils.isNotEmpty(errorLines)) {
            result.put("errorLines", errorLines);
            return RPCResult.okNoWrapper(result);
        }
        if (CollectionUtils.isNotEmpty(importList)) {
            // 模型转换(PlanningDesignImportVO -> PlanningDesignModel)
            List<PlanningDesignModel> designModels = importList.stream().map(i -> {
                PlanningDesignModel design = new PlanningDesignModel();
                BeanUtils.copyProperties(i, design, "type", "scene", "status", "origin", "drainagePred", "orientation");
                design.setType(PlanningDesignTypeEnum.parse(i.getType()));
                if (SupplierSceneEnum.TIAN_TIAN_FACTORY_STORE.getMessage().equals(i.getScene())) {
                    design.setScene(SupplierSceneEnum.TIAN_TIAN_FACTORY_STORE.getValue());
                }
                if (CollectionUtils.isNotEmpty(i.getKeyAttrs())) {
                    design.setKeyAttrs(new ArrayList<>(i.getKeyAttrs()));
                }
                if (CollectionUtils.isNotEmpty(i.getSpecs())) {
                    design.setSpecs(new ArrayList<>(i.getSpecs()));
                }
                buildDrainagePred(i.getDrainagePred(), design);
                PlanningDesignOrientationEnum orientationEnum = PlanningDesignOrientationEnum.checkValid(
                    i.getOrientation());
                if (orientationEnum != null) {
                    design.setOrientation(orientationEnum.getValue());
                }
                design.setStatus(PlanningDesignStatusEnum.DRAFT.getValue());
                return design;
            }).collect(Collectors.toList());
            // 插入DB
            for (int i = 0; i < designModels.size(); i++) {
                PlanningDesignModel designModel = designModels.get(i);
                if (designModel == null) {
                    continue;
                }
                UserModel userModel = new UserModel();
                userModel.setEmpId(LoginUtil.getEmpId(request));
                userModel.setStageName(LoginUtil.getLoginName(request));
                designModel.getAdmins().add(userModel);
                //捕捉数据库层错误，例如:字段长度不足，同样返回错误行
                try {
                    //新增失败处理
                    Long creatId = planningDesignService.create(designModel);
                    if (creatId <= 0) {
                        result.put(String.valueOf(i), designModel.getTitle() + ":数据格式错误");
                        errorLines.add(i + 2);
                        LOG.error(RPCResult.CODE_PARAM_ERROR, designModel.getTitle());
                        continue;
                    }
                } catch (Exception e) {
                    errorLines.add(i + 2);
                }
            }
            result.put("errorLines", errorLines);
        } else {
            result.put("errorLines", errorLines);
        }
        return RPCResult.okNoWrapper(result);
    }

    /**
     * 整理导入数据
     *
     * @param voList
     * @return
     */
    private List<PlanningDesignImportVO> handleExcelData(List<PlanningDesignImportVO> voList, Set<Integer> errorLines) {
        for (int i = 0; i < voList.size(); i++) {
            PlanningDesignImportVO planningDesignImportVO = voList.get(i);
            if (planningDesignImportVO == null) {
                continue;
            }
            try {
                if (null== planningDesignImportVO.getType() || null == planningDesignImportVO.getTitle() ||
                    null == planningDesignImportVO.getWaveBrand() || null == planningDesignImportVO.getUpTime() ||
                    null == planningDesignImportVO.getCategoryId() || null == planningDesignImportVO.getSellPoint() ||
                    StringUtils.isEmpty(planningDesignImportVO.getHandler()) ||
                    null == SupplierSceneEnum.parse(planningDesignImportVO.getScene()) || null == planningDesignImportVO
                    .getImportMax()) {
                    errorLines.add(i + 2);
                }
                //                if (planningDesignImportVO.getSpecMinPrice().length() > 5) {
                //                    errorLines.add(i+2);
                //                }
                //                if (planningDesignImportVO.getSpecMin().length() > 10) {
                //                    errorLines.add(i+2);
                //                }
                //竞品信息处理
                List<String> urlList = new ArrayList<>();
                urlList.add(planningDesignImportVO.getUrl1());
                urlList.add(planningDesignImportVO.getUrl2());
                urlList.add(planningDesignImportVO.getUrl3());
                urlList.add(planningDesignImportVO.getUrl4());
                urlList.add(planningDesignImportVO.getUrl5());
                urlList.add(planningDesignImportVO.getUrl6());
                //去除null值
                urlList.removeAll(Collections.singleton(null));
                buildCompetitor(planningDesignImportVO, urlList);
                //关键key处理
                planningDesignImportVO.setKeyAttrs(new HashSet<>());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs1());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs2());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs3());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs4());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs5());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs6());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs7());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs8());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs9());
                buildKeyAttrs(planningDesignImportVO, planningDesignImportVO.getKeyAttrs10());
                // 处理规格及指导价
                planningDesignImportVO.setSpecs(new HashSet<>());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec1());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec2());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec3());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec4());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec5());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec6());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec7());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec8());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec9());
                buildSpecs(planningDesignImportVO, planningDesignImportVO.getSpec10());
                //处理管理员和小二 根据工号查昵称
                if (StringUtils.isNotBlank(planningDesignImportVO.getAdmin())) {
                    List<String> adminIds = Arrays.asList(planningDesignImportVO.getAdmin().split(",|，"));
                    if (adminIds.size() > 10) {
                        errorLines.add(i + 2);
                    }
                    Map<String, EnhancedUser> adminMap = enhancedUserReadQueryService.findUsers(adminIds);
                    if (null != adminMap && !adminMap.isEmpty()) {
                        planningDesignImportVO.setAdmins(new HashSet<>());
                        for (Map.Entry<String, EnhancedUser> entry : adminMap.entrySet()) {
                            UserModel userModel = buildUserModel(entry);
                            planningDesignImportVO.getAdmins().add(userModel);
                        }
                    }
                }
                if (StringUtils.isNotBlank(planningDesignImportVO.getHandler())) {
                    List<String> handlerIds = Arrays.asList(planningDesignImportVO.getHandler().split(",|，"));
                    if (handlerIds.size() > 10) {
                        errorLines.add(i + 2);
                    }
                    Map<String, EnhancedUser> handlerMap = enhancedUserReadQueryService.findUsers(handlerIds);
                    if (null != handlerMap && !handlerMap.isEmpty()) {
                        planningDesignImportVO.setHandlers(new HashSet<>());
                        for (Map.Entry<String, EnhancedUser> entry : handlerMap.entrySet()) {
                            UserModel userModel = buildUserModel(entry);
                            planningDesignImportVO.getHandlers().add(userModel);
                        }
                    }
                }
                if (StringUtils.isNotBlank(planningDesignImportVO.getCategoryId())) {
                    PlanningCategoryInfo categoryInfo = getPlanningCategoryInfo(
                        Long.parseLong(planningDesignImportVO.getCategoryId()));
                    planningDesignImportVO.setCategoryId(categoryInfo.getCategoryId());
                    planningDesignImportVO.setCategoryValue(categoryInfo.getCategoryValue());
                }
                if (StringUtils.isNotBlank(planningDesignImportVO.getOfferIds()) && planningDesignImportVO.getOfferIdSet() != null) {
                    Set<Long> offerIdSet = Sets.newHashSet();
                    for (String offerId : planningDesignImportVO.getOfferIds().split(";")) {
                        offerIdSet.add(Long.parseLong(offerId));
                    }
                    planningDesignImportVO.setOfferIdSet(offerIdSet);
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                errorLines.add(i + 2);
            }
        }
        return voList;
    }

    /**
     * 工号和昵称处理
     *
     * @param entry
     * @return
     */
    private UserModel buildUserModel(Map.Entry<String, EnhancedUser> entry) {
        UserModel userModel = new UserModel();
        userModel.setEmpId(entry.getValue().getEmpId());
        // 先取花名, 拿不到再去取真实姓名
        String nickNameCn = entry.getValue().getNickNameCn();
        userModel.setStageName(nickNameCn == null ? entry.getValue().getLastName() : nickNameCn);
        return userModel;
    }

    /**
     * 竞品信息处理
     *
     * @param planningDesignImportVO
     * @param urls
     */
    private void buildCompetitor(PlanningDesignImportVO planningDesignImportVO, List<String> urls) {
        planningDesignImportVO.setCompetitors(new ArrayList<>());
        Result<List<PlanningCompetitor>> result = competitorOfferService.getCompetitorOffersByUrl(urls);
        if (!result.isSuccess() || CollectionUtils.isEmpty(result.getModel())) {
            LOG.error(result.getMsgCode(), result.getMsgInfo());
            return;
        }
        List<PlanningCompetitor> planningCompetitors = result.getModel();
        for (PlanningCompetitor competitor : planningCompetitors) {
            if (competitor == null) {
                continue;
            }
            planningDesignImportVO.getCompetitors().add(competitor);
        }
    }

    /**
     * 关键key处理
     *
     * @param planningDesignImportVO
     * @param keyAttrs
     */
    private void buildKeyAttrs(PlanningDesignImportVO planningDesignImportVO, String keyAttrs) {
        if (StringUtils.isBlank(keyAttrs)) {
            return;
        }

        String[] attr = keyAttrs.split(":|：");
        if(attr.length>=2) {
            PlanningAttr planningAttr = new PlanningAttr();
            planningAttr.setKey(attr[0]);
            planningAttr.setValue(attr[1]);
            planningDesignImportVO.getKeyAttrs().add(planningAttr);
        }
    }

    /**
     * 处理规格及指导价
     *
     * @param planningDesignImportVO
     * @param specs
     */
    private void buildSpecs(PlanningDesignImportVO planningDesignImportVO, String specs) {
        if (StringUtils.isBlank(specs)) {
            return;
        }
        String[] spec = specs.split(":|：");
        if(spec.length>= 2) {
            PlanningAttr planningAttr = new PlanningAttr();
            planningAttr.setKey(spec[0]);
            planningAttr.setValue(spec[1]);
            planningDesignImportVO.getSpecs().add(planningAttr);
        }
    }

    /**
     * 逐级查询类目信息
     *
     * @param categoryId
     * @return
     */
    private void findCategoryModel(Long categoryId, List<StdCategoryModel> categoryList) {
        StdCategoryModel leafCategoryModel = categoryReadService.getChannelStdCategoryDetail(
            StdCategoryChannelEnum.CHANNEL_TB, categoryId);
        if (null != leafCategoryModel) {
            int parentId = leafCategoryModel.getParentId();
            if (0 != parentId) {
                findCategoryModel((long)parentId, categoryList);
            }
            categoryList.add(leafCategoryModel);
        }
    }

    /**
     * 根据淘系叶子类目id组装类目信息
     *
     * @param categoryId
     * @return
     */
    private PlanningCategoryInfo getPlanningCategoryInfo(Long categoryId) {
        //处理淘系类目名称
        List<StdCategoryModel> categoryList = Lists.newArrayList();
        findCategoryModel(categoryId, categoryList);
        StringBuilder categoryIds = new StringBuilder();
        StringBuilder categoryValues = new StringBuilder();
        if (CollectionUtils.isNotEmpty(categoryList)) {
            for (StdCategoryModel item : categoryList) {
                if (item == null) {
                    continue;
                }
                categoryIds.append(item.getCategoryId()).append(",");
                categoryValues.append(item.getName()).append(">");
            }
        }
        PlanningCategoryInfo planningCategoryInfo = new PlanningCategoryInfo();
        if (categoryIds.length() > 0) {
            planningCategoryInfo.setCategoryId(categoryIds.deleteCharAt(categoryIds.length() - 1).toString());
            planningCategoryInfo.setCategoryValue(categoryValues.deleteCharAt(categoryValues.length() - 1).toString());
        }
        return planningCategoryInfo;
    }

    /**
     * 淘系类目封装类
     */
    @Data
    private static class PlanningCategoryInfo {
        private String categoryId;
        private String categoryValue;
    }

    /**
     * 获取机审通过商品数
     *
     * @param id
     * @param vo
     */
    private void getMachinePassNum(@RequestParam(name = "id", required = true) Long id, PlanningDesignVO vo) {
        PlanningOppItemParam oppItemParam = new PlanningOppItemParam();
        oppItemParam.setPlanningId(id);
        oppItemParam.setStatus(PlanningOppItemStatusEnum.CHANNEL_MACHINE_PASS.getValue());
        Integer offerCount = planningOppItemService.count(oppItemParam);
        vo.setOfferCount(offerCount);
    }

    /**
     * 权限判断
     *
     * @param request
     * @param i
     * @param vo
     */
    private void getPermission(HttpServletRequest request, PlanningDesignModel i, PlanningDesignVO vo) {
        PlanningOppItemParam planningOppItemParam = new PlanningOppItemParam();
        planningOppItemParam.setPlanningId(i.getId());
        planningOppItemParam.setStatusList(PlanningOppItemStatusEnum.careStatusEnums().stream().filter(s -> {
            return !PlanningOppItemStatusEnum.START.equals(s);
        }).map(PlanningOppItemStatusEnum::getValue).collect(Collectors.toList()));
        vo.setEditable(true);
        vo.setDeletable(true);
        if (CollectionUtils.isNotEmpty(i.getAdmins())) {
            if (i.getAdmins().stream().noneMatch(a -> {
                return a.getEmpId().equals(LoginUtil.getEmpId(request));
            }) ||
                planningOppItemService.count(planningOppItemParam) > 0) {
                vo.setEditable(false);
                vo.setDeletable(false);
            }
        }
    }

    /**
     * 竞品查询
     */
    @RequestMapping(value = "/competitorSearch", method = RequestMethod.POST)
    public RPCResult matchSupply(HttpServletRequest request,
        @RequestBody(required = true) String urls) {
        UrlDesignRequest urlDesignRequest = JSON.parseObject(urls, UrlDesignRequest.class);
        if (StringUtils.isEmpty(urlDesignRequest.getUrls())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        String[] urlArray = urlDesignRequest.getUrls().split(",");
        if (urlArray.length > 6) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "竞品数据不能多于6条");
        }
        // 获取符合条件的所有的精品数据
        Result<List<PlanningCompetitor>> planningCompetitorResult = competitorOfferService.getCompetitorOffersByUrl(
            Lists.newArrayList(urlArray));
        if (planningCompetitorResult == null || planningCompetitorResult.isSuccess() && CollectionUtils.isEmpty(
            planningCompetitorResult.getModel())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "竞品数据为空");
        }
        return RPCResult.ok(planningCompetitorResult.getModel());
    }

    /**
     * 组装企划VO(列表展示/详情)
     *
     * @param vo
     * @param design
     */
    private void buildPlanningVo(PlanningDesignVO vo, PlanningDesignModel design) {
        if (null != design.getType()) {
            PlanningDesignTypeEnum planningDesignTypeEnum = PlanningDesignTypeEnum.parse(design.getType().getValue());
            // 企划类型
            String type = null;
            if (planningDesignTypeEnum != null) {
                type = planningDesignTypeEnum.getMessage(design.getType().getValue());
            }
            vo.setType(type);
        }
        if (null != design.getStatus()) {
            PlanningDesignStatusEnum planningDesignStatusEnum = PlanningDesignStatusEnum.parse(design.getStatus());
            // 企划状态
            String status = null;
            if (planningDesignStatusEnum != null) {
                status = planningDesignStatusEnum.getMessage();
            }
            vo.setStatus(status);
        }
        if (null != design.getScene()) {
            SupplierSceneEnum supplierSceneEnum = SupplierSceneEnum.parse(design.getScene());
            // 企划渠道
            String sceneStr = null;
            if (supplierSceneEnum != null) {
                sceneStr = supplierSceneEnum.getMessage();
            }
            vo.setScene(sceneStr);
        }
        // 引流转化预测 多选
        if (CollectionUtils.isNotEmpty(design.getDrainagePred())) {
            StringBuilder drainageBuilder = new StringBuilder();
            for (String pred: design.getDrainagePred()) {
                if (StringUtils.isBlank(pred)) {
                    continue;
                }
                PlanningDesignDrainagePredEnum drainagePredEnum = PlanningDesignDrainagePredEnum.checkValid(pred);
                if (drainagePredEnum != null) {
                    drainageBuilder.append(drainagePredEnum.getValue()).append(",");
                }
            }
            if (drainageBuilder.length() > 0) {
                vo.setDrainagePred(drainageBuilder.substring(0, drainageBuilder.length() - 1));
            }
        }
        // 企划定位
        PlanningDesignOrientationEnum orientationDesc = PlanningDesignOrientationEnum.checkValid(design.getOrientation());
        if (orientationDesc != null) {
            vo.setOrientation(orientationDesc.getDesc());
        }
    }

    /**
     * 组装引流转化预测 多选(编辑/execl导入)
     *
     * @param drainagePred
     * @param design
     */
    private void buildDrainagePred(String drainagePred, PlanningDesignModel design) {
        if (drainagePred != null) {
            String[] drainage = drainagePred.split(",|，");
            List<String> drainageList = new ArrayList<>();
            for (String pred: drainage) {
                if (StringUtils.isBlank(pred)) {
                    continue;
                }
                PlanningDesignDrainagePredEnum drainagePredEnum = PlanningDesignDrainagePredEnum.checkValid(pred);
                if (drainagePredEnum != null) {
                    drainageList.add(drainagePredEnum.getValue());
                }
            }
            if (CollectionUtils.isNotEmpty(drainageList)) {
                design.setDrainagePred(drainageList);
            }
        }
    }
}
