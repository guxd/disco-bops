package com.alibaba.cbuscm.controller.onepiece.opportunity.util;

import com.alibaba.dataworks.dataservice.sdk.common.BeanRegistryProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * DataApiClient的spring配置 <br>
 */
@Configuration
@ComponentScan(basePackageClasses = {DsClientConfig.class})
public class DsClientConfig {

    @Bean
    public BeanRegistryProcessor beanRegistryProcessor() {
        return new BeanRegistryProcessor();
    }

}