package com.alibaba.cbuscm.controller.onepiece.opportunity.util;


import com.taobao.tair.DataEntry;
import com.taobao.tair.Result;
import com.taobao.tair.ResultCode;
import com.taobao.tair.TairManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * 类TairAdapter
 *
 * @author ruikun.xrk
 * @date 2017/06/15
 */
@Service
public class TairAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(TairAdapter.class);

    private static final int NAMESPACE = 167;

    private static final int VERSION = 0;

    private static final int EXPIRE_TIME = 0;

    private static final int LOCK_VERSION = 2;

    private static final int NO_EXPIRE = 0;

    @Resource
    private TairManager tairManager;

    public <K extends Serializable, V extends Serializable> boolean put(K key, V value) {
        try {
            ResultCode result = tairManager.put(NAMESPACE, key, value, VERSION, EXPIRE_TIME);
            return result.isSuccess();
        } catch (Exception e) {
            LOGGER.error("TairAdapter.put-errors!", e);
        }
        return false;
    }

    public <K extends Serializable, V extends Serializable> boolean put(K key, V value, Integer expireTime) {
        try {
            ResultCode result = tairManager.put(NAMESPACE, key, value, VERSION, expireTime);
            return result.isSuccess();
        } catch (Exception e) {
            LOGGER.error("TairAdapter.put-expireTime-errors!", e);
        }
        return false;
    }


    public <K extends Serializable> Object get(K key) {
        try {
            Result<DataEntry> result = tairManager.get(NAMESPACE, key);
            if (result.isSuccess() && null != result.getValue()) {
                return result.getValue().getValue();
            }
        } catch (Exception e) {
            LOGGER.error("TairAdapter.get-errors!key" + key, e);
        }

        return null;
    }

    public <K extends Serializable> boolean tryLock(K key) {
        ResultCode rc = tairManager.put(NAMESPACE, key, "lock", LOCK_VERSION, NO_EXPIRE);
        return ResultCode.SUCCESS.equals(rc);
    }

    public <K extends Serializable> boolean unlock(K key) {
        ResultCode rc = tairManager.invalid(NAMESPACE, key);
        return ResultCode.SUCCESS.equals(rc);
    }
}
