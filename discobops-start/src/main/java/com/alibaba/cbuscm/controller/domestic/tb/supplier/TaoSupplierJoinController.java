/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.controller.domestic.tb.supplier;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalReadService;
import com.alibaba.cbu.disco.shared.core.proposal.param.DcProposalQueryParam;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.AliqygSupplierJoinVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.CategoryViewVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.TaoSupplierJoinVO;
import com.alibaba.china.global.business.library.enums.StdCategoryChannelEnum;
import com.alibaba.china.global.business.library.interfaces.category.CategoryReadService;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.member.service.models.MemberModel;
import com.alibaba.china.shared.discosupplier.enums.SupplierSceneEnum;
import com.alibaba.china.shared.discosupplier.enums.operation.OperationDegreeEnum;
import com.alibaba.china.shared.discosupplier.enums.supplier.SupplierJoinStatusEnum;
import com.alibaba.china.shared.discosupplier.model.operation.OperationLogModel;
import com.alibaba.china.shared.discosupplier.model.supplier.CategoryModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierAscpRelationModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierInfoExtCaigouModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierInfoModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierJoinModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierManageModel;
import com.alibaba.china.shared.discosupplier.param.operation.OperationLogParam;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierAscpRelationParam;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierInfoParam;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierJoinParam;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierManageParam;
import com.alibaba.china.shared.discosupplier.service.common.SupplierAblityService;
import com.alibaba.china.shared.discosupplier.service.operation.OperationLogService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierAscpRelationService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierInfoExtCaigouService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierInfoService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierJoinService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierManageService;
import com.alibaba.china.winport.biz.enums.WinportUrlEnum;
import com.alibaba.china.winport.biz.model.WinportUrlModel;
import com.alibaba.china.winport.biz.service.WinportUrlService;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.PageList;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.result.Result;
import com.alibaba.up.common.mybatis.task.Paginator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 供应商入驻
 *
 * @author yaogaolin
 */
@RestController
@RequestMapping("/taoSupplierJoin")
public class TaoSupplierJoinController {
    private static final Logger LOG = LoggerFactory.getLogger(TaoSupplierJoinController.class);
    private static final Logger ALIQYGLOG = LoggerFactory.getLogger("aliqyg");


    @Autowired
    private SupplierJoinService supplierJoinService;
    @Autowired
    private OperationLogService operationLogService;
    @Autowired
    private MemberReadService memberReadService;
    @Autowired
    private WinportUrlService winportUrlService;
    @Autowired
    private CategoryReadService categoryReadService;
    @Autowired
    private SupplierInfoService supplierInfoService;
    @Autowired
    private SupplierAscpRelationService supplierAscpRelationService;
    @Autowired
    private DcProposalReadService dcProposalReadService;
    @Autowired
    private SupplierAblityService supplierAblityService;
    @Autowired
    private SupplierManageService supplierManageService;
    // 供货中心内贸bizType
    private static final String BIZ_TYPE_NEI_MAO = "INTERNAL";
    private SupplierInfoExtCaigouService supplierInfoExtCaigouService;


    /**
     * 获取供应商入驻申请列表
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RPCResult list(HttpServletRequest request,
        @RequestParam(name = "categoryLeafId", required = false) Long categoryLeafId,
        @RequestParam(name = "status", required = false) String status,
        @RequestParam(name = "companyName", required = false) String companyName,
        @RequestParam(name = "loginId", required = false) String loginId,
        @RequestParam(name = "page", defaultValue = "1") Integer page,
        @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
        @RequestParam(name = "orderBy", defaultValue = "GMT_JOIN") String orderBy,
        @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection,
        @RequestParam(name = "channel", required = true) String channel) {
        SupplierJoinParam param = new SupplierJoinParam();
        if (StringUtils.equals(channel, "domesrtic")) {
            param.setSceneList(SupplierSceneEnum.getDomesrticList());
        } else if (StringUtils.equals(channel, "crossBorder")) {
            param.setSceneList(SupplierSceneEnum.getCrossBorderList());
        }
        if (null != categoryLeafId && categoryLeafId > 0) {
            param.setTaoCategoryId(String.valueOf(categoryLeafId));
        }
        if (StringUtils.isNotBlank(status)) {
            param.setStatus(status);
        }
        param.setCountNeeded(true);
        if (StringUtils.isNotBlank(loginId)) {
            MemberModel memberModel = memberReadService.findMemberByLoginId(loginId);
            if (null != memberModel) {
                param.setLikeMemberId(memberModel.getMemberId());
            } else {
                param.setLikeMemberId(loginId);
            }
        }
        if (StringUtils.isNotBlank(companyName)) {
            param.setCompanyName(companyName);
        }
        param.setPage(page);
        param.setPageSize(itemsPerPage);
        param.setOrderBy(orderBy);
        param.setOrderDirection(orderDirection);

        PageList<SupplierJoinModel> pageList = supplierJoinService.query(param);
        // 批量查询旺铺地址
        List<String> memberIds = getMemberIds(pageList.getDataList());
        Map<String, WinportUrlModel> winportUrlMap = null;
        Map<String, String> loginIdsMap = null;
        if (CollectionUtils.isNotEmpty(memberIds)) {
            try {
                // 批量查询旺铺地址
                winportUrlMap = winportUrlService.getUrl(memberIds,
                    WinportUrlEnum.INDEX_URL);
                loginIdsMap = memberReadService.convertLoginIdsByMemberIds(memberIds);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }

        // 组装VO
        List<TaoSupplierJoinVO> voList = Lists.newArrayList();
        for (SupplierJoinModel model : pageList.getDataList()) {
            TaoSupplierJoinVO vo = new TaoSupplierJoinVO();
            vo.setJoinId(model.getId());
            vo.setMemberId(model.getMemberId());
            vo.setCompanyName(model.getCompanyName());
            vo.setScene(model.getScene());
            vo.setObjective(model.getObjective());
            if (null != loginIdsMap) {
                vo.setLoginId(loginIdsMap.get(model.getMemberId()));
            }
            try {
                SupplierInfoParam infoParam = new SupplierInfoParam();
                infoParam.setMemberId(model.getMemberId());
                SupplierInfoModel infoModel = supplierInfoService.queryOne(infoParam, Boolean.TRUE);
                if (null != infoModel) {
                    vo.setDockingDing(infoModel.getDockingDing());
                    vo.setDockingWang(infoModel.getDockingWang());
                    if (CollectionUtils.isNotEmpty(infoModel.getCategory())) {
                        List<CategoryViewVO> categories = Lists.newArrayList();
                        List<String> categoryIds = infoModel.getCategory().stream().map(CategoryModel::getCategoryKey)
                            .collect(Collectors.toList
                                ());
                        if (CollectionUtils.isNotEmpty(categoryIds)) {
                            for (String categoryId : categoryIds) {
                                if (!NumberUtils.isNumber(categoryId)) {
                                    continue;
                                }
                                String names = categoryReadService
                                    .getCategoryNamePath(StdCategoryChannelEnum.CHANNEL_TB, Long.valueOf(categoryId),
                                        ",");
                                List<String> nameList = Lists.newArrayList(names.split(","));
                                CategoryViewVO categoryViewVO = new CategoryViewVO();
                                categoryViewVO.setCategoryValues(nameList);
                                categories.add(categoryViewVO);
                            }
                        }
                        vo.setCategories(categories);
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
            if (null != model.getGmtJoin()) {
                vo.setGmtJoin(model.getGmtJoin().getTime());
            }

            if (StringUtils.equals(channel, "crossBorder")) {
                try {
                    SupplierAscpRelationParam ascpRelationParam = new SupplierAscpRelationParam();
                    ascpRelationParam.setMemberId(model.getMemberId());
                    SupplierAscpRelationModel supplierAscpRelationModel = supplierAscpRelationService.queryOne(
                        ascpRelationParam, true);
                    if (supplierAscpRelationModel != null) {
                        vo.setAscpSupplierId(supplierAscpRelationModel.getAscpSupplierId());
                    }
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }

            // 获取最近操作人
            OperationLogModel recentOperation = operationLogService.queryOne(
                new OperationLogParam(model.getScene(),
                    null, model.getMemberId()), true);
            vo.setRecentOperator(null == recentOperation ? "" : recentOperation.getOperator());
            vo.setStatus(model.getStatus());
            if (null != winportUrlMap && null != winportUrlMap.get(model.getMemberId())) {
                vo.setWinportUrl(winportUrlMap.get(model.getMemberId()).getIndexUrl());
            }
            voList.add(vo);
        }

        Map<String, Object> result = Maps.newHashMap();
        result.put("data", voList);

        result.put("paginator", new Paginator(pageList.getPaginator().getItems(), itemsPerPage, page));

        return RPCResult.okNoWrapper(result);
    }



    /**
     * 获取阿里企业购供应商入驻申请列表
     */
    @RequestMapping(value = "/aliqyg/list", method = RequestMethod.GET)
    public RPCResult aliqygList(HttpServletRequest request,
                          @RequestParam(name = "categoryLeafId", required = false) Long categoryLeafId,
                          @RequestParam(name = "status", required = false) String status,
                          @RequestParam(name = "companyName", required = false) String companyName,
                          @RequestParam(name = "loginId", required = false) String loginId,
                          @RequestParam(name = "page", defaultValue = "1") Integer page,
                          @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
                          @RequestParam(name = "orderBy", defaultValue = "GMT_JOIN") String orderBy,
                          @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection,
                          @RequestParam(name = "channel", required = false) String channel) {
        SupplierJoinParam param = new SupplierJoinParam();

        param.setSceneList(Arrays.asList(SupplierSceneEnum.ALIQYG));

        if (null != categoryLeafId && categoryLeafId > 0) {
            param.setTaoCategoryId(String.valueOf(categoryLeafId));
        }
        if (StringUtils.isNotBlank(status)) {
            param.setStatus(status);
        }
        param.setCountNeeded(true);
        if (StringUtils.isNotBlank(loginId)) {
            MemberModel memberModel = memberReadService.findMemberByLoginId(loginId);
            if (null != memberModel) {
                param.setLikeMemberId(memberModel.getMemberId());
            } else {
                param.setLikeMemberId(loginId);
            }
        }
        if(StringUtils.isNotBlank(companyName)){
            param.setCompanyName(companyName);
        }
        param.setPage(page);
        param.setPageSize(itemsPerPage);
        param.setOrderBy(orderBy);
        param.setOrderDirection(orderDirection);
        PageList<SupplierJoinModel> pageList = supplierJoinService.query(param);

        // 批量查询旺铺地址
        List<String> memberIds = getMemberIds(pageList.getDataList());
        Map<String, WinportUrlModel> winportUrlMap = null;
        Map<String, String> loginIdsMap = null;
        if (CollectionUtils.isNotEmpty(memberIds)) {
            try {
                // 批量查询旺铺地址
                winportUrlMap = winportUrlService.getUrl(memberIds,
                    WinportUrlEnum.INDEX_URL);
                loginIdsMap = memberReadService.convertLoginIdsByMemberIds(memberIds);
            } catch (Exception e) {
                ALIQYGLOG.error("batch process error, memberIds: "+ JSON.toJSONString(memberIds), e);
            }
        }

        // 组装VO
        List<AliqygSupplierJoinVO> voList = Lists.newArrayList();
        for (SupplierJoinModel model : pageList.getDataList()) {
            AliqygSupplierJoinVO vo = new AliqygSupplierJoinVO();
            vo.setJoinId(model.getId());
            vo.setMemberId(model.getMemberId());
            vo.setCompanyName(model.getCompanyName());
            vo.setScene(model.getScene());
            vo.setObjective(model.getObjective());

            if (null != loginIdsMap) {
                vo.setLoginId(loginIdsMap.get(model.getMemberId()));
            }
            try {
                SupplierInfoParam infoParam = new SupplierInfoParam();
                infoParam.setMemberId(model.getMemberId());
                SupplierInfoModel infoModel = supplierInfoService.queryOne(infoParam, Boolean.TRUE);
                if (null != infoModel) {
                    vo.setDockingDing(infoModel.getDockingDing());
                    vo.setDockingWang(infoModel.getDockingWang());
                    // 优势类目
                    if (CollectionUtils.isNotEmpty(infoModel.getCategory())) {

                        List<CategoryViewVO> categories = Lists.newArrayList();
                        List<String> categoryValues = infoModel.getCategory().stream().map(CategoryModel::getCategoryValue).collect(Collectors.toList
                            ());
                        CategoryViewVO categoryViewVO = new CategoryViewVO();
                        categoryViewVO.setCategoryValues(categoryValues);
                        categories.add(categoryViewVO);
                        vo.setCategories(categories);
                    }
                    // 主营类目
                    if(infoModel.getMainCategory() != null){
                        vo.setMainCategory(infoModel.getMainCategory().getCategoryValue());
                    }

                    // 分销模式：代销／经销
                    Result<SupplierInfoExtCaigouModel> caigouExtInfo = supplierInfoExtCaigouService.showSupplierInfoExt(model.getMemberId());
                    if(caigouExtInfo.isSuccess() && caigouExtInfo.getModel() != null){
                        vo.setDistributionMode(caigouExtInfo.getModel().getDistribution());
                    }

                }
            } catch (Exception e) {
                ALIQYGLOG.error("composit error, memberId:"+model.getMemberId(), e);
            }
            if (null != model.getGmtJoin()) {
                vo.setGmtJoin(model.getGmtJoin().getTime());
            }

            // 获取最近操作人
            OperationLogModel recentOperation = operationLogService.queryOne(
                new OperationLogParam(model.getScene(),
                    null, model.getMemberId()), true);
            vo.setRecentOperator(null == recentOperation ? "" : recentOperation.getOperator());
            vo.setStatus(model.getStatus());
            if(null != winportUrlMap && null != winportUrlMap.get(model.getMemberId())){
                vo.setWinportUrl(winportUrlMap.get(model.getMemberId()).getIndexUrl());
            }
            voList.add(vo);
        }

        Map<String, Object> result = Maps.newHashMap();
        result.put("data", voList);
        result.put("paginator", new Paginator(pageList.getPaginator().getItems(), itemsPerPage, page));
        return RPCResult.okNoWrapper(result);
    }



    /**
     * 供应商申请审核操作
     */
    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    public RPCResult approve(HttpServletRequest request,
        @RequestParam(name = "memberId", required = true) String memberId,
        @RequestParam(name = "auditPass", required = true) Boolean auditPass,
        @RequestParam(name = "auditRemark", required = false) String auditRemark,
        @RequestParam(name = "scene", required = true) String scene) {

        if (null == auditPass || StringUtils.isEmpty(memberId) || StringUtils.isEmpty(scene)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        Result<Boolean> result = null;
        try {
            result = supplierJoinService.approve(memberId, SupplierSceneEnum.parse(scene),
                auditPass, auditRemark, LoginUtil.getLoginName(request), OperationDegreeEnum.WAITER);
            supplierJoinService.showJoinStatus(memberId, SupplierSceneEnum.parse(scene));
        } catch (Exception e) {
            LOG.error("approveException,memberId=" + memberId, e);
        }
        if (null != result && !result.isSuccess()) {
            LOG.error("approveFail,memberId=" + memberId + ",result=" + result.toString());
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
        return RPCResult.ok(Boolean.TRUE);
    }

    private List<String> getMemberIds(List<SupplierJoinModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        List<String> memberIds = Lists.newArrayList();
        for (SupplierJoinModel item : list) {
            if (!memberIds.contains(item.getMemberId())) {
                memberIds.add(item.getMemberId());
            }
        }
        return memberIds;
    }

    /**
     * 判断供应商是否是工厂
     */
    @RequestMapping(value = "/isFactory", method = RequestMethod.GET)
    public RPCResult isFactory(HttpServletRequest request,
        @RequestParam(name = "memberId", required = true) String memberId) {
        if (StringUtils.isEmpty(memberId)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        Result<Boolean> result = null;
        try {
            result = supplierAblityService.isSupplierFactory(memberId);
        } catch (Exception e) {
            RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_CHINESS_SYSTEM);
        }
        if (result.isSuccess()) {
            return RPCResult.ok("isFactory",Boolean.TRUE);
        }
        return RPCResult.ok("isFactory",Boolean.FALSE);
    }

    /**
     * 是否入住成功
     */
    @RequestMapping(value = "/hasCheckSuccess", method = RequestMethod.GET)
    public RPCResult hasCheckSuccess(@RequestParam(name = "memberId", required = true) String memberId
        ,@RequestParam(name = "scene", required = true)String scene) {
        if (StringUtils.isEmpty(memberId)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        SupplierManageParam param = new SupplierManageParam();
        param.setMemberId(memberId);
        //SupplierObjectiveEnum.DOMESRTIC_TRADE.getObjective()
        param.setScene(scene);
        try {
            SupplierManageModel model = supplierManageService.queryOne(param, true);
            if (model != null && model.getStatus().equals(SupplierJoinStatusEnum.PAID)) {
                return RPCResult.ok(Boolean.TRUE);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return RPCResult.error(RPCResult.MSG_CODE_SERVER_ERROR, RPCResult.MSG_ERROR_CHINESS_SYSTEM);
        }
        return RPCResult.ok(Boolean.FALSE);
    }

}
