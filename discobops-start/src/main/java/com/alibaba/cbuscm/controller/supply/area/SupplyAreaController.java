package com.alibaba.cbuscm.controller.supply.area;

import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.interfaces.supply.area.SupplyAreaService;
import com.alibaba.china.global.business.library.models.supply.area.Area;
import com.alibaba.china.global.business.library.models.supply.area.AreaCountryRelation;
import com.alibaba.china.global.business.library.models.supply.area.Country;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.up.common.mybatis.result.RPCResult;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 全球供货中心地区前端交互接口
 * @author lichao.wlc
 * @date 2019/04/18
 */
@RestController
@RequestMapping("/supply/area")
@Slf4j
public class SupplyAreaController {
    @Autowired
    private SupplyAreaService supplyAreaService;

    @RequestMapping("/countries")
    public RPCResult getCountries() {
        //Area zhongdongArea = Area.builder().area("中东").index(0).build();
        //Country c1 = Country.builder().index(0).label("阿拉伯联合酋长国").rawVal("United Arab Emirates").uniqueIdentifier("ARE").build();
        //Country c2 = Country.builder().index(1).label("沙特阿拉伯").rawVal("Saudi Arabia").uniqueIdentifier("SAU").build();
        //AreaCountryRelation areaCountryRelation = AreaCountryRelation.builder().area(zhongdongArea).countries(Lists.newArrayList(c1, c2)).build();
        //List list = Lists.newArrayList(areaCountryRelation);
        try {
            ResultOf<List<AreaCountryRelation>> resultOf = supplyAreaService.getCountries();
            if(resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
        }catch (Exception e) {
            log.error("getCountries error",e.getMessage() ,e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "getCountries error");
    }

    @RequestMapping("/onepiece-countries")
    public RPCResult getOnePieceCountries() {
        try {
            ResultOf<List<AreaCountryRelation>> resultOf = supplyAreaService.getCountries();
            if(resultOf.isSuccess()) {
                List<AreaCountryRelation> areaCountryRelationList = resultOf.getData();
                JSONArray returnJson = new JSONArray();
                for(AreaCountryRelation areaCountryRelation : areaCountryRelationList) {
                    JSONObject areaJson = new JSONObject();
                    areaJson.put("label", areaCountryRelation.getArea().getArea());
                    JSONArray countryArray = new JSONArray();
                    List<Country> countryList = areaCountryRelation.getCountries();
                    for(Country country : countryList) {
                        JSONObject countryJson = new JSONObject();
                        countryJson.put("label", country.getLabel()+"（"+country.getRawVal()+"）");
                        countryJson.put("value", country.getUniqueIdentifier());
                        countryArray.add(countryJson);
                    }
                    areaJson.put("children", countryArray);
                    returnJson.add(areaJson);
                }
                return RPCResult.ok(returnJson);
            }
        }catch (Exception e) {
            log.error("getCountries error",e.getMessage() ,e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "getCountries error");
    }
}
