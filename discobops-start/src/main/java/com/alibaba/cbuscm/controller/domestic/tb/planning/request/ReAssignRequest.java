package com.alibaba.cbuscm.controller.domestic.tb.planning.request;

import java.util.List;

import com.alibaba.china.shared.discosupplier.model.common.UserModel;

import lombok.Data;
import lombok.ToString;

/**
 * 寻源重新指派处理人
 *
 * @author yaogaolin 2019-08-22 21:18
 */
@ToString
@Data
public class ReAssignRequest {

    /**
     * 寻源id
     */
    private Long sourceId;

    /**
     * 处理人
     */
    private List<UserModel> handlers;
}
