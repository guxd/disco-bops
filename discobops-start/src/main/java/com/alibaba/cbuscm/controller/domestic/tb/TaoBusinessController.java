/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.controller.domestic.tb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.cbu.disco.shared.business.tao.api.TaoProductReadService;
import com.alibaba.cbu.disco.shared.business.tao.model.TaoProductModel;
import com.alibaba.cbu.disco.shared.business.tao.param.TaoProductQueryParam;
import com.alibaba.cbu.disco.shared.business.tao.param.TaoProductQueryParam.DistinctFieldEnum;
import com.alibaba.cbu.disco.shared.business.tao.param.TaoProductQueryParam.OrderFieldEnum;
import com.alibaba.cbu.disco.shared.business.tao.param.TaoProductQueryParam.OrderModeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbuscm.datashare.DataShareFileService;
import com.alibaba.cbuscm.datashare.TempFileUtil;
import com.alibaba.cbuscm.enums.ChannelNameEnum;
import com.alibaba.cbuscm.enums.IndicationDuplicateRemoveEnum;
import com.alibaba.cbuscm.enums.IndicationSortFieldEnum;
import com.alibaba.cbuscm.enums.IndicationTopOriginEnum;
import com.alibaba.cbuscm.enums.OfferSellSpeedEnum;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.domestic.tb.IndicationExcelVO;
import com.alibaba.cbuscm.vo.domestic.tb.IndicationFilterVO;
import com.alibaba.cbuscm.vo.domestic.tb.IndicationOfferVO;
import com.alibaba.cbuscm.vo.domestic.tb.IndicationOfferVO.Channel;
import com.alibaba.cbuscm.vo.domestic.tb.IndicationOfferVO.ChannelOffer;
import com.alibaba.cbuscm.vo.domestic.tb.IndicationVO;
import com.alibaba.cbuscm.vo.domestic.tb.IndicationVO.Property;
import com.alibaba.china.global.business.library.common.ListOf;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.interfaces.search.SearchSameTbOfferSellInfoService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionBlacklistReadService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionBlacklistWriteService;
import com.alibaba.china.global.business.library.interfaces.selection.taoshop.TaoShopSelectionReadService;
import com.alibaba.china.global.business.library.interfaces.selection.taoshop.TaoShopSelectionWriteService;
import com.alibaba.china.global.business.library.interfaces.seller.SellerPropertyValueConfigService;
import com.alibaba.china.global.business.library.models.search.TbSellOfferInfo;
import com.alibaba.china.global.business.library.models.search.TbSellStatisticsModel;
import com.alibaba.china.global.business.library.models.selection.SelectionBlacklistModel;
import com.alibaba.china.global.business.library.models.selection.taoshop.TaoShopSelectionConfigModel;
import com.alibaba.china.global.business.library.models.selection.taoshop.TaoShopSelectionTaskModel;
import com.alibaba.china.global.business.library.params.selection.taoshop.TaoShopSelectionConfigCreateParam;
import com.alibaba.china.global.business.library.params.selection.taoshop.TaoShopSelectionConfigUpdateParam;
import com.alibaba.china.global.business.match.interfaces.C2BDataPicSearchService;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.security.spring.component.csrf.CsrfTokenModel;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.task.Paginator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类TaoBusinessRPCService.java的实现描述：淘大店指标相关
 *
 * @author jizhi.qy 2019年4月18日 上午10:56:12
 */
@RestController
@RequestMapping("/taoIndication")
public class TaoBusinessController {
    protected static final Logger LOG = LoggerFactory.getLogger("TaoBusinessRPCService");
    @Autowired
    private TaoShopSelectionWriteService taoShopSelectionWriteService;
    @Autowired
    private TaoShopSelectionReadService taoShopSelectionReadService;
    @Autowired
    private SellerPropertyValueConfigService sellerPropertyValueConfigService;
    @Autowired
    private TaoProductReadService taoProductReadService;
    @Autowired
    private C2BDataPicSearchService c2BDataPicSearchService;
    @Autowired
    private SelectionBlacklistWriteService selectionBlacklistWriteService;
    @Autowired
    private SelectionBlacklistReadService selectionBlacklistReadService;
    @Autowired
    private SearchSameTbOfferSellInfoService searchSameTbOfferSellInfoService;

    @Autowired
    private DataShareFileService dataShareFileService;

    // 商品详情页
    private static final String ITEM_DETAIL = "https://detail.taobao.com/item.htm?id=";

    // 淘宝logourl
    private static final String TAOBAO_LOGO_URL = "https://img.alicdn.com/tfs/TB1Ko0DSsbpK1RjSZFyXXX_qFXa-24-19.png";

    /**
     * 获取指标组的筛选排序方式
     *
     * @param request
     * @param context
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/getIndicationFilters")
    public RPCResult getIndicationFilters(HttpServletRequest request, Map<String, Object> context)
            throws IOException, ServletException {

        // 拼接VO
        IndicationFilterVO vo = new IndicationFilterVO();
        // 1. 排序方式
        Map<String, String> indicationSortFieldMap = IndicationSortFieldEnum.fullMap;
        vo.setSortBy(indicationSortFieldMap);
        // 2. 去重方式
        Map<String, String> indicationDuplicateRemoveMap = IndicationDuplicateRemoveEnum.fullMap;
        vo.setDuplicateRemoveType(indicationDuplicateRemoveMap);
        // 3. 原产地货源
        Map<String, String> indicationTopOriginMap = IndicationTopOriginEnum.fullMap;
        vo.setTopOrigin(indicationTopOriginMap);

        return RPCResult.ok(vo);
    }

    /**
     * 根据taskId获取指标组列表
     *
     * @param request
     * @param context
     * @param taskId  任务id
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/getIndicationsByTaskId")
    public RPCResult getIndicationsByTaskId(HttpServletRequest request, Map<String, Object> context,
                                            @RequestParam(name = "taskId") Long taskId)
            throws IOException, ServletException {

        if (null == taskId) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL_FORMAT);
        }

        List<IndicationVO> res = Lists.newArrayList();

        // 根据taskId获取对应指标组内容
        ResultOf<TaoShopSelectionTaskModel> taskRes = taoShopSelectionReadService.findTaskById(taskId);

        // 指标列表
        List<TaoShopSelectionConfigModel> indicationList = Lists.newArrayList();

        if (null != taskRes && taskRes.isSuccess() && null != taskRes.getData()) {
            TaoShopSelectionTaskModel taskModel = taskRes.getData();
            indicationList = taskModel.getIndexes();
        }

        if (CollectionUtils.isNotEmpty(indicationList)) {

            for (TaoShopSelectionConfigModel m : indicationList) {
                Long indicationId = m.getId();
                String imgUrl = m.getImageUrl();
                String indexName = m.getIndexName();
                String itemIndexes = m.getItemIndexes();
                String supplierIndexes = m.getSupplierIndexes();

                // key是groupName，value是拼接的stringbuffer
                Map<String, StringBuffer> appendMap = Maps.newHashMap();

                // 对每一个商品/商家维度指标进行拼接
                JSONObject itemIndexesJSON = JSONObject.parseObject(itemIndexes);

                if (null != itemIndexesJSON && itemIndexesJSON.containsKey("formValues")) {
                    List<JSONObject> itemList =
                            JSONArray.parseArray(itemIndexesJSON.getString("formValues"), JSONObject.class);


                    for (JSONObject j : itemList) {
                        // 如果没有父节点，说明自身已经是根节点
                        if (StringUtils.isBlank(j.getString("groupName"))) {
                            continue;
                        }

                        String groupName = j.getString("groupName");

                        StringBuffer sb = null;

                        if (appendMap.containsKey(groupName)) {
                            sb = appendMap.get(groupName);
                        }

                        if (null == sb) {
                            sb = new StringBuffer();
                        }

                        String name = j.getString("name");
                        if (StringUtils.isNotBlank(name) && !name.equals(groupName)) {
                            sb.append(name).append("：");
                        }

                        if (j.containsKey("values")) {
                            JSONArray values = j.getJSONArray("values");
                            if (null != values && !values.isEmpty()) {
                                for (Object value : values) {
                                    JSONObject valueJSON = (JSONObject) value;
                                    String valueText = valueJSON.getString("value");
                                    sb.append(valueText).append(" ");
                                    if (j.containsKey("unit")) {
                                        String unit = j.getString("unit");
                                        sb.append(unit);
                                    }
                                }
                                sb.append(" ");
                            }
                            appendMap.put(groupName, sb);
                        }
                    }

                    if (itemIndexesJSON.containsKey("category")) {
                        JSONObject category = JSONObject.parseObject(itemIndexesJSON.getString("category"));
                        if (null != category && category.containsKey("value")) {
                            appendMap.put("类目", new StringBuffer(category.getString("value")));
                        }
                    }
                }

                List<JSONObject> supplierList = JSONArray.parseArray(supplierIndexes, JSONObject.class);

                for (JSONObject j : supplierList) {
                    // 如果没有父节点，说明自身已经是根节点
                    if (StringUtils.isBlank(j.getString("groupName"))) {
                        continue;
                    }

                    String groupName = j.getString("groupName");

                    StringBuffer sb = null;

                    if (appendMap.containsKey(groupName)) {
                        sb = appendMap.get(groupName);
                    }

                    if (null == sb) {
                        sb = new StringBuffer();
                    }

                    String name = j.getString("name");
                    if (StringUtils.isNotBlank(name) && !name.equals(groupName)) {
                        sb.append(name).append("：");
                    }

                    if (j.containsKey("values")) {
                        JSONArray values = j.getJSONArray("values");
                        if (null != values && !values.isEmpty()) {
                            for (Object value : values) {
                                JSONObject valueJSON = (JSONObject) value;
                                String valueText = valueJSON.getString("value");
                                sb.append(valueText);
                                if (j.containsKey("unit")) {
                                    String unit = j.getString("unit");
                                    sb.append(unit);
                                }
                                if (j.containsKey("valueType")) {
                                    String valueType = j.getString("valueType");
                                    if ("GREATER_THAN".equals(valueType)) {
                                        sb.append("以上");
                                    }
                                }
                            }
                            sb.append(";");
                        }
                        appendMap.put(groupName, sb);
                    }
                }

                List<Property> properties = Lists.newArrayList();
                for (Entry<String, StringBuffer> entry : appendMap.entrySet()) {
                    if (null != entry.getValue()) {
                        Property p = new Property();
                        p.setName(entry.getKey());
                        p.setValue(entry.getValue().toString());
                        properties.add(p);
                    }
                }

                IndicationVO vo = new IndicationVO();
                vo.setProperties(properties);
                vo.setTaskId(taskId);
                vo.setIndicationId(indicationId);
                vo.setIndicationName(indexName);
                PageOf<TaoProductModel> pageModel =
                        getOfferModelListByIndication(m, null, null, null, null, null, 1, 10);
                if (null != pageModel) {
                    vo.setTotalCount(pageModel.getTotal());
                } else {
                    vo.setTotalCount(0);
                }
                res.add(vo);
            }
        }

        return RPCResult.ok(res);
    }

    /**
     * 根据指标组id获取指标详情
     *
     * @param request
     * @param context
     * @param indicationId
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/getIndicationByIndicationId")
    public RPCResult getIndicationByIndicationId(HttpServletRequest request, Map<String, Object> context,
                                                 @RequestParam(name = "indicationId", required = false) Long indicationId)
            throws IOException, ServletException {

        JSONObject supplierJSON = new JSONObject();

        try {
            String stdModel =
                    sellerPropertyValueConfigService.getTdSellerPropertyValueConfig();
            supplierJSON.put("formSchema", JSONArray.parseArray(stdModel));
        } catch (Exception e) {
            LOG.error("get seller property error", e.getMessage(), e);
        }

        if (null == indicationId) {
            IndicationVO vo = new IndicationVO();
            vo.setShopForm(StringUtils.chomp(StringEscapeUtils.unescapeHtml4(supplierJSON.toString())));
            // vo.setShopForm(supplierJSON);
            return RPCResult.ok(vo);
        }

        ResultOf<TaoShopSelectionConfigModel> configModelRes = null;

        try {
            configModelRes =
                    taoShopSelectionReadService.getTaskConfigById(indicationId);
        } catch (Exception e) {
            LOG.error("get task by id error", e.getMessage(), e);
        }

        // 没有获取到指标组
        if (null == configModelRes || !configModelRes.isSuccess() || null == configModelRes.getData()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "get indication by indicationId error, not found");
        }

        TaoShopSelectionConfigModel indexModel = configModelRes.getData();

        IndicationVO vo = new IndicationVO();
        vo.setIndicationId(indicationId);
        vo.setIndicationName(indexModel.getIndexName());
        vo.setSimilarLink(indexModel.getImageUrl());
        vo.setOfferForm(StringUtils.chomp(StringEscapeUtils.unescapeHtml4(indexModel.getItemIndexes())));

        // vo.setOfferForm(JSONObject.parseObject(indexModel.getItemIndexes()));

        if (null != indexModel.getSupplierIndexes()) {
            JSONArray supplierIndexes = JSONArray.parseArray(indexModel.getSupplierIndexes());
            supplierJSON.put("formValues", supplierIndexes);
        }

        vo.setShopForm(StringUtils.chomp(StringEscapeUtils.unescapeHtml4(supplierJSON.toString())));
        // vo.setShopForm(supplierJSON);
        return RPCResult.ok(vo);
    }

    /**
     * 创建指标组
     *
     * @param request
     * @param context
     * @param taskId      关联的taskId
     * @param shopForm    商家维度表单
     * @param offerForm   商品维度表单
     * @param similarLink 相似链接
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/createIndication")
    @CsrfTokenModel
    public RPCResult createIndication(HttpServletRequest request, Map<String, Object> context,
                                      @RequestParam(name = "taskId") Long taskId,
                                      @RequestParam(name = "shopForm") String shopForm,
                                      @RequestParam(name = "offerForm") String offerForm,
                                      @RequestParam(name = "similarLink", required = false) String similarLink)
            throws IOException, ServletException {

        if (null == taskId) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "task id can not be null");
        }

        TaoShopSelectionConfigCreateParam param = new TaoShopSelectionConfigCreateParam();
        param.setTaskId(taskId);
        param.setImageUrl(similarLink);
        param.setItemIndexes(StringUtils.chomp(offerForm));

        try {
            JSONObject shopFormJSON = (JSONObject) JSONObject.parse(shopForm);
            // 去除schema
            if (!shopFormJSON.containsKey("formValues")) {
                return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "formvalue is null");
            }

            String formValues = StringUtils.chomp(shopFormJSON.get("formValues").toString());
            param.setSupplierIndexes(formValues);
        } catch (Exception e) {
            LOG.error("paras json error", e.getMessage(), e);
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "formvalue is error");
        }

        String indexName = createIndexNameByTaskId(taskId);

        if (StringUtils.isBlank(indexName)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "TASK ID IS ERROR");
        }

        param.setIndexName(indexName);

        ResultOf<TaoShopSelectionConfigModel> createRes = taoShopSelectionWriteService.createConfig(param);

        if (null != createRes && createRes.isSuccess()) {
            return RPCResult.ok();
        }

        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, createRes.getErrorMessage());
    }

    /**
     * 修改指标组
     *
     * @param request
     * @param context
     * @param indicationId 指标组id
     * @param shopForm     商家维度表单
     * @param offerForm    商品维度表单
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/updateIndication")
    @CsrfTokenModel
    public RPCResult updateIndication(HttpServletRequest request, Map<String, Object> context,
                                      @RequestParam(name = "indicationId") Long indicationId,
                                      @RequestParam(name = "shopForm", required = false) String shopForm,
                                      @RequestParam(name = "offerForm", required = false) String offerForm,
                                      @RequestParam(name = "similarLink", required = false) String similarLink)
            throws IOException, ServletException {

        if (null == indicationId) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "indication id can not be null");
        }

        TaoShopSelectionConfigUpdateParam param = new TaoShopSelectionConfigUpdateParam();

        param.setId(indicationId);

        // 同款字段
        if (StringUtils.isNotBlank(similarLink)) {
            param.setImageUrl(similarLink);
        }

        // 商品字段
        if (StringUtils.isNotBlank(offerForm)) {
            param.setItemIndexes(StringUtils.chomp(offerForm));
        }

        // 商家字段
        if (StringUtils.isNotBlank(shopForm)) {
            try {
                JSONObject shopFormJSON = (JSONObject) JSONObject.parse(shopForm);
                // 去除schema
                if (!shopFormJSON.containsKey("formValues")) {
                    return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "formvalue is null");
                }
                String formValues = StringUtils.chomp(shopFormJSON.get("formValues").toString());
                param.setSupplierIndexes(formValues);
            } catch (Exception e) {
                LOG.error("parse indication json error", e.getMessage(), e);
                return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "formvalue is error");
            }
        }

        String errMsg = "";

        try {
            ResultOf<TaoShopSelectionConfigModel> updateRes = taoShopSelectionWriteService.updateConfig(param);
            if (null != updateRes && updateRes.isSuccess()) {
                return RPCResult.ok();
            }
            errMsg = updateRes.getErrorMessage();
        } catch (Exception e) {
            LOG.error("update selection error", e.getMessage(), e);
            errMsg = RPCResult.MSG_ERROR_SYSTEM;
        }

        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, errMsg);
    }

    /**
     * 删除指标组
     *
     * @param request
     * @param context
     * @param indicationId
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/deleteIndication")
    public RPCResult deleteIndication(HttpServletRequest request, Map<String, Object> context,
                                      @RequestParam(name = "indicationId") Long indicationId)
            throws IOException, ServletException {

        if (null == indicationId) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "indication id can not be null");
        }

        String errMsg = "";

        try {
            ResultOf<Boolean> deleteRes = taoShopSelectionWriteService.deleteConfig(indicationId);

            if (null != deleteRes && deleteRes.isSuccess()) {
                return RPCResult.ok();
            }
            errMsg = deleteRes.getErrorMessage();
        } catch (Exception e) {
            LOG.error("delete selection error", e.getMessage(), e);
            errMsg = RPCResult.MSG_ERROR_SYSTEM;
        }

        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, errMsg);
    }

    /**
     * 在指标组删除offer
     *
     * @param request
     * @param context
     * @param response
     * @param indicationId
     * @param offerId
     * @return
     */
    @RequestMapping("/deleteOffer")
    public RPCResult deleteOffer(HttpServletRequest request, Map<String, Object> context,
                                 HttpServletResponse response,
                                 @RequestParam(name = "indicationId") Long indicationId,
                                 @RequestParam(name = "offerId") Long offerId) {
        ResultOf<Boolean> addRes = selectionBlacklistWriteService.addItem(indicationId, offerId);

        if (null != addRes && addRes.isSuccess()) {
            return RPCResult.ok();
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, RPCResult.MSG_ERROR_SYSTEM);

    }

    /**
     * 下载指标组结果（传入任务id表示全量，传入indicationId表示仅一个）
     *
     * @param request
     * @param taskId
     * @param indicationId
     * @param topOriginStr
     * @param sortByStr
     * @param duplicateRemoveTypeStr
     * @param keyword
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/downloadIndications")
    public RPCResult downloadIndications(HttpServletRequest request,
        @RequestParam(name = "taskId", required = false) Long taskId,
        @RequestParam(name = "indicationId", required = false) Long indicationId,
        @RequestParam(name = "topOrigin", required = false) String topOriginStr,
        @RequestParam(name = "sortBy", required = false) String sortByStr,
        @RequestParam(name = "duplicateRemoveType", required = false) String duplicateRemoveTypeStr,
        @RequestParam(name = "keyword", required = false) String keyword,
        @RequestParam(name = "pageSize", required = false) Integer pageSize) throws IOException, ServletException {
        if (null == taskId && null == indicationId) {
            return RPCResult.error("param error","");
        }

        if (null == pageSize || pageSize > 5000) {
            pageSize = 5000;
        }

        Long itemId = null;

        // 强行转关键词，如果是数字，以offer处理
        if (StringUtils.isNumeric(keyword)) {
            itemId = Long.parseLong(keyword);
            keyword = null;
        }

        OutputStream outputStream = null;
        
        //ServletOutputStream out = null;
        String filePath = null;
        try {
            filePath = TempFileUtil.createTempXlsxFile("商品企划");
//            out = response.getOutputStream();
//            response.setContentType("multipart/form-data");
//            response.setCharacterEncoding("utf-8");
//            response.setHeader("Content-disposition",
//                "attachment;filename=" + URLEncoder.encode("商品企划", "UTF-8") + ".xlsx");

            outputStream = new FileOutputStream(filePath);

            ExcelWriter writer = EasyExcelFactory.getWriter(outputStream, ExcelTypeEnum.XLSX, true);

            List<IndicationExcelVO> data = new ArrayList<IndicationExcelVO>();

            if (null != indicationId) {
                Sheet sheet = new Sheet(1, 0, IndicationExcelVO.class);

                // 获取100条数据
                ResultOf<TaoShopSelectionConfigModel> configModelRes =
                        taoShopSelectionReadService.getTaskConfigById(indicationId);
                if (null != configModelRes && configModelRes.isSuccess() && null != configModelRes.getData()) {
                    PageOf<TaoProductModel> offerList = null;
                    try {
                        offerList =
                                this.getOfferModelListByIndication(configModelRes.getData(), topOriginStr, sortByStr,
                                        duplicateRemoveTypeStr,
                                        keyword, itemId, 1, pageSize);
                    } catch (Exception e) {
                        LOG.error("get selection result  error", e.getMessage(), e);
                    }

                    if (null != offerList && offerList.isSuccess() && CollectionUtils.isNotEmpty(offerList.getData())) {
                        assembleIndicationExcelVO(offerList.getData(), data);
                    }
                    sheet.setSheetName(configModelRes.getData().getIndexName());
                }

                writer.write(data, sheet);
            } else {
                // 根据task模型获取指标组
                ResultOf<TaoShopSelectionTaskModel> taskRes = taoShopSelectionReadService.findTaskById(taskId);
                if (null == taskRes || !taskRes.isSuccess() || null == taskRes.getData()
                    || CollectionUtils.isEmpty(taskRes.getData().getIndexes())) {
                    return RPCResult.ok();
                }

                // 指标组
                List<TaoShopSelectionConfigModel> indexes = taskRes.getData().getIndexes();

                int sheetNum = 1;

                for (TaoShopSelectionConfigModel index : indexes) {
                    Sheet sheet = new Sheet(sheetNum, 0, IndicationExcelVO.class);

                    PageOf<TaoProductModel> offerList = null;
                    try {
                        offerList =
                                this.getOfferModelListByIndication(index, topOriginStr, sortByStr, duplicateRemoveTypeStr,
                                        keyword, null, 1, pageSize);
                    } catch (Exception e) {
                        LOG.error("get selection result  error", e.getMessage(), e);
                    }

                    if (null != offerList && offerList.isSuccess() && CollectionUtils.isNotEmpty(offerList.getData())) {
                        assembleIndicationExcelVO(offerList.getData(), data);
                    }

                    sheet.setSheetName(index.getIndexName());

                    writer.write(data, sheet);
                    sheetNum += 1;
                    data = new ArrayList<IndicationExcelVO>();
                }
            }
            /**
             * IndicationExcelVO v = new IndicationExcelVO();
             * List<Object> channelOfferList = Lists.newArrayList();
             * ChannelOffer o = new ChannelOffer();
             * o.setLink("123");
             * o.setSales("230");
             * o.setPrice("01239");
             * channelOfferList.add(o);
             * JSONArray a = new JSONArray(channelOfferList);
             * v.setChannelOfferList(a.toJSONString());
             * v.setHasDepthInspection(true);
             * v.setIsTp(true);
             * v.setItemId(123123L);
             * v.setOnlineYear("30");
             * v.setPayment7Tao("100");
             * v.setPlace("hz");
             * v.setPrice("100");
             * v.setPrice7Tao("1000");
             * v.setSales7Tao("100");
             * v.setTitle("1ddd");
             * data.add(v);
             **/
            // writer.write(data, sheet1);
            writer.finish();
            dataShareFileService.uploadFile("商品企划",filePath,Lists.newArrayList(LoginUtil.getEmpId(request)));

        } catch (Exception e) {
            LOG.error("format selection result excel error", e.getMessage(), e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    LOG.error("output excel error",e.getMessage(), e);
                }
            }
            File file = new File(filePath);
            if(file.exists()){
                file.delete();
            }
        }
        return RPCResult.ok();
    }

    /**
     * 根据1688商品id获取下游数据
     *
     * @return
     */
    @RequestMapping("/getDownstreamDataByOfferId")
    public RPCResult getDownstreamDataByOfferId(HttpServletRequest request, Map<String, Object> context,
                                                @RequestParam(name = "offerId", required = false) Long offerId) {
        if (null == offerId) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }

        List<Channel> channelList = Lists.newArrayList();

        TbSellStatisticsModel tbSellStatistics = null;

        List<TbSellOfferInfo> tbSellOfferList = null;

        // 获取下游同款统计数据以及offer列表
        try {
            tbSellStatistics =
                    searchSameTbOfferSellInfoService.get1688SameTbOfferStatsticsInfo(offerId);

            tbSellOfferList =
                    searchSameTbOfferSellInfoService.search1688SameOfferList(offerId, 0, 10);
        } catch (Exception e) {
            LOG.error("get tb same offer error", e.getMessage(), e);
        }

        if (null == tbSellStatistics) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "search tb sellstatistics or tbsellofferlist error");
        }

        Channel channel = new Channel();
        channel.setLogoUrl(TAOBAO_LOGO_URL);
        channel.setName(ChannelNameEnum.TB.getValue());

        channel.setPayment7(assembleDoubleFormat(tbSellStatistics.getAvg7DBuyerNum()));

        channel.setPrice7(assembleDoubleFormat(tbSellStatistics.getAvg7DSellPrice()));

        channel.setSales7(assembleDoubleFormat(tbSellStatistics.getAve7DSellNum()));

        Double speed7 = tbSellStatistics.getSellIncrease();
        if (null != speed7) {
            if (speed7 > 0.5) {
                channel.setSpeed7(OfferSellSpeedEnum.HIGH.getValue());
            } else if (speed7 > 0.2) {
                channel.setSpeed7(OfferSellSpeedEnum.MID.getValue());
            } else {
                channel.setSpeed7(OfferSellSpeedEnum.LOW.getValue());
            }
        } else {
            channel.setSpeed7("--");
        }

        if (CollectionUtils.isNotEmpty(tbSellOfferList)) {
            List<ChannelOffer> channelOfferList = Lists.newArrayList();

            for (TbSellOfferInfo tbOfferInfo : tbSellOfferList) {
                ChannelOffer offer = new ChannelOffer();
                offer.setImgUrl(tbOfferInfo.getPictUrl());
                offer.setImgUrl64x64(tbOfferInfo.getPictUrl() + "_64x64q90.jpg");
                offer.setLink(ITEM_DETAIL + tbOfferInfo.getOfferId());

                if (null != tbOfferInfo.getPrice()) {
                    offer.setPrice(String.valueOf(tbOfferInfo.getPrice()));
                }
                offer.setSales(String.valueOf(tbOfferInfo.getNum()));
                channelOfferList.add(offer);
            }
            channel.setChannelOfferList(channelOfferList);
        }

        channelList.add(channel);

        return RPCResult.ok(channelList);
    }

    /**
     * 根据指标组查询商品
     *
     * @param request
     * @param context
     * @param indicationId           指标组id
     * @param topOriginStr           top产地
     * @param sortByStr              排序方式
     * @param duplicateRemoveTypeStr 去重方式
     * @param keyword                关键词
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @RequestMapping("/getOfferListByIndicationId")
    public RPCResult getOfferListByIndicationId(HttpServletRequest request, Map<String, Object> context,
                                                @RequestParam(name = "indicationId") Long indicationId, @RequestParam(name = "topOrigin", required = false) String topOriginStr,
                                                @RequestParam(name = "sortBy", required = false) String sortByStr, @RequestParam(name = "duplicateRemoveType", required = false) String duplicateRemoveTypeStr,
                                                @RequestParam(name = "keyword", required = false) String keyword, @RequestParam(name = "page", required = false) Integer page,
                                                @RequestParam(name = "pageSize", required = false) Integer pageSize) throws IOException, ServletException {

        Long itemId = null;

        // 强行转关键词，如果是数字，以offer处理
        if (StringUtils.isNumeric(keyword)) {
            itemId = Long.parseLong(keyword);
            keyword = null;
        }

        PageOf<IndicationOfferVO> offerList =
                this.getOfferListByIndicationId(indicationId, topOriginStr, sortByStr, duplicateRemoveTypeStr, keyword,
                        itemId, page, pageSize);

        Paginator paginator = null;
        RPCResult result = RPCResult.ok();

        if (null != offerList && CollectionUtils.isNotEmpty(offerList.getData())) {
            paginator = new Paginator(offerList.getTotal(), offerList.getPageSize());
            paginator.setPage(page);

            List<Long> itemIds = Lists.newArrayList();

            for (IndicationOfferVO vo : offerList.getData()) {
                itemIds.add(vo.getOfferId());
            }

            if (CollectionUtils.isNotEmpty(itemIds)) {
                ListOf<SelectionBlacklistModel> blackModelList =
                        selectionBlacklistReadService.findByTaskItems(indicationId, itemIds);
                if (null != blackModelList && CollectionUtils.isNotEmpty(blackModelList.getData())) {
                    Set<Long> itemSet = Sets.newHashSet();
                    for (SelectionBlacklistModel model : blackModelList.getData()) {
                        itemSet.add(model.getTargetId());
                    }

                    if (!itemSet.isEmpty()) {
                        Iterator<IndicationOfferVO> it = offerList.getData().iterator();
                        while (it.hasNext()) {
                            IndicationOfferVO x = it.next();
                            if (itemSet.contains(x.getOfferId())) {
                                it.remove();
                            }
                        }

                    }
                }
            }
            result.put("data", offerList.getData());
        } else {
            paginator = new Paginator(0, 10);
            paginator.setPage(1);
            result.put("data", Lists.newArrayList());
        }

        result.put("paginator", paginator);

        return result;
    }

    /**
     * 根据指标情况，实时获取数量
     *
     * @param request
     * @param context
     * @param shopForm
     * @param offerForm
     * @param similarLink
     * @return
     */
    @RequestMapping("/getOfferCountByIndication")
    @CsrfTokenModel
    public RPCResult getOfferCountByIndication(HttpServletRequest request, Map<String, Object> context,
                                               @RequestParam(name = "shopForm", required = false) String shopForm,
                                               @RequestParam(name = "offerForm", required = false) String offerForm,
                                               @RequestParam(name = "similarLink", required = false) String similarLink) {

        Integer page = 1;
        Integer pageSize = 10;

        TaoShopSelectionConfigModel configModel = new TaoShopSelectionConfigModel();
        configModel.setImageUrl(similarLink);
        configModel.setItemIndexes(offerForm);

        if (StringUtils.isNotBlank(shopForm)) {
            try {
                JSONObject shopFormJSON = (JSONObject) JSONObject.parse(shopForm);
                // 去除schema
                if (shopFormJSON.containsKey("formValues")) {
                    String formValues = StringUtils.chomp(shopFormJSON.get("formValues").toString());
                    configModel.setSupplierIndexes(formValues);
                }
            } catch (Exception e) {
                LOG.error("get indication count error", e.getMessage(), e);
                return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "formvalue is error");
            }
        }

        PageOf<TaoProductModel> offerList = null;

        try {
            offerList =
                    this.getOfferModelListByIndication(configModel, null, null, null,
                            null, null, page, pageSize);
        } catch (Exception e) {
            LOG.error("get indication offer list error", e.getMessage(), e);
        }

        if (null != offerList && offerList.isSuccess()) {
            Integer count = offerList.getTotal();
            return RPCResult.ok(count);
        }

        return RPCResult.ok(0);
    }

    /**
     * 组装过滤查询参数
     *
     * @param param
     * @param topOriginStr
     * @param sortByStr
     * @param duplicateRemoveTypeStr
     * @param keyword
     * @param page
     * @param pageSize
     */
    private void assembleFilterQueryParam(TaoProductQueryParam param, String topOriginStr, String sortByStr,
                                          String duplicateRemoveTypeStr, String keyword, Integer page, Integer pageSize) {
        // 关键词
        if (StringUtils.isNotBlank(keyword)) {
            param.setKeyword(keyword);
        }

        // 页数
        if (null != page) {
            param.setPageNo(page);
        } else {
            param.setPageNo(1);
        }

        // 每页数量
        if (null != pageSize) {
            param.setPageSize(pageSize);
        } else {
            param.setPageSize(10);
        }

        // 排序方式
        // 默认30天销量降序
        param.setOrderMode(OrderModeEnum.DESC.name());
        param.setOrderField(OrderFieldEnum.payOrdAmt30d.name());

        if (StringUtils.isNotBlank(sortByStr)) {
            IndicationSortFieldEnum sortBy = IndicationSortFieldEnum.parseByValue(sortByStr);

            if (null != sortBy) {
                if (sortBy.equals(IndicationSortFieldEnum.PRICE_ASC)) {
                    // 价格升序
                    param.setOrderMode(OrderModeEnum.ASC.name());
                    param.setOrderField(OrderFieldEnum.reservePrice.name());
                } else if (sortBy.equals(IndicationSortFieldEnum.PRICE_DESC)) {
                    // 价格降序
                    param.setOrderMode(OrderModeEnum.DESC.name());
                    param.setOrderField(OrderFieldEnum.reservePrice.name());
                } else if (sortBy.equals(IndicationSortFieldEnum.SELL_AMOUNT_7DAYS_1688_DESC)) {
                    // 7天销量降序
                    param.setOrderMode(OrderModeEnum.DESC.name());
                    param.setOrderField(OrderFieldEnum.payOrdAmt7d.name());
                } else if (sortBy.equals(IndicationSortFieldEnum.SELL_AMOUNT_30DAYS_1688_DESC)) {
                    // 30天销量降序
                    param.setOrderMode(OrderModeEnum.DESC.name());
                    param.setOrderField(OrderFieldEnum.payOrdAmt30d.name());
                } else if (sortBy.equals(IndicationSortFieldEnum.BUYER_AMOUNT_7DAYS_1688_DESC)) {
                    // 7天买家数
                    param.setOrderMode(OrderModeEnum.DESC.name());
                    param.setOrderField(OrderFieldEnum.payOrdByrCnt7d.name());
                } else if (sortBy.equals(IndicationSortFieldEnum.BUYER_AMOUNT_30DAYS_1688_DESC)) {
                    // 30天买家数
                    param.setOrderMode(OrderModeEnum.DESC.name());
                    param.setOrderField(OrderFieldEnum.payOrdByrCnt30d.name());
                }

            }
        }

        // 去重方式
        if (StringUtils.isNotBlank(duplicateRemoveTypeStr)) {
            IndicationDuplicateRemoveEnum duplicateRemove =
                    IndicationDuplicateRemoveEnum.parseByValue(duplicateRemoveTypeStr);
            if (null != duplicateRemove) {
                if (duplicateRemove.equals(IndicationDuplicateRemoveEnum.SELLER_1_ITEM_1)) {
                    // 一商1品
                    param.setDistinctField(DistinctFieldEnum.memberId.name());
                    param.setDistinctCnt(1);
                } else if (duplicateRemove.equals(IndicationDuplicateRemoveEnum.SELLER_1_ITEM_2)) {
                    // 一商2品
                    param.setDistinctField(DistinctFieldEnum.memberId.name());
                    param.setDistinctCnt(2);
                } else if (duplicateRemove.equals(IndicationDuplicateRemoveEnum.SELLER_1_ITEM_3)) {
                    // 一商3品
                    param.setDistinctField(DistinctFieldEnum.memberId.name());
                    param.setDistinctCnt(3);
                } else if (duplicateRemove.equals(IndicationDuplicateRemoveEnum.SELLER_1_ITEM_5)) {
                    // 一商5品
                    param.setDistinctField(DistinctFieldEnum.memberId.name());
                    param.setDistinctCnt(5);
                }
            }
        }

        // top产地
        if (StringUtils.isNotBlank(topOriginStr)) {
            IndicationTopOriginEnum topOrigin = IndicationTopOriginEnum.parseByValue(topOriginStr);
            if (null != topOrigin) {
                if (topOrigin.equals(IndicationTopOriginEnum.TOP_ORIGIN_1)) {
                    param.setBeltTopRanker(0L);
                } else if (topOrigin.equals(IndicationTopOriginEnum.TOP_ORIGIN_3)) {
                    param.setBeltTopRanker(3L);
                } else if (topOrigin.equals(IndicationTopOriginEnum.TOP_ORIGIN_5)) {
                    param.setBeltTopRanker(5L);
                }
            }
        }
    }

    /**
     * 商家维度参数组装
     *
     * @param param
     * @param supplierIndexes
     */
    private void assembleSupplierQueryParam(TaoProductQueryParam param, String supplierIndexes) {
        if (StringUtils.isBlank(supplierIndexes)) {
            return;
        }

        List<JSONObject> formValues = JSONArray.parseArray(supplierIndexes, JSONObject.class);

        for (int i = 0; i < formValues.size(); i++) {
            JSONObject formValueJSON = formValues.get(i);

            if (!formValueJSON.containsKey("fid") || !formValueJSON.containsKey("values")) {
                continue;
            }

            String fid = formValueJSON.getString("fid");

            if ("biz_type".equals(fid)) {
                // 企业类型
                String companyType = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(companyType)) {
                    param.setEnterpriseType(companyType);
                }
            } else if ("is_factory".equals(fid)) {
                // 是否工厂
                String isFactory = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(isFactory)) {
                    if ("Y".equals(isFactory)) {
                        param.setIsFactory(true);
                    } else if ("N".equals(isFactory)) {
                        param.setIsFactory(false);
                    }
                }
            } else if ("registerd_capital".equals(fid)) {
                // 注册资金
                String registeredCapitalStr = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(registeredCapitalStr)) {
                    try {
                        param.setRegisteredCapital(Long.parseLong(registeredCapitalStr));
                    } catch (Exception e) {
                        LOG.error("get seller registered capital error", e.getMessage(), e);
                    }
                }
            } else if ("is_vat_invoice".equals(fid)) {
                // 增值税发票
                String invoice = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(invoice)) {
                    if ("Y".equals(invoice)) {
                        param.setIsVatInvoice(true);
                    } else if ("N".equals(invoice)) {
                        param.setIsVatInvoice(false);
                    }
                }
            } else if ("brand_infos".equals(fid)) {
                // 自有品牌
                String brand = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(brand) && "Y".equals(brand)) {
                    try {
                        param.setIsOwnBrand(true);
                    } catch (Exception e) {
                        LOG.error("get seller brand error", e.getMessage(), e);
                    }
                }
            } else if ("business_age".equals(fid)) {
                // 经营年限
                String businessAge = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(businessAge)) {
                    try {
                        param.setBusinessAge(Long.parseLong(businessAge));
                    } catch (Exception e) {
                        LOG.error("get seller business age error", e.getMessage(), e);
                    }
                }
            } else if ("emp_cnt".equals(fid)) {
                // 工厂人数
                String empCntStr = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(empCntStr)) {
                    try {
                        param.setEmpCnt(Long.parseLong(empCntStr));
                    } catch (Exception e) {
                        LOG.error("get seller employee count error", e.getMessage(), e);
                    }
                }
            } else if ("factory_acreage".equals(fid)) {
                // 厂房面积
                String factoryAcreageStr = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(factoryAcreageStr)) {
                    try {
                        param.setFactoryAcreage(Long.parseLong(factoryAcreageStr));
                    } catch (Exception e) {
                        LOG.error("get seller factory acreage error", e.getMessage(), e);
                    }
                }
            } else if ("is_pm".equals(fid)) {
                // 实力商家
                String isPm = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(isPm)) {
                    if ("Y".equals(isPm)) {
                        param.setIsPm(true);
                    } else if ("N".equals(isPm)) {
                        param.setIsPm(false);
                    }
                }
            } else if ("is_deep_auth_fac".equals(fid)) {
                // 深度验厂
                String deepAuth = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(deepAuth)) {
                    if ("Y".equals(deepAuth)) {
                        param.setIsDeepAuthFac(true);
                    } else if ("N".equals(deepAuth)) {
                        param.setIsDeepAuthFac(false);
                    }
                }
            } else if ("trade_level".equals(fid)) {
                // 勋章等级
                String level = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(level)) {
                    try {
                        param.setTradeLevel(Long.parseLong(level));
                    } catch (Exception e) {
                        LOG.error("get seller trade level error", e.getMessage(), e);
                    }
                }
            } else if ("is_yjdf".equals(fid)) {
                // 一件代发
                String yjdf = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(yjdf)) {
                    if ("Y".equals(yjdf)) {
                        param.setIsYJDF(true);
                    } else if ("N".equals(yjdf)) {
                        param.setIsYJDF(false);
                    }
                }
            } else if ("fst_ons_on_itm_cnt_1y_001".equals(fid)) {
                // 年发布offer数量
                String offerCount = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(offerCount)) {
                    try {
                        param.setFstOnsOnItmCnt(Long.parseLong(offerCount));
                    } catch (Exception e) {
                        LOG.error("get seller offer count of 1 year", e.getMessage(), e);
                    }
                }
            } else if ("pay_ord_itm_cnt_1y_001".equals(fid)) {
                // 年成交商品数
                String orderCount = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(orderCount)) {
                    try {
                        param.setPayOrdItmCnt(Long.parseLong(orderCount));
                    } catch (Exception e) {
                        LOG.error("get seller pay order offer count of 1 year", e.getMessage(), e);
                    }
                }
            } else if ("pay_ord_itm_cnt_1y".equals(fid)) {
                // 爆款offer销量
                String boomOrdCount = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(boomOrdCount)) {
                    if ("pay_ord_itm_cnt_1y_1w".equals(boomOrdCount)) {
                        param.setHotSaleType(1L);
                    } else if ("pay_ord_itm_cnt_1y_10w".equals(boomOrdCount)) {
                        param.setHotSaleType(10L);
                    } else if ("pay_ord_itm_cnt_1y_50w".equals(boomOrdCount)) {
                        param.setHotSaleType(50L);
                    } else if ("pay_ord_itm_cnt_1y_100w".equals(boomOrdCount)) {
                        param.setHotSaleType(100L);
                    }
                }
            } else if ("pay_ord_itm_cnt_1y_num".equals(fid)) {
                // 爆款offer数量
                String boomOfferCount = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(boomOfferCount)) {
                    param.setHotSaleCnt(Long.parseLong(boomOfferCount));
                }
            } else if ("price_level".equals(fid)) {
                // 价格带（平价、中档、高档、精品还有空字符串）
                String priceLevel = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(priceLevel)) {
                    param.setBeltPriceLevel(priceLevel);
                }
            } else if ("supply_credit".equals(fid)) {
                //供应商信用
                String supplyCredit = getShopFormValueStr(formValueJSON);
                if (StringUtils.isNotBlank(supplyCredit)) {
                    try {
                        TaoProductQueryParam.SupplyCreditLevelEnum creditLevelEnums = TaoProductQueryParam.SupplyCreditLevelEnum.valueOf(supplyCredit);
                        param.setSupplyCreditLevel(creditLevelEnums);
                    }catch(Exception e){
                        //ignore
                    }
                }
            }

        }

    }

    /**
     * 商品维度参数组装
     *
     * @param param
     * @param itemIndexes
     */
    private void assembleItemQueryParam(TaoProductQueryParam param, String itemIndexes) {
        if (StringUtils.isBlank(itemIndexes)) {
            return;
        }

        JSONObject itemJSON = null;
        try {
            itemJSON = JSONObject.parseObject(itemIndexes);
        } catch (Exception e) {
            LOG.error("prase to TaoProductQueryParam json error", e.getMessage(), e);
        }

        if (null == itemJSON) {
            return;
        }

        // 组装类目
        if (itemJSON.containsKey("category")) {
            try {
                JSONObject categoryJSON = itemJSON.getJSONObject("category");
                Long categoryId = Long.parseLong(categoryJSON.getString("id"));
                // 类目id
                param.setCateId(categoryId);
            } catch (Exception e) {
                LOG.error("prase to TaoProductQueryParam category json error", e.getMessage(), e);
            }
        }

        // 组装offer参数
        if (itemJSON.containsKey("formValues")) {
            JSONArray formValues = itemJSON.getJSONArray("formValues");
            Map<String, List<String>> properties = Maps.newHashMap();

            for (int i = 0; i < formValues.size(); i++) {
                JSONObject formValue = formValues.getJSONObject(i);

                if (!formValue.containsKey("parentFid") || StringUtils.isBlank(formValue.getString("parentFid"))
                        || !formValue.containsKey("fid") || !formValue.containsKey("values")) {
                    continue;
                }

                String name = formValue.getString("name");
                JSONArray values = formValue.getJSONArray("values");

                List<String> valueList = Lists.newArrayList();

                for (int j = 0; j < values.size(); j++) {
                    String value = values.getJSONObject(j).getString("value");
                    valueList.add(value);
                }

                if (CollectionUtils.isNotEmpty(valueList)) {
                    properties.put(name, valueList);
                }
            }
            param.setProperties(properties);
        }

    }

    /**
     * 根据taskId给新建的指标组取名
     *
     * @param taskId
     * @return
     */
    private String createIndexNameByTaskId(Long taskId) {

        ResultOf<TaoShopSelectionTaskModel> queryRes = null;

        try {
            queryRes = taoShopSelectionReadService.findTaskById(taskId);
        } catch (Exception e) {
            LOG.error("find selection task by id error", e.getMessage(), e);
        }

        if (null != queryRes && queryRes.isSuccess() && null != queryRes.getData()) {
            // 取名
            List<TaoShopSelectionConfigModel> indicationList = queryRes.getData().getIndexes();
            if (CollectionUtils.isNotEmpty(indicationList)) {
                String name = indicationList.get(indicationList.size() - 1).getIndexName();
                try {
                    if (name.startsWith("指标组")) {
                        name = name.substring(3, name.length());
                        Long count = Long.parseLong(name);
                        count += 1;
                        return "指标组" + count;
                    }
                } catch (Exception e) {
                    LOG.error("prase selection number error", e.getMessage(), e);
                    return "指标组";
                }
            } else {
                return "指标组1";
            }
        }

        return null;
    }

    /**
     * 根据表单数据json查询value单值
     *
     * @param formValueJSON
     * @return
     */
    private String getShopFormValueStr(JSONObject formValueJSON) {
        JSONArray values = formValueJSON.getJSONArray("values");
        if (values.size() <= 0) {
            return null;
        }

        if (!values.getJSONObject(0).containsKey("vid")) {
            return null;
        }
        return values.getJSONObject(0).getString("vid");
    }


    /**
     * 根据指标组实体获取全部指标组的offerList
     *
     * @param topOriginStr
     * @param sortByStr
     * @param duplicateRemoveTypeStr
     * @param keyword
     * @param page
     * @param pageSize
     * @return
     */
    private PageOf<IndicationOfferVO> getOfferListByIndication(TaoShopSelectionConfigModel index, String topOriginStr,
                                                               String sortByStr, String duplicateRemoveTypeStr, String keyword, Long itemId, Integer page, Integer pageSize) {

        PageOf<TaoProductModel> seachRes = getOfferModelListByIndication(index, topOriginStr,
                sortByStr, duplicateRemoveTypeStr, keyword, itemId, page, pageSize);

        if (null == seachRes || CollectionUtils.isEmpty(seachRes.getData())) {
            PageOf<IndicationOfferVO> res = new PageOf<IndicationOfferVO>();
            res.setData(Lists.newArrayList());
            res.setPageNo(page);
            res.setPageSize(pageSize);
            res.setTotal(0);
            return res;
        }

        List<TaoProductModel> modelList = seachRes.getData();

        List<IndicationOfferVO> voList = Lists.newArrayList();

        List<Long> itemIdList = Lists.newArrayList();

        for (TaoProductModel model : modelList) {
            itemIdList.add(model.getItemId());
        }

        for (TaoProductModel model : modelList) {
            IndicationOfferVO offerVO = new IndicationOfferVO();
            // itemId
            offerVO.setOfferId(model.getItemId());
            // 图片链接
            offerVO.setImgUrl220(model.getMainImg());
            // title
            offerVO.setTitle(model.getTitle());
            // 价格
            if (null != model.getReservePrice()) {
                DecimalFormat df = new DecimalFormat("0.00");
                try {
                    offerVO.setPrice(df.format(model.getReservePrice().doubleValue() / 100));
                } catch (Exception e) {
                    LOG.error("get offer price error!", e.getMessage(), e);
                }
            }
            // 公司名称
            offerVO.setBusName(model.getCompanyName());
            // 产地
            offerVO.setPlace(model.getBeltPlace());
            // 深度验厂
            offerVO.setHasDepthInspection(model.getIsDeepAuthFac());
            // 诚信通
            offerVO.setHasTP(model.getIsTp());
            // 在线年份
            offerVO.setOnlineYear(String.valueOf(model.getTpYear()));
            // 一件代发
            offerVO.setHasOnePush(model.getIsYJDF());
            // 旺铺链接
            offerVO.setWinportDomain(model.getWinportDomain());
            // 诚信通年份
            offerVO.setTpYear(String.valueOf(model.getTpYear()));

            voList.add(offerVO);
        }

        PageOf<IndicationOfferVO> res = new PageOf<IndicationOfferVO>();
        res.setData(voList);
        res.setPageNo(seachRes.getPageNo());
        res.setPageSize(seachRes.getPageSize());
        res.setTotal(seachRes.getTotal());
        return res;
    }

    /**
     * 获取h3搜索的offer列表（未封装）
     *
     * @param index
     * @param topOriginStr
     * @param sortByStr
     * @param duplicateRemoveTypeStr
     * @param keyword
     * @param itemId
     * @param page
     * @param pageSize
     * @return
     */
    private PageOf<TaoProductModel> getOfferModelListByIndication(TaoShopSelectionConfigModel index,
                                                                  String topOriginStr,
                                                                  String sortByStr, String duplicateRemoveTypeStr, String keyword, Long itemId, Integer page, Integer pageSize) {
        TaoProductQueryParam param = new TaoProductQueryParam();

        if (null == index) {
            return null;
        }

        // 图搜
        if (StringUtils.isNotBlank(index.getImageUrl())) {
            String imgUrl = index.getImageUrl();
            try {
                com.alibaba.china.global.business.common.ResultOf<List<Long>> offerIdRes =
                        c2BDataPicSearchService.picSearch(imgUrl, 50);
                if (null != offerIdRes && offerIdRes.isSuccess() && CollectionUtils.isNotEmpty(offerIdRes.getData())) {
                    List<Long> offerIdList = offerIdRes.getData();
                    param.setItemIds(offerIdList);
                }
            } catch (Exception e) {
                LOG.error("get offer list by picture error", e.getMessage(), e);
            }
        }

        // 结果筛选的offerId优先
        if (null != itemId) {
            if (CollectionUtils.isNotEmpty(param.getItemIds())) {
                Set<Long> itemIdSet = Sets.newHashSet(param.getItemIds());
                if (!itemIdSet.contains(itemId)) {
                    PageOf<TaoProductModel> nullRes = new PageOf<TaoProductModel>();
                    nullRes.setData(Lists.newArrayList());
                    nullRes.setSuccess(true);
                    nullRes.setPageNo(page);
                    nullRes.setPageSize(pageSize);
                    nullRes.setTotal(0);
                    return nullRes;
                }
            }
            param.setItemIds(Lists.newArrayList(itemId));
        }

        // 商品维度指标
        String itemIndexes = index.getItemIndexes();
        // 商家维度指标
        String supplierIndexes = index.getSupplierIndexes();

        // 组装筛选查询
        assembleFilterQueryParam(param, topOriginStr, sortByStr, duplicateRemoveTypeStr, keyword, page, pageSize);

        // 商家维度组装
        assembleSupplierQueryParam(param, supplierIndexes);

        // 根据品维度筛选
        assembleItemQueryParam(param, itemIndexes);

        LOG.error("requestParam = " + param.toString());

        PageOf<TaoProductModel> seachRes = taoProductReadService.search(param);

        if (null == seachRes || !seachRes.isSuccess()) {
            // return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "search res is null");
            return null;
        }

        return seachRes;

    }

    /**
     * 根据指标组id找到offer列表
     *
     * @param indicationId
     * @param topOriginStr
     * @param sortByStr
     * @param duplicateRemoveTypeStr
     * @param keyword
     * @param page
     * @param pageSize
     * @return
     */
    private PageOf<IndicationOfferVO> getOfferListByIndicationId(Long indicationId, String topOriginStr,
                                                                 String sortByStr, String duplicateRemoveTypeStr, String keyword, Long itemId, Integer page, Integer pageSize) {
        // 查询task模型
        ResultOf<TaoShopSelectionConfigModel> configRes = taoShopSelectionReadService.getTaskConfigById(indicationId);

        if (null == configRes || !configRes.isSuccess() || null == configRes.getData()) {
            return null;
        }

        TaoShopSelectionConfigModel index = configRes.getData();

        return getOfferListByIndication(index, topOriginStr, sortByStr, duplicateRemoveTypeStr, keyword, itemId, page,
                pageSize);
    }

    /**
     * 组装excel
     *
     * @param offerList
     * @param data
     */
    private void assembleIndicationExcelVO(List<TaoProductModel> offerList, List<IndicationExcelVO> data) {
        if (CollectionUtils.isEmpty(offerList)) {
            return;
        }

        for (TaoProductModel model : offerList) {
            IndicationExcelVO v = new IndicationExcelVO();
            BeanUtils.copyProperties(model, v);
            if (null != model.getReservePrice()) {
                DecimalFormat df = new DecimalFormat("0.00");
                try {
                    String price = df.format(model.getReservePrice().doubleValue() / 100);
                    if (".00".equals(price)) {
                        price = "0";
                    }
                    v.setReservePrice(price);
                } catch (Exception e) {
                    LOG.error("conver offer price error", e.getMessage(), e);
                }
            }
            data.add(v);
        }
    }

    /**
     * 组装小数的保留2位
     *
     * @param num
     * @return
     */
    private String assembleDoubleFormat(Double num) {
        if (null != num) {
            // 保留2位小数
            DecimalFormat df = new DecimalFormat("0.00");
            String numberRes = "0";
            try {
                numberRes = String.valueOf(df.format(num));
                if (".00".equals(numberRes)) {
                    numberRes = "0";
                }
            } catch (Exception e) {
                LOG.error("conver num length 2 error", e.getMessage(), e);
            }
            return numberRes;
        } else {
            return "暂无";
        }
    }
}
