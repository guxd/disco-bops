package com.alibaba.cbuscm.controller.domestic.tb.supplier;

import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.core.product.constant.DcProductBizTypeEnum;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalReadService;
import com.alibaba.cbu.disco.shared.core.proposal.contant.OfferProposalFlowEnum;
import com.alibaba.cbu.disco.shared.core.proposal.contant.OfferProposalStatusEnum;
import com.alibaba.cbu.disco.shared.core.proposal.model.DcProposalModel;
import com.alibaba.cbu.disco.shared.core.proposal.param.DcProposalQueryParam;
import com.alibaba.cbuscm.enums.DepositDisplayFieldEnum;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.AliqygSupplierManageVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.CaigouSupplierDetailExtVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.CategoryViewVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.TagParamVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.*;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.TaoSupplierManageVO;
import com.alibaba.china.global.business.library.enums.StdCategoryChannelEnum;
import com.alibaba.china.global.business.library.interfaces.category.CategoryReadService;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.member.service.models.MemberModel;
import com.alibaba.china.shared.discosupplier.enums.SupplierSceneEnum;
import com.alibaba.china.shared.discosupplier.enums.operation.OperationDegreeEnum;
import com.alibaba.china.shared.discosupplier.enums.supplier.SupplierJoinStatusEnum;
import com.alibaba.china.shared.discosupplier.model.supplier.CategoryModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierInfoExtCaigouModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierInfoExtInModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierInfoExtOutModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierInfoModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierJoinModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierManageModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierManageSign;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierInfoParam;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierJoinParam;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierManageParam;
import com.alibaba.china.shared.discosupplier.service.common.SupplierAblityService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierInfoExtCaigouService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierInfoExtInService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierInfoExtOutService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierInfoService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierJoinService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierManageService;
import com.alibaba.china.shared.discosupplier.util.BitOperationUtils;
import com.alibaba.china.winport.biz.enums.WinportUrlEnum;
import com.alibaba.china.winport.biz.model.WinportUrlModel;
import com.alibaba.china.winport.biz.service.WinportUrlService;
import com.alibaba.fastjson.JSON;
import com.alibaba.shared.protect.api.service.DebtReadService;
import com.alibaba.shared.protect.api.service.PtsUserReadService;
import com.alibaba.shared.protect.enums.fund.FundSource;
import com.alibaba.shared.protect.enums.pts.PtsServiceCode;
import com.alibaba.shared.protect.enums.pts.PtsUserFundStatus;
import com.alibaba.shared.protect.model.debt.DebtItemModel;
import com.alibaba.shared.protect.model.debt.DebtModel;
import com.alibaba.shared.protect.model.pts.PtsDepositUser;
import com.alibaba.shared.protect.model.pts.PtsUser;
import com.alibaba.up.common.mybatis.result.PageList;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.result.Result;
import com.alibaba.up.common.mybatis.result.RpcResultError;
import com.alibaba.up.common.mybatis.task.Paginator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 供应商管理
 *
 * @author luxuetao
 */
@RestController
@RequestMapping("/taoSupplierManage")
public class TaoSupplierManageController {
    private static final Logger LOG = LoggerFactory.getLogger(TaoSupplierJoinController.class);
    private static final Logger ALIQYGLOG = LoggerFactory.getLogger("aliqyg");

    @Autowired
    private DcProposalReadService dcProposalReadService;
    @Autowired
    private SupplierInfoService supplierInfoService;
    @Autowired
    private SupplierJoinService supplierJoinService;
    @Autowired
    private MemberReadService memberReadService;
    @Autowired
    private WinportUrlService winportUrlService;
    @Autowired
    private PtsUserReadService ptsUserReadService;
    @Autowired
    private DebtReadService debtReadService;
    @Autowired
    private CategoryReadService categoryReadService;
    @Autowired
    private SupplierInfoExtInService supplierInfoExtInService;
    @Autowired
    private SupplierInfoExtOutService supplierInfoExtOutService;
    @Autowired
    private SupplierInfoExtCaigouService supplierInfoExtCaigouService;
    @Autowired
    private SupplierManageService supplierManageService;
    @Autowired
    private SupplierAblityService supplierAblityService;

    private static String HAS_EnrollOffer = "hasEnrollOffer";

    private static String NOT_EnrollOffer = "notEnrollOffer";

    private static String HAS_Debt = "hasDebt";

    private static String NOT_Debt = "notDebt";

    /**
     * 供应商详情
     */
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public RPCResult detail(HttpServletRequest request,
        @RequestParam(name = "memberId", required = true) String memberId,
        @RequestParam(name = "scene", required = true) String scene,
        @RequestParam(name = "joinId", required = false) Long joinId) {
        if (StringUtils.isEmpty(scene) || StringUtils.isEmpty(memberId)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        // 判断是否入驻成功，如果成功，则查对应资质信息和供应商状态信息
        SupplierInfoParam infoParam = new SupplierInfoParam();
        infoParam.setMemberId(memberId);
        SupplierInfoModel infoModel = supplierInfoService.queryOne(infoParam, Boolean.TRUE);
        if (null == infoModel) {
            return RPCResult.error(RpcResultError.CODE_PARAM_ERROR, RpcResultError.MSG_ERROR_ILLEGAL);
        }
        // 由于同一个渠道下一个供应商可以有多条入驻记录，详情要展示入驻信息，则根据id参数判断，有id取特定入驻记录，否则取最新入驻记录
        SupplierJoinModel joinModel = null;
        if (null != joinId && joinId > 0) {
            joinModel = supplierJoinService.find(joinId);
        } else {
            SupplierJoinParam joinParam = new SupplierJoinParam();
            joinParam.setMemberId(memberId);
            joinParam.setScene(scene);
            joinModel = supplierJoinService.queryOne(joinParam, Boolean.TRUE);
        }

        if (null == joinModel) {
            return RPCResult.error(RpcResultError.CODE_PARAM_ERROR, RpcResultError.MSG_ERROR_ILLEGAL);
        }
        TaoSupplierDetailVO vo = new TaoSupplierDetailVO();
        try {
            vo.setStatus(joinModel.getStatus());
            vo.setScene(joinModel.getScene());
            vo.setObjective(joinModel.getObjective());
            if (null != joinModel.getGmtJoin()) {
                vo.setGmtJoin(joinModel.getGmtJoin().getTime());
            }
            BeanUtils.copyProperties(infoModel, vo, new String[] {"express", "license",
                "vatInvoice", "mainCategory", "brandInfo", "category"});
            if (null != infoModel.getExpress()) {
                vo.setExpress(infoModel.getExpress().getCpName());
            }
            if (CollectionUtils.isNotEmpty(infoModel.getLicense())) {
                vo.setLicense(infoModel.getLicense());
            }
            Result<Boolean> result = null;
            //工厂标记
            try {
                result = supplierAblityService.isSupplierFactory(memberId);
                if (result != null && result.isSuccess()) {
                    //1表示工厂 2表示非工厂 3表示未知
                    if (result.getModel()) {
                        vo.setFactorySign(Boolean.TRUE);
                    } else {
                        vo.setFactorySign(Boolean.FALSE);
                    }
                } else {
                    vo.setFactorySign(Boolean.FALSE);
                }
            } catch (Exception e) {
                LOG.error(String.format("getFactoryFlag error %s", e.getMessage()));
                vo.setFactorySign(Boolean.FALSE);
            }
            vo.setBrandInfo(infoModel.getBrandInfo());
            vo.setMainCategory(infoModel.getMainCategory());
            if (null != infoModel.getVatInvoice() && infoModel.getVatInvoice() == 1) {
                vo.setVatInvoice(true);
            } else {
                vo.setVatInvoice(false);
            }
            if (SupplierSceneEnum.getDomesrticList().contains(SupplierSceneEnum.parse(scene))) {
                // 内贸
                Result<SupplierInfoExtInModel> inModelResult = supplierInfoExtInService.showSupplierInfoExt(memberId);
                if (null != inModelResult && null != inModelResult.getModel()) {
                    SupplierInfoExtInModel inModel = inModelResult.getModel();
                    BeanUtils.copyProperties(inModel, vo, new String[] {"substitute", "supportInvoice",
                        "deliveryCertificate", "printCertificate", "commonCosmetics", "explosionProofCertificate",
                        "electricDeviceCertificate", "tmallSpecCertificate", "softwareCertificate",
                        "productCertificate"});
                    if (null != inModel.getSubstitute() && inModel.getSubstitute() == 1) {
                        vo.setSubstitute(true);
                    } else {
                        vo.setSubstitute(false);
                    }
                    if (null != inModel.getSupportVoice() && inModel.getSupportVoice() == 1) {
                        vo.setSupportInvoice(true);
                    } else {
                        vo.setSupportInvoice(false);
                    }
                    if (CollectionUtils.isNotEmpty(inModel.getDeliveryCertificate())) {
                        vo.setDeliveryCertificate(inModel.getDeliveryCertificate());
                    }
                    if (CollectionUtils.isNotEmpty(inModel.getPrintCertificate())) {
                        vo.setPrintCertificate(inModel.getPrintCertificate());
                    }
                    if (CollectionUtils.isNotEmpty(inModel.getElectricDeviceCertificate())) {
                        vo.setElectricDeviceCertificate(inModel.getElectricDeviceCertificate());
                    }
                    if (CollectionUtils.isNotEmpty(inModel.getTmallSpecCertificate())) {
                        vo.setTmallSpecCertificate(inModel.getTmallSpecCertificate());
                    }
                    if (CollectionUtils.isNotEmpty(inModel.getSoftwareCertificate())) {
                        vo.setSoftwareCertificate(inModel.getSoftwareCertificate());
                    }
                    vo.setProductCertificate(inModel.getProductCertificate());
                    vo.setCommonCosmetics(inModel.getCommonCosmetics());
                    vo.setExplosionProofCertificate(inModel.getExplosionProofCertificate());
                }
            } else if (SupplierSceneEnum.getCrossBorderList().contains(SupplierSceneEnum.parse(scene))) {
                // 跨境
                Result<SupplierInfoExtOutModel> outModelResult =
                    supplierInfoExtOutService.showSupplierInfoExt(memberId);
                if (null != outModelResult && null != outModelResult.getModel()) {
                    SupplierInfoExtOutModel outModel = outModelResult.getModel();
                    BeanUtils.copyProperties(outModel, vo,
                        new String[] {"taxpayerQualificationCertificate", "copyrightCertificate",
                            "patentCertificate", "qualificationTestReport", "englishPackaging", "canProformaInvoice"});
                    vo.setTaxpayerQualificationCertificate(outModel.getTaxpayerQualificationCertificate());
                    vo.setCopyrightCertificate(outModel.getCopyrightCertificate());
                    vo.setPatentCertificate(outModel.getPatentCertificate());
                    vo.setQualificationTestReport(outModel.getQualificationTestReport());
                    if (null != outModel.getEnglishPackaging() && outModel.getEnglishPackaging() == 1) {
                        vo.setEnglishPackaging(true);
                    } else {
                        vo.setEnglishPackaging(false);
                    }
                    if (null != outModel.getCanProformaInvoice() && outModel.getCanProformaInvoice() == 1) {
                        vo.setCanProformaInvoice(true);
                    } else {
                        vo.setCanProformaInvoice(false);
                    }
                }
            } else if (SupplierSceneEnum.isAliqyg(joinModel.getObjective(), joinModel.getScene())) {
                setCaigouExtInfo(vo, memberId);
            }
            try {
                WinportUrlModel winportUrlModel = winportUrlService.getUrl(memberId, WinportUrlEnum.INDEX_URL);
                if (null != winportUrlModel) {
                    vo.setWinportUrl(winportUrlModel.getIndexUrl());
                }
                long userId = memberReadService.convertMemberIdToUserId(memberId);
                Map<String, String> loginIdMap = memberReadService.convertLoginIdsByMemberIds(Lists.newArrayList
                    (memberId));
                if (null != loginIdMap && null != loginIdMap.get(memberId)) {
                    vo.setLoginId(loginIdMap.get(memberId));
                }
                PtsDepositUser ptsDepositUser = ptsUserReadService.querySinglePtsDepositUser(userId,
                    TaoSupplierPayController.getPtsServiceCodeByScene(
                        SupplierSceneEnum.convertToEnum(joinModel.getObjective(), joinModel.getScene())));
                if (null != ptsDepositUser) {
                    if (!StringUtils.equals(ptsDepositUser.getFundSource(), FundSource
                        .INSURANCE.getCode())) {
                        vo.setPolicy(false);
                    } else {
                        vo.setPolicy(true);
                    }
                    if (null != ptsDepositUser.getOccupyAmount()) {
                        BigDecimal b1 = new BigDecimal(ptsDepositUser.getOccupyAmount());
                        BigDecimal b2 = new BigDecimal(1000000);
                        Double occupyAmount = b1.divide(b2, 6, BigDecimal.ROUND_HALF_UP).doubleValue();
                        vo.setOccupyAmount(occupyAmount);
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
            if (CollectionUtils.isNotEmpty(infoModel.getCategory())) {
                List<CategoryViewVO> categories = Lists.newArrayList();
                List<String> categoryIds = infoModel.getCategory().stream().map(CategoryModel::getCategoryKey).collect(
                    Collectors.toList
                        ());
                if (CollectionUtils.isNotEmpty(categoryIds)) {
                    for (String categoryId : categoryIds) {
                        if (!NumberUtils.isNumber(categoryId)) {
                            continue;
                        }
                        String names = categoryReadService
                            .getCategoryNamePath(StdCategoryChannelEnum.CHANNEL_TB, Long.valueOf(categoryId), ",");
                        List<String> nameList = Lists.newArrayList(names.split(","));
                        CategoryViewVO categoryViewVO = new CategoryViewVO();
                        categoryViewVO.setCategoryValues(nameList);
                        categories.add(categoryViewVO);
                    }
                }
                vo.setCategories(categories);
            }
            // 除了资质信息外 增加字段是否入驻成功，是否清退等状态
            if (SupplierJoinStatusEnum.PAID.equals(SupplierJoinStatusEnum.parse(joinModel.getStatus()))) {
                SupplierManageModel supplierManageModel = supplierManageService.find(memberId,
                    SupplierSceneEnum.parse(scene), false);
                if (null == supplierManageModel) {
                    return RPCResult.error(RpcResultError.CODE_SERVER_ERROR, RpcResultError.MSG_ERROR_SYSTEM);
                }

                BeanUtils.copyProperties(supplierManageModel, vo, new String[] {"gmtClear"});
                if (null != supplierManageModel.getGmtClear()) {
                    vo.setGmtClear(supplierManageModel.getGmtClear().getTime());
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return RPCResult.ok(vo);
    }

    /**
     * 设置阿里企业购业务扩展信息
     *
     * @param vo
     * @param memberId
     */
    private void setCaigouExtInfo(TaoSupplierDetailVO vo, String memberId) {
        Result<SupplierInfoExtCaigouModel> inModelResult = supplierInfoExtCaigouService.showSupplierInfoExt(memberId);
        if (null == inModelResult) {
            return;
        }
        vo.setCaigouSupplierDetailExtVO(new CaigouSupplierDetailExtVO());
        SupplierInfoExtCaigouModel inModel = inModelResult.getModel();

        if (null != inModel.getSubstitute() && inModel.getSubstitute() == 1) {
            vo.setSubstitute(true);
        } else {
            vo.setSubstitute(false);
        }

        vo.setTmallExist(inModel.getTmallExist());
        vo.setTmallUrl(inModel.getTmallUrl());
        vo.getCaigouSupplierDetailExtVO().setShop1688Exist(inModel.getShop1688Exist());
        vo.getCaigouSupplierDetailExtVO().setShop1688Url(inModel.getShop1688Url());
        vo.getCaigouSupplierDetailExtVO().setOperatorNums(inModel.getOperatorNums());
        vo.getCaigouSupplierDetailExtVO().setOrderEveryDay(inModel.getOrderEveryDay());
        vo.getCaigouSupplierDetailExtVO().setWarehouseList(inModel.getWarehouseList());
        vo.setDeliveryCertificate(inModel.getDeliveryCertificate());
        vo.setSoftwareCertificate(inModel.getSoftwareCertificate());
        vo.setPrintCertificate(inModel.getPrintCertificate());
        vo.setElectricDeviceCertificate(inModel.getElectricDeviceCertificate());
        vo.setProductCertificate(inModel.getProductCertificate());
        vo.setTmallSpecCertificate(inModel.getTmallSpecCertificate());
        vo.getCaigouSupplierDetailExtVO().setDistribution(inModel.getDistribution());
        vo.getCaigouSupplierDetailExtVO().setTrade(inModel.getTrade());
    }

    /**
     * 获取供应商入驻列表
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RPCResult list(HttpServletRequest request,
        @RequestParam(name = "categoryLeafId", required = false) Long categoryLeafId,
        @RequestParam(name = "status", required = false) String status,
        @RequestParam(name = "tag", required = false) String tag,
        @RequestParam(name = "companyName", required = false) String companyName,
        @RequestParam(name = "loginId", required = false) String loginId,
        @RequestParam(name = "intervalStartKey", required = false) String intervalStartKey,
        @RequestParam(name = "intervalStartValue", required = false) Integer intervalStartValue,
        @RequestParam(name = "intervalEndKey", required = false) String intervalEndKey,
        @RequestParam(name = "intervalEndValue", required = false) Integer intervalEndValue,
        @RequestParam(name = "page", defaultValue = "1") Integer page,
        @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
        @RequestParam(name = "orderBy", defaultValue = "GMT_CREATE") String orderBy,
        @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection,
        @RequestParam(name = "channel", required = false) String channel,
        @RequestParam(name = "scene", required = false) String scene,
        @RequestParam(name = "sign", required = false) String sign) {
        //如果是清退 返回清退相关信息 时间、原因

        SupplierManageParam param = new SupplierManageParam();
        if (null != categoryLeafId && categoryLeafId > 0) {
            param.setCategory(String.valueOf(categoryLeafId));
        }

        if (StringUtils.isNotBlank(tag)) {
            param.setTag(tag);
        }
        if (StringUtils.isNotBlank(status)) {
            param.setStatus(status);
        }
        if (sign != null) {
            SupplierManageSign supplierManageSign = new SupplierManageSign();
            if (HAS_EnrollOffer.equals(sign)) {
                supplierManageSign.setEnrollOffer(true);
            }
            if (HAS_Debt.equals(sign)) {
                supplierManageSign.setDebt(true);
            }
            if (NOT_Debt.equals(sign)) {
                supplierManageSign.setDebt(false);
            }
            if (NOT_EnrollOffer.equals(sign)) {
                supplierManageSign.setEnrollOffer(false);
            }
            param.setSign(supplierManageSign);
        }

        param.setCountNeeded(true);
        if (StringUtils.isNotBlank(loginId)) {
            MemberModel memberModel = memberReadService.findMemberByLoginId(loginId);
            if (null != memberModel) {
                param.setLikeMemberId(memberModel.getMemberId());
            } else {
                param.setLikeMemberId(loginId);
            }
        }

        if (StringUtils.isNotBlank(companyName)) {
            param.setName(companyName);
        }
        if (StringUtils.equals("korderNumStart", intervalStartKey)) {
            param.setKorderNumStart(intervalStartValue);
        } else if (StringUtils.equals("itemNumStart", intervalStartKey)) {
            param.setItemNumStart(intervalStartValue);
        } else if (StringUtils.equals("korderStyleNumStart", intervalStartKey)) {
            param.setKorderStyleNumStart(intervalStartValue);
        } else if (StringUtils.equals("coopGoodsNumStart", intervalStartKey)) {
            param.setCoopGoodsNumStart(intervalStartValue);
        } else if (StringUtils.equals("coopSalesNumStart", intervalStartKey)) {
            param.setCoopSalesNumStart(intervalStartValue);
        }
        if (StringUtils.equals("korderNumEnd", intervalEndKey)) {
            param.setKorderNumEnd(intervalEndValue);
        } else if (StringUtils.equals("itemNumEnd", intervalEndKey)) {
            param.setItemNumEnd(intervalEndValue);
        } else if (StringUtils.equals("korderStyleNumEnd", intervalEndKey)) {
            param.setKorderStyleNumEnd(intervalEndValue);
        } else if (StringUtils.equals("coopGoodsNumEnd", intervalEndKey)) {
            param.setCoopGoodsNumEnd(intervalEndValue);
        } else if (StringUtils.equals("coopSalesNumEnd", intervalEndKey)) {
            param.setCoopSalesNumEnd(intervalEndValue);
        }
        if (StringUtils.equals(channel, "domesrtic")) {
            param.setSceneList(SupplierSceneEnum.getDomesrticList());
        } else if (StringUtils.equals(channel, "crossBorder")) {
            param.setSceneList(SupplierSceneEnum.getCrossBorderList());
        }
        if (StringUtils.isNotEmpty(scene) && null != SupplierSceneEnum.parse(scene)) {
            param.setScene(SupplierSceneEnum.parse(scene).getValue());
        }
        param.setPage(page);
        param.setPageSize(itemsPerPage);
        param.setOrderBy(orderBy);
        param.setOrderDirection(orderDirection);
        param.setNeedExtend(true);
        PageList<SupplierManageModel> pageList = null;
        List<TaoSupplierManageVO> voList = Lists.newArrayList();
        try {
            pageList = supplierManageService.query(param);
            List<String> memberIds = getMemberIds(pageList.getDataList());
            Map<String, WinportUrlModel> winportUrlMap = null;
            Map<String, DebtModel> debtMap = null;
            Map<String, String> loginIdsMap = null;
            Map<String, PtsUser> ptsUserMap = null;
            if (CollectionUtils.isNotEmpty(memberIds)) {
                try {
                    // 批量查询旺铺地址
                    winportUrlMap = winportUrlService.getUrl(memberIds,
                        WinportUrlEnum.INDEX_URL);
                    // 批量获取保证金是否不足判断
                    debtMap = debtReadService.batchQueryValidDebtModel(memberIds, PtsServiceCode
                        .GHZXBZJ.getCode());
                    loginIdsMap = memberReadService.convertLoginIdsByMemberIds(memberIds);
                    // 这里获取单个供应商的额度进行判断保证金不足，为了方便测试，可以针对单个供应商进行设置测试
                    ptsUserMap = ptsUserReadService.queryPtsUserByMemberId(memberIds, PtsServiceCode.GHZXBZJ
                        .getCode());
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
            // 组装VO
            for (SupplierManageModel model : pageList.getDataList()) {
                TaoSupplierManageVO vo = new TaoSupplierManageVO();
                if (null != loginIdsMap) {
                    vo.setLoginId(loginIdsMap.get(model.getMemberId()));
                }
                vo.setHasDebt(null != model.getSign() && model.getSign().hasDebt());
                vo.setHasEnrollOffer(null != model && model.getSign().hasEnrollOffer());
                vo.setMemberId(model.getMemberId());
                vo.setCompanyName(model.getCompanyName());
                try {
                    SupplierInfoParam infoParam = new SupplierInfoParam();
                    infoParam.setMemberId(model.getMemberId());
                    SupplierInfoModel infoModel = supplierInfoService.queryOne(infoParam, Boolean.TRUE);
                    if (null != infoModel) {
                        vo.setDockingDing(infoModel.getDockingDing());
                        vo.setDockingWang(infoModel.getDockingWang());
                        if (CollectionUtils.isNotEmpty(infoModel.getCategory())) {
                            List<CategoryViewVO> categories = Lists.newArrayList();
                            List<String> categoryIds = infoModel.getCategory().stream().map(
                                CategoryModel::getCategoryKey).collect(Collectors.toList
                                ());
                            if (CollectionUtils.isNotEmpty(categoryIds)) {
                                for (String categoryId : categoryIds) {
                                    if (!NumberUtils.isNumber(categoryId)) {
                                        continue;
                                    }
                                    String names = categoryReadService
                                        .getCategoryNamePath(StdCategoryChannelEnum.CHANNEL_TB,
                                            Long.valueOf(categoryId), ",");
                                    List<String> nameList = Lists.newArrayList(names.split(","));
                                    CategoryViewVO categoryViewVO = new CategoryViewVO();
                                    categoryViewVO.setCategoryValues(nameList);
                                    categories.add(categoryViewVO);
                                }
                            }
                            vo.setCategories(categories);
                        }
                    }
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
                vo.setLevel(model.getLevel());
                vo.setScene(model.getScene());
                vo.setObjective(model.getObjective());
                if (null != model.getExtModel()) {
                    vo.setKorderNum(model.getExtModel().getKorderNum());
                    vo.setKorderStyleNum(model.getExtModel().getKorderStyleNum());
                    vo.setItemNum(model.getExtModel().getItemNum());
                    vo.setCoopGoodsNum(model.getExtModel().getCoopGoodsNum());
                    vo.setCoopSalesNum(model.getExtModel().getCoopSalesNum());
                }
                vo.setScore(model.getScore());
                vo.setTags(model.getTags());
                vo.setStatus(model.getStatus());
                if (null != model.getGmtClear()) {
                    vo.setGmtClear(model.getGmtClear().getTime());
                }
                vo.setClearRemark(model.getClearRemark());
                vo.setRemark(model.getRemark());
                if (null != winportUrlMap && null != winportUrlMap.get(model.getMemberId())) {
                    vo.setWinportUrl(winportUrlMap.get(model.getMemberId()).getIndexUrl());
                }
                // 保证金提醒 被罚金额大于额度的50%，即提醒保证金不足
                if (null != debtMap && null != debtMap.get(model.getMemberId())) {
                    DebtModel debtModel = debtMap.get(model.getMemberId());
                    if (null != debtModel.getTotalDebtMoney() && null != ptsUserMap && null !=
                        ptsUserMap.get(model.getMemberId()) && null != ptsUserMap.get(model.getMemberId())
                        .getOpenAmount()
                        && debtModel
                        .getTotalDebtMoney() > ptsUserMap.get(model.getMemberId()).getOpenAmount() * 0.5) {
                        vo.setDepositStatus(DepositDisplayFieldEnum.INSUFFICIENT.getValue());
                        // 被罚金额大于额度的50%同时7天未缴纳，判断方法所有7天之内的欠款记录，总额度小于额度的50%
                        if (CollectionUtils.isNotEmpty(debtModel.getDebtItemModel())) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_YEAR, -7);
                            Date sevenDayBefore = calendar.getTime();
                            List<DebtItemModel> sevenDebtItemList = debtModel.getDebtItemModel().stream().filter(e ->
                                null != e.getGmtCreate() && e.getGmtCreate().before(sevenDayBefore))
                                .collect(Collectors.toList());
                            if (CollectionUtils.isNotEmpty(sevenDebtItemList)) {
                                Long debtMoney = 0L;
                                for (DebtItemModel debtItemModel : sevenDebtItemList) {
                                    debtMoney += debtItemModel.getDebtMoney();
                                }
                                if (debtMoney > ptsUserMap.get(model.getMemberId()).getOpenAmount() * 0.5) {
                                    vo.setDepositStatus(DepositDisplayFieldEnum.SEVEN_DAYS_NON_SURRENDER.getValue());
                                }
                            }
                        }
                    }
                }
                voList.add(vo);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        Map<String, Object> result = Maps.newHashMap();
        result.put("data", voList);
        if (null != pageList && null != pageList.getPaginator()) {
            result.put("paginator", new Paginator(pageList.getPaginator().getItems(), itemsPerPage, page));
        }

        return RPCResult.okNoWrapper(result);
    }

    /**
     * 获取阿里企业购供应商入驻列表
     *
     * 如果已清退 返回清退相关信息： 时间、原因
     */
    @RequestMapping(value = "/aliqyg/list", method = RequestMethod.GET)
    public RPCResult aliqygList(HttpServletRequest request,
        @RequestParam(name = "categoryLeafId", required = false) Long categoryLeafId,
        @RequestParam(name = "status", required = false) String status,
        @RequestParam(name = "tag", required = false) String tag,
        @RequestParam(name = "companyName", required = false) String companyName,
        @RequestParam(name = "loginId", required = false) String loginId,
        @RequestParam(name = "intervalStartKey", required = false) String intervalStartKey,
        @RequestParam(name = "intervalStartValue", required = false) Integer intervalStartValue,
        @RequestParam(name = "intervalEndKey", required = false) String intervalEndKey,
        @RequestParam(name = "intervalEndValue", required = false) Integer intervalEndValue,
        @RequestParam(name = "page", defaultValue = "1") Integer page,
        @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
        @RequestParam(name = "orderBy", defaultValue = "GMT_CREATE") String orderBy,
        @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection,
        @RequestParam(name = "channel", required = false) String channel,
        @RequestParam(name = "scene", required = false) String scene) {

        SupplierManageParam param = new SupplierManageParam();
        if (null != categoryLeafId && categoryLeafId > 0) {
            param.setCategory(String.valueOf(categoryLeafId));
        }
        if (StringUtils.isNotBlank(tag)) {
            param.setTag(tag);
        }
        if (StringUtils.isNotBlank(status)) {
            param.setStatus(status);
        }
        param.setCountNeeded(true);
        if (StringUtils.isNotBlank(loginId)) {
            MemberModel memberModel = memberReadService.findMemberByLoginId(loginId);
            if (null != memberModel) {
                param.setLikeMemberId(memberModel.getMemberId());
            } else {
                param.setLikeMemberId(loginId);
            }
        }
        if (StringUtils.isNotBlank(companyName)) {
            param.setName(companyName);
        }

        param.setSceneList(Arrays.asList(SupplierSceneEnum.ALIQYG));
        param.setPage(page);
        param.setPageSize(itemsPerPage);
        param.setOrderBy(orderBy);
        param.setOrderDirection(orderDirection);
        param.setNeedExtend(true);
        PageList<SupplierManageModel> pageList = null;
        List<AliqygSupplierManageVO> voList = Lists.newArrayList();
        try {
            pageList = supplierManageService.query(param);
            List<String> memberIds = getMemberIds(pageList.getDataList());
            Map<String, WinportUrlModel> winportUrlMap = null;
            Map<String, DebtModel> debtMap = null;
            Map<String, String> loginIdsMap = null;
            Map<String, PtsUser> ptsUserMap = null;
            if (CollectionUtils.isNotEmpty(memberIds)) {
                try {
                    // 批量查询旺铺地址
                    winportUrlMap = winportUrlService.getUrl(memberIds,
                        WinportUrlEnum.INDEX_URL);

                    loginIdsMap = memberReadService.convertLoginIdsByMemberIds(memberIds);

                    // 这里获取单个供应商的额度进行判断保证金不足，为了方便测试，可以针对单个供应商进行设置测试
                    ptsUserMap = ptsUserReadService.queryPtsUserByMemberId(memberIds,
                        PtsServiceCode.ALIQIYEGOU.getCode());

                    debtMap = debtReadService.batchQueryValidDebtModel(memberIds, PtsServiceCode.ALIQIYEGOU.getCode());

                } catch (Exception e) {
                    ALIQYGLOG.error("batch process error, memberIds: " + JSON.toJSONString(memberIds), e);
                }
            }
            // 组装VO
            for (SupplierManageModel model : pageList.getDataList()) {
                AliqygSupplierManageVO vo = new AliqygSupplierManageVO();
                if (null != loginIdsMap) {
                    vo.setLoginId(loginIdsMap.get(model.getMemberId()));
                }
                vo.setMemberId(model.getMemberId());
                vo.setCompanyName(model.getCompanyName());
                try {
                    SupplierInfoParam infoParam = new SupplierInfoParam();
                    infoParam.setMemberId(model.getMemberId());
                    SupplierInfoModel infoModel = supplierInfoService.queryOne(infoParam, Boolean.TRUE);
                    if (null != infoModel) {
                        vo.setDockingDing(infoModel.getDockingDing());
                        vo.setDockingWang(infoModel.getDockingWang());
                        // 优势类目
                        if (CollectionUtils.isNotEmpty(infoModel.getCategory())) {

                            List<CategoryViewVO> categories = Lists.newArrayList();
                            List<String> categoryValues = infoModel.getCategory().stream().map(
                                CategoryModel::getCategoryValue).collect(Collectors.toList
                                ());
                            CategoryViewVO categoryViewVO = new CategoryViewVO();
                            categoryViewVO.setCategoryValues(categoryValues);
                            categories.add(categoryViewVO);
                            vo.setCategories(categories);
                        }
                        // 主营类目
                        if (infoModel.getMainCategory() != null) {
                            vo.setMainCategory(infoModel.getMainCategory().getCategoryValue());
                        }
                    }
                } catch (Exception e) {
                    ALIQYGLOG.error("composit error, memberId:" + model.getMemberId(), e);
                }
                vo.setLevel(model.getLevel());
                vo.setScene(model.getScene());
                vo.setObjective(model.getObjective());
                if (null != model.getExtModel()) {
                    vo.setKorderNum(model.getExtModel().getKorderNum());
                    vo.setKorderStyleNum(model.getExtModel().getKorderStyleNum());
                    vo.setItemNum(model.getExtModel().getItemNum());
                    vo.setCoopGoodsNum(model.getExtModel().getCoopGoodsNum());
                    vo.setCoopSalesNum(model.getExtModel().getCoopSalesNum());
                }
                vo.setScore(model.getScore());
                vo.setTags(model.getTags());
                vo.setStatus(model.getStatus());
                if (null != model.getGmtClear()) {
                    vo.setGmtClear(model.getGmtClear().getTime());
                }
                vo.setClearRemark(model.getClearRemark());
                vo.setRemark(model.getRemark());
                if (null != winportUrlMap && null != winportUrlMap.get(model.getMemberId())) {
                    vo.setWinportUrl(winportUrlMap.get(model.getMemberId()).getIndexUrl());
                }

                // 保证金提醒
                if (null != ptsUserMap && null != ptsUserMap.get(model.getMemberId())) {
                    PtsUser ptsUser = ptsUserMap.get(model.getMemberId());
                    // 保障金已退回状态查询
                    if (PtsUserFundStatus.RELEASE.getCode().equalsIgnoreCase(ptsUser.getFundStatus())) {
                        vo.setDepositStatus("release");
                    }
                }

                if (null != debtMap && null != debtMap.get(model.getMemberId())
                    && debtMap.get(model.getMemberId()).getTotalDebtMoney() > 0) {

                    vo.setDepositStatus(DepositDisplayFieldEnum.INSUFFICIENT.getValue());
                }

                // 查询在售商品数
                DcProposalQueryParam dcProposalQueryParam = new DcProposalQueryParam();
                dcProposalQueryParam.setMemberId(model.getMemberId());
                dcProposalQueryParam.setBizType(DcProductBizTypeEnum.ALIQYG.name());
                dcProposalQueryParam.setFlow(OfferProposalFlowEnum.ALIQYG_PROPOSAL.name());
                dcProposalQueryParam.setStatus(OfferProposalStatusEnum.BIND.name());
                dcProposalQueryParam.setPageNo(1);
                dcProposalQueryParam.setPageSize(1);
                PageOf<DcProposalModel> dcProposal = dcProposalReadService.listProposal(dcProposalQueryParam);
                vo.setItemNum(dcProposal == null ? 0 : dcProposal.getTotal());

                voList.add(vo);
            }
        } catch (Exception e) {
            ALIQYGLOG.error(e.getMessage(), e);
        }
        Map<String, Object> result = Maps.newHashMap();
        result.put("data", voList);
        if (null != pageList && null != pageList.getPaginator()) {
            result.put("paginator", new Paginator(pageList.getPaginator().getItems(), itemsPerPage, page));
        }
        return RPCResult.okNoWrapper(result);
    }

    /**
     * 商家打标 -- 标签直接覆盖
     */
    @RequestMapping(value = "/tag", method = RequestMethod.POST)
    public RPCResult tag(HttpServletRequest request, @RequestBody TagParamVO param) {
        if (null == param || StringUtils.isEmpty(param.getMemberId()) || StringUtils.isEmpty(param.getScene())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        Result<Boolean> result = null;
        try {
            result = supplierManageService.tag(param.getMemberId(), SupplierSceneEnum.parse(param.getScene()),
                param.getTags(),
                LoginUtil.getLoginName(request),
                OperationDegreeEnum.WAITER);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        if (null != result && !result.isSuccess()) {
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
        return RPCResult.ok();
    }

    /**
     * 商家备注 -- 备注内容直接覆盖
     */
    @RequestMapping(value = "/remark", method = RequestMethod.POST)
    public RPCResult remark(HttpServletRequest request, @RequestParam(name = "memberId") String memberId,
        @RequestParam(name = "remark") String remark, @RequestParam(name =
        "scene") String scene) {
        if (StringUtils.isEmpty(scene) || StringUtils.isEmpty(memberId)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        Result<Boolean> result = null;
        try {
            result = supplierManageService.remark(memberId, SupplierSceneEnum.parse(scene), remark,
                LoginUtil.getLoginName(request),
                OperationDegreeEnum.WAITER);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        if (null != result && !result.isSuccess()) {
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
        return RPCResult.ok();
    }

    /**
     * 清退
     */
    @RequestMapping(value = "/clear", method = RequestMethod.POST)
    public RPCResult clear(HttpServletRequest request, @RequestParam(name = "memberId") String memberId,
        @RequestParam(name = "clearRemark") String clearRemark, @RequestParam(name =
        "scene") String scene) {
        if (StringUtils.isEmpty(scene) || StringUtils.isEmpty(memberId)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        Result<Boolean> result = null;
        try {
            result = supplierManageService.clear(memberId, SupplierSceneEnum.parse(scene), clearRemark,
                LoginUtil.getLoginName(request),
                OperationDegreeEnum.WAITER);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        if (null != result && !result.isSuccess()) {
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
        return RPCResult.ok();
    }

    private <T> String listToString(Class<T> clz, List<T> list) {
        String str = "";
        if (String.class.equals(clz)) {
            str = org.apache.commons.lang.StringUtils.join(list.toArray(), ",");
        } else {
            List<String> objList = new ArrayList<>();
            list.stream().forEach(t -> objList.add(JSON.toJSONString(t)));
            str = org.apache.commons.lang.StringUtils.join(objList.toArray(), ",");
        }
        return str;
    }

    private List<String> getMemberIds(List<SupplierManageModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        List<String> memberIds = Lists.newArrayList();
        for (SupplierManageModel item : list) {
            if (!memberIds.contains(item.getMemberId())) {
                memberIds.add(item.getMemberId());
            }
        }
        return memberIds;
    }
}
