/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.controller.domestic.tb.supplier;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.CompanyInfoVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.PayParamVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.SupplierPayVO;
import com.alibaba.cbuscm.vo.domestic.tb.supplier.TapSupplierPayFilterVO;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.member.service.models.MemberModel;
import com.alibaba.china.shared.discosupplier.enums.SupplierSceneEnum;
import com.alibaba.china.shared.discosupplier.enums.protect.ProtectErrorCodeEnum;
import com.alibaba.china.shared.discosupplier.enums.supplier.BizType;
import com.alibaba.china.shared.discosupplier.enums.supplier.SupplierPayEnterPropertyEnum;
import com.alibaba.china.shared.discosupplier.enums.supplier.SupplierPayStatusEnum;
import com.alibaba.china.shared.discosupplier.enums.supplier.SupplierShopEnum;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierInfoModel;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierPayModel;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierInfoParam;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierPayParam;
import com.alibaba.china.shared.discosupplier.service.common.SupplierOssService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierInfoService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierJoinService;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierPayService;
import com.alibaba.china.winport.biz.enums.WinportUrlEnum;
import com.alibaba.china.winport.biz.model.WinportUrlModel;
import com.alibaba.china.winport.biz.service.WinportUrlService;
import com.alibaba.common.lang.StringUtil;
import com.alibaba.security.util.StringUtils;
import com.alibaba.shared.protect.api.request.pts.PtsFundPenaltyRequest;
import com.alibaba.shared.protect.api.service.PtsUserFundWriteService;
import com.alibaba.shared.protect.api.service.PtsUserReadService;
import com.alibaba.shared.protect.enums.pts.PtsServiceCode;
import com.alibaba.shared.protect.exception.ServiceException;
import com.alibaba.shared.protect.model.pts.PtsDepositUser;
import com.alibaba.up.common.mybatis.result.PageList;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.result.Result;
import com.alibaba.up.common.mybatis.task.Paginator;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类TaoSupplierPayController.java的实现描述：TODO 类实现描述
 *
 * @author jizhi.qy 2019年5月24日 上午10:47:19
 */

@RestController
@RequestMapping("/taoSupplierPay")
public class TaoSupplierPayController {

    private static final Logger LOG = LoggerFactory.getLogger(TaoSupplierPayController.class);

    @Autowired
    private MemberReadService memberReadService;
    @Autowired
    private PtsUserReadService ptsUserReadService;
    @Autowired
    private SupplierPayService supplierPayService;
    @Autowired
    private PtsUserFundWriteService ptsUserFundWriteService;
    @Autowired
    private SupplierOssService supplierOssService;
    @Autowired
    private SupplierJoinService supplierJoinService;
    @Autowired
    private SupplierInfoService supplierInfoService;
    @Autowired
    private WinportUrlService winportUrlService;


    /**
     * 根据loginId获取用户公司、余额等信息
     * 
     * @return
     */
    @RequestMapping(value = "/getInfoByLoginId", method = RequestMethod.GET)
    public RPCResult getInfoByLoginId(@RequestParam(name = "loginId") String loginId,
                                      @RequestParam(name = "bizType", required = false) String bizType) {
        if(org.apache.commons.lang3.StringUtils.isBlank(bizType)){
            bizType = BizType.CDGYL.name();
        }
        // 转换memberId
        MemberModel member = memberReadService.findMemberByLoginId(loginId);
        
        CompanyInfoVO companyInfoVO = new CompanyInfoVO();
        
        if (null == member || StringUtils.isBlank(member.getMemberId())) {
            return RPCResult.ok(companyInfoVO);
        }
        
        // 判断如果输入的memberId不是当前入驻的memberId，则返回error
        Result<Boolean> joined = supplierJoinService.isJoined(member.getMemberId(),
            (BizType.CAIGOU.name().equalsIgnoreCase(bizType) ? SupplierSceneEnum.ALIQYG
                : SupplierSceneEnum.TIAN_TIAN_FACTORY_STORE));

        if (null == joined || !joined.isSuccess() || null == joined.getModel() || !joined.getModel()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, ProtectErrorCodeEnum.SUPPLIER_NOT_EXIST.getMessage());
        }

        
        companyInfoVO.setMemberId(member.getMemberId());
        companyInfoVO.setCompanyName(member.getCompanyName());

        // 获取保证金余额
        if (null != member.getUserId()) {
            PtsDepositUser ptsDepositUser =
                    ptsUserReadService.querySinglePtsDepositUser(member.getUserId(), getPtsServiceCodeByBizType(bizType));

            if (null != ptsDepositUser) {
                Long leftMoney = ptsDepositUser.getOccupyAmount();
                companyInfoVO.setLeftMoney(leftMoney);
            }
        }

        return RPCResult.ok(companyInfoVO);
    }

    /**
     * 获取列表筛选项
     * 
     * @return
     */
    @RequestMapping(value = "/getFilterTypes", method = RequestMethod.GET)
    public RPCResult getFilterTypes() {
        TapSupplierPayFilterVO vo = new TapSupplierPayFilterVO();
        vo.setPsyStatus(SupplierPayStatusEnum.fullMap);
        vo.setEnterPropertyKey(SupplierPayEnterPropertyEnum.fullMap);
        return RPCResult.ok(vo);
    }

    /**
     * 获取收款店铺信息列表
     * 
     * @return
     */
    @RequestMapping(value = "/getReceiveShop", method = RequestMethod.GET)
    public RPCResult getReceiveShop(@RequestParam(name = "bizType", required = false) String bizType) {

        Map<String, String> all = new HashMap<>();
        if(StringUtil.isBlank(bizType)){
            bizType = BizType.CDGYL.name();
        }
        for (SupplierShopEnum shopEnum : SupplierShopEnum.values()) {

            if (shopEnum.getBizType().equalsIgnoreCase(bizType)) {
                all.put(shopEnum.name(), shopEnum.getShopName());

            }

        }

        return RPCResult.ok(all);
    }

    /**
     * 获取扣款记录列表

     *
     * @param status
     * @param companyName
     * @param loginId
     * @return
     */
    @RequestMapping(value = "/getRecordList", method = RequestMethod.GET)
    public RPCResult getRecordList(@RequestParam(name = "status", required = false) String status,
        @RequestParam(name = "companyName", required = false) String companyName,
        @RequestParam(name = "loginId", required = false) String loginId,
        @RequestParam(name = "page", required = false) Integer page,
        @RequestParam(name = "bizType", required = false) String bizType) {

        SupplierPayParam param = new SupplierPayParam();
        RPCResult result = RPCResult.ok();
        // 状态
        if (StringUtils.isNotBlank(status)) {
            SupplierPayStatusEnum statusEnum = SupplierPayStatusEnum.valueOf(status);
            if (null != statusEnum) {
                param.setStatus(statusEnum.name());
            }
        }

        // 公司名称
        if (StringUtils.isNotBlank(companyName)) {
            param.setCompanyName(companyName);
        }

        // loginId
        if (StringUtils.isNotBlank(loginId)) {
            MemberModel member = memberReadService.findMemberByLoginId(loginId);
            if (null != member && StringUtils.isNotBlank(member.getMemberId())) {
                param.setSupplierMemberId(member.getMemberId());
            } else {
                // 输入的loginId找不到的话，不查询
                Paginator paginator = new Paginator(0, 10, 1);
                result.put("paginator", paginator);
                result.put("data", Lists.newArrayList());
                return result;
            }
        }

        // 根据罚没收款账号区分
        if(StringUtil.isNotBlank(bizType)){
            param.setReceiveUserIds(SupplierShopEnum.parseUserIds(bizType));
        }else{
            param.setReceiveUserIds(SupplierShopEnum.parseUserIds(BizType.CDGYL.name()));
        }

        page = null == page ? 1 : page;

        param.setPage(page);
        param.setPageSize(10);
        param.setCountNeeded(true);

        PageList<SupplierPayModel> pageList = supplierPayService.query(param);

        // 默认paginator
        Paginator paginator = null;
        List<SupplierPayVO> voList = Lists.newArrayList();

        if (null != pageList && null != pageList.getPaginator()) {
            paginator =
                new Paginator(pageList.getPaginator().getItems(), pageList.getPaginator().getItemsPerPage(), page);
            
            if (CollectionUtils.isNotEmpty(pageList.getDataList())) {
                List<SupplierPayModel> modelList = pageList.getDataList();
                List<String> memberIds = Lists.newArrayList();
                for (SupplierPayModel model : modelList) {
                    memberIds.add(model.getSupplierMemberId());
                }

                Map<String, String> memberId2LoginIdMap = memberReadService.convertLoginIdsByMemberIds(memberIds);

                Map<String, WinportUrlModel> winportUrlMap = null;

                if (CollectionUtils.isNotEmpty(memberIds)) {
                    try {
                        // 批量查询旺铺地址
                        winportUrlMap = winportUrlService.getUrl(memberIds,
                            WinportUrlEnum.INDEX_URL);
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                    }
                }

                for (SupplierPayModel model : modelList) {
                    SupplierPayVO vo = new SupplierPayVO();
                    vo.setId(model.getId());
                    vo.setCompanyName(model.getCompanyName());
                    vo.setLoginId(memberId2LoginIdMap.get(model.getSupplierMemberId()));
                    vo.setGmtCreate(model.getGmtCreate());
                    vo.setGmtChargeBack(model.getGmtChargeBack());
                    vo.setChargeBackMoney(model.getChargeBackMoney());
                    vo.setReason(model.getReason());
                    vo.setStatus(model.getStatus());
                    vo.setOperator(model.getOperator());
                    vo.setFileLink(model.getFileLink());

                    SupplierInfoParam infoParam = new SupplierInfoParam();
                    infoParam.setMemberId(model.getSupplierMemberId());
                    SupplierInfoModel infoModel = supplierInfoService.queryOne(infoParam, Boolean.TRUE);
                    if (null != infoModel) {
                        vo.setDingdingId(infoModel.getDockingDing());
                        vo.setDingdingName(infoModel.getDockingName());
                    }

                    Long supplierUserId = memberReadService.convertMemberIdToUserId(model.getSupplierMemberId());
                    MemberModel member = memberReadService.findMemberByUserId(supplierUserId);

                    if (null != member && null != winportUrlMap && null != winportUrlMap.get(member.getMemberId())) {
                        vo.setShopUrl(winportUrlMap.get(member.getMemberId()).getIndexUrl());
                    }

                    voList.add(vo);
                }

            }
        }

        if (null == paginator) {
            paginator = new Paginator(0, 10, 1);
        }

        result.put("paginator", paginator);
        result.put("data", voList);

        return result;
    }

    /**
     * 重新扣款
     * 
     * @param id 扣款记录id
     * @return
     */
    @RequestMapping(value = "/reDeductMoney", method = RequestMethod.GET)
    public RPCResult reDeductMoney(HttpServletRequest request, @RequestParam(name = "id") Long id) {
        // 查询扣款信息，如果已经扣款成功，就返回
        SupplierPayModel model = supplierPayService.find(id);
        if (null == model || SupplierPayStatusEnum.SUCCESS.name().equals(model.getStatus())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, ProtectErrorCodeEnum.HAS_PAY_SUCCESS.getMessage());
        }

        // 保障商家id存在（保障为入驻商家）
        String supplierMemberId = model.getSupplierMemberId();
        Long supplierUserId = memberReadService.convertMemberIdToUserId(supplierMemberId);

        SupplierShopEnum shopEnum = SupplierShopEnum.parseByUserId(model.getReceiveUserId());
        if (null == shopEnum) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, ProtectErrorCodeEnum.SHOP_NOT_EXIST.getMessage());
        }

        Result<Boolean> joined;
        if(SupplierShopEnum.ALIQYG.equals(shopEnum)){
            joined = supplierJoinService.isJoined(supplierMemberId,
                SupplierSceneEnum.ALIQYG);
        }else {
            joined = supplierJoinService.isJoined(supplierMemberId,
                SupplierSceneEnum.TIAN_TIAN_FACTORY_STORE);
        }

        if (null == joined || !joined.isSuccess() || null == joined.getModel() || !joined.getModel()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, ProtectErrorCodeEnum.SUPPLIER_NOT_EXIST.getMessage());
        }

        // 保障必须有orderId
        // if (StringUtils.isBlank(model.getOrderId())) {
        // return RPCResult.error(RPCResult.CODE_PARAM_ERROR,
        // ProtectErrorCodeEnum.PAY_ORDER_ID_NOT_EXIST.getMessage());
        // }

        // 保障当前操作的小二和最开始发起扣款的小二是同一个人
        BucSSOUser user = null;

        try {
            user = SimpleUserUtil.getBucSSOUser(request);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        if (null == user || StringUtils.isBlank(user.getLastName())) {
            return RPCResult.error(RPCResult.CODE_FORBIDDEN, ProtectErrorCodeEnum.NEED_LOGIN.getMessage());
        }

        String operator = user.getLastName();

        // if (StringUtils.isBlank(model.getOperator()) || !user.equals(model.getOperator())) {
        // return RPCResult.error(RPCResult.CODE_FORBIDDEN, "different operator");
        // }

        // 扣钱
        String errorCode =
            deductMoney(supplierUserId, shopEnum, operator, String.valueOf(model.getId()), model.getChargeBackMoney(),
            model.getReason());

        ProtectErrorCodeEnum errorEnum = null;

        if (StringUtils.isNotBlank(errorCode)) {
            try {
                errorEnum = ProtectErrorCodeEnum.valueOf(errorCode);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }

        // 更新
        SupplierPayModel updateModel = new SupplierPayModel();
        updateModel.setId(id);
        updateModel.setGmtChargeBack(new Date());
        updateModel.setOperator(operator);
        updateModel
            .setStatus(StringUtils.isBlank(errorCode) ? SupplierPayStatusEnum.SUCCESS.name()
                : SupplierPayStatusEnum.FAIL.name());

        supplierPayService.update(updateModel);

        if (null != errorEnum) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, errorEnum.getMessage());
        }

        if (StringUtils.isNotBlank(errorCode)) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, errorCode);
        }

        return RPCResult.ok();
    }

    /**
     * 上传文件
     * 
     * @return
     */
    @RequestMapping(value = "/uploadFiles", method = RequestMethod.POST)
    public RPCResult uploadFiles(@RequestParam(name = "file") MultipartFile file) {
        if (file.isEmpty()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_REQUIRED);
        }

        String bucketName = "cbu-supplier";

        String uri = "";
        String name;
        try {
            String originName = file.getName();
            String originFullName = file.getOriginalFilename();

            String suffix = null;

            if (StringUtils.isNotBlank(originFullName)) {
                suffix = originFullName.substring(originFullName.lastIndexOf("."));
            }

            name = originName + System.currentTimeMillis();

            if (StringUtils.isNotBlank(suffix)) {
                name = name + "." + suffix;
            }

            byte[] bytes = file.getBytes();
            uri = supplierOssService.putObject(bucketName, name, bytes);
        } catch (Exception e) {
            LOG.error(String.format(
                "CB=[uploadImage.ossUpload] fail, file:%s, exception:%s",
                file.toString(),
                e.getMessage()), e);
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, RPCResult.CODE_SERVER_ERROR);
        }

        if (StringUtils.isNotBlank(uri)) {
            return RPCResult.ok(uri);
        }

        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, RPCResult.CODE_SERVER_ERROR);
    }

    /**
     * 提交扣款
     *
     * @return
     */
    @RequestMapping(value = "/submitRecord", method = RequestMethod.POST)
    public RPCResult submitRecord(HttpServletRequest request, @RequestBody PayParamVO payParamVO) {

        if (null == payParamVO) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }
        
        if (null == payParamVO.getChargeBackMoney()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR,
                "pay money is null");
        }
        
        if (StringUtils.isBlank(payParamVO.getLoginId())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "loginId is null");
        }
        
        if (StringUtils.isBlank(payParamVO.getReason())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "reason is null");
        }
        
        
        if (StringUtils.isBlank(payParamVO.getReceiveShop())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "receive shop is null");
        }
        
        if (StringUtils.isBlank(payParamVO.getFileLink())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "file is null");
        }

        // 操作人
        BucSSOUser user = null;

        try {
            user = SimpleUserUtil.getBucSSOUser(request);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        if (null == user || StringUtils.isBlank(user.getLastName())) {
            return RPCResult.error(RPCResult.CODE_FORBIDDEN, ProtectErrorCodeEnum.NEED_LOGIN.getMessage());
        }

        String operator = user.getLastName();

        // 转换memberId
        MemberModel member = memberReadService.findMemberByLoginId(payParamVO.getLoginId());
        if (null == member || StringUtils.isBlank(member.getMemberId())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, ProtectErrorCodeEnum.MEMBER_ID_NOT_EXIST.getMessage());
        }

        // 商铺需要在枚举范围内
        SupplierShopEnum shopEnum = SupplierShopEnum.valueOf(payParamVO.getReceiveShop());
        if (null == shopEnum) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, ProtectErrorCodeEnum.SHOP_NOT_EXIST.getMessage());
        }

        // 判断如果输入的memberId不是当前入驻的memberId，则返回error
        Result<Boolean> joined;
        if(SupplierShopEnum.ALIQYG.equals(shopEnum)){
            joined = supplierJoinService.isJoined(member.getMemberId(),
                SupplierSceneEnum.ALIQYG);
        }else {
            joined = supplierJoinService.isJoined(member.getMemberId(),
                SupplierSceneEnum.TIAN_TIAN_FACTORY_STORE);
        }
        if (null == joined || !joined.isSuccess() || null == joined.getModel() || !joined.getModel()) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, ProtectErrorCodeEnum.SUPPLIER_NOT_EXIST.getMessage());
        }

        // 金额大于0
        if (payParamVO.getChargeBackMoney() <= 0) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR,
                ProtectErrorCodeEnum.CHARGE_BACK_MONEY_LESS_THAN_0.getMessage());
        }


        // 先插入记录
        SupplierPayModel model = new SupplierPayModel();
        model.setChargeBackMoney(payParamVO.getChargeBackMoney());
        model.setCompanyName(member.getCompanyName());
        model.setFileLink(payParamVO.getFileLink());
        model.setGmtChargeBack(new Date());
        model.setOperator(operator);
        model.setOrderId(payParamVO.getOrderId());
        model.setReason(payParamVO.getReason());
        model.setReceiveUserId(shopEnum.getUserId());
        model.setStatus(SupplierPayStatusEnum.FAIL.name());
        model.setSupplierMemberId(member.getMemberId());

        Long createRes = supplierPayService.create(model);

        if (null == createRes || createRes <= 0L) {
            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, RPCResult.CODE_SERVER_ERROR);
        }

        // 扣款
        String errorCode =
            deductMoney(member.getUserId(), shopEnum, operator, String.valueOf(createRes),
                payParamVO.getChargeBackMoney(), payParamVO.getReason());

        ProtectErrorCodeEnum errorEnum = null;

        // 有错误信息先解析，然后解析失败直接返回
        if (StringUtils.isNotBlank(errorCode)) {
            try {
                errorEnum = ProtectErrorCodeEnum.valueOf(errorCode);

            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }

            if (null != errorEnum) {
                return RPCResult.error(RPCResult.CODE_SERVER_ERROR, errorEnum.getMessage());
            }

            return RPCResult.error(RPCResult.CODE_SERVER_ERROR, errorCode);
        }

        // 更新模型
        SupplierPayModel updateModel = new SupplierPayModel();
        updateModel.setId(createRes);
        updateModel.setGmtChargeBack(new Date());
        updateModel.setStatus(SupplierPayStatusEnum.SUCCESS.name());
        supplierPayService.update(updateModel);

        return RPCResult.ok();
    }

    private String deductMoney(Long supplierUserId, SupplierShopEnum shopEnum, String operator,
        String orderId,
        Long chargeBackMoney, String reason) {


        PtsFundPenaltyRequest penaltyRequest = new PtsFundPenaltyRequest();
        penaltyRequest.setUserId(supplierUserId);
        penaltyRequest.setToUserId(shopEnum.getUserId());
        penaltyRequest.setServiceCode(getPtsServiceCode(shopEnum));
        penaltyRequest.setOperatorId(operator);
        penaltyRequest.setOutBizId(orderId);
        penaltyRequest.setPenaltyAmount(chargeBackMoney);
        penaltyRequest.setRemark("超时退款保障");

        String code = null;

        try {
            ptsUserFundWriteService.penaltyFund(penaltyRequest);
        } catch (ServiceException e) {
            LOG.error(e.getMessage(), e);
            code = e.getErrorCode();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return ProtectErrorCodeEnum.SYSTEM_ERROR.name();
        }

        return code;
    }



    /**
     * 根据不同SupplierShopEnum获取对应的保证金serviceCode
     * @param shopEnum
     * @return
     */
    public static String getPtsServiceCode(SupplierShopEnum shopEnum){
        return getPtsServiceCodeByBizType(shopEnum.getBizType());
    }

    public static String getPtsServiceCodeByScene(SupplierSceneEnum supplierSceneEnum){
        String bizType;
        if(SupplierSceneEnum.ALIQYG.equals(supplierSceneEnum)){
            bizType = BizType.CAIGOU.name();
        }else{
            bizType = BizType.CDGYL.name();
        }
        return getPtsServiceCodeByBizType(bizType);
    }

    public static String getPtsServiceCodeByBizType(String bizType){
        if(BizType.CAIGOU.name().equalsIgnoreCase(bizType)){
            return PtsServiceCode.ALIQIYEGOU.getCode();
        }else{
            return PtsServiceCode.GHZXBZJ.getCode();
        }
    }

}
