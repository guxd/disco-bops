/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.controller.domestic.tb.supplier.operationLog;

import com.alibaba.cbuscm.vo.domestic.tb.supplier.TaoSupplierLogVO;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.shared.discosupplier.enums.operation.OperationActionEnum;
import com.alibaba.china.shared.discosupplier.model.operation.OperationLogModel;
import com.alibaba.china.shared.discosupplier.param.operation.OperationLogParam;
import com.alibaba.china.shared.discosupplier.service.operation.OperationLogService;
import com.alibaba.up.common.mybatis.result.PageList;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.task.Paginator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 操作日志
 *
 * @author yaogaolin
 */
@RestController
@RequestMapping("/supplier/operationLog")
public class OperationLogController {
    private static final Logger LOG = LoggerFactory.getLogger(OperationLogController.class);

    @Autowired
    private OperationLogService operationLogService;
    @Autowired
    private MemberReadService memberReadService;

    /**
     * 操作日志列表
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RPCResult list(HttpServletRequest request,
                          @RequestParam(name = "memberId") String memberId,
                          @RequestParam(name = "scene", required = true) String scene,
                          @RequestParam(name = "module", required = false) String module,
                          @RequestParam(name = "page", defaultValue = "1") Integer page,
                          @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
                          @RequestParam(name = "orderBy", defaultValue = "GMT_CREATE") String orderBy,
                          @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection) {
        OperationLogParam param = new OperationLogParam();
        param.setScene(scene);
        param.setItemId(memberId);
        param.setModule(module);
        param.setPage(page);
        param.setPageSize(itemsPerPage);

        PageList<OperationLogModel> pageList = operationLogService.query(param);
        List<TaoSupplierLogVO> voList = pageList.getDataList().stream().map(i -> {
            TaoSupplierLogVO vo = new TaoSupplierLogVO();
            BeanUtils.copyProperties(i, vo, new String[]{"gmtCreate", "action"});
            if(null != i.getGmtCreate()){
                vo.setGmtCreate(i.getGmtCreate().getTime());
            }
            if (StringUtils.isNotBlank(i.getAction()) && null != OperationActionEnum.parse(i.getAction())) {
                vo.setAction(OperationActionEnum.parse(i.getAction()).getMessage());
            }
            return vo;
        }).collect(Collectors.toList());
        Map<String, Object> result = new HashMap<>();
        result.put("data", voList);

        result.put("paginator", new Paginator(pageList.getPaginator().getItems(), itemsPerPage, page));

        return RPCResult.okNoWrapper(result);
    }

}