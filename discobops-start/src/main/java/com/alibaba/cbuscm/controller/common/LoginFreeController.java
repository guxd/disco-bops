package com.alibaba.cbuscm.controller.common;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.china.shared.discosupplier.open.supplier.service.SupplierLoginFreeService;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @Auther wb-wmq424206@alibaba-inc.com
 * @Date 2019-08-19
 */
@RestController
@RequestMapping("/loginFree")
@Slf4j
public class LoginFreeController {

    @Autowired
    private SupplierLoginFreeService supplierLoginFreeService;

    @RequestMapping("/getToken")
    public RPCResult getLoginFreeToken(HttpServletRequest request) {
        String targetUrl = request.getParameter("targetUrl");
        BucSSOUser bucSSOUser = null;
        Result<String> token = null;
        try {
            // 获取小二工号
            bucSSOUser = SimpleUserUtil.getBucSSOUser(request);
            if (bucSSOUser == null) {
                return RPCResult.error(RPCResult.CODE_REDIRECT, "bucSSOUser is null");
            } else if (bucSSOUser.getEmpId() == null) {
                return RPCResult.error(RPCResult.MSG_ERROR_SYSTEM, RPCResult.MSG_ERROR_CHINESS_SYSTEM);
            }
            // 获取免登token
            token = supplierLoginFreeService.getBUICFreeLoginToken(bucSSOUser.getEmpId(),targetUrl);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return RPCResult.error(RPCResult.MSG_ERROR_SYSTEM, RPCResult.MSG_ERROR_CHINESS_SYSTEM);
        }
        if (token == null || !token.isSuccess()) {
            log.error("takenIsNull","get token is failed");
            return RPCResult.error(RPCResult.MSG_ERROR_SYSTEM, RPCResult.MSG_ERROR_CHINESS_SYSTEM);
        }
        return RPCResult.ok(token.getModel());
    }
}
