package com.alibaba.cbuscm.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.buc.api.exception.BucException;
import com.alibaba.buc.api.exception.BucException;
import com.alibaba.buc.api.model.enhanced.EnhancedUser;
import com.alibaba.buc.api.unit.EnhancedUserQueryReadService;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.boot.velocity.annotation.VelocityLayout;
import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.china.work.service.dior.SteveExternalDiorService;
import com.alibaba.china.work.service.dior.enums.UserSystemEnum;
import com.alibaba.fastjson.JSON;

/**
 * Spring Mvc的根路径、健康检查等。
 * <p>
 * 其中使用了velocity，@VelocityLayout声明了页面使用的layout。详见
 * http://gitlab.alibaba-inc.com/middleware-container/pandora-boot/wikis/spring-boot-velocity
 *
 * 业务场景入口
 * 
 * @author jinxing.yjx
 */
@Controller
public class MainController {
    protected static final Logger LOG = LoggerFactory.getLogger("MainController");

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Value("${profiles.env}")
    private String env;

    @Autowired
    private SteveExternalDiorService steveService;

    @Autowired
    private EnhancedUserQueryReadService enhancedUserQueryService;

    private static final  String  CHANNEL_SCENE = "channel";

    private static final  String  XIAOER_SCENE = "supply";


    /**
     * 首页入口
     * @param request
     * @param context
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @GetMapping("/")
    @VelocityLayout("/velocity/layout/index.vm")
    public String root(HttpServletRequest request, Map<String,Object> context) throws IOException, ServletException {
        BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
        String defaultScene = CHANNEL_SCENE;

        String empId = null == user ? "": user.getEmpId();
        try {
            EnhancedUser enuser = enhancedUserQueryService.getUser(empId);
            String bu = enuser.getBusinessUnit();
            if(!StringUtils.isEmpty(bu) && (bu.toUpperCase().contains("CBU") || bu.toUpperCase().contains("1688"))){
                defaultScene = XIAOER_SCENE;
            }
            String path = request.getParameter("_path_");

            //默认路径到供应商管理
            if(null == path|| "".equals(path)) {
                // 前端要求改的，有问题找前端看
                path = "supply/index/supplier-list";
            }
            String framework = steveService.getEPframeWork(empId, UserSystemEnum.BUC, defaultScene, path);
            context.put("epframework", framework);
            context.put("bizType", StringUtils.isBlank(request.getParameter("bizType")) ? null
                : request.getParameter("bizType").toUpperCase());

        } catch(Exception e){
            LOG.error(e.getMessage(), e);
        }

        return "index";
    }


    /**
     * 测试拉取 默认页面
     */
    @GetMapping("/check")
    public String check() {
        return "success";
    }

    /**
     * 健康检查，系统部署需要
     * 请不要删除！！
     */
    @GetMapping("/checkpreload.htm")
    public @ResponseBody String checkPreload() {
        return "success";
    }

    /**
     * 测试 acl
     * @return
     */
    @GetMapping("/checkacl.htm")
    public @ResponseBody String checkAcl() {
        return "acl test";
    }

    /**
     * 测试 环境配置
     * @return
     */
    @GetMapping("/checkenv.htm")
    public @ResponseBody String checkEnv() {
        return "env = "  + activeProfile + env;
    }
}
