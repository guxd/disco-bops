package com.alibaba.cbuscm.controller.onepiece.selection;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementAdminReadService;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementAdminWriteService;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementScheduleService;
import com.alibaba.cbu.overseas.models.supplement.SupplementItemModel;
import com.alibaba.cbu.overseas.models.supplement.SupplementTaskModel;
import com.alibaba.cbu.overseas.params.supplement.SupplementItemAdminQueryParam;
import com.alibaba.cbu.overseas.params.supplement.SupplementItemAdminQueryParam.FilterEnum;
import com.alibaba.cbu.overseas.params.supplement.SupplementTaskAdminParam;
import com.alibaba.cbu.overseas.wrapper.ResultModel;
import com.alibaba.cbu.overseas.wrapper.ResultPageModel;
import com.alibaba.china.global.business.library.common.NestedPageOf;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.interfaces.onepiece.authority.AuthorityService;
import com.alibaba.china.global.business.library.interfaces.onepiece.authority.decisiondomain.DecisionDomainService;
import com.alibaba.china.global.business.library.interfaces.onepiece.selectioncenter.CommonSelectionTaskService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionResultReadService;
import com.alibaba.china.global.business.library.models.onepiece.authority.DecisionDomainModel;
import com.alibaba.china.global.business.library.models.onepiece.selectioncenter.CommonSelectionAdminModel;
import com.alibaba.china.global.business.library.models.onepiece.selectioncenter.CommonSelectionTaskModel;
import com.alibaba.china.global.business.library.models.onepiece.selectioncenter.SelectionTaskDTO;
import com.alibaba.china.global.business.library.models.selection.SelectionCountResult;
import com.alibaba.china.global.business.library.params.onepiece.authority.AuthorityCheckParam;
import com.alibaba.china.global.business.library.params.onepiece.authority.DecisionDomainParam;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.CommonSelectionTaskCreateParam;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.CommonSelectionTaskQueryParam;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.SelectionCreateTaskVO;
import com.alibaba.china.global.business.library.params.selection.SelectionCountQueryParam;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.alibaba.china.global.business.library.enums.onepiece.OnePieceTaskTypeEnum
    .ONE_PIECE_ADD_GOODS_AUTO_SELECT_TASK;

/**
 * 选品任务相关
 *
 * @author lichao.wlc
 * @date 2019/06/10
 */
@RestController
@RequestMapping("/one-piece/selection/task")
@Slf4j
public class TaskController {
    @Autowired
    private CommonSelectionTaskService commonSelectionTaskService;
    @Autowired
    private AuthorityService authorityService;
    @Autowired
    private DecisionDomainService decisionDomainService;
    @Autowired
    private SelectionResultReadService selectionResultReadService;
    @Autowired
    private SupplementAdminReadService supplementAdminReadService;
    @Autowired
    private SupplementAdminWriteService supplementAdminWriteService;
    /**
     * 任务类型权限验证
     *
     * @param selectionCreateTaskVO
     * @param request
     * @return
     */
    @PostMapping("/checkPermissions")
    public RPCResult tasks(@RequestBody SelectionCreateTaskVO selectionCreateTaskVO, HttpServletRequest request) {
        try {
            List<String> keys = Arrays.asList(selectionCreateTaskVO.getAuthorityKeys());
            String empId = SimpleUserUtil.getBucSSOUser(request).getEmpId();

            AuthorityCheckParam authorityCheckParam = AuthorityCheckParam.builder()
                    .empId(empId)
                    .authorityKeys(keys)
                    .build();
            ResultOf<Map<String, Object>> listResultOf = authorityService.checkPermissions(authorityCheckParam);
            if (listResultOf != null && listResultOf.isSuccess()) {
                return RPCResult.ok(listResultOf.getData());
            }
            return RPCResult.error(listResultOf.getErrorCode(), listResultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("checkPermissions error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "checkPermissions error");
    }

    /**
     * 任务类型站点列表
     *
     * @param type 任务类型
     * @return
     */
    @RequestMapping("/sites")
    public RPCResult taskTypeSites(@RequestParam String type) {
        try {
            ResultOf<List<DecisionDomainModel>> resultOf = decisionDomainService.selectDecisionDomainByParam(DecisionDomainParam.builder().taskType(type).build());
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData().get(0).getSites().split(","));
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("taskTypeSites error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "taskTypeSites error");
    }

    /**
     * 任务列表
     *
     * @param self     self=true我的任务，self=false全部任务
     * @param id       任务id
     * @param type     任务类型
     * @param name     任务名称
     * @param admin    管理员
     * @param pageSize 分页大小
     * @param pageNo   第几页
     * @return 分页列表
     */
    @RequestMapping("/list")
    public RPCResult tasks(@RequestParam Boolean self,
                           @RequestParam Long id,
                           @RequestParam String type,
                           @RequestParam String name,
                           @RequestParam String admin,
                           @RequestParam Integer pageSize,
                           @RequestParam Integer pageNo,
                           HttpServletRequest request) {
        try {
            String empId = SimpleUserUtil.getBucSSOUser(request).getEmpId();
            CommonSelectionTaskQueryParam queryParam = CommonSelectionTaskQueryParam.builder()
                    .allTaskFlag(!self)
                    .taskId(id)
                    .taskName(name)
                    .taskType(type)
                    .adminId(ObjectUtils.isEmpty(admin) ? empId : admin)
                    .loginId(empId)
                    .pageSize(pageSize)
                    .pageNo(pageNo)
                    .build();

            ResultOf<NestedPageOf<CommonSelectionTaskModel>> resultOf = commonSelectionTaskService.query(queryParam);
            if (resultOf.isSuccess()) {
                NestedPageOf<CommonSelectionTaskModel> nestedPageOf = resultOf.getData();
                if (CollectionUtils.isEmpty(nestedPageOf.getList())) {
                    return RPCResult.ok(Lists.newArrayList());
                }
                List<SelectionTaskDTO> taskModelList = nestedPageOf.getList().stream()
                        .map(commonSelectionTaskModel -> commonSelectionTaskModel2SelectionTaskDTO(commonSelectionTaskModel,
                                empId, "LIST"))
                        .collect(Collectors.toList());

                NestedPageOf newNestedPageOf = new NestedPageOf(nestedPageOf.getPageTotal(), nestedPageOf.getPageSize(), nestedPageOf.getPageNo(), taskModelList);
                return RPCResult.ok(newNestedPageOf);
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("tasks error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "tasks error");
    }

    /**
     * 创建任务
     *
     * @param selectionCreateTaskVO 创建任务参数聚合对象
     * @return 成功or失败
     */
    @PostMapping
    public RPCResult createTask(@RequestBody SelectionCreateTaskVO selectionCreateTaskVO, HttpServletRequest request) {
        try {
            //判断当前人是否在admin中
            BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
            List<CommonSelectionAdminModel> adminList = selectionCreateTaskVO.getAdmin();
            boolean flag = adminList.stream().anyMatch(commonSelectionAdminModel -> commonSelectionAdminModel.getEmpId().equals(user.getEmpId()));
            if (!flag) {
                adminList.add(CommonSelectionAdminModel.builder().empId(user.getEmpId()).stageName(StringUtils.hasText(user.getNickNameCn()) ? user.getNickNameCn() : user.getLastName()).build());
            }

            CommonSelectionTaskCreateParam selectionTaskCreateParam = selectionTaskVO2BusinessModel(
                    selectionCreateTaskVO);
            ResultOf<CommonSelectionTaskModel> resultOf = commonSelectionTaskService.create(selectionTaskCreateParam);
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("create task error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "create task error");
    }

    /**
     * drizzle.zk test
     *
     * @param request
     * @return
     */
    @RequestMapping("/tesTask")
    public RPCResult testcreateTask(
            @RequestParam Integer autoUpdate
            , HttpServletRequest request) {
        try {
            //判断当前人是否在admin中
            BucSSOUser user = SimpleUserUtil.getBucSSOUser(request);
            List<CommonSelectionAdminModel> adminList = Lists.newArrayList();
            CommonSelectionAdminModel commonSelectionAdminModel1 = new CommonSelectionAdminModel();
            commonSelectionAdminModel1.setEmpId("165480");
            commonSelectionAdminModel1.setStageName("琛琪");
            adminList.add(commonSelectionAdminModel1);

            SelectionCreateTaskVO selectionCreateTaskVO = new SelectionCreateTaskVO();
            selectionCreateTaskVO.setAdmin(adminList);
            selectionCreateTaskVO.setAutoUpdate(autoUpdate);
            selectionCreateTaskVO.setDescription("test 描述");
            selectionCreateTaskVO.setName("testName");
            selectionCreateTaskVO.setType("ONE_PIECE_ADD_GOODS_AUTO_SELECT_TASK");
            selectionCreateTaskVO.setVisibility(0);

            boolean flag = adminList.stream().anyMatch(commonSelectionAdminModel -> commonSelectionAdminModel.getEmpId().equals(user.getEmpId()));
            if (!flag) {
                adminList.add(CommonSelectionAdminModel.builder().empId(user.getEmpId()).stageName(StringUtils.hasText(user.getNickNameCn()) ? user.getNickNameCn() : user.getLastName()).build());
            }

            CommonSelectionTaskCreateParam selectionTaskCreateParam = selectionTaskVO2BusinessModel(
                    selectionCreateTaskVO);

            ResultOf<CommonSelectionTaskModel> resultOf = commonSelectionTaskService.create(selectionTaskCreateParam);
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("create task error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "create task error");
    }


    /**
     * 复制任务
     *
     * @param selectionCreateTaskVO 被复制任务
     * @return 成功or失败
     */
    @PostMapping("/copy")
    public RPCResult copyTask(@RequestBody SelectionCreateTaskVO selectionCreateTaskVO) {
        try {
            ResultOf<CommonSelectionTaskModel> resultOf = commonSelectionTaskService.copyTaskById(selectionCreateTaskVO.getId());
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("copy task error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "copy task error");
    }

    /**
     * 修改任务
     *
     * @param selectionCreateTaskVO 修改任务参数聚合对象
     * @return 成功or失败
     */
    @PutMapping
    public RPCResult updateTask(@RequestBody SelectionCreateTaskVO selectionCreateTaskVO, HttpServletRequest request) {
        try {
            CommonSelectionTaskCreateParam selectionTaskCreateParam = selectionTaskVO2BusinessModel(
                    selectionCreateTaskVO);
            selectionTaskCreateParam.setLoginPerson(SimpleUserUtil.getBucSSOUser(request).getEmpId());
            ResultOf<CommonSelectionTaskModel> resultOf = commonSelectionTaskService.update(selectionTaskCreateParam);
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("update task error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "update task error");
    }

    /**
     * 删除任务
     *
     * @param selectionCreateTaskVO 被删除任务
     * @return 成功or失败
     */
    @DeleteMapping
    public RPCResult deleteTask(@RequestBody SelectionCreateTaskVO selectionCreateTaskVO) {

        log.info("------------------------删除任务------------------------------------------------");
        try {
            supplementAdminWriteService.unpublishSupplementTask(SupplementTaskAdminParam.builder()
                .adminId("system")
                .taskId(selectionCreateTaskVO.getId())
                .build());
            log.info("------------------------删除任务1------------------------------------------------");
            ResultOf<Boolean> resultOf = commonSelectionTaskService.deleteById(selectionCreateTaskVO.getId());
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
            log.info("------------------------删除任务2------------------------------------------------");
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("delete task error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "delete task error");
    }

    /**
     * 单个任务详情
     *
     * @param id 任务id
     * @return 任务详情
     */
    @GetMapping
    public RPCResult taskDetail(@RequestParam Long id, HttpServletRequest request) {
        try {
            log.info("-----------单个任务详情，taskId:{}", id);
            ResultOf<CommonSelectionTaskModel> resultOf = commonSelectionTaskService.getTaskInfoByTaskId(id);
            if (resultOf.isSuccess()) {
                CommonSelectionTaskModel commonSelectionTaskModel = resultOf.getData();
                String empId = SimpleUserUtil.getBucSSOUser(request).getEmpId();
                SelectionTaskDTO selectionTaskDTO = commonSelectionTaskModel2SelectionTaskDTO(commonSelectionTaskModel, empId, "DETAIL");
                return RPCResult.ok(selectionTaskDTO);
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("delete task error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "delete task error");
    }

    /**
     * 开始圈选任务
     *
     * @param selectionCreateTaskVO 任务id
     * @param request
     * @return
     */
    @PostMapping("/start")
    public RPCResult startSelection(@RequestBody SelectionCreateTaskVO selectionCreateTaskVO, HttpServletRequest request) {
        try {
            log.info("开始圈选任务,param is:{}", JSON.toJSONString(selectionCreateTaskVO));
            ResultOf<Boolean> resultOf = commonSelectionTaskService.startSelection(selectionCreateTaskVO.getId());
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
            return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("startSelection task error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "startSelection task error");
    }


    private CommonSelectionTaskCreateParam selectionTaskVO2BusinessModel(SelectionCreateTaskVO selectionCreateTaskVO) {
        return CommonSelectionTaskCreateParam.builder()
                .taskId(selectionCreateTaskVO.getId())
                .taskDesc(selectionCreateTaskVO.getDescription())
                .taskName(selectionCreateTaskVO.getName())
                .taskType(selectionCreateTaskVO.getType())
                .admins(selectionCreateTaskVO.getAdmin())
                .auditType(selectionCreateTaskVO.getAutoConfirm())
                .publicFlag(selectionCreateTaskVO.getVisibility())
                .updateFlag(selectionCreateTaskVO.getAutoUpdate())
                .build();
    }

    private SelectionTaskDTO commonSelectionTaskModel2SelectionTaskDTO(CommonSelectionTaskModel commonSelectionTaskModel, String empId, String type) {
        Long confirmCount = 0L;
        Long waitConfirmCount = 0L;
        if ("LIST".equals(type)) {
            ResultOf<SelectionCountResult> resultResultOf = selectionResultReadService.queryCount(SelectionCountQueryParam.builder().taskId(commonSelectionTaskModel.getTaskId())
                    .confirmTagId(commonSelectionTaskModel.getConfirmTag())
                    .waitConfirmTagId(commonSelectionTaskModel.getSearchTag()).build());
            if (resultResultOf.isSuccess()) {
                SelectionCountResult selectionCountResult = resultResultOf.getData();
                confirmCount = selectionCountResult.getConfirmCount();
                waitConfirmCount = selectionCountResult.getWaitConfirmCount();
            }
            if (ONE_PIECE_ADD_GOODS_AUTO_SELECT_TASK.getTaskType().equals(commonSelectionTaskModel.getSubTaskType())) {
                ResultPageModel<SupplementItemModel> items = supplementAdminReadService.querySupplementItem(
                    SupplementItemAdminQueryParam.builder()
                        .adminId(empId)
                        .taskId(commonSelectionTaskModel.getTaskId())
                        .filter(FilterEnum.PUBLISH.name())
                        .pageNo(1)
                        .pageSize(1)
                        .build());
                if (!items.isSuccess() || items.getModel() == null || items.getModel().isEmpty()) {
                    confirmCount = 0L;
                } else {
                    confirmCount = (long)items.getTotal();
                }
            }
        } else if ("DETAIL".equals(type)) {
            confirmCount = commonSelectionTaskModel.getItemCount();
        }

        return SelectionTaskDTO.builder()
                .admin(commonSelectionTaskModel.getAdmins())
                .autoConfirm(commonSelectionTaskModel.getAuditType())
                .autoUpdate(commonSelectionTaskModel.getUpdateFlag())
                .gmtCreate(commonSelectionTaskModel.getGmtCreate().getTime())
                .gmtModified(commonSelectionTaskModel.getGmtModified().getTime())
                .id(commonSelectionTaskModel.getTaskId())
                .name(commonSelectionTaskModel.getTaskName())
                .pendingProductCount(waitConfirmCount)
                .selectedProductCount(confirmCount)
                .permission(hasPermission(empId, commonSelectionTaskModel.getAdmins()) ? 1 : 0)
                .type(commonSelectionTaskModel.getSubTaskType())
                .status(commonSelectionTaskModel.getTaskStatus())
                .visibility(commonSelectionTaskModel.getPublicFlag())
                .description(commonSelectionTaskModel.getTaskDesc())
                .specialFields(commonSelectionTaskModel.getSpecialFields())
                .build();
    }

    private Boolean hasPermission(String empId, List<CommonSelectionAdminModel> admins) {
        if (admins == null || admins.size() == 0) {
            return true;
        }
        return admins.stream().anyMatch(
                commonSelectionAdminModel -> empId.equals(commonSelectionAdminModel.getEmpId()));
    }

}
