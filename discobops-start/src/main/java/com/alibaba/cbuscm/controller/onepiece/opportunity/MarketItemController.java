package com.alibaba.cbuscm.controller.onepiece.opportunity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbuscm.contants.MarketConstants;
import com.alibaba.cbuscm.vo.onepiece.opportunity.ItemVO;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketItemVO;
import com.alibaba.china.dw.dataopen.api.OrderBy;
import com.alibaba.china.dw.dataopen.api.OrderBy.Order;
import com.alibaba.china.dw.dataopen.api.QueryException;
import com.alibaba.china.dw.dataopen.api.Result;
import com.alibaba.china.dw.dataopen.api.Results;
import com.alibaba.china.dw.dataopen.api.SQLIDQueryAPI;
import com.alibaba.china.dw.dataopen.api.User;
import com.alibaba.up.common.mybatis.result.ResultError;

import com.ali.unit.rule.util.lang.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 市场商品列表查询
 * Created by duanyang.zdy on 2019/9/3.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class MarketItemController {

    @Resource
    private SQLIDQueryAPI sqlidQueryService;

    private static final String DEFAULT_SALES30D = "0.0";

    private static final Double DEFAULT_PRICE = 0.0d;



    /**
     *
     * @param statDate 通讯表日期
     * @param marketId 市场ID
     * @param isGlobal 是否全球
     * @param pageIndex 当前页
     * @param pageNum 每页数量
     * @param orderByField 排序字段
     * @param orderByType 排序类型
     * @param request 请求参数
     * @return
     */
    @RequestMapping("/marketDetail/queryItem")
    public Object queryItem(@RequestParam(name="statDate") String statDate,
                                          @RequestParam(name="marketId") String marketId,
                                          @RequestParam(name="isGlobal",defaultValue = "1") String isGlobal,
                                          @RequestParam(name="pageIndex",defaultValue = "1") Integer pageIndex,
                                          @RequestParam(name="pageNum",defaultValue = "50") Integer pageNum,
                                          @RequestParam(name="orderByField",defaultValue = "sales30d") String orderByField,
                                          @RequestParam(name="orderByType",defaultValue = "desc") String orderByType,
                                          HttpServletRequest request) {

        //校验参数
        if (StringUtils.isBlank(statDate) || StringUtils.isBlank(marketId)) {
            return ResultOf.error(ResultError.MSG_CODE_PARAM_ERROR, "param is null");
        }

        // 建立查询条件
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("stat_date", statDate);
        conditions.put("market_id", marketId);
        //设置返回类型
        String[] return_item_list_fields = {
            "stat_date", "market_id", "item_id", "site", "site_id",
            "item_detail_url", "item_title_en", "item_title_zh", "market_name", "original_pic_url",
            "oss_pic_url", "price", "sales30d"};

        String[] return_item_count_fields = {"stat_date","market_id","item_cnt"};

        User user = getUserInfo();

        try {


            //获取商品查询列表
            List<OrderBy> orderByList = new ArrayList<>();
            orderByList.add(new OrderBy(orderByField, orderByType.equals("asc")?Order.ASC:Order.DESC));

            Results itemListResults = sqlidQueryService.list(user, MarketConstants.MARKET_ITEM_SQL_ID, conditions, return_item_list_fields,orderByList,(pageIndex - 1) * pageNum,pageNum);
            if (itemListResults == null || itemListResults.getResult()==null || itemListResults.getResult().isEmpty()) {
                return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, itemListResults.getMessage());
            }
            if (!itemListResults.isSuccessed()) {
                return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, itemListResults.getMessage());
            }

            //获取商品查询总数

            Result itemCountResults = sqlidQueryService.get(user,MarketConstants.MARKET_ITEM_PAGE_COUNT_SQL_ID,conditions,return_item_count_fields);
            if (itemCountResults == null || itemCountResults.getResult()==null || itemCountResults.getResult().isEmpty()) {
                return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, itemCountResults.getMessage());
            }
            if (!itemCountResults.isSuccessed()) {
                return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, itemCountResults.getMessage());
            }

            return ResultOf.general(buildMarketItemVO(itemListResults.getResult(),itemCountResults.getResult(),pageIndex,pageNum));

        }catch (QueryException e){

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, "query 出现异常 " + e.getMessage());

        }catch (Exception e) {

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, "query 出现异常 " + e.getMessage());
        }
    }

    //生成OneService User信息
    static User getUserInfo() {
        User user = new User();
        user.setAppName(MarketConstants.AppName);
        user.setName(MarketConstants.AppName);
        user.setPassword(MarketConstants.Pass);
        return user;
    }

    private MarketItemVO buildMarketItemVO(List<Map<String, Object>> maps,Map<String, Object> count,Integer pageIndex,Integer pageNum){

        List<ItemVO> listItems = new ArrayList<>();


        maps.stream().forEach(map->{

            ItemVO itemVO = ItemVO.builder()
                .itemId(String.valueOf(map.get("ITEM_ID")))
                .itemName(String.valueOf(map.get("ITEM_TITLE_ZH")))
                .detailUrl(String.valueOf(map.get("ITEM_DETAIL_URL")))
                .picUrl(map.get("OSS_PIC_URL")!=null?String.valueOf(map.get("OSS_PIC_URL")):String.valueOf(map.get("ORIGINAL_PIC_URL")))
                .price(map.get("PRICE")!=null?(double)Math.round(Double.valueOf(String.valueOf(map.get("PRICE")))*100)/100:DEFAULT_PRICE)
                .sales30d(map.get("SALES30D")!=null?String.valueOf(map.get("SALES30D")):DEFAULT_SALES30D)
                .siteCode(String.valueOf(map.get("SITE")))
                .build();

            listItems.add(itemVO);
        });

        MarketItemVO vo = MarketItemVO.builder()
            .marketId(String.valueOf(maps.get(0).get("MARKET_ID")))
            .marketName(String.valueOf(maps.get(0).get("MARKET_NAME")))
            .stat_date(String.valueOf(maps.get(0).get("STAT_DATE")))
            .data(listItems)
            .build();

        vo.setPageNo(pageIndex);
        vo.setPageSize(pageNum);
        //获取总量需要重新再封装一个接口
        vo.setTotal(count.get("ITEM_CNT")!=null?Long.valueOf(String.valueOf(count.get("ITEM_CNT"))):0L);
        //vo.setTotal(0L);

        return vo;

    }
}
