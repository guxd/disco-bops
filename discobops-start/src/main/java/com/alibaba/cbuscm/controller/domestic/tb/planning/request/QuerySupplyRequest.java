package com.alibaba.cbuscm.controller.domestic.tb.planning.request;

import lombok.Data;
import lombok.ToString;

/**
 * @author langjing
 * @date 2019/8/30 3:06 下午
 */
@Data
@ToString
public class QuerySupplyRequest {

    private String loginIds;

    private Long sourceId;
}
