package com.alibaba.cbuscm.controller.domestic.tb.planning.request;

import lombok.Data;
import lombok.ToString;

/**
 * @author wb-qiuth
 * 添加竞品信息接收参数ruls
 */
@Data
@ToString
public class UrlDesignRequest {
    private String urls;
}
