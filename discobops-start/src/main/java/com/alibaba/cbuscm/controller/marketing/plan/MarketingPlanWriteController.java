package com.alibaba.cbuscm.controller.marketing.plan;

import com.alibaba.cbu.panama.dc.client.model.marketingplan.MarketingPlanInitDTO;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MaterialDTO;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.RestResult;
import com.alibaba.cbuscm.service.marketing.plan.MarketingPlanWriteAdapter;
import com.alibaba.cbuscm.utils.validate.ValidateUtil;
import com.alibaba.cbuscm.vo.marketing.plan.MarketingPlanVO;
import com.alibaba.cbuscm.vo.marketing.plan.req.MarketingPlanReqVO;
import com.alibaba.cbuscm.vo.marketing.plan.req.MaterialReqVO;
import com.alibaba.cbuscm.vo.marketing.plan.req.QualificationReqVO;
import com.alibaba.cbuscm.vo.marketing.plan.req.WhiteItemIdListReqVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author <a href="mailto:jiangbo.jjb@alibaba-inc.com">半更</a>
 * @since 2019/9/30
 */
@RestController
@RequestMapping(path = "/marketingplan/write", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MarketingPlanWriteController {

    @Autowired
    private MarketingPlanWriteAdapter marketingPlanWriteAdapter;

    /**
     * 创建活动（基础信息）
     *
     * @param marketingPlanInitDTO
     * @return
     */
    @PutMapping("/saveBaseinfo")
    public RestResult saveBaseinfo(@RequestBody MarketingPlanInitDTO marketingPlanInitDTO) {
        ValidateUtil.validate(marketingPlanInitDTO);
        return marketingPlanWriteAdapter.saveBaseinfo(marketingPlanInitDTO);
    }

    /**
     * 增加资质配置
     *
     * @param reqVO marketingPlanId qcType qcInitDTO
     * @return
     * @see com.alibaba.cbu.panama.dc.client.constants.marketingplan.BizExtensionTypeEnum
     */
    @PutMapping("/addQualification")
    public RestResult<Boolean> addQualification(@RequestBody QualificationReqVO reqVO) {
        ValidateUtil.validate(reqVO);
        return marketingPlanWriteAdapter.addQualification(reqVO);
    }

    /**
     * 移除资质配置
     * @param reqVO marketingPlanId 营销活动id;qcType:资质类型;qcCode:资质code
     * @return
     * @see com.alibaba.cbu.panama.dc.client.constants.marketingplan.BizExtensionTypeEnum
     */
    @DeleteMapping("/removeQualification")
    public RestResult removeQualification(@RequestBody QualificationReqVO reqVO) {
        if (Objects.isNull(reqVO.getMarketingPlanId())){
            return RestResult.fail("001","marketingPlanId为空");
        }
        if (StringUtils.isBlank(reqVO.getQcType())){
            return RestResult.fail("001","qcType为空");
        }
        if (Objects.isNull(reqVO.getQcCode())){
            return RestResult.fail("001","qcCode为空");
        }
        return marketingPlanWriteAdapter.removeQualification(reqVO);
    }
    /**
     * 增加素材配置
     * 如果已经存在，以最新的数据为准，覆盖掉。通过code去重
     * @param reqVO marketingPlanId：营销活动id；material：素材信息
     * @return
     */
    @PutMapping("/addMaterial")
    public RestResult addMaterial(@RequestBody MaterialReqVO reqVO) {
        ValidateUtil.validate(reqVO.getMaterial());
        if (Objects.isNull(reqVO.getMarketingPlanId())){
            return RestResult.fail("001","marketingPlanId为空");
        }
        return marketingPlanWriteAdapter.addMaterial(reqVO);
    }
    /**
     * 移除素材配置
     *
     * @param reqVO marketingPlanId 营销活动id；materialCode：素材信息code
     * @return
     */
    @DeleteMapping("/removeMaterial")
    public RestResult removeMaterial(@RequestBody MaterialReqVO reqVO) {
        if (Objects.isNull(reqVO.getMarketingPlanId())){
            return RestResult.fail("001","marketingPlanId为空");
        }
        if (StringUtils.isBlank(reqVO.getMaterialCode())){
            return RestResult.fail("001","materialCode为空");
        }
        return marketingPlanWriteAdapter.removeMaterial(reqVO.getMarketingPlanId(),reqVO.getMaterialCode());
    }

    /**
     * 增加商品白名单
     * 修改资质列表中对应的白名单资质，并记录操作日志。通过set去重
     * @param reqVO：marketingPlanId：活动id；itemIdList：淘系商品的Id白名单列表；operator：操作人
     * @return
     */
    @PutMapping("/addWhiteItemIdList")
    public RestResult addWhiteItemIdList(@RequestBody WhiteItemIdListReqVO reqVO) {
        if (Objects.isNull(reqVO.getMarketingPlanId())){
            return RestResult.fail("001","marketingPlanId为空");
        }
        if (CollectionUtils.isEmpty(reqVO.getItemIdList())){
            return RestResult.fail("001","itemIdList为空");
        }
        List<Long> list = buildWhiteItemLongIdParam(reqVO);

        String operator = BopsLogUtil.getNickNamecn();
        if (StringUtils.isBlank(operator)){
            return RestResult.fail("001","operator为空");
        }
        return marketingPlanWriteAdapter.addWhiteItemIdList(reqVO.getMarketingPlanId(),list,operator);
    }

    private List<Long> buildWhiteItemLongIdParam(@RequestBody WhiteItemIdListReqVO reqVO) {
        List<String> itemIdList = reqVO.getItemIdList();
        List<Long> list = new ArrayList<>();
        for (String s : itemIdList) {
            list.add(Long.valueOf(s));
        }
        return list;
    }

    /**
     * 移除商品白名单
     * @param reqVO：marketingPlanId：活动id；itemIdList：淘系商品的Id白名单列表；operator：操作人
     * @return
     */
    @DeleteMapping("/removeWhiteItemIdList")
    public RestResult removeWhiteItemIdList(@RequestBody WhiteItemIdListReqVO reqVO) {
        if (Objects.isNull(reqVO.getMarketingPlanId())){
            RestResult.fail("001","marketingPlanId为空");
        }
        if (CollectionUtils.isEmpty(reqVO.getItemIdList())){
            RestResult.fail("001","itemIdList为空");
        }
        List<Long> list = buildWhiteItemLongIdParam(reqVO);

        String operator = BopsLogUtil.getNickNamecn();
        if (StringUtils.isBlank(operator)){
            RestResult.fail("001","operator为空");
        }
        return marketingPlanWriteAdapter.removeWhiteItemIdList(reqVO.getMarketingPlanId(),list,operator);
    }

    /**
     * 发布活动
     * @param reqVO：marketingPlanId：活动id：operator：操作人
     * @return
     */
    @PostMapping("/publish")
    public RestResult publish(@RequestBody MarketingPlanReqVO reqVO) {
        if (Objects.isNull(reqVO.getMarketingPlanId())){
            RestResult.fail("001","marketingPlanId为空");
        }
        if (StringUtils.isBlank(reqVO.getOperator())){
            RestResult.fail("001","operator为空");
        }
        return marketingPlanWriteAdapter.publishMarketingPlan(reqVO.getMarketingPlanId(),reqVO.getOperator());
    }
}
