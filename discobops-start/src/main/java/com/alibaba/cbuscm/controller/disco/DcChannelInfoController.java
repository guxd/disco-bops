package com.alibaba.cbuscm.controller.disco;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.buc.api.exception.BucException;
import com.alibaba.buc.api.model.enhanced.EnhancedUser;
import com.alibaba.buc.api.unit.EnhancedUserQueryReadService;
import com.alibaba.cbu.disco.channel.api.constants.BusinessModeEnum;
import com.alibaba.cbu.disco.channel.api.constants.CurrencyTypeEnum;
import com.alibaba.cbu.disco.channel.api.constants.DistributeStatusEnum;
import com.alibaba.cbu.disco.channel.api.constants.FeeTypeEnum;
import com.alibaba.cbu.disco.channel.api.constants.PricingStrategyEnum;
import com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum;
import com.alibaba.cbu.disco.channel.api.constants.ChannelSubMarketEnum;
import com.alibaba.cbu.disco.channel.api.constants.SellerTypeEnum;
import com.alibaba.cbu.disco.channel.api.constants.ShopConfigStatusEnum;
import com.alibaba.cbu.disco.channel.api.constants.ShopTypeEnum;
import com.alibaba.cbu.disco.channel.api.model.ChannelBaseResult;
import com.alibaba.cbu.disco.channel.api.model.ChannelPageResult;
import com.alibaba.cbu.disco.channel.api.model.ChannelShopDTO;
import com.alibaba.cbu.disco.channel.api.model.PricingFactorDTO;
import com.alibaba.cbu.disco.channel.api.model.SubMarketInfoDTO;
import com.alibaba.cbu.disco.channel.api.model.URCResDTO;
import com.alibaba.cbu.disco.channel.api.model.vo.ChannelShopVO;
import com.alibaba.cbu.disco.channel.api.model.vo.EmpVO;
import com.alibaba.cbu.disco.channel.api.param.ChannelShopQueryParam;
import com.alibaba.cbu.disco.channel.api.param.ChannelShopUpdateParam;
import com.alibaba.cbu.disco.channel.api.service.DiscoChannelPriceService;
import com.alibaba.cbu.disco.channel.api.service.DiscoChannelShopService;
import com.alibaba.cbu.disco.shared.common.constant.ErrorCodeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcChannelInfoService;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcMultiUserModel;
import com.alibaba.cbuscm.logic.DiscoLogic;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.fastjson.JSON;

import com.beust.jcommander.internal.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * DcChannelInfoController
 *
 * @author aniu.nxh
 * @date 2019/07/12 16:44
 */
@RestController
@RequestMapping(value = "/dcChannelInfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class DcChannelInfoController {

	@Autowired
	private DcChannelInfoService dcChannelInfoService;
	@Autowired
	private DiscoLogic discoLogic;
	@Autowired
	private DiscoChannelShopService discoChannelShopService;
	@Autowired
	private DiscoChannelPriceService discoChannelPriceService;
	@Autowired
	private EnhancedUserQueryReadService enhancedUserQueryService;

	@ResponseBody
	@RequestMapping(value = "/listChannelShops", method = RequestMethod.GET)
	public PageOf<ChannelShopVO> listChannelShops(HttpServletRequest request,
			@RequestParam(name = "channelMarket", required = false) String channelMarket,
			@RequestParam(name = "sellerName", required = false) String sellerName,
			@RequestParam(name = "status", required = false) String status,
			@RequestParam(name = "shopType", required = false) String shopType,
			@RequestParam(name = "owner", required = false) String owner,
			@RequestParam(name = "pageSize", required = false) Integer pageSize,
			@RequestParam(name = "pageNo", required = false) Integer pageNo) {
		String logPrefix = "DcChannelInfoController#listChannelShops#";
		log.error(logPrefix + "record");
		PageOf<ChannelShopVO> result = new PageOf<ChannelShopVO>();
		result.setSuccess(false);
		try {
			ChannelShopQueryParam param = new ChannelShopQueryParam();
			param.setPageIdx(pageNo == null ? 1 : pageNo);
			param.setPageSize(pageSize == null ? 10 : pageSize);
			param.setChannelMarket(channelMarket);
			param.setSellerName(sellerName);
			param.setShopType(shopType);
			param.setStatus(status);
			param.setOwner(owner);
			ChannelBaseResult<ChannelPageResult<ChannelShopDTO>> pageResult = discoChannelShopService
					.listChannelShopsByPage(param);
			if (!pageResult.isSuccess()) {
				throw new Exception(pageResult.getErrorDesc());
			}
			List<ChannelShopVO> list = Lists.newArrayList();
			ChannelPageResult<ChannelShopDTO> pageInfo = pageResult.getData();
			for (ChannelShopDTO dto : pageInfo.getData()) {
				ChannelShopVO vo = this.toVO(dto);
				list.add(vo);
			}
			result.setData(list);
			result.setPageNo(pageInfo.getPageNo());
			result.setPageSize(pageInfo.getPageSize());
			result.setTotal(pageInfo.getTotal());
			result.setSuccess(true);
		} catch (Exception e) {
			log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue(), e);
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/listSubMarkets", method = RequestMethod.GET)
	public ChannelBaseResult<List<SubMarketInfoDTO>> listSubMarkets(HttpServletRequest request,
			@RequestParam(name = "includeDeprecated") Boolean includeDeprecated) {
		String logPrefix = "DcChannelInfoController#listSubMarkets#";
		String paramInfo = "#includeDeprecated:" + includeDeprecated + "#";

		log.error(logPrefix + "record" + paramInfo);

		ChannelBaseResult<List<SubMarketInfoDTO>> result = new ChannelBaseResult<>();
		result.setSuccess(false);
		try {

			result = discoChannelShopService.listSubMarketInfos(includeDeprecated);
			if (!result.isSuccess()) {
				throw new Exception(result.getErrorDesc());
			}
			return result;
		} catch (Exception e) {
			log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/queryChannelShop", method = RequestMethod.GET)
	public ChannelBaseResult<ChannelShopVO> queryChannelShop(HttpServletRequest request,
			@RequestParam(name = "id") Long id) {
		String logPrefix = "DcChannelInfoController#queryChannelShop#";
		String paramInfo = "#shopId:" + id + "#";

		log.error(logPrefix + "record" + paramInfo);

		ChannelBaseResult<ChannelShopVO> result = new ChannelBaseResult<>();
		result.setSuccess(false);
		try {

			ChannelBaseResult<ChannelShopDTO> shop = discoChannelShopService.queryById(id);
			if (!shop.isSuccess()) {
				throw new Exception(shop.getErrorDesc());
			}
			ChannelShopVO vo = this.toVO(shop.getData());

			ChannelBaseResult<URCResDTO> urc = discoChannelShopService.queryURCResourceByMarket(
					shop.getData().getMarket(), shop.getData().getSubMarket(), shop.getData().getShopId(),
					BusinessModeEnum.PROXY.name());
			if (!urc.isSuccess()) {
				throw new Exception(urc.getErrorDesc());
			}
			vo.setUrcResource(urc.getData());

			ChannelBaseResult<List<PricingFactorDTO>> rules = discoChannelPriceService.listPricingFactors();
			if (!rules.isSuccess()) {
				throw new Exception(rules.getErrorDesc());
			}
			vo.setPricingFactors(rules.getData());

			return ChannelBaseResult.success(vo);
		} catch (Exception e) {
			log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/modifyChannelShop", method = RequestMethod.POST)
	public ChannelBaseResult<Boolean> modifyChannelShop(HttpServletRequest request,
			@RequestBody ChannelShopUpdateParam param) {

		String logPrefix = "DcChannelInfoController#modifyChannelShopOwner#";
		String paramInfo = "#param:" + JSON.toJSONString(param) + "#";

		log.error(logPrefix + "record" + paramInfo);

		ChannelBaseResult<Boolean> result = new ChannelBaseResult<>();
		result.setSuccess(false);
		try {
			result = discoChannelShopService.modifyChannelShopById(param.getShopId(), param.getBusinessModes(),
					param.getShopType(), param.getChannelSubMarket(), param.getChannelOwners(),
					LoginUtil.getEmpId(request));
		} catch (Exception e) {
			log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/listConstants", method = RequestMethod.GET)
	public ChannelBaseResult<Map<String, Map<String, String>>> listConstants(HttpServletRequest request) {
		String logPrefix = "DcChannelInfoController#listConstants#";
		log.error(logPrefix + "record");
		ChannelBaseResult<Map<String, Map<String, String>>> result = new ChannelBaseResult<>();
		result.setSuccess(false);
		try {
			Map<String, Map<String, String>> map = new HashMap<>(10);
			map.put("businessModel", BusinessModeEnum.map());
			map.put("currencyType", CurrencyTypeEnum.map());
			map.put("distributeStatus", DistributeStatusEnum.map());
			map.put("feeType", FeeTypeEnum.map());
			map.put("pricingStrategy", PricingStrategyEnum.map());
			map.put("channelMarketEnum", ChannelMarketEnum.map());
			map.put("channelSubMarketEnum", ChannelSubMarketEnum.map());
			map.put("shopConfigStatus", ShopConfigStatusEnum.map());
			map.put("shopType", ShopTypeEnum.map());
			map.put("sellerType", SellerTypeEnum.map());
			result.setData(map);
			result.setSuccess(true);
			return result;
		} catch (Exception e) {
			log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue(), e);
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/listMultiUserModel", method = RequestMethod.GET)
	public ListOf<DcMultiUserModel> listMultiUserModel(HttpServletRequest request) {

		String logPrefix = "DcChannelInfoController#listMultiUserModel#";

		log.error(logPrefix + "record");

		ListOf<DcMultiUserModel> result = new ListOf<>();
		result.setSuccess(false);
		try {
			// 获取dcEmpModel
			ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
			if (resultOf.isSuccess() && resultOf.getData() != null) {
				// 展示渠道店铺信息
				result = dcChannelInfoService.listMultiUserModel(null);
			} else {
				result.setErrorCode(resultOf.getErrorCode());
				result.setErrorMessage(resultOf.getErrorMessage());
				return result;
			}
		} catch (Exception e) {
			log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue(), e);
		}
		return result;
	}

	private EmpVO toEmpVO(String empId) {
		EmpVO vo = new EmpVO();
		try {
			EnhancedUser user = enhancedUserQueryService.getUser(empId);
			if (user == null) {
				vo.setEmpId("GSP");
				vo.setEmpName("GSP");
			} else {
				vo.setEmpId(empId);
				vo.setEmail(user.getEmailAddr());
				vo.setEmpName(StringUtils.isBlank(user.getNickNameCn()) ? user.getLastName() : user.getNickNameCn());
			}
		} catch (BucException e) {
			log.error(ErrorCodeEnum.EXCEPTION.getValue(), e);
			;
		}
		return vo;
	}

	private List<EmpVO> toEmpVOList(List<String> empIds) {
		List<EmpVO> result = Lists.newArrayList();
		if(empIds == null) {
			return result;
		}
		for (String str : empIds) {
			result.add(this.toEmpVO(str));
		}
		return result;
	}

	private ChannelShopVO toVO(ChannelShopDTO dto) {
		ChannelShopVO vo = new ChannelShopVO();
		vo.setBusinessModes(dto.getBusinessModes() == null? Lists.newArrayList():dto.getBusinessModes());
		vo.setCompanyName(dto.getCompanyName());
		vo.setCreator(this.toEmpVO(dto.getCreator()));
		vo.setGmtCreate(dto.getGmtCreate());
		vo.setGmtModified(dto.getGmtModified());
		vo.setId(dto.getId());
		vo.setLastModifier(this.toEmpVO(dto.getLastModifier()));
		vo.setOwnerList(this.toEmpVOList(dto.getOwnerList()));
		vo.setChannelMarket(dto.getMarket());
		vo.setChannelSubMarket(dto.getSubMarket());
		vo.setSellerName(dto.getSellerName());
		vo.setChannelShopId(dto.getShopId());
		vo.setShopName(dto.getShopName());
		vo.setShopType(dto.getShopType());
		vo.setSigningDate(dto.getSigningDate());
		vo.setStatus(dto.getStatus());
		vo.setSellerType(dto.getSellerType());
		return vo;
	}

}
