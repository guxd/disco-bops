package com.alibaba.cbuscm.controller.domestic.tb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jinxing.yjx
 * @create 2019-04-16 下午8:36
 */
@RestController
public class TaoTestController {

    @RequestMapping("/testJson")
    public List<String> getTao(HttpServletRequest request, Map<String,Object> context) throws IOException, ServletException {
        List<String> list = new ArrayList<>();
        list.add("Tao");
        list.add("Ae");
        list.add("CunTao");
        return list;
    }
}
