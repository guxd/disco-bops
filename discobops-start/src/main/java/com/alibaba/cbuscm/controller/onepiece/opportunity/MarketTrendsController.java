package com.alibaba.cbuscm.controller.onepiece.opportunity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbuscm.contants.MarketConstants;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketTrendsVO;
import com.alibaba.cbuscm.vo.onepiece.opportunity.TrendsSubVO;
import com.alibaba.cbuscm.vo.onepiece.opportunity.TrendsVo;
import com.alibaba.china.dw.dataopen.api.OrderBy;
import com.alibaba.china.dw.dataopen.api.OrderBy.Order;
import com.alibaba.china.dw.dataopen.api.QueryException;
import com.alibaba.china.dw.dataopen.api.Result;
import com.alibaba.china.dw.dataopen.api.Results;
import com.alibaba.china.dw.dataopen.api.SQLIDQueryAPI;
import com.alibaba.china.dw.dataopen.api.User;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.ResultError;

import com.ali.unit.rule.util.lang.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 趋势接口
 * Created by duanyang.zdy on 2019/9/10.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class MarketTrendsController {

    @Resource
    private SQLIDQueryAPI sqlidQueryService;

    private static final Double DEFAULT_INDEX = 0d;

    private static final Integer DEFAULT_RANK = 1;

    private static Map<String,String> map = new HashMap<>();


    @RequestMapping("marketDetail/queryMarketTrendsIndex")
    public Object queryMarketTrendsIndex(@RequestParam(name = "statDate") String statDate,
                                         @RequestParam(name = "marketId") String marketId,
                                         @RequestParam(name = "isGlobal", defaultValue = "1") String isGlobal,
                                         @RequestParam(name="pageIndex",defaultValue = "1") Integer pageIndex,
                                         @RequestParam(name="pageNum",defaultValue = "50") Integer pageNum,
                                         @RequestParam(name="orderByField",defaultValue = "google_indexed_volume") String orderByField,
                                         @RequestParam(name="orderByType",defaultValue = "desc") String orderByType,
                                        HttpServletRequest request) {

        //校验参数
        if (StringUtils.isBlank(statDate) || StringUtils.isBlank(marketId)) {
            return ResultOf.error(ResultError.MSG_CODE_PARAM_ERROR, "param is null");
        }

        // 建立查询条件
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("stat_date", statDate);
        conditions.put("market_id", marketId);
        //设置返回类型
        String[] return_market_trends_fields = {
            "stat_date", "market_id", "google_country", "google_query_zh", "rank",
            "google_indexed_volume", "google_indexed_volume_lw", "google_wow_delta", "google_trends"};

        String[] return_trends_count_fields = {"trends_count"};

        User user = getUserInfo();

        getOrderFiled(map);

        try {



            //获取价格带查询列表
            List<OrderBy> orderByList = new ArrayList<>();
            //orderByList.add(new OrderBy("google_country", Order.ASC));
            orderByList.add(new OrderBy(map.get(orderByField), orderByType.equals("asc")?Order.ASC:Order.DESC));

            //获取趋势详情数据
            Results trendsListResults = sqlidQueryService.list(user, MarketConstants.Market_TRENDS_SQL_ID, conditions,
                return_market_trends_fields, orderByList,(pageIndex - 1) * pageNum,pageNum);
            if (trendsListResults == null || trendsListResults.getResult() == null || trendsListResults.getResult()
                .isEmpty()) {
                return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, trendsListResults.getMessage());
            }
            if (!trendsListResults.isSuccessed()) {
                return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, trendsListResults.getMessage());
            }

            //获取商品查询总数

            Result trendsCountResults = sqlidQueryService.get(user,MarketConstants.Market_TRENDS_COUNT_SQL_ID,conditions,return_trends_count_fields);
            if (trendsCountResults == null || trendsCountResults.getResult()==null || trendsCountResults.getResult().isEmpty()) {
                return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, trendsCountResults.getMessage());
            }
            if (!trendsCountResults.isSuccessed()) {
                return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, trendsCountResults.getMessage());
            }

            return ResultOf.general(buildMarketTrendsVO(trendsListResults.getResult(),trendsCountResults.getResult(),pageIndex,pageNum));

        } catch (QueryException e) {

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, "query 出现异常 " + e.getMessage());

        } catch (Exception e) {

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, "query 出现异常 " + e.getMessage());
        }


    }

    //生成OneService User信息
    static User getUserInfo() {
        User user = new User();
        user.setAppName(MarketConstants.AppName);
        user.setName(MarketConstants.AppName);
        user.setPassword(MarketConstants.Pass);
        return user;
    }

    static void getOrderFiled(Map<String,String> map) {

        map.put("queryIndexThisWeek","google_indexed_volume");
        map.put("queryIndexLastWeek","google_indexed_volume_lw");
        return ;
    }


    private MarketTrendsVO buildMarketTrendsVO(List<Map<String, Object>> maps,Map<String, Object> count,Integer pageIndex,Integer pageNum){

        List<TrendsVo> listTrends = new ArrayList<>();


        maps.stream().forEach(map->{
            TrendsVo trendsVO = TrendsVo.builder()
                .areaName(String.valueOf(map.get("GOOGLE_COUNTRY")))
                .queryIndexLastWeek(map.get("GOOGLE_INDEXED_VOLUME_LW")!=null?Double.valueOf(String.valueOf(map.get("GOOGLE_INDEXED_VOLUME_LW"))):DEFAULT_INDEX)
                .queryIndexThisWeek(map.get("GOOGLE_INDEXED_VOLUME")!=null?Double.valueOf(String.valueOf(map.get("GOOGLE_INDEXED_VOLUME"))):DEFAULT_INDEX)
                .queryWord(String.valueOf(map.get("GOOGLE_QUERY_ZH")))
                .rank(map.get("RANK")!=null?Integer.valueOf(String.valueOf(map.get("RANK"))):DEFAULT_RANK)
                .trends(map.get("GOOGLE_TRENDS")!=null?JSON.parseArray((String.valueOf(map.get("GOOGLE_TRENDS"))), TrendsSubVO.class):null)
                .wowIncRate(map.get("GOOGLE_WOW_DELTA")!=null?Double.valueOf(String.valueOf(map.get("GOOGLE_WOW_DELTA"))):DEFAULT_INDEX)
                .build();

            listTrends.add(trendsVO);
        });

        MarketTrendsVO vo = MarketTrendsVO.builder()
            .marketId(String.valueOf(maps.get(0).get("MARKET_ID")))
            .marketName(String.valueOf(maps.get(0).get("MARKET_NAME")))
            .statDate(String.valueOf(maps.get(0).get("STAT_DATE")))
            .data(listTrends)
            .build();

        vo.setPageNo(pageIndex);
        vo.setPageSize(pageNum);
        //获取总量需要重新再封装一个接口
        vo.setTotal(count.get("TRENDS_COUNT")!=null?Long.valueOf(String.valueOf(count.get("TRENDS_COUNT"))):0L);
        return vo;

    }

    public static void main(String[] args) {

        String trends = "[{\"key\":\"201908\",\"value\":\"256\"},{\"key\":\"201907\",\"value\":\"123\"}]";

        List<TrendsSubVO> array = JSON.parseArray(trends, TrendsSubVO.class);

        System.out.println(JSON.toJSON(array));

        System.out.println((double)Math.round(Double.valueOf("23.4567")*100)/100);
    }


}
