package com.alibaba.cbuscm.controller.domestic.tb;

import com.alibaba.buc.api.exception.BucException;
import com.alibaba.buc.api.model.enhanced.EnhancedUser;
import com.alibaba.buc.api.unit.EnhancedUserQueryReadService;
import com.alibaba.cbu.disco.shared.common.constant.ChannelMarketEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcProductEnrollService;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditStatusEnum;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditTypeEnum;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcOfferEnrollStatusEnum;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcOfferEnrollModel;
import com.alibaba.cbu.disco.shared.core.planning.api.DiscoPlanningProgressSenderService;
import com.alibaba.cbu.disco.shared.core.planning.constant.TaoOperationTypeEnum;
import com.alibaba.cbu.disco.shared.core.product.constant.DcProductBizTypeEnum;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalReadService;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalWriteService;
import com.alibaba.cbu.disco.shared.core.proposal.contant.OfferProposalAuditLogStatusEnum;
import com.alibaba.cbu.disco.shared.core.proposal.contant.OfferProposalFlowEnum;
import com.alibaba.cbu.disco.shared.core.proposal.contant.OfferProposalTagEnum;
import com.alibaba.cbu.disco.shared.core.proposal.model.DcProposalAuditLogModel;
import com.alibaba.cbu.disco.shared.core.proposal.model.DcProposalModel;
import com.alibaba.cbu.disco.shared.core.proposal.model.SkuInfoModel;
import com.alibaba.cbu.disco.shared.core.proposal.param.DcProposalAuditParam;
import com.alibaba.cbu.disco.shared.core.proposal.param.DcProposalQueryParam;
import com.alibaba.cbu.disco.shared.core.supplier.api.DcSupplierService;
import com.alibaba.cbuscm.datashare.DataShareFileService;
import com.alibaba.cbuscm.datashare.TempFileUtil;
import com.alibaba.cbuscm.service.proposal.OfferProposalExtendService;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.utils.ResultUtils;
import com.alibaba.cbuscm.vo.disco.OfferProposalDetailVO;
import com.alibaba.cbuscm.vo.disco.OfferProposalVO;
import com.alibaba.cbuscm.vo.domestic.tb.InternalProposalExtraVO;
import com.alibaba.china.global.business.library.enums.StdCategoryChannelEnum;
import com.alibaba.china.global.business.library.interfaces.category.CategoryReadService;
import com.alibaba.china.global.business.library.models.category.StdCategoryModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningDesignModel;
import com.alibaba.china.shared.discosupplier.model.planning.opp.PlanningOppItemModel;
import com.alibaba.china.shared.discosupplier.service.planning.design.PlanningDesignService;
import com.alibaba.china.shared.discosupplier.service.planning.opp.PlanningOppItemService;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.taobao.imap.arithmetic.CategoryLevel;
import com.taobao.imap.arithmetic.DesCategory;
import com.taobao.imap.arithmetic.SourceCategory;
import com.taobao.imap.arithmetic.hsf.CategoryPredictService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.testng.collections.Maps;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;


@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/product/offer-proposal", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class InternalProposalAdminController {

    private static final String[] EASY_EXCEL_HEAD = new String[] {"商品ID", "商品名", "商品报名时间", "1688一级类目",
        "二级类目", "叶子类目", "品牌名", "品牌注册号", "品牌所有人", "品牌是否手填"};

    private static final String DATA_SHARE_URL
            = "http://datashare.alibaba-inc.com:8080/tddp2/index.do#A=myFile:myFile:myFileList";

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private DataShareFileService dataShareFileService;

    @Autowired
    private DcProposalReadService proposalReadService;

    @Autowired
    private DcProposalWriteService proposalWriteService;

    @Autowired
    private CategoryReadService categoryReadService;

    @Autowired
    private OfferProposalExtendService offerProposalExtendService;

    @Autowired
    private DcProductEnrollService dcProductEnrollService;

    @Autowired
    private EnhancedUserQueryReadService enhancedUserQueryService;

    @Autowired
    private DcSupplierService dcSupplierService;

    @Autowired
    private DiscoPlanningProgressSenderService discoPlanningProgressSenderService;

    @Autowired
    private CategoryPredictService categoryPredictService;

    @Autowired
    private PlanningOppItemService planningOppItemService;

    @Autowired
    private PlanningDesignService planningDesignService;

    private ProposalExcelExportThread1 excelTab1ExportThread = new ProposalExcelExportThread1();

    private ProposalExcelExportThread2 excelTab2ExportThread = new ProposalExcelExportThread2();

    @ResponseBody
    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public Object getOfferProposal(@RequestParam(value = "offerProposalId") Long offerProposalId) {
        try {
            ResultOf<DcProposalModel> result = proposalReadService.queryProposalById(
                offerProposalId);
            if (!ResultOf.isValid(result)) {
                return ResultOf.error(RPCResult.MSG_ERROR_ILLEGAL, "无效id");
            }
            ListOf<DcProposalAuditLogModel> logs = proposalReadService.listProposalAuditLog(
                offerProposalId);
            OfferProposalDetailVO vo = offerProposalExtendService.fillOfferProposalDetail(result.getData(),
                logs.getData());
            sendPlanningDesign(vo);
            return ResultOf.general(vo);
        } catch (Throwable t) {
            log.error("InternalProposalAdminController.get error!", t);
            return ResultOf.error("system-error", t.toString());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/auditlogs", method = RequestMethod.GET)
    public Object getOfferProposalAuditLogs(@RequestParam(value = "offerProposalId") Long offerProposalId) {
        try {
            ListOf<DcProposalAuditLogModel> rpcResult = proposalReadService.listProposalAuditLog(
                offerProposalId);
            if (ListOf.isValid(rpcResult)) {
                return ResultOf.general(ListOf.general(rpcResult.getData().stream()
                    .map(OfferProposalExtendService::convertAuditLog)
                    .collect(Collectors.toList())));
            }else{
                return ResultOf.empty();
            }
        } catch (Throwable t) {
            log.error("InternalProposalAdminController.auditlogs error!", t);
            return ResultOf.error("system-error", t.toString());
        }
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    @ResponseBody
    public PageOf<OfferProposalVO> list(
        @RequestParam(value = "status", required = false) String status,
        @RequestParam(value = "categorys", required = false) String categorys,
        @RequestParam(value = "cateId", required = false) Long cateId,
        @RequestParam(value = "cateLevel", required = false) Long cateLevel,
        @RequestParam(value = "offerId", required = false) Long offerId,
        @RequestParam(value = "offerTitle", required = false) String offerTitle,
        @RequestParam(value = "loginId", required = false) String loginId,
        @RequestParam(value = "company", required = false) String company,
        @RequestParam(value = "offerSource", required = false) String offerSource,
        @RequestParam(name = "searchTimeBeginMs", required = false) String searchTimeBeginMs,
        @RequestParam(name = "searchTimeEndMs", required = false) String searchTimeEndMs,
        @RequestParam("pageNo") Integer pageNo,
        @RequestParam("pageSize") Integer pageSize) {

        try {
            if ("TOTAL".equalsIgnoreCase(status)) {
                status = null;
            }
            Date createTimeStart = null;
            Date createTimeEnd = null;
            List<String> tags = null;

            if (!StringUtils.isBlank(searchTimeBeginMs)) {
                createTimeStart = new Date( Long.valueOf(searchTimeBeginMs));
            }
            if (!StringUtils.isBlank(searchTimeEndMs)) {
                createTimeEnd = new Date( Long.valueOf(searchTimeEndMs));
            }
            if (!StringUtils.isBlank(offerSource)) {
                tags = new ArrayList<>();
                for(OfferProposalTagEnum tagEnum : OfferProposalTagEnum.values()) {
                    if (Long.parseLong(offerSource)==tagEnum.getIndex()) {
                        tags.add(tagEnum.name());
                    }
                }
            }
            PageOf<DcProposalModel> pageResult = proposalReadService.listProposal(DcProposalQueryParam.builder()
                .bizType(DcProductBizTypeEnum.INTERNAL.name())
                .status(status)
                .cateId(cateId)
                .cateLevel(cateLevel)
                .offerId(offerId)
                .offerTitle(offerTitle)
                .loginId(loginId)
                .company(company)
                .createTimeStart(createTimeStart)
                .createTimeEnd(createTimeEnd)
                .flow(OfferProposalFlowEnum.PROPOSAL.name())
                .cateIds(splitCategory(categorys))
                .tags(tags)
                .pageNo(pageNo)
                .pageSize(pageSize)
                .build());
            return ResultUtils.map(pageResult, offerProposalExtendService::convertOfferProposalVO);
        } catch (Throwable err) {
            log.error("InternalProposalAdminController.list error!", err);
            return PageOf.error("system-error", err.getMessage());
        }
    }

    @RequestMapping(path = "/count", method = RequestMethod.GET)
    @ResponseBody
    public ResultOf<Map<String, Long>> countProductProposal() {
        return proposalReadService.countStatus(DcProposalQueryParam.builder()
            .bizType(DcProductBizTypeEnum.INTERNAL.name())
            .flow(OfferProposalFlowEnum.PROPOSAL.name())
            .build());
    }


    /**
     * 新品提报数据导出
     * @param type 1 查全量
     * @param offerImportIds
     * @param status
     * @param categorys
     * @param offerId
     * @param offerTitle
     * @param loginId
     * @param company
     * @param offerSource
     * @return
     */
    @RequestMapping(path = "/exportNewProduct", method = RequestMethod.GET)
    @ResponseBody
    public RPCResult exportNewProduct(HttpServletRequest request,
                                      @RequestParam(value = "type") Integer type,
                                      @RequestParam(value = "sheet") Integer sheet,
                                      @RequestParam(value = "offerImportIds", required = false) String offerImportIds,
                                      @RequestParam(value = "status", required = false) String status,
                                      @RequestParam(value = "categorys", required = false) String categorys,
                                      @RequestParam(value = "offerId", required = false) Long offerId,
                                      @RequestParam(value = "offerTitle", required = false) String offerTitle,
                                      @RequestParam(value = "loginId", required = false) String loginId,
                                      @RequestParam(value = "company", required = false) String company,
                                      @RequestParam(value = "offerSource", required = false) String offerSource) {
        String empId = LoginUtil.getEmpId(request);
        if (StringUtils.isEmpty(empId)) {
            return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, "请登陆！");
        }
        RPCResult result = new RPCResult();
        if(sheet!=1 && sheet!=2){
            result.setErrorCode("Param_Illegal");
            result.setErrorMsg("无效的导出类型");
            result.setSuccess(false);
            return result;
        }
        if(type==1){
            DcProposalQueryParam queryParam = new DcProposalQueryParam();
            queryParam.setBizType(DcProductBizTypeEnum.INTERNAL.name());
            queryParam.setStatus(status);
            queryParam.setCateIds(splitCategory(categorys));
            queryParam.setOfferId(offerId);
            queryParam.setOfferTitle(offerTitle);
            queryParam.setLoginId(loginId);
            queryParam.setCompany(company);
            // queryParam.setOfferSource(offerSource);
            if(sheet==1) {
                return processExportExcel(result, excelTab1ExportThread, type, queryParam, null, empId);
            }else{
                return processExportExcel(result, excelTab2ExportThread, type, queryParam, null, empId);
            }
        }else if(type==2){
            if(org.apache.commons.lang.StringUtils.isEmpty(offerImportIds)){
                result.setErrorCode("Param_Loss");
                result.setErrorMsg("缺少导出参数");
                result.setSuccess(false);
                return result;
            }
            String[] idArrays = offerImportIds.split(",");
            List<Long> idList = new ArrayList<>();
            for(String id : idArrays){
                try{
                    idList.add(Long.parseLong(id));
                }catch(Exception e){
                    //ignore
                }
            }
            if(sheet==1) {
                return processExportExcel(result, excelTab1ExportThread, type, null, idList, empId);
            }else{
                return processExportExcel(result, excelTab2ExportThread, type, null, idList, empId);
            }
        }else {
            result.setErrorCode("Param_Illegal");
            result.setErrorMsg("无效的导出类型");
            result.setSuccess(false);
            return result;
        }
    }

    private RPCResult processExportExcel(RPCResult result, AbstractProposalExportThread thread, int type, DcProposalQueryParam queryParam,
                                         List<Long> idList, String empId) {
        if (!thread.isStart()) {
            synchronized (thread) {
                thread.setType(type);
                thread.setIds(idList);
                thread.setQueryParam(queryParam);
                thread.setEmpNo(empId);
                ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("proposal-export-pool-%d").build();

                ExecutorService singleThreadPool = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
                singleThreadPool.execute(thread);
                singleThreadPool.shutdown();
            }
            return RPCResult.ok(DATA_SHARE_URL);
        } else {
            result.setErrorCode("Export_Using");
            result.setErrorMsg("数据导出正在运行，请稍后重试");
            result.setSuccess(false);
            return result;
        }
    }



    /**
     * 导出选择的品牌信息
     *
     * @param offerImportIds 提报商品的id
     */
    @RequestMapping(path = "/export", method = RequestMethod.GET)
    @ResponseBody
    public RPCResult exportOfferImportBrands(HttpServletRequest request,
                                             @RequestParam(name = "offerImportIds")
                                                     String offerImportIds) {
        List<Long> offerImportIdList = Lists.newArrayList();
        if(StringUtils.isEmpty(offerImportIds)){
            return RPCResult.error(RPCResult.MSG_ERROR_REQUIRED, "请选择要导出的数据");
        }
        String[] importIdArray = offerImportIds.split(",");
        for(String importId : importIdArray){
            try{
                long importIdL = Long.parseLong(importId);
                offerImportIdList.add(importIdL);
            }catch(Exception e){
                //ignor
            }
        }
        if(CollectionUtils.isEmpty(offerImportIdList)){
            return RPCResult.error(RPCResult.MSG_ERROR_REQUIRED, "请选择要导出的数据");
        }
        if (CollectionUtils.isEmpty(offerImportIdList)) {
            return RPCResult.error(RPCResult.MSG_ERROR_REQUIRED, "请选择要导出的数据");
        }
        //防止数据量过大
        if (offerImportIdList.size() > 100) {
            return RPCResult.error(RPCResult.MSG_ERROR_ILLEGAL, "选择导出数据量过大");
        }
        String empId = LoginUtil.getEmpId(request);
        if (StringUtils.isEmpty(empId)) {
            return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, "请登陆！");
        }

        //查询数据
        ListOf<DcProposalModel> offerImportList = proposalReadService.listProposalByIds(offerImportIdList);
        if (!offerImportList.isSuccess()) {
            log.error("query offer import error", offerImportList.getErrorMessage());
            return RPCResult.error(offerImportList.getErrorCode(), offerImportList.getErrorMessage());
        }
        if (CollectionUtils.isEmpty(offerImportList.getData())) {
            log.error("query offer import error,data empty");
            return RPCResult.error(RPCResult.MSG_ERROR, "数据为空");
        }
        String filePath = "";
        try {
            //文件名称
            String fileName = "提报商品导出" + System.currentTimeMillis();
            filePath = TempFileUtil.createTempXlsxFile(fileName);
            FileOutputStream bOutput = new FileOutputStream(filePath);
            //写入内存
            writeFile(bOutput, offerImportList.getData());

            dataShareFileService.uploadFile(fileName, filePath, Lists.newArrayList(empId));
        } catch (IOException ioException) {
            log.error("create excel or upload file error", ioException.getMessage(), ioException);
            return RPCResult.error(RPCResult.MSG_ERROR, "系统错误!");
        } finally {
            File file = new File(filePath);
            if (file.exists()) {
                file.delete();
            }
        }
        return RPCResult.ok(DATA_SHARE_URL);
    }

    /**
     * 提报商品审核通过
     *
     * @param offerImportId 提报商品id
     */
    @RequestMapping(path = "/pass", method = RequestMethod.GET)
    @ResponseBody
    public RPCResult auditPass(HttpServletRequest request,
                               @RequestParam(name = "offerProposalId") Long offerImportId) {
        String empId = LoginUtil.getEmpId(request);
        if (StringUtils.isEmpty(empId)) {
            log.error("offer import audit pass error, user not login");
            return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
        }
        ResultOf<Boolean> result = proposalWriteService.auditPass(DcProposalAuditParam.builder()
            .proposalId(offerImportId)
            .operator(OperatorModel.builder()
                .id(empId)
                .name(LoginUtil.getLoginName(request))
                .build())
            .bizType(DcProductBizTypeEnum.INTERNAL.name())
            .build());
        if (!result.isSuccess()) {
            return RPCResult.error(result.getErrorCode(), result.getErrorMessage());
        } else {
            //add by xuhb 发送消息
            sendAuditMessage(empId,offerImportId, TaoOperationTypeEnum.PROPOSAL_AUDIT_PASS,null);
            return RPCResult.ok(true);
        }
    }

    /**
     * 提报商品审核通过
     *
     * @param offerImportId 提报商品id
     */
    @RequestMapping(path = "/deny", method = RequestMethod.GET)
    @ResponseBody
    public RPCResult auditDeny(HttpServletRequest request,
                               @RequestParam(name = "offerProposalId") Long offerImportId,
                               @RequestParam(name = "reasonType") String reasonType,
                               @RequestParam(name = "msg") String msg) {
       String empId = LoginUtil.getEmpId(request);
        if (StringUtils.isEmpty(empId)) {
            log.error("offer import audit deny error, user not login");
            return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
        }
        if (StringUtils.isEmpty(reasonType)) {
            log.error("offer import audit deny error, reasonType is empty");
            return RPCResult.error(RPCResult.MSG_ERROR_ILLEGAL, RPCResult.MSG_ERROR_REQUIRED);
        }
        ResultOf<Boolean> result = proposalWriteService.auditDeny(DcProposalAuditParam.builder()
            .proposalId(offerImportId)
            .operator(OperatorModel.builder()
                .id(empId)
                .name(LoginUtil.getLoginName(request))
                .build())
            .bizType(DcProductBizTypeEnum.INTERNAL.name())
            .msgType(reasonType)
            .msg(msg)
            .build());
        if (!result.isSuccess()) {
            return RPCResult.error(result.getErrorCode(), result.getErrorMessage());
        } else {
            //add by xuhb 发送消息
            sendAuditMessage(empId,offerImportId,TaoOperationTypeEnum.PROPOSAL_AUDIT_NOTPASS,msg);
            return RPCResult.ok(true);
        }
    }

    /**
     * 提报商品审核通过
     *
     * @param offerImportId 提报商品id
     */
    @RequestMapping(path = "/reject", method = RequestMethod.GET)
    @ResponseBody
    public RPCResult auditReject(HttpServletRequest request,
                                 @RequestParam(name = "offerProposalId") Long offerImportId,
                                 @RequestParam(name = "reasonType",required = false) String reasonType,
                                 @RequestParam(name = "msg") String msg) {
        String empId = LoginUtil.getEmpId(request);
        if (StringUtils.isEmpty(empId)) {
            log.error("offer import audit reject error, user not login");
            return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
        }
        ResultOf<Boolean> result = proposalWriteService.auditReject(DcProposalAuditParam.builder()
            .proposalId(offerImportId)
            .operator(OperatorModel.builder()
                .id(empId)
                .name(LoginUtil.getLoginName(request))
                .build())
            .bizType(DcProductBizTypeEnum.INTERNAL.name())
            .msg(msg)
            .msgType(reasonType)
            .build());
        if (!result.isSuccess()) {
            return RPCResult.error(result.getErrorCode(), result.getErrorMessage());
        } else {
            //add by xuhb 发送消息
            sendAuditMessage(empId,offerImportId,TaoOperationTypeEnum.PROPOSAL_REJECT,msg);
            return RPCResult.ok(true);
        }
    }

    /**
     * 提报商品删除
     *
     * @param request
     * @param offerImportId 提报商品id
     * @return
     */
    @RequestMapping(path = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public RPCResult auditDelete(HttpServletRequest request,
                                 @RequestParam(name = "offerProposalId") Long offerImportId,
                                 @RequestParam(name = "msg") String msg) {
        String empId = LoginUtil.getEmpId(request);
        if (StringUtils.isEmpty(empId)) {
            log.error("offer import audit delete error, user not login");
            return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
        }
        ResultOf<Boolean> result = proposalWriteService.auditDelete(DcProposalAuditParam.builder()
            .proposalId(offerImportId)
            .operator(OperatorModel.builder()
                .id(empId)
                .name(LoginUtil.getLoginName(request))
                .build())
            .bizType(DcProductBizTypeEnum.INTERNAL.name())
            .msg(msg)
            .build());
        if (!result.isSuccess()) {
            return RPCResult.error(result.getErrorCode(), result.getErrorMessage());
        } else {
            return RPCResult.ok(true);
        }
    }

    private void sendAuditMessage(String empId,Long proposalId,TaoOperationTypeEnum type,String msg){
        //校验用户
        EnhancedUser user = null;
        try {
            user = enhancedUserQueryService.getUser(empId);
        } catch (BucException e) {
            return;
        }
        if (user == null || (user.getHrStatus() != null && user.getHrStatus().equals("I"))) {
            return;
        }
        PlanningOppItemModel planningOppItemModel = planningOppItemService.findByImportId(proposalId);
        Long planningId = null;
        if(planningOppItemModel!=null){
            planningId = planningOppItemModel.getPlanningId();
        }
        discoPlanningProgressSenderService.sendMessage(proposalId, planningId, type, empId, user.getLastName(), msg,
        null);
    }

    private void writeFile(OutputStream out, List<DcProposalModel> offerImportList) {
        ExcelWriter writer = EasyExcelFactory.getWriter(out);
        //写第一个sheet, sheet1  数据全是List<String> 无模型映射关系
        Sheet sheet1 = new Sheet(1, 3);
        sheet1.setSheetName("提报商品信息");
        sheet1.setHead(createSheetHead(EASY_EXCEL_HEAD));
        //or 设置自适应宽度
        sheet1.setAutoWidth(Boolean.TRUE);
        writer.write1(getSheetData(offerImportList), sheet1);
        //关闭资源
        writer.finish();
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                log.error("close excel error", e.getMessage(), e);
            }
        }
    }

    private List<List<Object>> getSheetData(List<DcProposalModel> offerImportList) {
        List<List<Object>> result = Lists.newArrayList();
        if (CollectionUtils.isEmpty(offerImportList)) {
            return result;
        }
        Map<Integer, StdCategoryModel> categoryMaps = categoryReadService.getChannelAllStdCategoryMap(
            StdCategoryChannelEnum.CHANNEL_1688);
        for (DcProposalModel proposal : offerImportList) {
            List<Object> rowData = Lists.newArrayList();
            long offerId = proposal.getOfferId();
            String offerTitle = proposal.getOfferTitle();
            String gmtCreateStr = convertDateToStr(proposal.getGmtCreate());
            String firstCategoryName = convertToCategoryName(proposal.getCateIdLv1(), categoryMaps);
            String secondCategoryName = convertToCategoryName(proposal.getCateIdLv2(), categoryMaps);
            String leafCategoryName = convertToCategoryName(proposal.getCateIdLv3(), categoryMaps);
            rowData.add(String.valueOf(offerId));
            rowData.add(offerTitle);
            rowData.add(gmtCreateStr);
            rowData.add(firstCategoryName);
            rowData.add(secondCategoryName);
            rowData.add(leafCategoryName);
            InternalProposalExtraVO extraM = JSON.parseObject(JSON.toJSONString(proposal.getExtra()),
                InternalProposalExtraVO.class);
            if (extraM != null) {
                String brandName = extraM.getBrandName();
                String brandNo = extraM.getBrandRegNo();
                String brandOwner = extraM.getBrandOwner();
                Boolean isNewBrand = extraM.isNewBrand();
                rowData.add(brandName);
                rowData.add(brandNo);
                rowData.add(brandOwner);
                if(isNewBrand) {
                    rowData.add("是");
                }else{
                    rowData.add("否");
                }
            }
            result.add(rowData);
        }
        return result;
    }

    private String convertDateToStr(Date createDate) {
        if (createDate == null) {
            return null;
        }
        Instant instant = createDate.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime.format(DATE_TIME_FORMATTER);
    }

    private String convertToCategoryName(Long categoryId, Map<Integer, StdCategoryModel> categoryMaps) {
        if (categoryId == null) {
            return null;
        }
        StdCategoryModel categotyM = categoryMaps.get(categoryId.intValue());
        if (categotyM == null) {
            return null;
        }
        return categotyM.getName();
    }

    private List<Long> splitCategory(String categorys) {
        List<Long> cateIds = null;
        if (StringUtils.isNotBlank(categorys)) {
            String[] offerCategorys = categorys.split(",");
            // TB类目ids
            cateIds = new ArrayList<>();
            for (String cateIdStr : offerCategorys) {
                if (StringUtils.isNotBlank(cateIdStr)) {
                    Long categoryId = Long.valueOf(cateIdStr);
                    cateIds.add(categoryId);
                }
            }
        }
        return cateIds;
    }

    private void sendPlanningDesign(OfferProposalDetailVO vo){
        if(vo ==null){
            return;
        }
        //设置企划信息
        PlanningOppItemModel planningOppItemModel = planningOppItemService.findByImportId(vo.getId());
        PlanningDesignModel planningDesignModel = null;
        if (planningOppItemModel != null) {
            planningDesignModel = planningDesignService.find(planningOppItemModel.getPlanningId());
        }
        offerProposalExtendService.setProposalPlanning(planningOppItemModel,planningDesignModel,vo);
    }


    abstract class AbstractProposalExportThread implements Runnable {

        @Getter
        @Setter
        DcProposalQueryParam queryParam;

        @Getter
        @Setter
        List<Long> ids;

        @Getter
        @Setter
        int type;

        @Getter
        @Setter
        String empNo;

        AtomicBoolean isStart = new AtomicBoolean(false);

        boolean isStart() {
            return isStart.get();
        }

        boolean setStart(boolean org, boolean newV) {
            return isStart.compareAndSet(org, newV);
        }
    }


    class ProposalExcelExportThread1 extends AbstractProposalExportThread {

        private String[] easyExcelHead = new String[]{"企划ID", "企划名称", "企划类型", "1688商品ID", "1688一级类目", "1688叶子类目", "1688商品标题", "淘系一级类目", "淘系叶子类目", "SKU名称", "1688售价", "SKU供货价(出厂价)", "供货库存", "包材成本", "物流成本", "材料成本", "制造费用", "期间费用", "品牌名称", "品牌注册号",
                "品牌所有人", "商标注册证", "商标使用权限书", "3C认证证书", "发货快递", "发货地址", "会员login_id", "会员member_id", "会员公司名称", "报名时间",
                "最后一次审核时间(年)", "最后一次审核时间(月)", "最后一次审核时间(日)", "在供货中心的审核状态", "审核不通过的原因分类", "审核不通过原因详情"};

        @Override
        public void run() {
            try {
                if(!setStart(false,true)){
                    return;
                }
                List<DcProposalModel> internalOfferProposalModelList = Lists.newArrayList();
                queryProposal(type, queryParam, ids, internalOfferProposalModelList);
                if(CollectionUtils.isEmpty(internalOfferProposalModelList)){
                    log.error("query offer import error,data empty");
                    return;
                }

                Map<Integer, StdCategoryModel> b2bStdMap  = categoryReadService.getChannelAllStdCategoryMap(StdCategoryChannelEnum.CHANNEL_1688);
                Map<Integer, StdCategoryModel> tbStdMap  = categoryReadService.getChannelAllStdCategoryMap(StdCategoryChannelEnum.CHANNEL_TB);
                //imap类目映射-start
                Map<Long, Long> cateMap = preprocessingIMap(internalOfferProposalModelList,b2bStdMap);
                Map<Long, String> tbFirstCateMap = preprocessingIMap(cateMap,tbStdMap);
                //imap类目映射-end
                List<Map<Integer,String>> sheetDatas = new ArrayList<>();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                for(DcProposalModel proposalModel : internalOfferProposalModelList){
                    if (proposalModel == null) {
                        continue;
                    }
                    if(CollectionUtils.isEmpty(proposalModel.getSkuInfoList())){
                        continue;
                    }
                    Map<Integer, String> sheetDataMap = new HashMap<>();

                    //查询提报商品关联的企划信息
                    PlanningOppItemModel byImportId = planningOppItemService.findByImportId(proposalModel.getId());
                    if (null != byImportId) {
                        PlanningDesignModel byPlanningId = planningDesignService.find(byImportId.getPlanningId());
                        if (null != byPlanningId) {
                            sheetDataMap.put(1, String.valueOf(byImportId.getPlanningId()));
                            sheetDataMap.put(2, byPlanningId.getTitle());
                            sheetDataMap.put(3, byPlanningId.getType().getMessage());
                        }
                    }

                    //4:1688offer id
                    sheetDataMap.put(4, String.valueOf(proposalModel.getOfferId()));
                    //5:1688一级类目
                    if (proposalModel.getCateIdLv1() != null && b2bStdMap.containsKey(proposalModel.getCateIdLv1().intValue())) {
                        sheetDataMap.put(5, b2bStdMap.get(proposalModel.getCateIdLv1().intValue()).getName());
                    }
                    //6:1688叶子类目
                    sheetDataMap.put(6, getLeafCateName(b2bStdMap, proposalModel));
                    //7:1688offer标题
                    sheetDataMap.put(7, proposalModel.getOfferTitle());

                    Long leafCateId = getLeafCateId(proposalModel);
                    //8:淘系一级
                    if (leafCateId != null && tbFirstCateMap.containsKey(leafCateId)) {
                        sheetDataMap.put(8, tbFirstCateMap.get(leafCateId));
                    }
                    //9:淘系叶子
                    if (leafCateId != null && cateMap.containsKey(leafCateId) && tbStdMap.containsKey(cateMap.get(leafCateId).intValue())) {
                        StdCategoryModel tbCateModel = tbStdMap.get(cateMap.get(leafCateId).intValue());
                        if (tbCateModel != null) {
                            sheetDataMap.put(9, tbCateModel.getName());
                        }
                    }
                    if (null != proposalModel.getExtra()) {
                        InternalProposalExtraVO extraVO = JSON.parseObject(JSON.toJSONString(proposalModel.getExtra()),
                                InternalProposalExtraVO.class);
                        ////9:材质
                        //sheetDataMap.put(11, extraVO.getMaterial());
                        ////10:工艺
                        //sheetDataMap.put(12, extraVO.getTechnics());
                        //14:包材成本
                        if (null != extraVO.getPackingCost()) {
                            sheetDataMap.put(14, String.valueOf(extraVO.getPackingCost()));
                        }
                        //15:物流成本
                        if (null != extraVO.getLogisticsCost()) {
                            sheetDataMap.put(15, String.valueOf(extraVO.getLogisticsCost()));
                        }
                        //16:材料成本
                        if (null != extraVO.getMaterialCost()) {
                            sheetDataMap.put(16, String.valueOf(extraVO.getMaterialCost()));
                        }
                        //17:制造成本
                        if (null != extraVO.getManufactureCost()) {
                            sheetDataMap.put(17, String.valueOf(extraVO.getManufactureCost()));
                        }
                        //18:期间费用
                        if (null != extraVO.getPeriodCost()) {
                            sheetDataMap.put(18, String.valueOf(extraVO.getPeriodCost()));
                        }
                        //19:品牌名称
                        if (StringUtils.isNotEmpty(extraVO.getBrandName())) {
                            sheetDataMap.put(19, String.valueOf(extraVO.getBrandName()));
                        }
                        //20:品牌注册号
                        if (StringUtils.isNotEmpty(extraVO.getBrandRegNo())) {
                            sheetDataMap.put(20, String.valueOf(extraVO.getBrandRegNo()));
                        }
                        //21:品牌人
                        if (StringUtils.isNotEmpty(extraVO.getBrandOwner())) {
                            sheetDataMap.put(21, String.valueOf(extraVO.getBrandOwner()));
                        }
                        //22:商标注册证
                        if (StringUtils.isNotEmpty(extraVO.getTrcUrl())) {
                            sheetDataMap.put(22, String.valueOf(extraVO.getTrcUrl()));
                        }
                        //23:商标使用授权书
                        if (StringUtils.isNotEmpty(extraVO.getTlUrl())) {
                            sheetDataMap.put(23, String.valueOf(extraVO.getTlUrl()));
                        }
                        //24:3C认证证书
                        if (StringUtils.isNotEmpty(extraVO.getAuth3cUrl())) {
                            sheetDataMap.put(24, String.valueOf(extraVO.getAuth3cUrl()));
                        }
                        //25:发货快递
                        if (StringUtils.isNotEmpty(extraVO.getLogisticsAgent())) {
                            sheetDataMap.put(25, String.valueOf(extraVO.getLogisticsAgent()));
                        }
                        //26:发货地址
                        if (StringUtils.isNotEmpty(extraVO.getSendAddress())) {
                            sheetDataMap.put(26, String.valueOf(extraVO.getSendAddress()));
                        }
                    }
                    //27:会员login_id
                    sheetDataMap.put(27, String.valueOf(proposalModel.getLoginId()));
                    //28:会员member id
                    sheetDataMap.put(28, String.valueOf(proposalModel.getMemberId()));
                    //29:公司名称
                    sheetDataMap.put(29, String.valueOf(proposalModel.getCompany()));
                    //30:报名时间
                    sheetDataMap.put(30, sdf.format(proposalModel.getGmtCreate()));
                    //31:以后一次审核时间
                    ListOf<DcProposalAuditLogModel> rpcResult = null;
                    if (proposalModel.getId() != null) {
                        rpcResult = proposalReadService.listProposalAuditLog(
                            proposalModel.getId());
                    }
                    String status = proposalModel.getStatus();
                    if (rpcResult != null && rpcResult.isSuccess() && CollectionUtils.isNotEmpty(rpcResult.getData())) {
                        List<DcProposalAuditLogModel> auditLogList = rpcResult.getData();
                        if (auditLogList.get(0) != null) {
                            //Collections.reverse(auditLogList);
                            Date auditTime = auditLogList.get(0).getGmtCreate();
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(auditTime);
                            sheetDataMap.put(31, String.valueOf(calendar.get(Calendar.YEAR)));
                            sheetDataMap.put(32, String.valueOf(calendar.get(Calendar.MONTH) + 1));
                            sheetDataMap.put(33, String.valueOf(calendar.get(Calendar.DATE)));

                            //34:拒绝类型
                            if (OfferProposalAuditLogStatusEnum.REJECT.value().equals(auditLogList.get(0).getResult()) ||
                                OfferProposalAuditLogStatusEnum.DENY.value().equals(auditLogList.get(0).getResult())) {
                                DcAuditTypeEnum auditType = DcAuditTypeEnum.safeValueOf(auditLogList.get(0).getMsgType());
                                if (auditType != null) {
                                    sheetDataMap.put(35, auditType.getDesc());
                                }
                                //36:拒绝原因
                                sheetDataMap.put(36, auditLogList.get(0).getMsg());
                            }
                        }
                    }
                    //29:新品报名状态
                    if ("init".equals(status)) {
                        sheetDataMap.put(34, "待审核");
                    } else if ("deny".equals(status)) {
                        sheetDataMap.put(34, "提报不通过");
                    } else if ("bind".equals(status)) {
                        sheetDataMap.put(34, "提报通过");
                    } else if ("delete".equals(status)) {
                        sheetDataMap.put(34, "已删除");
                    }

                    ResultOf<DcProposalModel> result = proposalReadService.queryProposalById(
                        proposalModel.getId());
                    if (result != null && result.isSuccess() && result.getData() != null && CollectionUtils.isNotEmpty(result.getData().getSkuInfoList())) {
                        int count = 0;
                        for (SkuInfoModel skuInfoModel: result.getData().getSkuInfoList()) {
                            if (skuInfoModel == null) {
                                continue;
                            }
                            Map<Integer,String> proposalMap = new HashMap<>();
                            if(count == 0){
                                proposalMap.putAll(sheetDataMap);
                            }
                            count++;
                            //5:sku名称
                            proposalMap.put(10, skuInfoModel.getDesc());
                            //8:1688售价
                            if (skuInfoModel.getSkuPrice() != null) {
                                proposalMap.put(11, new BigDecimal(skuInfoModel.getSkuPrice()).divide(new BigDecimal(100)).toString());
                            }
                            //9:供货价
                            if (skuInfoModel.getPurchasePrice() != null) {
                                proposalMap.put(12, new BigDecimal(skuInfoModel.getPurchasePrice()).divide(new BigDecimal(100)).toString());
                            }
                            //10：供货库存
                            if (null != skuInfoModel.getStock()) {
                                proposalMap.put(13, String.valueOf(skuInfoModel.getStock()));
                            }
                            sheetDatas.add(proposalMap);
                        }
                    }
                }
                if(CollectionUtils.isEmpty(sheetDatas)){
                    return;
                }
                String filePath = "";
                try {
                    //文件名称
                    String fileName = "提报信息导出" + System.currentTimeMillis();
                    filePath = TempFileUtil.createTempXlsxFile(fileName);
                    FileOutputStream bOutput = new FileOutputStream(filePath);
                    //写入内存
                    writeFile(bOutput, easyExcelHead, sheetDatas);

                    dataShareFileService.uploadFile(fileName, filePath, Lists.newArrayList(empNo));
                } catch (IOException ioException) {
                    log.error("create excel or upload file error", ioException);
                } finally {
                    File file = new File(filePath);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            } catch (Exception e) {
                log.error("create excel or upload file error", e);
            } finally {
                setStart(true, false);
            }
        }
    }


    class ProposalExcelExportThread2 extends AbstractProposalExportThread {

        private String[] easyExcelHead = new String[]{"企划ID", "企划名称", "企划类型", "1688商品ID", "1688一级类目", "1688叶子类目", "1688商品标题", "淘系一级类目", "淘系叶子类目", "在供货中心的审核状态", "通往下游渠道名称",
                "在下游渠道审核状态", "是否已缴纳保证金", "是否在渠道店铺上架销售", "最后一次审核小二", "最后一次审核时间(年)", "最后一次审核时间(月)", "最后一次审核时间(日)", "格式化的原因分类", "原因详情"};

        @Override
        public void run() {
            try {
                if(!setStart(false,true)){
                    return;
                }
                List<DcProposalModel> internalOfferProposalModelList = Lists.newArrayList();
                queryProposal(type, queryParam, ids, internalOfferProposalModelList);
                if(CollectionUtils.isEmpty(internalOfferProposalModelList)){
                    log.error("query offer import error,data empty");
                    return;
                }

                List<Long> offerIds = new ArrayList<>();
                for (DcProposalModel proposal : internalOfferProposalModelList){
                    offerIds.add(proposal.getOfferId());
                }
                ListOf<DcOfferEnrollModel> enrollMs = null;
                if (CollectionUtils.isNotEmpty(offerIds)) {
                    enrollMs = dcProductEnrollService.findByOfferIds(offerIds);
                }
                Map<Long,List<DcOfferEnrollModel>> offerCahnnelMap = new HashMap<>();
                if(enrollMs != null && enrollMs.isSuccess() && !CollectionUtils.isEmpty(enrollMs.getData())){
                    for(DcOfferEnrollModel enrollM : enrollMs.getData()){
                        if (enrollM == null || enrollM.getEnrollStatus().equals(DcOfferEnrollStatusEnum.DELETE.getValue())) {
                            continue;
                        }
                        if(offerCahnnelMap.containsKey(enrollM.getOfferId())){
                            offerCahnnelMap.get(enrollM.getOfferId()).add(enrollM);
                        }else{
                            List<DcOfferEnrollModel> channelList = new ArrayList<>();
                            channelList.add(enrollM);
                            offerCahnnelMap.put(enrollM.getOfferId(),channelList);
                        }
                    }
                }

                Map<Integer, StdCategoryModel> b2bStdMap  = categoryReadService.getChannelAllStdCategoryMap(StdCategoryChannelEnum.CHANNEL_1688);
                Map<Integer, StdCategoryModel> tbStdMap  = categoryReadService.getChannelAllStdCategoryMap(StdCategoryChannelEnum.CHANNEL_TB);
                //imap类目映射-start
                Map<Long, Long> cateMap = preprocessingIMap(internalOfferProposalModelList,b2bStdMap);
                Map<Long, String> tbFirstCateMap = preprocessingIMap(cateMap,tbStdMap);
                //imap类目映射-end
                List<Map<Integer,String>> sheetDatas = new ArrayList<>();
                Map<String, String> memberAudit = new HashMap<>();
                for(DcProposalModel proposalModel : internalOfferProposalModelList){
                    if (proposalModel == null) {
                        continue;
                    }
                    List<DcOfferEnrollModel> channels = new ArrayList<DcOfferEnrollModel>();
                    if (MapUtils.isNotEmpty(offerCahnnelMap)) {
                        channels = offerCahnnelMap.get(proposalModel.getOfferId());
                    }
                    //没有铺货到渠道的也显示出来
                    //if(CollectionUtils.isEmpty(channels)){
                    //    continue;
                    //}
                    Map<Integer, String> sheetDataMap = new HashMap<>();

                    //查询提报商品关联的企划信息
                    PlanningOppItemModel byImportId = planningOppItemService.findByImportId(proposalModel.getId());
                    if (null != byImportId) {
                        PlanningDesignModel byPlanningId = planningDesignService.find(byImportId.getPlanningId());
                        if (null != byPlanningId) {
                            sheetDataMap.put(1, String.valueOf(byImportId.getPlanningId()));
                            sheetDataMap.put(2, byPlanningId.getTitle());
                            sheetDataMap.put(3, byPlanningId.getType().getMessage());
                        }
                    }

                    //4:1688offer id
                    sheetDataMap.put(4, String.valueOf(proposalModel.getOfferId()));
                    //5:1688一级类目
                    if (proposalModel.getCateIdLv1() != null && b2bStdMap.containsKey(proposalModel.getCateIdLv1().intValue())) {
                        sheetDataMap.put(5, b2bStdMap.get(proposalModel.getCateIdLv1().intValue()).getName());
                    }
                    //6:1688叶子类目
                    sheetDataMap.put(6, getLeafCateName(b2bStdMap, proposalModel));
                   //7:商品标题
                    sheetDataMap.put(7, proposalModel.getOfferTitle());

                    Long leafCateId = getLeafCateId(proposalModel);
                    //8:淘系一级
                    if(leafCateId!=null && tbFirstCateMap.containsKey(leafCateId)){
                        sheetDataMap.put(8, tbFirstCateMap.get(leafCateId));
                    }
                    //9:淘系叶子
                    if(leafCateId!=null && cateMap.containsKey(leafCateId) && tbStdMap.containsKey(cateMap.get(leafCateId).intValue())){
                        StdCategoryModel tbCateModel = tbStdMap.get(cateMap.get(leafCateId).intValue());
                        if(tbCateModel!=null){
                            sheetDataMap.put(9, tbCateModel.getName());
                        }
                    }
                    //10:新品报名状态
                    String proposalStatus = proposalModel.getStatus();
                    if("init".equals(proposalStatus)){
                        sheetDataMap.put(10, "待审核");
                    }else if("deny".equals(proposalStatus)){
                        sheetDataMap.put(10, "提报不通过");
                    }else if("bind".equals(proposalStatus)){
                        sheetDataMap.put(10, "提报通过");
                    }else if("delete".equals(proposalStatus)){
                        sheetDataMap.put(10, "已删除");
                    }

                    //把审核提出来
                    ListOf<DcProposalAuditLogModel> rpcResult = null;
                    if (proposalModel.getId() != null) {
                        rpcResult = proposalReadService.listProposalAuditLog(proposalModel.getId());
                    }
                    if (rpcResult != null && rpcResult.isSuccess() && CollectionUtils.isNotEmpty(rpcResult.getData())) {
                        List<DcProposalAuditLogModel> auditLogList = rpcResult.getData();
                        if (auditLogList.get(0) != null && auditLogList.get(0).getGmtCreate() != null) {
                            DcProposalAuditLogModel proposalAuditLogModel = auditLogList.get(0);
                            //Collections.reverse(auditLogList);
                            if (proposalAuditLogModel.getOperator() != null) {
                                sheetDataMap.put(15, proposalAuditLogModel.getOperator().getName());
                            }
                            //10:最后一次审核时间
                            if (proposalAuditLogModel.getGmtCreate() != null) {
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(proposalAuditLogModel.getGmtCreate());
                                int year = cal.get(Calendar.YEAR);//获取年份
                                int month=cal.get(Calendar.MONTH)+1;//获取月份
                                int day=cal.get(Calendar.DATE);//获取日
                                sheetDataMap.put(16, String.valueOf(year));
                                sheetDataMap.put(17, String.valueOf(month));
                                sheetDataMap.put(18, String.valueOf(day));
                            }
                            //最后一次
                            if (OfferProposalAuditLogStatusEnum.REJECT.value().equals(proposalAuditLogModel.getResult()) ||
                                OfferProposalAuditLogStatusEnum.DENY.value().equals(proposalAuditLogModel.getResult())) {
                                if (DcAuditTypeEnum.safeValueOf(proposalAuditLogModel.getMsgType()) != null) {
                                    sheetDataMap.put(19, DcAuditTypeEnum.safeValueOf(proposalAuditLogModel.getMsgType()).getDesc());
                                }
                                sheetDataMap.put(20, proposalAuditLogModel.getMsg());
                            }
                        }
                    }
                    if (CollectionUtils.isNotEmpty(channels)) {
                        for (int i = 0; i < channels.size(); i++) {
                            DcOfferEnrollModel model = channels.get(i);
                            if (model == null) {
                                continue;
                            }
                            Map<Integer, String> paramMap = new HashMap<>();
                            if (i == 0) {
                                paramMap.putAll(sheetDataMap);
                            }
                            String channel = model.getChannel();
                            try {
                                //4：渠道
                                //添加测试渠道
                                if("TTTM_TEST_01".equals(channel)){
                                    paramMap.put(11, "测试一店");
                                }else if("TTTM_TEST_02".equals(channel)){
                                    paramMap.put(11, "测试二店");
                                }else if("TTTM_TEST_03".equals(channel)){
                                    paramMap.put(11, "测试三店");
                                }else {
                                    ChannelMarketEnum channe = ChannelMarketEnum.valueOf(channel);
                                    paramMap.put(11, channe.getDesc());
                                }
                            } catch (Exception e) {
                                //ignore
                            }
                            //5：渠道审核状态
                            String status = model.getAuditStatus();
                            if (org.apache.commons.lang.StringUtils.isEmpty(status)) {
                                paramMap.put(12, "待审核");
                            } else {
                                DcAuditStatusEnum statusEnum = DcAuditStatusEnum.valueOf(status);
                                paramMap.put(12, statusEnum.getDesc());
                            }
                            //7:是否已缴纳保证金
                            if (memberAudit.containsKey(proposalModel.getMemberId() + model.getChannel())) {
                                paramMap.put(13, memberAudit.get(proposalModel.getMemberId() + model.getChannel()));
                            } else {
                                boolean isJoined = dcSupplierService.isSupplierJoin(proposalModel.getMemberId(),model.getChannel());
                                //ResultOf<Boolean> isJoined = ResultOf.general(true);
                                if (isJoined) {
                                    paramMap.put(13, "是");
                                    memberAudit.put(proposalModel.getMemberId() + model.getChannel(), "是");
                                } else {
                                    paramMap.put(13, "否");
                                    memberAudit.put(proposalModel.getMemberId() + model.getChannel(), "否");
                                }
                            }
                            //8:是否上架
                            if (DcAuditStatusEnum.THIRD_APPROVE.getValue().equals(status)) {
                                paramMap.put(14, "是");
                            } else {
                                paramMap.put(14, "否");
                            }
                            sheetDatas.add(paramMap);
                        }
                    } else {
                        //否则 5/6/7/8空着
                        sheetDataMap.put(14, "否");
                        sheetDatas.add(sheetDataMap);
                    }
                }
                if(CollectionUtils.isEmpty(sheetDatas)){
                    return;
                }
                String filePath = "";
                try {
                    //文件名称
                    String fileName = "提报信息导出" + System.currentTimeMillis();
                    filePath = TempFileUtil.createTempXlsxFile(fileName);
                    FileOutputStream bOutput = new FileOutputStream(filePath);
                    //写入内存
                    writeFile(bOutput, easyExcelHead, sheetDatas);

                    dataShareFileService.uploadFile(fileName, filePath, Lists.newArrayList(empNo));
                } catch (IOException ioException) {
                    log.error("create excel or upload file error", ioException);
                } finally {
                    File file = new File(filePath);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            } catch (Exception e) {
                log.error("create excel or upload file error", e);
            } finally {
                setStart(true, false);
            }
        }
    }

    private void queryProposal(int type,  DcProposalQueryParam queryParam, List<Long> ids,
                               List<DcProposalModel> internalOfferProposalModelList) {
        int pageIdx = 1;
        int pageSize = 20;
        if (type == 1) {
            while (true) {
                queryParam.setPageNo(pageIdx++);
                queryParam.setPageSize(pageSize);
                if ("TOTAL".equalsIgnoreCase(queryParam.getStatus())) {
                    queryParam.setStatus(null);
                }
                PageOf<DcProposalModel> pageResult = proposalReadService.listProposal(queryParam);
                if (pageResult == null || !pageResult.isSuccess()) {
                    log.error("InternalOfferProposalModel_Illegal_Null");
                    continue;
                }
                if (CollectionUtils.isEmpty(pageResult.getData())) {
                    break;
                }
                List<DcProposalModel> proposalModels = pageResult.getData();
                internalOfferProposalModelList.addAll(proposalModels);
                if(proposalModels.size()<pageSize){
                    break;
                }
            }
        }else if(type == 2){
            ListOf<DcProposalModel> offerImportList = proposalReadService.listProposalByIds(ids);
            if (offerImportList==null || !offerImportList.isSuccess()) {
                log.error("query offer import error", offerImportList.getErrorMessage());
                return ;
            }
            internalOfferProposalModelList.addAll(offerImportList.getData());
        }
    }

    private String getLeafCateName(Map<Integer, StdCategoryModel> stdMap, DcProposalModel proposalModel) {
        if(null!=proposalModel.getCateIdLv3()){
            return stdMap.get(proposalModel.getCateIdLv3().intValue()).getName();
        }else if(null!=proposalModel.getCateIdLv2()){
            return stdMap.get(proposalModel.getCateIdLv2().intValue()).getName();
        }else if(null!=proposalModel.getCateIdLv1()){
            return stdMap.get(proposalModel.getCateIdLv1().intValue()).getName();
        }
        return null;
    }

    private Long getLeafCateId(DcProposalModel proposalModel) {
        if(null!=proposalModel.getCateIdLv3()){
            return proposalModel.getCateIdLv3();
        }else if(null!=proposalModel.getCateIdLv2()){
            return proposalModel.getCateIdLv2();
        }else if(null!=proposalModel.getCateIdLv1()){
            return proposalModel.getCateIdLv1();
        }
        return null;
    }

    private List<List<String>> createSheetHead(String[] excelHead) {
        List<List<String>> result = new ArrayList<>();
        for (String head : excelHead) {
            List<String> headList = Lists.newArrayList(head);
            result.add(headList);
        }
        return result;
    }

    private List<List<Object>> getSheetData(List<Map<Integer, String>> data, int maxRow) {
        List<List<Object>> result = Lists.newArrayList();
        if (org.springframework.util.CollectionUtils.isEmpty(data)) {
            return result;
        }
        for (Map<Integer, String> rowMap : data) {
            List<Object> rowData = Lists.newArrayList();
            for (int i = 1; i <= maxRow; i++) {
                rowData.add(rowMap.get(i));
            }
            result.add(rowData);
        }
        return result;
    }

    private void writeFile(OutputStream out, String[] header, List<Map<Integer, String>> data) {
        ExcelWriter writer = EasyExcelFactory.getWriter(out);
        //写第一个sheet, sheet1  数据全是List<String> 无模型映射关系
        Sheet sheet1 = new Sheet(1, 3);
        sheet1.setSheetName("商品提报信息");
        sheet1.setHead(createSheetHead(header));
        //or 设置自适应宽度
        sheet1.setAutoWidth(Boolean.TRUE);
        writer.write1(getSheetData(data, header.length), sheet1);
        //关闭资源
        writer.finish();
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                log.error("close excel error", e.getMessage(), e);
            }
        }
    }

    /**
     * 获取imap算出来的淘宝id
     * @param internalOfferProposalModelList
     * @param stdMap 1688类目信息
     * @return
     */
    private Map<Long, Long> preprocessingIMap(List<DcProposalModel> internalOfferProposalModelList,
        Map<Integer, StdCategoryModel> stdMap) {
        if (internalOfferProposalModelList.isEmpty()) {
            return Maps.newHashMap();
        }

        List<SourceCategory> sourceCategoryList = new ArrayList<>();
        for (DcProposalModel proposalModel : internalOfferProposalModelList) {
            Integer categoryId = null;
            String categoryName = null;
            if (null != proposalModel.getCateIdLv3()) {
                categoryId = stdMap.get(proposalModel.getCateIdLv3().intValue()).getCategoryId();
                categoryName = stdMap.get(proposalModel.getCateIdLv3().intValue()).getName();
            } else if (null != proposalModel.getCateIdLv2()) {
                categoryId = stdMap.get(proposalModel.getCateIdLv2().intValue()).getCategoryId();
                categoryName = stdMap.get(proposalModel.getCateIdLv2().intValue()).getName();
            } else if (null != proposalModel.getCateIdLv1()) {
                categoryId = stdMap.get(proposalModel.getCateIdLv1().intValue()).getCategoryId();
                categoryName = stdMap.get(proposalModel.getCateIdLv1().intValue()).getName();
            }
            if (categoryId != null) {
                SourceCategory sourceCategory = new SourceCategory();
                sourceCategory.setSrcCateId(categoryId.longValue());
                sourceCategory.setSrcCateText(categoryName);
                sourceCategoryList.add(sourceCategory);
            }
        }
        if(CollectionUtils.isEmpty(sourceCategoryList)){
            return Maps.newHashMap();
        }

        Map<Long, Long> catMap = new HashMap<>();
        int pageSize = 20;
        int pageCount = sourceCategoryList.size() % pageSize == 0 ? sourceCategoryList.size() / pageSize
            : sourceCategoryList.size() / pageSize + 1;
        for(int i = 1 ;i <= pageCount ; i++){
            int start = (i-1) * pageCount;
            int end = i * pageCount ;
            if(i==pageCount){
                end = sourceCategoryList.size();
            }
            try {
                List<SourceCategory> subList = sourceCategoryList.subList(start, end);
                List<DesCategory> taoCatList = categoryPredictService.CategoryPredictPerLevel(
                    sourceCategoryList.subList(start, end),
                    CategoryLevel.LEAF);

                if (CollectionUtils.isNotEmpty(taoCatList)) {
                    for (int idx = 0; idx < taoCatList.size(); ++idx) {
                        long srcCatId = subList.get(idx).getSrcCateId();
                        long taoCatId = taoCatList.get(idx).getDesCateId();
                        catMap.put(srcCatId, taoCatId);
                    }
                }
            }catch(Exception e){
                log.error("get imap category info error !",e);
                continue;
            }
        }
        return catMap;
    }


    private Map<Long, String> preprocessingIMap(Map<Long, Long> cateMap,Map<Integer, StdCategoryModel> stdMap) {
        if(cateMap==null || cateMap.isEmpty()){
            return Maps.newHashMap();
        }
        Map<Long, String> result = Maps.newHashMap();
        for(Entry<Long,Long> entry : cateMap.entrySet()){
            Long value = entry.getValue();
            StdCategoryModel categoryModel =  stdMap.get(value.intValue());
            if(categoryModel==null){
                continue;
            }
            while(stdMap.containsKey(categoryModel.getParentId()) && categoryModel.getParentId()!=0){
                categoryModel = stdMap.get(categoryModel.getParentId());
            }
            result.put(entry.getKey(),categoryModel.getName());
        }
        return result;
    }


}
