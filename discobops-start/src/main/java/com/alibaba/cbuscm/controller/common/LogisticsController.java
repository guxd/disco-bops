package com.alibaba.cbuscm.controller.common;

import com.alibaba.cbuscm.vo.common.LogisticsDetailVO;
import com.alibaba.cbuscm.vo.common.LogisticsSubVO;
import com.alibaba.shared.carriage.common.model.ListResult;
import com.alibaba.shared.carriage.tracking.model.LogisticsTraceDetail;
import com.alibaba.cbuscm.vo.common.LogisticsTraceStep;
import com.alibaba.shared.carriage.tracking.param.QueryTraceParam;
import com.alibaba.shared.carriage.tracking.service.LogisticsTraceService;
import com.alibaba.up.common.mybatis.result.RPCResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.testng.collections.Lists;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

/**
 * @author xuhb
 * @title: LogisticsController
 * @projectName discobops
 * @description: 物流信息查询
 * @date 2019-05-1615:49
 */
@RestController
@Slf4j
@RequestMapping("/logistics")
public class LogisticsController {

    @Autowired
    LogisticsTraceService logisticsTraceServic;

    @RequestMapping("/detail")
    public RPCResult getLogisticsDetail(HttpServletRequest request,@RequestParam(name="sellerId",required = true) Long sellerId, @RequestParam(name="logisticsId",required = true) String logisticsId) {
        if(StringUtils.isEmpty(logisticsId)){
            return RPCResult.ok();
        }
        QueryTraceParam param = new QueryTraceParam();
        param.setSenderUserId(sellerId);
        param.setLogisticsId(logisticsId);
        param.setTradeSourceType("TRADE");
        param.setOperatorIp(request.getRemoteAddr());
        ListResult<LogisticsTraceDetail> logisticsTraceDetailListResult = logisticsTraceServic.queryOrderTraceByOrderId(param);
        if (logisticsTraceDetailListResult == null || CollectionUtils.isEmpty(logisticsTraceDetailListResult.getModelList())) {
            return RPCResult.ok();
        }
        LogisticsTraceDetail detail = logisticsTraceDetailListResult.getModelList().get(0);
        LogisticsDetailVO detailVO = new LogisticsDetailVO();
        detailVO.setMailNo(detail.getMailNo());
        detailVO.setCompanyName(detail.getCpName());
        List<LogisticsSubVO> steps = Lists.newArrayList();
        detailVO.setDateline(steps);
        if (detail.getGroupTracesMap() != null && !detail.getGroupTracesMap().isEmpty()) {
            for (String key : detail.getGroupTracesMap().keySet()) {
                LogisticsSubVO vo = new LogisticsSubVO();
                vo.setDate(key);
                steps.add(vo);
                final List<LogisticsTraceStep> timeLine = Lists.newArrayList();
                vo.setTimeLine(timeLine);
                if (!CollectionUtils.isEmpty(detail.getGroupTracesMap().get(key))) {
                    detail.getGroupTracesMap().get(key).stream().forEach(a -> {
                        LogisticsTraceStep step = new LogisticsTraceStep();
                        BeanUtils.copyProperties(a, step);
                        timeLine.add(step);
                    });
                }
            }
        }
        Collections.sort(steps);
        return RPCResult.ok(detailVO);
    }

}
