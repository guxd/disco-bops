package com.alibaba.cbuscm.controller.onepiece.supplement;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementAdminReadService;
import com.alibaba.cbu.overseas.interfaces.supplement.SupplementAdminWriteService;
import com.alibaba.cbu.overseas.models.supplement.SupplementItemModel;
import com.alibaba.cbu.overseas.models.supplement.SupplementOfferModel;
import com.alibaba.cbu.overseas.models.supplement.SupplementProposalModel.AuditStatusEnum;
import com.alibaba.cbu.overseas.models.supplement.SupplementSiteItemModel;
import com.alibaba.cbu.overseas.params.supplement.SupplementItemAdminParam;
import com.alibaba.cbu.overseas.params.supplement.SupplementItemAdminQueryPairParam;
import com.alibaba.cbu.overseas.params.supplement.SupplementProposalAuditParam;
import com.alibaba.cbu.overseas.wrapper.ResultModel;
import com.alibaba.cbuscm.utils.MoneyUtils;
import com.alibaba.cbuscm.vo.onepiece.supplement.SupplementConfigVO;
import com.alibaba.cbuscm.vo.onepiece.supplement.SupplementItemAuditVO;
import com.alibaba.cbuscm.vo.onepiece.supplement.SupplementProposalVO;
import com.alibaba.china.global.business.library.common.PageOf;
import com.alibaba.china.global.business.library.interfaces.search.SearchDownstreamProductService;
import com.alibaba.china.global.business.library.interfaces.selection.SelectionResultWriteService;
import com.alibaba.china.global.business.library.models.search.DownstreamProductModel;
import com.alibaba.china.global.business.library.models.search.ha3.QueryRuleModel;
import com.alibaba.china.global.business.library.models.selection.CbuOfferModel;
import com.alibaba.china.global.business.library.models.selection.DsItemModel;
import com.alibaba.china.global.business.library.params.search.DownstreamProductSearchParam;
import com.alibaba.china.global.business.library.params.selection.DsSelectionUpdateParam;
import com.alibaba.security.spring.component.csrf.CsrfTokenModel;
import com.alibaba.up.common.mybatis.result.RPCResult;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CsrfTokenModel
@RequestMapping(path = "/one-piece/supplement", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class SupplementAdminController {

    @Autowired
    private SupplementAdminReadService supplementAdminReadService;

    @Autowired
    private SupplementAdminWriteService supplementAdminWriteService;

    @Autowired
    private SearchDownstreamProductService searchDownstreamProductService;

    @Autowired
    private SelectionResultWriteService selectionResultWriteService;

    @ResponseBody
    @RequestMapping(path = "/item", method = RequestMethod.GET)
    public RPCResult getItem(@RequestParam("cbuItemId") Long cbuItemId,
                             @RequestParam("dsItemId") String dsItemId,
                             HttpServletRequest request) {
        try {
            ResultModel<SupplementItemModel> result = querySupplement(cbuItemId, dsItemId);
            if (!ResultModel.isNotNull(result)) {
                return RPCResult.error("参数错误", "无关联补品");
            }
            ResultModel<SupplementItemModel> item = supplementAdminReadService.findSupplementItemById(
                SupplementItemAdminParam.builder()
                    .adminId(getEmpId(request))
                    .itemId(result.getModel().getId())
                    .build());
            if (!ResultModel.isNotNull(item)) {
                return RPCResult.error("参数错误", "补品查询失败");
            }
            return RPCResult.ok(convert(item.getModel()));
        } catch (Exception e) {
            log.error("SupplementAdminController.getItem error!", e);
            return RPCResult.error("系统错误", e.getMessage());
        }

    }

    @ResponseBody
    @RequestMapping(path = "/item", method = RequestMethod.DELETE)
    public RPCResult deleteItem(@RequestBody ProposalDeletePostBody body,
                                HttpServletRequest request) {
        ResultModel<SupplementItemModel> result = querySupplement(body.getCbuItemId(), body.getDsItemId());
        if (!ResultModel.isNotNull(result)) {
            return RPCResult.error("参数错误", "无关联补品");
        }
        Long taskId = result.getModel().getTaskId();
        String status = "DELETE";
        List<String> ids = new ArrayList<String>() {{ add(body.getDsItemId() + "&|^" + body.getCbuItemId()); }};
        DsSelectionUpdateParam dsSelectionUpdateParam = DsSelectionUpdateParam
            .builder()
            .status(status)
            .taskId(taskId)
            .cbuItemAndDsItemIds(Lists.newArrayList(ids))
            .build();
        selectionResultWriteService.update(dsSelectionUpdateParam);
        return RPCResult.ok(supplementAdminWriteService.removeSupplementItem(SupplementItemAdminParam.builder()
            .adminId(getEmpId(request))
            .itemId(result.getModel().getId())
            .build()));
    }

    @ResponseBody
    @RequestMapping(path = "/item", method = RequestMethod.POST)
    public RPCResult postProposalAudit(@RequestBody ProposalAuditPostBody body, HttpServletRequest request) {
        ResultModel<SupplementItemModel> result = querySupplement(body.getCbuItemId(), body.getDsItemId());
        if (!ResultModel.isNotNull(result)) {
            return RPCResult.error("参数错误", "无关联补品");
        }
        String adminId = getEmpId(request);
        if (body.getParams() != null) {
            body.getParams().forEach(it -> it.setAuditorId(adminId));
        }
        return RPCResult.ok(
            supplementAdminWriteService.batchAuditProposal(result.getModel().getId(), body.getParams()));
    }

    private DownstreamProductModel searchDsProduct(String site, String itemId) {
        QueryRuleModel queryRule = new QueryRuleModel();
        queryRule.setPageStart(1);
        queryRule.setPageSize(1);
        queryRule.addQueryEqualTo("item_site_id_str", String.format("%s$%s", itemId, site));
        DownstreamProductSearchParam searchParam = new DownstreamProductSearchParam();
        searchParam.setSearchBy("overseas-bops");
        searchParam.setQueryRuleModel(queryRule);
        PageOf<DownstreamProductModel> result = searchDownstreamProductService.search(searchParam);
        if (PageOf.isNonEmpty(result)) {
            return result.getData().get(0);
        }
        return null;
    }

    @Getter
    @Setter
    @ToString
    private static class ProposalDeletePostBody {
        private Long cbuItemId;
        private String dsItemId;
    }

    @Getter
    @Setter
    @ToString
    private static class ProposalAuditPostBody {
        private Long cbuItemId;
        private String dsItemId;
        private List<SupplementProposalAuditParam> params;
    }

    private String getEmpId(HttpServletRequest httpServletRequest) {
        try {
            return Optional.ofNullable(SimpleUserUtil.getBucSSOUser(httpServletRequest))
                .map(BucSSOUser::getEmpId)
                .orElse(null);
        } catch (Exception e) {
            log.error("getBucSSOUser error!", e);
            return null;
        }
    }

    private ResultModel<SupplementItemModel> querySupplement(Long cbuItemId, String dsItemId) {
        String[] dsSite = dsItemId.split("\\$");
        SupplementItemAdminQueryPairParam pair = SupplementItemAdminQueryPairParam
            .builder()
            .offerId(cbuItemId)
            .siteItemId(dsSite[0])
            .site(dsSite[1])
            .build();
        return supplementAdminReadService.findSupplementItemByRelation(pair);
    }

    private SupplementItemAuditVO convert(SupplementItemModel item) {
        SupplementItemAuditVO vo = new SupplementItemAuditVO();
        vo.setCbuItem(convert(item.getOffer()));
        vo.setDsItem(convert(item.getSiteItem()));
        vo.setConfig(SupplementConfigVO.builder()
            .proposalCount(item.getProposalCount())
            .supplyCount(item.getSupplyCount())
            .supplyLimit(item.getSupplyLimit())
            .supplyProgress(item.getSupplyProgress())
            .build());
        if (item.getProposalItems() != null && !item.getProposalItems().isEmpty()) {
            vo.setProposalList(item.getProposalItems().stream()
                .map(r -> SupplementProposalVO
                    .builder()
                    .offerId(r.getProposalOfferId())
                    .pic(r.getProposalOffer().getPic())
                    .price("¥" + MoneyUtils.fenToYuan(r.getProposalOffer().getPrice()))
                    .status(r.getAuditStatus())
                    .locked(!AuditStatusEnum.PROPOSED.name().equals(r.getAuditStatus()))
                    .build())
                .collect(Collectors.toList()));
        }
        return vo;
    }

    private CbuOfferModel convert(SupplementOfferModel item) {
        CbuOfferModel model = new CbuOfferModel();
        model.setItemId(item.getOfferId());
        model.setItemUrl(String.format("https://detail.1688.com/offer/%d.html", item.getOfferId()));
        model.setPic(String.format("https://cbu01.alicdn.com/%s", item.getPic()));
        model.setPrice("CNY " + MoneyUtils.fenToYuan(item.getPrice()));
        model.setTitle(item.getTitle());
        return model;
    }

    private DsItemModel convert(SupplementSiteItemModel item) {
        DownstreamProductModel ds = searchDsProduct(item.getSite(), item.getItemId());
        DsItemModel model = new DsItemModel();
        model.setSiteId(item.getSite());
        model.setSiteItemId(item.getItemId() + "$" + item.getSite());
        model.setItemUrl(Optional.ofNullable(ds).map(DownstreamProductModel::getDetailUrl).orElse(null));
        model.setPic(item.getPic());
        model.setPrice(item.getCurrency() + " " + MoneyUtils.fenToYuan(item.getPrice()));
        model.setTitle(item.getTitle());
        return model;
    }

}
