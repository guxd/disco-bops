/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.controller.domestic.tb.supplier.tag;

import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.shared.discosupplier.enums.SupplierSceneEnum;
import com.alibaba.china.shared.discosupplier.model.supplier.SupplierTagModel;
import com.alibaba.china.shared.discosupplier.param.supplier.SupplierTagParam;
import com.alibaba.china.shared.discosupplier.service.supplier.SupplierTagService;
import com.alibaba.up.common.mybatis.result.RPCResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.List;

/**
 * 标签列表
 *
 * @author yaogaolin
 */
@RestController
@RequestMapping("/supplier/tag")
public class TagController {
    private static final Logger LOG = LoggerFactory.getLogger(TagController.class);

    @Autowired
    private SupplierTagService supplierTagService;
    @Autowired
    private MemberReadService memberReadService;

    /**
     * 淘大店标签列表
     */
    @RequestMapping(value = "/list4Tao", method = RequestMethod.GET)
    public RPCResult list(HttpServletRequest request,
                          @RequestParam(name = "channel", required = true) String channel) {
        if (StringUtils.isEmpty(channel)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "param is null");
        }

        SupplierTagParam param = new SupplierTagParam();
        if(StringUtils.equals(channel, "domesrtic")){
            param.setSceneList(SupplierSceneEnum.getDomesrticList());
        }else if(StringUtils.equals(channel, "crossBorder")){
            param.setSceneList(SupplierSceneEnum.getCrossBorderList());
        }else if(StringUtils.equals(channel, SupplierSceneEnum.ALIQYG.getValue())){
            param.setSceneList(Arrays.asList(SupplierSceneEnum.ALIQYG));
        }

        List<SupplierTagModel> tags = supplierTagService.queryAll(param);

        return RPCResult.ok(tags);
    }




}