package com.alibaba.cbuscm.controller.domestic.tb.enroll;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditEmpType;
import com.alibaba.cbuscm.service.authority.AclControlPermissionLogic;
import com.alibaba.cbuscm.utils.LoginUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jianhao.yzy
 * @Description:
 * @date 2019/7/10上午10:49
 */
@RestController
@RequestMapping("/enroll")
public class HelloEnrollController {

    //这个类无需引入，boot中自带
    @Autowired
    AclControlPermissionLogic aclControlPermissionLogic;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public List<String> hello(HttpServletRequest request, Map<String,Object> context) throws IOException, ServletException {

        String empId = LoginUtil.getEmpId(request);

        Integer userId = LoginUtil.getUserId(request);

        String empName = LoginUtil.getLoginName(request);
        
        DcAuditEmpType authType = aclControlPermissionLogic.hasTaoShopPermission(userId);


        List<String> list = new ArrayList<>();
        list.add(authType == null ? null : authType.getValue());
        list.add(empId);
        list.add(empName);
        list.add(userId == null ? "null" : userId.toString());

        return list;
    }


}
