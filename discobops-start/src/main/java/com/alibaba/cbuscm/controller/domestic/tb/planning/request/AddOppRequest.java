package com.alibaba.cbuscm.controller.domestic.tb.planning.request;

import java.util.List;

import com.alibaba.china.shared.discosupplier.service.planning.source.PlanningSourceService.PlanningOpportunity;

import lombok.Data;
import lombok.ToString;

/**
 * 寻源添加商机请求
 *
 * @author yaogaolin 2019-08-22 21:22
 */
@Data
@ToString
public class AddOppRequest {
    /**
     * 寻源id
     */
    private Long sourceId;
    /**
     * 寻源类型
     */
    private String type;
    /**
     * 商机
     */
    private List<PlanningOpportunity> opps;

}
