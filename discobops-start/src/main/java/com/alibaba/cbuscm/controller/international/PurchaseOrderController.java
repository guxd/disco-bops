package com.alibaba.cbuscm.controller.international;

import com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum;
import com.alibaba.cbu.disco.shared.common.constant.ErrorCodeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.config.api.FsShopConfigService;
import com.alibaba.cbu.disco.shared.core.config.model.DomesticAddressConfigModel;
import com.alibaba.cbu.disco.shared.core.config.model.FsShopConfigModel;
import com.alibaba.cbu.disco.shared.core.order.api.FsChannelOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPerformOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPurchaseOperateService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPurchaseOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsTradeInfoService;
import com.alibaba.cbu.disco.shared.core.order.constant.FsPerformStatusEnum;
import com.alibaba.cbu.disco.shared.core.order.constant.FsPurchaseBizStatus;
import com.alibaba.cbu.disco.shared.core.order.constant.FsPurchasePayStatusEnum;
import com.alibaba.cbu.disco.shared.core.order.constant.FsPurchaseRefundStatusEnum;
import com.alibaba.cbu.disco.shared.core.order.model.data.*;
import com.alibaba.cbu.disco.shared.core.order.model.view.FsPurchasePageQueryCondition;
import com.alibaba.cbu.disco.shared.core.order.model.view.FsPurchasePageQueryParam;
import com.alibaba.cbu.disco.shared.core.order.model.view.PurchaseOrderBatchPayParam;
import com.alibaba.cbu.disco.shared.core.order.model.view.PurchaseOrderPayParam;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.common.PageVo;
import com.alibaba.cbuscm.vo.international.*;
import com.alibaba.china.offer.api.query.model.OfferModel;
import com.alibaba.china.offer.api.query.service.OfferQueryService;
import com.alibaba.excel.util.CollectionUtils;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

/**
 * @author haobo.xhb
 * @title: $PurchaseOrderController.java
 * @projectName discobops-start
 * @description: 采购单服务
 * @date 2019-05-08
 */
@RestController
@Slf4j
@RequestMapping("/purchaseOrder")
public class PurchaseOrderController {

    private static final int ONE_DAY_TIME = 24 * 60 * 60 * 1000;


    @Autowired
    private FsPurchaseOrderService purchaseOrderService;

    @Autowired
    private FsPerformOrderService performOrderService;

    @Autowired
    private OfferQueryService<OfferModel> offerQueryService;

    @Autowired
    private FsShopConfigService fsShopConfigService;

    @Autowired
    private FsChannelOrderService channelOrderService;

    @Autowired
    private FsPurchaseOperateService fsPurchaseOperateService;

    @Autowired
    private FsTradeInfoService fsTradeInfoService;

    /**
     * 获取采购单状态
     *
     * @return
     */
    @RequestMapping("/status")
    public RPCResult getPurchaseOrderStatus() {
        FsPurchaseBizStatus[] states = FsPurchaseBizStatus.values();
        List<PurchaseOrderStateVO> result = new ArrayList<PurchaseOrderStateVO>();
        Arrays.asList(states).stream().forEach(state -> {
            PurchaseOrderStateVO vo = new PurchaseOrderStateVO();
            vo.setName(state.getDesc());
            vo.setValue(state.getValue());
            result.add(vo);
        });
        return RPCResult.ok(result);
    }

    /**
     * @param supplierName    供应商名称
     * @param saleOrderId     销售单id
     * @param purchaseOrderId 采购单id
     * @param status          状态
     * @param createTime      采购单生成时间
     * @return
     */
    @RequestMapping("/getGSOrderList")
    public RPCResult getQGOrderList(
                                    @RequestParam(name = "channelId", required = false) String channelId,
                                    @RequestParam(name = "supplierName", required = false) String supplierName,
                                    @RequestParam(name = "saleOrderId", required = false) Long saleOrderId,
                                    @RequestParam(name = "id", required = false) Long purchaseOrderId,
                                    @RequestParam(name = "status", required = false) String status,
                                    @RequestParam(name = "createTime", required = false) Long createTime,
                                    @RequestParam(name = "pageNo", required = false) Integer page,
                                    @RequestParam(name = "pageSize", required = false) Integer pageSize) {

        /*List<PurchaseOrderVO> purchaseOrderVO = getPurchaseOrderVO();
        RPCResult result = RPCResult.ok(purchaseOrderVO);
        PageVo pageVo = new PageVo();
        pageVo.setPageNo(1);
        pageVo.setPageSize(20);
        pageVo.setTotal(10);
        result.put("paginator", pageVo);
        return result;*/
        ChannelMarketEnum channelMarketEnum = null;
        if(StringUtils.isBlank(channelId)) {
            channelMarketEnum = ChannelMarketEnum.AE;
        }
        else {
            channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);
        }
        FsPurchasePageQueryParam param = new FsPurchasePageQueryParam();
        FsPurchasePageQueryCondition condition = new FsPurchasePageQueryCondition();
        param.setCondition(condition);
        param.setBizType(channelMarketEnum.getOldMarketCode());
        param.setUserId(getPurchaseSellerId(channelMarketEnum.getOldMarketCode()));
        condition.setSupplierCompanyName(supplierName);
        if (!StringUtils.isEmpty(status)) {
            condition.setBizStatusSet(Sets.newHashSet(status));
        }
        if (purchaseOrderId != null && purchaseOrderId != 0) {
            condition.setPurchaseId(purchaseOrderId);
        }
        if (saleOrderId != null && saleOrderId != 0) {
            condition.setChannelOrderDbId(saleOrderId);
        }
        Date dateFrom = new Date();
        if (createTime != null && createTime > 0) {
            Date beginDa = new Date(createTime);
            Date endDa = new Date(createTime + ONE_DAY_TIME);
            condition.setCreateTimeBegin(beginDa);
            condition.setCreateTimeEnd(endDa);
        }
        if (page == null || page < 1) {
            page = 1;
        }
        if (pageSize == null || pageSize < 1 || pageSize > 100) {
            pageSize = 20;
        }
        param.setPageIdx(page);
        param.setPageSize(pageSize);
        PageOf<FsPurchaseOrderModel> purchaseOrderModelPageOf = purchaseOrderService.listPurchaseOrder(param);
        final Map<Long, Long> purchasePerformMap = Maps.newHashMap();
        final Map<Long, FsPerformSubOrderModel> performOrderMap = new HashMap<Long, FsPerformSubOrderModel>();
        final Map<Long, FsPerformOrderModel> performMainOrderMap = new HashMap<Long, FsPerformOrderModel>();
        if (purchaseOrderModelPageOf != null && !CollectionUtils.isEmpty(purchaseOrderModelPageOf.getData())) {
            //根据采购单获取所有子采购单关联的履约单ID
            final List<Long> performIdList = new ArrayList<Long>();
            purchaseOrderModelPageOf.getData().stream().forEach(purchaseOrder -> {
                if (!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList())) {
                    for (FsPurchaseSubOrderModel purchaseSub : purchaseOrder.getSubOrderList()) {
                        if (purchaseSub.getPerformId() != null && purchaseSub.getPerformId() != 0) {
                            performIdList.add(purchaseSub.getPerformId());
                            purchasePerformMap.put(purchaseSub.getId(), purchaseSub.getPerformId());
                        }
                    }
                }
            });
            /******************************卖家id(ae大店主体) 类型*************************/
            String sellerId = purchaseOrderModelPageOf.getData().get(0).getUserId();
            String bizType = purchaseOrderModelPageOf.getData().get(0).getBizType();
            /**********************************************************/
            if (!CollectionUtils.isEmpty(performIdList)) {
                List<List<Long>> performIdPartList = Lists.partition(performIdList, 100);
                //循环获取 并放入performOrderMap 中
                for (List<Long> performIdPart : performIdPartList) {
                    ListOf<FsPerformOrderModel> performOrderList = performOrderService.queryPerformOrder(sellerId, bizType, performIdPart);
                    if (performOrderList == null || CollectionUtils.isEmpty(performOrderList.getData())) {
                        continue;
                    }
                    performOrderList.getData().stream().forEach(performOrder -> {

                        if (!CollectionUtils.isEmpty(performOrder.getSubOrderList())) {
                            for (FsPerformSubOrderModel subPerform : performOrder.getSubOrderList()) {
                                performOrderMap.put(subPerform.getId(), subPerform);
                                performMainOrderMap.put(subPerform.getId(), performOrder);
                            }
                        }

                    });

                }
            }
        }
        if (!CollectionUtils.isEmpty(purchaseOrderModelPageOf.getData())) {
            List<PurchaseOrderVO> voResults = purchaseOrderModelPageOf.getData().stream().map(purchaseOrder -> convertToPurchaseOrder(purchaseOrder, purchasePerformMap, performOrderMap,performMainOrderMap)).collect(Collectors.toList());
            RPCResult result = RPCResult.ok(voResults);
            PageVo pageVo = new PageVo();
            pageVo.setPageNo(page);
            pageVo.setPageSize(pageSize);
            pageVo.setTotal(purchaseOrderModelPageOf.getTotal());
            result.put("paginator", pageVo);
            return result;
        } else {
            List<PurchaseOrderVO> voResults = Lists.newArrayList();
            RPCResult result = RPCResult.ok(voResults);
            PageVo pageVo = new PageVo();
            pageVo.setPageNo(page);
            pageVo.setPageSize(pageSize);
            pageVo.setTotal(0);
            result.put("paginator", pageVo);
            return result;
        }
    }

    /*private List<PurchaseOrderVO> getPurchaseOrderVO() {
        List<PurchaseOrderVO> purchaseOrderVOList = new ArrayList<>();
        PurchaseOrderVO purchaseOrderVO = new PurchaseOrderVO();
        purchaseOrderVO.setId(41);
        purchaseOrderVO.setQuantity(1);
        purchaseOrderVO.setReceiveQuantity(1);
        purchaseOrderVO.setSaleOrderId("888888wwww5555");
        purchaseOrderVO.setStatusName("待下单");
        purchaseOrderVO.setStatus(FsPurchaseBizStatus.CREATE.getValue());
        purchaseOrderVO.setTotalPay("55");
        purchaseOrderVO.setSupplierName("色漆二无");

        PurchaseOrderVO purchaseOrderVO1 = new PurchaseOrderVO();
        purchaseOrderVO1.setId(43);
        purchaseOrderVO1.setQuantity(22);
        purchaseOrderVO1.setReceiveQuantity(52);
        purchaseOrderVO1.setSaleOrderId("888888wwww5555");
        purchaseOrderVO1.setStatusName("待发货");
        purchaseOrderVO1.setStatus(FsPurchaseBizStatus.WAIT_SEND.getValue());
        purchaseOrderVO1.setTotalPay("66");
        purchaseOrderVO1.setSupplierName("wwqsdsd");

        PurchaseOrderVO purchaseOrderVO2 = new PurchaseOrderVO();
        purchaseOrderVO2.setId(45);
        purchaseOrderVO2.setQuantity(2);
        purchaseOrderVO2.setReceiveQuantity(4);
        purchaseOrderVO2.setSaleOrderId("888888wwww5555");
        purchaseOrderVO2.setStatusName("待付款");
        purchaseOrderVO2.setStatus(FsPurchaseBizStatus.WAIT_PAY.getValue());
        purchaseOrderVO2.setTotalPay("77");
        purchaseOrderVO2.setSupplierName("wwwwqsss");
        purchaseOrderVOList.add(purchaseOrderVO);
        purchaseOrderVOList.add(purchaseOrderVO1);
        purchaseOrderVOList.add(purchaseOrderVO2);
        return purchaseOrderVOList;
    }*/

  /**
     * @param id 采购单id
     * @return
     */
    @RequestMapping("/getGSOrderDetail")
    public RPCResult getQGOrderDetail(@RequestParam(name = "id", required = true) long id,
                                      @RequestParam(name = "userId", required = false) String userId,
                                      @RequestParam(name = "channelId", required = true) String channelId) {

        /*// mock
        PurchaseOrderDetailVO qgOrderDetail = getQGOrderDetail();
        return RPCResult.ok(qgOrderDetail);*/
        ChannelMarketEnum channelMarketEnum = null;
        if(StringUtils.isBlank(channelId)) {
            channelMarketEnum = ChannelMarketEnum.AE;
        }
        else {
            channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);
        }
        String purchaseBizType = channelMarketEnum.getOldMarketCode();
        String purchaseSellerId = getPurchaseSellerId(purchaseBizType);

        FsPurchasePageQueryParam param = new FsPurchasePageQueryParam();
        FsPurchasePageQueryCondition condition = new FsPurchasePageQueryCondition();
        param.setCondition(condition);
        condition.setPurchaseId(id);
        param.setBizType(purchaseBizType);
        param.setUserId(purchaseSellerId);
        PageOf<FsPurchaseOrderModel> purchaseOrderModelPageOf = purchaseOrderService.listPurchaseOrder(param);
        if (purchaseOrderModelPageOf == null || CollectionUtils.isEmpty(purchaseOrderModelPageOf.getData())) {
            return RPCResult.ok();
        }
        FsPurchaseOrderModel purchaseOrder = purchaseOrderModelPageOf.getData().get(0);
        final Map<Long, Long> purchasePerformMap = Maps.newHashMap();
        final Map<Long, FsPerformSubOrderModel> performOrderMap = new HashMap<Long, FsPerformSubOrderModel>();
        List<Long> performIdList = new ArrayList<Long>();
        //获取3段单关联的2段单id 后面获取境外收获地址使用
        final List<String> channelOrderIds = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList())) {
            for (FsPurchaseSubOrderModel purchaseSub : purchaseOrder.getSubOrderList()) {
                if (purchaseSub.getPerformId() != null && purchaseSub.getPerformId() != 0) {
                    performIdList.add(purchaseSub.getPerformId());
                    purchasePerformMap.put(purchaseSub.getId(), purchaseSub.getPerformId());
                }
                if (!CollectionUtils.isEmpty(purchaseSub.getChannelOrderInfoList())) {
                    purchaseSub.getChannelOrderInfoList().forEach(channelOrder -> {
                        channelOrderIds.add(channelOrder.getOrderId());
                    });
                } else if (StringUtils.isEmpty(purchaseSub.getChannelOrderId())) {
                    channelOrderIds.add(purchaseSub.getChannelOrderId());
                }

            }

        }
        /******************************卖家id(ae大店主体) 类型*************************/
        String sellerId = purchaseOrder.getUserId();
        String bizType = purchaseOrder.getBizType();
        /**********************************************************/
        List<FsPerformOrderModel> orderModelList = null;
        if (!CollectionUtils.isEmpty(performIdList)) {
            List<List<Long>> performIdPartList = Lists.partition(performIdList, 20);
            //循环获取 并放入performOrderMap 中
            for (List<Long> performIdPart : performIdPartList) {
                ListOf<FsPerformOrderModel> performOrderList = performOrderService.queryPerformOrder(sellerId, bizType, performIdPart);
                if (performOrderList == null || CollectionUtils.isEmpty(performOrderList.getData())) {
                    continue;
                }
                orderModelList = performOrderList.getData();
                orderModelList.stream().forEach(performOrder -> {
                    if (!CollectionUtils.isEmpty(performOrder.getSubOrderList())) {
                        for (FsPerformSubOrderModel subPerform : performOrder.getSubOrderList()) {
                            performOrderMap.put(subPerform.getId(), subPerform);
                        }
                    }
                });

            }
        }
        PurchaseOrderDetailVO detail = convertToPurchaseOrderDetail(purchaseOrder, purchasePerformMap, performOrderMap);
        /****************************************************************/
        FsShopConfigModel shopConfigure = fsShopConfigService.getB10ShopCoinfig();
        BuyerVO buyer = new BuyerVO();
        buyer.setChannelName(shopConfigure.getChannelName());
        buyer.setCompanyCode(shopConfigure.getCompanyCode());
        buyer.setCompanyName(shopConfigure.getCompanyName());
        SellerVO seller = new SellerVO();
        //seller.setChannelName(shopConfigure.getChannelName());
        //seller.setCompanyCode(shopConfigure.getCompanyCode());
        seller.setCompanyName(purchaseOrder.getSupplierCompanyName());
        DomesticAddressConfigModel domestaicAddress = fsShopConfigService.getDomesticAddressConfig();
        ReceivingAddressModel salerAddess = new ReceivingAddressModel();
        salerAddess.setAddress(domestaicAddress.getAddress());
        salerAddess.setName(domestaicAddress.getName());
        //salerAddess.setTelephone(domestaicAddress.getTelephone());
        salerAddess.setMobile(domestaicAddress.getTelephone());
        detail.setBuyer(buyer);
        detail.setSeller(seller);
        detail.setDomesticAddress(salerAddess);
        // 订单主ID
        if (orderModelList != null && orderModelList.get(0) != null) {
            detail.setOrderId(orderModelList.get(0).getOrderId());
        }
        // 采购单额外信息
        if (purchaseOrder.getFsPurchaseExtra() != null) {
            detail.setErrorCode(purchaseOrder.getFsPurchaseExtra().getMakeOrderErrorCode());
            detail.setErrorMsg(purchaseOrder.getFsPurchaseExtra().getMakeOrderErrorMsg());
        }
        // 订单的付款相关状态
        detail.setPayStatus(purchaseOrder.getPayStatus());
        // 订单的付款相关文案
        if (FsPurchasePayStatusEnum.safeValueOf(purchaseOrder.getPayStatus()) != null) {
            detail.setPayDesc(FsPurchasePayStatusEnum.safeValueOf(purchaseOrder.getPayStatus()).getDesc());
        }
        // 订单的退款相关状态
        detail.setRefundStatus(purchaseOrder.getRefundStatus());
        // 订单的退款相关文案
        if (FsPurchaseRefundStatusEnum.safeValueOf(purchaseOrder.getRefundStatus()) != null) {
            detail.setRefundDesc(FsPurchaseRefundStatusEnum.safeValueOf(purchaseOrder.getRefundStatus()).getDesc());
        }
        // 渠道主订单业务状态
        detail.setBizStatus(purchaseOrder.getBizStatus());
        // 渠道主订单业务文案
        if (FsPurchaseBizStatus.safeValueOf(purchaseOrder.getBizStatus()) != null) {
            detail.setBizDesc(FsPurchaseBizStatus.safeValueOf(purchaseOrder.getBizStatus()).getDesc());
        }
        ReceivingAddressModel buyerAddress = new ReceivingAddressModel();
        //TODO: 缺少根据channel order id  获取channel order 的方法
        //收获地址  现阶段只能先获取第一个 如果要获取多个 需要组织一下如何显示在前端
        if (!CollectionUtils.isEmpty(channelOrderIds)) {
            List<FsChannelOrderModel> channelOrders = channelOrderService.listChannelMainOrder(purchaseSellerId, purchaseBizType, channelOrderIds);
            if (!CollectionUtils.isEmpty(channelOrders)) {
                FsChannelOrderModel first = channelOrders.get(0);
                if (first.getRecvInfo() != null) {
                    buyerAddress.setTelephone(first.getRecvInfo().getTelephone());
                    buyerAddress.setMobile(first.getRecvInfo().getMobile());
                    buyerAddress.setName(first.getRecvInfo().getRecvName());
                    buyerAddress.setAddress(first.getRecvInfo().getAddrDetail());
                }
            }
        }
        detail.setOverseasAddress(buyerAddress);
        /****************************************************************/
        RPCResult result = RPCResult.ok(detail);
        return result;
    }


    /*private PurchaseOrderDetailVO getQGOrderDetail() {
        PurchaseOrderDetailVO detailVO = new PurchaseOrderDetailVO();
        detailVO.setErrorMsg("不想要了");
        detailVO.setErrorCode("QQE_qw");
        detailVO.setBizDesc("待付款");
        detailVO.setBizStatus("WAIT_PAY");
        detailVO.setRefundDesc("不要了");
        detailVO.setRefundStatus("www_qqw");
        detailVO.setPayDesc("已付款");
        detailVO.setPayStatus("wwqq_qrq");
        detailVO.setOrderId("ss44444444465555");
        detailVO.setChannelName("搜索");
        BuyerVO buyerVO = new BuyerVO();
        buyerVO.setChannelName("AE中东大店");
        buyerVO.setCompanyCode("Singapore E-commerce Private Limited");
        buyerVO.setCompanyName("AE中东大店");
        detailVO.setBuyer(buyerVO);
        detailVO.setCreateTime(1566306414000L);
        ReceivingAddressModel receivingAddressModel = new ReceivingAddressModel();
        receivingAddressModel.setAddress("广东省惠州市惠州区18号仓库");
        receivingAddressModel.setMobile("17824428781188");
        receivingAddressModel.setName("张珊www");
        detailVO.setDomesticAddress(receivingAddressModel);
        detailVO.setOverseasAddress(receivingAddressModel);
        SellerVO sellerVO = new SellerVO();
        sellerVO.setChannelName("AE中东大店");
        sellerVO.setCompanyCode("Singapore E-commerce Private Limited");
        sellerVO.setCompanyName("AE中东大店");
        detailVO.setSeller(sellerVO);
        detailVO.setPurchaseOrderId("7477779");
        detailVO.setStatusName("整单取消");

        List<PurchaseSubOrderLogisticsVO> logisticsList = new ArrayList<>();
        PurchaseSubOrderLogisticsVO purchaseSubOrderLogisticsVO = new PurchaseSubOrderLogisticsVO();
        purchaseSubOrderLogisticsVO.setCompanyName("qqq");
        purchaseSubOrderLogisticsVO.setId("777989144");
        purchaseSubOrderLogisticsVO.setLogisticsId("444");
        purchaseSubOrderLogisticsVO.setStatusName("待付款");
        purchaseSubOrderLogisticsVO.setSupplierId("777");
        List<PurchaseSubOrderVO> orderList = new ArrayList<>();
        PurchaseSubOrderVO purchaseSubOrderVO = new PurchaseSubOrderVO();
        purchaseSubOrderVO.setErrorMsg("不要了");
        purchaseSubOrderVO.setErrorCode("wqw_we");
        purchaseSubOrderVO.setOrderSubId("8888wwwwww7777");
        purchaseSubOrderVO.setOfferTitle("www");
        purchaseSubOrderVO.setPrice(new Double(77.6));
        purchaseSubOrderVO.setQuantity(1);
        purchaseSubOrderVO.setProductId("888888");
        purchaseSubOrderVO.setRefundStatus("NOT_REFUND");
        purchaseSubOrderVO.setRefundDesc("未退款");
        purchaseSubOrderVO.setReceiveAmount(5);
        purchaseSubOrderVO.setSpec("qq");
        purchaseSubOrderVO.setSupplierProductId("888ww");
        purchaseSubOrderVO.setTotal(new Double(6));
        orderList.add(purchaseSubOrderVO);
        purchaseSubOrderLogisticsVO.setOrderList(orderList);
        logisticsList.add(purchaseSubOrderLogisticsVO);
        detailVO.setLogisticsList(logisticsList);

        return detailVO;
    }*/


    private PurchaseOrderVO convertToPurchaseOrder(FsPurchaseOrderModel purchaseOrder, Map<Long, Long> purchasePerformMap, Map<Long, FsPerformSubOrderModel> performOrderMap,Map<Long, FsPerformOrderModel> performMainOrderMap) {
        final PurchaseOrderVO vo = new PurchaseOrderVO();
        if (purchaseOrder == null) {
            return vo;
        }
        vo.setCreateTime(purchaseOrder.getGmtCreate().getTime());
        vo.setId(purchaseOrder.getId());
        BigDecimal quantity = new BigDecimal(0);
        BigDecimal receiveQuantity = new BigDecimal(0);
        BigDecimal purchasePrice = new BigDecimal(0);
        final List<Long> channelOrderIds = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList())) {
            for (FsPurchaseSubOrderModel subPurchaseOrder : purchaseOrder.getSubOrderList()) {
                if (!CollectionUtils.isEmpty(subPurchaseOrder.getChannelOrderInfoList())) {
                    subPurchaseOrder.getChannelOrderInfoList().stream().forEach(channelOrder -> {
                        if (channelOrder.getDbMainId() != null && !channelOrderIds.contains(channelOrder.getDbMainId())) {
                            channelOrderIds.add(channelOrder.getDbMainId());
                        }
                    });
                } else if (subPurchaseOrder.getChannelOrderId() != null) {
                    if (!channelOrderIds.contains(subPurchaseOrder.getChannelDbId())) {
                        channelOrderIds.add(subPurchaseOrder.getChannelDbId());
                    }
                }

                if (purchasePerformMap.containsKey(subPurchaseOrder.getId()) && performOrderMap.containsKey(purchasePerformMap.get(subPurchaseOrder.getId()))) {
                    Long performId = purchasePerformMap.get(subPurchaseOrder.getId());
                    FsPerformSubOrderModel performSubOrderModel = performOrderMap.get(performId);
                    if (performSubOrderModel != null) {
                        quantity = quantity.add(performSubOrderModel.getBuyAmount());
                        purchasePrice = purchasePrice.add(performSubOrderModel.getSubOrderPrice());
                        if (FsPerformStatusEnum.COMPLETE.getValue().equals(performSubOrderModel.getSubStatus()) || FsPerformStatusEnum.RECEIVE.getValue().equals(performSubOrderModel.getSubStatus())) {
                            receiveQuantity = receiveQuantity.add(performSubOrderModel.getBuyAmount());
                        }
                    }
                }
            }
        }


        vo.setQuantity(quantity.intValue());
        vo.setReceiveQuantity(receiveQuantity.intValue());
        String saleOrderId = String.join(",", channelOrderIds.stream().map(id -> String.valueOf(id)).collect(Collectors.toList()));
        vo.setSaleOrderId(saleOrderId);
        if (!StringUtils.isEmpty(purchaseOrder.getBizStatus())) {
            FsPurchaseBizStatus status = FsPurchaseBizStatus.valueOf(purchaseOrder.getBizStatus());
            if (status != null) {
                vo.setStatus(status.getValue());
                vo.setStatusName(status.getDesc());
            }
        }
        vo.setSupplierName(purchaseOrder.getSupplierCompanyName());
        vo.setTotalPay(String.valueOf(purchasePrice.doubleValue()));
        vo.setUserId(purchaseOrder.getUserId());
        vo.setChannelId(purchaseOrder.getBizType());
        if(!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList()) && purchaseOrder.getSubOrderList().size()!=0){
            FsPurchaseSubOrderModel fsPurchaseSubOrderModel = purchaseOrder.getSubOrderList().get(0);
            if(fsPurchaseSubOrderModel.getPerformId()!=null){
                FsPerformOrderModel fsPerformOrderModel = performMainOrderMap.get(fsPurchaseSubOrderModel.getPerformId());
                if(fsPerformOrderModel!=null){
                    vo.setCbuTradeId(fsPerformOrderModel.getOrderId());
                }
            }
        }

        return vo;
    }

    private String makeSpec(List<FsAttrModel> attrList){
        if(CollectionUtils.isEmpty(attrList)){
            return null;
        }
        List<String> resultList = Lists.newArrayList();
        for(FsAttrModel attrModel:attrList){
            if(StringUtils.isEmpty(attrModel.getValueText())){
                continue;
            }
            resultList.add(attrModel.getValueText());
        }
        if(CollectionUtils.isEmpty(resultList)){
            return null;
        }
        return StringUtils.join(resultList," ");
    }


    private PurchaseOrderDetailVO convertToPurchaseOrderDetail(FsPurchaseOrderModel purchaseOrder, Map<Long, Long> purchasePerformMap, Map<Long, FsPerformSubOrderModel> performOrderMap) {
        if (purchaseOrder == null) {
            return null;
        }
        PurchaseOrderDetailVO vo = new PurchaseOrderDetailVO();
        vo.setChannelName(ChannelMarketEnum.obtainEnumByOldCode(purchaseOrder.getBizType()).getDesc());
        vo.setCreateTime(purchaseOrder.getGmtCreate().getTime());
        vo.setPurchaseOrderId(purchaseOrder.getId().toString());
        if (!StringUtils.isEmpty(purchaseOrder.getBizStatus())) {
            FsPurchaseBizStatus status = FsPurchaseBizStatus.valueOf(purchaseOrder.getBizStatus());
            if (status != null) {
                vo.setStatusName(status.getDesc());
            }
        }

        //提取主单信息，子单如果没有错误信息，用主单代替
        FsPurchaseExtra purchaseExtra = purchaseOrder.getFsPurchaseExtra();
        String mainErrorCode = purchaseExtra == null ? null : purchaseExtra.getMakeOrderErrorCode();
        String mainErrorMsg = purchaseExtra == null ? null : purchaseExtra.getMakeOrderErrorMsg();

        if (!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList())) {
            List<PurchaseSubOrderLogisticsVO> logisticsList = Lists.newLinkedList();
//            vo.setLogisticsList(logisticsList);
            for (FsPurchaseSubOrderModel subPurchaseOrder : purchaseOrder.getSubOrderList()) {
                PurchaseSubOrderLogisticsVO subOrderLogisticsVo = new PurchaseSubOrderLogisticsVO();
                logisticsList.add(subOrderLogisticsVo);
                PurchaseSubOrderVO subOrderVo = new PurchaseSubOrderVO();
                subOrderLogisticsVo.setOrderList(Lists.newArrayList(subOrderVo));
                subOrderLogisticsVo.setSupplierId(purchaseOrder.getSupplierId());
                subOrderLogisticsVo.setId("");
                if (subPurchaseOrder.getProductId() != null && subPurchaseOrder.getProductId() != 0) {
                    subOrderVo.setProductId(String.valueOf(subPurchaseOrder.getProductSubId()));
                }
                subOrderVo.setSpec(makeSpec(subPurchaseOrder.getProductAttr()));

                subOrderVo.setSupplierProductId(subPurchaseOrder.getPerformGoodId());
                // 采购子单拓展信息
                if (subPurchaseOrder.getPurchaseSubExtra() != null && subPurchaseOrder.getPurchaseSubExtra().getErrorCode() != null) {
                    subOrderVo.setErrorCode(subPurchaseOrder.getPurchaseSubExtra().getErrorCode());
                    subOrderVo.setErrorMsg(subPurchaseOrder.getPurchaseSubExtra().getErrorMsg());
                }
                else {
                    //子采购单上没有错误信息，用主采购单代替
                    subOrderVo.setErrorCode(mainErrorCode);
                    subOrderVo.setErrorMsg(mainErrorMsg);
                }

                // 子订单的退款相关状态
                subOrderVo.setRefundStatus(subPurchaseOrder.getRefundStatus());
                // 子订单的退款相关文案
                if (FsPurchaseRefundStatusEnum.safeValueOf(subPurchaseOrder.getRefundStatus()) != null) {
                    subOrderVo.setRefundDesc(FsPurchaseRefundStatusEnum.safeValueOf(subPurchaseOrder.getRefundStatus()).getDesc());
                }
                BigDecimal quantity = new BigDecimal(0);
                BigDecimal receiveQuantity = new BigDecimal(0);
                BigDecimal purchasePrice = new BigDecimal(0);
                if (purchasePerformMap.containsKey(subPurchaseOrder.getId()) && performOrderMap.containsKey(purchasePerformMap.get(subPurchaseOrder.getId()))) {
                    Long performId = purchasePerformMap.get(subPurchaseOrder.getId());
                    FsPerformSubOrderModel performSubOrderModel = performOrderMap.get(performId);
                    if (performSubOrderModel != null) {
                        subOrderVo.setOfferTitle(performSubOrderModel.getGoodTitle());
                        subOrderVo.setPrice(performSubOrderModel.getGoodPrice().doubleValue());
                        // 订单子ID
                        subOrderVo.setOrderSubId(performSubOrderModel.getOrderSubId());
                        quantity = quantity.add(performSubOrderModel.getBuyAmount());
                        purchasePrice = purchasePrice.add(performSubOrderModel.getSubOrderPrice());
                        if (FsPerformStatusEnum.COMPLETE.getValue().equals(performSubOrderModel.getSubStatus()) || FsPerformStatusEnum.RECEIVE.getValue().equals(performSubOrderModel.getSubStatus())) {
                            receiveQuantity = receiveQuantity.add(performSubOrderModel.getBuyAmount());
                        }
                        if (!StringUtils.isEmpty(performSubOrderModel.getSubStatus())) {
                            FsPerformStatusEnum status = FsPerformStatusEnum.valueOf(performSubOrderModel.getSubStatus());
                            if(status==FsPerformStatusEnum.CREATE){
                                subOrderLogisticsVo.setStatusName("待发货");
                            }else if (status != null) {
                                subOrderLogisticsVo.setStatusName(status.getDesc());
                            }
                        }
                        FsLogisticsModel logisticsInfo = performSubOrderModel.getLogisticsInfo();
                        if (logisticsInfo != null) {
                            subOrderLogisticsVo.setId(logisticsInfo.getLogisticsId());
                            subOrderLogisticsVo.setCompanyName(logisticsInfo.getCompany());
                            subOrderLogisticsVo.setLogisticsId(logisticsInfo.getCompanyLogisticsId());
                        }
                    }
                }
                if (StringUtils.isEmpty(subOrderVo.getOfferTitle())) {
                    OfferModel offerModel = offerQueryService.findAllWithoutDetail(Long.parseLong(subPurchaseOrder.getPerformGoodId()), null);
                    if (offerModel != null) {
                        subOrderVo.setOfferTitle(offerModel.getOffer().getSubject());
                    }
                }
                subOrderVo.setQuantity(quantity.intValue());
                subOrderVo.setReceiveAmount(receiveQuantity.intValue());
                subOrderVo.setTotal(purchasePrice.doubleValue());
            }
            Map<String, List<PurchaseSubOrderLogisticsVO>> groupLogisticsMap = logisticsList.stream().collect(Collectors.groupingBy(PurchaseSubOrderLogisticsVO::getId));
            List<PurchaseSubOrderLogisticsVO> subOrderList = Lists.newArrayList();
            for (String logisticsId : groupLogisticsMap.keySet()) {
                List<PurchaseSubOrderLogisticsVO> subOrders = groupLogisticsMap.get(logisticsId);
                if (StringUtils.isEmpty(logisticsId)) {
                    subOrderList.addAll(subOrders);
                } else {
                    PurchaseSubOrderLogisticsVO groupVo = new PurchaseSubOrderLogisticsVO();
                    List<PurchaseSubOrderVO> subSaleOrderList = new ArrayList<PurchaseSubOrderVO>();
                    groupVo.setId(subOrders.get(0).getId());
                    groupVo.setCompanyName(subOrders.get(0).getCompanyName());
                    groupVo.setStatusName(subOrders.get(0).getStatusName());
                    groupVo.setLogisticsId(subOrders.get(0).getLogisticsId());
                    groupVo.setSupplierId(subOrders.get(0).getSupplierId());
                    subOrders.stream().forEach(suborder -> {
                        subSaleOrderList.add(suborder.getOrderList().get(0));
                    });
                    groupVo.setOrderList(subSaleOrderList);
                    subOrderList.add(groupVo);
                }
            }
            vo.setLogisticsList(subOrderList);
        }
        return vo;
    }

    private String getPurchaseSellerId(String bizType) {
        if (StringUtils.isEmpty(bizType)) {
            return "";
        }

        Map<String, String> purchaseUserIdMap = fsTradeInfoService.listPurchaseUserIdInfo();

        return purchaseUserIdMap == null ? "" : purchaseUserIdMap.get(bizType);
    }

    /**
     * 取消采购单
     *
     * @param request
     * @param purchaseId 采购单id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/cancelPurchase", method = RequestMethod.GET)
    public ResultOf<Boolean> cancelPurchase(HttpServletRequest request, @RequestParam(name = "purchaseId") Long purchaseId,
                                            @RequestParam(name = "userId") String userId,
                                            @RequestParam(name = "channelId") String channelId) {

        String logPrefix = "PurchaseOrderController#cancelPurchase#";
        String paramInfo = "#purchaseId:" + purchaseId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ChannelMarketEnum channelMarketEnum = null;
        if(StringUtils.isBlank(channelId)) {
            channelMarketEnum = ChannelMarketEnum.AE;
        }
        else {
            channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);
        }


        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取工号
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isBlank(empId)) {
                result.setErrorCode("EmpId_IsNull");
                result.setErrorMessage("操作人的工号不能空");
                return result;
            }
            // 采购单买家userId
            Long userIdLong = Long.valueOf(getPurchaseSellerId(channelMarketEnum.getOldMarketCode()));
            ResultOf<Boolean> cancelResult = fsPurchaseOperateService.cancelPurchase(userIdLong, purchaseId);
            if (cancelResult != null && cancelResult.isSuccess()) {
                result.setSuccess(true);
            }

        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    /**
     * 采购下单
     *
     * @param request
     * @param purchaseId 采购单id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/makeCbuOrderAndPayByPurchase", method = RequestMethod.GET)
    public ResultOf<Boolean> makeCbuOrderAndPayByPurchase(HttpServletRequest request, @RequestParam(name = "purchaseId") Long purchaseId,
                                                          @RequestParam(name = "userId" ,required = false) String userId,
                                                          @RequestParam(name = "channelId",required = false) String channelId) {

        String logPrefix = "PurchaseOrderController#makeCbuOrderAndPayByPurchase#";
        String paramInfo = "#purchaseId:" + purchaseId + "#";

        log.error(logPrefix + "record" + paramInfo);
        ChannelMarketEnum channelMarketEnum = null;
        if(StringUtils.isBlank(channelId)) {
            channelMarketEnum = ChannelMarketEnum.AE;
        }
        else {
            channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);
        }
        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取工号
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isBlank(empId)) {
                result.setErrorCode("EmpId_IsNull");
                result.setErrorMessage("操作人的工号不能空");
                return result;
            }
            // 采购单买家userId
            Long userIdLong = Long.valueOf(getPurchaseSellerId(channelMarketEnum.getOldMarketCode()));
            ResultOf<Boolean> makeCbuOrderResult = fsPurchaseOperateService.makeCbuOrderAndPayByPurchase(userIdLong, purchaseId);
            if (makeCbuOrderResult != null && makeCbuOrderResult.isSuccess()) {
                result.setSuccess(true);
            }

        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }
    /**
     * 采购单支付
     *
     * @param request
     * @param purchaseId 采购单id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/payOrder", method = RequestMethod.GET)
    public ResultOf<Boolean> payOrder(HttpServletRequest request, @RequestParam(name = "purchaseId") Long purchaseId,
                                      @RequestParam(name = "userId",required = false) String userId,
                                      @RequestParam(name = "channelId",required = false) String channelId) {

        String logPrefix = "PurchaseOrderController#payOrder#";
        String paramInfo = "#purchaseId:" + purchaseId + "#";
        ChannelMarketEnum channelMarketEnum = null;
        if(StringUtils.isBlank(channelId)) {
            channelMarketEnum = ChannelMarketEnum.AE;
        }
        else {
            channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);
        }
        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取工号
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isBlank(empId)) {
                result.setErrorCode("EmpId_IsNull");
                result.setErrorMessage("操作人的工号不能空");
                return result;
            }

            PurchaseOrderPayParam purchaseOrderPayParam = new PurchaseOrderPayParam();
            // 采购单买家userId
            String userIdCal = getPurchaseSellerId(channelMarketEnum.getOldMarketCode());
            // 采购单买家userId
            purchaseOrderPayParam.setUserId(userIdCal);
            // 采购单id
            purchaseOrderPayParam.setPurchaseId(purchaseId);
            ResultOf<Boolean> payOrderResult = fsPurchaseOperateService.payOrder(purchaseOrderPayParam);
            if (payOrderResult != null && payOrderResult.isSuccess()) {
                result.setSuccess(true);
            }

        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }
    /**
     * 采购单批量触发支付，指定买家所有待支付订单
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/batchPayOrder", method = RequestMethod.GET)
    public ResultOf<Boolean> batchPayOrder(HttpServletRequest request) {

        String logPrefix = "PurchaseOrderController#batchPayOrder#";

        log.error(logPrefix + "record");

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取工号
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isBlank(empId)) {
                result.setErrorCode("EmpId_IsNull");
                result.setErrorMessage("操作人的工号不能空");
                return result;
            }

            PurchaseOrderBatchPayParam param = new PurchaseOrderBatchPayParam();
            // 采购单买家userId
            String userId = getPurchaseSellerId(ChannelMarketEnum.AE.getOldMarketCode());
            param.setUserId(userId);
            // 业务类型
            param.setBizType(ChannelMarketEnum.AE.getOldMarketCode());
            ResultOf<Boolean> batchPayOrderResult = fsPurchaseOperateService.batchPayOrder(param);
            if (batchPayOrderResult != null && batchPayOrderResult.isSuccess()) {
                result.setSuccess(true);
            }

        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue(), e);
        }
        return result;
    }

}
