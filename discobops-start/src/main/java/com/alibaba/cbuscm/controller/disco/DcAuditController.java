package com.alibaba.cbuscm.controller.disco;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.constant.ErrorCodeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcAuditService;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditInfoTypeEnum;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditTypeEnum;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcAuditInfoRecord;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbu.disco.shared.core.enroll.model.view.DcOfferItemMatchSnapshotVO;
import com.alibaba.cbuscm.logic.DiscoLogic;
import com.alibaba.cbuscm.vo.disco.DcAuditInfoRecordVO;
import com.alibaba.cbuscm.vo.disco.DcAuditTypeVO;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.up.common.mybatis.result.RPCResult;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * DcAuditController
 *
 * @author aniu.nxh
 * @date 2019/07/10 16:27
 */
@RestController
@RequestMapping(value = "/dcAudit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class DcAuditController {

    private static final String TB = "淘宝规格：";
    private static final String CBU = "1688规格：";
    private static final String SUPPLY_PRICE = "供货价：";
    private static final String QUANTITY = "库存：";

    @Autowired
    private DcAuditService dcAuditService;
    @Autowired
    private DiscoLogic discoLogic;

    @ResponseBody
    @RequestMapping(value = "/firstEnroll", method = RequestMethod.GET)
    public ResultOf<Boolean> firstEnroll(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                            @RequestParam(name = "enrollId") Long enrollId,
                                            @RequestParam(name = "offerId") Long offerId,
                                            @RequestParam(name = "itemId") Long itemId,
                                            @RequestParam(name = "enrollStatus") boolean enrollStatus,
                                            @RequestParam(name = "reasonType", required = false) String reasonType,
                                            @RequestParam(name = "msg") String msg) {

        String logPrefix = "DcAuditController#firstEnroll#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#oid:" + offerId + "#itemId:" + itemId
            + "#enrollStatus:" + enrollStatus + "#msg:" + msg;

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            //如果同意 清空 拒绝类型
            if(enrollStatus){
                reasonType = null;
            }
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 小二一审处理
                result = dcAuditService.firstEnroll(channel, enrollId, offerId, itemId, enrollStatus,
                    reasonType, msg, resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/secondEnroll", method = RequestMethod.GET)
    public ResultOf<Boolean> secondEnroll(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                            @RequestParam(name = "enrollId") Long enrollId,
                                            @RequestParam(name = "offerId") Long offerId,
                                            @RequestParam(name = "itemId") Long itemId,
                                            @RequestParam(name = "enrollStatus") boolean enrollStatus,
                                            @RequestParam(name = "reasonType", required = false) String reasonType,
                                            @RequestParam(name = "msg") String msg) {

        String logPrefix = "DcAuditController#secondEnroll#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#oid:" + offerId + "#itemId:" + itemId
            + "#enrollStatus:" + enrollStatus + "#msg:" + msg;

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            //如果同意 清空 拒绝类型
            if(enrollStatus){
                reasonType = null;
            }
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 小二二审处理
                result = dcAuditService.secondEnroll(channel, enrollId, offerId, itemId, enrollStatus,
                    reasonType, msg, resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/deleteEnroll", method = RequestMethod.GET)
    public ResultOf<Boolean> deleteEnroll(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                            @RequestParam(name = "enrollId") Long enrollId,
                                            @RequestParam(name = "offerId") Long offerId,
                                            @RequestParam(name = "itemId") Long itemId) {

        String logPrefix = "DcAuditController#deleteEnroll#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#oid:" + offerId + "#itemId:" + itemId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 小二删除处理
                result = dcAuditService.deleteEnroll(channel, null, enrollId, offerId, itemId, resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @RequestMapping("/status")
    public RPCResult getAuditTypeStatus() {
        DcAuditTypeEnum[] states = DcAuditTypeEnum.values();
        List<DcAuditTypeVO> result = new ArrayList<>();
        Arrays.asList(states).stream().forEach(state -> {
            DcAuditTypeVO vo = new DcAuditTypeVO();
            vo.setName(state.getDesc());
            vo.setValue(state.getValue());
            result.add(vo);
        });
        return RPCResult.ok(result);
    }

    @ResponseBody
    @RequestMapping(value = "/rejectEnroll", method = RequestMethod.GET)
    public ResultOf<Boolean> rejectEnroll(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                            @RequestParam(name = "enrollId") Long enrollId,
                                            @RequestParam(name = "itemId") Long itemId,
                                            @RequestParam(name = "reasonType", required = false) String reasonType,
                                            @RequestParam(name = "msg") String msg) {

        String logPrefix = "DcAuditController#rejectEnroll#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#itemId:" + itemId + "#msg:" + msg;

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 小二拒绝处理
                result = dcAuditService.rejectEnroll(channel, enrollId, itemId, reasonType, msg, resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/resetWaitEnroll", method = RequestMethod.GET)
    public ResultOf<Boolean> resetWaitEnroll(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                                @RequestParam(name = "enrollId") Long enrollId,
                                                @RequestParam(name = "offerId") Long offerId,
                                                @RequestParam(name = "itemId") Long itemId,
                                                @RequestParam(name = "msg") String msg) {

        String logPrefix = "DcAuditController#resetWaitEnroll#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#oid:" + offerId + "#itemId:" + itemId + "#msg:"
            + msg + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 小二重置状态待审核
                result = dcAuditService.resetWaitEnroll(channel, enrollId, offerId, itemId, msg, resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/stockManageEnroll", method = RequestMethod.GET)
    public ResultOf<Boolean> stockManageEnroll(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                                @RequestParam(name = "enrollId") Long enrollId,
                                                @RequestParam(name = "itemId") Long itemId,
                                                @RequestParam(name = "stockAuditStatus") boolean stockAuditStatus,
                                                @RequestParam(name = "msg") String msg) {

        String logPrefix = "DcAuditController#stockManageEnroll#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#itemId:" + itemId + "#stockAuditStatus:" +
            stockAuditStatus + "#msg:" + msg + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 同意/驳回库存提报
                result = dcAuditService.stockManageEnroll(channel, enrollId, itemId, stockAuditStatus, msg,
                    resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/listAuditInfo4Stock", method = RequestMethod.GET)
    public ListOf<DcAuditInfoRecord> listAuditInfo4Stock(HttpServletRequest request,
                                                            @RequestParam(name = "channel") String channel,
                                                            @RequestParam(name = "enrollId") Long enrollId) {

        String logPrefix = "DcAuditController#listAuditInfo4Stock#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ListOf<DcAuditInfoRecord> result = new ListOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 获取库存提报记录
                result = dcAuditService.listAuditInfo4Stock(channel, enrollId, null, true,
                    resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/listAuditInfo", method = RequestMethod.GET)
    public ListOf<DcAuditInfoRecordVO> listAuditInfo(HttpServletRequest request,
                                                    @RequestParam(name = "supplierUserId") Long supplierUserId,
                                                    @RequestParam(name = "enrollId") Long enrollId) {

        String logPrefix = "DcAuditController#listAuditInfo#";
        String paramInfo = "#enrollId:" + enrollId + "#sid:" + supplierUserId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ListOf<DcAuditInfoRecordVO> result = new ListOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 罗列展示审核信息列表
                ListOf<DcAuditInfoRecord> recordListOf = dcAuditService.listAuditInfo(supplierUserId,
                    enrollId, true);
                if (recordListOf.isSuccess() && recordListOf.getData() != null) {
                    result.setSuccess(true);
                    result.setData(getDcAuditInfoRecordVO(recordListOf.getData()));
                }
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    /**
     * 获取审核信息
     *
     * @param dcAuditInfoRecordList
     * @return
     */
    private List<DcAuditInfoRecordVO> getDcAuditInfoRecordVO(List<DcAuditInfoRecord> dcAuditInfoRecordList) {
        // 审核信息
        List<DcAuditInfoRecordVO> recordVoList = new ArrayList<>();
        for (DcAuditInfoRecord dcAuditInfoRecord: dcAuditInfoRecordList) {
            if (dcAuditInfoRecord == null) {
                continue;
            }
            DcAuditInfoRecordVO dcAuditInfoRecordVO = new DcAuditInfoRecordVO();
            // 审核类型
            dcAuditInfoRecordVO.setAuditType(dcAuditInfoRecord.getAuditType());
            // 审核文案
            dcAuditInfoRecordVO.setAuditTypeText(dcAuditInfoRecord.getAuditTypeText());
            // 报名主键id
            dcAuditInfoRecordVO.setEnrollId(dcAuditInfoRecord.getEnrollId());
            // 创建时间
            dcAuditInfoRecordVO.setGmtCreate(dcAuditInfoRecord.getGmtCreate());
            // 操作人
            dcAuditInfoRecordVO.setOperator(dcAuditInfoRecord.getOperator());
            // 操作人姓名
            dcAuditInfoRecordVO.setOperatorName(dcAuditInfoRecord.getOperatorName());
            // 原因
            //String reason = dcAuditInfoRecord.getReason();
            // 审核不通过原因类型
            //String reasonType = null;
            //if (DcAuditTypeEnum.safeValueOf(dcAuditInfoRecord.getReasonType()) != null) {
            //    reasonType = DcAuditTypeEnum.safeValueOf(dcAuditInfoRecord.getReasonType()).getDesc();
            //}
            //if (StringUtils.isNotBlank(reason) && StringUtils.isNotBlank(reasonType)) {
            //    reason = reasonType + "(" + reason + ")";
            //} else if (StringUtils.isBlank(reason) && StringUtils.isNotBlank(reasonType)) {
            //    reason = reasonType;
            //}
            dcAuditInfoRecordVO.setReason(dcAuditInfoRecord.getReason());
            // 快照
            if(!DcAuditInfoTypeEnum.COMMISSION_UPDATE.getValue().equals(dcAuditInfoRecord.getAuditType())) {
                List<DcOfferItemMatchSnapshotVO> snapshotVoList = null;
                if (dcAuditInfoRecord.getSnapshot() != null) {
                    snapshotVoList = JSON
                            .parseObject(dcAuditInfoRecord.getSnapshot(), new TypeReference<List<DcOfferItemMatchSnapshotVO>>() {
                            });
                }
                dcAuditInfoRecordVO.setSnapshot(getSnapshot(snapshotVoList));
            }
            //更新commission需要特殊处理
            else{
                JSONObject object = JSONObject.parseObject(dcAuditInfoRecord.getSnapshot());
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(object);
            }
            recordVoList.add(dcAuditInfoRecordVO);
        }
        return recordVoList;
    }

    /**
     * 获取快照
     *
     * @param snapshotVoList
     * @return
     */
    private JSONArray getSnapshot(List<DcOfferItemMatchSnapshotVO> snapshotVoList) {
        JSONArray jsonArray = new JSONArray();

        if (snapshotVoList == null || snapshotVoList.size() <= 0) {
            return jsonArray;
        }

        for (DcOfferItemMatchSnapshotVO dcOfferItemMatchSnapshotVO: snapshotVoList) {

            if (dcOfferItemMatchSnapshotVO == null) {
                continue;
            }

            StringBuilder snapshot = new StringBuilder();
            snapshot.append(CBU + dcOfferItemMatchSnapshotVO.getCbuSkuAttr() + "  ");
            snapshot.append(TB + dcOfferItemMatchSnapshotVO.getTbSkuAttr() + "  ");
            snapshot.append(SUPPLY_PRICE + dcOfferItemMatchSnapshotVO.getDcSupplyPrice() + "  ");
            snapshot.append(QUANTITY + dcOfferItemMatchSnapshotVO.getQuantity());

            jsonArray.add(snapshot.toString());
        }
        return jsonArray;
    }

    @ResponseBody
    @RequestMapping(value = "/editItemConfirm", method = RequestMethod.GET)
    public ResultOf<Boolean> editItemConfirm(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                                @RequestParam(name = "enrollId") Long enrollId,
                                                @RequestParam(name = "offerId") Long offerId,
                                                @RequestParam(name = "itemId") Long itemId,
                                                @RequestParam(name = "confirmStatus") boolean confirmStatus,
                                                @RequestParam(name = "msg") String msg) {

        String logPrefix = "DcAuditController#editItemConfirm#";
        String paramInfo = "#c:" + channel + "#eid:" + enrollId + "#oid:" + offerId + "#itemId:" + itemId
            + "#confirmStatus:" + confirmStatus + "#msg:" + msg;

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 运营对二审通过后的编辑进行确认
                result = dcAuditService.editItemConfirm(channel, enrollId, offerId, itemId, confirmStatus,
                    msg, resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

}
