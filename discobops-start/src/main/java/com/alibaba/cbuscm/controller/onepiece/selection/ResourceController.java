package com.alibaba.cbuscm.controller.onepiece.selection;

import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.enums.onepiece.OnePieceTaskTypeEnum;
import com.alibaba.china.global.business.library.enums.onepiece.SelectionChannelEnum;
import com.alibaba.china.global.business.library.enums.onepiece.UnitSelectStatueEnum;
import com.alibaba.china.global.business.library.interfaces.onepiece.resource.ResourceService;
import com.alibaba.china.global.business.library.interfaces.onepiece.selectioncenter.SelectionUnitService;
import com.alibaba.china.global.business.library.models.onepiece.resource.field.*;
import com.alibaba.china.global.business.library.models.onepiece.selectioncenter.SelectionUnitModel;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.FieldsSaveParam;
import com.alibaba.china.global.business.library.params.onepiece.selectioncenter.SelectionUnitQueryParam;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.alibaba.china.global.business.library.enums.onepiece.SelectionChannelEnum.*;

/**
 * one piece资源接口
 *
 * @author lichao.wlc
 * @date 2019/06/12
 */
@RestController
@RequestMapping("/one-piece/selection/resource")
@Slf4j
public class ResourceController {
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private SelectionUnitService selectionUnitService;

    /**
     * 任务下游指标结果
     *
     * @param taskId 任务id
     * @return
     */
    @RequestMapping("/result/downstream")
    public RPCResult downstreamFieldResult(@RequestParam Long taskId, HttpServletRequest request) {
        try {
            ResultOf<List<FieldsResultVO>> resultOf = resourceService.getFieldsResult(DOWNSTREAM, taskId,
                request.getHeader("lang"));
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
        } catch (Exception e) {
            log.error("downstreamFieldResult error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "downstreamFieldResult error");
    }

    /**
     * 任务上游指标结果
     *
     * @param taskId 任务id
     * @return
     */
    @RequestMapping("/result/upstream")
    public RPCResult upstreamFieldResult(@RequestParam Long taskId, HttpServletRequest request) {
        try {
            return getRpcResult(taskId, request.getHeader("lang"), UPSTREAM);
        } catch (Exception e) {
            log.error("upstreamFieldResult error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "upstreamFieldResult error");
    }

    /**
     * 任务利润指标结果
     *
     * @param taskId 任务id
     * @return
     */
    @RequestMapping("/result/profit")
    public RPCResult profitFieldResult(@RequestParam Long taskId, HttpServletRequest request) {
        try {
            return getRpcResult(taskId, request.getHeader("lang"), PROFIT_ANALYSIS);
        } catch (Exception e) {
            log.error("profitFieldResult error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "profitFieldResult error");
    }

    private RPCResult getRpcResult(Long taskId,
                                   String language, SelectionChannelEnum channelEnum) {
        ResultOf<List<FieldsResultVO>> resultOf = resourceService.getFieldsResult(channelEnum, taskId, language);
        if (resultOf.isSuccess()) {
            FieldsResultVO fieldsResultVO = CollectionUtils.isEmpty(resultOf.getData()) ? null : resultOf.getData().get(
                0);
            return RPCResult.ok(fieldsResultVO);
        }
        return null;
    }

    /**
     * 拿到单个指标组
     *
     * @param selectionUnitId 指标保存对象id
     * @param pageIdentifier  指标渠道来源（上游、下游、利润分析）
     * @param type            任务类型
     * @return
     */
    @RequestMapping("/conditions")
    public RPCResult getFieldConditions(@RequestParam Long selectionUnitId,
                                        @RequestParam String pageIdentifier,
                                        @RequestParam String type,
                                        HttpServletRequest request) {
        try {
            List<GroupFieldsComponent> groupFieldsComponents = Lists.newArrayList();
            //拿到筛选条件定义集合
            ResultOf<List<GroupFieldsComponent>> resultOf = resourceService.getGroupFieldsComponent(
                OnePieceTaskTypeEnum.valueOf(type), SelectionChannelEnum.valueOf(pageIdentifier),
                request.getHeader("lang"));
            if (resultOf.isSuccess()) {
                groupFieldsComponents = resultOf.getData();
            }

            if (!ObjectUtils.isEmpty(selectionUnitId)) {
                //如果是编辑查看，则需要回填已保存信息
                ResultOf<List<SelectionUnitModel>> selectionUnitModelResultOf = selectionUnitService.query(
                    SelectionUnitQueryParam.builder().id(selectionUnitId).build());
                if (selectionUnitModelResultOf.isSuccess() && selectionUnitModelResultOf.getData().size() == 1) {
                    SelectionUnitModel selectionUnitModel = selectionUnitModelResultOf.getData().get(0);
                    String indexGroupShow = selectionUnitModel.getIndexGroupShow();
                    Map<String, List<FieldResource>> savedConditions = JSONArray.parseArray(indexGroupShow,
                        FieldResource.class)
                        .stream()
                        .collect(Collectors.groupingBy(FieldResource::getResourceKey));

                    for (GroupFieldsComponent groupFieldsComponent : groupFieldsComponents) {
                        List<FieldResource> fieldResources = groupFieldsComponent.getFields();
                        if(CollectionUtils.isEmpty(fieldResources)) {
                            continue;
                        }

                        for (FieldResource fieldResource : fieldResources) {
                            String resourceKey = fieldResource.getResourceKey();
                            if (savedConditions.get(resourceKey) != null) {
                                fieldResource.setResourceValue(savedConditions.get(resourceKey).get(0)
                                    .getResourceValue());
                            }
                        }
                    }
                }
            }
            return RPCResult.ok(groupFieldsComponents);
        } catch (Exception e) {
            log.error("fieldConditions error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "fieldConditions error");
    }

    /**
     * 保存指标组
     *
     * @param fieldsSaveParam 保存参数模型
     * @return
     */
    @PostMapping("/conditions")
    public RPCResult saveFieldConditions(@RequestBody FieldsSaveParam fieldsSaveParam,
                                         HttpServletRequest httpServletRequest) {
        try {
            SelectionUnitModel selectionUnitModel = SelectionUnitModel.builder().selectionChannel(
                fieldsSaveParam.getConditionType())
                .indexGroupShow(JSONArray.toJSONString(fieldsSaveParam.getConditions()))
                .selectionRuler(JSONArray.toJSONString(fieldsSaveParam.getConditions()))
                .site(fieldsSaveParam.getSite())
                .siteName(fieldsSaveParam.getSite())
                .taskId(fieldsSaveParam.getTaskId())
                .id(fieldsSaveParam.getSelectionUnitId())
                .build();
            if (ObjectUtils.isEmpty(selectionUnitModel.getId())) {
                String errorMsg = preCheckSaveConditions(fieldsSaveParam, httpServletRequest.getHeader("lang"));
                if (StringUtils.hasText(errorMsg)) {
                    return RPCResult.error(RPCResult.MSG_ERROR_ALREADY_EXIST, errorMsg);
                }
                //新增保存
                log.info("save selectionUnitModel:{}", selectionUnitModel);
                ResultOf<SelectionUnitModel> resultOf = selectionUnitService.create(selectionUnitModel);
                if (resultOf.isSuccess()) {
                    log.info("fieldsSaveParam  success");
                    return RPCResult.ok(resultOf.getData().getId());
                } else {
                    log.warn("save failed, error code:{}, msg:{}", resultOf.getErrorCode(), resultOf.getErrorMessage());
                    return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
                }
            } else {
                //更新
                log.info("update selectionUnitModel:{}", selectionUnitModel);
                selectionUnitModel.setState(UnitSelectStatueEnum.INIT.name());
                ResultOf<Boolean> resultOf = selectionUnitService.update(selectionUnitModel);
                if (resultOf.isSuccess()) {
                    return RPCResult.ok();
                } else {
                    log.warn("update status:{}, error code:{}, msg:{}", resultOf.isSuccess(), resultOf.getErrorCode(),
                        resultOf.getErrorMessage());
                    return RPCResult.error(resultOf.getErrorCode(), resultOf.getErrorMessage());
                }
            }
        } catch (Exception e) {
            log.error("fieldConditions error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "fieldConditions error");
    }

    private String preCheckSaveConditions(FieldsSaveParam fieldsSaveParam, String language) {
        String conditionType = fieldsSaveParam.getConditionType();
        String errorMsg = "";
        if (DOWNSTREAM.name().equals(conditionType)) {
            //如果是下游，判断保存的指标站点，在task下是否已经保存
            SelectionUnitQueryParam queryParam = SelectionUnitQueryParam.builder()
                .taskId(fieldsSaveParam.getTaskId())
                .site(fieldsSaveParam.getSite())
                .selectionChannel(fieldsSaveParam.getConditionType())
                .build();
            ResultOf<List<SelectionUnitModel>> checkResultOf = selectionUnitService.query(queryParam);
            if (checkResultOf.isSuccess()) {
                if (checkResultOf.getData() != null) {
                    errorMsg = "zh".equals(language) ? "创建失败，该任务下已经存在相同站点：" + fieldsSaveParam.getSite()
                        : "Creation failed, the same site " + fieldsSaveParam.getSite()
                            + " already exists under the task";
                }
            } else {
                errorMsg = "zh".equals(language) ? "远程调用失败，请稍后重试" : "Remote call failed, please try again later";
            }
        } else if (UPSTREAM.name().equals(conditionType) || PROFIT_ANALYSIS.name().equals(conditionType)) {
            //在一个taskId下只能保存一个指标条件
            SelectionUnitQueryParam queryParam = SelectionUnitQueryParam.builder()
                .taskId(fieldsSaveParam.getTaskId())
                .selectionChannel(fieldsSaveParam.getConditionType())
                .build();
            ResultOf<List<SelectionUnitModel>> checkResultOf = selectionUnitService.query(queryParam);
            if (checkResultOf.isSuccess()) {
                if (checkResultOf.getData() != null) {
                    errorMsg = "zh".equals(language) ? "创建失败，该任务下已经存在筛选指标"
                        : "Creation failed, the filter indicator already exists under the task";
                }
            } else {
                errorMsg = "zh".equals(language) ? "远程调用失败，请稍后重试" : "Remote call failed, please try again later";
            }
        }
        return errorMsg;
    }

    /**
     * 拿到1688类目下商品属性
     *
     * @param selectionUnitId 指标组id
     * @param categoryId      类目id
     * @return
     */
    @RequestMapping("/category-properties")
    public RPCResult categoryProperties(@RequestParam Long selectionUnitId, @RequestParam Long categoryId,
                                        HttpServletRequest request) {
        try {
            ResultOf<List<FieldResource>> resultOf = resourceService.get1688CategoryProperties(categoryId,
                request.getHeader("lang"));
            if (resultOf.isSuccess()) {
                List<FieldResource> fieldResources = resultOf.getData();
                if (!ObjectUtils.isEmpty(selectionUnitId)) {
                    //回填已经保存的筛选条件值
                    ResultOf<List<SelectionUnitModel>> selectionUnitModelResultOf = selectionUnitService.query(
                        SelectionUnitQueryParam.builder().id(selectionUnitId).build());
                    if (selectionUnitModelResultOf.isSuccess() && selectionUnitModelResultOf.getData().size() == 1) {
                        String indexGroupShow = selectionUnitModelResultOf.getData().get(0).getIndexGroupShow();
                        Map<String, List<FieldResource>> savedConditions = JSONArray.parseArray(indexGroupShow,
                            FieldResource.class)
                            .stream()
                            .collect(Collectors.groupingBy(FieldResource::getResourceKey));

                        List<FieldResource> goodsPropertiesCategoryFieldList = savedConditions.get(
                            "selection.field.upstream.goodsProperties");
                        if (!CollectionUtils.isEmpty(goodsPropertiesCategoryFieldList)) {
                            FieldResource goodsPropertiesCategoryField = goodsPropertiesCategoryFieldList.get(0);
                            Long savedCategoryId = ((JSONObject)goodsPropertiesCategoryField.getResourceValue()
                                .getValue().get("0")).getLong("categoryId");
                            if (categoryId.equals(savedCategoryId)) {
                                for (FieldResource fieldResource : fieldResources) {
                                    List<FieldResource> propertiesFieldList = savedConditions.get(
                                        fieldResource.getResourceKey());
                                    if (!CollectionUtils.isEmpty(propertiesFieldList)) {
                                        FieldResource savedPropertiesField = propertiesFieldList.get(0);
                                        fieldResource.setResourceValue(savedPropertiesField.getResourceValue());
                                    }
                                }
                            }
                        }
                    }
                }
                return RPCResult.ok(fieldResources);
            }
        } catch (Exception e) {
            log.error("categoryProperties error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "categoryProperties error");
    }

    /**
     * 实时获得指标圈选数量
     *
     * @param fieldsSaveParam 保存参数模型
     * @return
     */
    @PostMapping("/offer-count")
    public RPCResult offerCount(@RequestBody FieldsSaveParam fieldsSaveParam) {
        try {
            ResultOf<Integer> resultOf = selectionUnitService.selectItemsCount(fieldsSaveParam.getConditions(),
                DOWNSTREAM.name().equals(fieldsSaveParam.getConditionType()) ? 1 : 2, fieldsSaveParam.getSite(),
                fieldsSaveParam.getTaskId());
            if (resultOf.isSuccess()) {
                return RPCResult.ok(resultOf.getData());
            }
            log.error("offer-count error msg:{}", resultOf.getErrorMessage());
        } catch (Exception e) {
            log.error("offerCount error", e);
        }
        return RPCResult.ok(0);
    }
}
