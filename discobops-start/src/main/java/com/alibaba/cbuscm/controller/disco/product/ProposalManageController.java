package com.alibaba.cbuscm.controller.disco.product;

import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.product.api.DcDistributeManageService;
import com.alibaba.cbu.disco.shared.core.product.constant.DcProductBizTypeEnum;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeProductParam;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalReadService;
import com.alibaba.cbu.disco.shared.core.proposal.api.DcProposalWriteService;
import com.alibaba.cbu.disco.shared.core.proposal.contant.OfferProposalFlowEnum;
import com.alibaba.cbu.disco.shared.core.proposal.model.DcProposalAuditLogModel;
import com.alibaba.cbu.disco.shared.core.proposal.model.DcProposalModel;
import com.alibaba.cbu.disco.shared.core.proposal.param.DcProposalAuditParam;
import com.alibaba.cbu.disco.shared.core.proposal.param.DcProposalQueryParam;
import com.alibaba.cbuscm.params.ProposalAuditParam;
import com.alibaba.cbuscm.params.ProposalDistributeParam;
import com.alibaba.cbuscm.service.proposal.OfferProposalExtendService;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.utils.ResultUtils;
import com.alibaba.up.common.mybatis.result.RPCResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/product/proposals", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class ProposalManageController {

    @Autowired
    private DcProposalReadService proposalReadService;

    @Autowired
    private DcProposalWriteService proposalWriteService;

    @Autowired
    private OfferProposalExtendService offerProposalExtendService;

    @Autowired
    private DcDistributeManageService dcDistributeManageService;
    /**
     * 提报统计信息
     */
    @RequestMapping(path = "/count", method = RequestMethod.GET)
    @ResponseBody
    public Object count() {
        try {
            return proposalReadService.countStatus(DcProposalQueryParam.builder()
                .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                .flow(OfferProposalFlowEnum.RETAIL.name())
                .build());
        }catch (Exception e) {
            log.error("CrossBorderProposalAdminController.count error!", e);
            return ResultOf.error("system-error", e.toString());
        }
    }

    /**
     * 商品提报列表
     */
    @RequestMapping(path = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(
        @RequestParam(value = "status", required = false) String status,
        @RequestParam(value = "cateId", required = false) Long cateId,
        @RequestParam(value = "cateLevel", required = false) Long cateLevel,
        @RequestParam(value = "offerId", required = false) Long offerId,
        @RequestParam(value = "offerTitle", required = false) String offerTitle,
        @RequestParam(value = "loginId", required = false) String loginId,
        @RequestParam(value = "company", required = false) String company,
        @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
        @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {

        try {
            if ("TOTAL".equalsIgnoreCase(status)) {
                status = null;
            }
            PageOf<DcProposalModel> pageResult = proposalReadService.listProposal(DcProposalQueryParam.builder()
                .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                .status(status)
                .cateId(cateId)
                .cateLevel(cateLevel)
                .offerId(offerId)
                .offerTitle(offerTitle)
                .loginId(loginId)
                .company(company)
                .flow(OfferProposalFlowEnum.RETAIL.name())
                .pageNo(pageNo)
                .pageSize(pageSize)
                .build());
            return ResultUtils.map(pageResult, offerProposalExtendService::convertOfferProposalVO);
        } catch (Throwable err) {
            log.error("CrossBorderProposalAdminController.list error!", err);
            return PageOf.error("system-error", err.getMessage());
        }
    }

    /**
     * 查询提报审核记录
     */
    @RequestMapping(path = "/audit-log", method = RequestMethod.GET)
    @ResponseBody
    public Object listAudits(@RequestParam(value = "proposalId") Long proposalId) {
        try {
            ListOf<DcProposalAuditLogModel> rpcResult = proposalReadService.listProposalAuditLog(proposalId);
            if (ListOf.isValid(rpcResult)) {
                return ListOf.general(rpcResult.getData().stream()
                    .map(OfferProposalExtendService::convertAuditLog)
                    .collect(Collectors.toList()));
            }else{
                return ListOf.empty();
            }
        } catch (Throwable t) {
            log.error("CrossBorderProposalAdminController.listAudits error!", t);
            return ListOf.error("system-error", t.toString());
        }
    }

    /**
     * 获取提报详情
     */
    @RequestMapping(path = "/detail", method = RequestMethod.GET)
    @ResponseBody
    public Object getDetail( @RequestParam(value = "proposalId") Long proposalId) {
        try {
            ResultOf<DcProposalModel> result = proposalReadService.queryProposalById(proposalId);
            if (!ResultOf.isValid(result)) {
                return ResultOf.error(RPCResult.MSG_ERROR_ILLEGAL, "无效提报id");
            }
            ListOf<DcProposalAuditLogModel> logs = proposalReadService.listProposalAuditLog(proposalId);
            return ResultOf.general(offerProposalExtendService.fillOfferProposalDetail(result.getData(),
                logs.getData()));
        } catch (Throwable t) {
            log.error("CrossBorderProposalAdminController.get error!", t);
            return ResultOf.error("system-error", t.toString());
        }
    }

    /**
     * 提交审核通过
     */
    @RequestMapping(path = "/audit-pass", method = RequestMethod.POST)
    @ResponseBody
    public Object postAuditPass(HttpServletRequest request, @RequestBody ProposalDistributeParam body) {
        try {
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isEmpty(empId)) {
                log.error("proposal audit pass error, user not login");
                return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
            }
            OperatorModel operator = OperatorModel.builder()
                .id(empId)
                .name(LoginUtil.getLoginName(request))
                .build();
            ResultOf<Boolean> result = proposalWriteService.auditPass(DcProposalAuditParam.builder()
                .proposalId(body.getProposalId())
                .operator(operator)
                .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                .build());

            if (!result.isSuccess()) {
                return RPCResult.error(result.getErrorCode(), result.getErrorMessage());
            } else {
                ResultOf<DcProposalModel> proposal = proposalReadService.queryProposalById(body.getProposalId());
                if (!ResultOf.isValid(proposal)) {
                    return RPCResult.error(proposal.getErrorCode(), proposal.getErrorMessage());
                }
                ResultOf<Boolean> distributeResult = dcDistributeManageService.productDistribution(
                    DcDistributeProductParam.builder()
                        .productId(proposal.getData().getProductId())
                        .channels(body.getChannelIds())
                        .operator(operator)
                        .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                        .build());
                if(ResultOf.isValid(distributeResult) && distributeResult.getData()) {
                    return RPCResult.ok(distributeResult.getData());
                } else {
                    return RPCResult.error("system-error", distributeResult.getErrorMessage());
                }
            }
        } catch (Exception e) {
            log.error("CrossBorderProposalAdminController.postAuditPass error!", e);
            return ResultOf.error("system-error", e.toString());
        }
    }

    /**
     * 提交审核通过
     */
    @RequestMapping(path = "/audit-pass-for-market", method = RequestMethod.POST)
    @ResponseBody
    public Object postAuditPassForMarket(HttpServletRequest request, @RequestBody ProposalDistributeParam body) {
        String errorTypeKey = "errorType";
        String offerIdKey = "offerId";
        String productIdKey = "productId";
        String auditValue = "audit";
        String distributeValue = "distribute";

        Long offerId = null;
        Long productId = null;
        try {
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isEmpty(empId)) {
                log.error("proposalResult audit pass for market error, user not login");
                return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR)
                    .addData(errorTypeKey, auditValue);
            }

            Long proposalId = body.getProposalId();
            OperatorModel operator = OperatorModel.builder()
                .id(empId)
                .name(LoginUtil.getLoginName(request))
                .build();

            //审核通过
            ResultOf<Boolean> result = proposalWriteService.auditPass(DcProposalAuditParam.builder()
                .proposalId(proposalId)
                .operator(operator)
                .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                .build());

            //查提报信息
            ResultOf<DcProposalModel> proposalResult = proposalReadService.queryProposalById(proposalId);
            if (!ResultOf.isValid(proposalResult)) {
                return RPCResult.error(proposalResult.getErrorCode(), proposalResult.getErrorMessage())
                    .addData(errorTypeKey, auditValue);
            }

            DcProposalModel proposalModel = proposalResult.getData();
            offerId = proposalModel.getOfferId();
            productId = proposalModel.getProductId();

            if (!result.isSuccess()) {
                return RPCResult.error(result.getErrorCode(), result.getErrorMessage())
                    .addData(errorTypeKey, auditValue)
                    .addData(offerIdKey, offerId)
                    .addData(productIdKey, Optional.ofNullable(productId).map(Object::toString).orElse(""));

            } else {

                ResultOf<Boolean> distributeResult = dcDistributeManageService.productMarketDistribution(
                    DcDistributeProductParam.builder()
                        .productId(productId)
                        .operator(operator)
                        .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                        .build());

                if(ResultOf.isValid(distributeResult) && distributeResult.getData()) {
                    return RPCResult.ok(distributeResult.getData())
                        .addData(offerIdKey, offerId)
                        .addData(productIdKey, Optional.ofNullable(productId).map(Object::toString).orElse(""));

                } else {
                    return RPCResult.error(distributeResult.getErrorCode(), distributeResult.getErrorMessage())
                        .addData(errorTypeKey, distributeValue)
                        .addData(offerIdKey, offerId)
                        .addData(productIdKey, Optional.ofNullable(productId).map(Object::toString).orElse(""));
                }
            }
        } catch (Exception e) {
            log.error("CrossBorderProposalAdminController.postAuditPass error!", e);
            return RPCResult.error("system-error", e.toString())
                .addData(offerIdKey, offerId)
                .addData(productIdKey, Optional.ofNullable(productId).map(Object::toString).orElse(""));
        }
    }



    /**
     * 提交审核不通过
     */
    @RequestMapping(path = "/audit-deny", method = RequestMethod.POST)
    @ResponseBody
    public Object postAuditDeny(HttpServletRequest request, @RequestBody ProposalAuditParam body) {
        try {
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isEmpty(empId)) {
                log.error("proposal audit deny error, user not login");
                return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
            }
            ResultOf<Boolean> result = proposalWriteService.auditDeny(DcProposalAuditParam.builder()
                .proposalId(body.getProposalId())
                .operator(OperatorModel.builder()
                    .id(empId)
                    .name(LoginUtil.getLoginName(request))
                    .build())
                .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                .msg(body.getMsg())
                .build());
            if (!result.isSuccess()) {
                return RPCResult.error(result.getErrorCode(), result.getErrorMessage());
            } else {
                return RPCResult.ok(true);
            }
        } catch (Exception e) {
            log.error("CrossBorderProposalAdminController.postAuditDeny error!", e);
            return ResultOf.error("system-error", e.toString());
        }
    }

    /**
     * 提交审核退回
     */
    @RequestMapping(path = "/audit-reject", method = RequestMethod.POST)
    @ResponseBody
    public Object postAuditReject(HttpServletRequest request, @RequestBody ProposalAuditParam body) {
        try {
            String empId = LoginUtil.getEmpId(request);
            if (StringUtils.isEmpty(empId)) {
                log.error("proposal audit reject error, user not login");
                return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
            }
            ResultOf<Boolean> result = proposalWriteService.auditReject(DcProposalAuditParam.builder()
                .proposalId(body.getProposalId())
                .operator(OperatorModel.builder()
                    .id(empId)
                    .name(LoginUtil.getLoginName(request))
                    .build())
                .bizType(DcProductBizTypeEnum.CROSSBORDER.name())
                .msg(body.getMsg())
                .build());
            if (!result.isSuccess()) {
                return RPCResult.error(result.getErrorCode(), result.getErrorMessage());
            } else {
                return RPCResult.ok(true);
            }
        } catch (Exception e) {
            log.error("CrossBorderProposalAdminController.postAuditReject error!", e);
            return ResultOf.error("system-error", e.toString());
        }
    }

}
