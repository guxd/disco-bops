package com.alibaba.cbuscm.controller.disco;

import com.alibaba.cbu.disco.shared.common.constant.ErrorCodeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcAuditService;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcChannelInfoService;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcProductEnrollService;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditStatusEnum;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcAuditTypeEnum;
import com.alibaba.cbu.disco.shared.core.enroll.constant.DcStockCommitStatus;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcAuditInfoRecord;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcMultiUserModel;
import com.alibaba.cbu.disco.shared.core.enroll.model.view.*;
import com.alibaba.cbu.disco.shared.core.product.api.DcOfferEnrollService;
import com.alibaba.cbu.disco.shared.core.supplier.api.DcSupplierService;
import com.alibaba.cbuscm.datashare.DataShareFileService;
import com.alibaba.cbuscm.datashare.TempFileUtil;
import com.alibaba.cbuscm.logic.DiscoLogic;
import com.alibaba.china.global.business.library.enums.StdCategoryChannelEnum;
import com.alibaba.china.global.business.library.interfaces.category.CategoryReadService;
import com.alibaba.china.global.business.library.models.category.StdCategoryModel;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.member.service.models.MemberModel;
import com.alibaba.china.offer.api.query.model.OfferModel;
import com.alibaba.china.offer.api.query.service.OfferQueryService;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningDesignModel;
import com.alibaba.china.shared.discosupplier.model.planning.opp.PlanningOppItemModel;
import com.alibaba.china.shared.discosupplier.service.planning.design.PlanningDesignService;
import com.alibaba.china.shared.discosupplier.service.planning.opp.PlanningOppItemService;
import com.alibaba.china.shared.sm.api.itemquery.ItemQueryService;
import com.alibaba.china.shared.sm.model.SmCommonResultDO;
import com.alibaba.china.shared.sm.model.itemquery.ItemModel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * DcProductEnrollController
 *
 * @author aniu.nxh
 * @date 2019/07/10 16:28
 */
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/dcProductEnroll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class DcProductEnrollController {

    private static final String OFFERID = "OFFERID";
    private static final String ITEMID = "ITEMID";
    private static final String TITLE = "TITLE";
    private static final String SUPPLIERLOGINID = "SUPPLIERLOGINID";

    private static final String SEARCH_TIME_ENROLL = "ENROLL";
    private static final String SEARCH_TIME_FIRST_APPROVE = "FIRST_APPROVE";
    private static final String SEARCH_TIME_SECOND_APPROVE = "SECOND_APPROVE";

    @Autowired
    private DcProductEnrollService dcProductEnrollService;
    @Autowired
    private DcSupplierService dcSupplierService;
    @Autowired
    private DcOfferEnrollService dcOfferEnrollService;
    @Autowired
    private DcAuditService dcAuditService;
    @Autowired
    private DiscoLogic discoLogic;
    @Autowired
    private MemberReadService memberReadService;
    @Autowired
    private DcChannelInfoService dcChannelInfoService;
    @Autowired
    private CategoryReadService categoryReadService;
    @Autowired
    private OfferQueryService<OfferModel> offerQueryService;
    @Autowired
    private ItemQueryService itemQueryService;
    @Autowired
    private DataShareFileService dataShareFileService;
    @Autowired
    private PlanningOppItemService planningOppItemService;
    @Autowired
    private PlanningDesignService planningDesignService;

    private static final String DATASHAREURL
            = "http://datashare.alibaba-inc.com:8080/tddp2/index.do#A=myFile:myFile:myFileList";

    private ExcelExportThread exportThread = new ExcelExportThread();

    @ResponseBody
    @RequestMapping(value = "/editBatchOfferCommission", method = RequestMethod.POST)
    public ListOf<ResultOf<Boolean>> editBatchOfferCommission(HttpServletRequest request,
                                                                @RequestBody BatchCommissionParam batchCommissionParam) {

        String logPrefix = "DcProductEnrollController#editBatchOfferCommission#";
        String paramInfo = "#params:" + JSON.toJSONString(batchCommissionParam) + "#";

        log.error(logPrefix + "record" + paramInfo);

        ListOf<ResultOf<Boolean>> result = new ListOf<>();
        result.setSuccess(false);
        try {
            if (batchCommissionParam == null) {
                return result;
            }
            List<CommissionParam> commissionParamList = batchCommissionParam.getCommissionParamList();

            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {

                List<ResultOf<Boolean>> resultList = new ArrayList<ResultOf<Boolean>>();
                result.setData(resultList);

                if (CollectionUtils.isEmpty(commissionParamList)) {
                    result.setErrorCode("EditOffer_Null");
                    result.setErrorMessage("修改佣金比例不能为空");
                    return result;
                }

                boolean isAllSuccess = true;
                StringBuilder sb = new StringBuilder();

                for (CommissionParam commissionParam: commissionParamList) {
                    if (commissionParam == null) {
                        continue;
                    }
                    DcCommissionParam param = new DcCommissionParam();
                    param.setChannel(commissionParam.getChannel());
                    param.setCommission(new BigDecimal(commissionParam.getCommission()));
                    param.setEnrollId(commissionParam.getEnrollId());
                    param.setOfferId(commissionParam.getOfferId());
                    param.setOriginUserId(null);

                    // 修改
                    ResultOf<Boolean> editResult = dcProductEnrollService.editOfferCommission(param, resultOf.getData());
                    resultList.add(editResult);

                    if (!editResult.isSuccess()) {
                        isAllSuccess = false;
                        sb.append("CurFail:" + JSON.toJSONString(commissionParam) + "#CurResult:" + editResult.getErrorCode() + "$");
                    }
                }

                if (isAllSuccess) {
                    result.setSuccess(true);
                } else {
                    result.setErrorCode("Not_All_Success");
                    result.setErrorMessage(sb.toString());
                }
            } else {
                result.setData(new ArrayList<>());
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/editOfferCommission", method = RequestMethod.GET)
    public ResultOf<Boolean> editOfferCommission(HttpServletRequest request,
                                                    @RequestParam(name = "channel") String channel,
                                                    @RequestParam(name = "commission") String commission,
                                                    @RequestParam(name = "enrollId") Long enrollId,
                                                    @RequestParam(name = "offerId") Long offerId) {

        String logPrefix = "DcProductEnrollController#editOfferCommission#";
        String paramInfo = "#channel:" + channel + "#commission:" + commission + "#enrollId:" + enrollId +
            "#offerId:" + offerId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                DcCommissionParam commissionParam = new DcCommissionParam();
                commissionParam.setChannel(channel);
                commissionParam.setCommission(new BigDecimal(commission));
                commissionParam.setEnrollId(enrollId);
                commissionParam.setOfferId(offerId);
                commissionParam.setOriginUserId(null);
                // 修改佣金比例
                result = dcProductEnrollService.editOfferCommission(commissionParam, resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/updateOfferItemInfo", method = RequestMethod.POST)
    public ResultOf<Boolean> updateOfferItemInfo(HttpServletRequest request, @RequestBody OfferItemInfoParam param) {

        String logPrefix = "DcProductEnrollController#updateOfferItemInfo#";
        String paramInfo = "#offerMap:" + JSON.toJSONString(param)+ "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                if (param == null) {
                    return result;
                }

                // 更新报名入库的规格匹配信息
                result = dcProductEnrollService.updateOfferItemInfo(param.getChannel(), param.getSupplierUserId(), param.getOfferId(),
                    param.getItemId(), param.getOfferItemSkuPairList(), resultOf.getData());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/countByStatus", method = RequestMethod.GET)
    public ResultOf<Map<DcAuditStatusEnum, Long>> countByStatus(HttpServletRequest request,
                                                                    @RequestParam(name = "channel") String channel) {

        String logPrefix = "DcProductEnrollController#countByStatus#";
        String paramInfo = "#channel:" + channel + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Map<DcAuditStatusEnum, Long>> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                DcEmpModel empModel = resultOf.getData();
                Map<DcAuditStatusEnum, Long> resultMap = new HashMap<>();
                result.setData(resultMap);

                resultMap.put(DcAuditStatusEnum.ALL, fetchCount(channel, null, empModel));
                resultMap.put(DcAuditStatusEnum.WAIT,
                    fetchCount(channel, DcAuditStatusEnum.WAIT.getValue(), empModel));
                resultMap.put(DcAuditStatusEnum.FIRST_REJECT,
                    fetchCount(channel, DcAuditStatusEnum.FIRST_REJECT.getValue(), empModel));
                resultMap.put(DcAuditStatusEnum.FIRST_APPROVE,
                    fetchCount(channel, DcAuditStatusEnum.FIRST_APPROVE.getValue(), empModel));
                resultMap.put(DcAuditStatusEnum.SECOND_REJECT,
                    fetchCount(channel, DcAuditStatusEnum.SECOND_REJECT.getValue(), empModel));
                resultMap.put(DcAuditStatusEnum.SECOND_APPROVE,
                    fetchCount(channel, DcAuditStatusEnum.SECOND_APPROVE.getValue(), empModel));
                resultMap.put(DcAuditStatusEnum.THIRD_APPROVE,
                        fetchCount(channel, DcAuditStatusEnum.THIRD_APPROVE.getValue(), empModel));
                resultMap.put(DcAuditStatusEnum.THIRD_REJECT,
                        fetchCount(channel, DcAuditStatusEnum.THIRD_REJECT.getValue(), empModel));
                resultMap.put(DcAuditStatusEnum.REJECT,
                    fetchCount(channel, DcAuditStatusEnum.REJECT.getValue(), empModel));
                result.setSuccess(true);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/countByStockStatus", method = RequestMethod.GET)
    public ResultOf<Map<DcStockCommitStatus, Long>> countByStockStatus(HttpServletRequest request,
                                                                        @RequestParam(name = "channel") String channel) {

        String logPrefix = "DcProductEnrollController#countByStockStatus#";
        String paramInfo = "#channel:" + channel + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Map<DcStockCommitStatus, Long>> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                DcEmpModel empModel = resultOf.getData();
                Map<DcStockCommitStatus, Long> resultMap = new HashMap<>();
                result.setData(resultMap);

                Set<String> allStockCommitStatus = new HashSet<>();
                allStockCommitStatus.add(DcStockCommitStatus.STOCK_APPROVED.getValue());
                allStockCommitStatus.add(DcStockCommitStatus.STOCK_REJECT.getValue());
                allStockCommitStatus.add(DcStockCommitStatus.STOCK_SUBMIT.getValue());

                resultMap.put(DcStockCommitStatus.STOCK_ALL, fetchCount(channel, null, allStockCommitStatus, empModel));
                resultMap.put(DcStockCommitStatus.STOCK_APPROVED,
                    fetchCount(channel, DcStockCommitStatus.STOCK_APPROVED.getValue(), null, empModel));
                resultMap.put(DcStockCommitStatus.STOCK_REJECT,
                    fetchCount(channel, DcStockCommitStatus.STOCK_REJECT.getValue(), null, empModel));
                resultMap.put(DcStockCommitStatus.STOCK_SUBMIT,
                    fetchCount(channel, DcStockCommitStatus.STOCK_SUBMIT.getValue(), null, empModel));
                result.setSuccess(true);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/fetchSingleOffer", method = RequestMethod.GET)
    public ResultOf<DcOfferVO> fetchSingleOffer(HttpServletRequest request,
                                                @RequestParam(name = "supplierUserId") Long supplierUserId,
                                                @RequestParam(name = "offerId") Long offerId) {

        String logPrefix = "DcProductEnrollController#fetchSingleOffer#";
        String paramInfo = "#sid:" + supplierUserId + "#offerId:" + offerId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<DcOfferVO> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 提取单个offer的商品信息 采购单里使用用来获取当前下单的offerId信息的
                result = dcProductEnrollService.fetchSingleOffer(offerId, supplierUserId);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/getOfferItemVO", method = RequestMethod.GET)
    public ResultOf<DcOfferItemMatchVO> getOfferItemVO(HttpServletRequest request,
                                                        @RequestParam(name = "channel") String channel,
                                                        @RequestParam(name = "supplierUserId") Long userId,
                                                        @RequestParam(name = "offerId") Long offerId,
                                                        @RequestParam(name = "itemId") Long itemId) {

        String logPrefix = "DcProductEnrollController#getOfferItemVO#";
        String paramInfo = "#channel:" + channel + "#sid:" + userId + "#offerId:" + offerId + "#itemId:" + itemId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<DcOfferItemMatchVO> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // 获取单个商品规格匹配的基础信息 这里是基于淘宝sku为主，去映射1688
                result = dcProductEnrollService.getOfferItemVO(channel, userId, offerId, itemId,
                    resultOf.getData().getEmpType());
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/listOfferItemVO", method = RequestMethod.GET)
    public PageOf<DcOfferItemVO> listOfferItemVO(HttpServletRequest request,
                            @RequestParam(name = "channel") String channel,
                            @RequestParam(name = "pageIdx") int pageIdx,
                            @RequestParam(name = "pageSize") int pageSize,
                            @RequestParam(name = "searchType") String searchType,
                            @RequestParam(name = "auditStatus", required = false) String auditStatus,
                            @RequestParam(name = "stockCommitStatus", required = false) String stockCommitStatus,
                            @RequestParam(name = "keyword", required = false) String keyword,
                            @RequestParam(name = "timeSearchType", required = false) String timeSearchType,
                            @RequestParam(name = "searchTimeBeginMs", required = false) String searchTimeBeginMs,
                            @RequestParam(name = "searchTimeEndMs", required = false) String searchTimeEndMs,
                            @RequestParam(name = "offerCategorys", required = false) String offerCategorys,
                            @RequestParam(name = "itemCategorys", required = false) String itemCategorys,
                                                 @RequestParam(name = "offerSource", required = false) String offerSource) {

        String logPrefix = "DcProductEnrollController#listOfferItemVO#";
        String paramInfo = "#channel:" + channel + "#page:" + pageIdx + "#pageSize:" + pageSize + "#searchType:"
            + searchType + "#auditStatus:" + auditStatus + "#stockCommitStatus:" + stockCommitStatus + "#keyword:"
            + keyword + "#timeSearchType:" + timeSearchType + "#searchTimeBeginMs:" + searchTimeBeginMs
            + "#searchTimeEndMs:" + searchTimeEndMs + "#offerCategorys:" + offerCategorys
            + "#itemCategorys:" + itemCategorys + "#"+ "#offerSource:" + offerSource + "#";

        log.error(logPrefix + "record" + paramInfo);

        PageOf<DcOfferItemVO> result = new PageOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                Long searchTimeBeginDateMs = StringUtils.isBlank(searchTimeBeginMs) ? null : Long.valueOf(
                    searchTimeBeginMs);
                Long searchTimeEndsDateMs = StringUtils.isBlank(searchTimeEndMs) ? null : Long.valueOf(searchTimeEndMs);

                DateTimeBetween dateTimeBetween = null;
                if (StringUtils.isNotBlank(timeSearchType) && (searchTimeBeginDateMs != null
                    || searchTimeEndsDateMs != null)) {
                    Date searchTimeBegin = searchTimeBeginDateMs == null ? new Date(0L) : new Date(
                        searchTimeBeginDateMs);
                    Date searchTimeEnd = searchTimeEndsDateMs == null ? new Date() : new Date(searchTimeEndsDateMs);
                    dateTimeBetween = new DateTimeBetween();
                    dateTimeBetween.setStartTime(searchTimeBegin);
                    dateTimeBetween.setEndTime(searchTimeEnd);
                }

                DcOfferItemQueryParam queryParam = new DcOfferItemQueryParam();
                queryParam.setChannel(channel);
                queryParam.setEmpType(resultOf.getData().getEmpType());
                queryParam.setOriginUserId(null);
                queryParam.setPageIdx(pageIdx);
                queryParam.setPageSize(pageSize);
                //add by sean 2019.8.15 增加商品来源
                queryParam.setOfferSource(offerSource);

                if (StringUtils.isNotBlank(auditStatus)) {
                    Set<String> auditStatusSet = new HashSet<String>();
                    auditStatusSet.add(auditStatus);
                    queryParam.setAuditStatus(auditStatusSet);
                }

                if (StringUtils.isNotBlank(stockCommitStatus)) {
                    Set<String> commitStatusSet = new HashSet<String>();
                    if (stockCommitStatus.equals(DcStockCommitStatus.STOCK_ALL.getValue())) {
                        commitStatusSet.add(DcStockCommitStatus.STOCK_SUBMIT.getValue());
                        commitStatusSet.add(DcStockCommitStatus.STOCK_REJECT.getValue());
                        commitStatusSet.add(DcStockCommitStatus.STOCK_APPROVED.getValue());
                    } else {
                        commitStatusSet.add(stockCommitStatus);
                    }
                    queryParam.setStockCommitStatus(commitStatusSet);
                }

                //时间判断
                if (StringUtils.equals(timeSearchType, SEARCH_TIME_ENROLL)) {
                    queryParam.setCreateTimeRange(dateTimeBetween);
                } else if (StringUtils.equals(timeSearchType, SEARCH_TIME_FIRST_APPROVE)) {
                    queryParam.setFirstApproveTimeRange(dateTimeBetween);
                } else if (StringUtils.equals(timeSearchType, SEARCH_TIME_SECOND_APPROVE)) {
                    queryParam.setSecondApproveTimeRange(dateTimeBetween);
                }

                // TB类目id
                if (StringUtils.isNotBlank(itemCategorys)) {
                    queryParam.setItemCategorys(itemCategorys);
                }
                // 1688类目id
                if (StringUtils.isNotBlank(offerCategorys)) {
                    queryParam.setOfferCategorys(offerCategorys);
                }

                List<String> keywordList = Lists.newArrayList();
                if(StringUtils.isNotBlank(keyword)){
                    keyword = keyword.replaceAll("，",",");
                    keyword = keyword.replaceAll("\\s+",",");
                    keywordList=Arrays.asList(keyword.split(","));
                }

                //检索类型互斥地划分为 供应商ID，ItemId，offerId三类
                if (CollectionUtils.isEmpty(keywordList)) {
                    //全量查询，没有额外地处理
                } else if (searchType.equals(SUPPLIERLOGINID)) {
                    List<MemberModel> memberModels = memberReadService.listMemberByLoginIds(keywordList);
                    MemberModel memberModel = memberReadService.findMemberByLoginId(keyword);
                    if (memberModel==null) {
                        result.setErrorCode("Member_Illegal");
                        result.setErrorMessage("无效的用户");
                        return result;
                    }
                    queryParam.setOriginUserId(memberModel.getUserId());
                } else if (searchType.equals(OFFERID)) {
                    List<Long> offerIds = keywordList.stream()
                            .filter(StringUtils::isNumeric)
                            .map(Long::valueOf)
                            .collect(Collectors.toList());
                    if(CollectionUtils.isEmpty(offerIds)){
                        result.setData(new ArrayList<DcOfferItemVO>());
                        result.setTotal(0);
                        result.setErrorCode("OfferId_Illegal");
                        result.setErrorMessage("无效的1688商品Id");
                        return result;
                    }else if(offerIds.size()==1){
                        queryParam.setOfferId(offerIds.get(0));
                    }else{
                        queryParam.setOfferIds(offerIds);
                    }

                } else if (searchType.equals(ITEMID)) {
                    List<Long> itemIds = keywordList.stream()
                            .filter(StringUtils::isNumeric)
                            .map(Long::valueOf)
                            .collect(Collectors.toList());
                    if(CollectionUtils.isEmpty(itemIds)){
                        result.setData(new ArrayList<DcOfferItemVO>());
                        result.setTotal(0);
                        result.setErrorCode("ItemId_Illegal");
                        result.setErrorMessage("无效的淘宝商品Id");
                        return result;
                    }else if(itemIds.size()==1){
                        queryParam.setItemId(itemIds.get(0));
                    }else{
                        queryParam.setItemIds(itemIds);
                    }
                } else if (searchType.equals(TITLE)) {
                    queryParam.setTitle(keyword);
                }

                result = dcProductEnrollService.listOfferItemVO(queryParam);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "/exportOfferItemVO", method = RequestMethod.GET)
    public RPCResult exportOfferItemVO(HttpServletRequest request,
                                                 @RequestParam(name = "channel",required = true) String channel,
                                                 @RequestParam(name = "type",required= true) int type,
                                                 @RequestParam(name = "enrollIds",required = false) String enrollIds,
                                                 @RequestParam(name = "searchType",required = false) String searchType,
                                                 @RequestParam(name = "auditStatus", required = false) String auditStatus,
                                                 @RequestParam(name = "stockCommitStatus", required = false) String stockCommitStatus,
                                                 @RequestParam(name = "keyword", required = false) String keyword,
                                                 @RequestParam(name = "timeSearchType", required = false) String timeSearchType,
                                                 @RequestParam(name = "searchTimeBeginMs", required = false) String searchTimeBeginMs,
                                                 @RequestParam(name = "searchTimeEndMs", required = false) String searchTimeEndMs,
                                                 @RequestParam(name = "offerCategorys", required = false) String offerCategorys,
                                                 @RequestParam(name = "itemCategorys", required = false) String itemCategorys,
                                                 @RequestParam(name = "offerSource", required = false) String offerSource) {

        String logPrefix = "DcProductEnrollController#exportOfferItemVO#";
        String paramInfo = "#channel:" + channel + "#searchType:"
                + searchType + "#auditStatus:" + auditStatus + "#stockCommitStatus:" + stockCommitStatus + "#keyword:"
                + keyword + "#timeSearchType:" + timeSearchType + "#searchTimeBeginMs:" + searchTimeBeginMs
                + "#searchTimeEndMs:" + searchTimeEndMs + "#offerCategorys:" + offerCategorys
                + "#itemCategorys:" + itemCategorys + "#"+ "#offerSource:" + offerSource + "#";

        log.error(logPrefix + "record" + paramInfo);
        RPCResult result = new RPCResult();

        if (StringUtils.isBlank(channel)) {
            result.setErrorCode("Channel_IsNull");
            result.setErrorMsg("渠道不能为空");
            return result;
        }

        try {
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if(resultOf == null || !resultOf.isSuccess() || resultOf.getData() == null){
                if (resultOf != null) {
                    result.setErrorCode(resultOf.getErrorCode());
                    result.setErrorMsg(resultOf.getErrorMessage());
                }
                result.setSuccess(false);
                return result;
            }
            if(type == 1){
                // 获取dcEmpModel
                    Long searchTimeBeginDateMs = StringUtils.isBlank(searchTimeBeginMs) ? null : Long.valueOf(
                            searchTimeBeginMs);
                    Long searchTimeEndsDateMs = StringUtils.isBlank(searchTimeEndMs) ? null : Long.valueOf(searchTimeEndMs);

                    DateTimeBetween dateTimeBetween = null;
                    if (StringUtils.isNotBlank(timeSearchType) && (searchTimeBeginDateMs != null
                            || searchTimeEndsDateMs != null)) {
                        Date searchTimeBegin = searchTimeBeginDateMs == null ? new Date(0L) : new Date(
                                searchTimeBeginDateMs);
                        Date searchTimeEnd = searchTimeEndsDateMs == null ? new Date() : new Date(searchTimeEndsDateMs);
                        dateTimeBetween = new DateTimeBetween();
                        dateTimeBetween.setStartTime(searchTimeBegin);
                        dateTimeBetween.setEndTime(searchTimeEnd);
                    }

                    DcOfferItemQueryParam queryParam = new DcOfferItemQueryParam();
                    queryParam.setChannel(channel);
                    queryParam.setEmpType(resultOf.getData().getEmpType());
                    queryParam.setOriginUserId(null);
                    queryParam.setOfferSource(offerSource);

                    if (StringUtils.isNotBlank(auditStatus)) {
                        Set<String> auditStatusSet = new HashSet<String>();
                        auditStatusSet.add(auditStatus);
                        queryParam.setAuditStatus(auditStatusSet);
                    }

                    if (StringUtils.isNotBlank(stockCommitStatus)) {
                        Set<String> commitStatusSet = new HashSet<String>();
                        if (stockCommitStatus.equals(DcStockCommitStatus.STOCK_ALL.getValue())) {
                            commitStatusSet.add(DcStockCommitStatus.STOCK_SUBMIT.getValue());
                            commitStatusSet.add(DcStockCommitStatus.STOCK_REJECT.getValue());
                            commitStatusSet.add(DcStockCommitStatus.STOCK_APPROVED.getValue());
                        } else {
                            commitStatusSet.add(stockCommitStatus);
                        }
                        queryParam.setStockCommitStatus(commitStatusSet);
                    }

                    //时间判断
                    if (StringUtils.equals(timeSearchType, SEARCH_TIME_ENROLL)) {
                        queryParam.setCreateTimeRange(dateTimeBetween);
                    } else if (StringUtils.equals(timeSearchType, SEARCH_TIME_FIRST_APPROVE)) {
                        queryParam.setFirstApproveTimeRange(dateTimeBetween);
                    } else if (StringUtils.equals(timeSearchType, SEARCH_TIME_SECOND_APPROVE)) {
                        queryParam.setSecondApproveTimeRange(dateTimeBetween);
                    }

                    //类目ID
                    if (StringUtils.isNotBlank(offerCategorys)) {
                        queryParam.setOfferCategorys(offerCategorys);
                    }
                    if (StringUtils.isNotBlank(itemCategorys)) {
                        queryParam.setItemCategorys(offerCategorys);
                    }

                    List<String> keywordList = Lists.newArrayList();
                    if (StringUtils.isNotBlank(keyword)) {
                        keyword = keyword.replaceAll("，", ",");
                        keyword = keyword.replaceAll("\\s+", ",");
                        keywordList = Arrays.asList(keyword.split(","));
                    }

                    //检索类型互斥地划分为 供应商ID，ItemId，offerId三类
                    if (CollectionUtils.isEmpty(keywordList)) {
                        //全量查询，没有额外地处理
                    } else if (searchType.equals(SUPPLIERLOGINID)) {
                        List<MemberModel> memberModels = memberReadService.listMemberByLoginIds(keywordList);
                        MemberModel memberModel = memberReadService.findMemberByLoginId(keyword);
                        if (memberModel == null) {
                            result.setErrorCode("Member_Illegal");
                            result.setErrorMsg("无效的用户");
                            return result;
                        }
                        queryParam.setOriginUserId(memberModel.getUserId());
                    } else if (searchType.equals(OFFERID)) {
                        List<Long> offerIds = keywordList.stream()
                                .filter(StringUtils::isNumeric)
                                .map(Long::valueOf)
                                .collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(offerIds)) {
                            result.setSuccess(false);
                            result.setErrorCode("OfferId_Illegal");
                            result.setErrorMsg("无效的1688商品Id");
                            return result;
                        } else if (offerIds.size() == 1) {
                            queryParam.setOfferId(offerIds.get(0));
                        } else {
                            queryParam.setOfferIds(offerIds);
                        }

                    } else if (searchType.equals(ITEMID)) {
                        List<Long> itemIds = keywordList.stream()
                                .filter(StringUtils::isNumeric)
                                .map(Long::valueOf)
                                .collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(itemIds)) {
                            result.setSuccess(false);
                            result.setErrorCode("ItemId_Illegal");
                            result.setErrorMsg("无效的淘宝商品Id");
                            return result;
                        } else if (itemIds.size() == 1) {
                            queryParam.setItemId(itemIds.get(0));
                        } else {
                            queryParam.setItemIds(itemIds);
                        }
                    } else if (searchType.equals(TITLE)) {
                        queryParam.setTitle(keyword);
                    }
                    if(!exportThread.isStart()){
                        synchronized(exportThread){
                            exportThread.setType(type);
                            exportThread.setQueryParam(queryParam);
                            exportThread.setEmpNo(resultOf.getData().getEmpId());
                            new Thread(exportThread).start();
                        }
                        return RPCResult.ok(DATASHAREURL);
                    }else{
                        result.setErrorCode("Export_Using");
                        result.setErrorMsg("数据导出正在运行，请稍后重试");
                        result.setSuccess(false);
                        return result;
                    }

            }else if(type==2){
                if(StringUtils.isEmpty(enrollIds) || StringUtils.isEmpty(channel)){
                    result.setErrorCode("Param_Loss");
                    result.setErrorMsg("缺少导出参数");
                    result.setSuccess(false);
                    return result;
                }
                String[] idArrays = enrollIds.split(",");
                List<Long> idList = new ArrayList<>();
                if (idArrays != null) {
                    for(String id : idArrays){
                        try{
                            idList.add(Long.parseLong(id));
                        }catch(Exception e){
                            //ignore
                        }
                    }
                }
                if(!exportThread.isStart()){
                    synchronized(exportThread){
                        DcOfferItemQueryParam param = new DcOfferItemQueryParam();
                        param.setChannel(channel);
                        exportThread.setIds(idList);
                        exportThread.setQueryParam(param);
                        exportThread.setType(type);
                        exportThread.setEmpNo(resultOf.getData().getEmpId());
                        new Thread(exportThread).start();
                    }
                    return RPCResult.ok(DATASHAREURL);
                }else{
                    result.setErrorCode("Export_Using");
                    result.setErrorMsg("数据导出正在运行，请稍后重试");
                    result.setSuccess(false);
                    return result;
                }
            }else{
                result.setErrorCode("Param_Illegal");
                result.setErrorMsg("无效的导出类型");
                result.setSuccess(false);
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/updatePurchaseOrder", method = RequestMethod.GET)
    public ResultOf<Boolean> updatePurchaseOrder(HttpServletRequest request,
                                                    @RequestParam(name = "channel") String channel,
                                                    @RequestParam(name = "purchaseSubId") Long purchaseSubId,
                                                    @RequestParam(name = "offerId") Long offerId,
                                                    @RequestParam(name = "skuId") Long skuId) {

        String logPrefix = "DcProductEnrollController#updatePurchaseOrder#";
        String paramInfo = "#channel:" + channel + "#offerId:" + offerId + "#purchaseSubId:" + purchaseSubId + "#skuId:"
            + skuId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                // Long userId = 3687900876L;
                ResultOf<DcMultiUserModel> channelResult = dcChannelInfoService.fetchMultiModelByChannel(channel);
                if (channelResult == null || !channelResult.isSuccess() || channelResult.getData() == null) {
                    result.setErrorCode("channel_Illegal");
                    result.setErrorMessage("渠道异常");
                    return result;
                }
                String purchaseUserIdText = channelResult.getData().getPurchaseUserId();
                if (StringUtils.isBlank(purchaseUserIdText)) {
                    result.setErrorCode("channel_Illegal");
                    result.setErrorMessage("渠道异常");
                    return result;
                }
                Long purchaseUserId = Long.valueOf(purchaseUserIdText);
                result = dcProductEnrollService.updatePurchaseOrder(purchaseUserId, purchaseSubId, offerId, skuId);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    /**
     * 返回各个状态的数量
     *
     * @param channel     渠道
     * @param auditStatus 状态
     * @param empModel    empModel
     * @return
     */
    private Long fetchCount(String channel, String auditStatus, DcEmpModel empModel) {

        ResultOf<Long> result = null;
        if (auditStatus == null) {
            result = dcProductEnrollService.countByStatus(channel, null, null, empModel);
        } else if (DcAuditStatusEnum.valueOf(auditStatus) != null) {
            Set<String> auditSet = new HashSet<String>();
            auditSet.add(auditStatus);
            result = dcProductEnrollService.countByStatus(channel, null, auditSet, empModel);
        }
        if (result != null && result.isSuccess()) {
            return result.getData();
        }
        return 0L;
    }

    /**
     * 返回各个库存提报状态的数量
     *
     * @param channel      渠道
     * @param singleStatus singleStatus
     * @param multiStatus  库存提报状态
     * @param empModel     empModel
     * @return
     */
    private Long fetchCount(String channel, String singleStatus, Set<String> multiStatus, DcEmpModel empModel) {

        ResultOf<Long> result = null;
        if (singleStatus == null && CollectionUtils.isEmpty(multiStatus)) {
            result = dcProductEnrollService.countByStockStatus(channel, null, null, empModel);
        } else if (CollectionUtils.isNotEmpty(multiStatus)) {
            result = dcProductEnrollService.countByStockStatus(channel, null, multiStatus, empModel);
        } else if (DcStockCommitStatus.valueOf(singleStatus) != null) {
            Set<String> statusSet = new HashSet<String>();
            statusSet.add(singleStatus);
            result = dcProductEnrollService.countByStockStatus(channel, null, statusSet, empModel);
        }
        if (result != null && result.isSuccess()) {
            return result.getData();
        }
        return 0L;
    }

    private Set<String> textToSet(String originText) {

        if (StringUtils.isBlank(originText)) {
            return null;
        }
        String[] arr = StringUtils.split(originText, ",");
        if (arr == null || arr.length <= 0) {
            return null;
        }
        Set<String> result = new HashSet<String>(Arrays.asList(arr));
        return result;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    private static class CommissionParam{
        private String channel;
        private String commission;
        private Long enrollId;
        private Long offerId;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    private static class BatchCommissionParam{
        private List<CommissionParam> commissionParamList;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    private static class OfferItemInfoParam{
        private String channel;
        private Long offerId;
        private Long supplierUserId;
        private Long itemId;
        private List<DcOfferItemSkuPair> offerItemSkuPairList;
    }


    class ExcelExportThread implements Runnable{

        private String[] easyExcelHead = new String[]{"企划ID", "企划名称", "企划类型", "1688商品ID", "淘系商品ID", "1688商家公司名称", "1688供应商login_id", "1688供应商uesr_id", "1688商品一级类目",
                "1688商品二级类目", "1688商品三级类目", "1688商品SKU名称", "1688商品SKUID", "淘系商品SKU名称", "淘系商品SKUID", "淘系商品一级类目", "淘系商品二级类目", "淘系商品三级类目", "淘系商品叶子类目",
                "渠道库存", "淘系零售价（元）", "划线价（元）", "佣金比例", "供应商首次报名时间(年)", "供应商首次报名时间(月)", "供应商首次报名时间(日)", "审核状态", "是否已缴纳保证金", "最后一次审核小二",
                "最后一次审核时间(年)", "最后一次审核时间(月)", "最后一次审核时间(日)", "格式化的原因分类", "原因详情"};

        DcOfferItemQueryParam queryParam;

        List<Long> ids;

        int type;

        String empNo;

        AtomicBoolean isStart = new AtomicBoolean(false);

        public DcOfferItemQueryParam getQueryParam() {
            return queryParam;
        }

        public String getEmpNo() {
            return empNo;
        }

        public void setEmpNo(String empNo) {
            this.empNo = empNo;
        }

        public void setQueryParam(DcOfferItemQueryParam queryParam) {
            this.queryParam = queryParam;
        }

        public List<Long> getIds() {
            return ids;
        }

        public void setIds(List<Long> ids) {
            this.ids = ids;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public boolean isStart() {
            return isStart.get();
        }

        public boolean setStart(boolean org,boolean newV) {
            return isStart.compareAndSet(org,newV);
        }

        @Override
        public void run() {
            try {
                if(!setStart(false,true)){
                    return;
                }
                int pageIdx = 1;
                int pageSize = 20;
                List<DcOfferItemVO> items = new ArrayList<DcOfferItemVO>();
                if (type == 1) {
                    while (true) {
                        queryParam.setPageIdx(pageIdx++);
                        queryParam.setPageSize(pageSize);
                        PageOf<DcOfferItemVO> pageItems = dcProductEnrollService.listOfferItemVO(queryParam);
                        if (pageItems == null || !pageItems.isSuccess() || pageItems.getData() == null) {
                            sleep100();
                            break;
                        } else {
                            List<DcOfferItemVO> pageData = pageItems.getData();
                            items.addAll(pageData);
                            if (pageData.size() < pageSize) {
                                break;
                            }
                            sleep100();
                        }
                    }
                } else {
                    ListOf<DcOfferItemVO> pageLists = dcProductEnrollService.findByIds(queryParam.getChannel(), ids);
                    if (pageLists == null || !pageLists.isSuccess() && CollectionUtils.isEmpty(pageLists.getData())) {
                        return;
                    }
                    items = pageLists.getData();
                }

                Map<String, String> memberAudit = new HashMap<String, String>();
                Map<Integer, StdCategoryModel> cbuCategoryMaps = categoryReadService.getChannelAllStdCategoryMap(StdCategoryChannelEnum.CHANNEL_1688);
                Map<Integer, StdCategoryModel> tbCategoryMaps = categoryReadService.getChannelAllStdCategoryMap(StdCategoryChannelEnum.CHANNEL_TB);
                List<Map<Integer, String>> sheetDatas = new ArrayList<Map<Integer, String>>();
                List<Long> offerIds = new ArrayList<Long>();
                if (CollectionUtils.isNotEmpty(items)) {
                    for (DcOfferItemVO item : items) {
                        offerIds.add(item.getOfferId());
                    }
                }
                Long[] longArray = null;
                if (offerIds != null) {
                    longArray = offerIds.toArray(new Long[offerIds.size()]);
                }
                List<OfferModel> offerModelList = new ArrayList<>();;
                if (longArray != null && longArray.length > 0) {
                    int length = longArray.length;
                    int index = 50;
                    // 所有的OfferModel数据集
                    for (int i = 0; i < length; i += 50 ){
                        if (i + 50 > length) {
                            index = length - i;
                        }
                        Long[] arrays = Arrays.copyOfRange(longArray, i, i + index);
                        List<OfferModel> offerModels = offerQueryService.list(arrays);
                        offerModelList.addAll(offerModels);
                    }
                }
                Map<Long, OfferModel> offerMap = new HashMap<Long, OfferModel>();
                if (CollectionUtils.isNotEmpty(offerModelList)) {
                    for (OfferModel offer : offerModelList) {
                        offerMap.put(offer.getOffer().getId(), offer);
                    }
                }
                Map<Long, String> companyNameMap = new HashMap<>();
                for (DcOfferItemVO item : items) {
                    //没有sku就退出
                    if (item == null || CollectionUtils.isEmpty(item.getOfferSkuList())) {
                        continue;
                    }
                    Map<Integer, String> sheetData = new HashMap<Integer, String>();

                    //查询提报商品关联的企划信息
                    PlanningOppItemModel byEnrollId = planningOppItemService.findByEnrollId(item.getEnrollId());
                    if (null!=byEnrollId) {
                        PlanningDesignModel byPlanningId = planningDesignService.find(byEnrollId.getPlanningId());
                        if (null != byPlanningId) {
                            sheetData.put(1, String.valueOf(item.getPlanningId()));
                            sheetData.put(2, byPlanningId.getTitle());
                            sheetData.put(3, byPlanningId.getType().getMessage());
                        }
                    }

                    //4:offerId
                    sheetData.put(4, String.valueOf(item.getOfferId()));
                    //5:itemId
                    sheetData.put(5, String.valueOf(item.getItemId()));

                    //6:1688商家公司
                    if (item.getSupplierUserId() != null) {
                        String companyName = companyNameMap.get(item.getSupplierUserId());
                        if (StringUtils.isBlank(companyName)) {
                            MemberModel memberModel = memberReadService.findMemberByHavanaId(item.getSupplierUserId());
                            if (memberModel != null) {
                                sheetData.put(6, memberModel.getCompanyName());
                                companyNameMap.put(item.getSupplierUserId(), memberModel.getCompanyName());
                            }
                        } else {
                            sheetData.put(6, companyName);
                        }
                    }
                    //7:1688商家名称
                    sheetData.put(7, item.getSupplierLoginId());
                    //8:1688商家 user id
                    sheetData.put(8, String.valueOf(item.getSupplierUserId()));
                    OfferModel offer = null;
                    if (MapUtils.isNotEmpty(offerMap)) {
                        offer = offerMap.get(item.getOfferId());
                    }
                    if (offer != null && offer.getOffer() != null && offer.getOffer().getProduct() != null) {
                        if (MapUtils.isNotEmpty(cbuCategoryMaps)) {
                            Long categoryID = offer.getOffer().getProduct().getCategoryID();
                            StdCategoryModel stdCategoryModel = cbuCategoryMaps.get(categoryID == null ? 0 : categoryID.intValue());
                            List<String> catePath = new ArrayList<String>();
                            while (stdCategoryModel != null) {
                                catePath.add(stdCategoryModel.getName());
                                stdCategoryModel = cbuCategoryMaps.get(stdCategoryModel.getParentId());
                            }
                            //9:1688商品一级类目
                            if (CollectionUtils.isNotEmpty(catePath)) {
                                if (catePath.size() > 1) {
                                    sheetData.put(9, catePath.get(catePath.size() - 2));
                                }
                                //10:1688商品二级类目
                                if (catePath.size() > 2) {
                                    sheetData.put(10, catePath.get(catePath.size() - 3));
                                }
                                //11: 1688商品三级类目
                                if (catePath.size() > 3) {
                                    sheetData.put(11, catePath.get(catePath.size() - 4));
                                }
                            }
                        }
                    }


                    SmCommonResultDO<ItemModel> itemModels = itemQueryService.queryItemModel(item.getItemId());
                    if (itemModels != null && itemModels.isSuccess() && itemModels.getResultObject() != null) {
                        if (MapUtils.isNotEmpty(tbCategoryMaps)) {
                            Long itemCatId = itemModels.getResultObject().getCategoryId();
                            StdCategoryModel stdCategoryModel = new StdCategoryModel();
                            stdCategoryModel = tbCategoryMaps.get(itemCatId == null ? 0 : itemCatId.intValue());
                            List<String> catePath = new ArrayList<String>();
                            while (stdCategoryModel != null) {
                                catePath.add(stdCategoryModel.getName());
                                stdCategoryModel = tbCategoryMaps.get(stdCategoryModel.getParentId());
                            }
                            //16:TB商品一级类目
                            if (CollectionUtils.isNotEmpty(catePath)) {
                                String leafCateName = null;
                                if (catePath.size() > 1) {
                                    sheetData.put(16, catePath.get(catePath.size() - 2));
                                    leafCateName = catePath.get(catePath.size() - 2);
                                }
                                //17:TB商品二级类目
                                if (catePath.size() > 2) {
                                    sheetData.put(17, catePath.get(catePath.size() - 3));
                                    leafCateName = catePath.get(catePath.size() - 3);
                                }
                                //18:TB商品三级类目
                                if (catePath.size() > 3) {
                                    sheetData.put(18, catePath.get(catePath.size() - 4));
                                    leafCateName = catePath.get(catePath.size() - 4);
                                }
                                sheetData.put(19, leafCateName);
                            }
                        }
                    }
                    //23:佣金比例
                    if (item.getCommission() != null) {
                        sheetData.put(23, item.getCommission().getCommissionText());
                    }
                    //21:首次报名时间
                    if (item.getGmtCreate() != null) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(item.getGmtCreate());
                        sheetData.put(24, String.valueOf(calendar.get(Calendar.YEAR)));
                        sheetData.put(25, String.valueOf(calendar.get(Calendar.MONTH) + 1));
                        sheetData.put(26, String.valueOf(calendar.get(Calendar.DATE)));
                    }

                    //22:审核状态
                    String status = item.getAuditStatus();
                    if (StringUtils.isEmpty(status)) {
                        sheetData.put(27, "待审核");
                    } else {
                        DcAuditStatusEnum statusEnum = DcAuditStatusEnum.valueOf(status);
                        if (statusEnum != null) {
                            sheetData.put(27, statusEnum.getDesc());
                        }
                    }

                    //23:是否已缴纳保证金
                    if (memberAudit.containsKey(offer.getOffer().getMemberID() + item.getChannel())) {
                        sheetData.put(28, memberAudit.get(offer.getOffer().getMemberID() + item.getChannel()));
                    } else {
                        boolean isJoined = dcSupplierService.isSupplierJoin(offer.getOffer().getMemberID(), item.getChannel());
                        if (isJoined) {
                            sheetData.put(28, "是");
                            memberAudit.put(offer.getOffer().getMemberID() + item.getChannel(), "是");
                        } else {
                            sheetData.put(28, "否");
                            memberAudit.put(offer.getOffer().getMemberID() + item.getChannel(), "否");
                        }
                    }


                    ListOf<DcAuditInfoRecord> records = dcAuditService.listAuditInfo(item.getSupplierUserId(), item.getEnrollId(), true);
                    if (records != null && records.isSuccess() && CollectionUtils.isNotEmpty(records.getData())) {
                        List<DcAuditInfoRecord> recList = records.getData();
                        List<DcAuditInfoRecord> recordList = new ArrayList<>();
                        for (DcAuditInfoRecord dcAuditInfoRecord: recList) {
                            if (dcAuditInfoRecord != null && (DcAuditStatusEnum.WAIT.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                DcAuditStatusEnum.FIRST_APPROVE.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                DcAuditStatusEnum.FIRST_REJECT.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                DcAuditStatusEnum.SECOND_APPROVE.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                DcAuditStatusEnum.SECOND_REJECT.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                DcAuditStatusEnum.REJECT.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                DcAuditStatusEnum.THIRD_APPROVE.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                DcAuditStatusEnum.THIRD_REJECT.getValue().equals(dcAuditInfoRecord.getAuditType())) &&
                                StringUtils.isNotEmpty(dcAuditInfoRecord.getOperator())) {
                                recordList.add(dcAuditInfoRecord);
                            }
                        }
                        // 筛掉待审核状态
                        List<DcAuditInfoRecord> auditInfoList = new ArrayList<>();
                        if (CollectionUtils.isNotEmpty(recordList)) {
                            Collections.reverse(recordList);
                            for (DcAuditInfoRecord dcAuditInfoRecord: recordList) {
                                if (dcAuditInfoRecord != null && (DcAuditStatusEnum.FIRST_APPROVE.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                    DcAuditStatusEnum.FIRST_REJECT.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                    DcAuditStatusEnum.SECOND_APPROVE.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                    DcAuditStatusEnum.SECOND_REJECT.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                    DcAuditStatusEnum.REJECT.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                    DcAuditStatusEnum.THIRD_APPROVE.getValue().equals(dcAuditInfoRecord.getAuditType()) ||
                                    DcAuditStatusEnum.THIRD_REJECT.getValue().equals(dcAuditInfoRecord.getAuditType()))) {
                                    auditInfoList.add(dcAuditInfoRecord);
                                }
                            }
                        }
                        if (CollectionUtils.isNotEmpty(auditInfoList) && auditInfoList.get(0) != null) {
                            //26:最后一次审核小二
                            DcAuditInfoRecord dcAuditInfoRecord = auditInfoList.get(0);
                            sheetData.put(29, dcAuditInfoRecord.getOperatorName());
                            //27:最后一次审核时间
                            if (dcAuditInfoRecord.getGmtCreate() != null) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(dcAuditInfoRecord.getGmtCreate());
                                sheetData.put(30, String.valueOf(calendar.get(Calendar.YEAR)));
                                sheetData.put(31, String.valueOf(calendar.get(Calendar.MONTH) + 1));
                                sheetData.put(32, String.valueOf(calendar.get(Calendar.DATE)));
                            }
                        }
                        if (CollectionUtils.isNotEmpty(auditInfoList)) {
                            for (DcAuditInfoRecord record : auditInfoList) {
                                if (record != null && (DcAuditStatusEnum.FIRST_REJECT.getValue().equals(record.getAuditType()) ||
                                    DcAuditStatusEnum.SECOND_REJECT.getValue().equals(record.getAuditType()) ||
                                    DcAuditStatusEnum.REJECT.getValue().equals(record.getAuditType()) ||
                                    DcAuditStatusEnum.THIRD_REJECT.getValue().equals(record.getAuditType()))
                                    && StringUtils.isNotEmpty(record.getReason())) {
                                    DcAuditTypeEnum auditType = DcAuditTypeEnum.safeValueOf(record.getReasonType());
                                    //格式化的原因
                                    if (auditType != null) {
                                        sheetData.put(33, auditType.getDesc());
                                    }
                                    //具体原因
                                    sheetData.put(34, record.getReason());
                                    break;
                                }
                            }
                        }
                    }
                    int count = 0;
                    for (DcOfferSkuComplexVO offerSkuComplexVO: item.getOfferSkuList()) {
                        if (offerSkuComplexVO == null || offerSkuComplexVO.getOfferSku() == null ||
                            offerSkuComplexVO.getItemSku() == null) {
                            continue;
                        }
                        Map<Integer, String> skuSheetData = new HashMap<Integer, String>();
                        if (count == 0) {
                            skuSheetData.putAll(sheetData);
                        }
                        count++;
                        //12:1688商品SKU名称
                        List<String> names = new ArrayList<>();
                        DcOfferSku dcOfferSku = offerSkuComplexVO.getOfferSku();
                        String sku1688Id = "";
                        if (dcOfferSku != null) {
                            sku1688Id = String.valueOf(dcOfferSku.getSkuId());
                            if (CollectionUtils.isNotEmpty(dcOfferSku.getAttrList())) {
                                for (DcAttrModel attrModel : dcOfferSku.getAttrList()) {
                                    names.add(attrModel.getValueText());
                                }
                            }
                        }
                        List<String> tbNames = new ArrayList<>();
                        DcItemSku dcItemSku = offerSkuComplexVO.getItemSku();
                        String taoSkuId = "";
                        if (dcItemSku != null) {
                            taoSkuId = String.valueOf(dcItemSku.getSkuId());
                            if (CollectionUtils.isNotEmpty(dcItemSku.getAttrList())) {
                                for (DcAttrModel attrModel : dcItemSku.getAttrList()) {
                                    tbNames.add(attrModel.getValueText());
                                }
                            }
                        }
                        if (CollectionUtils.isNotEmpty(names)) {
                            skuSheetData.put(12, StringUtils.join(names, ","));
                        }
                        //10:1688 sku id
                        skuSheetData.put(13, String.valueOf(sku1688Id));
                        //11:淘宝sku
                        if (CollectionUtils.isNotEmpty(tbNames)) {
                            skuSheetData.put(14, StringUtils.join(tbNames, ","));
                        }
                        //12:淘宝SKUid
                        skuSheetData.put(15, String.valueOf(taoSkuId));
                        //17:渠道库存
                        if (null != offerSkuComplexVO.getChannelInventory()) {
                            skuSheetData.put(20, String.valueOf(offerSkuComplexVO.getChannelInventory()));
                        }
                        //18:淘系零售价
                        if (offerSkuComplexVO.getSupplyPrice() != null) {
                            skuSheetData.put(21, offerSkuComplexVO.getSupplyPrice().toString());
                        }
                        //19:划线价 暂定不知道
                        if (offerSkuComplexVO.getRetailPrice() != null) {
                            skuSheetData.put(22, offerSkuComplexVO.getRetailPrice().toString());
                        }
                        sheetDatas.add(skuSheetData);

                    }
                }
                if(!CollectionUtils.isEmpty(sheetDatas)){
                    String filePath = null;
                    try {
                        //文件名称
                        String fileName = "商品通导出" + System.currentTimeMillis();
                        filePath = TempFileUtil.createTempXlsxFile(fileName);
                        FileOutputStream bOutput = new FileOutputStream(filePath);
                        //写入内存
                        writeFile(bOutput,sheetDatas);

                        dataShareFileService.uploadFile(fileName, filePath, Lists.newArrayList(empNo));
                    } catch (IOException ioException) {
                        log.error("create excel or upload file error", ioException);
                    } finally {
                        File file = new File(filePath);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }catch(Exception e){
                log.error("create excel or upload file error", e);
            }finally {
                setStart(true,false);
            }
        }

        private void sleep100(){
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void writeFile(OutputStream out, List<Map<Integer, String>> sheetDatas) {
            ExcelWriter writer = EasyExcelFactory.getWriter(out);
            //写第一个sheet, sheet1  数据全是List<String> 无模型映射关系
            Sheet sheet1 = new Sheet(1, 3);
            sheet1.setSheetName("提报商品信息");
            //设置列宽 设置每列的宽度
            //        Map columnWidth = new HashMap();
            //        columnWidth.put(0,10000);columnWidth.put(1,40000);columnWidth.put(2,10000);columnWidth.put(3,10000);
            //        sheet1.setColumnWidthMap(columnWidth);
            sheet1.setHead(createSheetHead());
            //or 设置自适应宽度
            sheet1.setAutoWidth(Boolean.TRUE);
            writer.write1(getSheetData(sheetDatas), sheet1);
            //关闭资源
            writer.finish();
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    log.error("close excel error", e.getMessage(), e);
                }
            }
        }
        private List<List<String>> createSheetHead() {
            List<List<String>> result = new ArrayList<List<String>>();
            for (String head : easyExcelHead) {
                List<String> headn = Lists.newArrayList(head);
                result.add(headn);
            }
            return result;
        }

        private List<List<Object>> getSheetData(List<Map<Integer, String>> sheetDatas) {
            List result = Lists.newArrayList();
            if (org.springframework.util.CollectionUtils.isEmpty(sheetDatas)) {
                return result;
            }
            for (Map<Integer, String> rowMap : sheetDatas) {
                List<Object> rowData = Lists.newArrayList();
                for(int i=1;i<=easyExcelHead.length;i++){
                    rowData.add(rowMap.get(i));
                }
                result.add(rowData);
            }
            return result;
        }
    }


}
