package com.alibaba.cbuscm.controller.disco;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.business.tao.api.TaoOrderWriteService;
import com.alibaba.cbu.disco.shared.common.constant.ErrorCodeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcChannelInfoService;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcMultiUserModel;
import com.alibaba.cbuscm.logic.DiscoLogic;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * TaoOrderWriteController
 * 批量对采购单执行1688下单操作
 *
 * @author aniu.nxh
 * @date 2019/07/12 17:57
 */
@RestController
@RequestMapping(value = "/taoOrderWrite", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class TaoOrderWriteController {

    @Autowired
    private TaoOrderWriteService taoOrderWriteService;
    @Autowired
    private DcChannelInfoService dcChannelInfoService;
    @Autowired
    private DiscoLogic discoLogic;

    @ResponseBody
    @RequestMapping(value = "/batchMakeCbuOrder", method = RequestMethod.GET)
    public ListOf<ResultOf<Boolean>> batchMakeCbuOrder(HttpServletRequest request,
                                                        @RequestParam(name = "channel") String channel,
                                                        @RequestParam(name = "purchaseMap") String purchaseMapText) {

        String logPrefix = "TaoOrderWriteController#batchMakeCbuOrder#";
        String paramInfo = "#c:" + channel + "#purchaseMap:" + purchaseMapText + "#";

        log.error(logPrefix + "record" + paramInfo);

        ListOf<ResultOf<Boolean>> result = new ListOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                if (StringUtils.isBlank(purchaseMapText)) {
                    result.setErrorCode("Purchase_Empty");
                    result.setErrorMessage("没有选中采购单");
                    return result;
                }

                // purchaseId: channelOrderId
                Map<Long, String> purchaseMap = JSON.parseObject(purchaseMapText,
                    new TypeReference<Map<Long, String>>() {});

                ResultOf<DcMultiUserModel> channelResult = dcChannelInfoService.fetchMultiModelByChannel(channel);
                if (channelResult == null || !channelResult.isSuccess() || channelResult.getData() == null) {
                    result.setErrorCode("channel_Illegal");
                    result.setErrorMessage("渠道异常");
                    return result;
                }

                String cbuBuyerIdText = channelResult.getData().getCbuBuyerId();
                if (StringUtils.isBlank(cbuBuyerIdText)) {
                    result.setErrorCode("channel_Illegal");
                    result.setErrorMessage("渠道异常");
                    return result;
                }

                if (MapUtils.isEmpty(purchaseMap)) {
                    result.setErrorCode("Purchase_Empty");
                    result.setErrorMessage("采购单为空");
                    return result;
                }

                // Long userId = 3687900876L;
                Long cbuBuyerId = Long.valueOf(cbuBuyerIdText);

                result = new ListOf<>();
                List<ResultOf<Boolean>> resultList = new ArrayList<ResultOf<Boolean>>();
                result.setData(resultList);

                boolean isAllSuccess = true;
                StringBuilder sb = new StringBuilder();

                for (Long purchaseId : purchaseMap.keySet()) {
                    if (purchaseId == null) {
                        continue;
                    }
                    String channelOrderId = purchaseMap.get(purchaseId);

                    ResultOf<Boolean> curResult = taoOrderWriteService.makeCbuOrder(channel, cbuBuyerId, purchaseId,
                        channelOrderId);
                    resultList.add(curResult);

                    if (!curResult.isSuccess()) {
                        isAllSuccess = false;
                        sb.append(purchaseId + ":" + curResult.getErrorMessage() + ";");
                    }
                }
                if (isAllSuccess) {
                    result.setSuccess(true);
                } else {
                    result.setErrorCode("Not_All_Success");
                    result.setErrorMessage(sb.toString());
                }
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/makeCbuOrder", method = RequestMethod.GET)
    public ResultOf<Boolean> makeCbuOrder(HttpServletRequest request, @RequestParam(name = "channel") String channel,
                                            @RequestParam(name = "purchaseId") Long purchaseId,
                                            @RequestParam(name = "channelOrderId") String channelOrderId) {

        String logPrefix = "TaoOrderWriteController#makeCbuOrder#";
        String paramInfo = "#c:" + channel + "#pid:" + purchaseId + "#channelOrderId:" + channelOrderId + "#";

        log.error(logPrefix + "record" + paramInfo);

        ResultOf<Boolean> result = new ResultOf<>();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                ResultOf<DcMultiUserModel> channelResult = dcChannelInfoService.fetchMultiModelByChannel(channel);
                if (channelResult == null || !channelResult.isSuccess() || channelResult.getData() == null) {
                    result.setErrorCode("channel_Illegal");
                    result.setErrorMessage("渠道异常");
                    return result;
                }

                String cbuBuyerIdText = channelResult.getData().getCbuBuyerId();
                if (StringUtils.isBlank(cbuBuyerIdText)) {
                    result.setErrorCode("channel_Illegal");
                    result.setErrorMessage("渠道异常");
                    return result;
                }

                // Long userId = 3687900876L;
                Long cbuBuyerId = Long.valueOf(cbuBuyerIdText);
                result = taoOrderWriteService.makeCbuOrder(channel, cbuBuyerId, purchaseId, channelOrderId);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMessage(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

}
