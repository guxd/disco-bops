package com.alibaba.cbuscm.controller.disco;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.constant.ErrorCodeEnum;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.enroll.api.DcChannelInfoService;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcEmpModel;
import com.alibaba.cbu.disco.shared.core.enroll.model.data.DcMultiUserModel;
import com.alibaba.cbu.disco.shared.core.order.api.FsOrderInfoReadService;
import com.alibaba.cbu.disco.shared.core.order.model.view.FsPurchaseListCondition;
import com.alibaba.cbu.disco.shared.core.order.model.view.FsPurchaseListResult;
import com.alibaba.cbuscm.logic.DiscoLogic;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.member.service.models.MemberModel;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * FsOrderInfoReadController
 *
 * @author aniu.nxh
 * @date 2019/07/12 16:30
 */
@RestController
@RequestMapping(value = "/fsOrderInfoRead", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class FsOrderInfoReadController {

    @Autowired
    private FsOrderInfoReadService fsOrderInfoReadService;
    @Autowired
    private DcChannelInfoService dcChannelInfoService;
    @Autowired
    private MemberReadService memberReadService;
    @Autowired
    private DiscoLogic discoLogic;

    @ResponseBody
    @RequestMapping(value = "/listPurchasePageInfo", method = RequestMethod.GET)
    public FsPurchaseListResult listPurchasePageInfo(HttpServletRequest request,
                                                        @RequestParam(name = "channel") String channel,
                                                        @RequestParam(name = "pageIdx") Long pageIdx,
                                                        @RequestParam(name = "pageSize") Long pageSize,
                                                        @RequestParam(name = "supplierLoginId") String supplierLoginId,
                                                        @RequestParam(name = "channelOrderId") String channelOrderId) {

        String logPrefix = "FsOrderInfoReadController#listPurchasePageInfo#";
        String paramInfo = "#c:" + channel + "#pageIdx:" + pageIdx + "#pageSize:" + pageSize + "#supplierLoginId:" +
            supplierLoginId + "#channelOrderId:" + channelOrderId + "#";

        log.error(logPrefix + "record" + paramInfo);

        FsPurchaseListResult result = new FsPurchaseListResult();
        result.setSuccess(false);
        try {
            // 获取dcEmpModel
            ResultOf<DcEmpModel> resultOf = discoLogic.fetchEmpModel(request);
            if (resultOf.isSuccess() && resultOf.getData() != null) {
                ResultOf<DcMultiUserModel> channelResult = dcChannelInfoService.fetchMultiModelByChannel(channel);
                if (channelResult == null || !channelResult.isSuccess() || channelResult.getData() == null) {
                    result.setErrorCode("channel_Illegal");
                    result.setErrorMsg("渠道异常");
                    return result;
                }
                String purchaseUserId = channelResult.getData().getPurchaseUserId();

                FsPurchaseListCondition condition = new FsPurchaseListCondition();
                condition.setCreatePerform(false);

                if (StringUtils.isNotBlank(supplierLoginId)) {
                    MemberModel memberModel = memberReadService.findMemberByLoginId(supplierLoginId);
                    if (memberModel == null) {
                        result.setSuccess(true);
                        result.setCount(0L);
                        return result;
                    }
                    condition.setSupplierUserId(memberModel.getUserId().toString());
                }
                if (StringUtils.isNotBlank(channelOrderId)) {
                    condition.setChannelOrderId(channelOrderId);
                }
                // 展示采购单列表 该接口由AE和淘宝复用
                result = fsOrderInfoReadService.listPurchasePageInfo(channel, purchaseUserId, pageSize, pageIdx,
                    condition);
            } else {
                result.setErrorCode(resultOf.getErrorCode());
                result.setErrorMsg(resultOf.getErrorMessage());
                return result;
            }
        } catch (Exception e) {
            log.error(logPrefix + ErrorCodeEnum.EXCEPTION.getValue() + paramInfo, e);
        }
        return result;
    }

}
