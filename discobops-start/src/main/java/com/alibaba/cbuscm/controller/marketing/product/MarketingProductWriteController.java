package com.alibaba.cbuscm.controller.marketing.product;

import com.alibaba.cbu.panama.dc.client.model.param.marketingproduct.ModifyPaymentInfoParam;
import com.alibaba.cbu.panama.dc.client.model.param.marketingproduct.RejectMarketingProductParam;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.RestResult;
import com.alibaba.cbuscm.enums.RemarkEnum;
import com.alibaba.cbuscm.service.marketing.product.MarketingProductWriteAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * @Auther: gxd
 * @Date: 2019/10/8 10:48
 * @Description:
 */
@RestController
@RequestMapping(path = "/marketingproduct/write", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MarketingProductWriteController {


    @Autowired
    private MarketingProductWriteAdapter marketingProductWriteAdapter;

    /**
     * 缴纳提交
     * @return
     */
    @GetMapping("/configPayment")
    public RestResult<Boolean> configPayment(@RequestParam Long marketingProductId,
                                             @RequestParam String chargeFee){

//        String empId = BopsLogUtil.getEmpId();
        //nickName花名
        String nickNamecn = BopsLogUtil.getNickNamecn();
        ModifyPaymentInfoParam paymentInfoParam = new ModifyPaymentInfoParam();

        paymentInfoParam.setMarketingProduceId(marketingProductId);
        paymentInfoParam.setOperator(nickNamecn);
        paymentInfoParam.setChargeFee(yuan2Fen(chargeFee));

        return RestResult.success(marketingProductWriteAdapter.configPayment(paymentInfoParam));

    }

    /**
     * 运营确认打款信息
     * @param marketingProductId id
     * @return
     */
    @GetMapping("/payConfirm")
    public RestResult<Boolean> payConfirm(@RequestParam Long marketingProductId){
//        String empId = BopsLogUtil.getEmpId();
        //nickName花名
        String nickNamecn = BopsLogUtil.getNickNamecn();
        if(Objects.isNull(marketingProductId)){
            return RestResult.fail("001","marketingPlanId为空");
        }

        return RestResult.success(marketingProductWriteAdapter.paymentConfirmByOwner(marketingProductId,nickNamecn));


    }


    /**
     * 第一次审核通过
     * @param marketingProductId
     * @return
     */
    @GetMapping("/firstAudit")
    public RestResult<Boolean> firstAudit(@RequestParam Long marketingProductId){

//        String empId = BopsLogUtil.getEmpId();
        //nickName花名
        String nickNamecn = BopsLogUtil.getNickNamecn();

        if(Objects.isNull(marketingProductId)){
            return RestResult.fail("001","marketingPlanId为空");
        }
        return RestResult.success(marketingProductWriteAdapter.firstAudit(marketingProductId,nickNamecn, RemarkEnum.FIRST_AUDIT.getDesc()));


    }

    /**
     * 第二次审核通过
     * @param marketingProductId
     * @return
     */
    @GetMapping("/secondAudit")
    public RestResult<Boolean> secondAudit(@RequestParam Long marketingProductId){

//        String empId = BopsLogUtil.getEmpId();
        //nickName花名
        String nickNamecn = BopsLogUtil.getNickNamecn();
        if(Objects.isNull(marketingProductId)){
            return RestResult.fail("001","marketingPlanId为空");
        }
        return RestResult.success(marketingProductWriteAdapter.secondAudit(marketingProductId,nickNamecn, RemarkEnum.SECOND_AUDIT.getDesc()));
    }

    /**
     * 审核驳回
     * @return
     */
    @GetMapping("/reject")
    public RestResult<Boolean> reject(@RequestParam Long marketingProductId,
            @RequestParam(required = false) String remark,@RequestParam String code){
//        String empId = BopsLogUtil.getEmpId();
        //nickName花名
        String nickNamecn = BopsLogUtil.getNickNamecn();
        RejectMarketingProductParam param = new RejectMarketingProductParam();
        param.setMarketingProduceId(marketingProductId);
        param.setReasonCode(code);
        param.setRemark(remark);
        param.setOperator(nickNamecn);

        return RestResult.success(marketingProductWriteAdapter.reject(param));
    }

    @GetMapping("/configSale")
    public RestResult<Boolean> configSale(
                                          @RequestParam Long marketingProductId,
                                          @RequestParam Integer guaranteedSales,
                                          @RequestParam Integer expectedSales){

        if(Objects.isNull(marketingProductId)){
            return RestResult.fail("001","marketingPlanId为空");
        }

        if(Objects.isNull(guaranteedSales)){
            return RestResult.fail("001","guaranteedSales为空");
        }

        if(Objects.isNull(guaranteedSales)){
            return RestResult.fail("001","guaranteedSales为空");
        }

        return RestResult.success(marketingProductWriteAdapter.configSale(marketingProductId,guaranteedSales,expectedSales));

    }


    public static Integer yuan2Fen(BigDecimal yuan) {
        return Optional.ofNullable(yuan)
                .map(f -> f.movePointRight(2))
                .map(BigDecimal::intValue)
                .orElse(null);
    }

    public static Integer yuan2Fen(String yuan) {
        try {
            BigDecimal bd = new BigDecimal(yuan);
            return yuan2Fen(bd);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
