package com.alibaba.cbuscm.controller.onepiece.selection;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.interfaces.onepiece.authority.AuthorityService;
import com.alibaba.china.global.business.library.models.onepiece.authority.OnePieceAuthException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 权限判断拦截器
 * @author lichao.wlc
 * @date 2019/06/20
 */
@Slf4j
@AllArgsConstructor
public class AuthInterceptor implements HandlerInterceptor {
    private AuthorityService authorityService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
//        try {
//            if (!RequestMethod.GET.name().equalsIgnoreCase(httpServletRequest.getMethod())) {
//                return true;
//            }
//            String id = httpServletRequest.getParameter("id");
//            String taskId = httpServletRequest.getParameter("taskId");
//            if(StringUtils.isEmpty(id) && StringUtils.isEmpty(taskId)) {
//                throw new OnePieceAuthException("No permissions");
//            }
//            String empId = SimpleUserUtil.getBucSSOUser(httpServletRequest).getEmpId();
//            String queryId = StringUtils.isEmpty(taskId) ? id : taskId;
//            log.info("AuthInterceptor preHandle empId:{}, queryId:{}", empId, queryId);
//            ResultOf<Map<String,Object>> resultOf = authorityService.checkTaskAuthority(empId, Long.parseLong(queryId));
//            if(resultOf.isSuccess()) {
//                Map<String,Object> resultMap = resultOf.getData();
//                Boolean hasAdminAuth = (Boolean)resultMap.get("taskAdminAuth");
//                Boolean hasTaskTypeAuth = (Boolean)resultMap.get("taskTypeAuth");
//                if(!hasTaskTypeAuth) {
//                    throw new OnePieceAuthException("You do not have permission for this task type");
//                }else if(hasTaskTypeAuth && !hasAdminAuth) {
//                    throw new OnePieceAuthException("You're not the task administrator");
//                }
//            }else {
//                throw new OnePieceAuthException("No permissions");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error("-----------出现异常，原因为：{}-------------",e);
//        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o,
                           ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {

    }
}
