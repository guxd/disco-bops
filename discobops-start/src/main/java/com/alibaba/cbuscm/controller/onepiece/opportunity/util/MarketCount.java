package com.alibaba.cbuscm.controller.onepiece.opportunity.util;

import lombok.*;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MarketCount {

    Integer total;

}
