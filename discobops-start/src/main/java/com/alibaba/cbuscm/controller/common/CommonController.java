package com.alibaba.cbuscm.controller.common;


import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.china.global.business.library.models.onepiece.authority.UserModel;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.RPCResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * one piece通用接口
 *
 * @author lichao.wlc
 * @date 2019/06/12
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController {

    /**
     * 任务下游指标结果
     *
     * @return
     */
    @RequestMapping("/user")
    public RPCResult user(HttpServletRequest request) {
        try {
            BucSSOUser bucSSOUser = SimpleUserUtil.getBucSSOUser(request);

            if (bucSSOUser==null) {
                return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "bucSSOUser is null");
            } else if (bucSSOUser.getEmpId() == null) {
                return RPCResult.error(RPCResult.CODE_SERVER_ERROR,  JSON.toJSONString(bucSSOUser));
            }
            String avatarUrl = String.format("https://work.alibaba-inc.com/photo/%s.200x200.jpg", bucSSOUser.getEmpId());
            UserModel userModel = UserModel.builder().jobNumber(bucSSOUser.getEmpId()).userName(bucSSOUser.getNickNameCn()).avatarUrl(avatarUrl).build();
            return RPCResult.ok(userModel);
        } catch (Exception e) {
            log.error("user error", e);
        }
        return RPCResult.error(RPCResult.CODE_SERVER_ERROR, "user error");
    }
}
