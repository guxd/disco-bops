package com.alibaba.cbuscm.controller.international.ae;

import com.alibaba.cbuscm.utils.DateUtils;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.international.ae.AeShopSelectionConfigVO;
import com.alibaba.china.global.business.library.common.PageOf;
import com.alibaba.china.global.business.library.common.ResultOf;
import com.alibaba.china.global.business.library.interfaces.selection.aeshop.AeShopSelectionTaskReadService;
import com.alibaba.china.global.business.library.interfaces.selection.aeshop.AeShopSelectionTaskWriteService;
import com.alibaba.china.global.business.library.models.selection.SelectionAdminModel;
import com.alibaba.china.global.business.library.models.selection.aeshop.AeShopSelectionConfigModel;
import com.alibaba.china.global.business.library.models.selection.aeshop.AeShopSelectionTaskModel;
import com.alibaba.china.global.business.library.params.selection.aeshop.AeShopSelectionConfigParam;
import com.alibaba.china.global.business.library.params.selection.aeshop.AeShopSelectionTaskOperateParam;
import com.alibaba.china.global.business.library.params.selection.aeshop.AeShopSelectionTaskPostParam;
import com.alibaba.china.global.business.library.params.selection.aeshop.AeShopSelectionTaskQueryParam;
import com.alibaba.security.spring.component.csrf.CsrfTokenModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static com.alibaba.cbuscm.utils.AssertUtils.asserts;
import static com.alibaba.cbuscm.utils.CollectionUtils.any;

/**
 * Ae shop selection task controller.
 *
 * @author zhuoxian
 */
@RestController
@CrossOrigin("*")
@CsrfTokenModel
@RequestMapping(path = "/api/ae-shop/selection", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class AeShopSelectionTaskController {

    private static final String ERR_TASK_QUERY = "ERR_TASK_QUERY";
    private static final String ERR_TASK_CREATE = "ERR_TASK_CREATE";
    private static final String ERR_TASK_UPDATE = "ERR_TASK_UPDATE";
    private static final String ERR_TASK_COPY = "ERR_TASK_COPY";
    private static final String ERR_TASK_PUBLISH = "ERR_TASK_PUBLISH";
    private static final String ERR_TASK_PAUSE = "ERR_TASK_PAUSE";
    private static final String ERR_TASK_CONFIG = "ERR_TASK_CONFIG";
    private static final String ERR_TASK_ESTIMATE = "ERR_TASK_ESTIMATE";

    @Autowired
    private AeShopSelectionTaskReadService aeShopSelectionTaskReadService;

    @Autowired
    private AeShopSelectionTaskWriteService aeShopSelectionTaskWriteService;

    @ResponseBody
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public Object getTasks(
            @RequestParam(value = "startTime", required = false) String startTime,
            @RequestParam(value = "endTime", required = false) String endTime,
            @RequestParam(value = "taskStatus", required = false) String taskStatus,
            @RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
            HttpServletRequest request) {

        try {
            return aeShopSelectionTaskReadService.query(AeShopSelectionTaskQueryParam.builder()
                    .adminId(LoginUtil.getEmpId(request))
                    .timeBegin(DateUtils.parseDateTime(startTime))
                    .timeEnd(DateUtils.parseDateTime(endTime))
                    .status(taskStatus)
                    .pageNo(pageNo)
                    .pageSize(pageSize)
                    .build());
        } catch (Throwable err) {
            log.error(ERR_TASK_QUERY,err.getMessage() ,err);
            return PageOf.error(ERR_TASK_QUERY, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(value = "/tasks",method = RequestMethod.POST)
    public Object postTask(@RequestBody AeShopSelectionTaskPostParam param, HttpServletRequest request) {
        try {
            // ensure current user is in admin list
            String adminId = LoginUtil.getEmpId(request);
            if (!any(param.getAdmins(), it -> adminId.equals(it.getAdminId()))) {
                String adminName = LoginUtil.getLoginName(request);
                param.getAdmins().add(0, new SelectionAdminModel(adminId, adminName));
            }

            // invoke service to create task
            return aeShopSelectionTaskWriteService.create(param);

        } catch (Throwable err) {
            log.error("{}, param={}", ERR_TASK_CREATE, param, err);
            return ResultOf.error(ERR_TASK_CREATE, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/task", method = RequestMethod.GET)
    public Object getTask(@RequestParam Long taskId, HttpServletRequest request) {

        String adminId = LoginUtil.getEmpId(request);
        log.info("taskId={}, adminId={}", taskId, adminId);

        try {
            return aeShopSelectionTaskReadService.findById(
                AeShopSelectionTaskQueryParam.builder()
                    .taskId(taskId)
                    .adminId(adminId)
                    .build());
        } catch (Throwable err) {
            log.error("{}, taskId={}", ERR_TASK_QUERY, taskId, err.getMessage() , err);
            return ResultOf.error(ERR_TASK_QUERY, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(value = "/task",method = RequestMethod.PUT)
    public Object putTask(@RequestBody AeShopSelectionTaskPostParam param, HttpServletRequest request) {
        try {
            // ensure current user is in admin list
            String adminId = LoginUtil.getEmpId(request);
            if (!any(param.getAdmins(), it -> adminId.equals(it.getAdminId()))) {
                String adminName = LoginUtil.getLoginName(request);
                param.getAdmins().add(0, new SelectionAdminModel(adminId, adminName));
            }

            // invoke service to create task
            return aeShopSelectionTaskWriteService.update(param);
        } catch (Throwable err) {
            log.error("{}, param={}", ERR_TASK_UPDATE, param, err.getMessage() , err);
            return ResultOf.error(ERR_TASK_UPDATE, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/task/copy", method = RequestMethod.POST)
    public Object copyTask(@RequestBody TaskIdentity taskIdentity, HttpServletRequest request) {

        String adminId = LoginUtil.getEmpId(request);
        try {
            // find task
            ResultOf<AeShopSelectionTaskModel> findResult = aeShopSelectionTaskReadService.findById(
                AeShopSelectionTaskQueryParam.builder()
                    .taskId(taskIdentity.getTaskId())
                    .adminId(adminId)
                    .build());
            asserts(ResultOf.isValid(findResult), "Task not found.");

            AeShopSelectionTaskModel taskModel = findResult.getData();
            AeShopSelectionConfigModel configModel = taskModel.getConfig();

            // ensure current user is in admin list
            List<SelectionAdminModel> admins = taskModel.getAdmins();

            if (!any(admins, it -> adminId.equals(it.getAdminId()))) {
                String adminName = LoginUtil.getLoginName(request);
                admins.add(0, new SelectionAdminModel(adminId, adminName));
            }

            // do create new task
            ResultOf<AeShopSelectionTaskModel> createResult = aeShopSelectionTaskWriteService.create(
                AeShopSelectionTaskPostParam.builder()
                    .taskType(taskModel.getTaskType())
                    .taskName(taskModel.getTaskName())
                    .taskDesc(taskModel.getTaskDesc())
                    .admins(admins)
                    .build());
            asserts(ResultOf.isValid(createResult), "Fail creating task.");

            // build config param
            if (configModel != null) {
                AeShopSelectionConfigParam configParam = AeShopSelectionConfigParam.builder()
                    .taskId(createResult.getData().getTaskId())
                    .adminId(adminId)
                    .configModel(configModel)
                    .build();

                // set config
                ResultOf<AeShopSelectionConfigModel> updateResult = aeShopSelectionTaskWriteService.updateConfig(configParam);
                asserts(ResultOf.isValid(updateResult), "Fail updating task config.");
            }

            return createResult;

        } catch (Throwable err) {
            log.error("{}, taskIdentity={}, adminId={}", ERR_TASK_COPY, taskIdentity, adminId, err.getMessage() , err);
            return ResultOf.error(ERR_TASK_COPY, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/task/publish", method = RequestMethod.POST)
    public Object publishTask(@RequestBody TaskIdentity taskIdentity, HttpServletRequest request) {
        String adminId = LoginUtil.getEmpId(request);
        try {
            // update publish status
            ResultOf<Boolean> opResult = aeShopSelectionTaskWriteService.publish(
                AeShopSelectionTaskOperateParam.builder()
                    .taskId(taskIdentity.getTaskId())
                    .adminId(adminId)
                    .build());

            // ensure update success
            if (!(ResultOf.isValid(opResult) && Boolean.TRUE.equals(opResult.getData()))) {
                return opResult;
            }

            // return task model
            return aeShopSelectionTaskReadService.findById(
                AeShopSelectionTaskQueryParam.builder().taskId(taskIdentity.getTaskId()).adminId(adminId).build());

        } catch (Throwable err) {
            log.error("{}, taskIdentity={}, adminId={}", ERR_TASK_PUBLISH, taskIdentity, adminId, err.getMessage() , err);
            return ResultOf.error(ERR_TASK_PUBLISH, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/task/pause", method = RequestMethod.POST)
    public Object pauseTask(@RequestBody TaskIdentity taskIdentity, HttpServletRequest request) {
        String adminId = LoginUtil.getEmpId(request);
        try {
            // update publish status
            ResultOf<Boolean> opResult = aeShopSelectionTaskWriteService.unpublish(
                AeShopSelectionTaskOperateParam.builder()
                    .taskId(taskIdentity.getTaskId())
                    .adminId(adminId)
                    .build());

            // ensure update success
            if (!(ResultOf.isValid(opResult) && Boolean.TRUE.equals(opResult.getData()))) {
                return opResult;
            }

            // return task model
            return aeShopSelectionTaskReadService.findById(
                AeShopSelectionTaskQueryParam.builder().taskId(taskIdentity.getTaskId()).adminId(adminId).build());

        } catch (Throwable err) {
            log.error("{}, taskIdentity={}, adminId={}", ERR_TASK_PAUSE, taskIdentity, adminId, err.getMessage()  , err);
            return ResultOf.error(ERR_TASK_PAUSE, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/task/config", method = RequestMethod.GET)
    public Object getTaskConfig(@RequestParam Long taskId, HttpServletRequest request) {

        String adminId = LoginUtil.getEmpId(request);
        log.info("taskId={}, adminId={}", taskId, adminId);

        try {
            ResultOf<AeShopSelectionTaskModel> findResult = aeShopSelectionTaskReadService.findById(
                AeShopSelectionTaskQueryParam.builder()
                    .taskId(taskId)
                    .adminId(adminId)
                    .build());
            asserts(ResultOf.isValid(findResult), "Fail get task.");

            AeShopSelectionConfigVO configVO = convertToViewObject(findResult.getData().getConfig());
            return ResultOf.general(configVO);

        } catch (Throwable err) {
            log.error("{}, taskId={}", ERR_TASK_CONFIG, taskId, err.getMessage() , err);
            return ResultOf.error(ERR_TASK_CONFIG, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/task/config", method = RequestMethod.PUT)
    public Object putTaskConfig(@RequestBody AeShopSelectionConfigVO body, HttpServletRequest request) {
        try {
            log.info("configVO: {}", body);
            ResultOf<AeShopSelectionConfigModel> updateResult = aeShopSelectionTaskWriteService.updateConfig(
                AeShopSelectionConfigParam.builder()
                    .taskId(body.getTaskId())
                    .adminId(LoginUtil.getEmpId(request))
                    .configModel(convertToConfigModel(body))
                    .build());
            if (!ResultOf.isValid(updateResult)) {
                return updateResult;
            }
            return ResultOf.general(convertToViewObject(updateResult.getData()));
        } catch (Throwable err) {
            log.error("{}, body={}", ERR_TASK_CONFIG, body, err);
            return ResultOf.error(ERR_TASK_CONFIG, err.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/task/estimate", method = RequestMethod.POST)
    public Object getTaskPreviewCount(@RequestBody AeShopSelectionConfigVO body) {
        try {
            ResultOf<Long> result = aeShopSelectionTaskReadService.getPreviewCount(
                convertToConfigModel(body));
            return ResultOf.isValid(result) ? ResultOf.general(result.getData()) : result;
        } catch (Throwable err) {
            log.error("{}, body={}", ERR_TASK_ESTIMATE, body, err);
            return ResultOf.error(ERR_TASK_ESTIMATE, err.getMessage());
        }
    }

    private AeShopSelectionConfigVO convertToViewObject(AeShopSelectionConfigModel configModel) {

        if (configModel == null) {
            return null;
        }

        AeShopSelectionConfigVO configVO = new AeShopSelectionConfigVO();
        configVO.setSites(configModel.getSites());
        configVO.setPriceMaxRatio(configModel.getPriceLimitRatio());
        configVO.setCountry(configModel.getLgCountryId());
        configVO.setLogistics(convertToLongList(configModel.getLgLines()));

        AeShopSelectionConfigVO.Greps greps = new AeShopSelectionConfigVO.Greps();

        if (configModel.getSalesQuantity() != null) {
            greps.setSalesQuantity(new AeShopSelectionConfigVO.Range(
                configModel.getSalesQuantity().getMin(), configModel.getSalesQuantity().getMax()));
        }
        if (configModel.getCommentCount() != null) {
            greps.setRate(new AeShopSelectionConfigVO.Range(
                configModel.getCommentCount().getMin(), configModel.getCommentCount().getMax()));
        }
        if (configModel.getLikeCount() != null) {
            greps.setLike(new AeShopSelectionConfigVO.Range(
                configModel.getLikeCount().getMin(), configModel.getLikeCount().getMax()));
        }

        configVO.setGreps(greps);

        AeShopSelectionConfigVO.Costs costs = new AeShopSelectionConfigVO.Costs();

        costs.setItemCost(new AeShopSelectionConfigVO.CostDefine(true, null, null));

        costs.setLogisticsCost(new AeShopSelectionConfigVO.CostDefine(
            Boolean.TRUE.equals(configModel.getRequireOverseaCost()), null, null));

        costs.setProfitCost(new AeShopSelectionConfigVO.CostDefine(
            Boolean.TRUE.equals(configModel.getRequireProfit()),
            configModel.getProfitRate(),
            configModel.getProfitBasis()));

        costs.setOtherCost(new AeShopSelectionConfigVO.CostDefine(
            Boolean.TRUE.equals(configModel.getRequireExtraCost()),
            configModel.getExtraCostRate(),
            configModel.getExtraCostBasis()));

        configVO.setCosts(costs);

        return configVO;
    }

    private AeShopSelectionConfigModel convertToConfigModel(AeShopSelectionConfigVO configVO) {

        if (configVO == null) {
            return null;
        }

        AeShopSelectionConfigModel configModel = new AeShopSelectionConfigModel();

        configModel.setSites(configVO.getSites());

        if (configVO.getGreps() != null) {
            if (configVO.getGreps().getSalesQuantity() != null) {
                configModel.setSalesQuantity(new AeShopSelectionConfigModel.NumberRange(
                    configVO.getGreps().getSalesQuantity().getMin(),
                    configVO.getGreps().getSalesQuantity().getMax()));
            }
            if (configVO.getGreps().getRate() != null) {
                configModel.setCommentCount(new AeShopSelectionConfigModel.NumberRange(
                    configVO.getGreps().getRate().getMin(),
                    configVO.getGreps().getRate().getMax()));
            }
            if (configVO.getGreps().getLike() != null) {
                configModel.setLikeCount(new AeShopSelectionConfigModel.NumberRange(
                    configVO.getGreps().getLike().getMin(),
                    configVO.getGreps().getLike().getMax()));
            }
        }

        configModel.setPriceLimitRatio(configVO.getPriceMaxRatio());
        configModel.setLgCountryId(configVO.getCountry());
        configModel.setLgLines(convertLongList(configVO.getLogistics()));

        if (configVO.getCosts() != null) {
            if (configVO.getCosts().getLogisticsCost() != null) {
                configModel.setRequireOverseaCost(Boolean.TRUE.equals(configVO.getCosts().getLogisticsCost().getChecked()));
            }
            if (configVO.getCosts().getProfitCost() != null) {
                configModel.setRequireProfit(Boolean.TRUE.equals(configVO.getCosts().getProfitCost().getChecked()));
                configModel.setProfitRate(configVO.getCosts().getProfitCost().getRatio());
                configModel.setProfitBasis(configVO.getCosts().getProfitCost().getBasis());
            }
            if (configVO.getCosts().getOtherCost() != null) {
                configModel.setRequireExtraCost(Boolean.TRUE.equals(configVO.getCosts().getOtherCost().getChecked()));
                configModel.setExtraCostRate(configVO.getCosts().getOtherCost().getRatio());
                configModel.setExtraCostBasis(configVO.getCosts().getOtherCost().getBasis());
            }
        }

        return configModel;
    }

    private static List<String> convertLongList(List<Long> list) {
        if (list == null) {
            return null;
        }
        try {
            return list.stream().map(Object::toString).collect(Collectors.toList());
        } catch (Throwable err) {
            return null;
        }
    }

    private static List<Long> convertToLongList(List<String> list) {
        if (list == null) {
            return null;
        }
        try {
            return list.stream().map(Long::parseLong).collect(Collectors.toList());
        } catch (Throwable err) {
            return null;
        }
    }

    @Getter
    @Setter
    @ToString
    public static class TaskIdentity {
        private Long taskId;
    }

}
