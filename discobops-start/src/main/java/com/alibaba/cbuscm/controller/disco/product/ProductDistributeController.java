package com.alibaba.cbuscm.controller.disco.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.buc.sso.client.util.SimpleUserUtil;
import com.alibaba.buc.sso.client.vo.BucSSOUser;
import com.alibaba.cbuscm.service.onepiece.CommonDistributeService;
import com.alibaba.cbuscm.service.onepiece.model.PlatformShopModel;
import com.alibaba.up.common.mybatis.result.RPCResult;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类ProductDistributeController
 *
 * @author ruikun.xrk
 */
@RestController
@RequestMapping(value = "/api/product-distribute", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class ProductDistributeController {


    @Autowired
    private CommonDistributeService commonDistributeService;
    /**
     * 获取渠道店铺列表信息
     *
     * @param request
     * @return
     */
    @RequestMapping("/getChannelShopList")
    public RPCResult getChannelShopList(HttpServletRequest request) {
        try {
            BucSSOUser bucSSOUser = SimpleUserUtil.getBucSSOUser(request);
            List<PlatformShopModel> platformShopModels = commonDistributeService.getChannelShopList(bucSSOUser);
            return RPCResult.ok(platformShopModels);
        } catch (Exception e) {
            log.error("error getChannelShopList", e);
            return RPCResult.error("getChannelShopList error", e.getMessage());
        }
    }
}
