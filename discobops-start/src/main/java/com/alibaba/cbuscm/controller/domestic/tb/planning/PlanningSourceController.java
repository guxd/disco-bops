/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm.controller.domestic.tb.planning;

import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.core.proposal.api.InternalOfferProposalCommonService;
import com.alibaba.cbu.disco.shared.core.proposal.model.internal.InternalOfferProposalModel;
import com.alibaba.cbuscm.concurrent.CustomThreadFactory;
import com.alibaba.cbuscm.controller.domestic.tb.planning.request.AddOppRequest;
import com.alibaba.cbuscm.controller.domestic.tb.planning.request.DeleteOppRequest;
import com.alibaba.cbuscm.controller.domestic.tb.planning.request.QuerySupplyRequest;
import com.alibaba.cbuscm.controller.domestic.tb.planning.request.ReAssignRequest;
import com.alibaba.cbuscm.service.proposal.OfferDwService;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.cbuscm.vo.domestic.tb.planning.PlanningOppItemVO;
import com.alibaba.cbuscm.vo.domestic.tb.planning.PlanningOpportunityVO;
import com.alibaba.cbuscm.vo.domestic.tb.planning.PlanningSourceItemVO;
import com.alibaba.cbuscm.vo.domestic.tb.planning.PlanningSourceOfferVO;
import com.alibaba.cbuscm.vo.domestic.tb.planning.PlanningSourceVO;
import com.alibaba.china.company.model.Company;
import com.alibaba.china.company.model.CompanyProfile;
import com.alibaba.china.company.remote.CompanyReadService;
import com.alibaba.china.global.business.common.ResultOf;
import com.alibaba.china.global.business.match.interfaces.C2BDataPicSearchService;
import com.alibaba.china.idatacenter.service.IDataCenterQueryService;
import com.alibaba.china.idatacenter.service.model.IDataCenterQueryParam;
import com.alibaba.china.member.service.MemberReadService;
import com.alibaba.china.member.service.models.MemberModel;
import com.alibaba.china.offer.api.domain.general.Image;
import com.alibaba.china.offer.api.domain.general.Offer;
import com.alibaba.china.offer.api.domain.general.Product;
import com.alibaba.china.offer.api.query.model.OfferAttributesWrapper;
import com.alibaba.china.offer.api.query.model.OfferModel;
import com.alibaba.china.offer.api.query.service.OfferQueryService;
import com.alibaba.china.shared.discosupplier.constant.DiscoConstants;
import com.alibaba.china.shared.discosupplier.enums.operation.OperationDegreeEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.design.PlanningDesignTypeEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.opp.PlanningOppItemStatusEnum;
import com.alibaba.china.shared.discosupplier.enums.planning.opp.PlanningOppTypeEnum;
import com.alibaba.china.shared.discosupplier.model.common.UserModel;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningCompetitor;
import com.alibaba.china.shared.discosupplier.model.planning.design.PlanningDesignModel;
import com.alibaba.china.shared.discosupplier.model.planning.opp.PlanningOppItemModel;
import com.alibaba.china.shared.discosupplier.model.planning.opp.PlanningOpportunityModel;
import com.alibaba.china.shared.discosupplier.model.planning.source.PlanningOppAuditLog;
import com.alibaba.china.shared.discosupplier.model.planning.source.PlanningSourceModel;
import com.alibaba.china.shared.discosupplier.param.planning.opp.PlanningOppItemParam;
import com.alibaba.china.shared.discosupplier.param.planning.opp.PlanningOpportunityParam;
import com.alibaba.china.shared.discosupplier.param.planning.source.PlanningSourceParam;
import com.alibaba.china.shared.discosupplier.service.common.SupplierAblityService;
import com.alibaba.china.shared.discosupplier.service.planning.design.PlanningDesignService;
import com.alibaba.china.shared.discosupplier.service.planning.opp.PlanningOppItemService;
import com.alibaba.china.shared.discosupplier.service.planning.opp.PlanningOpportunityService;
import com.alibaba.china.shared.discosupplier.service.planning.source.PlanningSourceService;
import com.alibaba.china.shared.zdzb.api.ZdzbService;
import com.alibaba.china.shared.zdzb.enumeration.ItemType;
import com.alibaba.china.shared.zdzb.model.ItemQueryParam;
import com.alibaba.china.shared.zdzb.model.PowerOfferModel;
import com.alibaba.china.shared.zdzb.model.ReturnResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.PageList;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.alibaba.up.common.mybatis.result.Result;
import com.alibaba.up.common.mybatis.task.Paginator;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 企划寻源
 *
 * @author yaogaolin
 */
@RestController
@RequestMapping(path = "/planningSource", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//@CsrfTokenModel
//@CrossOrigin("*")
public class PlanningSourceController {

    private static final Logger LOG = LoggerFactory.getLogger(PlanningSourceController.class);

    private static final ThreadPoolExecutor THREAD_POOL_EXECUTOR = new ThreadPoolExecutor(
        25,
        50,
        60,
        TimeUnit.MINUTES,
        new LinkedBlockingQueue<>(100),
        new CustomThreadFactory("PlanningSourceController"));

    @Autowired
    private PlanningSourceService planningSourceService;
    @Autowired
    private PlanningOpportunityService planningOpportunityService;
    @Autowired
    private PlanningOppItemService planningOppItemService;
    @Autowired
    private PlanningDesignService planningDesignService;
    @Autowired
    private MemberReadService memberReadService;
    @Autowired
    private CompanyReadService companyReadService;
    @Autowired
    private OfferQueryService<OfferModel> offerQueryService;
    @Autowired
    private ZdzbService zdzbService;
    @Autowired
    private IDataCenterQueryService iDataCenterQueryService;
    @Autowired
    private C2BDataPicSearchService c2BDataPicSearchService;
    @Autowired
    private SupplierAblityService supplierAblityService;
    @Autowired
    private InternalOfferProposalCommonService internalOfferProposalCommonService;

    /**
     * 寻源列表
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RPCResult list(HttpServletRequest request,
        @RequestParam(name = "mine", required = false) boolean mine,
        @RequestParam(name = "title", required = false) String title,
        @RequestParam(name = "sourceId", required = false) Long sourceId,
        @RequestParam(name = "page", defaultValue = "1") Integer page,
        @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
        @RequestParam(name = "orderBy", defaultValue = "GMT_MODIFIED") String orderBy,
        @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection) {
        PlanningSourceParam planningSourceParam = new PlanningSourceParam();
        if (mine) {
            planningSourceParam.setHandler(LoginUtil.getEmpId(request));
        }
        if (StringUtils.isNotBlank(title)) {
            planningSourceParam.setPlanningTitle(title);
        }
        if (null != sourceId) {
            planningSourceParam.setSourceId(sourceId);
        }
        planningSourceParam.setPage(page);
        planningSourceParam.setPageSize(itemsPerPage);
        planningSourceParam.setCountNeeded(true);
        planningSourceParam.setOrderBy(orderBy);
        planningSourceParam.setOrderDirection(orderDirection);
        PageList<PlanningSourceModel> pageList = planningSourceService.query(planningSourceParam);
        if (pageList == null || CollectionUtils.isEmpty(pageList.getDataList())) {
            return buildRPCResult(Collections.emptyList(), 0, itemsPerPage, page, null);
        }
        List<PlanningSourceModel> sourceModelList = pageList.getDataList();
        List<PlanningSourceVO> sourceVOList = new ArrayList<>(sourceModelList.size());

        List<Long> planningIdList =
            sourceModelList.stream().map(PlanningSourceModel::getPlanningId).collect(Collectors.toList());
        List<PlanningDesignModel> designModelList = new ArrayList<>(planningIdList.size());
        try {
            designModelList = planningDesignService.find(planningIdList);
        } catch (Exception e) {
            LOG.error("planningDesignService.find error, planningIdList={}", JSON.toJSONString(planningIdList), e);
        }
        // planningId -> PlanningDesignModel
        Map<Long, PlanningDesignModel> designModelMap =
            designModelList.stream().collect(Collectors.toMap(PlanningDesignModel::getId, i -> i));
        for (PlanningSourceModel planningSourceModel : sourceModelList) {
            PlanningDesignModel planningDesignModel = designModelMap.get(planningSourceModel.getPlanningId());
            sourceVOList.add(buildPlanningSourceVO(planningSourceModel, planningDesignModel, request));
        }
        return buildRPCResult(sourceVOList, pageList.getPaginator().getItems(), itemsPerPage, page, null);
    }

    /**
     * 寻源列表
     */
    @RequestMapping(value = "/opps", method = RequestMethod.GET)
    public RPCResult listOpps(HttpServletRequest request,
        @RequestParam(name = "status", required = false) String status,
        @RequestParam(name = "sourceId") Long sourceId,
        @RequestParam(name = "page", defaultValue = "1") Integer page,
        @RequestParam(name = "itemsPerPage", defaultValue = "10") Integer itemsPerPage,
        @RequestParam(name = "orderBy", defaultValue = "SCORE") String orderBy,
        @RequestParam(name = "orderDirection", defaultValue = "DESC") String orderDirection) {
        // 1.按条件查询出符合的企划商机
        PageList<PlanningOpportunityModel> pageList =
            queryOpp(status, sourceId, page, itemsPerPage, orderBy, orderDirection);
        boolean editable = editable(request, sourceId);
        if (pageList == null || CollectionUtils.isEmpty(pageList.getDataList())) {
            return buildRPCResult(Collections.emptyList(), 0, itemsPerPage, page, editable);
        }
        List<PlanningOpportunityModel> opportunityModelList = pageList.getDataList();

        // oppId -> oppId下的所有品
        Map<Long, List<PlanningOppItemModel>> oppItemMap = new HashMap<>(opportunityModelList.size());
        // 所有oppId的所有品Id
        List<Long> itemIdList = new ArrayList<>(opportunityModelList.size());
        // 所有memberId
        List<String> memberIdList =
            opportunityModelList.stream().map(PlanningOpportunityModel::getMemberId).collect(Collectors.toList());
        // memberId -> loginId
        Map<String, String> memberId2LoginIdMap = buildLoginIdsByMemberIds(memberIdList);
        for (PlanningOpportunityModel opportunityModel : opportunityModelList) {
            List<PlanningOppItemModel> oppItemModelList =
                planningOppItemService.find(sourceId, opportunityModel.getMemberId(), status);
            oppItemMap.put(opportunityModel.getId(), oppItemModelList);
            itemIdList.addAll(
                oppItemModelList.stream().map(PlanningOppItemModel::getItemId).collect(Collectors.toList()));
        }

        List<Future<com.alibaba.china.idc.common.model.Result<List<Map<String, Object>>>>>
            offer30DaySalesFutureList = new ArrayList<>();
        List<Future<ReturnResult<Map<Long, PowerOfferModel>>>> powerOfferFutureList = new ArrayList<>();
        // 处理金冠
        buildPowerOfferModelFuture(itemIdList, powerOfferFutureList);
        // 处理月销量
        offer30DaySalesFuture(itemIdList, offer30DaySalesFutureList);
        // 处理金冠商品结果
        Map<Long, PowerOfferModel> powerOfferMap = buildPowerOfferModelFutureResult(powerOfferFutureList);
        // 处理月销量结果
        Map<Long, Offer30DaySalesAndPrice> offer30DaySalesMap = buildOffer30DaySalesFutureResult(
            offer30DaySalesFutureList);

        // 2.构建企划商机完整信息
        List<PlanningOpportunityVO> opportunityVOList = new ArrayList<>(opportunityModelList.size());
        for (PlanningOpportunityModel opportunityModel : opportunityModelList) {
            // 2.1 构建基础信息
            PlanningOpportunityVO opportunityVO = buildBaseOppInfo(opportunityModel, memberId2LoginIdMap);
            // 2.2 填充供应商信息
            fillingCompanyInfo(opportunityVO, opportunityModel);
            // 2.3 过滤掉商家提报
            opportunityVO.setTypes(buildTypes(opportunityModel.getTypes()));

            List<PlanningOppItemModel> oppItemModelList = oppItemMap.get(opportunityModel.getId());
            if (CollectionUtils.isNotEmpty(oppItemModelList)) {
                // 构建寻品信息
                List<PlanningOppItemVO> oppItemVOList = new ArrayList<>(oppItemModelList.size());
                for (PlanningOppItemModel oppItemModel : oppItemModelList) {
                    oppItemVOList.add(buildOppItemVO(oppItemModel, powerOfferMap, offer30DaySalesMap));
                }
                opportunityVO.setOpps(oppItemVOList);
            }
            opportunityVOList.add(opportunityVO);
        }
        return buildRPCResult(opportunityVOList, pageList.getPaginator().getItems(), itemsPerPage, page, editable);
    }

    private Map<String, String> buildLoginIdsByMemberIds(List<String> memberIdList) {
        if (CollectionUtils.isEmpty(memberIdList)) {
            return new HashMap<>(2);
        }
        List<Future<Map<String, String>>> futureList = new ArrayList<>(10);
        Map<String, String> memberId2LoginIdMap = new HashMap<>(memberIdList.size());
        // 上限110个，否则报IS_NOT_EXCEED_LIMIT
        List<List<String>> memberIdSubList = Lists.partition(memberIdList, 100);
        for (List<String> list : memberIdSubList) {
            futureList.add(THREAD_POOL_EXECUTOR.submit(() -> memberReadService.convertLoginIdsByMemberIds(list)));
        }
        for (Future<Map<String, String>> future : futureList) {
            try {
                Map<String, String> map = future.get();
                if (MapUtils.isNotEmpty(map)) {
                    memberId2LoginIdMap.putAll(map);
                }
            } catch (Exception e) {
                LOG.error("get memberId2LoginIdMap result error", e);
            }
        }
        return memberId2LoginIdMap;
    }

    private List<String> buildTypes(Set<String> types) {
        List<String> typeList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(types)) {
            for (String type : types) {
                String name = PlanningOppTypeEnum.parseName(type);
                if (StringUtils.isNotBlank(name)) {
                    typeList.add(name);
                }
            }
        }
        return typeList;
    }

    private PlanningOpportunityVO buildBaseOppInfo(PlanningOpportunityModel opportunityModel,
        Map<String, String> memberId2LoginIdMap) {
        PlanningOpportunityVO opportunityVO = new PlanningOpportunityVO();
        opportunityVO.setSourceId(opportunityModel.getSourceId());
        opportunityVO.setPlanningId(opportunityModel.getPlanningId());
        opportunityVO.setPlanningTitle(opportunityModel.getPlanningTitle());
        opportunityVO.setMemberId(opportunityModel.getMemberId());
        opportunityVO.setUserId(opportunityModel.getUserId());
        opportunityVO.setScore(opportunityModel.getScore());
        opportunityVO.setStatus(opportunityModel.getStatus());
        opportunityVO.setLoginId(memberId2LoginIdMap.get(opportunityModel.getMemberId()));
        return opportunityVO;
    }

    private PlanningOppItemVO buildOppItemVO(PlanningOppItemModel oppItemModel,
        Map<Long, PowerOfferModel> powerOfferMap, Map<Long, Offer30DaySalesAndPrice> offer30DaySalesMap) {
        PlanningOppItemVO oppItemVO = new PlanningOppItemVO();
        oppItemVO.setOfferProposalId(oppItemModel.getImportId());
        oppItemVO.setType(oppItemModel.getType());
        oppItemVO.setStatus(oppItemModel.getStatus());

        // TODO 优化成批量查询
        OfferModel offerModel = null;
        try {
            offerModel = offerQueryService.find(oppItemModel.getItemId());
        } catch (Exception e) {
            LOG.error("offerQueryService.find error，itemId={}", oppItemModel.getItemId(), e);
        }
        // 构建item
        buildOppItemBase(oppItemVO, oppItemModel.getItemId(), offerModel, offer30DaySalesMap, powerOfferMap);
        PlanningOppItemStatusEnum statusEnum = PlanningOppItemStatusEnum.parse(oppItemModel.getStatus());
        if (null != statusEnum) {
            oppItemVO.setStatusMessage(statusEnum.getMessage());
        }
        return oppItemVO;
    }

    private void fillingCompanyInfo(PlanningOpportunityVO opportunityVO, PlanningOpportunityModel opportunityModel) {
        try {
            Company company = companyReadService.findFullCompanyByVaccountId(opportunityModel.getMemberId());
            if (null != company) {
                opportunityVO.setCompanyName(company.getName());
                CompanyProfile companyProfile = company.getCompanyProfile();
                if (null != companyProfile && null != companyProfile.getRegistration()) {
                    opportunityVO.setAddress(companyProfile.getRegistration().getFoundedPlace());
                }
            }
        } catch (Exception e) {
            LOG.error("companyReadService.findFullCompanyByVaccountId error, memberId={}",
                opportunityModel.getMemberId(), e);
        }
    }

    private PageList<PlanningOpportunityModel> queryOpp(String status, Long sourceId, Integer page,
        Integer itemsPerPage, String orderBy, String orderDirection) {
        PlanningOpportunityParam planningOpportunityParam = new PlanningOpportunityParam();
        if (null != status) {
            planningOpportunityParam.setItemStatus(status);
        }
        planningOpportunityParam.setSourceId(sourceId);
        planningOpportunityParam.setPage(page);
        planningOpportunityParam.setPageSize(itemsPerPage);
        planningOpportunityParam.setCountNeeded(true);
        planningOpportunityParam.setOrderBy(orderBy);
        planningOpportunityParam.setOrderDirection(orderDirection);
        return planningOpportunityService.query(planningOpportunityParam);
    }

    private void offer30DaySalesFuture(List<Long> itemIds,
        List<Future<com.alibaba.china.idc.common.model.Result<List<Map<String, Object>>>>> futureList) {
        String paramString = Joiner.on(",").join(itemIds);
        IDataCenterQueryParam dataCenterQueryParam = new IDataCenterQueryParam();
        dataCenterQueryParam.setCombineId(366L);
        dataCenterQueryParam.setAppName(OfferDwService.APP_NAME);
        dataCenterQueryParam.setTimeout(10000);
        dataCenterQueryParam.setReturnFields(
            Sets.newHashSet("offer30DaySalesVolumeService.offer30DaySales", "od.price"));

        dataCenterQueryParam.addParameter("offerId", paramString);
        dataCenterQueryParam.setPageSize(itemIds.size());
        dataCenterQueryParam.setPageNO(1);
        futureList.add(THREAD_POOL_EXECUTOR.submit(() -> iDataCenterQueryService.query(dataCenterQueryParam)));
    }

    private Map<Long, Offer30DaySalesAndPrice> offer30DaySalesResult(
        com.alibaba.china.idc.common.model.Result<List<Map<String, Object>>> result) {
        if (result == null || !result.isSuccess() || result.getDefaultModel() == null) {
            LOG.error("iDataCenterQueryService.query failed! msg={}", JSON.toJSONString(result));
            return null;
        }
        List<Map<String, Object>> defaultModelList = result.getDefaultModel();
        List<Offer30DaySalesAndPrice> salesAndPriceList = new ArrayList<>(defaultModelList.size());
        for (Map<String, Object> map : defaultModelList) {
            Offer30DaySalesAndPrice offer30DaySalesAndPrice = new Offer30DaySalesAndPrice();
            offer30DaySalesAndPrice.itemId = Optional.ofNullable(map.get("offerId"))
                .map(Object::toString)
                .map(Long::parseLong)
                .orElse(null);
            offer30DaySalesAndPrice.offer30DaysSales = Optional.ofNullable(
                map.get("offer30DaySalesVolumeService.offer30DaySales"))
                .map(Object::toString)
                .map(Integer::parseInt)
                .orElse(null);
            offer30DaySalesAndPrice.price = (Optional.ofNullable(map.get("od.price"))
                .map(Object::toString)
                .map(Double::parseDouble)
                .orElse(null));
            salesAndPriceList.add(offer30DaySalesAndPrice);
        }
        return salesAndPriceList.stream().collect(Collectors.toMap(Offer30DaySalesAndPrice::getItemId, i -> i));
    }

    @Data
    private class Offer30DaySalesAndPrice {
        /**
         * 商品id
         */
        private Long itemId;
        /**
         * 月销量
         */
        private Integer offer30DaysSales;
        /**
         * 主搜价格
         */
        private Double price;
    }

    /**
     * 商机删除
     */
    @RequestMapping(value = "/deleteOpp", method = RequestMethod.POST)
    public RPCResult deleteOpp(HttpServletRequest request, @RequestBody DeleteOppRequest deleteOppRequest) {
        Long sourceId = deleteOppRequest.getSourceId();
        Long itemId = deleteOppRequest.getItemId();
        String memberId = deleteOppRequest.getMemberId();
        Assert.notNull(sourceId, "sourceId为空");
        Assert.notNull(memberId, "memberId为空");
        Assert.isTrue(editable(request, sourceId), "没有权限");

        Result<Boolean> result = planningSourceService.removeOpportunity(sourceId, memberId, itemId,
            LoginUtil.getLoginName(request), OperationDegreeEnum.WAITER);

        if (null != Result.successObj(result)) {
            return RPCResult.ok(Result.successObj(result));
        } else {
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
    }

    /**
     * 查询供应商
     */
    @RequestMapping(value = "/querySupply", method = RequestMethod.POST)
    public RPCResult querySupply(HttpServletRequest request, @RequestBody QuerySupplyRequest querySupplyRequest) {
        String loginIds = querySupplyRequest.getLoginIds();
        Long sourceId = querySupplyRequest.getSourceId();
        Assert.notNull(loginIds, "loginIds为空");
        Assert.notNull(sourceId, "sourceId为空");
        Assert.isTrue(StringUtils.isNotBlank(loginIds), "loginIds为空字符串");

        String convertedLoginId = loginIds.replaceAll("\\s+", " ");
        List<String> loginIdList = Lists.newArrayList(Sets.newHashSet(convertedLoginId.split(",|;|，|\\s")));
        if (CollectionUtils.isEmpty(loginIdList)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "loginId为空");
        }
        if (loginIdList.size() > 500) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "loginId过长");
        }

        // loginId转memberId
        List<String> memberIdList = new ArrayList<>(loginIdList.size());
        // 合法的loginId
        List<String> legalLoginId = new ArrayList<>(loginIdList.size());
        // loginId -> memberId
        Map<String, String> loginId2MemberIdMap = new HashMap<>(loginIdList.size());
        List<List<String>> loginIdSubList = Lists.partition(loginIdList, 50);
        for (List<String> list : loginIdSubList) {
            Map<String, String> currentMap = memberReadService.convertMemberIdsByLoginIds(list);
            if (MapUtils.isNotEmpty(currentMap)) {
                loginId2MemberIdMap.putAll(currentMap);
                memberIdList.addAll(currentMap.values());
                legalLoginId.addAll(currentMap.keySet());
            }
        }
        // 不存在的loginId
        List<String> illegalLoginId = buildNoExist(loginIdList, legalLoginId);

        // memberId -> loginId
        Map<String, String> memberId2LoginIdMap = new HashMap<>(loginId2MemberIdMap.size());
        for (Map.Entry<String, String> item : loginId2MemberIdMap.entrySet()) {
            memberId2LoginIdMap.put(item.getValue(), item.getKey());
        }

        // 查询寻源任务下的所有供应商
        PlanningOpportunityParam opportunityParam = new PlanningOpportunityParam();
        opportunityParam.setSourceId(sourceId);
        opportunityParam.setPageSize(null);
        List<PlanningOpportunityModel> opportunityModelList = planningOpportunityService.queryAll(opportunityParam);
        List<String> existMemberIdList =
            opportunityModelList.stream().map(PlanningOpportunityModel::getMemberId).collect(Collectors.toList());

        // 查询是否工厂
        //Result<Map<String, Boolean>> result = supplierAblityService.isSuppliersFactory(memberIdList);
        //if (!result.isSuccess() || result.getModel() == null) {
        //    LOG.error("supplierAblityService.isSuppliersIsFactory fail, result={}", JSON.toJSONString(result));
        //    return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        //}
        //// 供应商是否是工厂, supplierId -> isFactory
        //Map<String, Boolean> isFactoryMap = result.getModel();

        // 供应商过滤: 供应商是工厂(添加供应商不校验) && 在该寻源任务下不存在的
        // 供应商不是工厂
        List<String> notFactory = new ArrayList<>();
        // 在该寻源任务已经存在
        List<String> existLoginId = new ArrayList<>();
        Iterator<String> iterator = memberIdList.iterator();
        while (iterator.hasNext()) {
            String memberId = iterator.next();
            //if (!Boolean.TRUE.equals(isFactoryMap.get(memberId))) {
            //    notFactory.add(memberId2LoginIdMap.get(memberId));
            //    iterator.remove();
            //} else
            if (existMemberIdList.contains(memberId)) {
                existLoginId.add(memberId2LoginIdMap.get(memberId));
                iterator.remove();
            }
        }

        RPCResult rpcResult = new RPCResult();
        rpcResult.put("data", buildPlanningSourceItemVO(memberIdList, memberId2LoginIdMap));
        rpcResult.put("statusMsg", buildStatusMsg(illegalLoginId, notFactory, existLoginId));
        return RPCResult.okNoWrapper(rpcResult);
    }

    private List<PlanningSourceItemVO> buildPlanningSourceItemVO(List<String> memberIdList,
        Map<String, String> memberId2LoginIdMap) {
        List<PlanningSourceItemVO> planningSourceItemVOList = new ArrayList<>(memberIdList.size());
        List<List<String>> memberIdSubList = Lists.partition(memberIdList, 5);
        for (List<String> list : memberIdSubList) {
            // 根据memberId查询供应商的详细信息
            List<Company> companyList = companyReadService.listFullCompanyByVaccountIds(list);
            if (companyList == null) {
                companyList = new ArrayList<>();
            }
            Map<String, Company> companyMap = companyList.stream().collect(
                Collectors.toMap(Company::getVaccountId, c -> c, (c1, c2) -> c1));
            for (String memberId : list) {
                PlanningSourceItemVO planningSourceItemVO = new PlanningSourceItemVO();
                planningSourceItemVO.setLoginId(memberId2LoginIdMap.get(memberId));
                planningSourceItemVO.setMemberId(memberId);
                Company company = companyMap.get(memberId);
                if (company != null) {
                    planningSourceItemVO.setCompanyName(company.getName());
                    if (null != company.getCompanyProfile() && null != company.getCompanyProfile().getRegistration()) {
                        planningSourceItemVO.setAddress(
                            company.getCompanyProfile().getRegistration().getFoundedPlace());
                    }
                }
                planningSourceItemVOList.add(planningSourceItemVO);
            }
        }
        return planningSourceItemVOList;
    }

    private String buildStatusMsg(List<String> illegalLoginId, List<String> notFactory, List<String> existLoginId) {
        StringBuilder sb = new StringBuilder();
        if (CollectionUtils.isNotEmpty(illegalLoginId)) {
            sb.append("不存在的loginId：").append(Joiner.on(",").join(illegalLoginId)).append(";");
        }
        if (CollectionUtils.isNotEmpty(notFactory)) {
            sb.append("供应商不是工厂：").append(Joiner.on(",").join(notFactory)).append(";");
        }
        if (CollectionUtils.isNotEmpty(existLoginId)) {
            sb.append("供应商在该寻源任务中已经存在：").append(Joiner.on(",").join(existLoginId)).append(";");
        }
        return sb.toString();
    }

    private List<String> buildNoExist(List<String> loginIdList, List<String> legalLoginId) {
        List<String> noExist = new ArrayList<>();
        for (String loginId : loginIdList) {
            if (!legalLoginId.contains(loginId)) {
                noExist.add(loginId);
            }
        }
        return noExist;
    }

    /**
     * 同款匹配
     */
    @RequestMapping(value = "/matchSupply", method = RequestMethod.GET)
    public RPCResult matchSupply(HttpServletRequest request, @RequestParam Long sourceId) {
        Assert.notNull(sourceId, "sourceId为空");
        Assert.isTrue(editable(request, sourceId), "没有权限");
        // 1.反查对应企划
        PlanningSourceModel sourceModel = planningSourceService.find(sourceId);
        Assert.notNull(sourceModel, "sourceId不存在对应的寻源任务");
        PlanningDesignModel designModel = planningDesignService.find(sourceModel.getPlanningId());
        Assert.notNull(designModel, "sourceId不存在对应的商品企划");

        // 2.根据企划获取对应竞品数据，然后获取同款数据的商品Id
        List<Long> offerIdList = buildOfferIdList(designModel);
        Assert.notEmpty(offerIdList, "同款数据为空");
        // 去重后的同款品Id，1688的offerId就是企划的itemId
        List<Long> itemIdList = Lists.newArrayList(Sets.newHashSet(offerIdList));

        // 3.查询同款匹配到的商品信息、月销量、金冠商品
        // zdzbService：每次查询最大为20，offerQueryService：每次查询最大为50
        List<List<Long>> itemIdSubList = Lists.partition(itemIdList, 50);
        List<Future<List<OfferModel>>> offerModelFutureList = new ArrayList<>();
        List<Future<com.alibaba.china.idc.common.model.Result<List<Map<String, Object>>>>>
            offer30DaySalesFutureList = new ArrayList<>();
        List<Future<ReturnResult<Map<Long, PowerOfferModel>>>> powerOfferFutureList = new ArrayList<>();
        for (List<Long> itemIdsSub : itemIdSubList) {
            // 处理月销量
            offer30DaySalesFuture(itemIdsSub, offer30DaySalesFutureList);
            // 处理商品memberId
            offerModelFutureList.add(
                THREAD_POOL_EXECUTOR.submit(() -> offerQueryService.list(itemIdsSub.toArray(new Long[0]))));
            // 处理金冠商品
            buildPowerOfferModelFuture(itemIdsSub, powerOfferFutureList);
        }

        // 4.处理同款匹配到的商品信息
        Map<Long, OfferModel> offerMap = new HashMap<>(256);
        Set<String> memberIdSet = new HashSet<>(256);
        buildOfferModelResult(offerModelFutureList, memberIdSet, offerMap);
        List<String> memberIdList = Lists.newArrayList(memberIdSet);

        // 5.获取memberId和Company映射，memberId -> Company
        List<Future<List<Company>>> companyFutureList = buildCompanyMapFuture(memberIdList);

        // 6.获取已提报的商品Id
        List<Long> proposalItemId = buildProposalItemId(itemIdList);

        try {
            // 7.过滤寻源任务下已存在的品、过滤品的供应商不是工厂、过滤掉已提报的商品Id
            filterItemId(itemIdList, sourceId, memberIdList, offerMap, proposalItemId);
        } catch (Exception e) {
            LOG.error("filterItemId error", e);
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "过滤商品出现异常");
        }

        // 8.构建memberId -> loginId映射
        Map<String, String> memberId2LoginIdMap = buildLoginIdsByMemberIds(memberIdList);

        // 9.处理金冠商品结果
        Map<Long, PowerOfferModel> powerOfferMap = buildPowerOfferModelFutureResult(powerOfferFutureList);

        // 10.处理月销量
        Map<Long, Offer30DaySalesAndPrice> offer30DaySalesMap = buildOffer30DaySalesFutureResult(
            offer30DaySalesFutureList);

        // 11.构建匹配到的同款商品基本信息
        List<PlanningSourceItemVO> sourceItemVOList =
            buildSourceItemVOList(itemIdList, offerMap, powerOfferMap, offer30DaySalesMap, memberId2LoginIdMap);

        // 12.处理memberId的company结果
        Map<String, Company> companyMap = buildCompanyFutureResult(companyFutureList);

        // 13.同款商品信息组装商家信息
        sourceItemVOList.forEach(sourceItemVO -> {
            Company company = companyMap.get(sourceItemVO.getMemberId());
            if (null != company) {
                sourceItemVO.setCompanyName(company.getName());
                if (null != company.getCompanyProfile() && null != company.getCompanyProfile().getRegistration()) {
                    sourceItemVO.setAddress(company.getCompanyProfile().getRegistration().getFoundedPlace());
                }
            }
        });
        return RPCResult.ok(sourceItemVOList);
    }


    /**
     * 获取offer信息 包括企划信息
     */
    @RequestMapping(value = "/listOffer", method = RequestMethod.GET)
    public RPCResult listOffer(HttpServletRequest request, @RequestParam Long sourceId, @RequestParam String offerIds) {
        Assert.notNull(offerIds, "offerId为空");
        Assert.notNull(sourceId, "sourceId为空");
        //Assert.isTrue(editable(request, sourceId), "没有权限");
        // 1.反查对应企划
        PlanningSourceModel sourceModel = planningSourceService.find(sourceId);
        Assert.notNull(sourceModel, "sourceId不存在对应的寻源任务");
        PlanningDesignModel designModel = planningDesignService.find(sourceModel.getPlanningId());
        Assert.notNull(designModel, "sourceId不存在对应的商品企划");

        offerIds = offerIds.replaceAll(";", ",");
        offerIds = offerIds.replaceAll("，", ",");
        offerIds = offerIds.replaceAll("\\s+", ",");
        List<String> idList = Arrays.asList(offerIds.split(","));//根据逗号分隔转化为list
        Assert.notEmpty(idList, "查询offer数据为空");
        // 去重后的同款品Id，1688的offerId就是企划的itemId
        List<Long> itemIdList = idList.stream().map(id -> Long.parseLong(id)).collect(Collectors.toList());

        // 3.查询同款匹配到的商品信息、月销量、金冠商品
        // zdzbService：每次查询最大为20，offerQueryService：每次查询最大为50
        Assert.isTrue(itemIdList.size()<50, "查询数据量过多");

        List<Future<com.alibaba.china.idc.common.model.Result<List<Map<String, Object>>>>>
            offer30DaySalesFutureList = new ArrayList<>();
        List<Future<ReturnResult<Map<Long, PowerOfferModel>>>> powerOfferFutureList = new ArrayList<>();
        Map<Long,Future<List<PlanningOppItemModel>>> oppItemFutureMap = new HashMap<>();


        //处理三十天销量
        offer30DaySalesFuture(itemIdList, offer30DaySalesFutureList);
        // 处理金冠商品
        buildPowerOfferModelFuture(itemIdList, powerOfferFutureList);

        //商机
        itemIdList.stream().forEach(id ->
            oppItemFutureMap.put(id,THREAD_POOL_EXECUTOR.submit(() -> planningOppItemService.findByItemId(id))));

        //查询
        List<OfferModel> offerModels = offerQueryService.list(itemIdList.toArray(new Long[itemIdList.size()]));
        if(CollectionUtils.isEmpty(offerModels)){
            List<String> itemIdStringList = itemIdList.stream().map(String::valueOf).collect(Collectors.toList());
            RPCResult result = RPCResult.ok(Lists.newArrayList());
            result.put("statusMsg","不存在offer id:" + String.join(",",itemIdStringList));
            return result;
        }
        //company
        List<String> memberIdList = offerModels.stream().map(offer -> offer.getOffer().getMemberID()).collect(Collectors.toList());

        // 5.获取memberId和Company映射，memberId -> Company
        List<Future<List<MemberModel>>> companyFutureList = buildCompanyMapFutureByMemberId(memberIdList);

        Map<Long, Offer30DaySalesAndPrice> offer30DaySalesMap = buildOffer30DaySalesFutureResult(
            offer30DaySalesFutureList);
        Map<Long, PowerOfferModel> powerOfferMap = buildPowerOfferModelFutureResult(powerOfferFutureList);
        // 12.处理memberId的company结果
        Map<String, MemberModel> companyMap = buildCompanyMemberFutureResult(companyFutureList);
        Map<Long, PlanningOppItemModel>  buildOppItemResult = buildOppItemFutureResult(oppItemFutureMap);

        List<PlanningSourceOfferVO> planningSourceOfferVOS = new ArrayList<PlanningSourceOfferVO>();
        for(OfferModel offerModel : offerModels){
            PlanningSourceOfferVO vo = new PlanningSourceOfferVO();
            Offer offer = offerModel.getOffer();
            if (null == offer) {
               continue;
            }
            vo.setOfferId(offer.getId());
            vo.setTitle(offer.getSubject());
            Product product = offer.getProduct();
            if (null != product) {
                List<Image> imageList = product.getImageList();
                if (CollectionUtils.isNotEmpty(imageList)) {
                    vo.setPicUrls(imageList.stream().map(Image::getSize220x220ImageURI)
                        .map(i -> DiscoConstants.CBU_ITEM_PICTURE_PREFIX + i).collect(Collectors.toList()));
                }
            }
            String memberId = offer.getMemberID();
            OfferAttributesWrapper attributesWrapper = offerModel.getAttributesWrapper();
            if (attributesWrapper != null) {
                vo.setLink(DiscoConstants.CBU_ITEM_DETAIL_PREFIX + attributesWrapper.getOfferURL());
            }
            if (null != offerModel.getSign()) {
                vo.setBestOffer(offerModel.getSign().get("isBestOffer"));
            }
            PowerOfferModel powerOffer = powerOfferMap.get(offer.getId());
            if (null != powerOffer && null != powerOffer.getItemType()) {
                vo.setGoldOffer(ItemType.GOLD.equals(powerOffer.getItemType()));
            }
            // 月销量
            Offer30DaySalesAndPrice offer30DaySalesAndPrice = offer30DaySalesMap.get(offer.getId());
            if (null != offer30DaySalesAndPrice) {
                vo.setSales(offer30DaySalesAndPrice.getOffer30DaysSales());
            }

            if(companyMap.containsKey(memberId) && companyMap.get(memberId)!=null){
                MemberModel company = companyMap.get(memberId);
                if(company!=null) {
                    vo.setCompany(company.getCompanyName());
                }
            }
            vo.setPlanningTitle("尚未关联企划");
            if(buildOppItemResult.containsKey(offer.getId())){
                PlanningOppItemModel oppItemModel = buildOppItemResult.get(offer.getId());
                vo.setPlanningId(oppItemModel.getPlanningId());
                PlanningDesignModel designModel1 = planningDesignService.find(oppItemModel.getPlanningId());
                if(designModel1!=null){
                    vo.setPlanningTitle(designModel1.getTitle());
                }
            }
            planningSourceOfferVOS.add(vo);
        }
        //求差量 没有获取到offer的ID要提示用户
        RPCResult result = RPCResult.ok(planningSourceOfferVOS);
        itemIdList.removeAll(offerModels.stream().map(offer -> offer.getOffer().getId()).collect(Collectors.toList()));
        if(CollectionUtils.isNotEmpty(itemIdList)){
            List<String> itemIdStringList = itemIdList.stream().map(String::valueOf).collect(Collectors.toList());
            result.put("statusMsg","不存在offer id:" + String.join(",",itemIdStringList));
        }
        return result;
    }

    /**
     * 企划关联 把某个品关联到企划
     * @param request
     * @param planningId
     * @param planningSourceId
     * @param offerIds
     * @return
     */
    @RequestMapping(value = "/relation", method = RequestMethod.GET)
    public RPCResult relationPlanning(HttpServletRequest request, @RequestParam Long planningId,@RequestParam Long planningSourceId,@RequestParam String offerIds) {
        if (StringUtils.isEmpty(offerIds)) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "企划要关联的offer不能为空!");
        }
        String empId = LoginUtil.getEmpId(request);
        String empName = LoginUtil.getLoginName(request);
        if (StringUtils.isEmpty(empId)) {
            LOG.error("offer import audit pass error, user not login");
            return RPCResult.error(RPCResult.MSG_ERROR_UNAUTHORIZED, RPCResult.MSG_ERROR_CHINESS_LOGOUT_ERROR);
        }
        offerIds = offerIds.replaceAll("\\s+", ",");
        offerIds = offerIds.replaceAll(";", ",");
        offerIds = offerIds.replaceAll("，", ",");
        Set<String> offerIdSet = new HashSet<>(Arrays.asList(offerIds.split(",")));
        if (offerIdSet.size() > 5) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, "企划要关联的offer每次不能大于5条!");
        }
        List<Long> offerLongList = offerIdSet.stream().map(Long::parseLong).collect(Collectors.toList());
        for (Long offerId : offerLongList) {
            Result<Boolean> offerRelationResult = planningSourceService.relationPlanningSourceAndOffer(planningSourceId,
                offerId, empId, empName);
            if (offerRelationResult != null && !offerRelationResult.isSuccess()) {
                return RPCResult.error(offerRelationResult.getMsgCode(),
                    offerId + ":" + offerRelationResult.getMsgInfo());
            }
        }
        return RPCResult.ok(true);
    }

    private Map<Long, Offer30DaySalesAndPrice> buildOffer30DaySalesFutureResult(
        List<Future<com.alibaba.china.idc.common.model.Result<List<Map<String, Object>>>>> offer30DaySalesFutureList) {
        Map<Long, Offer30DaySalesAndPrice> offer30DaySalesMap = new HashMap<>(256);
        for (Future<com.alibaba.china.idc.common.model.Result<List<Map<String, Object>>>> future :
            offer30DaySalesFutureList) {
            try {
                Map<Long, Offer30DaySalesAndPrice> currentOffer30DaySalesMap = offer30DaySalesResult(future.get());
                if (MapUtils.isNotEmpty(currentOffer30DaySalesMap)) {
                    offer30DaySalesMap.putAll(currentOffer30DaySalesMap);
                }
            } catch (Exception e) {
                LOG.error("get Offer30DaySales result", e);
            }
        }
        return offer30DaySalesMap;
    }

    private Map<Long, PowerOfferModel> buildPowerOfferModelFutureResult(
        List<Future<ReturnResult<Map<Long, PowerOfferModel>>>> powerOfferFutureList) {
        Map<Long, PowerOfferModel> powerOfferMap = new HashMap<>(256);
        for (Future<ReturnResult<Map<Long, PowerOfferModel>>> future : powerOfferFutureList) {
            try {
                ReturnResult<Map<Long, PowerOfferModel>> result = future.get();
                if (result != null && result.isSuccess()) {
                    powerOfferMap.putAll(result.getContent());
                }
            } catch (Exception e) {
                LOG.error("get power result error", e);
            }
        }
        return powerOfferMap;
    }

    private Map<Long, PlanningOppItemModel> buildOppItemFutureResult(
        Map<Long,Future<List<PlanningOppItemModel>>> oppItemFutureMap) {
        Map<Long, PlanningOppItemModel> oppItemMap = new HashMap<>(256);
        for (Long id : oppItemFutureMap.keySet()) {
            try {
                List<PlanningOppItemModel> result = oppItemFutureMap.get(id).get();
                if (CollectionUtils.isNotEmpty(result)) {
                    oppItemMap.put(id,result.get(0));
                }
            } catch (Exception e) {
                LOG.error("get power result error", e);
            }
        }
        return oppItemMap;
    }

    private Map<String, Company> buildCompanyFutureResult(List<Future<List<Company>>> companyFutureList) {
        Map<String, Company> companyMap = new HashMap<>(64);
        for (Future<List<Company>> future : companyFutureList) {
            try {
                List<Company> companyList = future.get();
                if (CollectionUtils.isNotEmpty(companyList)) {
                    for (Company company : companyList) {
                        companyMap.put(company.getVaccountId(), company);
                    }
                }
            } catch (Exception e) {
                LOG.error("get company result error", e);
            }
        }
        return companyMap;
    }
    private Map<String, MemberModel> buildCompanyMemberFutureResult(List<Future<List<MemberModel>>> companyFutureList) {
        Map<String, MemberModel> companyMap = new HashMap<>(64);
        for (Future<List<MemberModel>> future : companyFutureList) {
            try {
                List<MemberModel> memberModelList = future.get();
                if (CollectionUtils.isNotEmpty(memberModelList)) {
                    for (MemberModel memberModel : memberModelList) {
                        companyMap.put(memberModel.getMemberId(), memberModel);
                    }
                }
            } catch (Exception e) {
                LOG.error("get company result error", e);
            }
        }
        return companyMap;
    }

    private void buildOfferModelResult(List<Future<List<OfferModel>>> offerModelFutureList, Set<String> memberIdSet,
        Map<Long, OfferModel> offerMap) {
        for (Future<List<OfferModel>> future : offerModelFutureList) {
            try {
                List<OfferModel> offerModelList = future.get();
                if (CollectionUtils.isNotEmpty(offerModelList)) {
                    offerModelList.forEach(o -> {
                        memberIdSet.add(o.getOffer().getMemberID());
                        offerMap.put(o.getOffer().getId(), o);
                    });
                }
            } catch (Exception e) {
                LOG.error("get OfferModel result", e);
            }
        }
    }

    private List<Future<List<Company>>> buildCompanyMapFuture(List<String> memberIdList) {
        List<Future<List<Company>>> futures = new ArrayList<>(30);
        // 根据memberId查询company信息
        List<List<String>> memberIdSubList = Lists.partition(memberIdList, 5);
        for (List<String> list : memberIdSubList) {
            try {
                futures.add(THREAD_POOL_EXECUTOR.submit(() -> companyReadService.listFullCompanyByVaccountIds(list)));
            } catch (Exception e) {
                LOG.error("companyReadService.listFullCompanyByVaccountIds error", e);
            }
        }
        return futures;
    }

    private List<Future<List<MemberModel>>> buildCompanyMapFutureByMemberId(List<String> memberIdList) {
        List<Future<List<MemberModel>>> futures = new ArrayList<>();
        // memberId -> Company
        // 根据memberId查询company信息
        List<List<String>> memberIdSubList = Lists.partition(memberIdList, 10);
        for (List<String> list : memberIdSubList) {
            try {
                futures.add(THREAD_POOL_EXECUTOR.submit(() -> memberReadService.listMember(list)));
            } catch (Exception e) {
                LOG.error("companyReadService.listFullCompanyByVaccountIds error", e);
            }
        }
        return futures;
    }

    private Map<String, Company> buildCompanyMap(List<String> memberIdList) {
        List<Future<List<Company>>> futureList = new ArrayList<>(10);
        // memberId -> Company
        Map<String, Company> companyMap = new HashMap<>(memberIdList.size());
        // 根据memberId查询company信息
        List<List<String>> memberIdSubList = Lists.partition(memberIdList, 5);
        for (List<String> list : memberIdSubList) {
            try {
                futureList.add(
                    THREAD_POOL_EXECUTOR.submit(() -> companyReadService.listFullCompanyByVaccountIds(list)));
                // vaccountIds 最大长度为5
                List<Company> companies = companyReadService.listFullCompanyByVaccountIds(list);
                if (CollectionUtils.isNotEmpty(companies)) {
                    for (Company c : companies) {
                        companyMap.put(c.getVaccountId(), c);
                    }
                }
            } catch (Exception e) {
                LOG.error("companyReadService.listFullCompanyByVaccountIds error", e);
            }
        }
        return companyMap;
    }

    private List<Long> buildProposalItemId(List<Long> itemIdList) {
        List<Long> proposalItemId = new ArrayList<>();
        ListOf<InternalOfferProposalModel> offerProposalModelListOf =
            internalOfferProposalCommonService.queryOfferProposalByOfferIds(itemIdList);
        if (ListOf.isValid(offerProposalModelListOf)) {
            proposalItemId = offerProposalModelListOf.getData().stream().map(InternalOfferProposalModel::getOfferId)
                .collect(Collectors.toList());
        }
        return proposalItemId;
    }

    private void filterItemId(List<Long> itemIdList, Long sourceId, List<String> memberIdList,
        Map<Long, OfferModel> offerMap, List<Long> proposalItemId) {
        // 查询寻源任务下的所有品
        PlanningOppItemParam planningOppItemParam = new PlanningOppItemParam();
        planningOppItemParam.setSourceId(sourceId);
        planningOppItemParam.setPageSize(null);
        List<PlanningOppItemModel> planningOppItemModelList = planningOppItemService.queryAll(planningOppItemParam);
        List<Long> existItemIdList =
            planningOppItemModelList.stream().map(PlanningOppItemModel::getItemId).collect(Collectors.toList());

        // 查询供应商是否为工厂
        Result<Map<String, Boolean>> result = supplierAblityService.isSuppliersFactory(memberIdList);
        if (!result.isSuccess() || result.getModel() == null) {
            throw new RuntimeException(
                "supplierAblityService.isSuppliersIsFactory fail, result=" + JSON.toJSONString(result));
        }
        // 供应商是否是工厂, supplierId -> isFactory
        Map<String, Boolean> isFactoryMap = result.getModel();

        // 过滤掉商品在该寻源任务下已经存在的、过滤掉品的供应商不是工厂、过滤掉已经提报的商品
        Iterator<Long> itemIdIterator = itemIdList.iterator();
        while (itemIdIterator.hasNext()) {
            Long itemId = itemIdIterator.next();
            OfferModel offerModel = offerMap.get(itemId);
            if (offerModel == null || offerModel.getOffer() == null) {
                // 这边理论上也可以直接remove
                continue;
            }
            if (existItemIdList.contains(itemId) || proposalItemId.contains(itemId) ||
                !Boolean.TRUE.equals(isFactoryMap.get(offerModel.getOffer().getMemberID()))) {
                itemIdIterator.remove();
            }
        }
    }

    private List<Long> buildOfferIdList(PlanningDesignModel designModel) {
        List<Long> offerIdList = new ArrayList<>();
        List<PlanningCompetitor> competitorList = designModel.getCompetitors();
        if (CollectionUtils.isNotEmpty(competitorList)) {
            List<String> picUrlList = new ArrayList<>();
            // 获取所有的url
            for (PlanningCompetitor competitor : competitorList) {
                if (CollectionUtils.isNotEmpty(competitor.getPicUrls())) {
                    picUrlList.addAll(competitor.getPicUrls());
                }
            }

            List<Future<ResultOf<List<Long>>>> futureList = new ArrayList<>(picUrlList.size());
            // 通过url查询同款数据，每个url最多取10个
            for (String picUrl : picUrlList) {
                try {
                    Future<ResultOf<List<Long>>> future =
                        THREAD_POOL_EXECUTOR.submit(() -> c2BDataPicSearchService.picSearch(picUrl, 10));
                    futureList.add(future);
                } catch (Exception e) {
                    LOG.error("c2BDataPicSearchService.picSearch error", e);
                }
            }
            for (Future<ResultOf<List<Long>>> future : futureList) {
                try {
                    ResultOf<List<Long>> resultOf = future.get();
                    if (ResultOf.isValid(resultOf)) {
                        offerIdList.addAll(resultOf.getData());
                    }
                } catch (Exception e) {
                    LOG.error("获取结果异常", e);
                }
            }
        }
        return offerIdList;
    }

    private List<PlanningSourceItemVO> buildSourceItemVOList(List<Long> itemIds, Map<Long, OfferModel> offerMap,
        Map<Long, PowerOfferModel> powerOfferMap, Map<Long, Offer30DaySalesAndPrice> offer30DaySalesMap,
        Map<String, String> memberId2LoginIdMap) {
        List<PlanningSourceItemVO> sourceItemVOList = new ArrayList<>(itemIds.size());
        for (Long itemId : itemIds) {
            PlanningSourceItemVO sourceItemVO = new PlanningSourceItemVO();
            PlanningOppItemVO oppItemVO = new PlanningOppItemVO();
            sourceItemVO.setItem(oppItemVO);
            OfferModel offerModel = offerMap.get(itemId);
            // 理论上这边不会为空，之前已经过滤过一次
            if (offerModel.getOffer() != null) {
                String memberId = offerModel.getOffer().getMemberID();
                sourceItemVO.setMemberId(offerModel.getOffer().getMemberID());
                sourceItemVO.setLoginId(memberId2LoginIdMap.get(memberId));
            }
            // 构建item
            buildOppItemBase(oppItemVO, itemId, offerModel, offer30DaySalesMap, powerOfferMap);
            sourceItemVOList.add(sourceItemVO);
        }
        return sourceItemVOList;
    }

    private void buildOppItemBase(PlanningOppItemVO oppItemVO, Long itemId, OfferModel offerModel,
        Map<Long, Offer30DaySalesAndPrice> offer30DaySalesMap, Map<Long, PowerOfferModel> powerOfferMap) {
        oppItemVO.setItemId(itemId);
        if (null != offerModel) {
            Offer offer = offerModel.getOffer();
            if (null != offer) {
                oppItemVO.setTitle(offer.getSubject());
                Product product = offer.getProduct();
                if (null != product) {
                    List<Image> imageList = product.getImageList();
                    if (CollectionUtils.isNotEmpty(imageList)) {
                        oppItemVO.setPicUrls(imageList.stream().map(Image::getSize220x220ImageURI)
                            .map(i -> DiscoConstants.CBU_ITEM_PICTURE_PREFIX + i).collect(Collectors.toList()));
                    }
                }
            }
            OfferAttributesWrapper attributesWrapper = offerModel.getAttributesWrapper();
            if (attributesWrapper != null) {
                oppItemVO.setLink(DiscoConstants.CBU_ITEM_DETAIL_PREFIX + attributesWrapper.getOfferURL());
            }
            if (null != offerModel.getSign()) {
                oppItemVO.setBestOffer(offerModel.getSign().get("isBestOffer"));
            }
            if (null != offerModel.getSystemAttributes()) {
                oppItemVO.setUptime(offerModel.getSystemAttributes().getPostDate());
            }
        }

        PowerOfferModel powerOffer = powerOfferMap.get(itemId);
        if (null != powerOffer && null != powerOffer.getItemType()) {
            oppItemVO.setGoldOffer(ItemType.GOLD.equals(powerOffer.getItemType()));
        }
        // 月销量
        Offer30DaySalesAndPrice offer30DaySalesAndPrice = offer30DaySalesMap.get(itemId);
        if (null != offer30DaySalesAndPrice) {
            oppItemVO.setPrice(offer30DaySalesAndPrice.getPrice());
            oppItemVO.setSales(offer30DaySalesAndPrice.getOffer30DaysSales());
        }
    }

    private void buildPowerOfferModelFuture(List<Long> itemIds,
        List<Future<ReturnResult<Map<Long, PowerOfferModel>>>> powerOfferFutureList) {
        List<List<Long>> itemIdSubList = Lists.partition(itemIds, 20);
        for (List<Long> list : itemIdSubList) {
            ItemQueryParam itemQueryParam = new ItemQueryParam();
            itemQueryParam.setItemType(ItemType.GOLD);
            itemQueryParam.setItemList(Sets.newHashSet(list));
            try {
                powerOfferFutureList.add(
                    THREAD_POOL_EXECUTOR.submit(() -> zdzbService.queryItemWithDetail(itemQueryParam)));
            } catch (Exception e) {
                LOG.error("zdzbService.queryItemWithDetail error, param={}", JSON.toJSONString(itemQueryParam), e);
            }
        }
    }

    /**
     * 添加商机
     */
    @RequestMapping(value = "/addOpp", method = RequestMethod.POST)
    public RPCResult addOpp(HttpServletRequest request, @RequestBody AddOppRequest addOppRequest) {
        //AddOppRequest addOppRequest = JSON.parseObject(data, AddOppRequest.class);
        if (null == addOppRequest || null == addOppRequest.getSourceId() || null == addOppRequest.getType()
            || CollectionUtils.isEmpty(addOppRequest.getOpps())) {
            return RPCResult.error(RPCResult.CODE_PARAM_ERROR, RPCResult.MSG_ERROR_ILLEGAL);
        }
        if (!editable(request, addOppRequest.getSourceId())) {
            return RPCResult.error(RPCResult.CODE_UNAUTHORIZED, "没有权限");
        }

        Result<List<Long>> result = planningSourceService.addOpportunities(addOppRequest.getSourceId(),
            PlanningOppTypeEnum.parse(addOppRequest.getType()),
            Sets.newHashSet(addOppRequest.getOpps()), LoginUtil.getLoginName(request), OperationDegreeEnum.WAITER);

        if (null != Result.successObj(result)) {
            return RPCResult.ok(Result.successObj(result));
        } else {
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
    }

    /**
     * 重新指定
     */
    @RequestMapping(value = "/reAssign", method = RequestMethod.POST)
    public RPCResult reAssign(HttpServletRequest request, @RequestBody ReAssignRequest reAssignRequest) {
        Assert.notNull(reAssignRequest, "入参为空");
        Assert.notNull(reAssignRequest.getSourceId(), "sourceId为空");
        Assert.isTrue(editable(request, reAssignRequest.getSourceId()), "没有权限");

        // 如果handler中不包含操作者，则将操作者添进去
        List<UserModel> handlers =
            reAssignRequest.getHandlers() == null ? Collections.emptyList() : reAssignRequest.getHandlers();
        List<String> empIdList = handlers.stream().map(UserModel::getEmpId).collect(Collectors.toList());
        String empId = LoginUtil.getEmpId(request);
        String loginName = LoginUtil.getLoginName(request);
        if (!empIdList.contains(empId)) {
            UserModel userModel = new UserModel();
            userModel.setEmpId(empId);
            userModel.setStageName(loginName);
            handlers.add(userModel);
        }
        Assert.isTrue(handlers.size() <= 10, "处理人不能超过10个");
        Result<Boolean> result = planningSourceService.reAssign(reAssignRequest.getSourceId(),
            Sets.newHashSet(handlers), loginName, OperationDegreeEnum.WAITER);

        if (null != Result.successObj(result)) {
            return RPCResult.ok(Result.successObj(result));
        } else {
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
    }

    /**
     * 查询审核记录
     */
    @RequestMapping(value = "/auditLog", method = RequestMethod.GET)
    public RPCResult auditLog(HttpServletRequest request,
        @RequestParam Long sourceId,
        @RequestParam String memberId,
        @RequestParam Long itemId) {

        Result<List<PlanningOppAuditLog>> result = planningSourceService.queryAuditLogs(sourceId, memberId, itemId);
        if (null != Result.successObj(result)) {
            return RPCResult.ok(Result.successObj(result));
        } else {
            return RPCResult.error(result.getMsgCode(), result.getMsgInfo());
        }
    }

    /**
     * 获取商机状态列表
     */
    @RequestMapping(value = "/itemStatus", method = RequestMethod.GET)
    public RPCResult itemStatus(HttpServletRequest request) {

        List<PlanningOppItemStatusEnum> statusEnumList = PlanningOppItemStatusEnum.careStatusEnums();

        List<PlanningOppAuditLog> statusVOs = statusEnumList.stream().map(i -> {
            PlanningOppAuditLog vo = new PlanningOppAuditLog();
            vo.setStatus(i.getValue());
            vo.setStatusMessage(i.getMessage());
            return vo;
        }).collect(Collectors.toList());

        return RPCResult.ok(statusVOs);
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public RPCResult detail(HttpServletRequest request, @RequestParam Long sourceId) {
        Assert.notNull(sourceId, "sourceId为空");
        PlanningSourceModel planningSourceModel = planningSourceService.find(sourceId);
        Assert.notNull(planningSourceModel, "非法的sourceId");
        PlanningDesignModel planningDesignModel = planningDesignService.find(planningSourceModel.getPlanningId());
        return RPCResult.ok(buildPlanningSourceVO(planningSourceModel, planningDesignModel, request));
    }

    private PlanningSourceVO buildPlanningSourceVO(PlanningSourceModel sourceModel, PlanningDesignModel designModel,
        HttpServletRequest request) {
        if (sourceModel == null) {
            return null;
        }
        Long planningId = sourceModel.getPlanningId();
        Long sourceId = sourceModel.getId();
        PlanningSourceVO planningSourceVO = new PlanningSourceVO();
        planningSourceVO.setId(sourceId);
        planningSourceVO.setPlanningId(planningId);
        planningSourceVO.setPlanningTitle(sourceModel.getPlanningTitle());
        planningSourceVO.setHandlers(sourceModel.getHandlers());
        planningSourceVO.setStatus(sourceModel.getStatus());
        if (null != designModel) {
            planningSourceVO.setCategoryId(designModel.getCategoryId());
            planningSourceVO.setCategoryValue(designModel.getCategoryValue());
            planningSourceVO.setPlanningType(getPlanningType(designModel.getType()));
            planningSourceVO.setPlanningWaveBrand(designModel.getWaveBrand());
            planningSourceVO.setImportLower(designModel.getImportLower());
            planningSourceVO.setImportUp(designModel.getImportUp());
        }
        planningSourceVO.setRecruitMemberCount(planningOpportunityService.count(planningId, sourceId));
        planningSourceVO.setApproveCount(planningOppItemService.count(planningId, sourceId,
            PlanningOppItemStatusEnum.IMPORT.getValue()));
        planningSourceVO.setImportPassCount(planningOppItemService.count(planningId, sourceId,
            PlanningOppItemStatusEnum.IMPORT_PASS.getValue()));
        planningSourceVO.setChannelImportCount(planningOppItemService.count(planningId, sourceId,
            PlanningOppItemStatusEnum.CHANNEL_IMPORT.getValue()));
        planningSourceVO.setChannelOnePassCount(planningOppItemService.count(planningId, sourceId,
            PlanningOppItemStatusEnum.CHANNEL_1_PASS.getValue()));
        planningSourceVO.setOfferCount(planningOppItemService.count(planningId, sourceId,
            PlanningOppItemStatusEnum.CHANNEL_MACHINE_PASS.getValue()));
        planningSourceVO.setEditable(editable(request, sourceId));
        return planningSourceVO;
    }

    private String getPlanningType(PlanningDesignTypeEnum planningDesignTypeEnum) {
        if( null == planningDesignTypeEnum){
            return null;
        }
        return planningDesignTypeEnum.getMessage();
    }

    /**
     * 判断当前登录人是否有权限修改该寻源任务
     *
     * @param request
     * @param sourceId
     * @return
     */
    private boolean editable(HttpServletRequest request, Long sourceId) {
        String empId = LoginUtil.getEmpId(request);
        PlanningSourceModel planningSourceModel = planningSourceService.find(sourceId);
        if (planningSourceModel != null && CollectionUtils.isNotEmpty(planningSourceModel.getHandlers())) {
            List<String> empIdList =
                planningSourceModel.getHandlers().stream().map(UserModel::getEmpId).collect(Collectors.toList());
            if (empIdList.contains(empId)) {
                return true;
            }
        }
        return false;
    }

    private static <T> RPCResult buildRPCResult(List<T> data, int items, int itemsPerPage, int page, Boolean editable) {
        Map<String, Object> result = new HashMap<>(4);
        result.put("data", data);
        result.put("paginator", new Paginator(items, itemsPerPage, page));
        if (editable != null) {
            result.put("editable", editable);
        }
        return RPCResult.okNoWrapper(result);
    }
}
