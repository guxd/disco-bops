package com.alibaba.cbuscm.controller.onepiece.opportunity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbuscm.contants.MarketConstants;
import com.alibaba.cbuscm.vo.onepiece.opportunity.MarketPriceVO;
import com.alibaba.cbuscm.vo.onepiece.opportunity.PriceVO;
import com.alibaba.china.dw.dataopen.api.OrderBy;
import com.alibaba.china.dw.dataopen.api.OrderBy.Order;
import com.alibaba.china.dw.dataopen.api.QueryException;
import com.alibaba.china.dw.dataopen.api.Results;
import com.alibaba.china.dw.dataopen.api.SQLIDQueryAPI;
import com.alibaba.china.dw.dataopen.api.User;
import com.alibaba.up.common.mybatis.result.ResultError;

import com.ali.unit.rule.util.lang.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 价格带接口
 * Created by duanyang.zdy on 2019/9/3.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class MarketPriceController {

    @Resource
    private SQLIDQueryAPI sqlidQueryService;

    private static final String DEFAULT_PRICE = "0";


    @RequestMapping("marketDetail/queryMarketPriceIndex")
    public Object queryMarketPriceIndex(@RequestParam(name = "statDate") String statDate,
                                       @RequestParam(name = "marketId") String marketId,
                                       @RequestParam(name = "isGlobal", defaultValue = "1") String isGlobal,
                                       HttpServletRequest request) {

        //校验参数
        if (StringUtils.isBlank(statDate) || StringUtils.isBlank(marketId)) {
            return ResultOf.error(ResultError.MSG_CODE_PARAM_ERROR, "param is null");
        }

        // 建立查询条件
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("stat_date", statDate);
        conditions.put("market_id", marketId);
        //设置返回类型
        String[] return_market_price_fields = {
            "stat_date", "market_id", "rank", "is_global", "market_type",
            "item_cnt", "market_name", "price_max", "price_min", "sale30"};

        User user = getUserInfo();

        try {

            //获取价格带查询列表
            List<OrderBy> orderByList = new ArrayList<>();
            orderByList.add(new OrderBy("rank", Order.ASC));

            Results PriceListResults = sqlidQueryService.list(user, MarketConstants.MARKET_PRICE_SQL_ID, conditions,
                return_market_price_fields, orderByList);
            if (PriceListResults == null || PriceListResults.getResult() == null || PriceListResults.getResult()
                .isEmpty()) {
                return ResultOf.error(ResultError.MSG_CODE_NOT_FOUND, PriceListResults.getMessage());
            }
            if (!PriceListResults.isSuccessed()) {
                return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, PriceListResults.getMessage());
            }

            return ResultOf.general(buildMarketItemVO(PriceListResults.getResult()));

        } catch (QueryException e) {

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, "query 出现异常 " + e.getMessage());

        } catch (Exception e) {

            return ResultOf.error(ResultError.MSG_CODE_SERVER_ERROR, "query 出现异常 " + e.getMessage());
        }
    }

    //生成OneService User信息
    static User getUserInfo() {
        User user = new User();
        user.setAppName(MarketConstants.AppName);
        user.setName(MarketConstants.AppName);
        user.setPassword(MarketConstants.Pass);
        return user;
    }

    private MarketPriceVO buildMarketItemVO(List<Map<String, Object>> maps){

        List<PriceVO> listPrices = new ArrayList<>();


        maps.stream().forEach(map->{

            PriceVO priceVO = PriceVO.builder()
                .priceMax(map.get("PRICE_MAX")!=null?String.valueOf(map.get("PRICE_MAX")):DEFAULT_PRICE)
                .priceMin(map.get("PRICE_MIN")!=null?String.valueOf(map.get("PRICE_MIN")):DEFAULT_PRICE)
                .productSales(map.get("SALE30")!=null?String.valueOf(map.get("SALE30")):DEFAULT_PRICE)
                .productNum(map.get("ITEM_CNT")!=null?String.valueOf(map.get("ITEM_CNT")):DEFAULT_PRICE)
                .rank(map.get("RANK")!=null?String.valueOf(map.get("RANK")):DEFAULT_PRICE)
                .build();


        listPrices.add(priceVO);
        });

        MarketPriceVO vo = MarketPriceVO.builder()
            .marketId(String.valueOf(maps.get(0).get("MARKET_ID")))
            .marketName(String.valueOf(maps.get(0).get("MARKET_NAME")))
            .statDate(String.valueOf(maps.get(0).get("STAT_DATE")))
            .data(listPrices)
            .build();

        return vo;

    }

}
