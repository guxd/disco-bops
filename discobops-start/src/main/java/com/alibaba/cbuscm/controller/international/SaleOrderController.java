package com.alibaba.cbuscm.controller.international;

import com.alibaba.cbu.disco.channel.api.constants.ChannelMarketEnum;
import com.alibaba.cbu.disco.channel.api.constants.ShopConfigStatusEnum;
import com.alibaba.cbu.disco.channel.api.model.ChannelBaseResult;
import com.alibaba.cbu.disco.channel.api.model.ChannelPageResult;
import com.alibaba.cbu.disco.channel.api.model.ChannelShopDTO;
import com.alibaba.cbu.disco.channel.api.model.MarketAdapterDTO;
import com.alibaba.cbu.disco.channel.api.model.SubMarketInfoDTO;
import com.alibaba.cbu.disco.channel.api.param.ChannelShopQueryParam;
import com.alibaba.cbu.disco.channel.api.service.DiscoChannelShopService;
import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.core.config.api.FsShopConfigService;
import com.alibaba.cbu.disco.shared.core.config.model.DomesticAddressConfigModel;
import com.alibaba.cbu.disco.shared.core.config.model.FsShopConfigModel;
import com.alibaba.cbu.disco.shared.core.config.model.OrderTradeInfoModel;
import com.alibaba.cbu.disco.shared.core.order.api.FsChannelOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPerformOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsPurchaseOrderService;
import com.alibaba.cbu.disco.shared.core.order.api.FsTradeInfoService;
import com.alibaba.cbu.disco.shared.core.order.constant.FsChannelOrderBizStatus;
import com.alibaba.cbu.disco.shared.core.order.constant.FsPerformStatusEnum;
import com.alibaba.cbu.disco.shared.core.order.model.data.*;
import com.alibaba.cbu.disco.shared.core.order.model.view.FsChannelPageQueryCondition;
import com.alibaba.cbu.disco.shared.core.order.model.view.FsChannelPageQueryParam;
import com.alibaba.cbuscm.config.OrderChannelDiamondConfiguration;
import com.alibaba.cbuscm.vo.common.PageVo;
import com.alibaba.cbuscm.vo.international.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xuhb
 * @title: SaleOrderController
 * @projectName discobops
 * @description: 销售单服务类
 * @date 2019-05-0910:40
 */
@RestController
@Slf4j
@RequestMapping("/saleOrder")
public class SaleOrderController {

    private static final int ONE_DAY_TIME = 24 * 60 * 60 * 1000;

    @Autowired
    private FsChannelOrderService channelOrderService;

    @Autowired
    private FsPurchaseOrderService purchaseOrderService;

    @Autowired
    private FsPerformOrderService performOrderService;

    @Autowired
    private FsShopConfigService fsShopConfigService;

    @Autowired
    private DiscoChannelShopService discoChannelShopService;

    @Autowired
    private FsTradeInfoService fsTradeInfoService;

    @Resource
    private OrderChannelDiamondConfiguration orderChannelDiamondConfiguration;

    /**
     * 获取采购单状态
     *
     * @return
     */
    @RequestMapping("/status")
    public RPCResult getSaleOrderStatus() {
        FsChannelOrderBizStatus[] states = FsChannelOrderBizStatus.values();
        List<SaleOrderStateVO> result = new ArrayList<SaleOrderStateVO>();
        Arrays.asList(states).stream().forEach(state -> {
            SaleOrderStateVO vo = new SaleOrderStateVO();
            vo.setName(state.getDesc());
            vo.setValue(state.getValue());
            result.add(vo);
        });
        return RPCResult.ok(result);
    }


    @RequestMapping("/getChannelMarketList")
    public RPCResult getChannelMarketList() {

        List<ChannelDataVO> voList = new ArrayList<>();
        for(ChannelMarketEnum channelMarketEnum:ChannelMarketEnum.values()){
            if(!orderChannelDiamondConfiguration.getOldMarketCodes().contains(channelMarketEnum.getOldMarketCode())){
                continue;
            }
            ChannelDataVO vo = new ChannelDataVO();
            vo.setChannelCode(channelMarketEnum.getOldMarketCode());
            vo.setChannelName(channelMarketEnum.getDesc());

            voList.add(vo);
        }

        return RPCResult.ok(voList);

    }


    /**
     * 本方法已经演进为展示店铺列表，不是子市场，由于前端无资源，所以暂不改名
     * @param channelId     渠道ID(一级渠道oldMarketCode)
     * @return
     */
    @RequestMapping("/getChannelSubMarketList")
    public RPCResult getChannelSubMarketList(@RequestParam(name = "channelId", required = false) String channelId) {

        long current = System.currentTimeMillis();
        log.info("ENTRY@current="+current);
        try{
            //目前前端并没有传channelId，即使传也是按照订单域来传
            ChannelMarketEnum channelMarketEnum = null;

            if(StringUtils.isBlank(channelId)) {
                channelMarketEnum = ChannelMarketEnum.AE;
            }
            else {
                channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);
            }

            List<ChannelDataVO> voList = Lists.newArrayList();

            Map<String, OrderTradeInfoModel> tradeInfoModelMap = fsTradeInfoService.listTradeInfo();
            if(MapUtils.isEmpty(tradeInfoModelMap)) {
                return RPCResult.ok(voList);
            }

            //key是oldchannelSubMarket
            Map<String, SubMarketInfoDTO> channelSubMarketMap = queryChannelSubMarketMap(channelMarketEnum.name());
            if(channelSubMarketMap == null) {
                channelSubMarketMap = new HashMap<>();
            }

            //获取当前所有的店铺
            List<ChannelShopDTO> shopList = listAllShopsByChannel(channelMarketEnum.name());

            if(shopList != null && shopList.size() > 0) {

                /**
                 //首先增加主市场
                 String mainSellerId = fetchChannelSellerId(tradeInfoModelMap, channelMarketEnum.getOldMarketCode(), null);
                 if(StringUtils.isBlank(mainSellerId)) {
                 return RPCResult.ok(voList);
                 }

                 ChannelDataVO mainVO = new ChannelDataVO();
                 mainVO.setChannelCode(channelMarketEnum.getOldMarketCode());
                 mainVO.setChannelName(channelMarketEnum.getDesc());
                 mainVO.setChannelSellerId(mainSellerId);
                 mainVO.setChannelSubMarket(null);
                 mainVO.setChannelSubName(null);
                 mainVO.setShopId(null);
                 mainVO.setShopName(null);
                 voList.add(mainVO);
                 **/
                //然后增加店铺
                for(ChannelShopDTO curSub : shopList) {
                    if(curSub == null) {
                        continue;
                    }

                    ChannelBaseResult<MarketAdapterDTO> curQueryResult = discoChannelShopService.adaptMarketInfo(curSub.getMarket(), curSub.getSubMarket());
                    if(curQueryResult != null && curQueryResult.isSuccess() && curQueryResult.getData() != null) {

                        MarketAdapterDTO marketAdapterDTO = curQueryResult.getData();

                        String curSellerId = fetchChannelSellerId(tradeInfoModelMap, marketAdapterDTO.getOldMarketCode(), marketAdapterDTO.getOldSubMarketCode());
                        if(StringUtils.isBlank(curSellerId)) {
                            continue;
                        }

                        ChannelDataVO subVO = new ChannelDataVO();
                        subVO.setChannelCode(channelMarketEnum.getOldMarketCode());
                        subVO.setChannelName(channelMarketEnum.getDesc());

                        String oldChannelSubMarket = marketAdapterDTO.getOldSubMarketCode();
                        SubMarketInfoDTO subMarketInfoDTO = channelSubMarketMap.get(oldChannelSubMarket);
                        if(subMarketInfoDTO==null){
                            log.warn("WARN@getChannelSubMarketList,subMarketInfoDTO is null,oldChannelSubMarket={},marketAdapterDTO={},curSub={}",new Object[]{oldChannelSubMarket,marketAdapterDTO, JSON.toJSONString(curSub)});
                            continue;
                        }
                        subVO.setChannelSubMarket(oldChannelSubMarket);
                        subVO.setChannelSubName(subMarketInfoDTO.getSubMarketDesc());

                        subVO.setShopId(curSub.getShopId());
                        subVO.setShopName(curSub.getShopName());

                        subVO.setChannelSellerId(curSellerId);
                        voList.add(subVO);
                    }
                    else {
                        continue;
                    }
                }
            }
            else {
                log.info("OK@current="+current);
                return RPCResult.ok(voList);
            }
            log.info("OK@current="+current);
            return RPCResult.ok(voList);
        }catch (Throwable e){
            log.info("ERROR@current="+current,e);
        }
        return RPCResult.ok(null);

    }

    /**
     * 罗列所有的店铺
     * @param channelMarket 标准市场名
     */
    private List<ChannelShopDTO> listAllShopsByChannel(String channelMarket) {

        String logPrefix = "SaleOrderController#listAllShopsByChannel#";
        String paramInfo = "#cm:" + channelMarket + "#";

        List<ChannelShopDTO> resultList = new ArrayList<>();

        //先获得总量，再通过多次循环，全部获取

        ChannelShopQueryParam queryParam = new ChannelShopQueryParam();
        queryParam.setChannelMarket(channelMarket);
        queryParam.setStatus(ShopConfigStatusEnum.BOUND.name());

        ChannelBaseResult<Integer> countResult = discoChannelShopService.count(queryParam);

        if(!countResult.isSuccess()) {
            log.error(logPrefix + "count_fail_" + countResult.getErrorCode() + paramInfo);
            return resultList;
        }

        Integer totalSize = countResult.getData();

        //分页从第一页开始
        int pageSize = 20;
        int totalPage = totalSize % pageSize > 0 ? totalSize / pageSize + 1 : totalSize / pageSize;
        int idx = 1;

        //for循环逐页查
        queryParam.setPageSize(pageSize);
        while (idx <= totalPage) {
            queryParam.setPageIdx(idx);

            ChannelBaseResult<ChannelPageResult<ChannelShopDTO>> curInvokeResult = discoChannelShopService.listChannelShopsByPage(queryParam);

            if(!curInvokeResult.isSuccess()) {
                log.error(logPrefix + "query_fail_" + curInvokeResult.getErrorCode() + paramInfo);
                break;
            }

            ChannelPageResult<ChannelShopDTO> curPageResult = curInvokeResult.getData();
            if(curPageResult.getData() == null) {
                log.error(logPrefix + "query_page_null" + paramInfo);
                break;
            }
            resultList.addAll(curPageResult.getData());

            //下一页
            ++idx;
        }

        //做一次for循环过滤
        List<ChannelShopDTO> filterList = new ArrayList<>();
        for(ChannelShopDTO curShopDTO : resultList) {
            if(curShopDTO == null) {
                continue;
            }

            if(curShopDTO.getShopId() == null || curShopDTO.getShopId() <= 0) {
                continue;
            }
            if(!ShopConfigStatusEnum.BOUND.name().equalsIgnoreCase(curShopDTO.getStatus())){
                continue;
            }

            filterList.add(curShopDTO);
        }

        //做一次基于名称的排序
        Collections.sort(filterList, new Comparator<ChannelShopDTO>() {
            @Override
            public int compare(ChannelShopDTO o1, ChannelShopDTO o2) {

                String shopName1 = o1.getShopName();
                String shopName2 = o2.getShopName();

                if(shopName1 == null) {
                    return -1;
                }
                if(shopName2 == null) {
                    return 1;
                }
                return shopName1.compareTo(shopName2);
            }
        });

        return filterList;
    }

    private Map<Long, ChannelShopDTO> fetchAllShopsByChannel(String channelMarket) {

        List<ChannelShopDTO> shopDTOList = listAllShopsByChannel(channelMarket);

        Map<Long, ChannelShopDTO> shopMap = new HashMap<>();

        if(CollectionUtils.isEmpty(shopDTOList)) {
            return shopMap;
        }

        for(ChannelShopDTO curDTO : shopDTOList) {
            if(curDTO == null) {
                continue;
            }

            shopMap.put(curDTO.getShopId(), curDTO);
        }

        return shopMap;
    }


    private String fetchChannelSellerId(String channelMarket, String channelSubMarket) {

        Map<String, OrderTradeInfoModel> tradeInfoModelMap = fsTradeInfoService.listTradeInfo();
        return fetchChannelSellerId(tradeInfoModelMap, channelMarket, channelSubMarket);
    }


    private String fetchChannelSellerId(Map<String, OrderTradeInfoModel> tradeInfoModelMap, String channelMarket, String channelSubMarket) {

        if(MapUtils.isEmpty(tradeInfoModelMap) || StringUtils.isBlank(channelMarket)) {
            return null;
        }

        StringBuilder key = new StringBuilder();
        key.append(channelMarket);
        if(StringUtils.isNotBlank(channelSubMarket)) {
            key.append("#").append(channelSubMarket);
        }

        OrderTradeInfoModel curModel = tradeInfoModelMap.get(key.toString());
        if(curModel == null) {
            curModel = tradeInfoModelMap.get("DEFAULT");
        }

        return curModel == null ? null : curModel.getChannelSellerId();
    }

    /**
     * 查询从old到new的映射
     * @param uniChannelMarket
     * @return
     */
    private Map<String, SubMarketInfoDTO> queryChannelSubMarketMap(String uniChannelMarket) {

        Map<String, SubMarketInfoDTO> resultMap = new HashMap<>();

        ChannelBaseResult<List<SubMarketInfoDTO>> result = discoChannelShopService.listSubMarketInfosByMarket(uniChannelMarket,true);

        if(result != null && result.isSuccess() && result.getData() != null) {
            for(SubMarketInfoDTO curDTO : result.getData()) {
                if(curDTO == null) {
                    continue;
                }

                ChannelBaseResult<MarketAdapterDTO> curResult = discoChannelShopService.adaptMarketInfo(curDTO.getMarketCode(), curDTO.getSubMarketCode());
                if(curResult != null && curResult.isSuccess() && curResult.getData() != null) {
                    resultMap.put(curResult.getData().getOldSubMarketCode(), curDTO);
                }
                else {
                    continue;
                }
            }
        }

        return resultMap;
    }


    /**
     * @param channelId     渠道ID
     * @param saleOrderId   销售单id
     * @param channelSaleId 采购单id
     * @param status        状态
     * @param createTime    采购单生成时间
     * @return
     */
    @RequestMapping("/getQGOrderList")
    public RPCResult getQGOrderList(@RequestParam(name = "channelId", required = false) String channelId,
                                    @RequestParam(name = "channelSubMarket", required = false) String channelSubMarket,
                                    @RequestParam(name = "shopId", required = false) Long shopId,
                                    @RequestParam(name = "saleOrderId", required = false) String saleOrderId,
                                    @RequestParam(name = "id", required = false) Long channelSaleId,
                                    @RequestParam(name = "status", required = false) String status,
                                    @RequestParam(name = "createTime", required = false) Long createTime,
                                    @RequestParam(name = "pageNo", required = false) Integer page,
                                    @RequestParam(name = "pageSize", required = false) Integer pageSize) {

        /*List<SaleOrderVO> saleOrderVos = getSaleOrderVos();
        RPCResult result = RPCResult.ok(saleOrderVos);
        PageVo pageVo = new PageVo();
        pageVo.setPageNo(1);
        pageVo.setPageSize(50);
        pageVo.setTotal(10);
        result.put("paginator", pageVo);
        return result;*/


        FsChannelPageQueryParam param = new FsChannelPageQueryParam();
        param.setChannel(channelId);

        ChannelMarketEnum channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);

        //key是shopSellerId
        Map<Long, ChannelShopDTO> channelShopDTOMap =  fetchAllShopsByChannel(channelMarketEnum.name());

        //key是oldChannelSubMarket
        Map<String, SubMarketInfoDTO> channelSubMarketMap = queryChannelSubMarketMap(channelMarketEnum.name());


        String channelSellerId = fetchChannelSellerId(channelId, channelSubMarket);
        if(channelSellerId != null) {
            param.setChannel(channelId);
            //param.setChannelSubMarket(channelSubMarket);

            if(shopId != null  && shopId != 0L) {
                param.setChannelActualSellerId(shopId.toString());
            }
            param.setChannelSellerId(channelSellerId);
        }

        FsChannelPageQueryCondition condition = new FsChannelPageQueryCondition();
        condition.setChannelOrderId(saleOrderId);
        if (channelSaleId != null && channelSaleId > 0) {
            condition.setChannelMainDbId(channelSaleId);
        }
        if (page == null || page < 1) {
            page = 1;
        }
        if (pageSize == null || pageSize < 1 || pageSize > 100) {
            pageSize = 20;
        }
        if (!StringUtils.isEmpty(status)) {
            Set states = new HashSet();
            states.add(status);
            condition.setBizStatusSet(states);
        }
        Date dateFrom = new Date();
        if (createTime != null && createTime > 0) {
            Date beginDa = new Date(createTime);
            Date endDa = new Date(createTime + ONE_DAY_TIME);
            condition.setCreateTimeBegin(beginDa);
            condition.setCreateTimeEnd(endDa);
        }
        param.setCondition(condition);
        param.setPageIdx(page);
        param.setPageSize(pageSize);
        log.info("list channel order , param - " + param.toString());
        PageOf<FsChannelOrderModel> channelOrderPage = channelOrderService.listChannelOrder(param);
        log.info("list channel order , result -" + channelOrderPage.toString());
        if (channelOrderPage == null || CollectionUtils.isEmpty(channelOrderPage.getData())) {
            List<SaleOrderVO> saleOrderVos = Lists.newArrayList();
            RPCResult result = RPCResult.ok(saleOrderVos);
            PageVo pageVo = new PageVo();
            pageVo.setPageNo(page);
            pageVo.setPageSize(pageSize);
            pageVo.setTotal(0);
            result.put("paginator", pageVo);
            return result;
        }
        /*************************************获取销售单中有效的采购单-start*********************************************/
        final List<FsChannelOrderSubModel> subChannelOrderList = Lists.newArrayList();
        channelOrderPage.getData().stream().forEach(channelOrder -> {
                    //无取消
                    if (!FsChannelOrderBizStatus.ALL_CANCEL.getValue().equals(channelOrder.getBizStatus()) && !CollectionUtils.isEmpty(channelOrder.getSubOrderList())) {
                        subChannelOrderList.addAll(channelOrder.getSubOrderList());
                    }
                }
        );
        //List<FsChannelOrderSubModel> subChannelOrderAvaliableList = subChannelOrderList.stream().filter(FsChannelOrderSubModel::isPurchased).collect(Collectors.toList());
        List<FsChannelOrderSubModel> subChannelOrderAvaliableList = subChannelOrderList;
        /*************************************获取销售单中有效的采购单-end*********************************************/
        /******************************卖家id(ae大店主体) 类型*************************/
        String sellerId = channelOrderPage.getData().get(0).getSellerId();
        String bizType = channelOrderPage.getData().get(0).getChannelMarket();
        /**********************************************************/
        //分批次查询 每批次20个
        List<List<FsChannelOrderSubModel>> parts = Lists.partition(subChannelOrderAvaliableList, 20);
        log.info("channel sub order avaliable number - " + subChannelOrderAvaliableList.size());
        //purchaseid -> sub purchase platform ids
        //final Map<Long,List<Long>> purchaseOrderMap = new HashMap<Long,List<Long>>();
        final Map<Long, FsPerformSubOrderModel> performOrderMap = new HashMap<Long, FsPerformSubOrderModel>();
        //销售单id -> 子采购单 用于计算采购价格
        final Map<String, List<FsPurchaseSubOrderModel>> purchaseSubOrderMaps = Maps.newHashMap();

        //分批次获取
        for (List<FsChannelOrderSubModel> part : parts) {
            //采购单id
            List<Long> purchaseIds = part.stream().map(subModel -> subModel.getPurchaseId()).collect(Collectors.toList());
            log.info("query purchase order begin,param - " + purchaseIds.size());
            if (CollectionUtils.isEmpty(purchaseIds)) {
                continue;
            }
            //获取采购单信息
            ListOf<FsPurchaseOrderModel> purchaseOrderList = purchaseOrderService.queryPurchaseOrder(sellerId, bizType, purchaseIds, true);
            if (purchaseOrderList != null && !CollectionUtils.isEmpty(purchaseOrderList.getData())) {
                //根据采购单获取所有子采购单关联的履约单ID
                final List<Long> performIdList = new ArrayList<Long>();
                purchaseOrderList.getData().stream().forEach(purchaseOrder -> {
                    if (!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList())) {
                        for (FsPurchaseSubOrderModel purchaseSub : purchaseOrder.getSubOrderList()) {
                            if (purchaseSub.getPerformId() != null && purchaseSub.getPerformId() != 0) {
                                performIdList.add(purchaseSub.getPerformId());
                                //放入sale order id -> sub chase order
                                if (purchaseSubOrderMaps.containsKey(purchaseSub.getChannelOrderId())) {
                                    purchaseSubOrderMaps.get(purchaseSub.getChannelOrderId()).add(purchaseSub);
                                } else {
                                    List purchaseSubOrder = Lists.newArrayList(purchaseSub);
                                    purchaseSubOrderMaps.put(purchaseSub.getChannelOrderId(), purchaseSubOrder);
                                }

                            }
                        }
                    }
                });
                if (CollectionUtils.isEmpty(performIdList)) {
                    continue;
                }

                List<List<Long>> performIdPartList = Lists.partition(performIdList, 20);
                //循环获取 并放入performOrderMap 中
                for (List<Long> performIdPart : performIdPartList) {
                    ListOf<FsPerformOrderModel> performOrderList = performOrderService.queryPerformOrder(sellerId, bizType, performIdPart);
                    if (performOrderList == null || CollectionUtils.isEmpty(performOrderList.getData())) {
                        continue;
                    }
                    performOrderList.getData().stream().forEach(performOrder -> {
                        if (!CollectionUtils.isEmpty(performOrder.getSubOrderList())) {
                            for (FsPerformSubOrderModel subPerform : performOrder.getSubOrderList()) {
                                performOrderMap.put(subPerform.getId(), subPerform);
                            }
                        }
                    });

                }
            }
        }



        List<SaleOrderVO> saleOrderVos = channelOrderPage.getData().stream().map(channelOrder -> convertChannelOrderToSaleOrderVO(channelOrder, purchaseSubOrderMaps, performOrderMap, channelShopDTOMap, channelSubMarketMap)).collect(Collectors.toList());
        RPCResult result = RPCResult.ok(saleOrderVos);
        PageVo pageVo = new PageVo();
        pageVo.setPageNo(page);
        pageVo.setPageSize(pageSize);
        pageVo.setTotal(channelOrderPage.getTotal());
        result.put("paginator", pageVo);
        return result;
    }

    /*private List<SaleOrderVO> getSaleOrderVos() {
        List<SaleOrderVO> saleOrderVos = new ArrayList<>();
        SaleOrderVO saleOrderVO = new SaleOrderVO();
        saleOrderVO.setChannelName("www");
        saleOrderVO.setChannelSellerId("888www");
        saleOrderVO.setChannelSubName("wwwwrrr1");
        saleOrderVO.setId("7777888");
        saleOrderVO.setQuantity(1);
        saleOrderVO.setReceiveQuantity(5);
        saleOrderVO.setSaleOrderId("ww8");
        saleOrderVO.setStatus("ww");
        saleOrderVO.setStatusName("待发货");
        saleOrderVO.setTotalPay("55");
        saleOrderVos.add(saleOrderVO);
        return saleOrderVos;
    }*/

    /**
     * @param saleOrderId   销售单id
     * @param channelSaleId 采购单id
     * @param status        状态
     * @param createTime    采购单生成时间
     * @return
     */
    @RequestMapping("/getQGAEOrderList")
    public RPCResult getQGAEOrderList(@RequestParam(name = "channelId", required = false) String channelId,
                                      @RequestParam(name = "saleOrderId", required = false) String saleOrderId,
                                      @RequestParam(name = "id", required = false) Long channelSaleId,
                                      @RequestParam(name = "status", required = false) String status,
                                      @RequestParam(name = "createTime", required = false) Long createTime,
                                      @RequestParam(name = "pageNo", required = false) Integer page,
                                      @RequestParam(name = "pageSize", required = false) Integer pageSize) {

        ChannelMarketEnum channelMarketEnum = null;

        if(StringUtils.isBlank(channelId)) {
            channelMarketEnum = ChannelMarketEnum.AE;
        }
        else {
            channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(channelId);
        }
        //key是shopSellerId
        Map<Long, ChannelShopDTO> channelShopDTOMap =  fetchAllShopsByChannel(channelMarketEnum.name());
        String channelSellerId = fetchChannelSellerId(channelMarketEnum.getOldMarketCode(), null);

        //key是OldchannelSubMarket
        Map<String, SubMarketInfoDTO> channelSubMarketMap = queryChannelSubMarketMap(channelMarketEnum.name());

        FsChannelPageQueryParam param = new FsChannelPageQueryParam();
        param.setChannel(channelMarketEnum.getOldMarketCode());
        param.setChannelSellerId(channelSellerId);
        FsChannelPageQueryCondition condition = new FsChannelPageQueryCondition();
        condition.setChannelOrderId(saleOrderId);
        if (channelSaleId != null && channelSaleId > 0) {
            condition.setChannelMainDbId(channelSaleId);
        }
        if (page == null || page < 1) {
            page = 1;
        }
        if (pageSize == null || pageSize < 1 || pageSize > 100) {
            pageSize = 20;
        }
        if (!StringUtils.isEmpty(status)) {
            Set states =  new HashSet();
            states.add(status);
            condition.setBizStatusSet(states);
        }
        Date dateFrom = new Date();
        if (createTime != null && createTime > 0) {
            Date beginDa = new Date(createTime);
            Date endDa = new Date(createTime + ONE_DAY_TIME);
            condition.setCreateTimeBegin(beginDa);
            condition.setCreateTimeEnd(endDa);
        }
        param.setCondition(condition);
        param.setPageIdx(page);
        param.setPageSize(pageSize);
        log.info("list channel order , param - " + param.toString());
        PageOf<FsChannelOrderModel> channelOrderPage = channelOrderService.listChannelOrder(param);
        log.info("list channel order , result -" + channelOrderPage.toString());
        if (channelOrderPage == null || CollectionUtils.isEmpty(channelOrderPage.getData())) {
            List<SaleOrderVO> saleOrderVos = Lists.newArrayList();
            RPCResult result = RPCResult.ok(saleOrderVos);
            PageVo pageVo = new PageVo();
            pageVo.setPageNo(page);
            pageVo.setPageSize(pageSize);
            pageVo.setTotal(0);
            result.put("paginator", pageVo);
            return result;
        }
        /*************************************获取销售单中有效的采购单-start*********************************************/
        final List<FsChannelOrderSubModel> subChannelOrderList = Lists.newArrayList();
        channelOrderPage.getData().stream().forEach(channelOrder -> {
                    //无取消
                    if (!FsChannelOrderBizStatus.ALL_CANCEL.getValue().equals(channelOrder.getBizStatus()) && !CollectionUtils.isEmpty(channelOrder.getSubOrderList())) {
                        subChannelOrderList.addAll(channelOrder.getSubOrderList());
                    }
                }
        );
        List<FsChannelOrderSubModel> subChannelOrderAvaliableList = subChannelOrderList.stream().filter(FsChannelOrderSubModel::isPurchased).collect(Collectors.toList());
        /*************************************获取销售单中有效的采购单-end*********************************************/
        /******************************卖家id(ae大店主体) 类型*************************/
        String sellerId = channelOrderPage.getData().get(0).getSellerId();
        String bizType = channelOrderPage.getData().get(0).getChannelMarket();
        /**********************************************************/
        //分批次查询 每批次20个
        List<List<FsChannelOrderSubModel>> parts = Lists.partition(subChannelOrderAvaliableList, 20);
        log.info("channel sub order avaliable number - " + subChannelOrderAvaliableList.size());
        //purchaseid -> sub purchase platform ids
        //
//        final Map<Long,List<Long>> purchaseOrderMap = new HashMap<Long,List<Long>>();
        final Map<Long, FsPerformSubOrderModel> performOrderMap = new HashMap<Long, FsPerformSubOrderModel>();
        //销售单id -> 子采购单 用于计算采购价格
        final Map<String, List<FsPurchaseSubOrderModel>> purchaseSubOrderMaps = Maps.newHashMap();

        //分批次获取
        for (List<FsChannelOrderSubModel> part : parts) {
            //采购单id
            List<Long> purchaseIds = part.stream().map(subModel -> subModel.getPurchaseId()).collect(Collectors.toList());
            log.info("query purchase order begin,param - " + purchaseIds.size());
            if (CollectionUtils.isEmpty(purchaseIds)) {
                continue;
            }
            //获取采购单信息
            ListOf<FsPurchaseOrderModel> purchaseOrderList = purchaseOrderService.queryPurchaseOrder(sellerId, bizType, purchaseIds, true);
            if (purchaseOrderList != null && !CollectionUtils.isEmpty(purchaseOrderList.getData())) {
                //根据采购单获取所有子采购单关联的履约单ID
                final List<Long> performIdList = new ArrayList<Long>();
                purchaseOrderList.getData().stream().forEach(purchaseOrder -> {
                    if (!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList())) {
                        for (FsPurchaseSubOrderModel purchaseSub : purchaseOrder.getSubOrderList()) {
                            if (purchaseSub.getPerformId() != null && purchaseSub.getPerformId() != 0) {
                                performIdList.add(purchaseSub.getPerformId());
                                //放入sale order id -> sub chase order
                                if (purchaseSubOrderMaps.containsKey(purchaseSub.getChannelOrderId())) {
                                    purchaseSubOrderMaps.get(purchaseSub.getChannelOrderId()).add(purchaseSub);
                                } else {
                                    List purchaseSubOrder = Lists.newArrayList(purchaseSub);
                                    purchaseSubOrderMaps.put(purchaseSub.getChannelOrderId(), purchaseSubOrder);
                                }
                            }
                        }
                    }
                });
                if (CollectionUtils.isEmpty(performIdList)) {
                    continue;
                }

                List<List<Long>> performIdPartList = Lists.partition(performIdList, 20);
                //循环获取 并放入performOrderMap 中
                for (List<Long> performIdPart : performIdPartList) {
                    ListOf<FsPerformOrderModel> performOrderList = performOrderService.queryPerformOrder(sellerId, bizType, performIdPart);
                    if (performOrderList == null || CollectionUtils.isEmpty(performOrderList.getData())) {
                        continue;
                    }
                    performOrderList.getData().stream().forEach(performOrder -> {
                        if (!CollectionUtils.isEmpty(performOrder.getSubOrderList())) {
                            for (FsPerformSubOrderModel subPerform : performOrder.getSubOrderList()) {
                                performOrderMap.put(subPerform.getId(), subPerform);
                            }
                        }
                    });

                }
            }
        }

        List<SaleOrderVO> saleOrderVos = channelOrderPage.getData().stream().map(channelOrder -> convertChannelOrderToSaleOrderVO(channelOrder, purchaseSubOrderMaps, performOrderMap, channelShopDTOMap, channelSubMarketMap)).collect(Collectors.toList());
        RPCResult result = RPCResult.ok(saleOrderVos);
        PageVo pageVo = new PageVo();
        pageVo.setPageNo(page);
        pageVo.setPageSize(pageSize);
        pageVo.setTotal(channelOrderPage.getTotal());
        result.put("paginator", pageVo);
        return result;

    }


    /**
     * @param id 销售单id
     * @return
     */
    @RequestMapping("/getQGOrderDetail")
    public RPCResult getQGOrderDetail(@RequestParam(name = "id", required = true) long id,@RequestParam(name = "channelSellerId", required = true) String channelSellerId,@RequestParam(name = "channelId", required = false) String channelId) {

        // mock
        /*SaleOrderDetailVO saleOrderDetailVO = getSaleOrderDetailVO();
        return RPCResult.ok(saleOrderDetailVO);*/


        FsChannelPageQueryParam param = new FsChannelPageQueryParam();
        FsChannelPageQueryCondition condition = new FsChannelPageQueryCondition();
        condition.setChannelMainDbId(id);
        param.setCondition(condition);
        param.setChannelSellerId(channelSellerId);
        if(!StringUtils.isBlank(channelId)){
            param.setChannel(channelId);
        }
        /*******************************************************与上面的方法是一致的**************************************************************/
        log.info("list channel order , param - " + param.toString());
        PageOf<FsChannelOrderModel> channelOrderPage = channelOrderService.listChannelOrder(param);
        log.info("list channel order , result -" + channelOrderPage.toString());
        if (channelOrderPage == null || CollectionUtils.isEmpty(channelOrderPage.getData())) {
            return RPCResult.ok();
        }
        FsChannelOrderModel orderModel = channelOrderPage.getData().get(0);
        /******************************卖家id(ae大店主体) 类型*************************/
        String sellerId = orderModel.getSellerId();
        String bizType = orderModel.getChannelMarket();
        /**********************************************************/
        //sub sale order id ->  purchase sub order
        final Map<String, List<FsPurchaseSubOrderModel>> subSaleOrderMap = new HashMap<String, List<FsPurchaseSubOrderModel>>();


        //sub purchaseid -> sub purchase platform ids
        //final Map<String, Long> subPurchaseOrderMap = new HashMap<String, Long>();
        final Map<Long, FsPerformSubOrderModel> performOrderMap = new HashMap<Long, FsPerformSubOrderModel>();
        //sub purchase id -> supper id
        final Map<Long, String> purchaseSupplierMap = Maps.newHashMap();
        //sub perform id -> purchase  id
        List<FsChannelOrderSubModel> subChannelOrderAvaliableList = orderModel.getSubOrderList();
        if (subChannelOrderAvaliableList != null && !CollectionUtils.isEmpty(subChannelOrderAvaliableList)) {
            //分批次查询 每批次20个
            List<List<FsChannelOrderSubModel>> parts = Lists.partition(subChannelOrderAvaliableList, 20);
            log.info("channel sub order avaliable number - " + subChannelOrderAvaliableList.size());
            //分批次获取
            for (List<FsChannelOrderSubModel> part : parts) {
                //采购单id
                List<Long> purchaseIds = part.stream().map(subModel -> subModel.getPurchaseId()).collect(Collectors.toList());
                log.info("query purchase order begin,param - " + purchaseIds.size());
                if (CollectionUtils.isEmpty(purchaseIds)) {
                    continue;
                }
                //获取采购单信息
                ListOf<FsPurchaseOrderModel> purchaseOrderList = purchaseOrderService.queryPurchaseOrder(sellerId, bizType, purchaseIds, true);
                if (purchaseOrderList != null && !CollectionUtils.isEmpty(purchaseOrderList.getData())) {
                    //根据采购单获取所有子采购单关联的履约单ID
                    final List<Long> performIdList = new ArrayList<Long>();
                    purchaseOrderList.getData().stream().forEach(purchaseOrder -> {
                        if (!CollectionUtils.isEmpty(purchaseOrder.getSubOrderList())) {
                            for (FsPurchaseSubOrderModel purchaseSub : purchaseOrder.getSubOrderList()) {
                                purchaseSupplierMap.put(purchaseSub.getId(), purchaseOrder.getSupplierId());
                                if (purchaseSub.getPerformId() != null && purchaseSub.getPerformId() != 0) {
                                    performIdList.add(purchaseSub.getPerformId());
                                    if (subSaleOrderMap.containsKey(purchaseSub.getChannelOrderSubId())) {
                                        subSaleOrderMap.get(purchaseSub.getChannelOrderSubId()).add(purchaseSub);
                                    } else {
                                        List<FsPurchaseSubOrderModel> purchaseOrderIdList = Lists.newArrayList(purchaseSub);
                                        subSaleOrderMap.put(purchaseSub.getChannelOrderSubId(), purchaseOrderIdList);
                                    }
                                }
                            }
                        }
                    });
                    if (CollectionUtils.isEmpty(performIdList)) {
                        continue;
                    }

                    List<List<Long>> performIdPartList = Lists.partition(performIdList, 20);
                    //循环获取 并放入performOrderMap 中
                    for (List<Long> performIdPart : performIdPartList) {
                        ListOf<FsPerformOrderModel> performOrderList = performOrderService.queryPerformOrder(sellerId, bizType, performIdPart);
                        if (performOrderList == null || CollectionUtils.isEmpty(performOrderList.getData())) {
                            continue;
                        }
                        performOrderList.getData().stream().forEach(performOrder -> {
                            if (!CollectionUtils.isEmpty(performOrder.getSubOrderList())) {
                                for (FsPerformSubOrderModel subPerform : performOrder.getSubOrderList()) {
                                    performOrderMap.put(subPerform.getId(), subPerform);
                                }
                            }
                        });

                    }
                }
            }
        }
        /**********************************************************/
        SaleOrderDetailVO result = convertSaleDetailVo(orderModel, subSaleOrderMap, performOrderMap,purchaseSupplierMap);
        BuyerVO buyer = new BuyerVO();
        SellerVO seller = new SellerVO();
        FsShopConfigModel shopConfigure = fsShopConfigService.getB10ShopCoinfig();
        buyer.setChannelName(shopConfigure.getChannelName());
        buyer.setCompanyCode(shopConfigure.getCompanyCode());
        buyer.setCompanyName(shopConfigure.getCompanyName());


        seller.setChannelName(shopConfigure.getChannelName());
        seller.setCompanyCode(shopConfigure.getCompanyCode());
        seller.setCompanyName(shopConfigure.getCompanyName());
//        List<AeChannelData> aeChannelDataList = listChannelData(orderModel.getChannelMarket(),orderModel.getChannelSubMarket());
//        if(CollectionUtils.isEmpty(aeChannelDataList)){
//            FsShopConfigModel shopConfigure = fsShopConfigService.getB10ShopCoinfig();
//            buyer.setChannelName(shopConfigure.getChannelName());
//            buyer.setCompanyCode(shopConfigure.getCompanyCode());
//            buyer.setCompanyName(shopConfigure.getCompanyName());
//
//
//            seller.setChannelName(shopConfigure.getChannelName());
//            seller.setCompanyCode(shopConfigure.getCompanyCode());
//            seller.setCompanyName(shopConfigure.getCompanyName());
//        }else{
//            FsShopConfigModel shopConfigure = fsShopConfigService.getB10ShopCoinfig();
//            AeChannelData aeChannelData = aeChannelDataList.get(0);
//            buyer.setChannelName(aeChannelData.getChannelName());
//            buyer.setCompanyCode(shopConfigure.getCompanyCode());
//            buyer.setCompanyName(aeChannelData.getChannelSubName());
//
//
//            seller.setChannelName(shopConfigure.getChannelName());
//            seller.setCompanyCode(shopConfigure.getCompanyCode());
//            seller.setCompanyName(shopConfigure.getCompanyName());
//        }

        DomesticAddressConfigModel domestaicAddress = fsShopConfigService.getDomesticAddressConfig();
        ReceivingAddressModel salerAddess = new ReceivingAddressModel();
        salerAddess.setAddress(domestaicAddress.getAddress());
        salerAddess.setName(domestaicAddress.getName());
        //salerAddess.setTelephone(domestaicAddress.getTelephone());
        salerAddess.setMobile(domestaicAddress.getTelephone());
        result.setBuyer(buyer);
        result.setSeller(seller);
        // 主销售订单编号
        result.setOrderId(orderModel.getOrderId());
        // 渠道主单拓展信息
        if (orderModel.getChannelOrderExtra() != null) {
            result.setErrorCode(orderModel.getChannelOrderExtra().getErrorCode());
            result.setErrorMsg(orderModel.getChannelOrderExtra().getErrorMsg());
        }
        result.setDomesticAddress(salerAddess);
        ReceivingAddressModel buyerAddress = new ReceivingAddressModel();
        if (orderModel.getRecvInfo() != null) {
            buyerAddress.setTelephone(orderModel.getRecvInfo().getTelephone());
            buyerAddress.setMobile(orderModel.getRecvInfo().getMobile());
            buyerAddress.setName(orderModel.getRecvInfo().getRecvName());
            buyerAddress.setAddress(orderModel.getRecvInfo().getAddrDetail());
        }
        result.setOverseasAddress(buyerAddress);
        return RPCResult.ok(result);
        /*******************************************************与上面的方法是一致的**************************************************************/
    }


    /*private SaleOrderDetailVO getSaleOrderDetailVO() {
        SaleOrderDetailVO detailVO = new SaleOrderDetailVO();
        detailVO.setErrorMsg("不想要了");
        detailVO.setErrorCode("PRODUCT_INVALID");
        detailVO.setOrderId("GSP_AE_444622222222");
        detailVO.setChannelName("sss");
        detailVO.setPurchaseOrderId("588ww");
        detailVO.setStatus("WAIT_SEND");
        detailVO.setStatusName("待发货");
        BuyerVO buyerVO = new BuyerVO();
        buyerVO.setChannelName("AE中东大店");
        buyerVO.setCompanyCode("Singapore E-commerce Private Limited");
        buyerVO.setCompanyName("AE中东大店");
        detailVO.setBuyer(buyerVO);
        detailVO.setCreateTime(1566306414000L);
        ReceivingAddressModel receivingAddressModel = new ReceivingAddressModel();
        receivingAddressModel.setAddress("广东省惠州市惠州区18号仓库");
        receivingAddressModel.setMobile("17824428781188");
        receivingAddressModel.setName("张珊www");
        detailVO.setDomesticAddress(receivingAddressModel);
        detailVO.setOverseasAddress(receivingAddressModel);
        SellerVO sellerVO = new SellerVO();
        sellerVO.setChannelName("AE中东大店");
        sellerVO.setCompanyCode("Singapore E-commerce Private Limited");
        sellerVO.setCompanyName("AE中东大店");
        detailVO.setSeller(sellerVO);
        List<SaleSubOrderLogisticsVO> logisticsList = new ArrayList<>();
        SaleSubOrderLogisticsVO vo = new SaleSubOrderLogisticsVO();
        List<SaleSubOrderVO> orderList = new ArrayList<>();
        SaleSubOrderVO saleSubOrderVO = new SaleSubOrderVO();
        saleSubOrderVO.setErrorMsg("去去去去去去");
        saleSubOrderVO.setErrorCode("WSSSS_qqww");
        saleSubOrderVO.setOrderSubId("GSP_AE_777784444");
        saleSubOrderVO.setChannelGoodId("444sww");
        saleSubOrderVO.setOfferTitle("qwww");
        saleSubOrderVO.setPrice(new Double(100.55));
        saleSubOrderVO.setProductId("77778qqq");
        saleSubOrderVO.setPurchaseId("77777998");
        saleSubOrderVO.setQuantity(4);
        saleSubOrderVO.setReceiveAmount(5);
        saleSubOrderVO.setSpec("55w");
        saleSubOrderVO.setTotal(new Double(10.55));
        orderList.add(saleSubOrderVO);
        vo.setOrderList(orderList);
        vo.setCompanyName("wwww8");
        vo.setId("7779");
        vo.setLogisticsId("8888");
        vo.setStatusName("待发货");
        vo.setSupplierId("777999858");
        logisticsList.add(vo);
        detailVO.setLogisticsList(logisticsList);

        return detailVO;
    }*/

    /**
     * 转换channelorder 到 Vo
     *
     * @param model
     * @param purchaseSubOrderMaps
     * @param performOrderMap
     * @return
     */
    private SaleOrderVO convertChannelOrderToSaleOrderVO(FsChannelOrderModel model, Map<String, List<FsPurchaseSubOrderModel>> purchaseSubOrderMaps,
                                                         Map<Long, FsPerformSubOrderModel> performOrderMap,
                                                         Map<Long, ChannelShopDTO> channelShopDTOMap, Map<String, SubMarketInfoDTO> channelSubMarketMap) {

        ChannelMarketEnum channelMarketEnum = ChannelMarketEnum.obtainEnumByOldCode(model.getChannelMarket());


        SaleOrderVO vo = new SaleOrderVO();
        vo.setChannelName(channelMarketEnum.getDesc());
        vo.setChannelId(channelMarketEnum.getOldMarketCode());
        vo.setCreateTime(model.getGmtCreate().getTime());
        vo.setSaleOrderId(model.getOrderId());
        vo.setChannelSellerId(model.getSellerId());
        log.error("qgOrderList:"+model.getChannelMarket()+";"+model.getChannelSubMarket());

        String channelActualSellerIdText = model.getChannelActualSellerId();

        if(StringUtils.isNotBlank(model.getChannelSubMarket())) {
            SubMarketInfoDTO subMarketInfoDTO = channelSubMarketMap.get(model.getChannelSubMarket());
            if(subMarketInfoDTO != null) {
                vo.setChannelSubName(subMarketInfoDTO.getSubMarketDesc());
            }
        }

        if(StringUtils.isNotBlank(channelActualSellerIdText) && StringUtils.isNumeric(channelActualSellerIdText)){

            Long channelActualSellerId = Long.valueOf(channelActualSellerIdText);
            ChannelShopDTO channelShopDTO = channelShopDTOMap.get(channelActualSellerId);

            if(channelShopDTO != null) {
                vo.setShopName(channelShopDTO.getShopName());
            }
        }

        BigDecimal quantity = new BigDecimal(0);
        BigDecimal receiveQuantity = new BigDecimal(0);
        BigDecimal totalPrice = new BigDecimal(0);

        final Map<String, FsChannelOrderSubModel> fsChannelSubOrderMap = new HashMap<String, FsChannelOrderSubModel>();
        if (model != null && !CollectionUtils.isEmpty(model.getSubOrderList())) {
            model.getSubOrderList().stream().forEach(subOrder -> fsChannelSubOrderMap.put(subOrder.getOrderSubId(), subOrder));
        }
        if (purchaseSubOrderMaps.containsKey(model.getOrderId())) {
            List<FsPurchaseSubOrderModel> subPurchaseOrders = purchaseSubOrderMaps.get(model.getOrderId());
            if (subPurchaseOrders != null) {
                for (FsPurchaseSubOrderModel subPurchaseOrder : subPurchaseOrders) {
                    //TODO:后续添加关于状态的判断
                    BigDecimal purchasePrice = new BigDecimal(0);
                    if (fsChannelSubOrderMap.containsKey(subPurchaseOrder.getChannelOrderSubId())) {
                        if (fsChannelSubOrderMap.get(subPurchaseOrder.getChannelOrderSubId()).getChannelSubExtra() != null && fsChannelSubOrderMap.get(subPurchaseOrder.getChannelOrderSubId()).getChannelSubExtra().getChannelPurchasePrice() != null) {
                            purchasePrice = fsChannelSubOrderMap.get(subPurchaseOrder.getChannelOrderSubId()).getChannelSubExtra().getChannelPurchasePrice();
                        }
                    }
                    if (subPurchaseOrder.getBuyAmount() != null) {
                        quantity = quantity.add(subPurchaseOrder.getBuyAmount());
                        totalPrice = totalPrice.add(purchasePrice.multiply(subPurchaseOrder.getBuyAmount()));
                        Long performId = subPurchaseOrder.getPerformId();
                        if (performId != null && performId != 0 && performOrderMap.containsKey(performId)) {
                            String subPerformStatus = performOrderMap.get(performId).getSubStatus();
                            if (!StringUtils.isEmpty(subPerformStatus) && (subPerformStatus.equals(FsPerformStatusEnum.RECEIVE.getValue()) || subPerformStatus.equals(FsPerformStatusEnum.COMPLETE.getValue()))) {
                                receiveQuantity = receiveQuantity.add(subPurchaseOrder.getBuyAmount());
                            }
                        }
                    }
                }
            }
        }
        //TODO:是否只要不取消的都会有对应的履约订单生成
        vo.setQuantity(quantity.intValue());
        vo.setReceiveQuantity(receiveQuantity.intValue());
        vo.setTotalPay(String.valueOf(totalPrice.doubleValue()));
        vo.setId(String.valueOf(model.getChannelMainDbId()));
//
//        if (salePurchaseNameMap.containsKey(model.getOrderId()) && !CollectionUtils.isEmpty(salePurchaseNameMap.get(model.getOrderId()))) {
//            String charseOrderId = String.join(",", salePurchaseNameMap.get(model.getOrderId()));
//            vo.setPurchaseOrderId(charseOrderId);
//        }
        //状态名称
        if (!StringUtils.isEmpty(model.getBizStatus())) {
            FsChannelOrderBizStatus status = FsChannelOrderBizStatus.safeValueOf(model.getBizStatus());
            if (status != null) {
                vo.setStatus(status.getValue());
                vo.setStatusName(status.getDesc());
            }
        }
        //暂时这里处理一下 整单取消
//        if(quantity.intValue()==0){
//            vo.setStatusName(FsChannelOrderBizStatus.ALL_CANCEL.getDesc());
//        }
        return vo;
    }

    private String makeSpec(List<FsAttrModel> attrList){
        if(CollectionUtils.isEmpty(attrList)){
            return null;
        }
        List<String> resultList = Lists.newArrayList();
        for(FsAttrModel attrModel:attrList){
            if(StringUtils.isEmpty(attrModel.getValueText())){
                continue;
            }
            resultList.add(attrModel.getValueText());
        }
        if(CollectionUtils.isEmpty(resultList)){
            return null;
        }
        return StringUtils.join(resultList," ");
    }

    private SaleOrderDetailVO convertSaleDetailVo(FsChannelOrderModel model, final Map<String, List<FsPurchaseSubOrderModel>> subSaleOrderMap, Map<Long, FsPerformSubOrderModel> performOrderMap,Map<Long, String> purchaseSupplierMap) {

        SaleOrderDetailVO vo = new SaleOrderDetailVO();
        if (!StringUtils.isEmpty(model.getChannelMarket())) {
            vo.setChannelName(ChannelMarketEnum.obtainEnumByOldCode(model.getChannelMarket()).getDesc());
        }
        if (model.getOrderCreateTime() != null) {
            vo.setCreateTime(model.getOrderCreateTime().getTime());
        }
        if (!StringUtils.isEmpty(model.getBizStatus())) {
            FsChannelOrderBizStatus status = FsChannelOrderBizStatus.safeValueOf(model.getBizStatus());
            if (status != null) {
                vo.setStatus(status.getValue());
                vo.setStatusName(status.getDesc());
            }
        }
        //结果
        final List<SaleSubOrderLogisticsVO> subOrderList = new ArrayList<SaleSubOrderLogisticsVO>();
        final List<String> purchaseOrderIds = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(model.getSubOrderList())) {
            //组合 - 分组
            //这里暂时还是 1个2段单 ->  1个三段单这样处理 后续如果有拆单 合单 需要修改
            Map<String, List<SaleSubOrderLogisticsVO>> saleSubOrderGroup = model.getSubOrderList().stream().map(subOrder -> {
                        SaleSubOrderLogisticsVO logisticsVo = new SaleSubOrderLogisticsVO();
                        SaleSubOrderVO saleSubOrder = new SaleSubOrderVO();
                        logisticsVo.setOrderList(Lists.newArrayList(saleSubOrder));

                        saleSubOrder.setSpec(makeSpec(subOrder.getGoodSubAttr()));
                        if (subOrder.getChannelSubExtra() != null) {
                            saleSubOrder.setOfferTitle(subOrder.getChannelSubExtra().getProductSubject());
                        }
                        //先设置物流
                        logisticsVo.setId("");
                        logisticsVo.setCompanyName("");
                        if (subOrder.getPurchaseId() != null && subOrder.getPurchaseId() != 0) {
                            saleSubOrder.setPurchaseId(subOrder.getPurchaseId().toString());
                            if (!purchaseOrderIds.contains(subOrder.getPurchaseId().toString())) {
                                purchaseOrderIds.add(subOrder.getPurchaseId().toString());
                            }
                        }
                        if (subOrder.getChannelSubExtra() != null && subOrder.getChannelSubExtra().getChannelPurchasePrice() != null) {
                            saleSubOrder.setPrice(subOrder.getChannelSubExtra().getChannelPurchasePrice().doubleValue());
                        }
                        saleSubOrder.setChannelGoodId(subOrder.getGoodId());
                        if (subOrder.getProductId() != null) {
                            saleSubOrder.setProductId(String.valueOf(subOrder.getProductSubId()));
                        }
                        // 子销售订单编号
                        saleSubOrder.setOrderSubId(subOrder.getOrderSubId());
                        // 子渠道子单拓展信息
                        if (subOrder.getChannelSubExtra() != null) {
                            saleSubOrder.setErrorCode(subOrder.getChannelSubExtra().getErrorCode());
                            saleSubOrder.setErrorMsg(subOrder.getChannelSubExtra().getErrorMsg());
                        }

                        BigDecimal quantity = new BigDecimal(0);
                        BigDecimal receiveAmount = new BigDecimal(0);
                        List<FsPurchaseSubOrderModel> purchaseSubOrderLists = subSaleOrderMap.get(subOrder.getOrderSubId());
                        if (!CollectionUtils.isEmpty(purchaseSubOrderLists)) {
                            for (FsPurchaseSubOrderModel subPurchaseOrder : purchaseSubOrderLists) {
                                quantity = quantity.add(subPurchaseOrder.getBuyAmount());
                                //设置卖家id
                                if (purchaseSupplierMap.containsKey(subPurchaseOrder.getId())) {
                                    logisticsVo.setSupplierId(purchaseSupplierMap.get(subPurchaseOrder.getId()));
                                }
                                /************************************/
                                Long performId = subPurchaseOrder.getPerformId();
                                if (performId != null && performOrderMap.containsKey(performId)) {
                                    FsPerformSubOrderModel performSubOrder = performOrderMap.get(performId);
                                    if (FsPerformStatusEnum.RECEIVE.getValue().equals(performSubOrder.getSubStatus()) || FsPerformStatusEnum.COMPLETE.getValue().equals(performSubOrder.getSubStatus())) {
                                        receiveAmount = receiveAmount.add(subPurchaseOrder.getBuyAmount());
                                    }
                                    if (!StringUtils.isEmpty(performSubOrder.getSubStatus())) {
                                        FsPerformStatusEnum performStatus = FsPerformStatusEnum.valueOf(performSubOrder.getSubStatus());
                                        if (performStatus != null) {
                                            logisticsVo.setStatusName(performStatus.getDesc());
                                        }
                                    }
                                    //在这里设置真实的物流信息
                                    if (performSubOrder.getLogisticsInfo() != null) {
                                        logisticsVo.setId(performSubOrder.getLogisticsInfo().getLogisticsId());
                                        logisticsVo.setCompanyName(performSubOrder.getLogisticsInfo().getCompany());
                                        logisticsVo.setLogisticsId(performSubOrder.getLogisticsInfo().getCompanyLogisticsId());
                                    }
                                }
                            }
                        }

                        saleSubOrder.setQuantity(quantity.intValue());
                        saleSubOrder.setReceiveAmount(receiveAmount.intValue());
                        if (subOrder.getChannelSubExtra() != null && subOrder.getChannelSubExtra().getChannelPurchasePrice() != null) {
                            saleSubOrder.setTotal(quantity.intValue() * subOrder.getChannelSubExtra().getChannelPurchasePrice().doubleValue());
                        }
                        return logisticsVo;
                    }
            ).collect(Collectors.toList()).stream().collect(Collectors.groupingBy(SaleSubOrderLogisticsVO::getId));

            for (String logisticsId : saleSubOrderGroup.keySet()) {
                List<SaleSubOrderLogisticsVO> subOrders = saleSubOrderGroup.get(logisticsId);
                if (StringUtils.isEmpty(logisticsId)) {
                    subOrderList.addAll(subOrders);
                } else {
                    SaleSubOrderLogisticsVO groupVo = new SaleSubOrderLogisticsVO();
                    List<SaleSubOrderVO> subSaleOrderList = new ArrayList<>();
                    groupVo.setId(subOrders.get(0).getId());
                    groupVo.setCompanyName(subOrders.get(0).getCompanyName());
                    groupVo.setStatusName(subOrders.get(0).getStatusName());
                    groupVo.setLogisticsId(subOrders.get(0).getLogisticsId());
                    groupVo.setSupplierId(subOrders.get(0).getSupplierId());

                    subOrders.stream().forEach(suborder -> {
                        subSaleOrderList.add(suborder.getOrderList().get(0));
                    });
                    groupVo.setOrderList(subSaleOrderList);
                    subOrderList.add(groupVo);
                }
            }
        }
        vo.setLogisticsList(subOrderList);
        String charseOrderId = String.join(",", purchaseOrderIds);
        vo.setPurchaseOrderId(charseOrderId);
        return vo;
    }


}
