package com.alibaba.cbuscm.controller.disco.product;

import com.alibaba.cbu.disco.shared.common.wrapper.ListOf;
import com.alibaba.cbu.disco.shared.common.wrapper.PageOf;
import com.alibaba.cbu.disco.shared.common.wrapper.ResultOf;
import com.alibaba.cbu.disco.shared.core.product.api.DcChannelProductReadService;
import com.alibaba.cbu.disco.shared.core.product.api.DcDistributeManageService;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductOpLogQueryService;
import com.alibaba.cbu.disco.shared.core.product.api.DcProductReadService;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductModel;
import com.alibaba.cbu.disco.shared.core.product.model.DcProductSkuModel;
import com.alibaba.cbu.disco.shared.core.product.model.OperatorModel;
import com.alibaba.cbu.disco.shared.core.product.param.DcChannelProductQueryParam;
import com.alibaba.cbu.disco.shared.core.product.param.DcDistributeProductParam;
import com.alibaba.cbu.disco.shared.core.product.param.DcProductQueryParam;
import com.alibaba.cbu.disco.shared.core.product.param.DcProductSkuQueryParam;
import com.alibaba.cbuscm.service.authority.ProductDataAccessService;
import com.alibaba.cbuscm.service.authority.model.DcBizTypeInfo;
import com.alibaba.cbuscm.service.authority.model.DcChannelInfo;
import com.alibaba.cbuscm.service.product.ProductConverter;
import com.alibaba.cbuscm.utils.LoginUtil;
import com.alibaba.up.common.mybatis.result.RPCResult;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 产品管理页面的 Controller
 * @author ruikun.xrk
 */
@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/product-manage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class ProductManageController {

    @Autowired
    private DcProductReadService dcProductReadService;

    @Autowired
    private DcChannelProductReadService dcChannelProductReadService;

    @Autowired
    private ProductDataAccessService productDataAccessService;

    @Autowired
    private DcDistributeManageService dcDistributeManageService;

    @Autowired
    private DcProductOpLogQueryService dcProductOpLogQueryService;


    @ResponseBody
    @RequestMapping(path = "/access-product", method = RequestMethod.GET)
    public Object accessProduct(HttpServletRequest request) {
        try {
            Integer userId = LoginUtil.getUserId(request);
            List<DcBizTypeInfo> dcBizTypeInfos = new ArrayList<>();
            for (int i = 0; i < 1; i++) {
                DcBizTypeInfo dcBizTypeInfo = new DcBizTypeInfo();
                dcBizTypeInfo.setBizType("CROSSBORDER");
                dcBizTypeInfo.setPriceCurrency("234");
                dcBizTypeInfo.setChannel(Lists.newArrayList("test"));
                dcBizTypeInfos.add(dcBizTypeInfo);
            }
            return ListOf.general(dcBizTypeInfos)/*productDataAccessService.queryUserAuthorizedBizType(userId)*/;
        } catch (Throwable t) {
            log.error("ProductManageController.list error!", t);
            return ListOf.error("system-error", t.toString());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/access-channel", method = RequestMethod.GET)
    public Object accessChannel(HttpServletRequest request) {
        try {
            Integer userId = LoginUtil.getUserId(request);
            return productDataAccessService.queryUserAuthorizedChannel(userId);
        } catch (Exception e) {
            log.error("ProductManageController.list error!", e);
            return ListOf.error("system-error", e.toString());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/list-product", method = RequestMethod.GET)
    public Object list(@RequestParam(value = "productId", required = false) List<String> productId,
                       @RequestParam(value = "productSkuId", required = false) List<String> productSkuId,
                       @RequestParam(value = "bizType" , defaultValue = "CROSSBORDER") String bizType,
                       @RequestParam(value = "pageNo", defaultValue = "1") int page,
                       @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
                       HttpServletRequest request) {
        try {
            //Integer userId = LoginUtil.getUserId(request);
            //ListOf<DcBizTypeInfo> bizTypeList = productDataAccessService.queryUserAuthorizedBizType(userId);
            //if (!ListOf.isNonEmpty(bizTypeList) || bizTypeList.getData().stream()
            //    .noneMatch(b -> bizType.equals(b.getBizType()))) {
            //    return PageOf.error("system-error", "no authorized BizType:" + bizType);
           // }
            DcProductQueryParam queryParam = new DcProductQueryParam();
            queryParam.setProductIds(queryProductIdByProductSkuId(productSkuId, productId));
            queryParam.setBizType(bizType);
            queryParam.setPageNo(page);
            queryParam.setPageSize(pageSize);
            queryParam.setReturnSku(true);
            queryParam.setReturnSupplyRelations(true);
            queryParam.setReturnDistributeStatus(true);
            return ProductConverter.convert(dcProductReadService.listProductWithSkuAndSupplyRelation(queryParam),
                ProductConverter::convertProduct);
        } catch (Exception e) {
            log.error("ProductManageController.list error!", e);
            return PageOf.error("system-error", e.toString());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/list-channel-product", method = RequestMethod.GET)
    public Object listChannel(@RequestParam(value = "productId", required = false) List<String> productId,
                              @RequestParam(value = "productSkuId", required = false) List<String> productSkuId,
                              @RequestParam(value = "channel") String channel,
                              @RequestParam(value = "pageNo", defaultValue = "1") int page,
                              @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
                              HttpServletRequest request) {
        try {
            Integer userId = LoginUtil.getUserId(request);
            ListOf<DcChannelInfo> channelList = productDataAccessService.queryUserAuthorizedChannel(userId);
            if (!ListOf.isNonEmpty(channelList) || channelList.getData().stream()
                .noneMatch(c -> channel.equals(c.getChannel()))) {
                return PageOf.error("system-error", "no authorized channel:" + channel);
            }
            DcChannelProductQueryParam queryParam = new DcChannelProductQueryParam();
            queryParam.setProductIds(queryProductIdByProductSkuId(productSkuId, productId));
            queryParam.setChannel(channel);
            queryParam.setPageNo(page);
            queryParam.setPageSize(pageSize);
            queryParam.setReturnChannelSku(true);
            return ProductConverter.convert(dcChannelProductReadService.queryChannelProduct(queryParam),
                ProductConverter::convertChannel);
        } catch (Exception e) {
            log.error("ProductManageController.list error!", e);
            return PageOf.error("system-error", e.toString());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/product-detail", method = RequestMethod.GET)
    public Object getProductDetail(@RequestParam(value = "productId") String productIdStr,
                                   @RequestParam(value = "bizType", defaultValue = "CROSSBORDER") String bizType,
                                   HttpServletRequest request) {
        try {
            Integer userId = LoginUtil.getUserId(request);
           /* ListOf<DcBizTypeInfo> bizTypeList = productDataAccessService.queryUserAuthorizedBizType(userId);
            if (!ListOf.isNonEmpty(bizTypeList) || bizTypeList.getData().stream()
                    .noneMatch(b -> bizType.equals(b.getBizType()))) {
                return PageOf.error("system-error", "no authorized BizType:" + bizType);
            }*/
            Long productId = Long.parseLong(productIdStr);
            ResultOf<DcProductModel> result = dcProductReadService.getProductWithDetail(productId);
            if (result == null || result.getData() == null) {
                return RPCResult.error(result == null ? "system_error" : result.getErrorCode(),
                        result == null ? "未查询到数据" : result.getErrorMessage());
            }
            return RPCResult.ok(ProductConverter.convertProduct(result.getData()));
        } catch (Exception e) {
            log.error("ProductSkuManageController.getSkuInfo error,productId={},errMsg={}", productIdStr, e.getMessage(), e);
            return RPCResult.error("system-error", e.toString());
        }
    }

    @ResponseBody
    @RequestMapping(path = "/distribution", method = RequestMethod.POST)
    public Object postProductDistribute(@RequestBody ProductDistributeParam productDistributeParam,
                                         HttpServletRequest request) {
        try {

            Integer userId = LoginUtil.getUserId(request);

            DcDistributeProductParam param = DcDistributeProductParam.builder()
                    .channels(productDistributeParam.getChannelIds().stream().map(Object::toString).collect(Collectors.toList()))
                    .productIds(productDistributeParam.getProductIds().stream().map(Long::parseLong).collect(Collectors.toList()))
                    .operator(OperatorModel.builder()
                            .id(LoginUtil.getEmpId(request))
                            .name(LoginUtil.getLoginName(request))
                            .build())
                    .build();
            ListOf<Long> successProductIdListOf = dcDistributeManageService.batchProductDistribution(param);
            List<Long> successIds = successProductIdListOf.getData();
            if (successIds.isEmpty()) {
                successProductIdListOf.setSuccess(false);
                successProductIdListOf.setErrorMessage("批量铺货失败");
                successProductIdListOf.setErrorCode("批量铺货失败");
                return successProductIdListOf;
            }
            // 记录铺货失败的id 返回前端
            List<Long> productIds = productDistributeParam.getProductIds().stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
            List<Long> errorIds = new ArrayList<>();
            for (Long productId : productIds) {
                if (!successIds.contains(productId)) {
                    errorIds.add(productId);
                }
            }
            if (errorIds.size() == 0) {
                return successProductIdListOf;
            }
            successProductIdListOf.setSuccess(false);
            successProductIdListOf.setErrorMessage(errorIds.toString() + "铺货失败");
            successProductIdListOf.setErrorCode("铺货失败");
            return successProductIdListOf;
        } catch (Exception e) {
            log.error("ProductManageController.updateSkuAttr error!", e);
            return ResultOf.error("system-error", e.toString());
        }
    }

    @ResponseBody
    @RequestMapping(value = "/op-logs", method = RequestMethod.GET)
    public Object getOperationLogs(@RequestParam(value = "productId") String productId) {
        try {
            return dcProductOpLogQueryService.getLatestProductOperationLog(Long.parseLong(productId));
        } catch (Throwable err) {
            log.error("ERR_QUERY_OP_LOG, productId = {}", productId, err);
            return ListOf.error("ERR_QUERY_OP_LOG", err.getMessage());
        }
    }

    private List<Long> queryProductIdByProductSkuId(List<String> productSkuIds, List<String> defaultProductIds) {
        List<Long> productIds = null;
        if (productSkuIds != null && !productSkuIds.isEmpty()) {
            DcProductSkuQueryParam queryParam = new DcProductSkuQueryParam();
            queryParam.setProductSkuIds(productSkuIds.stream().map(Long::parseLong).collect(Collectors.toList()));
            ListOf<DcProductSkuModel> sku = dcProductReadService.queryProductSku(queryParam);
            if (ListOf.isNonEmpty(sku)) {
                productIds = sku.getData().stream().map(DcProductSkuModel::getProductId).distinct()
                    .collect(Collectors.toList());
            } else {
                productIds = Collections.singletonList(-1L);
            }
        } else if (defaultProductIds != null && !defaultProductIds.isEmpty()) {
            productIds = defaultProductIds.stream().map(Long::parseLong).collect(Collectors.toList());
        }
        return productIds;
    }

    @Getter
    @Setter
    private static class ProductDistributeParam {
        private List<String> productIds;
        private List<Long> channelIds;

    }
}
