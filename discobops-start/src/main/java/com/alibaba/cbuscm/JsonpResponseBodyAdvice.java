/*
 * Copyright 2019 Alibaba.com All right reserved. This software is the
 * confidential and proprietary information of Alibaba.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Alibaba.com.
 */
package com.alibaba.cbuscm;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

/**
 * 类JsonpResponseBodyAdvice.java的实现描述：TODO 类实现描述 
 * @author jizhi.qy 2019年4月19日 下午5:20:58
 */
@ControllerAdvice(basePackages = "com.alibaba.cbuscm.rpc")
public class JsonpResponseBodyAdvice extends AbstractJsonpResponseBodyAdvice {
    public JsonpResponseBodyAdvice() {
        super("callback", "jsonp");
    }
}
