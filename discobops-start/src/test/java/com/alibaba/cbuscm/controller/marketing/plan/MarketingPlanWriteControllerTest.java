package com.alibaba.cbuscm.controller.marketing.plan;

import com.alibaba.cbu.panama.dc.client.model.marketingplan.QualificationInitDTO;
import com.alibaba.cbuscm.service.marketing.plan.MarketingPlanWriteAdapter;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MarketingPlanWriteControllerTest {
    @Autowired
    private MarketingPlanWriteAdapter marketingPlanWriteAdapter;

    @BeforeMethod
    public void setUp() {
        MarketingPlanWriteAdapter marketingPlanWriteAdapter = new MarketingPlanWriteAdapter();
        System.out.println("before");
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("after");
    }

    @Test
    public void testSaveBaseinfo() {
    }

    @Test
    public void testRemoveQualification() {
    }

    @Test
    public void testAddMaterial() {
    }

    @Test
    public void testRemoveMaterial() {
    }

    @Test
    public void testAddWhiteItemIdList() {
    }

    @Test
    public void testRemoveWhiteItemIdList() {
    }

    @Test
    public void testPublishMarketingPlan() {

    }
}