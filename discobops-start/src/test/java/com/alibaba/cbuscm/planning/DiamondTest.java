package com.alibaba.cbuscm.planning;

import java.io.IOException;

import javax.annotation.PostConstruct;

import com.alibaba.cbuscm.BaseTest;

import com.taobao.diamond.client.Diamond;
import com.taobao.diamond.manager.ManagerListenerAdapter;
import org.junit.Test;

/**
 * @author langjing
 * @date 2019/8/31 3:09 下午
 */
public class DiamondTest extends BaseTest {

    private static final String DATA_ID = "com.alibaba.china.cbuscm.disco.bops.matchSupplyMockItem";

    private static final String GROUP = "disco-bops";
    // 属性/开关
    private static String config = "";

    @Test
    public void test() {
        // 测试让主线程不退出，因为订阅配置是守护线程，主线程退出守护线程就会退出，实际代码中不需要。
        while (true) {
            try {
                Thread.sleep(1000);
                String configInfo = Diamond.getConfig(DATA_ID, GROUP, 1000);
                System.out.println("dataId+group:" + configInfo);
            } catch (Exception e) {
            }
        }
    }

    //@PostConstruct
    private static void initConfig() {
        // 启动只用一次场景，直接get获取配置值
        try {
            String configInfo = Diamond.getConfig(DATA_ID, GROUP, 1000);
            System.out.println("dataId+group:" + configInfo);
        } catch (IOException e1) {

        }

        // 启动用，并且变化需要立即推送最新值
        Diamond.addListener(DATA_ID, GROUP,
            new ManagerListenerAdapter() {
                public void receiveConfigInfo(String configInfo) {
                    //推空保护
                    if (configInfo == null) {
                        System.out.println("configInfo is null");
                        return;
                    }

                    try {
                        config = configInfo;
                        System.out.println(configInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
    }

    public static void main(String[] args) throws IOException {
        // 如果使用spring，此类等同于init方法
        initConfig();
        // 测试让主线程不退出，因为订阅配置是守护线程，主线程退出守护线程就会退出，实际代码中不需要。
        while (true) {
            try {
                Thread.sleep(1000);
                System.out.println(getConfig());
            } catch (InterruptedException e) {
            }
        }
    }
    // 通过get接口把配置值暴露出去使用
    public static String getConfig() {
        return config;
    }
}
