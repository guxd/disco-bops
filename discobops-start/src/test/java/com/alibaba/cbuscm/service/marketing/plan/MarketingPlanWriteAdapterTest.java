package com.alibaba.cbuscm.service.marketing.plan;

import com.alibaba.cbu.panama.dc.client.model.DcBaseResult;
import com.alibaba.cbu.panama.dc.client.model.marketingplan.MaterialDTO;
import com.alibaba.cbu.panama.dc.client.service.marketingplan.MarketingPlanWriteService;
import com.alibaba.cbuscm.BopsLogUtil;
import com.alibaba.cbuscm.log.constant.LogConstant;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MarketingPlanWriteAdapterTest {
    @Autowired
    private MarketingPlanWriteService marketingPlanWriteService;

    @BeforeMethod
    public void setUp() {
        Long marketingPlanId = 43002L;
        MaterialDTO materialDTO = new MaterialDTO();
        materialDTO.setCode("dd");
        materialDTO.setName("dd");
        materialDTO.setDesc("dd");
        materialDTO.setRequired(0);
        materialDTO.setParam("dd");
        materialDTO.setType("dd");


    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testSaveBaseinfo() {
    }

    @Test
    public void testAddQualification() {

    }

    @Test
    public void testRemoveQualification() {
    }

    @Test
    public void testAddMaterial(Long marketingPlanId, MaterialDTO material) {
        DcBaseResult<Boolean> dtoResult = marketingPlanWriteService.addMaterial(marketingPlanId, material);
        if (!dtoResult.isSuccess()){
            BopsLogUtil.logServiceFailed("MarketingPlanWriteService","addMaterial.marketingPlanWriteService.addMaterial",
                    String.format("marketingPlanId=%s,material=%s",marketingPlanId,JSON.toJSON(material)),0L,LogConstant.LogType.SAL,new Exception(dtoResult.getErrorDesc()));
        }

    }

    @Test
    public void testRemoveMaterial() {
    }

    @Test
    public void testAddWhiteItemIdList() {
    }

    @Test
    public void testRemoveWhiteItemIdList() {
    }

    @Test
    public void testPublishMarketingPlan() {
    }
}